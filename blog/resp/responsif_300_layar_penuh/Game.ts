let cellAr: Cell[] = [];
let cellMax: number = 100;			//maksimum cell boleh dibuat
const gp: number = 360;
const gl: number = 360;

let peta: string[] = [
	"XXXXXXXXXXX",
	"X         X",
	"X         X",
	"X         X",
	"X    X    X",
	"X    X    X",
	"X    X    X",
	"X    X    X",
	"X    X    X",
	"X         X",
	"XXXXXXXXXXX"
]

const st_idle: number = 1;
const st_jalan: number = 2;

let karakter: Karakter = {
	jalur: [],
	jalurn: 0,
	pindahJml: 4,
	pindahn: 0,
	pos: {
		x: 32,
		y: 32
	},
	status: st_idle
}

let kanvas: HTMLCanvasElement;
let kanvasKtk: CanvasRenderingContext2D;
let kanvasSkala: number = 1;

let gbrBox: HTMLImageElement;
let gbrBola: HTMLImageElement;
let gbrTigan: HTMLImageElement;

window.onload = () => {
	kanvas = document.body.querySelector('canvas') as HTMLCanvasElement;
	kanvasKtk = kanvas.getContext("2d");
	gbrBola = document.body.querySelector("img#img-bola") as HTMLImageElement;
	gbrBox = document.body.querySelector("img#img-box") as HTMLImageElement;
	gbrTigan = document.body.querySelector("img#img-tigan") as HTMLImageElement;

	window.onresize = () => {
		resize();
		render();
	};

	resize();
	render();

	kanvas.onclick = (e: MouseEvent) => {

		if (karakter.status != st_idle) return;

		let pos: Pos2D = proyeksiKlik(e.clientX, e.clientY);

		//posisi grid
		let posx: number = Math.floor(pos.x / 32);
		let posy: number = Math.floor(pos.y / 32);

		let posGrid: Pos2D = krkPosisiGrid(karakter);

		let hasil: number[][] = pfCariJalan(posGrid.x, posGrid.y, posx, posy);

		karakter.status = st_jalan;
		karakter.jalur = hasil;
		karakter.jalurn = -1;
	}

	setInterval(() => {
		update();
		render();
	}, 100);
}

function update() {
	if (karakter.status == st_idle) {

	}
	else if (karakter.status == st_jalan) {
		if (krkCheckPosisiDiGrid(karakter)) {
			karakter.jalurn++;
			if (karakter.jalurn >= karakter.jalur.length - 1) {
				karakter.status = st_idle;
			}
			else {
				karakter.status = st_jalan;
				karakter.pindahn = 0;
				krkPindahGrid(karakter);
			}
		}
		else {
			krkPindahGrid(karakter);
		}
	}
}

function render(): void {
	bersihkanLayar();
	gambarPeta();
	gambarJalan(karakter.jalur);
	gambarKarakter();
}

function gambarKarakter(): void {
	let skala: dimSkala = proyeksiGambar(karakter.pos.x, karakter.pos.y, 32, 32);
	kanvasKtk.drawImage(gbrTigan, skala.x, skala.y, skala.sx, skala.sy);
}

function gambarJalan(hasil: number[][]): void {
	hasil.forEach((item: number[]) => {
		let skala: dimSkala = proyeksiGambar(item[0] * 32 + 8, item[1] * 32 + 8, 16, 16);
		kanvasKtk.drawImage(gbrBola, skala.x, skala.y, skala.sx, skala.sy);
	})
}

function gambarPeta(): void {
	for (let jx: number = 0; jx < peta.length; jx++) {
		for (let ix: number = 0; ix < peta[jx].length; ix++) {
			if (petaKosong(ix, jx)) {

			}
			else {
				let skala: dimSkala = proyeksiGambar(ix * 32, jx * 32);
				kanvasKtk.drawImage(gbrBox, skala.x, skala.y, skala.sx, skala.sy);
			}
		}
	}
}



