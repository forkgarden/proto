"use strict";
function resize() {
    let wp = window.innerWidth;
    let wl = window.innerHeight;
    kanvasSkala = Math.min((wp / gp), (wl / gl));
    kanvas.style.width = '100%';
    kanvas.style.height = '100%';
    kanvas.width = wp;
    kanvas.height = wl;
}
function bersihkanLayar() {
    kanvasKtk.clearRect(0, 0, kanvas.width, kanvas.height);
}
function proyeksiGambar(x, y, p = 32, l = 32) {
    let px;
    let py;
    let sx;
    let sy;
    px = x - (gp / 2);
    py = y - (gl / 2);
    px = px * kanvasSkala;
    py = py * kanvasSkala;
    px = Math.floor(px + (window.innerWidth / 2));
    py = Math.floor(py + (window.innerHeight / 2));
    sx = Math.ceil(p * kanvasSkala);
    sy = Math.ceil(l * kanvasSkala);
    return {
        x: px,
        y: py,
        sx: sx,
        sy: sy
    };
}
function proyeksiKlik(poslx, posly) {
    //posisi di layar relatif terhadap titik tengah
    poslx = poslx - (kanvas.width / 2);
    posly = posly - (kanvas.height / 2);
    //posisi di konversi ke koordinate game
    poslx = poslx / kanvasSkala;
    posly = posly / kanvasSkala;
    //posisi relatif terhadap titik tengah koordinate game
    poslx = poslx + (gp / 2);
    posly = posly + (gl / 2);
    return {
        x: poslx,
        y: posly
    };
}
