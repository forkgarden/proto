interface Cell {
	x: number;
	y: number;
	buka: number;
	jarak: number;
	induk: Cell;

	// x:number;
}

type Pos2D = {
	x: number,
	y: number
}

// type Jalur = {
// 	n: number;
// 	data: number[][]
// }

// type Langkah = {
// 	// target:Pos2D,
// 	n: number,
// 	nMax: number
// }

type Karakter = {
	pos: Pos2D,
	jalurn: number,
	jalur: number[][],

	pindahn: number,
	pindahJml: number,

	status: number
}

interface dimSkala {
	x: number,
	y: number,
	sx: number,
	sy: number
}