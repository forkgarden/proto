"use strict";
function resize(pjLayar, lbLayar) {
    //ratio skala dari resolusi game ke resolusi target
    kanvasSkala = Math.min((pjLayar / gp), (lbLayar / gl));
    if (kanvasSkala > 1.2) {
        //bila skala terlalu besar, ulangi lagi dengan ukurang yang lebih kecil
        resize(pjLayar / kanvasSkala, lbLayar / kanvasSkala);
    }
    else {
        //ukuran resolusi baru kanvas
        kanvas.width = Math.ceil(pjLayar);
        kanvas.height = Math.ceil(lbLayar);
        //ratio antara skala resolusi game terhadap resolusi kanvas
        kanvasSkala = Math.min(kanvas.width / gp, kanvas.height / gl);
        //ratio antara skala kanvas dibandingkan layar
        kanvasSkalaX = window.innerWidth / kanvas.width;
        kanvasSkalaY = window.innerHeight / kanvas.height;
        //kanvas dibuat full screen
        kanvas.style.width = '100%';
        kanvas.style.height = '100%';
    }
}
function proyeksiGambar(x, y, p = 32, l = 32) {
    let px;
    let py;
    let sx;
    let sy;
    //posisi pinggir ke tengah
    px = x - (gp / 2);
    py = y - (gl / 2);
    //posisi game ke kanvas
    px = px * kanvasSkala;
    py = py * kanvasSkala;
    //posisi tengah ke pinggir
    px = ((kanvas.width / 2) + px);
    py = ((kanvas.height / 2) + py);
    //skala
    sx = Math.ceil(p * kanvasSkala);
    sy = Math.ceil(l * kanvasSkala);
    return {
        x: px,
        y: py,
        sx: sx,
        sy: sy
    };
}
function bersihkanLayar() {
    kanvasKtk.clearRect(0, 0, kanvas.width, kanvas.height);
}
function test2() {
    let pos = proyeksiGambar(0, 0);
    kanvasKtk.strokeRect(pos.x - 1, pos.y - 1, 2, 2);
    pos = proyeksiGambar(gp, 0);
    kanvasKtk.strokeRect(pos.x - 1, pos.y - 1, 2, 2);
    pos = proyeksiGambar(gp, gl);
    kanvasKtk.strokeRect(pos.x - 1, pos.y - 1, 2, 2);
    pos = proyeksiGambar(0, gl);
    kanvasKtk.strokeRect(pos.x - 1, pos.y - 1, 2, 2);
    pos = proyeksiGambar(gp / 2, gl / 2);
    kanvasKtk.strokeRect(pos.x - 1, pos.y - 1, 2, 2);
}
function proyeksiKlik(poslx, posly) {
    //posisi layar ke  kanvas
    poslx = (poslx / kanvasSkalaX);
    posly = (posly / kanvasSkalaY);
    //posisi pinggir ke  tengah
    poslx = poslx - (kanvas.width / 2);
    posly = posly - (kanvas.height / 2);
    //posisi kanvas ke game
    poslx = poslx / kanvasSkala;
    posly = posly / kanvasSkala;
    //posisi tengah ke pinggir
    poslx = poslx + (gp / 2);
    posly = posly + (gl / 2);
    return {
        x: poslx,
        y: posly
    };
}
