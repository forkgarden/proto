function resize(pjLayar: number, lbLayar: number) {

	//ratio skala dari resolusi game ke resolusi target
	kanvasSkala = Math.min((pjLayar / gp), (lbLayar / gl));

	if (kanvasSkala > 1.2) {
		//bila skala terlalu besar, ulangi lagi dengan ukurang yang lebih kecil
		resize(pjLayar / kanvasSkala, lbLayar / kanvasSkala);
	}
	else {
		//ukuran resolusi baru kanvas
		kanvas.width = Math.ceil(pjLayar);
		kanvas.height = Math.ceil(lbLayar);

		//ratio antara skala resolusi game terhadap resolusi kanvas
		kanvasSkala = Math.min(kanvas.width / gp, kanvas.height / gl);

		//ratio antara skala kanvas dibandingkan layar
		kanvasSkalaX = window.innerWidth / kanvas.width;
		kanvasSkalaY = window.innerHeight / kanvas.height;

		//kanvas dibuat full screen
		kanvas.style.width = '100%';
		kanvas.style.height = '100%';
	}

}

function proyeksiKanvasKeLayar(x: number, y: number): Pos2D {
	return {
		x: x * kanvasSkalaX,
		y: y * kanvasSkalaY
	}
}


function proyeksiKanvasKeGame(px: number, py: number): Pos2D {
	//pinggir ke tengah

	//kanvas ke game

	//tengah ke pinggir
}

function proyeksiGameKeKanvas(px: number, py: number): Pos2D {
	//posisi pinggir ke tengah
	px = px - (gp / 2);
	py = py - (gl / 2);

	//posisi game ke kanvas
	px = px * kanvasSkala;
	py = py * kanvasSkala;

	//posisi tengah ke pinggir
	px = ((kanvas.width / 2) + px);
	py = ((kanvas.height / 2) + py);

	return {
		x: px,
		y: py
	}

}

function proyeksiGambar(x: number, y: number, p: number = 32, l: number = 32): dimSkala {
	// let px: number;
	// let py: number;
	let sx: number;
	let sy: number;
	let pos: Pos2D;

	pos = proyeksiGameKeKanvas(x, y);

	//skala
	sx = Math.ceil(p * kanvasSkala);
	sy = Math.ceil(l * kanvasSkala);

	return {
		x: pos.x,
		y: pos.y,
		sx: sx,
		sy: sy
	}
}

function bersihkanLayar(): void {
	kanvasKtk.clearRect(0, 0, kanvas.width, kanvas.height);
}

function test2(): void {
	let pos: dimSkala = proyeksiGambar(0, 0);
	kanvasKtk.strokeRect(pos.x - 1, pos.y - 1, 2, 2);

	pos = proyeksiGambar(gp, 0);
	kanvasKtk.strokeRect(pos.x - 1, pos.y - 1, 2, 2);

	pos = proyeksiGambar(gp, gl);
	kanvasKtk.strokeRect(pos.x - 1, pos.y - 1, 2, 2);

	pos = proyeksiGambar(0, gl);
	kanvasKtk.strokeRect(pos.x - 1, pos.y - 1, 2, 2);

	pos = proyeksiGambar(gp / 2, gl / 2);
	kanvasKtk.strokeRect(pos.x - 1, pos.y - 1, 2, 2);
}

function proyeksiKlik(poslx: number, posly: number): Pos2D {

	//layar ke kanvas

	//kanvas ke game

	//posisi layar ke  kanvas
	poslx = (poslx / kanvasSkalaX);
	posly = (posly / kanvasSkalaY);

	//posisi pinggir ke  tengah
	poslx = poslx - (kanvas.width / 2); dc
	posly = posly - (kanvas.height / 2);

	//posisi kanvas ke game
	poslx = poslx / kanvasSkala;
	posly = posly / kanvasSkala;

	//posisi tengah ke pinggir
	poslx = poslx + (gp / 2);
	posly = posly + (gl / 2);

	return {
		x: poslx,
		y: posly
	}
}