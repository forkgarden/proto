let canvas: HTMLCanvasElement;
let canvasCtx: CanvasRenderingContext2D;
let gambar: HTMLImageElement;

window.onload = () => {
	canvas = document.body.querySelector('canvas') as HTMLCanvasElement;
	canvasCtx = canvas.getContext("2d");
	gambar = document.body.querySelector('img.gambar') as HTMLImageElement;
	gambarCanvas(canvasCtx);

	window.onresize = resize;
	resize();

	setTimeout(() => {
		resize();
	}, 100);
}

function resize() {
	let cp = 360;
	let cl = 218;
	let wp = window.innerWidth;
	let wl = window.innerHeight;
	let ratio = Math.max((wp / cp), (wl / cl));
	let cp2 = Math.floor(cp * ratio);
	let cl2 = Math.floor(cl * ratio);

	canvas.style.width = cp2 + 'px';
	canvas.style.height = cl2 + 'px';

	canvas.style.top = ((wl - cl2) / 2) + 'px';
	canvas.style.left = ((wp - cp2) / 2) + 'px';

	gambarCanvas(canvasCtx);
}

function gambarCanvas(ctx: CanvasRenderingContext2D): void {

	ctx.drawImage(gambar, 0, 0);
}
