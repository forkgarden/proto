const RG_KOSONG: number = 1;
const RG_UBIN: number = 2;
const RG_TEMBOK: number = 3;
const RG_PINTU_KANAN: number = 4;
const RG_PINTU_KIRI: number = 5;

const RGST_AWAL: number = 1;
const RGST_TEMBOK: number = 2;
const RGST_PILIH_TEMBOK: number = 3;
const RGST_UBIN_BARU: number = 4;
const RGST_SELESAI: number = 5;

let rgPeta: Cell[][] = [];
let rgState: number = 1;
let rgTembokAktif: Cell;
let rgJmlUbinMax: number = 40;
let rgJmlUbin: number = 0;

let rgPetaPjg: number = 10;
let rgPetalbr: number = 10;

function rgPasangPintuKiri(): void {
	let posx: number = 1000;
	let posy: number = 1000;

	for (let i: number = 0; i < rgPetalbr; i++) {
		for (let j: number = 0; j < rgPetaPjg; j++) {
			if (rgPeta[i][j].type == RG_TEMBOK) {
				if (rgPintuKiriOk(i, j)) {
					if (i < posx) {
						posx = i;
						posy = j;
					}
				}
			}
		}
	}

	if (posx < 1000) {
		rgPeta[posx][posy].type = RG_PINTU_KIRI;
	}
}

function rgPintuKiriOk(x: number, y: number): boolean {
	if (x == rgPetaPjg - 1) return false;
	if (rgPeta[x + 1][y].type != RG_UBIN) return false;
	return true;
}

function rgPintuKananOk(x: number, y: number): boolean {
	if (x == 0) return false;
	if (rgPeta[x - 1][y].type != RG_UBIN) return false;
	return true;
}

function rgSekelilingAdaUbin(x: number, y: number): boolean {

	if (x > 0) {
		if (y > 0) {
			if (rgPeta[x - 1][y - 1].type == RG_UBIN) return true;
		}
		if (y < rgPetalbr - 1) {
			if (rgPeta[x - 1][y + 1].type == RG_UBIN) return true;
		}
	}

	if (x < rgPetaPjg - 1) {
		if (y > 0) {
			if (rgPeta[x + 1][y - 1].type == RG_UBIN) return true;
		}

		if (y < rgPetalbr - 1) {
			if (rgPeta[x + 1][y + 1].type == RG_UBIN) return true;
		}

	}


	return rgSebelahAdaUbin(x, y);
}

function rgStateUbinBaru(): void {
	if (rgTembokAktif) {
		rgTembokAktif.type = RG_UBIN;
		rgState = RGST_TEMBOK;
		rgJmlUbin++;
	}
}

function rgBuat(): void {
	rgReset();
	while (rgState != RGST_SELESAI) {
		// console.log('udate state ' + rgState);
		rgStateUpdate();
	}
}

function rgPinggir(x: number, y: number): boolean {
	if (x == 0) return true;
	if (x == rgPetaPjg - 1) return true;
	if (y == 0) return true;
	if (y == rgPetalbr - 1) return true;

	return false;
}

function rgSebelahAdaUbin(x: number, y: number): boolean {

	if (x > 0) {
		if (rgPeta[x - 1][y].type == RG_UBIN) return true;
	}

	if (x < rgPetaPjg - 1) {
		if (rgPeta[x + 1][y].type == RG_UBIN) return true;
	}

	if (y > 0) {
		if (rgPeta[x][y - 1].type == RG_UBIN) return true;
	}

	if (y < rgPetalbr - 1) {
		if (rgPeta[x][y + 1].type == RG_UBIN) return true;
	}

	return false;
}

function rgPasangPintuKanan(): void {
	let posx: number = -1;
	let posy: number = -1;

	for (let i: number = 0; i < rgPetalbr; i++) {
		for (let j: number = 0; j < rgPetaPjg; j++) {
			if (rgPeta[i][j].type == RG_TEMBOK) {
				if (rgPintuKananOk(i, j)) {
					if (i > posx) {
						posx = i;
						posy = j;
					}
				}
			}
		}
	}

	if (posx >= 0) {
		rgPeta[posx][posy].type = RG_PINTU_KANAN;
	}
}


function rgStateAwal(): void {
	rgPeta[Math.floor(rgPetaPjg / 2)][Math.floor(rgPetalbr / 2)].type = RG_UBIN;
	rgState = RGST_TEMBOK
}

function rgStateBangunTembok(): void {
	//set tembok
	for (let i: number = 0; i < rgPetalbr; i++) {
		for (let j: number = 0; j < rgPetaPjg; j++) {
			if (rgPeta[i][j].type == RG_KOSONG) {
				if (rgSekelilingAdaUbin(i, j)) {
					rgPeta[i][j].type = RG_TEMBOK;
				}
			}
		}
	}

	if (rgJmlUbin >= rgJmlUbinMax) {
		rgPasangPintuKiri();
		rgPasangPintuKanan();
		rgState = RGST_SELESAI;
	}
	else {
		rgState = RGST_PILIH_TEMBOK;
	}
}

function rgStatePilihTembok(): void {
	let kotakAr: Cell[] = [];

	for (let i: number = 0; i < rgPetalbr; i++) {
		for (let j: number = 0; j < rgPetaPjg; j++) {
			if (rgPeta[i][j].type == RG_TEMBOK) {
				if (rgSebelahAdaUbin(i, j) && !rgPinggir(i, j)) {
					kotakAr.push(rgPeta[i][j]);
				}
			}
		}
	}

	rgTembokAktif = kotakAr[Math.floor(Math.random() * kotakAr.length)];
	rgState = RGST_UBIN_BARU;
}

function rgReset(): void {
	for (let i: number = 0; i < rgPetalbr; i++) {
		rgPeta[i] = new Array(10);
		for (let j: number = 0; j < rgPetaPjg; j++) {
			rgPeta[i][j] = {
				type: RG_KOSONG,
				pemilik: []
			}

		}
	}

	rgState = RGST_AWAL;
}

function rgStateUpdate(): void {
	// state++;

	if (rgState == RGST_AWAL) {
		rgStateAwal();
	} else if (rgState == RGST_TEMBOK) {
		rgStateBangunTembok();
	}
	else if (rgState == RGST_PILIH_TEMBOK) {
		rgStatePilihTembok();
	}
	else if (rgState == RGST_UBIN_BARU) {
		rgStateUbinBaru();
	}
	else if (rgState == RGST_SELESAI) {
		//selesai
	}
	else {
		throw Error('state tidak terdaftar');
	}

}

