class Bagi {
    private acak: Acak = new Acak(19);

    constructor() {
        this.bacaSession();
        console.log(config);
    }

    private bacaSession(): void {
        let sesStr: string = window.sessionStorage.getItem('config');

        console.log(sesStr);

        if (!sesStr || sesStr.length == 0) {
            return;
        }

        config = JSON.parse(sesStr);
    }

    buat(): void {
        let str: string = '';

        console.log('buat');
        // console.log(this.config);
        // console.log(this.config.jmlHal);

        for (let hal: number = 0; hal < config.jmlHal; hal++) {
            // console.log(hal);
            str += `
                <div class='text-align-right'>hal ${hal + 1}</div>
                <div class="judul">${config.judul}</div>
                <hr/>
                <div class="row">
                    ${this.buatSoalN(config.jmlSoal)}
                </div>
                <div class="ganti_hal"></div>
            `;

            // console.log(str);
        }

        // console.log(str);

        document.body.querySelector('.container .soal-cont').innerHTML = str;

    }

    private buatSoalN(n: number): string {
        let hasil: string = '';

        for (let i: number = 0; i < n; i++) {
            hasil += this.buatSoalSingle();
        }

        return hasil;
    }

    buatSoalSingle(): string {
        let angka1: number;
        let angka2: number;
        let hasil: string = ''
        // let ulang: number = 0;

        angka2 = Math.floor(Math.random() * config.kali.max) + config.kali.offset;
        // angka2 = this.acak.angka() + 2;

        angka1 = angka2 * (Math.floor(Math.random() * 8) + 2);

        hasil = `
            <div class='text-align-left col-12'>
                <div>${angka1} : ${angka2} = ....................................... </div>
                <br/>
            </div>
        `;

        return hasil;
    }

}