class Susun {
	private acak: Acak = new Acak(10);
	// private config: Config;

	constructor() {
		this.bacaSession();
	}

	private bacaSession(): void {
		let sesStr: string = window.sessionStorage.getItem('config');

		if (!sesStr || sesStr.length == 0) {
			// config = new Config();
			return;
		}

		config = JSON.parse(sesStr);
	}

	buat(): void {
		let str: string = '';

		console.log('buat');
		// console.log(config);
		// console.log(config.jmlHal);

		for (let hal: number = 0; hal < config.jmlHal; hal++) {
			// console.log(hal);
			str += `
                <div class='text-align-right'>hal ${hal + 1}</div>
                <div class="judul">${config.judul}</div>
                <hr/>
                <div class="row">
                    ${this.buatSoalN(config.jmlSoal)}
                </div>
                <div class="ganti_hal"></div>
            `;

			// console.log(str);
		}

		// console.log(str);

		document.body.querySelector('.container .soal-cont').innerHTML = str;

	}

	private gantiChar(str: string, char: string, idx: number): string {
		let str2: string = '';

		for (let i: number = 0; i < str.length; i++) {
			let charIdx: string = str.charAt(i);

			if (i != idx) {
				str2 += charIdx;
			}
			else {
				str2 += char;
			}
		}

		return str2;
	}

	private simpanTambah(angka1: number, angka2: number): number {
		let angka1Str: string = angka1 + '';
		let angka2Str: string = angka2 + '';

		for (let i: number = angka1Str.length - 1; i >= 0; i--) {
			let c1iStr: string = angka1Str.charAt(i);
			let c2iStr: string = angka2Str.charAt(i);

			let c1i: number = parseInt(c1iStr);
			let c2i: number = parseInt(c2iStr);

			// console.group('');
			// console.log('angka 1 ' + angka1Str + '/angka 2 ' + angka2Str);
			// console.log('c1i ' + c1i + '/c2i ' + c2i);

			this.acak.acak();
			while ((c2i + c1i) >= 10) {
				c2i = this.acak.angka();

				if (c2i == 0 && c1i != 9) c2i = 99;
			}

			angka2Str = this.gantiChar(angka2Str, c2i + '', i);

			// console.log('angka 1 ' + angka1Str + '/angka 2 ' + angka2Str);
			// console.log('c1i ' + c1i + '/c2i ' + c2i);

			// console.groupEnd();

		}

		return parseInt(angka2Str);
	}

	private simpanKurang(angka1: number, angka2: number): number {
		let angka1Str: string = angka1 + '';
		let angka2Str: string = angka2 + '';

		for (let i: number = angka1Str.length - 1; i >= 0; i--) {
			let c1iStr: string = angka1Str.charAt(i);
			let c2iStr: string = angka2Str.charAt(i);

			let c1i: number = parseInt(c1iStr);
			let c2i: number = parseInt(c2iStr);

			this.acak.acak();
			while (c2i > c1i) {
				c2i = this.acak.angka();
				if (c2i == 0 && c1i != 0) c2i = 99;
			}

			angka2Str = this.gantiChar(angka2Str, c2i + '', i);
		}

		return parseInt(angka2Str);
	}

	private buatSoalN(n: number): string {
		let hasil: string = '';

		for (let i: number = 0; i < n; i++) {
			hasil += this.buatSoalSingle();
		}

		return hasil;
	}

	buatSoalSingle(): string {
		let angka1: number;
		let angka2: number;
		let hasil: string = ''

		angka1 = Math.floor(Math.random() * config.max) + config.offset;
		angka2 = Math.floor(Math.random() * config.max) + config.offset;

		// angka1 = 1000;

		if (config.tanda == "x") {
			angka2 = Math.floor(Math.random() * 8) + 2;
		}
		else if (config.tanda == "-") {
			while (angka2 > angka1) {
				angka2 = Math.floor(Math.random() * config.max) + config.offset;
			}

			if (config.simpan) {
				angka2 = this.simpanKurang(angka1, angka2);
			}
		}

		else if (config.tanda == "+") {
			if (config.simpan) {
				angka2 = this.simpanTambah(angka1, angka2);
			}
		}

		hasil = `
            <div class='text-align-right col-2'>
                <div>${angka1} &nbsp;</div>
                <div>${angka2} &nbsp;</div>
                <div>==== ${config.tanda}</div>
                <div>.... &nbsp;</div>
                <br/>
                <br/>
            </div>
        `;

		return hasil;
	}

}