class Hal {
    private hal: string = '';
    private susun: Susun = new Susun();
    private bagi2: Bagi = new Bagi();
    private soalType: Function[] = [];

    buat(): void {
        this.soalType.push(() => {
            this.jumlah();
        });

        this.soalType.push(() => {
            this.kali();
        });

        this.soalType.push(() => {
            this.kurang()
        });

        this.soalType.push(() => {
            this.bagi()
        });

        for (let i: number = 0; i < 100; i++) {
            let a: number = Math.floor(Math.random() * this.soalType.length);
            let b: number = Math.floor(Math.random() * this.soalType.length);

            let c: Function = this.soalType[a];
            this.soalType[a] = this.soalType[b];
            this.soalType[b] = c;
        }

        // this.soalType[0]();

        for (let i: number = 0; i < this.soalType.length; i++) {
            this.soalType[i]();
        }

        // this.jumlah();
        // this.kali();
        // this.kurang();
        // this.bagi();
    }

    bagi(): void {
        config.tanda = "x";
        this.hal += `<div class='row'>`;
        for (let i: number = 0; i < 5; i++) {
            this.hal += this.bagi2.buatSoalSingle();
        }
        this.hal += '</div>'

        document.body.innerHTML = this.hal;
    }

    kali(): void {
        config.tanda = "x";
        this.hal += `<div class='row'>`;
        for (let i: number = 0; i < 5; i++) {
            this.hal += this.susun.buatSoalSingle();
        }
        this.hal += '</div>'

        document.body.innerHTML = this.hal;

    }

    kurang(): void {
        console.log(this);
        config.tanda = "-";
        this.hal += `<div class='row'>`;
        for (let i: number = 0; i < 5; i++) {
            this.hal += this.susun.buatSoalSingle();
        }
        this.hal += '</div>'

        document.body.innerHTML = this.hal;

    }

    jumlah(): void {
        //penjumlahan
        config.tanda = "+";
        this.hal += `<div class='row'>`;
        for (let i: number = 0; i < 5; i++) {
            this.hal += this.susun.buatSoalSingle();
        }
        this.hal += '</div>'

        document.body.innerHTML = this.hal;
    }
}

let hal: Hal = new Hal();
hal.buat();