import { Acak } from "./Acak.js";
class JumlahTanpaSimpan {
    acak;
    acakAngka;
    _max = 10000;
    _min = 1000;
    _jmlSoal = 20;
    buat() {
        console.log('buat');
        let str = '';
        this.acak = new Acak(this._max - this._min);
        this.acakAngka = new Acak(10);
        for (let i = 0; i < this._jmlSoal; i++) {
            str += this.buatSoal();
        }
        document.body.querySelector('.container .soal-cont').innerHTML = str;
    }
    buatSoal() {
        let angka1;
        let angka2;
        let hasil = '';
        console.group('buat soal');
        angka1 = this.acak.angka() + this._min;
        angka2 = this.buatAngka2Min(angka1);
        console.debug('angka 1 ' + angka1);
        console.debug('angka 2 ' + angka2);
        hasil = `
            <div class='text-align-right col-3'>
                <div>${angka1} &nbsp;</div>
                <div>${angka2} &nbsp;</div>
                <div>==== +</div>
                <div>.... &nbsp;</div>
                <br/>
                <br/>
            </div>
        `;
        console.groupEnd();
        return hasil;
    }
    buatAngka2Min(angka1) {
        let ctr = 0;
        while (true) {
            let hasil = this.buatAngka2(angka1);
            if (hasil > this._min)
                return hasil;
            ctr++;
            if (ctr > 1000)
                throw Error('buatAngka2Min');
        }
    }
    buatAngka2(angka1) {
        let hasil = 0;
        let angka1Str = angka1 + '';
        let angka2Str = '';
        console.group('buat angka 2');
        console.debug(angka1);
        for (let i = angka1Str.length - 1; i >= 0; i--) {
            let char1 = '';
            let char2;
            char1 = angka1Str.slice(i, i + 1);
            char2 = this.buatAngka3(parseInt(char1)) + '';
            angka2Str = char2 + angka2Str;
            console.debug('char 1 ' + char1);
            console.debug('char 2 ' + char2);
            console.debug('angka ' + angka2Str);
        }
        console.groupEnd();
        hasil = parseInt('0000' + angka2Str);
        return hasil;
    }
    buatAngka3(angka) {
        if (angka >= 10)
            throw Error(angka + '');
        this.acakAngka.acak();
        let ctr = 0;
        while (true) {
            let angka2 = this.acakAngka.angka();
            if (angka + angka2 < 10)
                return angka2;
            ctr++;
            if (ctr > 1000)
                throw Error('buatAngka3');
        }
    }
    get jmlSoal() {
        return this._jmlSoal;
    }
    set jmlSoal(value) {
        this._jmlSoal = value;
    }
    get max() {
        return this._max;
    }
    set max(value) {
        this._max = value;
    }
    get min() {
        return this._min;
    }
    set min(value) {
        this._min = value;
    }
}
let app = new JumlahTanpaSimpan();
app.buat();
