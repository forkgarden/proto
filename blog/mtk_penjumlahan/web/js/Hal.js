"use strict";
class Hal {
    hal = '';
    susun = new Susun();
    bagi2 = new Bagi();
    soalType = [];
    buat() {
        this.soalType.push(() => {
            this.jumlah();
        });
        this.soalType.push(() => {
            this.kali();
        });
        this.soalType.push(() => {
            this.kurang();
        });
        this.soalType.push(() => {
            this.bagi();
        });
        for (let i = 0; i < 100; i++) {
            let a = Math.floor(Math.random() * this.soalType.length);
            let b = Math.floor(Math.random() * this.soalType.length);
            let c = this.soalType[a];
            this.soalType[a] = this.soalType[b];
            this.soalType[b] = c;
        }
        // this.soalType[0]();
        for (let i = 0; i < this.soalType.length; i++) {
            this.soalType[i]();
        }
        // this.jumlah();
        // this.kali();
        // this.kurang();
        // this.bagi();
    }
    bagi() {
        config.tanda = "x";
        this.hal += `<div class='row'>`;
        for (let i = 0; i < 5; i++) {
            this.hal += this.bagi2.buatSoalSingle();
        }
        this.hal += '</div>';
        document.body.innerHTML = this.hal;
    }
    kali() {
        config.tanda = "x";
        this.hal += `<div class='row'>`;
        for (let i = 0; i < 5; i++) {
            this.hal += this.susun.buatSoalSingle();
        }
        this.hal += '</div>';
        document.body.innerHTML = this.hal;
    }
    kurang() {
        console.log(this);
        config.tanda = "-";
        this.hal += `<div class='row'>`;
        for (let i = 0; i < 5; i++) {
            this.hal += this.susun.buatSoalSingle();
        }
        this.hal += '</div>';
        document.body.innerHTML = this.hal;
    }
    jumlah() {
        //penjumlahan
        config.tanda = "+";
        this.hal += `<div class='row'>`;
        for (let i = 0; i < 5; i++) {
            this.hal += this.susun.buatSoalSingle();
        }
        this.hal += '</div>';
        document.body.innerHTML = this.hal;
    }
}
let hal = new Hal();
hal.buat();
