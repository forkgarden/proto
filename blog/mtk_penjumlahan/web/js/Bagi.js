"use strict";
class Bagi {
    acak = new Acak(19);
    constructor() {
        this.bacaSession();
        console.log(config);
    }
    bacaSession() {
        let sesStr = window.sessionStorage.getItem('config');
        console.log(sesStr);
        if (!sesStr || sesStr.length == 0) {
            return;
        }
        config = JSON.parse(sesStr);
    }
    buat() {
        let str = '';
        console.log('buat');
        // console.log(this.config);
        // console.log(this.config.jmlHal);
        for (let hal = 0; hal < config.jmlHal; hal++) {
            // console.log(hal);
            str += `
                <div class='text-align-right'>hal ${hal + 1}</div>
                <div class="judul">${config.judul}</div>
                <hr/>
                <div class="row">
                    ${this.buatSoalN(config.jmlSoal)}
                </div>
                <div class="ganti_hal"></div>
            `;
            // console.log(str);
        }
        // console.log(str);
        document.body.querySelector('.container .soal-cont').innerHTML = str;
    }
    buatSoalN(n) {
        let hasil = '';
        for (let i = 0; i < n; i++) {
            hasil += this.buatSoalSingle();
        }
        return hasil;
    }
    buatSoalSingle() {
        let angka1;
        let angka2;
        let hasil = '';
        // let ulang: number = 0;
        angka2 = Math.floor(Math.random() * config.kali.max) + config.kali.offset;
        // angka2 = this.acak.angka() + 2;
        angka1 = angka2 * (Math.floor(Math.random() * 8) + 2);
        hasil = `
            <div class='text-align-left col-12'>
                <div>${angka1} : ${angka2} = ....................................... </div>
                <br/>
            </div>
        `;
        return hasil;
    }
}
