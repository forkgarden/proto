"use strict";
// import { Acak } from "./Acak.js";
class kali {
    // private acak: Acak;
    // private acakAngka: Acak;
    _max = 10000;
    _min = 1000;
    _jmlSoal = 30;
    buat() {
        console.log('buat');
        let str = '';
        // this.acak = new Acak(this._max - this._min);
        // this.acakAngka = new Acak(10);
        for (let i = 0; i < this._jmlSoal; i++) {
            str += this.buatSoal();
        }
        document.body.querySelector('.container .soal-cont').innerHTML = str;
    }
    buatSoal() {
        let angka1;
        let angka2;
        let hasil = '';
        console.group('buat soal');
        angka1 = Math.floor(Math.random() * 9000) + 1000;
        angka2 = Math.floor(Math.random() * 8) + 2;
        hasil = `
            <div class='text-align-right col-2'>
                <div>${angka1} &nbsp;</div>
                <div>${angka2} &nbsp;</div>
                <div>==== x </div>
                <div>.... &nbsp;</div>
                <br/>
                <br/>
            </div>
        `;
        console.groupEnd();
        return hasil;
    }
    get jmlSoal() {
        return this._jmlSoal;
    }
    set jmlSoal(value) {
        this._jmlSoal = value;
    }
    get max() {
        return this._max;
    }
    set max(value) {
        this._max = value;
    }
    get min() {
        return this._min;
    }
    set min(value) {
        this._min = value;
    }
}
let app = new kali();
app.buat();
