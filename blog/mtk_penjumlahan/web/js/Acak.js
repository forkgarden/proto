"use strict";
class Acak {
    idx = 0;
    angkas = [];
    _max = 0;
    // private _offset: number = 0;
    constructor(max) {
        this._max = max;
        this.resetArray();
        this.acak();
    }
    test() {
        this._max = 10;
        for (let i = 0; i < 100; i++) {
            let res = this.angka();
            if (res >= 10)
                throw new Error();
        }
        this.max = 1;
        for (let i = 0; i < 1000; i++) {
            let res = this.angka();
            if (res >= 1)
                throw new Error(res + '');
        }
        //TODO:test acak kecuali
    }
    angka() {
        this.idx++;
        if (this.idx >= this.angkas.length - 1) {
            this.acak();
            this.idx = 0;
        }
        return this.angkas[this.idx];
    }
    acak() {
        let i = 0;
        let ax = 0;
        let bx = 0;
        let max = 0;
        let c = 0;
        max = this._max * 10;
        for (i = 0; i < max; i++) {
            ax = Math.floor(Math.random() * this._max);
            bx = Math.floor(Math.random() * this._max);
            c = this.angkas[ax];
            this.angkas[ax] = this.angkas[bx];
            this.angkas[bx] = c;
        }
    }
    resetArray() {
        this.angkas = [];
        for (let i = 0; i < this._max; i++) {
            this.angkas.push(i);
        }
    }
    set max(value) {
        this._max = value;
        this.resetArray();
        this.acak();
    }
}
