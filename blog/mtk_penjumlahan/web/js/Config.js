"use strict";
let config = {
    jmlSoal: 30,
    offset: 10,
    max: 100,
    jmlHal: 7,
    simpan: false,
    judul: 'Pengurangan',
    tanda: "-",
    kali: {
        max: 10,
        offset: 1,
    }
};
