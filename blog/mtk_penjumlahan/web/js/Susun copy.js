"use strict";
class Susun {
    acak = new Acak(10);
    config;
    constructor() {
        this.bacaSession();
    }
    bacaSession() {
        let sesStr = window.sessionStorage.getItem('config');
        if (!sesStr || sesStr.length == 0) {
            this.config = new Config();
            return;
        }
        this.config = JSON.parse(sesStr);
    }
    buat() {
        let str = '';
        console.log('buat');
        // console.log(this.config);
        // console.log(this.config.jmlHal);
        for (let hal = 0; hal < this.config.jmlHal; hal++) {
            // console.log(hal);
            str += `
                <div class='text-align-right'>hal ${hal + 1}</div>
                <div class="judul">${config.judul}</div>
                <hr/>
                <div class="row">
                    ${this.buatSoalN(config.jmlSoal)}
                </div>
                <div class="ganti_hal"></div>
            `;
            // console.log(str);
        }
        // console.log(str);
        document.body.querySelector('.container .soal-cont').innerHTML = str;
    }
    gantiChar(str, char, idx) {
        let str2 = '';
        for (let i = 0; i < str.length; i++) {
            let charIdx = str.charAt(i);
            if (i != idx) {
                str2 += charIdx;
            }
            else {
                str2 += char;
            }
        }
        return str2;
    }
    simpanTambah(angka1, angka2) {
        let angka1Str = angka1 + '';
        let angka2Str = angka2 + '';
        for (let i = angka1Str.length - 1; i >= 0; i--) {
            let c1iStr = angka1Str.charAt(i);
            let c2iStr = angka2Str.charAt(i);
            let c1i = parseInt(c1iStr);
            let c2i = parseInt(c2iStr);
            // console.group('');
            // console.log('angka 1 ' + angka1Str + '/angka 2 ' + angka2Str);
            // console.log('c1i ' + c1i + '/c2i ' + c2i);
            this.acak.acak();
            while ((c2i + c1i) >= 10) {
                c2i = this.acak.angka();
                if (c2i == 0 && c1i != 9)
                    c2i = 99;
            }
            angka2Str = this.gantiChar(angka2Str, c2i + '', i);
            // console.log('angka 1 ' + angka1Str + '/angka 2 ' + angka2Str);
            // console.log('c1i ' + c1i + '/c2i ' + c2i);
            // console.groupEnd();
        }
        return parseInt(angka2Str);
    }
    simpanKurang(angka1, angka2) {
        let angka1Str = angka1 + '';
        let angka2Str = angka2 + '';
        for (let i = angka1Str.length - 1; i >= 0; i--) {
            let c1iStr = angka1Str.charAt(i);
            let c2iStr = angka2Str.charAt(i);
            let c1i = parseInt(c1iStr);
            let c2i = parseInt(c2iStr);
            this.acak.acak();
            while (c2i > c1i) {
                c2i = this.acak.angka();
                if (c2i == 0 && c1i != 0)
                    c2i = 99;
            }
            angka2Str = this.gantiChar(angka2Str, c2i + '', i);
        }
        return parseInt(angka2Str);
    }
    buatSoalN(n) {
        let hasil = '';
        for (let i = 0; i < n; i++) {
            hasil += this.buatSoalSingle();
        }
        return hasil;
    }
    buatSoalSingle() {
        let angka1;
        let angka2;
        let hasil = '';
        angka1 = Math.floor(Math.random() * this.config.max) + this.config.offset;
        angka2 = Math.floor(Math.random() * this.config.max) + this.config.offset;
        // angka1 = 1000;
        if (this.config.tanda == "x") {
            angka2 = Math.floor(Math.random() * 10);
        }
        else if (this.config.tanda == "-") {
            if (this.config.simpan) {
                angka2 = this.simpanKurang(angka1, angka2);
            }
        }
        else if (this.config.tanda == "+") {
            if (this.config.simpan) {
                angka2 = this.simpanTambah(angka1, angka2);
            }
        }
        hasil = `
            <div class='text-align-right col-2'>
                <div>${angka1} &nbsp;</div>
                <div>${angka2} &nbsp;</div>
                <div>==== ${config.tanda}</div>
                <div>.... &nbsp;</div>
                <br/>
                <br/>
            </div>
        `;
        return hasil;
    }
}
