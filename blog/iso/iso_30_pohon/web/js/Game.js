"use strict";
let kanvas;
let kanvasCtx;
let ubin;
let pohon;
window.onload = () => {
    kanvas = document.body.querySelector('canvas');
    kanvasCtx = kanvas.getContext("2d");
    ubin = document.body.querySelector('img.gbr-ubin');
    pohon = document.body.querySelector('img.gbr-pohon');
    window.onresize = resize;
    resize();
    gambar();
};
function gambar() {
    console.group('');
    let ix = 0;
    let jx = 0;
    for (let i = 0; i <= 1; i++) {
        for (let j = 0; j < 18; j++) {
            if (j % 2 == 0) {
                ix = j / 2;
                jx = j / 2;
                // jx -= i;
                console.log(i + '/' + j + ' = ' + ix + '/' + jx);
                kanvasCtx.drawImage(ubin, isoProjectX(ix * 32, jx * 32) + 120 - 32, isoProjectZ(ix * 32, jx * 32));
            }
        }
    }
    console.groupEnd();
    /*
    for (let i: number = 0; i < 10; i++) {
        for (let j: number = 0; j < 10; j++) {
            if (j >= 3 && (j <= 6)) {
                if ((i >= 3) && (i <= 6)) {
                    kanvasCtx.drawImage(pohon, isoProjectX(i * 32, j * 32) + 120 - 18, isoProjectZ(i * 32, j * 32) - 73 + 16);
                }
            }
        }
    }
    */
}
function resize() {
    let cp = 240;
    let cl = 320;
    let wp = window.innerWidth;
    let wl = window.innerHeight;
    let ratio = Math.min((wp / cp), (wl / cl));
    let cp2 = Math.floor(cp * ratio);
    let cl2 = Math.floor(cl * ratio);
    kanvas.style.width = cp2 + 'px';
    kanvas.style.height = cl2 + 'px';
    kanvas.style.top = ((wl - cl2) / 2) + 'px';
    kanvas.style.left = ((wp - cp2) / 2) + 'px';
    gambar();
}
