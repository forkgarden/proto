"use strict";
let kanvas;
let kanvasCtx;
let ubin;
window.onload = () => {
    kanvas = document.body.querySelector('canvas');
    kanvasCtx = kanvas.getContext("2d");
    ubin = document.body.querySelector('img.gbr-ubin');
    window.onresize = resize;
    resize();
    gambar();
};
function gambar() {
    for (let i = 0; i < 9; i++) {
        for (let j = 0; j < 9; j++) {
            kanvasCtx.drawImage(ubin, isoProjectX(i * 32, j * 32) + 120 - 32, isoProjectZ(i * 32, j * 32));
        }
    }
}
function resize() {
    let cp = 240;
    let cl = 320;
    let wp = window.innerWidth;
    let wl = window.innerHeight;
    let ratio = Math.min((wp / cp), (wl / cl));
    let cp2 = Math.floor(cp * ratio);
    let cl2 = Math.floor(cl * ratio);
    kanvas.style.width = cp2 + 'px';
    kanvas.style.height = cl2 + 'px';
    kanvas.style.top = ((wl - cl2) / 2) + 'px';
    kanvas.style.left = ((wp - cp2) / 2) + 'px';
    gambar();
}
