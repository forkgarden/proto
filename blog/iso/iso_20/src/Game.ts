let kanvas: HTMLCanvasElement;
let kanvasCtx: CanvasRenderingContext2D;
let ubin: HTMLImageElement;

window.onload = () => {
	kanvas = document.body.querySelector('canvas') as HTMLCanvasElement;
	kanvasCtx = kanvas.getContext("2d");
	ubin = document.body.querySelector('img.gbr-ubin') as HTMLImageElement;

	window.onresize = resize;
	resize();
	gambar();

}

function gambar(): void {
	for (let i: number = 0; i < 9; i++) {
		for (let j: number = 0; j < 9; j++) {
			kanvasCtx.drawImage(ubin, isoProjectX(i * 32, j * 32) + 120 - 32, isoProjectZ(i * 32, j * 32));
		}
	}
}

function resize(): void {
	let cp = 240;
	let cl = 320;

	let wp = window.innerWidth;
	let wl = window.innerHeight;

	let ratio = Math.min((wp / cp), (wl / cl));
	let cp2 = Math.floor(cp * ratio);
	let cl2 = Math.floor(cl * ratio);

	kanvas.style.width = cp2 + 'px';
	kanvas.style.height = cl2 + 'px';
	kanvas.style.top = ((wl - cl2) / 2) + 'px';
	kanvas.style.left = ((wp - cp2) / 2) + 'px';

	gambar();
}