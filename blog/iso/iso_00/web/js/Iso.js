"use strict";
function layar2IsoX(sx, sy) {
    return (sx + 2 * sy) / 2;
}
function layar2IsoY(sx, sy) {
    return (2 * sy - sx) / 2;
}
function isoProjectX(isoX, isoY) {
    return (isoX - isoY);
}
function isoProjectZ(isoX, isoY) {
    return (isoX + isoY) / 2;
}
