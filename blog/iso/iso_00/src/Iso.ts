function layar2IsoX(sx: number, sy: number): number {
	return (sx + 2 * sy) / 2;
}

function layar2IsoY(sx: number, sy: number): number {
	return (2 * sy - sx) / 2;
}

function isoProjectX(isoX: number, isoY: number): number {
	return (isoX - isoY);
}

function isoProjectZ(isoX: number, isoY: number): number {
	return (isoX + isoY) / 2;
}
