let kanvas: HTMLCanvasElement;
let kanvasCtx: CanvasRenderingContext2D;

window.onload = () => {
	kanvas = document.body.querySelector('canvas') as HTMLCanvasElement;
	kanvasCtx = kanvas.getContext("2d");
	window.onresize = resize;
	resize();
	gambar();

}

function gambar(): void {
	kanvasCtx.fillStyle = "#0000ff";

	for (let i: number = 0; i < 16; i++) {
		for (let j: number = 0; j < 16; j++) {
			kanvasCtx.fillRect(isoProjectX(i * 8, j * 8) + 120, isoProjectZ(i * 8, j * 8), 2, 2);
		}
	}
}

function resize(): void {
	let cp = 240;
	let cl = 320;

	let wp = window.innerWidth;
	let wl = window.innerHeight;

	let ratio = Math.min((wp / cp), (wl / cl));
	let cp2 = Math.floor(cp * ratio);
	let cl2 = Math.floor(cl * ratio);

	kanvas.style.width = cp2 + 'px';
	kanvas.style.height = cl2 + 'px';
	kanvas.style.top = ((wl - cl2) / 2) + 'px';
	kanvas.style.left = ((wp - cp2) / 2) + 'px';

	gambar();
}