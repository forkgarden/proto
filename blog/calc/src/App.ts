
class App {
	private views: Item[] = [];

	//TODO: optimise file on load
	//optimise file saat maximise
	constructor() {
		let item: NodeListOf<Element> = document.querySelectorAll('item');

		item.forEach((item: Element) => {
			let view: Item = new Item();
			view.init(item as HTMLElement);
			this.views.push(view);
			console.log(item);
		});
	}

	static getEl(query: string): HTMLElement {
		let el: HTMLElement;

		el = document.body.querySelector(query);

		if (el) {
			return el
		} else {
			console.log(document.body);
			console.log(query);
			throw new Error('query not found ');
		}
	}

}

class Item extends BaseComponent {
	constructor() {
		super();
	}

	init(el: HTMLElement): void {

		this._elHtml = el;

		this._elHtml.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			console.log('item on click');
			this._elHtml.classList.add('fokus');
			document.body.style.overflowY = 'hidden';
			this.gbrBesar.src = this.gbrBesar.getAttribute('gbr');
		}


		this.gbrKecil.src = this.gbrKecil.getAttribute('gbr');

		this.tutupTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			this._elHtml.classList.remove('fokus');
			document.body.style.overflowY = 'auto';
		}

		this.chatTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			window.top.location.href = ''; //TODO: chat
		}
	}

	get waP(): HTMLParagraphElement {
		return this.getEl('p.wa') as HTMLParagraphElement;
	}

	get chatTbl(): HTMLLinkElement {
		return this.getEl('a.chat') as HTMLLinkElement;
	}

	get tutupTbl(): HTMLButtonElement {
		return this.getEl('p.tutup button') as HTMLButtonElement;
	}

	get gbrKecil(): HTMLImageElement {
		return this.getEl('img.kecil') as HTMLImageElement;
	}

	get gbrBesar(): HTMLImageElement {
		return this.getEl('img.besar') as HTMLImageElement;
	}
}

window.onload = () => {
	new App();
}