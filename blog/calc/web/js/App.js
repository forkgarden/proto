"use strict";
class App {
    //TODO: optimise file on load
    //optimise file saat maximise
    constructor() {
        this.views = [];
        let item = document.querySelectorAll('item');
        item.forEach((item) => {
            let view = new Item();
            view.init(item);
            this.views.push(view);
            console.log(item);
        });
    }
    static getEl(query) {
        let el;
        el = document.body.querySelector(query);
        if (el) {
            return el;
        }
        else {
            console.log(document.body);
            console.log(query);
            throw new Error('query not found ');
        }
    }
}
class Item extends BaseComponent {
    constructor() {
        super();
    }
    init(el) {
        this._elHtml = el;
        this._elHtml.onclick = (e) => {
            e.stopPropagation();
            console.log('item on click');
            this._elHtml.classList.add('fokus');
            document.body.style.overflowY = 'hidden';
            this.gbrBesar.src = this.gbrBesar.getAttribute('gbr');
        };
        this.gbrKecil.src = this.gbrKecil.getAttribute('gbr');
        this.tutupTbl.onclick = (e) => {
            e.stopPropagation();
            this._elHtml.classList.remove('fokus');
            document.body.style.overflowY = 'auto';
        };
        this.chatTbl.onclick = (e) => {
            e.stopPropagation();
            window.top.location.href = ''; //TODO: chat
        };
    }
    get waP() {
        return this.getEl('p.wa');
    }
    get chatTbl() {
        return this.getEl('a.chat');
    }
    get tutupTbl() {
        return this.getEl('p.tutup button');
    }
    get gbrKecil() {
        return this.getEl('img.kecil');
    }
    get gbrBesar() {
        return this.getEl('img.besar');
    }
}
window.onload = () => {
    new App();
};
