"use strict";
class Byte {
    kompress(data, byte) {
        let hasil = '';
        for (let i = 0; i < data.length; i++) {
            let char = data.charAt(i);
            let kode = data.charCodeAt(i);
            let bin = this.char2bin(kode);
            if (char == byte) {
                hasil += '0';
            }
            else {
                hasil += ('1' + bin) + '';
            }
        }
        return hasil;
    }
    char2bin(angka) {
        let bin = '000000000';
        bin += angka.toString(2);
        bin = bin.slice(bin.length - 8);
        return bin;
    }
    string2bin(data) {
        let hasil = '';
        for (let i = 0; i < data.length; i++) {
            let char = data.charCodeAt(i);
            let bin = this.char2bin(char);
            hasil += bin;
        }
        return hasil;
    }
    ektrak(bin) {
        let char;
        let hasil = '';
        while (bin.length > 0) {
            char = bin.slice(0, 1);
            if (char == '0') {
                hasil += this.char2bin('a'.charCodeAt(0));
                bin = bin.slice(1);
            }
            else if (char == '1') {
                let bin2 = bin.slice(1, 9);
                hasil += bin2;
                bin = bin.slice(9);
            }
        }
        return hasil;
    }
}
let b = new Byte();
let data;
let dataBin;
let hasil;
let ekstrak;
//contoh 1:
data = 'aaabcd';
dataBin = b.string2bin(data);
hasil = b.kompress(data, 'a');
ekstrak = b.ektrak(hasil);
console.log('input : ' + data);
console.log('input (bin)  : ' + dataBin);
console.log('hasil (bin)  : ' + hasil);
console.log('kompresi : ' + (100 - ((hasil.length / dataBin.length) * 100)) + '%');
console.log('validasi :' + (ekstrak == dataBin));
console.log('------------');
console.log('');
//contoh 2:
data = 'aaaaaaaaaa';
dataBin = b.string2bin(data);
hasil = b.kompress(data, 'a');
ekstrak = b.ektrak(hasil);
console.log('input : ' + data);
console.log('input(bin)  : ' + dataBin);
console.log('hasil (bin) : ' + hasil);
console.log('kompresi : ' + (100 - ((hasil.length / dataBin.length) * 100)) + '%');
console.log('validasi :' + (ekstrak == dataBin));
console.log('------------');
console.log('');
//contoh 3:
data = 'abcdefgh';
dataBin = b.string2bin(data);
hasil = b.kompress(data, 'a');
ekstrak = b.ektrak(hasil);
console.log('input : ' + data);
console.log('input(bin)  : ' + dataBin);
console.log('hasil (bin) : ' + hasil);
console.log('kompresi : ' + (100 - ((hasil.length / dataBin.length) * 100)) + '%');
console.log('validasi :' + (ekstrak == dataBin));
console.log('------------');
console.log('');
//contoh 4:
data = 'abcdefghijkl';
dataBin = b.string2bin(data);
hasil = b.kompress(data, 'a');
ekstrak = b.ektrak(hasil);
console.log('input : ' + data);
console.log('input(bin)  : ' + dataBin);
console.log('hasil (bin) : ' + hasil);
console.log('kompresi : ' + (100 - ((hasil.length / dataBin.length) * 100)) + '%');
console.log('validasi :' + (ekstrak == dataBin));
console.log('------------');
console.log('');
