"use strict";
class K4 {
    constructor() {
        //
        this.data = [];
        this.stat = [];
        this.variasiData = 0;
        this.jmlBitData = 8; //tidak boleh berubah
        this.jmlData = 2048;
        this.jmlUntung = 7;
        this.variasiData = Math.pow(2, this.jmlBitData);
        // this.jmlUntung = this.jmlBitData - 1;
        console.log(this.jmlBitData);
        console.log(this.variasiData);
        console.log(this.jmlUntung);
        // this.data = this.buatData();
        this.init().catch((e) => {
            console.log(e);
        });
    }
    async init() {
        this.data = await this.buatData();
        await this.hitung();
        await this.urut();
        await this.hitungUntung();
        await this.validasi();
        this.rekap();
    }
    async validasi() {
        //validasi jumlah
        let jml = 0;
        this.stat.forEach((item) => {
            jml += item.jml;
        });
        if (jml > this.data.length) {
            console.log('jml ' + jml);
            console.log('jml data ' + this.data.length);
            throw Error('validasi jumlah');
        }
    }
    rekap() {
        let untung = 0;
        let rugi = 0;
        let indexMin = 255;
        let ctr = 0;
        this.stat.forEach((item) => {
            untung += item.untung;
            rugi += item.rugi;
            if (item.untung > item.rugi) {
                if (ctr <= indexMin) {
                    indexMin = ctr;
                }
            }
            ctr++;
        });
        console.log('untung: ' + untung);
        console.log('rugi  : ' + rugi);
        console.log('rugi %: ' + Math.floor((rugi / untung)));
        console.log('index : ' + (255 - indexMin));
        // for (let i: number = indexMin - 1; i < this.stat.length; i++) {
        // 	console.log(this.stat[i]);
        // }
    }
    renderHasil() {
        this.stat.forEach((item) => {
            console.log(item);
        });
    }
    tambah(stat, nilai) {
        for (let i = 0; i < stat.length; i++) {
            if (stat[i].key == nilai) {
                stat[i].jml++;
                return;
            }
        }
        stat.push({
            key: nilai,
            jml: 0,
            untung: 0,
            rugi: 0
        });
    }
    async hitung() {
        for (let i = 0; i < this.data.length; i++) {
            this.tambah(this.stat, this.data[i]);
        }
    }
    async hitungUntung() {
        for (let i = 0; i < this.stat.length; i++) {
            let stat = this.stat[i];
            stat.untung = stat.jml * this.jmlUntung;
            stat.rugi = await this.hitungRugi(i);
        }
    }
    async hitungRugi(idx) {
        let hasil = 0;
        for (let i = idx + 1; i < this.stat.length; i++) {
            let stat = this.stat[i];
            hasil += stat.jml;
        }
        return hasil;
    }
    async urut() {
        let ulang = true;
        let ctr = 0;
        while (ulang) {
            ulang = false;
            ctr++;
            for (let i = 0; i < this.stat.length - 1; i++) {
                let item1 = this.stat[i];
                let item2 = this.stat[i + 1];
                if (item2.jml > item1.jml) {
                    this.tukar(item1, item2);
                    ulang = true;
                }
            }
            if (ctr > 1000000)
                ulang = true;
        }
    }
    tukar(item1, item2) {
        let c;
        c = item1.key;
        item1.key = item2.key;
        item2.key = c;
        c = item1.jml;
        item1.jml = item2.jml;
        item2.jml = c;
    }
    async buatData() {
        let hasil = [];
        for (let i = 0; i < this.jmlData; i++) {
            hasil.push(Math.floor(Math.random() * this.variasiData));
        }
        return hasil;
    }
}
new K1();
