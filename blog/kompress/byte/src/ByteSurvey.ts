export { } //typescript bug work-around

interface IHuruf {
	kode: number,
	jml: number;
}

class ByteSurvey {

	kompress(data: string): string {
		let hasil: string = '';
		let byte: number;
		let survey: IHuruf[] = [];

		survey = this.survey(data);
		byte = survey[0].kode;

		for (let i: number = 0; i < data.length; i++) {
			// let char: string = data.charAt(i);
			let kode: number = data.charCodeAt(i);
			let bin: string = this.char2bin(kode);

			if (kode == byte) {
				hasil += '0';
			}
			else {
				hasil += ('1' + bin) + '';
			}
		}

		return hasil;
	}

	survey(data: string): IHuruf[] {
		let hasil: IHuruf[] = [];

		for (let i: number = 0; i < data.length; i++) {
			let huruf: string = data[i];
			let kode: number = huruf.charCodeAt(0);

			//check data ada, kalau ada update jumlahnya
			let ada: boolean = false;
			for (let j: number = 0; j < hasil.length; j++) {
				if (hasil[j].kode == kode) {
					hasil[j].jml++;
					ada = true;
					break;
				}
			}

			//kalau data tidak ada masukkan data baru
			if (!ada) {
				hasil.push({
					kode: kode,
					jml: 0
				})
			}
		}

		//urutkan data
		let ulangi: boolean = true;
		while (ulangi) {
			ulangi = false;

			for (let i: number = 0; i < hasil.length - 1; i++) {
				if (hasil[i].jml < hasil[i + 1].jml) {
					let kode: number = hasil[i].kode;
					hasil[i].kode = hasil[i + 1].kode;
					hasil[i + 1].kode = kode;

					let jml: number = hasil[i].jml;
					hasil[i].jml = hasil[i + 1].jml;
					hasil[i + 1].jml = jml;

					ulangi = true;
				}
			}
		}

		return hasil;
	}

	char2bin(angka: number): string {
		let bin: string = '000000000';
		bin += angka.toString(2);
		bin = bin.slice(bin.length - 8);
		return bin;
	}

	string2bin(data: string): string {
		let hasil: string = '';

		for (let i: number = 0; i < data.length; i++) {
			let char: number = data.charCodeAt(i);
			let bin: string = this.char2bin(char);
			hasil += bin;
		}

		return hasil;
	}

	ektrak(bin: string): string {
		let char: string;
		let hasil: string = '';

		while (bin.length > 0) {
			char = bin.slice(0, 1);

			if (char == '0') {
				hasil += this.char2bin('a'.charCodeAt(0));
				bin = bin.slice(1);
			}
			else if (char == '1') {
				let bin2: string = bin.slice(1, 9);
				hasil += bin2
				bin = bin.slice(9);
			}

		}

		return hasil;
	}
}

let b: ByteSurvey = new ByteSurvey();

let data: string;
let dataBin: string;
let hasil: string;
let ekstrak: string;

//contoh 1:
data = 'aaabcd';
dataBin = b.string2bin(data);
hasil = b.kompress(data);
ekstrak = b.ektrak(hasil);

console.log('input : ' + data);
console.log('input (bin)  : ' + dataBin);
console.log('hasil (bin)  : ' + hasil);
console.log('kompresi : ' + (100 - ((hasil.length / dataBin.length) * 100)) + '%');
console.log('validasi :' + (ekstrak == dataBin));
console.log('------------');
console.log('');

//contoh 2:
data = 'aaaaaaaaaa';
dataBin = b.string2bin(data);
hasil = b.kompress(data);
ekstrak = b.ektrak(hasil);

console.log('input : ' + data);
console.log('input(bin)  : ' + dataBin);
console.log('hasil (bin) : ' + hasil);
console.log('kompresi : ' + (100 - ((hasil.length / dataBin.length) * 100)) + '%');
console.log('validasi :' + (ekstrak == dataBin));
console.log('------------');
console.log('');


//contoh 3:
data = 'abcdefgh';
dataBin = b.string2bin(data);
hasil = b.kompress(data);
ekstrak = b.ektrak(hasil);

console.log('input : ' + data);
console.log('input(bin)  : ' + dataBin);
console.log('hasil (bin) : ' + hasil);
console.log('kompresi : ' + (100 - ((hasil.length / dataBin.length) * 100)) + '%');
console.log('validasi :' + (ekstrak == dataBin));
console.log('------------');
console.log('');

//contoh 4:
data = 'abcdefghijkl';
dataBin = b.string2bin(data);
hasil = b.kompress(data);
ekstrak = b.ektrak(hasil);

console.log('input : ' + data);
console.log('input(bin)  : ' + dataBin);
console.log('hasil (bin) : ' + hasil);
console.log('kompresi : ' + (100 - ((hasil.length / dataBin.length) * 100)) + '%');
console.log('validasi :' + (ekstrak == dataBin));
console.log('------------');
console.log('');
