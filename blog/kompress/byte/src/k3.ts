class K3 {

	//

	private data: number[] = [];
	private stat: IStat[] = [];
	private variasiData: number = 0;

	private jmlBitData: number = 8;	//tidak boleh berubah
	private jmlData: number = 2048;
	private jmlUntung: number = 7;

	constructor() {

		this.variasiData = Math.pow(2, this.jmlBitData);
		// this.jmlUntung = this.jmlBitData - 1;

		console.log(this.jmlBitData);
		console.log(this.variasiData);
		console.log(this.jmlUntung);

		// this.data = this.buatData();
		this.init().catch((e) => {
			console.log(e);
		})
	}

	async init(): Promise<void> {
		this.data = await this.buatData();
		await this.hitung();
		await this.urut();
		await this.hitungUntung();
		await this.validasi();
		this.rekap();
	}

	async validasi(): Promise<void> {
		//validasi jumlah
		let jml: number = 0;
		this.stat.forEach((item: IStat) => {
			jml += item.jml;
		});

		if (jml > this.data.length) {
			console.log('jml ' + jml);
			console.log('jml data ' + this.data.length);
			throw Error('validasi jumlah');
		}
	}

	rekap(): void {
		let untung: number = 0;
		let rugi: number = 0;
		let indexMin: number = 255;
		let ctr: number = 0;

		this.stat.forEach((item: IStat) => {
			untung += item.untung;
			rugi += item.rugi;
			if (item.untung > item.rugi) {
				if (ctr <= indexMin) {
					indexMin = ctr;
				}
			}
			ctr++;
		});

		console.log('untung: ' + untung);
		console.log('rugi  : ' + rugi);
		console.log('rugi %: ' + Math.floor((rugi / untung)));
		console.log('index : ' + (255 - indexMin));

		// for (let i: number = indexMin - 1; i < this.stat.length; i++) {
		// 	console.log(this.stat[i]);
		// }
	}

	renderHasil(): void {
		this.stat.forEach((item: IStat) => {
			console.log(item);
		});
	}

	tambah(stat: IStat[], nilai: number): void {
		for (let i: number = 0; i < stat.length; i++) {
			if (stat[i].key == nilai) {
				stat[i].jml++;
				return;
			}
		}

		stat.push({
			key: nilai,
			jml: 0,
			untung: 0,
			rugi: 0
		});
	}

	async hitung(): Promise<void> {
		for (let i: number = 0; i < this.data.length; i++) {
			this.tambah(this.stat, this.data[i]);
		}
	}

	async hitungUntung(): Promise<void> {
		for (let i: number = 0; i < this.stat.length; i++) {
			let stat: IStat = this.stat[i];
			stat.untung = stat.jml * this.jmlUntung;
			stat.rugi = await this.hitungRugi(i);
		}
	}

	async hitungRugi(idx: number): Promise<number> {
		let hasil: number = 0;

		for (let i: number = idx + 1; i < this.stat.length; i++) {
			let stat: IStat = this.stat[i];
			hasil += stat.jml;
		}
		return hasil;
	}

	async urut(): Promise<void> {
		let ulang: boolean = true;
		let ctr: number = 0;

		while (ulang) {
			ulang = false;
			ctr++;

			for (let i: number = 0; i < this.stat.length - 1; i++) {
				let item1: IStat = this.stat[i];
				let item2: IStat = this.stat[i + 1];

				if (item2.jml > item1.jml) {
					this.tukar(item1, item2);
					ulang = true;
				}
			}

			if (ctr > 1000000) ulang = true;

		}


	}

	tukar(item1: IStat, item2: IStat): void {
		let c: number

		c = item1.key;
		item1.key = item2.key;
		item2.key = c;

		c = item1.jml;
		item1.jml = item2.jml;
		item2.jml = c;
	}

	async buatData(): Promise<number[]> {
		let hasil: number[] = [];

		for (let i: number = 0; i < this.jmlData; i++) {
			hasil.push(Math.floor(Math.random() * this.variasiData));
		}

		return hasil;
	}

}

interface IStat {
	key: number,
	jml: number,
	untung: number,
	rugi: number
}

new K1();