jln.ui = {
	editTbl: document.body.querySelector('div.header button.edit') as HTMLButtonElement,
	jalanTbl: document.body.querySelector('div.header button.jalan') as HTMLButtonElement,
}

jln.ui.kontainer2 = {
	el: document.body.querySelector('div.kontainer-utama div.kontainer-2') as HTMLDivElement,
	daftarFile: {
		el: document.body.querySelector('div.kontainer-utama div.kontainer-2 div.daftar-file') as HTMLDivElement,
	},
	editText: {
		el: document.body.querySelector('div.kontainer-utama div.kontainer-2 div.edit-text') as HTMLDivElement,
		textArea: document.body.querySelector('div.kontainer-utama div.kontainer-2 div.edit-text textarea') as HTMLTextAreaElement,
		judul: document.body.querySelector('div.kontainer-utama div.kontainer-2 div.edit-text div.judul') as HTMLDivElement,
		kembaliTbl: document.body.querySelector('div.kontainer-utama div.kontainer-2 div.edit-text button.kembali') as HTMLButtonElement,
		muatUlangTbl: document.body.querySelector('div.kontainer-utama div.kontainer-2 div.edit-text button.muat-ulang') as HTMLButtonElement
	}
}

jln.ui.kontainer2.daftarFile.daftarCss = jln.ui.kontainer2.daftarFile.el.querySelector('div.daftar-css') as HTMLDivElement;
jln.ui.kontainer2.daftarFile.daftarJs = jln.ui.kontainer2.daftarFile.el.querySelector('div.daftar-js') as HTMLDivElement;
jln.ui.kontainer2.daftarFile.daftarHtml = jln.ui.kontainer2.daftarFile.el.querySelector('div.daftar-html') as HTMLDivElement;

jln.ui.kontainer2.web = {
	el: jln.ui.kontainer2.el.querySelector('div.web') as HTMLDivElement,
	iframe: jln.ui.kontainer2.el.querySelector('div.web iframe') as HTMLIFrameElement,
}