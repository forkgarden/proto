interface IJalanObj {
	init?: () => Promise<void>;
	compile?: Function;
	load?: (url: string) => Promise<string>;
	data?: IProject;
	template?: (query: string) => HTMLElement;
	buatItem?: (file: IFile, cont: HTMLDivElement) => void;
	editFile?: (item: IFile) => void;
	html?: (str: string) => string;

	ui?: {
		editTbl?: HTMLButtonElement;
		jalanTbl?: HTMLButtonElement;
		kontainer2?: {
			el?: HTMLDivElement;
			daftarFile?: {
				el?: HTMLDivElement;
				daftarCss?: HTMLDivElement;
				daftarJs?: HTMLDivElement;
				daftarHtml?: HTMLDivElement;
			};
			editText?: {
				el: HTMLDivElement,
				textArea?: HTMLTextAreaElement,
				judul?: HTMLDivElement,
				kembaliTbl?: HTMLButtonElement,
				muatUlangTbl?: HTMLButtonElement
			},
			web?: {
				el?: HTMLDivElement
				iframe?: HTMLIFrameElement;
			}
		}
	},	

	loading?: {
		el?: HTMLDivElement,
		tampil?: () => void,
		tutup?: () => void
	}

}

interface IJalan {
	compile?: Function;
	load?: (url: string) => Promise<string>;
}

interface IData {

}

interface IProject {
	nama: string;
	file: {
		baseUrl: string;
		gbr: IFile[],
		js: IFile[],
		css: IFile[],
		html: IFile[]
	}
}

interface IFile {
	nama: string;
	url: string;
	konten?: string;
	kontenEdit?: string;
	ui?: {
		el?: HTMLDivElement;
		tbl?: HTMLButtonElement;
	}
}

class CError extends Error {
	private _code: Number;
	public get code(): Number {
		return this._code;
	}

	constructor(code: number, msg: string) {
		super(msg);
		this._code = code;
	}
}