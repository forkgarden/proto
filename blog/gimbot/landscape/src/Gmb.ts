let gmb: GUI = {}

gmb.init = () => {
	gmb.kananTbl = document.body.querySelector('button.kanan') as HTMLButtonElement;
	gmb.kiriTbl = document.body.querySelector('button.kiri');
	gmb.aTbl = document.body.querySelector('button.aksi');
	gmb.kanvas = document.body.querySelector('canvas') as HTMLCanvasElement;
	gmb.kanvasCont = document.body.querySelector('div.gimbot div.canvas-cont') as HTMLDivElement;

	gmb.kananTblDitekan = false;
	gmb.kiriTblDitekan = false;
	gmb.aTblDitekan = false;

	window.onkeydown = (e: KeyboardEvent) => {
		e.stopPropagation();
		// console.log(e.code);

		if (e.code == 'ArrowLeft') {
			gmb.kiriTblDitekan = true;
		}
		else if (e.code == 'ArrowRight') {
			gmb.kananTblDitekan = true;
		}
		else if (e.code == 'KeyCode') {
			gmb.aTblDitekan = true;
		}
	}

	gmb.arahTblDitekan = (): boolean => {
		if (gmb.kiriTblDitekan) return true;
		if (gmb.kananTblDitekan) return true;
		return false;
	}

	window.onkeyup = (e: KeyboardEvent) => {
		e.stopPropagation();

		if (e.code == 'ArrowLeft') {
			gmb.kiriTblDitekan = false;
		}
		else if (e.code == 'ArrowRight') {
			gmb.kananTblDitekan = false;
		}
		else if (e.code == 'KeyCode') {
			gmb.aTblDitekan = false;
		}

	}

	gmb.kananTbl.ontouchstart = (e: TouchEvent) => {
		e.stopPropagation();
		gmb.kananTblDitekan = true;
	}

	gmb.kananTbl.ontouchend = (e: TouchEvent) => {
		e.stopPropagation();
		gmb.kananTblDitekan = false;
	}

	gmb.kiriTbl.ontouchstart = (e: TouchEvent) => {
		e.stopPropagation();
		gmb.kiriTblDitekan = true;
	}

	gmb.kiriTbl.ontouchend = (e: TouchEvent) => {
		e.stopPropagation();
		gmb.kiriTblDitekan = false;
	}

	gmb.aTbl.ontouchstart = (e: TouchEvent) => {
		e.stopPropagation();
		gmb.aTblDitekan = true;
	}

	gmb.aTbl.ontouchend = (e: TouchEvent) => {
		e.stopPropagation();
		gmb.aTblDitekan = false;
	}
}


interface GUI {
	kananTbl?: HTMLButtonElement;
	kiriTbl?: HTMLButtonElement;
	aTbl?: HTMLBRElement;
	init?: Function;
	kanvas?: HTMLCanvasElement;
	kiriTblDitekan?: boolean;
	kananTblDitekan?: boolean;
	aTblDitekan?: boolean;
	kanvasCont?: HTMLDivElement;
	arahTblDitekan?: () => boolean;
}