let tdl: ITdl = {
	tambahTbl: null,
	daftarCont: null,
	daftarItem: [],

	init: () => {
		tdl.tambahTbl = document.body.querySelector('div.tdl div.header button.tambah') as HTMLButtonElement;
		tdl.daftarCont = document.body.querySelector('div.tdl div.daftar-cont');

		tdl.tambahTbl.onclick = () => {
			let isi: string = window.prompt('Item Baru:');
			let item: IItem = tdl.item.buat(isi);

			tdl.daftarItem.push(item);

			tdl.item.sembunyiTombol();
			tdl.renderList();
			tdl.db.simpan();
		}

		tdl.db.load();
		tdl.daftarItem.forEach((item: IItem) => {
			tdl.item.updateTampilan(item);
		});
		tdl.renderList();
	},

	template: (query: string): HTMLElement => {
		let template: DocumentFragment = document.body.querySelector('template').content;
		return template.querySelector(query).cloneNode(true) as HTMLElement;
	},

	renderList: (): void => {
		tdl.daftarCont.innerHTML = '';
		for (let i: number = 0; i < tdl.daftarItem.length; i++) {
			tdl.daftarCont.appendChild(tdl.daftarItem[i].el);
		}
	}

}

tdl.item = {
	sembunyiTombol: (): void => {
		if (tdl.item.aktif) {
			tdl.item.aktif.tombolCont.style.display = 'none';
			tdl.item.aktif = null;
		}
	},

	buat: (isi: string): IItem => {
		if (!isi || isi == '') isi = '-kosong-';

		let item: IItem = {
			deskripsi: isi,
			selesai: false
		}

		item.el = tdl.template('div.item');
		item.deskripsiP = item.el.querySelector('p.desk');
		item.tombolCont = item.el.querySelector('div.tombol-cont') as HTMLDivElement;
		item.hapusTbl = item.el.querySelector('button.hapus') as HTMLButtonElement;
		item.editTbl = item.el.querySelector('button.edit') as HTMLButtonElement;
		item.sudahTbl = item.el.querySelector('button.sudah') as HTMLButtonElement;

		item.deskripsiP.innerHTML = isi;

		item.el.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			tdl.item.viewKlik(item);
		}

		item.hapusTbl.onclick = (e) => {
			e.stopPropagation();
			tdl.item.hapusTblKlik(item);
		}

		item.editTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			tdl.item.editTblKlik(item);
		}

		item.sudahTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			tdl.item.sudahTblKlik(item);
		}

		return item;
	},

	viewKlik: (item: IItem) => {
		if (tdl.item.aktif != item) {
			tdl.item.sembunyiTombol();
			item.tombolCont.style.display = 'block';
			if (item.selesai) {
				item.sudahTbl.innerText = 'belum';
			}
			else {
				item.sudahTbl.innerText = 'sudah';
			}
			tdl.item.aktif = item;
		}
		else {
			tdl.item.sembunyiTombol();
			tdl.item.aktif = null;
		}
	},

	hapusTblKlik: (item: IItem) => {
		let ok: boolean = window.confirm('Apakah anda akan menghapus item ini?');
		tdl.item.sembunyiTombol();
		if (ok) {
			for (let i: number = 0; i < tdl.daftarItem.length; i++) {
				if (tdl.daftarItem[i].el == item.el) {
					tdl.daftarItem.splice(i, 1);
					tdl.renderList();
					tdl.db.simpan();
					return;
				}
			}
		}
	},

	editTblKlik: (item: IItem) => {
		let isi: string = window.prompt('Edit deskripsi:', item.deskripsi);
		item.deskripsi = isi;
		item.deskripsiP.innerHTML = item.deskripsi;
		tdl.db.simpan();
		tdl.item.sembunyiTombol();
	},

	sudahTblKlik: (item: IItem) => {
		item.selesai = !item.selesai;
		tdl.item.updateTampilan(item);
		tdl.db.simpan();
		tdl.item.sembunyiTombol();
	},

	updateTampilan: (item: IItem): void => {
		console.log('update tampilan');
		console.log(item);
		if (item.selesai) {
			item.deskripsiP.style.textDecoration = 'line-through';
			item.deskripsiP.style.fontStyle = 'italic';
			item.deskripsiP.style.fontWeight = 'bold';
		}
		else {
			item.deskripsiP.style.textDecoration = 'none';
			item.deskripsiP.style.fontStyle = 'normal';
			item.deskripsiP.style.fontWeight = 'normal';
		}
	}
}

tdl.db = {
	load: (): void => {
		console.log('load');

		let dataStr: string = window.localStorage.getItem('tdl');
		let dataObj: IItem[];

		tdl.daftarItem = [];
		if (dataStr && dataStr != '') {
			dataObj = JSON.parse(dataStr) as IItem[];
			dataObj.forEach((obj: IItem) => {
				let item: IItem = tdl.item.buat(obj.deskripsi);
				item.selesai = obj.selesai;
				tdl.daftarItem.push(item);
			});
		}
	},

	simpan: (): void => {
		let obj: IItem[] = [];

		tdl.daftarItem.forEach((item: IItem) => {
			obj.push({
				deskripsi: item.deskripsi,
				selesai: item.selesai
			});
		})

		window.localStorage.setItem('tdl', JSON.stringify(obj));
	}

}

interface ITdl {
	tambahTbl?: HTMLButtonElement
	daftarCont?: HTMLDivElement
	daftarItem?: IItem[]

	template?: (query: string) => HTMLElement;
	renderList?: () => void;
	init?: () => void;

	db?: {
		simpan?: () => void;
		load?: () => void;
	}

	item?: {
		aktif?: IItem;
		buat?: (desk: string) => IItem;
		updateTampilan?: (item: IItem) => void;
		sembunyiTombol?: () => void;
		sudahTblKlik?: (item: IItem) => void;
		editTblKlik: (item: IItem) => void;
		hapusTblKlik: (item: IItem) => void;
		viewKlik: (item: IItem) => void;
	}
}

interface IItem {
	deskripsi: string;
	selesai?: boolean;

	el?: HTMLElement;
	sudahTbl?: HTMLButtonElement;
	hapusTbl?: HTMLButtonElement;
	editTbl?: HTMLButtonElement;
	deskripsiP?: HTMLParagraphElement;
	tombolCont?: HTMLDivElement;
}

interface IViewItem {
	el: HTMLElement;
	sudahTbl?: HTMLButtonElement;
	hapusTbl?: HTMLButtonElement;
	editTbl?: HTMLButtonElement;
	deskripsiP?: HTMLParagraphElement;
	tombolCont?: HTMLDivElement;
}