import express from "express";
import { sql } from "../Sql";
import { util } from "../Util";
import { config } from "./Config";

class Api {
	readonly router = express.Router();

	// cors(_req: express.Request, resp: express.Response, next: express.NextFunction) {
	// 	console.debug('set cors');
	// 	resp.setHeader('Access-Control-Allow-Origin', '*');
	// 	resp.setHeader('Access-Control-Allow-Headers', 'Content-Type, Accept');
	// 	resp.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
	// 	next();
	// }

	mapRouter(): void {
		// console.debug('api router');

		// app.use("/", this.router);

		this.router.post("/api", (req: express.Request, resp: express.Response) => {
			try {
				if (!config.dev) throw Error('dev');
				// console.debug('api dipanggil');

				sql.query(req.body.api, []).then((h) => {
					// console.debug('set header');
					h;
					// resp.header("Access-Control-Allow-Origin", "*");
					resp.status(200).send(h);
				}).catch((e) => {
					util.respError(resp, e);
				});
			}
			catch (e) {
				util.respError(resp, e);
			}
		});
	}
}

export var api: Api = new Api();

