import express from "express";
import { util } from "../../Util";
import { config } from "../Config";
import { RouterKOns } from "../RouterKons";
import { session } from "../SessionData";
import { bm } from "../SilsilahModule";

export class RelasiCont {

	async renderTambahPasangan(_req: express.Request, resp: express.Response): Promise<void> {
		try {
			let id: number = parseInt(_req.params.id);

			let kunci: string = '-';
			let jmlAbs: number = 0;
			let hal: number = 0;
			let where: string = bm.dao.anggota.where_jkl;
			let select: string = bm.dao.anggota.select_profile;
			let order: string = bm.dao.anggota.order_nama;
			let jkl: string = '';
			let data: any[] = [];

			//parameter bila ada
			if (_req.params.kunci) kunci = _req.params.kunci;
			if (_req.params.hal) hal = parseInt(_req.params.hal);

			//buat relasi bila belum ada
			let anggota: ISlAnggota = (await bm.dao.anggota.lihat(id))[0];
			if (anggota.rel_id == 0) {
				anggota.rel_id = (await bm.dao.rel.baru()).insertId;
				await bm.dao.anggota.updateRel(anggota.id, anggota.rel_id);
			}

			//daftar anggota
			if (anggota.jkl == 'l') {
				jkl = 'p';
			}
			else {
				jkl = 'l';
			}

			if ("-" != kunci) {
				where = " WHERE jkl = ? AND (nama LIKE ? OR nama_lengkap LIKE ?)";
				data = [jkl, '%' + kunci + '%', '%' + kunci + '%'];
			}
			else {
				where = " WHERE jkl = ? ";
				data = [jkl];
			}

			//filter bani
			where += ` AND bani = ? `;
			data.push(session(_req).id);

			let anggotaAr: ISlAnggota[] = await bm.dao.anggota.baca(select, where, hal * config.jmlPerHal, order, data);

			jmlAbs = (await bm.dao.anggota.jmlWhere(
				where,
				data
			)).jumlah;

			// console.debug('');
			// console.debug('relasi cont: render pilih anggota => tambah pasangan');
			// console.debug('anggota id: ' + id);
			// console.debug('rel id: ' + anggota.rel_id);
			// console.debug('hal log: ' + hal);
			// console.debug('jml abs: ' + jmlAbs);
			// console.debug('=====');

			let str: string = bm.render.pilihAnggota.render(
				anggotaAr,
				anggota,
				RouterKOns.p_anggota_id_rel_edit_id,
				RouterKOns.g_anggota_id_pas_tambah_kunci_hal,
				'pilih pasangan:',
				kunci,
				jmlAbs,
				hal);

			resp.status(200).send(str);
		} catch (e) {
			util.respError(resp, e);
		}
	}
}