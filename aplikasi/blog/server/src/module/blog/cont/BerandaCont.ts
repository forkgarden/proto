import express from "express";
import { util } from "../../Util";
import { session } from "../SessionData";
import { bm } from "../SilsilahModule";

export class BerandaCont {

	async renderBerandaId(_req: express.Request, resp: express.Response): Promise<void> {
		try {
			let id: number = parseInt((_req).params.id);
			let anggotaAr: ISlAnggota[] = await bm.dao.anggota.lihat(id);
			let anggota: ISlAnggota = anggotaAr[0]
			let hal: string;

			hal = bm.render.silsilah.render(anggota);

			resp.status(200).send(hal);

		}
		catch (e) {
			util.respError(resp, e);
		}
	}

	async renderBeranda(_req: express.Request, resp: express.Response): Promise<void> {
		try {
			let id: number = session(_req).defId;
			let anggotaAr: ISlAnggota[] = await bm.dao.anggota.lihat(id);
			let anggota: ISlAnggota = anggotaAr[0]
			let hal: string;

			// console.debug('render beranda:');
			// console.debug('id ' + id);
			// console.debug(anggota);
			// console.debug(session(_req).defId);
			// console.debug(session(_req).id);
			// console.debug(session(_req).defId);
			// console.debug(session(_req).statusLogin);

			hal = bm.render.silsilah.render(anggota);

			resp.status(200).send(hal);
		}
		catch (e) {
			util.respError(resp, e);
		}
	}

	async lihatProfileAnggota(_req: express.Request, resp: express.Response): Promise<void> {
		try {
			let id: number = parseInt(_req.params.id);
			let hal: string;
			let anggota: ISlAnggota = await bm.ent.anggota.populate(id);
			await bm.ent.kerabat.muat(anggota);

			hal = bm.render.web.profile.render(anggota);

			resp.status(200).send(hal);
		} catch (e) {
			util.respError(resp, e);
		}
	}


}