import fs from "fs";
import express from "express";
import { util } from "../../Util";
import { v } from "../../Validator";
import { config } from "../Config";
import { RouterKOns } from "../RouterKons";
import { bm } from "../SilsilahModule";
import { session } from "../SessionData";

export class AnggotaCont {

	async renderDaftarAnggota(_req: express.Request, resp: express.Response): Promise<void> {
		try {
			let anggotaAr: ISlAnggota[] = await bm.dao.anggota.cariAnggota('---', 0, session(_req).id);
			let jml: number = (await bm.dao.anggota.jmlCariAnggota('---', session(_req).id)).jumlah;

			let hal: string = bm.render.daftarAnggota.render(anggotaAr, 0, jml, '---', RouterKOns.g_anggota_daftar_kunci_hal);
			resp.status(200).send(hal);
		}
		catch (e) {
			util.respError(resp, e);
		}
	}

	async renderEditBerandaById(_req: express.Request, resp: express.Response): Promise<void> {
		try {

			let id: number = parseInt(_req.params.id);
			let anggotaAr: ISlAnggota[] = await bm.dao.anggota.lihat(id);
			let anggota: ISlAnggota = anggotaAr[0]
			let hal: string;

			//pasangan info
			if (anggota.rel_id > 0) {
				let pasAr: ISlAnggota[] = await bm.dao.anggota.lihatPasangan(anggota.id, anggota.rel_id);
				let pas: ISlAnggota = pasAr[0];
				anggota.pas = pas;
				anggota.rel = (await bm.dao.rel.byId(anggota.rel_id))[0];
				anggota.anak = (await bm.dao.anggota.daftarAnak(anggota.rel_id))
			}
			else {
				anggota.anak = [];
			}

			hal = bm.render.editBeranda.render(anggota);

			resp.status(200).send(hal);
		}
		catch (e) {
			util.respError(resp, e);
		}
	}

	async renderEditProfileAnggota(_req: express.Request, resp: express.Response): Promise<void> {
		try {

			let id: number = parseInt(_req.params.id);
			let anggotaAr: ISlAnggota[] = await bm.dao.anggota.lihat(id);
			let anggota: ISlAnggota = anggotaAr[0];


			let hal: string = bm.render.editProfileAnggota.render(anggota);

			resp.status(200).send(hal);
		}
		catch (e) {
			util.respError(resp, e);
		}
	}

	async renderAnakBaru(_req: express.Request, resp: express.Response): Promise<void> {
		let anggotaAr: ISlAnggota[] = await bm.dao.anggota.cariAnggota("-", 0, session(_req).id);
		let jml: number = (await bm.dao.anggota.jmlCariAnggota("---", session(_req).id)).jumlah
		let anggota: ISlAnggota = (await bm.dao.anggota.lihat(parseInt(_req.params.id)))[0];

		// console.debug('render pilih anggota => anak baru, id sumber: ' + anggota.id + '/ortu id: ' + anggota.rel_id);

		let hal: string = bm.render.pilihAnggota.render(
			anggotaAr,
			anggota,
			RouterKOns.p_anggota_id_ortu_edit_id,
			RouterKOns.g_anggota_id_anak_tambah_kunci_hal,
			'pilih anak',
			"-",
			jml,
			0);

		resp.status(200).send(hal);
	}

	async renderAnakBaruCari(_req: express.Request, resp: express.Response): Promise<void> {
		let id: number = parseInt(_req.params.id);
		let kunci: string = decodeURI(_req.params.kunci);
		let anggota: ISlAnggota = (await bm.dao.anggota.lihat(id))[0];
		let select: string = bm.dao.anggota.select_profile;
		let where: string = bm.dao.anggota.where_semua;
		let order: string = bm.dao.anggota.order_nama;
		let offsetLog: number = parseInt(_req.params.hal);
		let jml: number = (await bm.dao.anggota.jmlCariAnggota(kunci, session(_req).id)).jumlah;
		let kunciSql: string = '%' + kunci + '%';

		if (!kunci || "-" == kunci) {
			where = bm.dao.anggota.where_semua;
		}
		else {
			where = bm.dao.anggota.where_cari;
		}

		//fiter bani
		where += " AND bani = ? "

		let anggotaAr: ISlAnggota[] = await bm.dao.anggota.baca(select, where, offsetLog * config.jmlPerHal, order, [kunciSql, kunciSql, session(_req).id]);

		// console.debug('render pilih anggota => anak baru, id sumber: ' + anggota.id + '/ortu id: ' + anggota.rel_id);

		let hal: string = bm.render.pilihAnggota.render(anggotaAr, anggota, RouterKOns.p_anggota_id_ortu_edit_id, RouterKOns.g_anggota_id_anak_tambah_kunci_hal, 'pilih anak', kunci, jml, offsetLog);

		resp.status(200).send(hal);
	}

	async renderDaftarAnggotaCari(_req: express.Request, resp: express.Response): Promise<void> {
		try {
			let kunci: string = decodeURI(_req.params.kunci);
			let hal: number = parseInt(_req.params.hal);
			let jml: number = (await bm.dao.anggota.jmlCariAnggota(kunci, session(_req).id)).jumlah;
			let offsetAbs: number = hal * config.jmlPerHal;
			let anggotaAr: ISlAnggota[] = await bm.dao.anggota.cariAnggota(kunci, offsetAbs, session(_req).id);

			// console.debug("renderDaftarAnggotaCari:");
			// console.debug(anggotaAr);
			// console.debug("----");

			let str: string = bm.render.daftarAnggota.render(anggotaAr, hal, jml, kunci, RouterKOns.g_anggota_daftar_kunci_hal);

			resp.status(200).send(str);
		}
		catch (e) {
			util.respError(resp, e);
		}
	}


	async renderAnggotaBaru(_req: express.Request, resp: express.Response): Promise<void> {
		try {
			let hal: string = bm.render.anggotaBaru.render();
			resp.status(200).send(hal);
		}
		catch (e) {
			util.respError(resp, e);
		}
	}

	async lihatPasangan(_req: express.Request, resp: express.Response): Promise<void> {
		try {

			let id: number = parseInt(_req.params.id);
			let anggotaAr: ISlAnggota[] = await bm.dao.anggota.lihat(id);
			let anggota: ISlAnggota = anggotaAr[0];
			let pasangan: ISlAnggota[] = [];

			if (anggota.rel_id == 0) {
				pasangan = [];
			}
			else {
				pasangan = (await bm.dao.anggota.lihatPasangan(id, anggota.rel_id));
			}

			resp.status(200).send(JSON.stringify(pasangan));
		}
		catch (e) {
			util.respError(resp, e);
		}
	}


	//POST
	//====

	private async gambarTulisDisk(p: string, data: any): Promise<void> {
		// console.debug('gambar tulis disk ' + p);
		return new Promise((resolve, reject) => {
			fs.writeFile(p, data, (err) => {
				if (err) {
					console.error(err);
					reject(err);
				}
				else {
					resolve();
				}
			});
		})
	}


	async upload(req: express.Request, resp: express.Response): Promise<void> {
		try {
			// console.debug('gambar upload');

			let buf: Buffer;
			let id: number = parseInt(req.params.id);

			let foto: IFoto = {
				gbr_baru: req.body.gbr_baru,
				thumb_baru: req.body.thumb_baru,
				nama_gbr: req.body.nama_gbr,
				nama_thumb: req.body.nama_thumb
			}

			//simpan gbr besar
			buf = Buffer.from(foto.gbr_baru, 'base64');
			await bm.cont.anggota.gambarTulisDisk(util.baseDir + bm.kons.folder_upload + foto.nama_gbr, buf);

			//simpan gambar kecil
			buf = Buffer.from(foto.thumb_baru, 'base64');
			await bm.cont.anggota.gambarTulisDisk(util.baseDir + bm.kons.folder_upload + foto.nama_thumb, buf);

			await bm.dao.anggota.update({
				thumb: bm.kons.folder_download + foto.nama_thumb,
				foto: bm.kons.folder_download + foto.nama_gbr
			}, id);

			resp.status(200).send(JSON.stringify({
				url_thumb: bm.kons.folder_download + foto.nama_thumb,
				url_gbr: bm.kons.folder_download + foto.nama_gbr
			}));

		}
		catch (e) {
			// console.debug('gambar updload error');
			util.respError(resp, e);
		}
	}

	async daftarAnak(_req: express.Request, resp: express.Response): Promise<void> {
		try {
			let id: number = parseInt(_req.params.id);
			let anggota: ISlAnggota = (await bm.dao.anggota.lihat(id))[0];
			let hasil: ISlAnggota[] = [];

			if (anggota.rel_id == 0) {
				hasil = [];
			}
			else {
				hasil = await bm.dao.anggota.daftarAnak(anggota.rel_id);
			}

			resp.status(200).send(JSON.stringify(hasil));
		}
		catch (e) {
			util.respError(resp, e);
		}
	}

	async baru(_req: express.Request, resp: express.Response): Promise<void> {
		try {
			let data: ISlAnggota = {
				nama: v.escape(_req.body.nama),
				nama_lengkap: v.escape(_req.body.nama_lengkap),
				alamat: v.escape(_req.body.alamat),
				fb: v.escape(_req.body.fb),
				wa: v.escape(_req.body.wa),
				instagram: v.escape(_req.body.instagram),
				jkl: v.escape(_req.body.jkl),
				tgl_lahir: v.escape(_req.body.tgl_lahir),
				tgl_meninggal: v.escape(_req.body.tgl_meninggal),
				foto: v.escape(_req.body.foto),
				thumb: v.escape(_req.body.thumb),
				bani: session(_req).id
			};

			let hasil: IHasilQuery = await bm.dao.anggota.baru(data);
			resp.status(200).send(hasil.insertId + '');
		}
		catch (e) {
			util.respError(resp, e);
		}
	}

	async editInfo(_req: express.Request, resp: express.Response): Promise<void> {
		try {

			let data: ISlAnggota = {
				// id: parseInt(_req.params.id),
				nama: v.escape(_req.body.nama),
				nama_lengkap: v.escape(_req.body.nama_lengkap),
				alamat: v.escape(_req.body.alamat),
				fb: v.escape(_req.body.fb),
				wa: v.escape(_req.body.wa),
				instagram: v.escape(_req.body.instagram),
				jkl: v.escape(_req.body.jkl),
				tgl_lahir: v.escape(_req.body.tgl_lahir),
				tgl_meninggal: v.escape(_req.body.tgl_meninggal)
			};

			let id: number = parseInt(_req.params.id);

			//TODO:[ux] validate

			// console.debug("edit anggota info");
			// console.debug(data);

			let hasil: IHasilQuery = await bm.dao.anggota.update(data, id);
			resp.status(200).send(JSON.stringify(hasil));
		}
		catch (e) {
			util.respError(resp, e);
		}
	}

	async editRel(_req: express.Request, resp: express.Response): Promise<void> {
		try {
			let id: number = parseInt(_req.params.id);
			let id2: number = parseInt(_req.params.id2);

			// console.debug('update rel, id: ' + id + '/rel id: ' + id2);

			await bm.dao.anggota.updateRel(id, id2);

			resp.status(200).send('');
		}
		catch (e) {
			util.respError(resp, e);
		}
	}

	async editOrtu(_req: express.Request, resp: express.Response): Promise<void> {
		try {

			let id: number = parseInt(_req.params.id);
			let idOrtu = parseInt(_req.params.id2);

			// console.debug('post, edit ortu ');
			// console.debug('id: ' + id + '/id ortu: ' + idOrtu);

			await bm.dao.anggota.updateOrtu(id, idOrtu);

			resp.status(200).send('');
		}
		catch (e) {
			util.respError(resp, e);
		}

	}

	async hapus(_req: express.Request, resp: express.Response): Promise<void> {
		try {

			await bm.dao.anggota.hapus(parseInt(_req.params.id));
			resp.status(200).send('');
		}
		catch (e) {
			util.respError(resp, e);
		}
	}




}