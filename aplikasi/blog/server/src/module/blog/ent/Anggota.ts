import { bm } from "../SilsilahModule";

export class Anggota {

	async populate(id: number,): Promise<ISlAnggota> {
		let anggotaAr: ISlAnggota[] = await bm.dao.anggota.lihat(id);
		let anggota: ISlAnggota = anggotaAr[0]

		//pasangan info
		if (anggota.rel_id > 0) {
			let pasAr: ISlAnggota[] = await bm.dao.anggota.lihatPasangan(anggota.id, anggota.rel_id);
			let pas: ISlAnggota = pasAr[0];
			anggota.pas = pas;
			anggota.rel = (await bm.dao.rel.byId(anggota.rel_id))[0];
			anggota.anak = (await bm.dao.anggota.daftarAnak(anggota.rel_id))
		}
		else {
			anggota.anak = [];
		}

		return anggota;
	}
}