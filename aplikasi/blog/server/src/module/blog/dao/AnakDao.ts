import { sql } from "../../Sql";
import { config } from "../Config";
import { bm } from "../SilsilahModule";

export class AnakDao {
	async daftarAnakBaru(kunci: string, bani: number, offset: number): Promise<ISlAnggota[]> {
		offset = parseInt(offset + ''); //validate number

		let where: string = '';
		let data: any[];

		if ("-" == kunci) {
			where = ` (nama = ? OR nama_lengkap = ?) AND bani = ?`;
			data = [kunci, kunci, bani];

		}
		else {
			where = ` bani = ? `;
			data = [bani];
		}

		let query: string = `
			SELECT ${bm.dao.anggota.select_profile}
			FROM sl_anggota
			WHERE ${where}
			LIMIT ${config.jmlPerHal}
			OFFSET ${offset}
		`;

		// console.debug("AnggotaDao.baca:");
		// console.debug('query ' + query);

		let hasil: ISlAnggota[] = await sql.query(query,
			data) as ISlAnggota[];

		return hasil;


		return []//todo;
	}
}