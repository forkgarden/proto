import { AnakDao } from "./AnakDao";
import { AnggotaDao } from "./AnggotaDao";
import { AuthSql } from "./AuthSql";
import { RelDao } from "./RelDao";

export class Dao {
	readonly auth: AuthSql = new AuthSql();
	readonly anggota: AnggotaDao = new AnggotaDao();
	readonly rel: RelDao = new RelDao();
	readonly anak: AnakDao = new AnakDao();
}