import express from "express";
import { RouterKOns } from "./RouterKons";
import { bm } from "./SilsilahModule";

export class Router {

	readonly router = express.Router();

	mapRouter(): void {

		this.router.post(RouterKOns.post, null); //TODO:
		this.router.get(RouterKOns.post, null); //TODO:

		this.router.patch(RouterKOns.post_id, null); //TODO:
		this.router.delete(RouterKOns.post_id, null); //TODO:

		//TODO: dihapus
		this.router.get(RouterKOns.g_anggota_daftar, bm.auth.checkAuthGet, bm.cont.anggota.renderDaftarAnggota);
		this.router.get(RouterKOns.g_anggota_daftar_kunci_hal, bm.auth.checkAuthGet, bm.cont.anggota.renderDaftarAnggotaCari);
		this.router.get(RouterKOns.g_anggota_id_info_edit, bm.auth.checkAuthGet, bm.cont.anggota.renderEditProfileAnggota);
		this.router.get(RouterKOns.g_anggota_id_edit_beranda, bm.auth.checkAuthGet, bm.cont.anggota.renderEditBerandaById);
		this.router.get(RouterKOns.gp_anggota_baru, bm.auth.checkAuthGet, bm.cont.anggota.renderAnggotaBaru);
		this.router.get(RouterKOns.g_anggota_id_anak_tambah, bm.auth.checkAuthGet, bm.cont.anggota.renderAnakBaru);
		this.router.get(RouterKOns.g_anggota_id_anak_tambah_kunci_hal, bm.auth.checkAuthGet, bm.cont.anggota.renderAnakBaruCari);

		this.router.get(RouterKOns.g_anggota_id_pas_tambah, bm.auth.checkAuthGet, bm.cont.relasi.renderTambahPasangan);
		this.router.get(RouterKOns.g_anggota_id_pas_tambah_kunci_hal, bm.auth.checkAuthGet, bm.cont.relasi.renderTambahPasangan);

		this.router.get(RouterKOns.gp_auth_login, bm.cont.auth.renderLogin);
		this.router.get(RouterKOns.gp_auth_logout, bm.cont.auth.logout);

		this.router.get("/", bm.auth.checkAuthGet, bm.cont.beranda.renderBeranda);
		this.router.get(RouterKOns.g_beranda_id, bm.auth.checkAuthGet, bm.cont.beranda.renderBerandaId);
		this.router.get(RouterKOns.g_beranda_lihat_id, bm.auth.checkAuthGet, bm.cont.beranda.lihatProfileAnggota);

		this.router.post(RouterKOns.gp_anggota_baru, bm.auth.checkAuthSession, bm.cont.anggota.baru);
		this.router.post(RouterKOns.g_anggota_id_info_edit, bm.auth.checkAuthSession, bm.cont.anggota.editInfo);
		this.router.post(RouterKOns.p_anggota_hapus_id, bm.auth.checkAuthSession, bm.cont.anggota.hapus);
		this.router.post(RouterKOns.p_anggota_id_anak_baca, bm.auth.checkAuthSession, bm.cont.anggota.daftarAnak);
		this.router.post(RouterKOns.p_anggota_id_pas_lihat, bm.auth.checkAuthSession, bm.cont.anggota.lihatPasangan);
		this.router.post(RouterKOns.p_anggota_id_gbr_upload, bm.auth.checkAuthSession, bm.cont.anggota.upload);
		this.router.post(RouterKOns.p_anggota_id_rel_edit_id, bm.auth.checkAuthSession, bm.cont.anggota.editRel);
		this.router.post(RouterKOns.p_anggota_id_ortu_edit_id, bm.auth.checkAuthSession, bm.cont.anggota.editOrtu);

		this.router.post(RouterKOns.gp_auth_login, bm.cont.auth.login);

	}
}