-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2021 at 03:57 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tugas`
--

-- --------------------------------------------------------

--
-- Table structure for table `aksi`
--

CREATE TABLE `aksi` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aksi`
--

INSERT INTO `aksi` (`id`, `nama`) VALUES
(2, 'berkomentar pada'),
(1, 'membuat'),
(3, 'mengupdate');

-- --------------------------------------------------------

--
-- Table structure for table `aktifitas`
--

CREATE TABLE `aktifitas` (
  `id` int(11) NOT NULL,
  `proyek_id` int(11) DEFAULT NULL,
  `anggota_id` int(11) DEFAULT NULL,
  `tugas_id` int(11) DEFAULT NULL,
  `aksi_id` int(11) NOT NULL,
  `petugas_id` int(11) DEFAULT NULL,
  `tgl` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aktifitas`
--

INSERT INTO `aktifitas` (`id`, `proyek_id`, `anggota_id`, `tugas_id`, `aksi_id`, `petugas_id`, `tgl`) VALUES
(77, 0, 1, 56, 1, 1, '2021-07-22 07:57:50'),
(78, 0, 11, 56, 3, 9, '2021-07-22 08:03:34'),
(79, 0, 11, 56, 3, 0, '2021-07-22 08:03:40'),
(80, 0, 11, 56, 2, 0, '2021-07-22 08:03:48'),
(81, 0, 1, 56, 3, NULL, '2021-07-22 08:19:01'),
(82, NULL, 1, 56, 2, 0, '2021-07-24 13:54:43'),
(83, NULL, 1, 57, 1, 1, '2021-07-24 13:56:05'),
(84, NULL, 1, 58, 1, 8, '2021-07-25 07:27:01'),
(85, NULL, 1, 59, 1, 4, '2021-07-25 07:32:10'),
(86, NULL, 1, 60, 1, 1, '2021-07-25 09:37:14'),
(87, NULL, 1, 61, 1, 1, '2021-07-25 09:44:01'),
(88, NULL, 1, 61, 2, 0, '2021-07-25 10:13:52'),
(89, NULL, 1, 61, 2, 0, '2021-07-25 10:19:39'),
(90, NULL, 1, 61, 2, 0, '2021-07-25 10:20:15'),
(91, NULL, 1, 61, 2, 0, '2021-07-25 10:21:28'),
(92, NULL, 1, 61, 2, 0, '2021-07-25 10:22:27');

-- --------------------------------------------------------

--
-- Table structure for table `anggota`
--

CREATE TABLE `anggota` (
  `id` int(11) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `nama` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `filter` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `anggota`
--

INSERT INTO `anggota` (`id`, `user_name`, `nama`, `password`, `filter`) VALUES
(1, 'test', 'test', '098f6bcd4621d373cade4e832627b4f6', ''),
(4, 'mukson', 'mukson', '4a54dad1365e66560e66ab3e2de0d92c', ''),
(5, 'nova', 'nova', '9d835328486639a871c5910a0e6f6e44', ''),
(6, 'aliyah', 'aliyah', '169db135389ce7714f15aa71258b030d', ''),
(7, 'kiki', 'kiki', '4df35da4ce87e89fc84135824ca1b14a', ''),
(8, 'mala', 'mala', '894841efec584ae89b7c224acfce90cf', ''),
(9, 'syahril', 'syahril', 'b32ef54dfa7a6819690e51dafd171ecf', ''),
(10, 'dede', 'dede', '5a57000b2e78f4bc8210990a00710878', ''),
(11, 'test2', 'test2', 'ad0234829205b9033196ba818f7a872b', '');

-- --------------------------------------------------------

--
-- Table structure for table `fr_komentar`
--

CREATE TABLE `fr_komentar` (
  `id` int(11) NOT NULL,
  `isi` varchar(500) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `post_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fr_post`
--

CREATE TABLE `fr_post` (
  `id` int(11) NOT NULL,
  `isi` text NOT NULL,
  `anggota_id` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp(),
  `tgl_update` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id` int(11) NOT NULL,
  `tugas_id` int(11) NOT NULL,
  `anggota_id` int(11) NOT NULL,
  `isi` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`id`, `tugas_id`, `anggota_id`, `isi`, `tanggal`) VALUES
(1, 1, 1, '0', '2021-07-02 20:20:02'),
(2, 1, 1, 'sdfasdfa', '2021-07-02 21:07:46'),
(3, 3, 1, 'komentar test', '2021-07-03 06:34:39'),
(4, 3, 1, 'komentar 2', '2021-07-03 06:35:32'),
(5, 3, 1, 'komentar 3\n', '2021-07-03 06:35:44'),
(6, 4, 1, 'test komentra', '2021-07-03 06:38:51'),
(7, 4, 1, 'test komentar 2', '2021-07-03 06:39:21'),
(8, 14, 1, 'komentar test 123', '2021-07-03 14:14:08'),
(9, 51, 4, 'test komentar', '2021-07-18 03:03:42'),
(10, 51, 10, '', '2021-07-18 03:12:04'),
(11, 51, 1, 'n,.m.', '2021-07-18 07:27:23'),
(12, 55, 1, 'test', '2021-07-19 01:20:41'),
(13, 55, 1, 'dfafdasf', '2021-07-20 10:53:17'),
(14, 56, 11, 'test', '2021-07-22 08:03:48'),
(15, 56, 1, 'test komentar\ntest komentar line 2\ntest komentar line 3\n      test komentar dengan spasi', '2021-07-24 13:54:43'),
(16, 61, 1, 'test komentar 1\ntest komentar 2\n  test komentar 3\n  test komentar &lt;b&gt;bold&lt;/b&gt;', '2021-07-25 10:13:52'),
(17, 61, 1, 'test\ntest\n&lt;b&gt; test &lt;/b&gt;', '2021-07-25 10:19:39'),
(18, 61, 1, 'estdafds\n&lt;b&gt;test&lt;/b&gt;', '2021-07-25 10:20:15'),
(19, 61, 1, 'fdfasfsa\n&lt;b&gt;test&lt;/b&gt;', '2021-07-25 10:21:28'),
(20, 61, 1, 'safaffds<b>bold</b>', '2021-07-25 10:22:27');

-- --------------------------------------------------------

--
-- Table structure for table `mengetahui`
--

CREATE TABLE `mengetahui` (
  `id` int(11) NOT NULL,
  `tugas_id` int(11) NOT NULL,
  `anggota_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id` int(11) NOT NULL,
  `tugas_id` int(11) NOT NULL,
  `anggota_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `proyek`
--

CREATE TABLE `proyek` (
  `id` int(11) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `isi` varchar(500) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp(),
  `tgl_update` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `proyek`
--

INSERT INTO `proyek` (`id`, `judul`, `isi`, `tanggal`, `tgl_update`) VALUES
(0, 'internal/rutinitas edit', 'edit pekerjaan rutin dan pekerjaan internal kantor', '2021-07-23 13:09:26', '2021-07-25 06:19:39'),
(2, 'judul2', 'isi2', '2021-07-25 07:16:38', '2021-07-25 07:16:38');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `nama` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `nama`) VALUES
(1, 'Belum Dikerjakan'),
(3, 'Dibatalkan'),
(2, 'Sedang Dikerjakan'),
(4, 'Selesai');

-- --------------------------------------------------------

--
-- Table structure for table `tugas`
--

CREATE TABLE `tugas` (
  `id` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tgl_update` timestamp NOT NULL DEFAULT current_timestamp(),
  `anggota_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `petugas_id` int(11) NOT NULL,
  `arsip` tinyint(4) NOT NULL DEFAULT 0,
  `proyek_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tugas`
--

INSERT INTO `tugas` (`id`, `judul`, `isi`, `tanggal`, `tgl_update`, `anggota_id`, `status_id`, `petugas_id`, `arsip`, `proyek_id`) VALUES
(56, 'judul-test2', 'is-test23', '2021-07-22 08:19:01', '2021-07-22 07:57:50', 1, 2, 9, 0, 0),
(57, 'judul tugas 3', 'line 1\nbaris 2\nbaris 3\nbaris 4\nbaris 5\nbaris 6\nbaris 7\nbaris 8\nbaris 9', '2021-07-24 13:56:05', '2021-07-24 13:56:05', 1, 1, 1, 0, 0),
(58, 'tugas baru proyek 0', 'tugas baru proyek 0', '2021-07-25 07:27:01', '2021-07-25 07:27:01', 1, 4, 8, 0, 0),
(59, 'tugas baru proyek 2', 'deskripsi tugas', '2021-07-25 07:32:10', '2021-07-25 07:32:10', 1, 4, 4, 0, 0),
(60, 'judul proyek 02', 'deskripsi proyek 02', '2021-07-25 09:37:14', '2021-07-25 09:37:14', 1, 1, 1, 0, 2),
(61, 'proyek 0201', 'deskripsi\ndeskripsi2', '2021-07-25 09:44:01', '2021-07-25 09:44:01', 1, 1, 1, 0, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aksi`
--
ALTER TABLE `aksi`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama` (`nama`);

--
-- Indexes for table `aktifitas`
--
ALTER TABLE `aktifitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama` (`user_name`);

--
-- Indexes for table `fr_komentar`
--
ALTER TABLE `fr_komentar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fr_post`
--
ALTER TABLE `fr_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mengetahui`
--
ALTER TABLE `mengetahui`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `proyek`
--
ALTER TABLE `proyek`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama` (`nama`);

--
-- Indexes for table `tugas`
--
ALTER TABLE `tugas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aksi`
--
ALTER TABLE `aksi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `aktifitas`
--
ALTER TABLE `aktifitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `fr_komentar`
--
ALTER TABLE `fr_komentar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fr_post`
--
ALTER TABLE `fr_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `mengetahui`
--
ALTER TABLE `mengetahui`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `proyek`
--
ALTER TABLE `proyek`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tugas`
--
ALTER TABLE `tugas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
