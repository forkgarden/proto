// import { Kons } from "./Kons.js";
// import { Util } from "./Util.js";

import { Util } from "../comp/Util.js";
import { Kons } from "./Kons.js";

export class AnggotaDao {

	async bacaAnak(anggota: ISlAnggota): Promise<ISlAnggota[]> {

		if (anggota.rel_id == 0) {
			console.debug('tidak ada data pasangan.');
			return [];
		}

		let h: string = await Util.Ajax2('post', Util.getUrl(Kons.anggota_id_anak_baca, [anggota.id]), '');
		return JSON.parse(h) as ISlAnggota[];
	}

	async bacaPasangan(anggota: ISlAnggota): Promise<ISlAnggota[]> {
		console.group('baca pasangan api:');
		let pas: ISlAnggota[] = [];

		// let relAr: ISlRelasi[] = await Util.sql(`
		// 	SELECT * FROM sl_relasi WHERE id = ${anggota.rel_id}`) as ISlRelasi[];
		// let rel: ISlRelasi = relAr[0];

		// if (rel) {
		// 	pas = await Util.sql(`
		// 		SELECT * FROM sl_anggota 
		// 		WHERE rel_id = ${rel.id}
		// 		AND id != ${anggota.id}
		// 	`) as ISlAnggota[];
		// }
		// else {

		// }

		let url: string = Util.getUrl(Kons.anggota_id_pasangan_lihat, [anggota.id]);
		let hasil: string = await Util.Ajax2('post', url, '');
		pas = JSON.parse(hasil) as ISlAnggota[];

		console.log('pasangan');
		console.log(pas);

		console.groupEnd();

		return pas;
	}

	async bacaId(id: number): Promise<ISlAnggota[]> {
		return (await Util.sql(`
            SELECT *
            FROM sl_anggota
            WHERE id = ${id}`));
	}
}
