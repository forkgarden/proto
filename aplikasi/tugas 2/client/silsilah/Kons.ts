export class Kons {

	static readonly auth_login: string = "/sm/auth/login";

	static readonly anggota_daftar: string = "/sm/anggota/daftar";
	static readonly anggota_lihat_id: string = "/sm/anggota/lihat/:id";
	static readonly anggota_edit_id: string = "/sm/anggota/edit/:id";
	static readonly anggota_id_rel_edit_id: string = "/sm/anggota/:id/rel/edit/:id2";
	static readonly anggota_baru: string = "/sm/anggota/baru";
	static readonly anggota_id_ortu_update_id: string = "/sm/anggota/:id/ortu/update/:id2";
	static readonly anggota_hapus_id: string = "/sm/anggota/hapus/:id";
	static readonly anggota_rel_hapus_id: string = "/sm/anggota/rel/hapus/:id";
	static readonly anggota_cari_jkl_hal: string = "/sm/anggota/cari/jkl/:jkl/hal/:hal";
	static readonly anggota_cari_jkl: string = "/sm/anggota/cari/jkl/:jkl";
	static readonly anggota_id_pasangan_tambah_id: string = "sm/anggota/:id/pasangan/tambah/:id2";
	static readonly anggota_id_pasangan_lihat: string = "/sm/anggota/:id/pasangan/lihat";
	static readonly anggota_id_anak_baca: string = "sm/anggota/:id/anak/baca";

	static readonly rel_daftar: string = "/sm/rel/daftar";
	static readonly rel_hapus_id: string = "/sm/rel/hapus/:id";
}