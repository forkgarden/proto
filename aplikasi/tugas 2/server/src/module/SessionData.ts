import express from "express";

//TODO: dihapus di buat local
class SessionData implements ISessionData {
	private _statusLogin: boolean = false;
	private _level: string = '';
	private _lapak: string = '';
	private _id: number = 0;

	public get id(): number {
		return this._id;
	}
	public set id(value: number) {
		this._id = value;
	}

	public get lapak(): string {
		return this._lapak;
	}
	public set lapak(value: string) {
		this._lapak = value;
	}

	public get level(): string {
		return this._level;
	}
	public set level(value: string) {
		this._level = value;
	}
	public get statusLogin(): boolean {
		return this._statusLogin;
	}
	public set statusLogin(value: boolean) {
		this._statusLogin = value;
	}
}

export function session(req: express.Request): ISessionData {
	if (!req.session) {
		req.session = new SessionData();
	}

	return req.session as ISessionData;
}