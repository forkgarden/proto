import  express  from "express";
import { Cont } from "./cont/Cont";
import { Dao } from "./dao/Dao";
// import { Render } from "./render/Render";
import { Router } from "./router/Router";
import { session } from "./SessionData";

class ForumModule {
	readonly router: Router = new Router();
	readonly cont: Cont = new Cont();
	readonly dao: Dao = new Dao();
	// readonly render: Render = new Render();
	readonly session:(req:express.Request) => ISessionData = session;
}

export var fm: ForumModule = new ForumModule();