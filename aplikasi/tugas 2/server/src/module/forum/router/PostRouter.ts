import express from "express";
import { util } from "../../Util";
import { fm } from "../Forum";
import { checkAuthSession } from "../cont/AuthCont";
// import { session } from "../SessionData";

export var postRouter: express.Router = express.Router();

// postRouter.get("/forum/post/baru", checkAuthGet, (_req: express.Request, resp: express.Response) => {
// 	try {
// 		fm.cont.post.renderPostBaru().then((hal: string) => {
// 			resp.status(200).send(hal);
// 		}).catch((e) => {
// 			util.respError(resp, e);
// 		});
// 	}
// 	catch (err) {
// 		util.respError(resp, err);
// 	}
// });

// postRouter.get("/forum/post/edit/:id", checkAuthGet, (_req: express.Request, resp: express.Response) => {
// 	try {
// 		fm.cont.post.renderEditPost(parseInt(_req.params.id), session(_req).id).then((hal:string) => {
// 			resp.status(200).send(hal);
// 		}).catch((e) => {
// 			util.respError(resp,e);
// 		})
// 	}
// 	catch (err) {
// 		util.respError(resp, err);
// 	}
// });


// postRouter.get("/forum/post/lihat/:id", checkAuthGet, (_req: express.Request, resp: express.Response) => {
// 	try {
// 		fm.cont.post.renderPostLihat(parseInt(_req.params.id), fm.session(_req).id).then((hal: string) => {
// 			resp.status(200).send(hal);
// 		}).catch((e) => {
// 			util.respError(resp, e);
// 		});
// 	}
// 	catch (err) {
// 		util.respError(resp, err);
// 	}
// });

/**
 * POST
 */

postRouter.post("/forum/post/baru", checkAuthSession, (_req: express.Request, resp: express.Response) => {
	try {
		let post: IFrPost = {
			id: _req.body.id,
			judul: _req.body.judul,
			isi: _req.body.isi,
			anggota_id: fm.session(_req).id
		}
		fm.cont.post.simpanPostBaru(post).then(() => {
			resp.status(200).send('');
		}).catch((e) => {
			util.respError(resp, e);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});

postRouter.post("/forum/post/edit", checkAuthSession, (_req: express.Request, resp: express.Response) => {
	try {

		let post: IFrPost = {
			judul: _req.body.judul,
			isi: _req.body.isi,
			id: parseInt(_req.body.id),
			anggota_id: parseInt(_req.body.anggota_id)
		}

		fm.cont.post.simpanPostEdit(post, fm.session(_req).id).then(() => {
			resp.status(200).send('');
		}).catch((e) => {
			util.respError(resp, e);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});