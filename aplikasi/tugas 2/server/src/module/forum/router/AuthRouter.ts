import express from "express";
import { util } from "../../Util";
import { fm } from "../Forum";

export var authRouter = express.Router();

authRouter.get("/forum/auth/logout", (req: express.Request, resp: express.Response) => {
	try {
		req.session = null;
		resp.status(200).send('');
	}
	catch (e) {
		util.respError(resp, e);
	}
});

authRouter.post("/forum/auth/login", (req: express.Request, resp: express.Response) => {
	try {
		fm.cont.auth.login(req.body.user_name, req.body.password)
			.then((h: IAnggota) => {
				fm.session(req).id = h.id;
				fm.session(req).statusLogin = true;
				resp.status(200).send(fm.session(req));
			}).catch((e) => {
				req.session = null;
				util.respError(resp, e);
			});
	}
	catch (e) {
		util.respError(resp, e);
	}
});