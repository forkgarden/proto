import express from "express";
import { fm } from "../Forum";
import { authRouter } from "./AuthRouter";
import { komentarRouter } from "./KomentarRouter";
import { postRouter } from "./PostRouter";
import { webRouter } from "./WebRouter";

export class Router {
	readonly web: express.Router = webRouter;
	readonly post: express.Router = postRouter;
	readonly auth: express.Router = authRouter;
	readonly komentar:express.Router = komentarRouter;

	route(app: express.Express):void {
		app.use("/", fm.router.web);
		app.use("/", fm.router.post);
		app.use("/", fm.router.auth);
		app.use("/", fm.router.komentar);
	}
}