import express from "express";
import { util } from "../../Util";
import { checkAuthSession } from "../cont/AuthCont";
import { fm } from "../Forum";

export var komentarRouter: express.Router = express.Router();

komentarRouter.post("/forum/komentar/baru/:postId", checkAuthSession, (_req: express.Request, resp: express.Response) => {
	try {
		fm.cont.komentar.simpanKomentarBaru({
			anggota_id: (fm.session(_req).id),
			isi: _req.body.isi,
			post_id: _req.body.post_id
		}).then(() => {
			resp.status(200).send('');
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});