import { AuthSql } from "./AuthSql";
import { KomentarDao } from "./KomentarDao";
import { PostDao } from "./PostDao";

export class Dao {
	readonly post: PostDao = new PostDao();
	readonly auth: AuthSql = new AuthSql();
	readonly komentar: KomentarDao = new KomentarDao();;
}