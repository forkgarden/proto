import { sql } from "../../Sql";
import { v } from "../../Validator";

export class PostDao {

	async bacaPost(id: number):Promise<IFrPost[]> {
		return await sql.query(`
			SELECT 
			fr_post.id, fr_post.judul, fr_post.isi, fr_post.anggota_id, fr_post.tgl_update,
			anggota.nama  as anggota_nama
			FROM fr_post
			LEFT JOIN anggota ON anggota.id = fr_post.anggota_id
			WHERE fr_post.id = ?
		`,[id]) as IFrPost[]
	}

	async simpanPostEdit(post: IFrPost): Promise<void> {
		await sql.query(`
			UPDATE fr_post
			SET	judul = ?,	isi = ?, tgl_update = NOW()
			WHERE fr_post.id = ?`,
		[
			v.escape(v.checkScriptErr(post.judul)), 
			v.escape(v.checkScriptErr(post.isi)), 
			post.id
		]);
	}

	async simpanPostBaru(post: IFrPost): Promise<void> {
		await sql.query(`
			INSERT 
			INTO fr_post	(judul,	isi, 	anggota_id)
			VALUE			(?,		?,		?	)`,
		[
			v.escape(v.checkScriptErr(post.judul)), 
			v.escape(v.checkScriptErr(post.isi)), 
			post.anggota_id
		]);
	}

	async daftarPost(): Promise<IFrPost[]> {
		return await sql.query(`
			SELECT 
				fr_post.id, fr_post.judul, fr_post.tgl_update,
				anggota.nama  as anggota_nama
			FROM fr_post
			LEFT JOIN anggota ON anggota.id = fr_post.anggota_id
			ORDER BY tgl_update DESC
		`, []) as IFrPost[]
	}
}