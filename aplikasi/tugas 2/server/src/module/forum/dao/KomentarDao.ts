import { sql } from "../../Sql";
import { v } from "../../Validator";

export class KomentarDao {

    async simpan(komentar: IFrKomentar):Promise<void> {

        await sql.query(`
            INSERT INTO fr_komentar
                    (isi,   anggota_id, post_id)
            VALUE   (?,     ?,          ?    )
        `,[
            v.escape((komentar.isi)), 
            komentar.anggota_id, 
            komentar.post_id
        ]);
    }

    async BacaByPostId(id:number):Promise<IFrKomentar[]> {
        return await sql.query(`
            SELECT 
                fr_komentar.isi, fr_komentar.tanggal,
                anggota.nama as anggota_nama
            FROM fr_komentar
            LEFT JOIN anggota ON anggota.id = fr_komentar.anggota_id
            WHERE fr_komentar.post_id = ?
            ORDER BY fr_komentar.tanggal DESC
        `,[id]) as IFrKomentar[];
    }
    
}