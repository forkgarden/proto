interface IFrAnggota {
	id?: number,
	user_name?: string,
	nama?: string,
	password?: string
}

interface IFrPost {
    id?:number,
    judul?:string,
    isi?:string,
    anggota_id?:number,
    tanggal?:string,
    tgl_update?:string
}

interface IFrKomentar {
    id?:number,
    isi?:string,
    tanggal?:string,
    post_id?:number,
    anggota_id?:number,

    anggota_nama?:string,
}