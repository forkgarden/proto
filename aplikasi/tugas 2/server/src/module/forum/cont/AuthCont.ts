import express from "express";
import { fm } from "../Forum";

export class AuthController {

	async login(userName: string, password: string): Promise<IAnggota> {
		let anggotaAr: IAnggota[] = await fm.dao.auth.login(userName, password);
		let anggota: IAnggota;

		if (!anggotaAr) throw Error('Username atau password salah');
		if (anggotaAr.length == 0) throw Error('Username atau password salah');
		anggota = anggotaAr[0];

		return anggota;
	}
}

export var authController: AuthController = new AuthController();

//check auth middle ware
export function checkAuthGet(req: express.Request, resp: express.Response, next: express.NextFunction) {
	if (!fm.session(req).statusLogin) {
		resp.status(401).redirect('/forum/auth/login');
	}
	else {
		next();
	}
}

export function checkAuthSession(req: express.Request, resp: express.Response, next: express.NextFunction) {
	if (!fm.session(req).statusLogin) {
		resp.status(401).send('belum login');
		next();
	}
	else {
		next();
	}
}