import { authController, AuthController } from "./AuthCont";
import { KomentarCont } from "./KomentarCont";
import { PostCont } from "./PostCont";
import { WebCont } from "./WebCont";

export class Cont {
	readonly web: WebCont = new WebCont();
	readonly post: PostCont = new PostCont();
	readonly auth: AuthController = authController;
	readonly komentar: KomentarCont = new KomentarCont();
}