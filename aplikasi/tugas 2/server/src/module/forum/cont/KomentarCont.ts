import { fm } from "../Forum";

export class KomentarCont {
    async simpanKomentarBaru(komentar:IFrKomentar):Promise<void> {
        await fm.dao.komentar.simpan(komentar);
    }
}