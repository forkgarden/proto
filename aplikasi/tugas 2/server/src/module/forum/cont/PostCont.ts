// import { util } from "../../Util";
import { fm } from "../Forum";

export class PostCont {
	async simpanPostEdit(post: IFrPost, anggotaId:number):Promise<void> {

		if (post.anggota_id != anggotaId) {
			throw new Error('Akses ditolak');
		}

		await fm.dao.post.simpanPostEdit(post);
	}

	// async renderEditPost(id: number, anggotaId:number):Promise<string> 
	// {
	// 	let post:IFrPost = (await fm.dao.post.bacaPost(id))[0];

	// 	if (post.anggota_id != anggotaId) throw Error('Akses ditolak');

	// 	return fm.render.postEdit.render(post);
	// }

	// async renderPostLihat(id: number, anggotaId:number):Promise<string> {
	// 	let post:IFrPost = (await fm.dao.post.bacaPost(id))[0];
		
	// 	console.log(post);

	// 	let komentarAr:IFrKomentar[] = await fm.dao.komentar.BacaByPostId(id)

	// 	post.isi = util.renderSpasiEnter(post.isi);

	// 	return fm.render.postBaca.render(post, komentarAr, anggotaId);;
	// }

	async simpanPostBaru(post:IFrPost):Promise<void> {
		return await fm.dao.post.simpanPostBaru(post);
	}

	// async renderPostBaru(): Promise<string> {
	// 	return fm.render.postBaru.renderPostBaru();
	// }

}