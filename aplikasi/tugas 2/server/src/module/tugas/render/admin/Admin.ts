import { HalDaftarAnggota } from "./HalDaftarAnggota";
import { HalAdmin } from "./HalUtama";
import { TestForm } from "./TestForm";

export class Admin {
	readonly daftarAnggota: HalDaftarAnggota = new HalDaftarAnggota();
	readonly halUtama: HalAdmin = new HalAdmin();
	readonly testForm:TestForm = new TestForm();
}