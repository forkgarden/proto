import { util } from "../../../Util";

export class HalDaftarAnggota {
	async render(anggota: IAnggota[]): Promise<string> {
		return `
			<!DOCTYPE html>
			<html lang="id">

			<head>
				<title>disposisi</title>
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<link href='/css/bootstrap.min.css' rel='stylesheet' />
				<link rel='stylesheet' href="/css/css.css?r=${util.randId}" />
				<link rel='stylesheet' href="/css/umum.css?r=${util.randId}" />
			</head>

			<body>
				<div class="cont container">
					<h3>Daftar Anggota</h3>
					<div class="daftar-anggota">
						${this.daftarAnggota(anggota)}
					</div>
				</div>
			</body>

			</html>
		`;
	}

	daftarAnggota(anggota: IAnggota[]): string {
		let hasil: string = '';

		for (let i: number = 0; i < anggota.length; i++) {
			hasil += `
				<div class='item'>
					<span class='nama'>${anggota[i].nama}</span>
					<button class='btn edit btn-primary'>edit</button>
					<button class='btn hapus btn-primary'>hapus</button>
				</div>
			`;
		}

		return hasil;
	}
}