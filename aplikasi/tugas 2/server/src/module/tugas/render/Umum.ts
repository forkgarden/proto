import { util } from "../../Util";

export class Umum {
	daftarTugas(tugas: ITugas[]): string {
		let hasil: string = '';

		tugas.forEach((item: ITugas) => {
			let el: string = `
			<div class='item list-group daftar-tugas' id=${item.id}>
				<a class="list-group-item list-group-item-action" href="/tugas/lihat/${item.id}">
					<div class='tanggal rata-kanan'>${util.dateTimeStamp(item.tgl_update)}</div>
					<p class='judul'>${item.judul}</p>
					<span class='petugas'>Pelaksana: <strong>${item.petugas_nama}</strong></span>

					<div class='status'>
						<span class='status'>Status: <strong>${item.status_nama} </strong></span>
					</div>
				</a>
			</div>
			`.trimStart().trimEnd();
			hasil += el;
		})

		return hasil;
	}


}