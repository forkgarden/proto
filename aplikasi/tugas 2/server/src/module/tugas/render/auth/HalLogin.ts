import { config } from "../../Config";
import { util, Util } from "../../../Util";

export class HalLogin {
	render(): string {
		return `
		<!DOCTYPE html>
		<html lang="id">
		
		<head>
			<title>${config.judul}</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
		
			<link href='/css/bootstrap.min.css' rel='stylesheet' />
			<link href='/css/umum.css' rel='stylesheet' />
			
			<script src="/lib/md5.min.js"></script> 
			<script type="module" src="/js${Util.revisi}/auth/Login.js?r=${util.randId}"></script>
		
		</head>
		
		<body>
			<div class='container hal-login'>
				<div class='form-login'>
					<h2>Form login</h2>
					<form class='form-login' action="" method="post">
		
						<div class="form-group">
							<label for="user-name">User:</label>
							<input type="text" class="form-control user-name" name="user-name" id="user-name" required
								value="auni" />
						</div>
		
						<div class="form-group">
							<label for="password">Deskripsi:</label>
							<input type="password" class="form-control password" name="password" id="password" required md5 
								value="12345" />
						</div>

						<button type="submit" class="btn btn-primary submit">Login</button>
					</form>
				</div>
			</div>
		
		</body>
		
		</html>`.trimStart().trimEnd();
	}
}