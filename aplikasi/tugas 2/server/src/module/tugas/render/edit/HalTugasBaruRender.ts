import { config } from "../../Config";
import { Util, util } from "../../../Util";

export class HalTugasBaruRender {

	constructor() {
		console.log('Hal tugas baru render');
	}

	render(petugas: IAnggota[], status: IStatus[], proyekId: number): string {
		return `
		<!DOCTYPE html>
		<html lang="id">
		
		<head>
			<title>${config.judul}</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">

			<link href='/css/bootstrap.min.css' rel='stylesheet' />
			<link rel='stylesheet' href="/css/css.css?r=${util.randId}" />
			<link rel='stylesheet' href="/css/umum.css?r=${util.randId}" />
			
			<script type="module" src="/js${Util.revisi}/tugas/HalTugasBaru.js?r=${util.randId}"></script>

		</head>

		<body>
		<div class="cont container hal-tugas-baru">

			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="/proyek/lihat/${proyekId}">Lihat Proyek</a></li>
					<li class="breadcrumb-item active" aria-current="page">Tugas Baru</li>
				</ol>
			</nav>
			<br/>

			<form method="post">

				<div class="form-group">
					<label for="judul">Judul:</label>
					<input type="text" class="form-control judul" name="judul" id="judul"
						maxlength="50" placeholder="judul" required />
				</div>

				<div class="form-group">
					<label for="isi">Deskripsi:</label>
					<textarea class="isi form-control" name="isi" id="isi" rows="5"></textarea>
				</div>

				<div class="form-group">
					<label for="petugas">Petugas:</label>
					${this.daftarPetugas(petugas)}
				</div>

				<div class="form-group">
					<label for="petugas">Status:</label>
					${this.renderStatus(status)}
				</div>

				<div class="form-group">
					<input type="hidden" class="proyek-id" value="${proyekId}"/>
				</div>


				<button type='submit' class="btn btn-primary submit">Simpan</button>			
			</form>
		</div>
		</body>
		</html>
		`.trimStart().trimEnd();
	}

	private renderStatus(status: IStatus[]): string {
		let hasil: string = '';
		let checked: string = '';

		for (let i: number = 0; i < status.length; i++) {
			if (i == 0) {
				checked = 'checked'
			}
			else {
				checked = '';
			}

			hasil += `
			<div class="form-check">
				<input class="form-check-input status" type="radio" name="status" id="status${status[i].id}" ${checked} value="${status[i].id}">
				<label class="form-check-label" for="status${status[i].id}">${status[i].nama}</label>
			</div>`;
		}

		return hasil;
	}


	daftarPetugas(petugas: IAnggota[]): string {
		let hasil: string = ``;
		let checked: string = 'checked';

		for (let i: number = 0; i < petugas.length; i++) {
			checked = '';
			if (i == 0) {
				checked = 'checked="checked"';
			}
			hasil += `
			<div class="form-check">
				<input class="form-check-input" type="radio" name="petugas" id="petugas${i}" ${checked} value="${petugas[i].id}">
				<label class="form-check-label" for="petugas${i}">${petugas[i].nama}</label>
			</div>`.trimStart().trimEnd();
		}

		return hasil.trimStart().trimEnd();
	}
}