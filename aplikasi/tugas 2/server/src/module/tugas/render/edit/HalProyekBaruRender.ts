import { config } from "../../Config";
import { Util, util } from "../../../Util";

export class HalProyekBaruRender {

	constructor() {
		console.log('Hal proyek baru render');
	}

	render(): string {
		return `
		<!DOCTYPE html>
		<html lang="id">
		
		<head>
			<title>${config.judul}</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">

			<link href='/css/bootstrap.min.css' rel='stylesheet' />
			<link rel='stylesheet' href="/css/css.css?r=${util.randId}" />
			<link rel='stylesheet' href="/css/umum.css?r=${util.randId}" />
			
			<script type="module" src="/js${Util.revisi}/tugas/HalProyekBaru.js?r=${util.randId}"></script>

		</head>

		<body>
			<div class="cont container hal-proyek-baru">

				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="/proyek/daftar">Daftar Proyek</a></li>
						<li class="breadcrumb-item active" aria-current="page">Proyek Baru</li>
					</ol>
				</nav>

				<br/>

				<form method="post">

					<div class="form-group">
						<label for="judul">Judul:</label>
						<input type="text" class="form-control judul" name="judul" id="judul"
							maxlength="50" placeholder="judul" required />
					</div>

					<div class="form-group">
						<label for="isi">Deskripsi:</label>
						<textarea class="isi form-control" name="isi" id="isi" rows="5"></textarea>
					</div>

					<button type='submit' class="btn btn-primary submit">Simpan</button>
				</form>
				
			</div>
		</body>
		</html>`;
	}
}