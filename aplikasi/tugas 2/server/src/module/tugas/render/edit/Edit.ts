import { HalLihatProyekRender } from "../web/HalLihatProyekRender";
import { HalEditProyekRender } from "./HalEditPRoyekRender";
import { HalEditTugasRender } from "./HalEditTugasRender";
import { HalLihatTugasRender } from "./HalLihatTugasRender";
import { HalProyekBaruRender } from "./HalProyekBaruRender";
import { HalTugasBaruRender } from "./HalTugasBaruRender";

export class Edit {
	readonly editTugas: HalEditTugasRender = new HalEditTugasRender();
	readonly lihatTugas: HalLihatTugasRender = new HalLihatTugasRender();
	readonly tugasBaru: HalTugasBaruRender = new HalTugasBaruRender();
	readonly proyekBaru: HalProyekBaruRender = new HalProyekBaruRender();
	readonly lihatProyek: HalLihatProyekRender = new HalLihatProyekRender();
	readonly editProyek: HalEditProyekRender = new HalEditProyekRender();
}