import { config } from "../../Config";
import { Util, util } from "../../../Util";

export class HalEditTugasRender {

	constructor() {
		console.log('Hal edit tugas render');
	}

	render(tugas: ITugas): string {
		return `
		<!DOCTYPE html>
		<html lang="id">
		<head>
			<title>${config.judul}</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link href='/css/bootstrap.min.css' rel='stylesheet' />
			<link rel='stylesheet' href="/css/css.css?r=${util.randId}" />
			<link rel='stylesheet' href="/css/umum.css?r=${util.randId}" />
			
			<script type="module" src="/js${Util.revisi}/tugas/HalEditTugas.js?r=${util.randId}"></script>

			<script>
				let data = ${JSON.stringify(tugas)}
			</script>
			
		</head>
		<body>
		<div class="cont container edit-tugas">

			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a class='daftar-tugas' href="/tugas/daftar">Daftar Tugas</a></li>
					<li class="breadcrumb-item"><a class='lihat-tugas' href="/tugas/lihat/${tugas.id}">Lihat Tugas</a></li>
					<li class="breadcrumb-item active" aria-current="page">Edit Tugas</li>
				</ol>
			</nav>  
			<br/>

			<form method="post" class="tugas">

				<div class="form-group">
					<label for="judul">Judul:</label>
					<input type="text" class="form-control judul" name="judul" id="judul"
						maxlength="50" placeholder="judul" required value="${tugas.judul}" />
				</div>

				<div class="form-group">
					<label for="isi">Deskripsi:</label>
					<textarea class="form-control isi" name="isi" id="isi" value="" rows="5">${tugas.isi}</textarea>
				</div>

				<div class="form-group">
					<input type="hidden" name="id" id="id" class="id" value="${tugas.id}" readonly>
				</div>

				<button type='submit' class="btn btn-primary submit simpan-tugas">Simpan</button>
			
			</form>
		</div>
		</body>
		</html>
		`.trimStart().trimEnd();
	}

	// daftarStatus(statusSkr: number, status: IStatus[]): string {
	// 	let hasil: string = '';
	// 	let checked: string = '';

	// 	for (let i: number = 0; i < status.length; i++) {
	// 		checked = '';
	// 		if (statusSkr == status[i].id) {
	// 			checked = ' checked="checked" ';
	// 		}
	// 		hasil += `
	// 			<div class="form-check">
	// 				<input class="form-check-input" type="radio" name="status" id="status${i}" ${checked} value="${status[i].id}">
	// 				<label for="status${i}">${status[i].nama}</label>
	// 			</div>`.trimStart().trimEnd();
	// 	}

	// 	return hasil;
	// }

	// daftarPetugas(petugasId: number, petugas: IAnggota[]): string {
	// 	let hasil: string = ``;
	// 	let checked: string = 'checked';

	// 	for (let i: number = 0; i < petugas.length; i++) {
	// 		checked = '';

	// 		if (petugasId == petugas[i].id) {
	// 			checked = 'checked="checked"';
	// 		}

	// 		hasil += `
	// 		<div class="form-check">
	// 			<input class="form-check-input" type="radio" name="petugas" id="petugas${i}" ${checked} value="${petugas[i].id}">
	// 			<label for="petugas${i}">${petugas[i].nama}</label>
	// 		</div>`.trimStart().trimEnd();
	// 	}

	// 	return hasil.trimStart().trimEnd();
	// }
}