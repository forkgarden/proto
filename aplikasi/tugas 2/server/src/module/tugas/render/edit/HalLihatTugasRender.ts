import { config } from "../../Config";
import { Util, util } from "../../../Util";
import { v } from "../../../Validator";

export class HalLihatTugasRender {

	render(tugas: ITugas, komentar: IKomentar[], status: IStatus[], petugasId: number, petugas: IAnggota[]): string {
		return `
		<!DOCTYPE html>
		<html lang="id">
		<head>
			<title>${config.judul}</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">

			<link href='/css/bootstrap.min.css' rel='stylesheet' />
			<link rel='stylesheet' href="/css/css.css?r=${util.randId}" />
			<link rel='stylesheet' href="/css/umum.css?r=${util.randId}" />
			
			<script type="module" src="/js${Util.revisi}/tugas/HalLihatTugas.js?r=${util.randId}"></script>
			
			<script>
				let data = ${JSON.stringify(tugas)}
			</script>

		</head>
		<body>
			<div class="cont container lihat-tugas">

				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a class="daftar-tugas" href="/tugas/daftar">Daftar Tugas</a></li>
						<li class="breadcrumb-item active" aria-current="page">Lihat Tugas</li>
					</ol>
				</nav>
				<br/>
				<form method="post" class="tugas">
					<div class="form-group">
						<label for="judul">Judul:</label>
						<input type="text" class="form-control judul" name="judul" id="judul" readonly value="${v.checkScriptErr(tugas.judul)}" />
					</div>

					<div class="form-group">
						<label for="isi">Deskripsi:</label><br/>
						<textarea class="form-control isi" name="isi" id="isi" rows="5" readonly value="">${v.checkScriptErr(tugas.isi)}</textarea>
					</div>

				</form>

				<div class='tombol edit-cont'>
					${this.renderEditPanel(tugas, status, petugasId, petugas)}
				</div>

				<hr/>

				<!-- FORM KOMENTAR -->
				<div class="komentar cont">
					<span>Komentar</span>
					<form class="komentar">
						<div class="from-group">
							<textarea class="komentar form-control" rows="5" required></textarea>
						</div>
						<div class="from-group">
							<input type="hidden" class="id" readonly value="${tugas.id}"></input>
						</div>
						<br/>
						<button class="kirim-komentar btn btn-primary" type="submit">Kirim</button>
					</form>
				</div>

				<hr/>

				<!-- DAFTAR KOMENTAR -->
				<div class="daftar-komentar">
					${this.renderDaftarKomentar(komentar)}
				</div>
			</div>
		</body>
		</html>`
	}

	private renderEditPanel(tugas: ITugas, status: IStatus[], petugasId: number, anggota: IAnggota[]): string {
		let hasil: string = '';

		//Edit tugas
		hasil += `
			<a href="/tugas/edit/${tugas.id}" class="edit-tugas">
				<button type="button" class="btn btn-primary edit">Edit Detail</button>
			</a>
			<br/>`;

		//Render pelaksana
		hasil += `
			<hr/>
			<div class='ubah-petugas'>
				<form class='ubah-petugas'> 
					<p>Pelaksana:</p>
					${this.renderDaftarPetugas(petugasId, anggota)}
					<input type="hidden" class="tugas-id" value="${tugas.id}" readonly>
					<br/>
					<button type="submit" class="btn btn-primary edit">Update Pelaksana</button>
				</form>
			</div>`;

		//ubah status
		hasil += `
			<hr/>
			<div class='ubah-status'>
				<form class='ubah-status'> 
					<p>Status:</p>
					${this.renderDaftarStatus(status, tugas.status_id)}
					<input type="hidden" class="tugas-id" value="${tugas.id}" readonly>
					<br/>
					<div>
						<button type="submit" class="update-status btn btn-primary">Update Status</button>
					</div>
				</form>
			</div>`;

		return hasil;
	}

	renderDaftarPetugas(petugasId: number, petugas: IAnggota[]): string {
		let hasil: string = ``;
		let checked: string = 'checked';

		for (let i: number = 0; i < petugas.length; i++) {
			checked = '';

			if (petugasId == petugas[i].id) {
				checked = 'checked="checked"';
			}

			hasil += `
			<div class="form-check">
				<input class="form-check-input" type="radio" name="petugas" id="petugas${i}" ${checked} value="${petugas[i].id}">
				<label class="form-check-label" for="petugas${i}">${petugas[i].nama}</label>
			</div>`.trimStart().trimEnd();
		}

		return hasil.trimStart().trimEnd();
	}

	private renderDaftarStatus(status: IStatus[], statusId: number): string {
		let hasil: string = '';

		for (let i: number = 0; i < status.length; i++) {
			let checked: string = '';

			if (status[i].id == statusId) {
				checked = "checked=checked";
			}

			hasil += `
			<div class="form-check">
				<input class="form-check-input status" type="radio" name="status" id="status${i}" ${checked} value="${status[i].id}">
				<label class="form-check-label" for="status${i}">${status[i].nama}</label>
			</div>`;
		}

		return hasil;
	}

	private renderDaftarKomentar(komentar: IKomentar[]): string {
		let hasil: string = '';


		for (let i: number = 0; i < komentar.length; i++) {

			let date: Date = new Date(komentar[i].tanggal);
			let dateStr: string = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();

			hasil += `
				<div class="item item${i}">
					<div class="tanggal">${dateStr}</div>
					<div class="nama">${v.checkScriptErr(komentar[i].anggota_nama)}</div>
					<div class="isi komentar">${v.checkScriptErr(komentar[i].isi)}</div>
				</div>
				<hr/>
			`.trimStart().trimEnd();
		}

		if (hasil == '') {
			hasil = "<div>Belum ada komentar</div>";
		}

		return hasil;
	}
}