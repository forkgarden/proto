import { config } from "../../Config";
import { Util, util } from "../../../Util";

export class HalEditProyekRender {

	constructor() {
		console.log('Hal edit tugas render');
	}

	render(proyek: IProyek): string {
		return `
		<!DOCTYPE html>
		<html lang="id">
		<head>
			<title>${config.judul}</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link href='/css/bootstrap.min.css' rel='stylesheet' />
			<link rel='stylesheet' href="/css/css.css?r=${util.randId}" />
			<link rel='stylesheet' href="/css/umum.css?r=${util.randId}" />
			
			<script type="module" src="/js${Util.revisi}/tugas/HalEditProyek.js?r=${util.randId}"></script>			
		</head>

		<body>
			<div class="cont container edit-proyek">

				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a class='daftar-proyek' href="/proyek/daftar">Daftar Proyek</a></li>
						<li class="breadcrumb-item"><a class='lihat-proyek' href="/proyek/lihat/${proyek.id}">Lihat Proyek</a></li>
						<li class="breadcrumb-item active" aria-current="page">Edit Proyek</li>
					</ol>
				</nav>

				<br/>

				<form method="post" class="proyek">

					<div class="form-group">
						<label for="judul">Judul:</label>
						<input type="text" class="form-control judul" name="judul" id="judul"
							maxlength="50" placeholder="judul" required value="${proyek.judul}" />
					</div>

					<div class="form-group">
						<label for="isi">Deskripsi:</label>
						<textarea class="form-control isi" name="isi" id="isi" value="" rows="5">${proyek.isi}</textarea>
					</div>

					<div class="form-group">
						<input type="hidden" name="id" id="id" class="id" value="${proyek.id}" readonly>
					</div>

					<button type='submit' class="btn btn-primary submit simpan-tugas">Simpan</button>
				
				</form>
			</div>
		</body>

		</html>`;
	}
}