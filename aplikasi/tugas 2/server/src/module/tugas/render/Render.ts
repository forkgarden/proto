import { Admin } from "./admin/Admin";
import { Auth } from "./auth/Auth";
import { HalDepanRender } from "./web/HalDepanRender";
import { Umum } from "./Umum";
import { Web } from "./web/Web";
import { Edit } from "./edit/Edit";

class Render {
	private _halDepan: HalDepanRender;
	readonly umum: Umum = new Umum();
	readonly auth: Auth = new Auth();
	readonly admin: Admin = new Admin();
	readonly web: Web = new Web();
	readonly edit: Edit = new Edit();


	public get halDepan(): HalDepanRender {
		if (!this._halDepan) {
			this._halDepan = new HalDepanRender();
		}
		return this._halDepan;
	}

}

export var render: Render = new Render();