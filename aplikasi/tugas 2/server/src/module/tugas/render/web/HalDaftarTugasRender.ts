import { config } from "../../Config";
import { util } from "../../../Util";
import { render } from "../Render";

export class HalDaftarTugasRender {

	constructor() {
		console.log('hal daftar tugas constructor');
	}

	render(tugas: ITugas[], offsetAbs: number, jumlahAbs: number, filter: IFilter): string {
		console.log('render halaman daftar tugas');

		offsetAbs;
		jumlahAbs;

		return `
		<!DOCTYPE html>
		<html lang="id">

		<head>
			<title>${config.judul}</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link href='/css/bootstrap.min.css' rel='stylesheet' />
			<link rel='stylesheet' href="/css/css.css?r=${util.randId}" />
			<link rel='stylesheet' href="/css/umum.css?r=${util.randId}" />
		</head>

		<body>
		<div class="cont container daftar-tugas">

			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a class="nav-link tab status" href="/">Status</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" href="#">Tugas</a>
				</li>
				<li class="nav-item">
					<a class="nav-link tab proyek" href="/proyek/daftar">Proyek</a>
				</li>
			</ul>

			<div class='filter-cont'>
				<div class='tombol-cont'>
					${this.renderInfoFilterAktif(filter)}
					<a class='filter' href="/tugas/filter/">Filter</a>
				</div>
			</div>

			<div class='hal-cont'>
				<div class='info-cont daftar-tugas'>
					${render.umum.daftarTugas(tugas)}
				</div>
			</div>

			<br/>

			<div class="hal">
			</div>

			<br/>
			<br/>
			<br/>

			<!--<a class="tambah" href='/tugas/baru'>+</a>-->

		</div>
		</body>
		</html>`;
	}

	renderInfoFilterAktif(filter: IFilter): string {
		if (filter && filter.petugas != 0) {
			return '[Filter aktif]';
		}
		else {
			return '';
		}
	}

	renderPilihPetugas(anggota: IAnggota[]): string {
		let hasil: string = '';

		hasil += `
		<div class="form-check">
			<input class="form-check-input" type="radio" name="filter-petugas" id="filter-petugas0" checked value="0">
			<label for="filter0petugas0">semua</label>
		</div>`;

		for (let i: number = 0; i < anggota.length; i++) {
			let nama: string = anggota[i].nama;
			let id: number = anggota[i].id;

			hasil += `
			<div class="form-check">
				<input class="form-check-input" type="radio" name="filter-petugas" id="filter-petugas${i}" value="${id}">
				<label for="filter0petugas0">${nama}</label>
			</div>`;
		}

		hasil += ``;

		return hasil;
	}
}