import { config } from "../../Config";
import { Util, util } from "../../../Util";
import { v } from "../../../Validator";
import { render } from "../Render";

export class HalLihatProyekRender {

	renderDenganTabKomentar(proyek: IProyek, komentar: IKomentar[]): void {
		let str: String = `
		<!-- FORM KOMENTAR -->
		<div class="komentar cont">
			<span>Komentar</span>
			<form class="komentar">
				<div class="from-group">
					<textarea class="komentar form-control" rows="5" required></textarea>
				</div>
				<div class="from-group">
					<input type="hidden" class="id" readonly value="${proyek.id}"></input>
				</div>
				<br/>
				<button class="kirim-komentar btn btn-primary" type="submit">Kirim</button>
			</form>
		</div>`;

		let str2: string = `
			<!-- DAFTAR KOMENTAR -->
			<div class="daftar-komentar">
				${this.renderDaftarKomentar(komentar)}
			</div>
		`;

		str;
		str2;

	}

	renderDenganTabAktifitas(): void {
		//TODO: next
	}

	renderDenganTabDaftarTugas(proyek: IProyek, daftarTugas: ITugas[]): string {
		return `
		<!DOCTYPE html>
		<html lang="id">
		<head>
			<title>${config.judul}</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">

			<link href='/css/bootstrap.min.css' rel='stylesheet' />
			<link rel='stylesheet' href="/css/css.css?r=${util.randId}" />
			<link rel='stylesheet' href="/css/umum.css?r=${util.randId}" />
			
			<script type="module" src="/js${Util.revisi}/tugas/HalLihatProyek.js?r=${util.randId}"></script>
			
			<script>
				let data = ${JSON.stringify(proyek)}
			</script>

		</head>

		<body>
			<div class="cont container lihat-proyek">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a class="daftar-proyek" href="/proyek/daftar">Daftar Proyek</a></li>
						<li class="breadcrumb-item active" aria-current="page">Lihat Proyek</li>
					</ol>
				</nav>

				<br/>

				<form method="post" class="proyek">

					<div class="form-group">
						<label for="judul">Judul:</label>
						<input type="text" class="form-control judul" name="judul" id="judul" readonly value="${v.checkScriptErr(proyek.judul)}" />
					</div>

					<div class="form-group">
						<label for="isi">Deskripsi:</label><br/>
						<textarea class="form-control isi" name="isi" id="isi" readonly rows="5" value="">${v.checkScriptErr(proyek.isi)}</textarea>
					</div>

				</form>

				<div class='tombol edit-cont'>
					<a href="/proyek/edit/${proyek.id}">
						<button class="btn btn-primary edit">Edit Detail</button>
					</a>
				</div>

				<hr/>

				<!-- TAB -->
				<ul class="nav nav-tabs">
					<li class="nav-item">
						<a class="nav-link active" href="#">TUGAS</a>
					</li>
				</ul>

				<div class="tugas cont">
					${render.umum.daftarTugas(daftarTugas)}
				</div>

				<a class="tambah" href='/tugas/baru/${proyek.id}'>tugas<br/>baru</a>				

			</div>
		</body>
		</html>`
	}

	daftarTugas(tugas: ITugas[]): string {
		let hasil: string = '';

		tugas.forEach((item: ITugas) => {
			let el: string = `
			<div class='item list-group daftar-tugas' id=${item.id}>
				<a class="list-group-item list-group-item-action" href="/tugas/lihat/${item.id}">
					<div class='tanggal rata-kanan'>${util.dateTimeStamp(item.tanggal)}</div>
					<p class='judul'>${item.judul}</p>
					<span class='petugas'>Pelaksana: <strong>${item.petugas_nama}</strong></span>

					<div class='status'>
						<span class='status'>Status: <strong>${item.status_nama} </strong></span>
					</div>
				</a>
			</div>
			`.trimStart().trimEnd();
			hasil += el;
		})

		return hasil;
	}

	private renderDaftarKomentar(komentar: IKomentar[]): string {
		let hasil: string = '';


		for (let i: number = 0; i < komentar.length; i++) {

			//TODO: dibikin util
			let date: Date = new Date(komentar[i].tanggal);
			let dateStr: string = date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();

			hasil += `
				<div class="item item${i}">
					<div class="tanggal">${dateStr}</div>
					<div class="nama">${v.checkScriptErr(komentar[i].anggota_nama)}</div>
					<div class="isi">${v.checkScriptErr(komentar[i].isi)}</div>
				</div>
				<hr/>
			`.trimStart().trimEnd();
		}

		if (hasil == '') {
			hasil = "<div>Belum ada komentar</div>";
		}

		return hasil;
	}
}