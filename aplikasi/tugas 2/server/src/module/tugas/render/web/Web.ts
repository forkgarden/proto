import { HalDaftarProyek } from "./HalDaftarProyekRender";
import { HalDaftarTugasRender } from "./HalDaftarTugasRender";
import { HalFilterRender } from "./HalFilterRender";
import { HalLihatProyekRender } from "./HalLihatProyekRender";

export class Web {
	readonly filter: HalFilterRender = new HalFilterRender();
	readonly daftarProyek: HalDaftarProyek = new HalDaftarProyek();
	readonly lihatProyek: HalLihatProyekRender = new HalLihatProyekRender();
	readonly daftarTugas: HalDaftarTugasRender = new HalDaftarTugasRender();
}