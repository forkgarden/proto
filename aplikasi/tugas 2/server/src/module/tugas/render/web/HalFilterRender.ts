import { config } from "../../Config";
import { Util, util } from "../../../Util";

export class HalFilterRender {
	render(petugas: IAnggota[]): string {
		return `
		<!DOCTYPE html>
		<html lang="id">

		<head>
			<title>${config.judul}</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link href='/css/bootstrap.min.css' rel='stylesheet' />
			<link rel='stylesheet' href="/css/css.css?r=${util.randId}" />
			<link rel='stylesheet' href="/css/umum.css?r=${util.randId}" />

			<script type="module" src="/js${Util.revisi}/tugas/HalFilter.js?r=${util.randId}"></script>

		</head>

		<body>
			<div class="cont container daftar-tugas">

				<nav aria-label="breadcrumb">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a class='daftar-tugas' href="/tugas/daftar">Daftar Tugas</a></li>
						<li class="breadcrumb-item active" aria-current="page">Filter</li>
					</ol>
				</nav>  
				<br/>

				<form>
					<div class='petugas'>
						<label>Petugas:</label>
						${this.daftarPetugas(petugas)}					
					</div>
					<button type='submit' class='btn btn-primary'>OK</button>
				</form>

			</div>
		</body>
		</html>`;
	}

	daftarPetugas(petugas: IAnggota[]): string {
		let hasil: string = ``;

		hasil += `
		<div class="form-check">
			<input class="form-check-input" type="radio" name="petugas" id="petugas0" value="0" checked>
			<label for="petugas0">Semua</label>
		</div>`

		for (let i: number = 0; i < petugas.length; i++) {
			hasil += `
			<div class="form-check">
				<input class="form-check-input" type="radio" name="petugas" id="petugas${petugas[i].id}" value="${petugas[i].id}">
				<label for="petugas${petugas[i].id}">${petugas[i].nama}</label>
			</div>`
		}

		return hasil;
	}

}