import { config } from "../../Config";
import { url } from "../../Url";
import { util } from "../../../Util";

export class HalDepanRender {
	render(daftarInfo: IAktifitas[], offsetAbs: number, jumlahAbs: number): string {
		offsetAbs;
		jumlahAbs;

		return `
		<!DOCTYPE html>
		<html lang="id">

		<head>
			<title>${config.judul}</title>

			<meta name="viewport" content="width=device-width, initial-scale=1">
			
			<link href='/css/bootstrap.min.css' rel='stylesheet' />
			<link rel='stylesheet' href="/css/css.css?r=${util.randId}" />
			<link rel='stylesheet' href="/css/umum.css?r=${util.randId}" />
		</head>

		<body>
		<div class="cont container">
			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a class="nav-link active" href="#">Status</a>
				</li>
				<li class="nav-item">
					<a class="ul li nav-link tab daftar-tugas" href="${url.urlDaftarTugas}">Tugas</a>
				</li>
				<li class="nav-item">
					<a class="ul li nav-link tab daftar-proyek" href="${url.urlDaftarProyek}">Proyek</a>
				</li>
				<li class="nav-item">
					<a class="ul li nav-link tab daftar-proyek" href="/forum">Forum</a>
				</li>
			</ul>

			<br/>

			<div class='hal-cont'>
				<div class='info-cont'>
					${this.renderAktifitas(daftarInfo)}
				</div>
			</div>

			<br/>

			<nav aria-label="Page navigation example" style="text-align:center">
				
			</nav>
		</div>
		</body>
		</html>`;
	}

	renderAktifitas(aktifitas: IAktifitas[]): string {
		let hasil: string = '';
		aktifitas.forEach((item: IAktifitas) => {
			hasil += `
				<div class='item list-group'>
					<a class="list-group-item list-group-item-action" href="/tugas/lihat/${item.tugas_id}">
						<span class='anggota'>${item.anggota_nama}</span>
						telah <span class='aksi'>${item.aksi_nama}</span>
						tugas <span class='tugas'>${item.tugas_judul}</span>
					</a>
				</div>`;
		});
		return hasil;
	}

}