import { config } from "../../Config";
import { util } from "../../../Util";

export class HalDaftarProyek {

	constructor() {
		console.log('hal daftar proyek constructor');
	}

	render(proyek: IProyek[], offsetAbs: number, jumlahAbs: number): string {
		offsetAbs;
		jumlahAbs;

		console.log('render halaman daftar proyek');

		return `
		<!DOCTYPE html>
		<html lang="id">

		<head>
			<title>${config.judul}</title>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link href='/css/bootstrap.min.css' rel='stylesheet' />
			<link rel='stylesheet' href="/css/css.css?r=${util.randId}" />
			<link rel='stylesheet' href="/css/umum.css?r=${util.randId}" />
		</head>

		<body>
		<div class="cont container daftar-proyek">

			<ul class="nav nav-tabs">
				<li class="nav-item">
					<a class="nav-link tab status" href="/">Status</a>
				</li>
				<li class="nav-item">
					<a class="nav-link tab tugas" href="/tugas/daftar">Tugas</a>
				</li>
				<li class="nav-item">
					<a class="nav-link active" href="#">Proyek</a>
				</li>
			</ul>

			<div class='hal-cont'>
				<div class='info-cont daftar-proyek'>
					${this.daftarProyek(proyek)}
				</div>
			</div>

			<br/>

			<div class="hal">
			</div>

			<br/>
			<br/>
			<br/>

			<a class="tambah" href='/proyek/baru'>+</a>

		</div>
		</body>
		</html>`;
	}

	private daftarProyek(proyek: IProyek[]): string {
		let hasil: string = '';

		proyek.forEach((item: IProyek) => {
			let el: string = `
			<div class='item list-group daftar-proyek' id=${item.id}>
				<a class="list-group-item list-group-item-action" href="/proyek/lihat/${item.id}">
					<div class='tanggal rata-kanan'>${util.dateTimeStamp(item.tanggal)}</div>
					<p class='judul'>${item.judul}</p>
				</a>
			</div>`
			hasil += el;
		})

		return hasil;
	}
}