interface ITugas {
	id?: number;
	judul?: string;
	isi?: string;
	anggota_id?: number;
	status_id?: number;
	petugas_id?: number;
	tanggal?: string;
	tgl_update?: string;
	arisp?: number;
	proyek_id?: number;

	anggota_nama?: string;
	petugas_nama?: string;
	status_nama?: string;
}

interface IProyek {
	id?: number,
	judul?: string,
	isi?: string,
	tanggal?: string,
	tgl_update?: string
}

interface IAnggota {
	id?: number,
	nama?: string,
	filter?: string,

	user_name?: string,
	password?: string,
	filterObj?: IFilter
}

interface IAksi {
	id?: number;
	nama?: string;
}

interface IAktifitas {
	id?: number,
	anggota_id?: number,
	tugas_id?: number,
	aksi_id?: number
	petugas_id?: number,
	proyek_id?: number,

	anggota_nama?: number,
	tugas_judul?: string,
	aksi_nama?: string,
	mengetahui_id?: string
	mengetahui_nama?: string,
}

interface IKomentar {
	id?: number,
	tugas_id?: number,
	anggota_id?: number,
	isi?: string,
	tanggal?: number,

	anggota_nama?: string
}

interface IMengetahui {
	id?: number,
	tugas_id?: number,
	anggota_id?: number
}

interface IPetugas {
	id?: number,
	tugas_id?: number,
	anggota_id?: number
}

interface IStatus {
	id?: number,
	nama?: string
}

interface IQuery {
	fieldCount?: number,
	affectedRows?: number,
	insertId?: number,
	serverStatus?: number,
	warningCount?: number,
	message?: string,
	protocol41?: boolean,
	changedRows?: number
}

interface ISessionData {
	id: number;
	statusLogin: boolean;
	filter?: IFilter;
}

interface IFilter {
	petugas: number
}