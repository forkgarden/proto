//NOTE: FINAL

export var config = {
	namaToko: 'AuniStore',
	deskripsiToko: '',
	jmlPerHal: 5,
	dev: true,
	judul: 'Manajemen Tugas',
	tokoId: 2,
	website: 'http://warungwa.hagarden.xyz',
	footer: `<H3>Auni Store</H3>Perum Taman Melati Blok FE 07 Bojong Sari - Sawangan - Depok<br/><br/>`
}

export interface ISetting {
	kunci: string,
	deskripsi: string,
	nilai: string
}