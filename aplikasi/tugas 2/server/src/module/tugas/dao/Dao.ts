// import { HalDepanSql } from "./web/HalDepanSql";
// import { HalDaftarTugasSql } from "./web/HalDaftarTugasSql";
import { Umum } from "./Umum";
import { AuthSql } from "./admin/AuthSql";
import { DevSql } from "./admin/DevSql";
import { Table } from "./table/Table";
import { Web } from "./web/Web";

class Dao {
	readonly auth: AuthSql = new AuthSql();
	// readonly daftarTugas: HalDaftarTugasSql = new HalDaftarTugasSql();
	// readonly depan: HalDepanSql = new HalDepanSql();
	readonly dev: DevSql = new DevSql();
	readonly umum: Umum = new Umum();
	readonly table: Table = new Table();
	readonly web: Web = new Web();
}

export var dao: Dao = new Dao();