export class Aksi {
	static readonly MEMBUAT: number = 1;
	static readonly BERKOMENTAR: number = 2;
	static readonly MENGUPDATE: number = 3;
}

export class StatusTbl {
	static readonly BELUM_DIKERJAKAN: number = 1;
	static readonly SEDANG_DIKERJAKAN: number = 2;
	static readonly DIBATALKAN: number = 3;
	static readonly SELESAI: number = 4;
}