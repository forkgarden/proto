import { HalDaftarProyekDao } from "./HalDaftarProyekSql";
import { HalDaftarTugasSql } from "./HalDaftarTugasSql";
import { HalDepanSql } from "./HalDepanSql";

export class Web {
	readonly daftarProyek: HalDaftarProyekDao = new HalDaftarProyekDao();
	readonly daftarTugas: HalDaftarTugasSql = new HalDaftarTugasSql();
	readonly halDepan: HalDepanSql = new HalDepanSql();
}