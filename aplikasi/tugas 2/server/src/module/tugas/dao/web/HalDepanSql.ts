import { config } from "../../Config";
import { sql } from "../../../Sql";

export class HalDepanSql {

	async jumlah(): Promise<IJUmlah[]> {
		return await sql.query(`
			SELECT COUNT(ID) AS jumlah
			FROM aktifitas
		`, []) as IJUmlah[];
	}

	async aktifitas(offset: number): Promise<IAktifitas[]> {
		let hasil: IAktifitas[] = await sql.query(`
			SELECT 
				aktifitas.id, aktifitas.anggota_id, aktifitas.tugas_id, aktifitas.aksi_id, aktifitas.petugas_id,
				anggota.nama AS anggota_nama, 
				tugas.judul AS tugas_judul,
				aksi.nama AS aksi_nama
			FROM aktifitas
			LEFT JOIN tugas ON aktifitas.tugas_id = tugas.id
			LEFT JOIN anggota ON aktifitas.anggota_id = anggota.id
			LEFT JOIN aksi ON aktifitas.aksi_id = aksi.id
			ORDER BY aktifitas.tgl DESC
			LIMIT ?
			OFFSET ?`,
			[config.jmlPerHal, offset]
		) as IAktifitas[];

		return hasil;
	}

}