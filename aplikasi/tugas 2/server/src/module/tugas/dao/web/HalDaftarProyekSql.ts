import { sql } from "../../../Sql";

export class HalDaftarProyekDao {
	/**
	 * baca tugas: id, judul, tgl_update, status_id, petugas_id
	 * untuk tugas di daftar proyek
	 * @param proyekid 
	 * @returns 
	 */
	async lihatDaftarTugasByProyekId(proyekid: number): Promise<ITugas[]> {
		return (await sql.query(`
			SELECT 
				tugas.id, tugas.proyek_id, tugas.judul, tugas.tgl_update, tugas.status_id, tugas.petugas_id,
				anggota.nama as petugas_nama,
				status.nama as status_nama
			FROM tugas
			LEFT JOIN anggota ON anggota.id = petugas_id
			LEFT JOIN status ON status.id = status_id
			WHERE tugas.proyek_id = ? 
		`, [proyekid]) as ITugas[]);
	}

}