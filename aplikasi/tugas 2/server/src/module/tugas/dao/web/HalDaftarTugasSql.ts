import { config } from "../../Config";
import { sql } from "../../../Sql";

export class HalDaftarTugasSql {

	async total(filter: IFilter): Promise<IJUmlah[]> {
		let where: string = ` WHERE 1 = 1 `;

		if (filter && filter.petugas > 0) {
			where += ` AND petugas_id = ${filter.petugas} `;
		}

		let query: string = `
		SELECT COUNT(id) as jumlah
		FROM tugas
		${where} `;

		let hasil: IJUmlah[] = await sql.query(query, []) as IJUmlah[];

		return hasil;
	}

	async daftarTugas(offsetAbs: number, filter: IFilter): Promise<ITugas[]> {

		let where: string = ' WHERE 1=1 ';

		if (filter && filter.petugas > 0) {
			where += `AND petugas_id = ${filter.petugas} `;
		}

		let query: string = `
		SELECT tugas2.*, anggota.nama as petugas_nama 
		FROM (
			SELECT 
				tugas.id, judul, isi, anggota_id, status_id, petugas_id, tugas.tgl_update,
				anggota.nama as anggota_nama, status.nama as status_nama
			FROM tugas
			LEFT JOIN anggota ON anggota.id = anggota_id
			LEFT JOIN status ON status.id = status_id
		) tugas2
		LEFT JOIN anggota ON tugas2.petugas_id = anggota.id  
		${where}
		ORDER BY tugas2.tgl_update DESC
		LIMIT ?
		OFFSET ?`;

		// console.log(query);

		return await sql.query(
			query,
			[config.jmlPerHal, offsetAbs]
		)
	}


}