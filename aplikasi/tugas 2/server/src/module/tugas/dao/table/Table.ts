import { AktifitasDao } from "./AktifitasDao";
import { AnggotaDao } from "./AnggotaDao";
import { KomentarDao } from "./KomentarDao";
import { ProyekDao } from "./ProyekDao";
import { TugasDao } from "./TugasDao";

export class Table {
	readonly aktifitas: AktifitasDao = new AktifitasDao();
	readonly proyek: ProyekDao = new ProyekDao();
	readonly tugas: TugasDao = new TugasDao();
	readonly komentar: KomentarDao = new KomentarDao();
	readonly anggota: AnggotaDao = new AnggotaDao();
}