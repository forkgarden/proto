import { v } from "../../../Validator";
import { sql } from "../../../Sql";

export class TugasDao {
	async lihatPemilikById(id: number): Promise<ITugas[]> {
		return (await sql.query(`
			SELECT anggota_id
			FROM tugas
			WHERE id = ? 
		`, [id]) as ITugas[]);
	}

	async lihatPetugasById(id: number): Promise<ITugas[]> {
		return (await sql.query(`
			SELECT petugas_id
			FROM tugas
			WHERE id = ? 
		`, [id]) as ITugas[]);
	}

	async lihatTugasById(id: number): Promise<ITugas[]> {
		return (await sql.query(`
			SELECT 
				tugas.id, tugas.judul, tugas.isi, tugas.tanggal, tugas.tgl_update, tugas.anggota_id, tugas.status_id, tugas.petugas_id,
				anggota.nama as petugas_nama,
				status.nama as status_nama
			FROM tugas
			LEFT JOIN anggota ON anggota.id = petugas_id
			LEFT JOIN status ON status.id = status_id
			WHERE tugas.id = ? 
		`, [id]) as ITugas[]);
	}


	/**
	 * Cuman simpan judul dan isi
	 * tanggal update belum ada
	 * @param data 
	 */
	async tugasUpdateIsiJudul(data: ITugas): Promise<void> {

		await sql.query(`
			UPDATE tugas SET
				judul = ?,
				isi = ?
			WHERE id = ?`,
			[
				v.checkScriptErr(data.judul),
				v.checkScriptErr(data.isi),
				data.id
			]);
	}

	async tugasUpdatePetugas(tugasId: number, petugasId: number): Promise<void> {
		await sql.query(`
			UPDATE tugas SET
				petugas_id = ?
			WHERE id = ?
		`, [petugasId, tugasId]);
	}

	async tugasUpdateStatus(tugasId: number, statusId: number): Promise<void> {
		await sql.query(`
			UPDATE tugas SET
				status_id = ?
			WHERE id = ?
		`, [statusId, tugasId]);
	}

	async arsip(tugasId: number): Promise<void> {
		await sql.query(`
			UPDATE tugas SET
				arsip = 1
			WHERE id = ?
		`, [tugasId]) as unknown;
	}

	async hapuSemua(): Promise<void> {
		await sql.query(`DELETE FROM tugas WHERE 1=1`, []);
	}

	async tugasBaruSimpan(data: ITugas): Promise<IQuery> {

		let insert: IQuery = await sql.query(`
				INSERT INTO tugas	(judul, isi, anggota_id, status_id, petugas_id, proyek_id)
				VALUE              	(?,     ?,   ?,          ?,         ?,			?)`,
			[
				v.checkScriptErr(data.judul),
				v.checkScriptErr(data.isi),
				data.anggota_id,
				data.status_id,
				data.petugas_id,
				data.proyek_id
			]
		) as unknown;
		return insert;
	}


}