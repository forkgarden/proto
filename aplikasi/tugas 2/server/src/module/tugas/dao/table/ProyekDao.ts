import { config } from "../../Config";
import { v } from "../../../Validator";
import { sql } from "../../../Sql";

export class ProyekDao {
	async total(): Promise<IJUmlah[]> {

		let query: string = `
			SELECT COUNT(id) as jumlah
			FROM proyek`;

		let hasil: IJUmlah[] = await sql.query(query, []) as IJUmlah[];

		return hasil;
	}

	async daftarNamaProyek(offset: number): Promise<IProyek[]> {

		let query: string = `
		SELECT id, judul, tanggal, tgl_update
		FROM proyek
		ORDER BY tgl_update DESC
		LIMIT ?
		OFFSET ?`;
		return await sql.query(
			query,
			[config.jmlPerHal, offset]
		)
	}

	async bacaProyekById(id: number): Promise<IProyek[]> {

		let query: string = `
			SELECT id, judul, isi, tanggal, tgl_update
			FROM proyek
			WHERE id = ?
			ORDER BY tgl_update DESC`;

		return await sql.query(
			query,
			[id]
		)
	}

	async simpanProyekBaru(proyek: IProyek): Promise<void> {
		console.log('simpan proyek baru');

		let query: string = `
			INSERT INTO proyek 	(judul,	isi)
			VALUE 				(?,		?)`;

		await sql.query(
			query,
			[v.checkScriptErr(proyek.judul), v.checkScriptErr(proyek.isi)]
		);

	}

	async simpanEditProyek(id: number, proyek: IProyek): Promise<void> {
		console.log('simpan edit proyek');
		console.log(proyek);

		let query: string = `
			UPDATE proyek SET judul = ?, isi = ?, tgl_update = NOW()
			WHERE id = ?`;
		await sql.query(
			query,
			[v.checkScriptErr(proyek.judul), v.checkScriptErr(proyek.isi), id]
		);
	}


}