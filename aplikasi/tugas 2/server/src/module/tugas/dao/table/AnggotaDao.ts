import { sql } from "../../../Sql";

export class AnggotaDao {
	/**
	 * Daftar id, nama anggota
	 * @returns 
	 */
	async daftarNamaAnggota(): Promise<IAnggota[]> {
		return (await sql.query(`
				SELECT id, nama 
				FROM anggota
			`,[])) as IAnggota[]
	}

	/**
	 * 
	 */
	async simpanFilter(id: number, filter: IFilter): Promise<void> {
		await sql.query(`
			UPDATE TABLE 
			SET filter = ?
			WHERE id = ?
		`, [JSON.stringify(filter), id]);
	}

	async bacaFilter(id: number): Promise<IAnggota[]> {
		return await sql.query(`
			SELECT filter 
			FROM anggota
			WHERE id = ?
		`, [id]) as IAnggota[];
	}

}