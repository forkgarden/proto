import { config } from "../../Config";
import { sql } from "../../../Sql";

export class AktifitasDao {
	async insertAktifitasBaru(aktifitas: IAktifitas): Promise<void> {
		await sql.query(`
			INSERT INTO aktifitas (anggota_id, tugas_id, aksi_id, petugas_id, tgl)
			VALUE                 (?,          ?,        ?,       ?,          NOW())
		`, [
			aktifitas.anggota_id,
			aktifitas.tugas_id,
			aktifitas.aksi_id,
			aktifitas.petugas_id
		]).catch((e) => {
			console.error(e);
		});
	}

	async hapuSemua(): Promise<void> {
		if (!config.dev) throw Error('Akses ditolak');
		await sql.query(`DELETE FROM aktifitas WHERE 1=1`, []);
	}

}