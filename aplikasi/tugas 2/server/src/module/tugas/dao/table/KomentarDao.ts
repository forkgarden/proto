import { v } from "../../../Validator";
import { sql } from "../../../Sql";

export class KomentarDao {
	async daftarkomentarByTugasId(tugasId: number): Promise<IKomentar[]> {
		return await sql.query(`
			SELECT komentar.id, komentar.tugas_id, komentar.anggota_id, komentar.isi, komentar.tanggal,
			anggota.nama as anggota_nama
			FROM komentar
			LEFT JOIN anggota ON komentar.anggota_id = anggota.id
			WHERE komentar.tugas_id = ?
			ORDER BY komentar.tanggal DESC`, [tugasId]) as IKomentar[];
	}

	async simpanKomentarBaru(data: IKomentar): Promise<void> {
		await sql.query(
			`INSERT INTO 
					komentar	(tugas_id, anggota_id, isi)
				VALUE           (?,        ?,          ?)`
			, [
				data.tugas_id,
				data.anggota_id,
				v.checkScriptErr(data.isi)
			]);
	}

}