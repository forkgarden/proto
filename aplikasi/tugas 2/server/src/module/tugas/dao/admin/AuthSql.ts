import { sql } from "../../../Sql";

export class AuthSql {
	async login(userName: string, password: string): Promise<IAnggota[]> {
		let hasil: IAnggota[] = await sql.query(`
			SELECT id, nama, filter
			FROM anggota
			WHERE user_name = ? AND password = ?
		`, [userName, password]) as IAnggota[];

		console.log(userName + '/' + password);
		console.log(hasil);

		return hasil;
	}
}
