import { config } from "../../Config";
import { sql } from "../../../Sql";

export class DevSql {
	async hapusTugasTbl(): Promise<void> {
		if (!config.dev) throw Error('Akses ditolak');

		await sql.query(`
			DELETE FROM tugas WHERE 1
		`, [])
	}
}