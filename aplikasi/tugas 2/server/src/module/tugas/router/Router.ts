import { adminRouter } from "./AdminRouter";
import { authRouter } from "./AuthRouter";
import { halDepanRouter } from "./DepanRouter";
import { proyekRouter } from "./ProyekRouter";
import { tugasRouter } from "./TugasRouter";

export const router = {
	depan: halDepanRouter,
	tugas: tugasRouter,
	admin: adminRouter,
	auth: authRouter,
	proyek: proyekRouter
}