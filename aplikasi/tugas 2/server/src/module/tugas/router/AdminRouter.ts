import express from "express";
import { config } from "../Config";
import { configDB } from "../../ConfigDB";
import { cont } from "../controller/Cont";
import { util } from "../../Util";

export var adminRouter = express.Router();

adminRouter.get("/admin", (req: express.Request, resp: express.Response) => {
	try {
		cont.admin.renderHalAnggota().then().catch();
		resp.status(200).send('');
	}
	catch (e) {
		req.session = null;
		util.respError(resp, e);
	}
});

adminRouter.get("/admin/test/form", (req: express.Request, resp: express.Response) => {
	try {
		resp.status(200).send(cont.admin.renderHalTestForm());
	}
	catch (e) {
		req.session = null;
		util.respError(resp, e);
	}
});



adminRouter.get("/admin/auth/db/reset/:pass", (req: express.Request, resp: express.Response) => {
	try {
		if (!config.dev) throw Error('Akses ditolak');
		if (req.params.pass != configDB.admin.pass) throw Error('Akses ditolak');
		req.session = null;
		cont.admin.hapuSemua(req.params.pass).then(() => {
			req.cookies = null;
			req.session = null;
			resp.cookie = null;
			resp.status(200).send('');
		}).catch((e) => {
			req.session = null;
			util.respError(resp, e);
		})
	}
	catch (e) {
		req.session = null;
		req.cookies = null;
		util.respError(resp, e);
	}
});
