import express, { Router } from "express";
import { checkAuthGet, checkAuthSession } from "../controller/AuthCont";
import { cont } from "../controller/Cont";
import { session } from "../../SessionData";
import { util } from "../../Util";
import { config } from "../Config";

export var halDepanRouter: Router = express.Router();

halDepanRouter.get("/", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		cont.halDepan.renderInfo(0).then((hal: string) => {
			resp.status(200).send(hal);
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});

halDepanRouter.get("/hal/:hal", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		cont.halDepan.renderInfo(parseInt(_req.params.hal) * config.jmlPerHal).then((hal: string) => {
			resp.status(200).send(hal);
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});


/* POST */
//TODO: komentar di taruh di controller sendiri
halDepanRouter.post("/komentar/baru", checkAuthSession, (_req: express.Request, resp: express.Response) => {
	try {

		cont.halDepan.simpanKomentarBaru({
			id: parseInt(_req.body.id),
			isi: (_req.body.isi),
			anggota_id: session(_req).id,
			tugas_id: _req.body.tugas_id
		}).then(() => {
			resp.status(200).send('');
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});



