import express, { Router } from "express";
import { config } from "../Config";
import { checkAuthGet, checkAuthSession } from "../controller/AuthCont";
import { cont } from "../controller/Cont";
import { session } from "../../SessionData";
import { util } from "../../Util";

export var tugasRouter: Router = express.Router();

/**
 * ======
 * TUGAS
 * ======
 */

tugasRouter.get("/tugas/filter", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		cont.tugas.renderFilter().then((hal: string) => {
			resp.status(200).send(hal);
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});

tugasRouter.get("/tugas/daftar", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		cont.halDepan.renderHalDaftarTugas(0, session(_req).filter).then((hal: string) => {
			resp.status(200).send(hal);
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});

tugasRouter.get("/tugas/daftar/:hal", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		cont.halDepan.renderHalDaftarTugas(parseInt(_req.params.hal) * config.jmlPerHal, session(_req).filter).then((hal: string) => {
			resp.status(200).send(hal);
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});

tugasRouter.get("/tugas/baru/:proyekId", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		console.log('tugas baru');
		cont.tugas.renderHalTugasBaru(parseInt(_req.params.proyekId)).then((hal: string) => {
			resp.status(200).send(hal);
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});

// tugasRouter.get("/tugas/baru", checkAuthGet, (_req: express.Request, resp: express.Response) => {
// 	try {
// 		console.log('tugas baru');
// 		cont.tugas.renderHalTugasBaru().then((hal: string) => {
// 			resp.status(200).send(hal);
// 		}).catch((err) => {
// 			util.respError(resp, err);
// 		});
// 	}
// 	catch (err) {
// 		util.respError(resp, err);
// 	}
// });

tugasRouter.get("/tugas/lihat/:id", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		console.log('tugas lihat');
		cont.tugas.renderHalLihatTugas(parseInt(_req.params.id)).then((hal: string) => {
			resp.status(200).send(hal);
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});

tugasRouter.get("/tugas/edit/:id", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		console.log('tugas edit, id: ' + _req.params.id);
		let anggotaId: number = (session(_req).id)
		cont.tugas.renderHalEditTugas(parseInt(_req.params.id), anggotaId).then((hal: string) => {
			resp.status(200).send(hal);
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});


/* API */
tugasRouter.post("/tugas/baru", checkAuthSession, (_req: express.Request, resp: express.Response) => {
	try {

		cont.tugas.simpanTugasBaru({
			judul: (_req.body.judul),
			isi: (_req.body.isi),
			petugas_id: _req.body.petugas_id,
			anggota_id: session(_req).id,
			status_id: _req.body.status_id,
			proyek_id: _req.body.proyek_id
		}).then(() => {
			resp.status(200).send();
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});

tugasRouter.post("/tugas/edit", checkAuthSession, (_req: express.Request, resp: express.Response) => {
	try {

		cont.tugas.simpanTugasUpdateIsiJudul({
			id: parseInt(_req.body.id),
			judul: (_req.body.judul),
			isi: (_req.body.isi),
			petugas_id: _req.body.petugas_id,
			anggota_id: session(_req).id,
			status_id: _req.body.status_id
		}).then(() => {
			resp.status(200).send('');
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});

tugasRouter.post("/tugas/status/update", checkAuthSession, (_req: express.Request, resp: express.Response) => {
	try {

		cont.tugas.simpanTugasUpdateStatus(
			session(_req).id,
			parseInt(_req.body.tugas_id),
			parseInt(_req.body.status_id)
		).then(() => {
			resp.status(200).send('');
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});

tugasRouter.post("/tugas/petugas/update", checkAuthSession, (_req: express.Request, resp: express.Response) => {
	try {

		cont.tugas.simpanTugasUpdatePetugas(
			session(_req).id,
			parseInt(_req.body.tugas_id),
			parseInt(_req.body.petugas_id)
		).then(() => {
			resp.status(200).send('');
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});


tugasRouter.post("/tugas/filter", checkAuthSession, (_req: express.Request, resp: express.Response) => {
	try {
		console.log(_req.body.filter);
		cont.tugas.simpanFilterDB(_req.body.id, _req.body.filter).then(() => {
			resp.status(200).send('');
		}).catch((err) => {
			util.respError(resp, err);
		});
	}
	catch (err) {
		util.respError(resp, err);
	}
});

