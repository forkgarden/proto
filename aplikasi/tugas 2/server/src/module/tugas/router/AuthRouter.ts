import express from "express";
import { cont } from "../controller/Cont";
import { render } from "../render/Render";
import { session } from "../../SessionData";
import { util } from "../../Util";

export var authRouter = express.Router();

authRouter.get("/login", (req: express.Request, resp: express.Response) => {
	try {
		resp.status(200).send(render.auth.login.render());
	}
	catch (e) {
		req.session = null;
		util.respError(resp, e);
	}
});

authRouter.get("/logout", (req: express.Request, resp: express.Response) => {
	try {
		req.session = null;
		resp.status(200).send('');
	}
	catch (e) {
		util.respError(resp, e);
	}
});

authRouter.post("/login", (req: express.Request, resp: express.Response) => {
	try {
		cont.auth.login(req.body.user_name, req.body.password)
			.then((h: IAnggota) => {
				session(req).id = h.id;
				session(req).statusLogin = true;
				session(req).filter = h.filterObj;
				resp.status(200).send(session(req));
			}).catch((e) => {
				req.session = null;
				util.respError(resp, e);
			});
	}
	catch (e) {
		util.respError(resp, e);
	}
});