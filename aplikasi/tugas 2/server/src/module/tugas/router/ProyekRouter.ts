import express, { Router } from "express";
import { config } from "../Config";
import { util } from "../../Util";
import { checkAuthSession, checkAuthGet } from "../controller/AuthCont";
import { cont } from "../controller/Cont";

export var proyekRouter: Router = express.Router();

proyekRouter.get("/proyek/daftar/", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		cont.proyek.renderDaftarProyek(0)
			.then((hal: string) => {
				resp.status(200).send(hal);
			})
			.catch((e) => {
				util.respError(resp, e);
			})
	}
	catch (err) {
		util.respError(resp, err);
	}
});

proyekRouter.get("/proyek/daftar/:hal", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		cont.proyek.renderDaftarProyek(parseInt(_req.params.hal) * config.jmlPerHal)
			.then((hal: string) => {
				resp.status(200).send(hal);
			})
			.catch((e) => {
				util.respError(resp, e);
			})
	}
	catch (err) {
		util.respError(resp, err);
	}
});

proyekRouter.get("/proyek/lihat/:id", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		cont.proyek.renderLihatProyek(parseInt(_req.params.id))
			.then((hal: string) => {
				resp.status(200).send(hal);
			})
			.catch((e) => {
				util.respError(resp, e);
			})
	}
	catch (err) {
		util.respError(resp, err);
	}
});

proyekRouter.get("/proyek/edit/:id", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		cont.proyek.renderEditProyek(parseInt(_req.params.id))
			.then((hal: string) => {
				resp.status(200).send(hal);
			})
			.catch((e) => {
				util.respError(resp, e);
			})
	}
	catch (err) {
		util.respError(resp, err);
	}
});

proyekRouter.get("/proyek/baru", checkAuthGet, (_req: express.Request, resp: express.Response) => {
	try {
		cont.proyek.renderProyekBaru()
			.then((hal: string) => {
				resp.status(200).send(hal);
			})
			.catch((e) => {
				util.respError(resp, e);
			})
	}
	catch (err) {
		util.respError(resp, err);
	}
});


/**
 * POST
 */
proyekRouter.post("/proyek/edit/", checkAuthSession, (_req: express.Request, resp: express.Response) => {
	try {
		cont.proyek.simpanEditProyek({
			id: _req.body.id,
			isi: (_req.body.isi),
			judul: (_req.body.judul)
		}).then(() => {
			resp.status(200).send('');
		}).catch((e) => {
			util.respError(resp, e);
		})
	}
	catch (err) {
		util.respError(resp, err);
	}
});

proyekRouter.post("/proyek/baru/", checkAuthSession, (_req: express.Request, resp: express.Response) => {
	try {
		cont.proyek.simpanProyekBaru({
			isi: (_req.body.isi),
			judul: (_req.body.judul)
		}).then(() => {
			resp.status(200).send('');
		}).catch((e) => {
			util.respError(resp, e);
		})
	}
	catch (err) {
		util.respError(resp, err);
	}
});
