import { dao } from "../dao/Dao";
import { Aksi } from "../dao/Kons";
import { render } from "../render/Render";

export class HalDepanController {

	async renderInfo(offsetAbs: number): Promise<string> {
		let aktifitas: IAktifitas[] = await dao.web.halDepan.aktifitas(offsetAbs);
		let jumlah: IJUmlah[] = await dao.web.halDepan.jumlah();

		return render.halDepan.render(aktifitas, offsetAbs, jumlah[0].jumlah);
	}

	async renderHalDaftarTugas(offsetAbs: number, filter: IFilter): Promise<string> {


		let tugas: ITugas[] = await dao.web.daftarTugas.daftarTugas(offsetAbs, filter);
		let jumlah: IJUmlah[] = await dao.web.daftarTugas.total(filter);

		return render.web.daftarTugas.render(tugas, offsetAbs, jumlah[0].jumlah, filter);
	}

	async simpanKomentarBaru(data: IKomentar): Promise<void> {
		console.log('simpan komentar baru: ');
		console.log(data);

		await dao.table.komentar.simpanKomentarBaru(data);
		await dao.table.aktifitas.insertAktifitasBaru({
			aksi_id: Aksi.BERKOMENTAR,
			anggota_id: data.anggota_id,
			petugas_id: 0,
			tugas_id: data.tugas_id
		})
	}


}