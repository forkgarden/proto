import { config } from "../Config";
import { configDB } from "../../ConfigDB";
import { dao } from "../dao/Dao";
import { render } from "../render/Render";

export class AdminCont {
	async hapuSemua(pass: string): Promise<void> {
		if (pass != configDB.admin.pass) throw Error('Akses ditolak');
		if (!config.dev) throw Error('Akses ditolak');
		await dao.table.tugas.hapuSemua();
		await dao.table.aktifitas.hapuSemua();
	}

	async renderHalAnggota(): Promise<string> {
		let anggota: IAnggota[] = await dao.table.anggota.daftarNamaAnggota();
		return render.admin.daftarAnggota.daftarAnggota(anggota);
	}

	renderHalTestForm(): string {
		return render.admin.testForm.render();
	}
}