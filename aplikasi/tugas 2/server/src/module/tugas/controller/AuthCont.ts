import express from "express";
import { dao } from "../dao/Dao";
import { session } from "../../SessionData";

export class AuthController {

	async login(userName: string, password: string): Promise<IAnggota> {
		let anggotaAr: IAnggota[] = await dao.auth.login(userName, password);
		let anggota: IAnggota;

		if (!anggotaAr) throw Error('Username atau password salah');
		if (anggotaAr.length == 0) throw Error('Username atau password salah');
		anggota = anggotaAr[0];

		try {
			anggota.filterObj = JSON.parse(anggota.filter);
		}
		catch (e) {
			console.error(e);
			anggota.filterObj = {
				petugas: anggota.id
			}
		}


		return anggota;
	}
}

export var authController: AuthController = new AuthController();

//check auth middle ware
export function checkAuthGet(req: express.Request, resp: express.Response, next: express.NextFunction) {
	if (!session(req).statusLogin) {
		resp.status(401).redirect('/auth/login');
	}
	else {
		next();
	}
}

export function checkAuthSession(req: express.Request, resp: express.Response, next: express.NextFunction) {
	if (!session(req).statusLogin) {
		resp.status(401).send('belum login');
		next();
	}
	else {
		next();
	}
}