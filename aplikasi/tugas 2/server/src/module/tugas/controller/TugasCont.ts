import { dao } from "../dao/Dao";
import { Aksi } from "../dao/Kons";
import { render } from "../render/Render";

export class TugasCont {

	async simpanFilterDB(id: number, filter: IFilter): Promise<void> {
		console.log('simpan filter');
		console.log(filter);
		dao.table.anggota.simpanFilter(id, filter);
	}

	/*
	async simpanFilter(s: ISessionData, filter: IFilter): Promise<void> {
		console.log('simpan filter');
		console.log(filter);

		if (!filter) return;

		if (!s.filter) {
			s.filter = {
				petugas: 0
			}
		}
		s.filter.petugas = filter.petugas;
	}
	*/

	async simpanTugasBaru(data: ITugas): Promise<void> {
		let insert: IQuery = await dao.table.tugas.tugasBaruSimpan(data);

		await dao.table.aktifitas.insertAktifitasBaru({
			aksi_id: Aksi.MEMBUAT,
			anggota_id: data.anggota_id,
			petugas_id: data.petugas_id,
			tugas_id: insert.insertId
		});
	}

	async simpanTugasUpdatePetugas(anggotaId: number, tugasId: number, petugasId: number): Promise<void> {
		await dao.table.tugas.tugasUpdatePetugas(tugasId, petugasId);
		await dao.table.aktifitas.insertAktifitasBaru({
			aksi_id: Aksi.MENGUPDATE,
			anggota_id: anggotaId,
			petugas_id: petugasId,
			tugas_id: tugasId
		});
	}

	//TODO: status id belum di set
	async simpanTugasUpdateStatus(anggotaId: number, tugasId: number, statusId: number): Promise<void> {
		await dao.table.tugas.tugasUpdateStatus(tugasId, statusId);
		await dao.table.aktifitas.insertAktifitasBaru({
			aksi_id: Aksi.MENGUPDATE,
			anggota_id: anggotaId,
			petugas_id: 0,
			tugas_id: tugasId
		});
	}

	async simpanTugasUpdateIsiJudul(data: ITugas): Promise<void> {


		//validate pemilik
		let tugas: ITugas = (await dao.table.tugas.lihatPemilikById(data.id))[0];

		if (tugas.anggota_id != data.anggota_id) {
			throw Error('Akses ditolak');
		}

		await dao.table.tugas.tugasUpdateIsiJudul(data);
		await dao.table.aktifitas.insertAktifitasBaru({
			aksi_id: Aksi.MENGUPDATE,
			anggota_id: data.anggota_id,
			petugas_id: data.petugas_id,
			tugas_id: data.id
		});
	}

	async renderFilter(): Promise<string> {
		let petugas: IAnggota[] = await dao.table.anggota.daftarNamaAnggota();
		return render.web.filter.render(petugas);
	}

	async renderHalTugasBaru(proyekId: number): Promise<string> {
		let anggota: IAnggota[] = await dao.table.anggota.daftarNamaAnggota();
		let status: IStatus[] = await dao.umum.daftarStatus();

		return render.edit.tugasBaru.render(anggota, status, proyekId);
	}

	async renderHalEditTugas(tugasId: number, anggotaId: number): Promise<string> {
		let tugas: ITugas;

		tugas = (await dao.table.tugas.lihatTugasById(tugasId))[0];
		if (tugas.anggota_id != anggotaId) {
			console.log("validasi tugas gagal, tugas.anggota_id " + tugas.anggota_id + '/anggotaId' + anggotaId);
			throw Error('Akses ditolak');
		}

		console.log(tugas);

		return render.edit.editTugas.render(tugas);
	}

	async renderHalLihatTugas(tugasId: number): Promise<string> {
		let tugas: ITugas;
		let komentar: IKomentar[];
		let status: IStatus[];
		let petugasId: number;
		let petugas: IAnggota[];


		tugas = (await dao.table.tugas.lihatTugasById(tugasId))[0];
		komentar = await dao.table.komentar.daftarkomentarByTugasId(tugasId);
		status = await dao.umum.daftarStatus();
		petugasId = tugas.petugas_id;
		petugas = await dao.table.anggota.daftarNamaAnggota();

		return render.edit.lihatTugas.render(tugas, komentar, status, petugasId, petugas);
	}


}