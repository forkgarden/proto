import { AdminCont } from "./AdminCont";
import { AuthController, authController } from "./AuthCont";
import { ProyekCont } from "./ProyekCont";
import { TugasCont } from "./TugasCont";
import { HalDepanController } from "./UtamaCont";

class Controller {
	readonly tugas: TugasCont = new TugasCont();
	readonly admin: AdminCont = new AdminCont();
	readonly proyek: ProyekCont = new ProyekCont();
	readonly halDepan: HalDepanController = new HalDepanController();
	readonly auth: AuthController = authController;

	constructor() {
		console.log('Controller constructor');
	}

}

export var cont: Controller = new Controller();