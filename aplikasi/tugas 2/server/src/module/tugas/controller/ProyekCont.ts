import { dao } from "../dao/Dao";
import { render } from "../render/Render";

export class ProyekCont {
	async renderDaftarProyek(offsetAbs: number): Promise<string> {
		let proyek: IProyek[] = await dao.table.proyek.daftarNamaProyek(offsetAbs);
		let jmlAbs: number = (await dao.table.proyek.total())[0].jumlah;
		return render.web.daftarProyek.render(proyek, offsetAbs, jmlAbs);
	}

	async renderLihatProyek(proyekid: number): Promise<string> {
		let proyek: IProyek[] = await dao.table.proyek.bacaProyekById(proyekid);
		let tugas: ITugas[] = await dao.web.daftarProyek.lihatDaftarTugasByProyekId(proyekid);
		return render.web.lihatProyek.renderDenganTabDaftarTugas(proyek[0], tugas);
	}

	async renderEditProyek(proyekId: number): Promise<string> {
		let proyek: IProyek[] = await dao.table.proyek.bacaProyekById(proyekId);
		return render.edit.editProyek.render(proyek[0]);
	}

	async renderProyekBaru(): Promise<string> {
		return render.edit.proyekBaru.render();
	}

	async simpanProyekBaru(proyek: IProyek): Promise<void> {
		return await dao.table.proyek.simpanProyekBaru(proyek);
	}

	async simpanEditProyek(proyek: IProyek): Promise<void> {
		return await dao.table.proyek.simpanEditProyek(proyek.id, proyek);
	}


}