export class Url {
	readonly urlTugasBaru: string = '/tugas/baru';
	readonly urlAuthLogin: string = '/auth/login';
	readonly urlDaftarTugas: string = '/tugas/daftar';
	readonly urlDaftarProyek: string = '/proyek/daftar';
}


export var url: Url = new Url;