SELECT tbl2.*, anggota.nama as mengetahui_nama FROM (
		SELECT 
			tbl1.id, tbl1.anggota_id, tbl1.tugas_id, tbl1.aksi_id, tbl1.anggota_nama, tbl1.tugas_judul, tbl1.aksi_nama,
			mengetahui.id AS mengetahui_id
		FROM (
			SELECT 
				aktifitas.id, aktifitas.anggota_id, aktifitas.tugas_id, aktifitas.aksi_id,
				anggota.nama AS anggota_nama, 
				tugas.judul AS tugas_judul,
				aksi.nama AS aksi_nama
			FROM aktifitas
			LEFT JOIN tugas ON aktifitas.tugas_id = tugas.id
			LEFT JOIN anggota ON aktifitas.anggota_id = anggota.id
			LEFT JOIN aksi ON aktifitas.aksi_id = aksi.id
			ORDER BY aktifitas.tgl DESC
		) tbl1
		LEFT JOIN mengetahui ON tbl1.tugas_id = mengetahui.tugas_id
	) tbl2
	LEFT JOIN anggota ON tbl2.mengetahui_id = anggota.id
	WHERE tbl2.mengetahui_id = 1