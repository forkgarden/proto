class Geser {
	private gbr: HTMLImageElement;
	private kanvas: HTMLCanvasElement;
	private k2d: CanvasRenderingContext2D;
	private pointer: Pointer = new Pointer();
	private kotak: Kotak = new Kotak();
	private gap: Point = new Point();

	constructor() {
		// this.init();
	}

	init(): void {
		this.gbr = document.body.querySelector('div.muat-gbr img.pohon') as HTMLImageElement;
		this.kanvas = document.body.querySelector('canvas') as HTMLCanvasElement;
		this.k2d = this.kanvas.getContext('2d');

		this.kotak.luas.x = 50;
		this.kotak.luas.y = 50;
		this.kotak.pos.x = 0;
		this.kotak.pos.y = 0;

		this.k2d.drawImage(this.gbr, 0, 0);
		this.pointerInit();
	}

	bolehJalan(x: number, y: number): boolean {
		if (x < this.kotak.pos.x) return false;
		if (y < this.kotak.pos.y) return false;

		if ((this.kotak.pos.x + this.kotak.luas.x) < x) return false;
		if ((this.kotak.pos.y + this.kotak.luas.y) < y) return false;

		// console.debug('boleh jalan: kotak x ' + this.kotak.pos.x + '/y ' + this.kotak.pos.y + '/luas x ' + this.kotak.luas.x + '/luas y ' + this.kotak.luas.y);
		// console.debug('pos x ' + x + '/y ' + y)

		return true;
	}

	render(): void {
		this.k2d.clearRect(0, 0, this.kanvas.width, this.kanvas.height);
		this.k2d.drawImage(this.gbr, 0, 0);
		this.k2d.strokeRect(this.kotak.pos.x, this.kotak.pos.y, this.kotak.luas.x, this.kotak.luas.y);
		this.k2d.strokeRect(0, 0, 30, 30);
		// console.debug(this.kotak);
	}

	pointerInit(): void {
		this.kanvas.onpointerdown = (e: MouseEvent) => {
			e.stopPropagation();

			if (this.bolehJalan(e.clientX, e.clientY)) {
				this.pointer.pencet = true;

				this.pointer.awal.x = e.clientX;
				this.pointer.awal.y = e.clientY;
				this.pointer.pencet = true;

				this.kotak.posAwal.x = this.kotak.pos.x;
				this.kotak.posAwal.y = this.kotak.pos.y;
			}

		}

		this.kanvas.onpointermove = (e: MouseEvent) => {
			e.stopPropagation();
			if (this.pointer.pencet) {

				this.pointer.gerak.x = e.clientX;
				this.pointer.gerak.y = e.clientY;

				this.gap.x = this.pointer.gerak.x - this.pointer.awal.x;
				this.gap.y = this.pointer.gerak.y - this.pointer.awal.y;

				this.kotak.pos.x = this.kotak.posAwal.x + this.gap.x;
				this.kotak.pos.y = this.kotak.posAwal.y + this.gap.y;

				// console.debug('move: ' + this.gap.x + '/' + this.gap.y);


				this.render();
			}
		}

		this.kanvas.onpointerup = (e: MouseEvent) => {
			e.stopPropagation();
			this.pointer.pencet = false;
		}

	}
}

class Kotak {
	private _pos: Point = new Point();
	private _posAwal: Point = new Point();
	private _luas: Point = new Point();

	public get pos(): Point {
		return this._pos;
	}
	public set pos(value: Point) {
		this._pos = value;
	}
	public get posAwal(): Point {
		return this._posAwal;
	}
	public set posAwal(value: Point) {
		this._posAwal = value;
	}
	public get luas(): Point {
		return this._luas;
	}
	public set luas(value: Point) {
		this._luas = value;
	}
}

class Pointer {
	private _pencet: boolean = false;
	public get pencet(): boolean {
		return this._pencet;
	}
	public set pencet(value: boolean) {
		this._pencet = value;
	}
	private _awal: Point = new Point();
	public get awal(): Point {
		return this._awal;
	}
	public set awal(value: Point) {
		this._awal = value;
	}
	private _gerak: Point = new Point();
	public get gerak(): Point {
		return this._gerak;
	}
	public set gerak(value: Point) {
		this._gerak = value;
	}
}


class Point {
	private _x: number;
	public get x(): number {
		return this._x;
	}
	public set x(value: number) {
		this._x = value;
	}
	private _y: number;
	public get y(): number {
		return this._y;
	}
	public set y(value: number) {
		this._y = value;
	}
}

var geser: Geser = new Geser();
window.onload = () => {
	geser.init();
}