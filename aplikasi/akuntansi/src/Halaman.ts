import { BaseComponent } from "./BaseComponent.js";

export class Halaman {
	private hals: Array<BaseComponent> = [];
	private _cont: HTMLDivElement;
	public set cont(value: HTMLDivElement) {
		this._cont = value;
	}

	gantiHal(hal: BaseComponent): void {
		this.hals.push(hal);
		hal.attach(this._cont);
	}

	balik(): void {
		let hal: BaseComponent;

		hal = this.hals.pop();
		if (hal) hal.detach();
	}
}