import { BaseComponent } from "./BaseComponent.js";
import { Akun } from "./Akun.js";
import { JournalRow } from "./JournalRow.js";
import { ListView } from "./laporan/lapSemua/ListView.js";

export class Edit extends BaseComponent {

	private okTbl: HTMLButtonElement = null;
	private cancelTbl: HTMLButtonElement = null;
	private desc: HTMLTextAreaElement = null;
	private jml: HTMLInputElement = null;
	private journal: JournalRow = null;
	private tgl: HTMLInputElement = null;

	constructor() {
		super();
	}

	init(): void {
		this._el = Akun.inst.template.formEdit;

		this.okTbl = this.getEl('button.ok') as HTMLButtonElement;
		this.cancelTbl = this.getEl('button.cancel') as HTMLButtonElement;
		this.desc = this.getEl('textarea.deskripsi') as HTMLTextAreaElement;
		this.jml = this.getEl('input.jml') as HTMLInputElement;
		this.tgl = this.getEl('input.tanggal') as HTMLInputElement;

		this.okTbl.onclick = () => {
			this.okClick();
		};

		this.cancelTbl.onclick = () => {
			this.cancelClick();
		}
		this.desc.onfocus = () => {
			this.desc.select();
		}

		this.jml.onfocus = () => {
			this.jml.select();
		}

	}

	cancelClick(): void {
		this.hide();
	}

	okClick(): void {
		let list: ListView = Akun.inst.homePage.lapSemua.listView;
		let date: Date = new Date(this.tgl.value);
		let dateNumber: number = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());

		//update journal
		this.journal.desc = this.desc.value;
		this.journal.jumlah = parseInt(this.jml.value);
		this.journal.date = dateNumber;

		list.update();

		this.detach();
		Akun.inst.homePage.attach(Akun.inst.pageCont);
		Akun.inst.system.simpan();
	}

	edit(journal: JournalRow): void {
		this.journal = journal;
		this.desc.value = journal.desc;
		this.jml.value = journal.jumlah + '';
		this.tgl.value = journal.date2Input();
	}

}