import { Util } from "./Util.js";

export class BaseComponent {
	protected _template: string = '';
	protected _el: HTMLElement;

	build(): void {
		let div: HTMLElement = document.createElement('div');
		let el: HTMLElement;

		div.innerHTML = this._template;

		el = div.firstElementChild as HTMLElement;

		this._el = el;
	}


	getEl(query: string): HTMLElement {
		return Util.getEl(this._el, query);
	}

	detach(): void {
		if (this._el.parentElement) {
			this._el.parentElement.removeChild(this._el);
		}
	}

	destroy(): void {
		this.detach();

		while (this._el.childElementCount > 0) {
			this._el.removeChild(this._el.firstElementChild);
		}
	}

	attach(cont: HTMLDivElement): void {
		cont.appendChild(this.el);
	}

	show(): void {
		this._el.style.display = 'block';
	}

	hide(): void {
		this._el.style.display = 'none';
	}

	public get el(): HTMLElement {
		return this._el;
	}
	public set el(value: HTMLElement) {
		this._el = value;
	}



}