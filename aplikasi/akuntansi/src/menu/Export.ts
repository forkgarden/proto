import { BaseComponent } from "../BaseComponent.js";
import { Akun } from "../Akun.js";

export class Export extends BaseComponent {
	private button: HTMLButtonElement;

	constructor() {
		super();
		this._el = Akun.inst.template.menuItem;
		this.button = this.getEl('button') as HTMLButtonElement;
		this.button.innerText = 'Export';

		this.button.onclick = () => {
			this.onClick();
		}
	}

	onClick(): void {
		Akun.inst.menu.hide();
		Akun.inst.belumSelesai();
	}


}