import { BaseComponent } from "../BaseComponent.js";
import { Akun } from "../Akun.js";
import { Db } from "../Db.js";
import { LapSemua } from "../laporan/lapSemua/LapSemua.js";
import { Menu } from "./Menu.js";
import { Dialog } from "../Dialog.js";

export class Hapus extends BaseComponent {
	private button: HTMLButtonElement;

	constructor() {
		super();
		this._el = Akun.inst.template.menuItem;
		this.button = this.getEl('button') as HTMLButtonElement;
		this.button.innerText = 'Hapus Data';

		this.button.onclick = () => {
			this.onClick();
		}
	}

	onClick(): void {
		let db: Db = Akun.inst.db;
		let lapSemua: LapSemua = Akun.inst.homePage.lapSemua
		let menu: Menu = Akun.inst.menu;
		let dialog: Dialog = Akun.inst.dialog;

		menu.hide();

		dialog.setAsYN();
		dialog.teks = 'Apa Anda yakin akan menghapus data ini?<br/>Ini akan bersifat permanent';
		dialog.show();
		console.log('dialog show');
		console.log(dialog.el.style.display);
		console.log(dialog.el.parentElement);
		dialog.onClick = () => {
			if (dialog.hasil == 1) {
				console.log('hapus data');
				db.removeAll();
				lapSemua.refresh();
				Akun.inst.system.simpan();
			}
		}

	}


}