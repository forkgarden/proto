import { Util } from "../Util.js";
import { Hapus } from "./Hapus.js";
import { Export } from "./Export.js";
import { MenuButton } from "./MenuButton.js";
import { Akun } from "../Akun.js";
import { LapBulanan } from "../laporan/lap-bulanan/LapBulanan.js";
import { LapMingguan } from "../laporan/lap-mingguan/LapMingguan.js";
import { LapTahunan } from "../laporan/lap-tahunan/LapTahunan.js";

export class Menu {

	private menuEL: HTMLDivElement = null;
	private button: HTMLButtonElement;
	private _cont: HTMLDivElement;
	public set cont(value: HTMLDivElement) {
		if (!value) throw new Error();
		console.log('Menu: set cont ' + value);
		this._cont = value;
	}

	constructor() {
		this.menuEL = Util.getEl(document.body, 'div.header div.menu-cont') as HTMLDivElement;
		this.button = Util.getEl(document.body, "div.header > button.menu") as HTMLButtonElement;
	}

	init(): void {
		this.hide();

		this.button.onclick = () => {
			let display: string = this.menuEL.style.display;

			if (display == 'none') {
				this.show();
			}
			else if (display == 'block') {
				this.hide();
			}
			else {
				throw new Error();
			}

		}

		this.menuEL.appendChild((new Hapus()).el);
		this.menuEL.appendChild((new Export).el);

		this.menuEL.appendChild(MenuButton.create("Import", () => {
			Akun.inst.import();
			this.hide();
		}, this).el);

		this.menuEL.appendChild(MenuButton.create("Lap. Tahunan", () => {
			let lap: LapTahunan;

			Akun.inst.contBersih();

			lap = Akun.inst.lapTahunan;
			lap.buatLaporan();
			lap.attach(this._cont);
			lap.render();
			this.hide();

		}, this).el);


		this.menuEL.appendChild(MenuButton.create("Lap. Bulanan", () => {
			let lap: LapBulanan;

			Akun.inst.contBersih();

			lap = Akun.inst.lapBulanan;
			lap.buatLaporan();
			lap.attach(this._cont);
			lap.render();
			this.hide();
		}, this).el);

		this.menuEL.appendChild(MenuButton.create("Lap. Mingguan", () => {
			let lap: LapMingguan;

			Akun.inst.contBersih();

			lap = Akun.inst.lapMinggu;
			lap.buatLaporan();
			lap.attach(this._cont);
			lap.render();
			this.hide();

		}, this).el);


		this.menuEL.appendChild(MenuButton.create("Laporan Harian", () => {
			Akun.inst.belumSelesai();
			this.hide();
		}, this).el);
	}

	hide(): void {
		this.menuEL.style.display = 'none';
	}

	show(): void {
		this.menuEL.style.display = 'block';
	}


}