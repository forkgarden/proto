import { BaseComponent } from "../BaseComponent.js";
import { Akun } from "../Akun.js";
import { Menu } from "./Menu.js";

export class MenuButton extends BaseComponent {
	private button: HTMLButtonElement;

	static create(label:string, f:Function, menu:Menu):MenuButton {
		return new MenuButton(label, f, menu);
	}

	constructor(label:string, f:Function, menu:Menu) {
		super();
		this._el = Akun.inst.template.menuItem;
		this.button = this.getEl('button') as HTMLButtonElement;
		this.button.innerText = label;

		this.button.onclick = () => {
			f();
			menu.hide();
		}
	}
}