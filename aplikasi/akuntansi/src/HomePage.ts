import { BaseComponent } from "./BaseComponent.js";
import { Baru } from "./Baru.js";
import { LapSemua } from "./laporan/lapSemua/LapSemua.js";

export class HomePage extends BaseComponent {
	private _baru: Baru = null;
	private _lapSemua: LapSemua = null;
	private baruContEl: HTMLDivElement = null;
	private lapContEl: HTMLDivElement = null;

	constructor() {
		super();

		this._template = `
			<div class='home-page'>
				<div class='baru-cont'></div>
				<div class="daftar-cont"></div>
			</div>
		`;

		this.build();
		this.baruContEl = this.getEl('div.baru-cont') as HTMLDivElement;
		this.lapContEl = this.getEl('div.daftar-cont') as HTMLDivElement;

		this._baru = new Baru();
		this._lapSemua = new LapSemua();
	}

	init(): void {
		console.log('home page init')
		this._baru.attach(this.baruContEl);
		this._baru.init();

		this._lapSemua.init();
		this._lapSemua.refresh();
		this._lapSemua.attach(this.lapContEl);
	}

	public get baru(): Baru {
		return this._baru;
	}
	public get lapSemua(): LapSemua {
		return this._lapSemua;
	}

}