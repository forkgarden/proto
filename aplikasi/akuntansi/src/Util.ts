export class Util {
	static getEl(parent: HTMLElement, query: string): HTMLElement {
		let el: HTMLElement;

		el = parent.querySelector(query);

		if (!el) {
			console.log(parent);
			throw new Error(query);
		}

		return el;
	}

	static tambahTitik(str: string): string {
		let res: string = '';
		let ctr: number = 0;

		for (let i: number = str.length - 1; i >= 0; i--) {
			let char: string;

			char = str.slice(i, i + 1);
			res = char + res;

			ctr++;

			if (ctr % 3 == 0) {
				res = "." + res;
			}
		}

		if (res.slice(0, 1) == ".") {
			res = res.slice(1, res.length);
		}
		return res;
	}

	static debug(): void {
		try {
			throw new Error();
		}
		catch (e) {
			console.log(e);
		}
	}
}