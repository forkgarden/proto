import { Db } from "./Db";

export class System {
	private _db: Db;
	public set db(value: Db) {
		this._db = value;
	}

	load(): void {
		let dataStr: string;

		dataStr = window.localStorage.getItem('data');
		if (dataStr) {
			this._db.fromString(dataStr);
		}
		else {
			console.log('data not availble');
		}
	}

	simpan(): void {
		window.localStorage.setItem('data', this._db.toString());
		console.log(this._db.toString());
	}
}