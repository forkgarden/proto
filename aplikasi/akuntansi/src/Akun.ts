import { Db } from "./Db.js";
import { Util } from "./Util.js";
import { Template } from "./Template.js";
import { Edit } from "./Edit.js";
import { Menu } from "./menu/Menu.js";
import { Dialog } from "./Dialog.js";
import { LapMingguan } from "./laporan/lap-mingguan/LapMingguan.js";
import { HomePage } from "./HomePage.js";
import { LapBulanan } from "./laporan/lap-bulanan/LapBulanan.js";
import { System } from "./System.js";
import { LapTahunan } from "./laporan/lap-tahunan/LapTahunan.js";
import { Baru } from "./Baru.js";

export class Akun {
	private _db: Db = null;
	private _system: System = null;
	private _template: Template = null;
	private _edit: Edit = null;
	private _menu: Menu = null;
	private _mainCont: HTMLDivElement = null;
	private _pageCont: HTMLDivElement = null;
	private _dialog: Dialog = null;
	private _homePage: HomePage = null;
	private _lapMinggu: LapMingguan = null;
	private _lapTahunan: LapTahunan = null;
	private _lapBulanan: LapBulanan = null;
	private _baru: Baru = null;

	private static _inst: Akun;

	constructor() {
		window.onload = () => {
			console.log('Akun constructor');
			Akun._inst = this;

			this._db = new Db();
			this._edit = new Edit();
			this._template = new Template();
			this._menu = new Menu();
			this._lapMinggu = new LapMingguan();
			this._lapBulanan = new LapBulanan();
			this._lapTahunan = new LapTahunan();
			this._homePage = new HomePage();
			this._mainCont = Util.getEl(document.body, 'div.cont') as HTMLDivElement;
			this._pageCont = Util.getEl(document.body, 'div.cont div.page-cont') as HTMLDivElement;

			this._baru = new Baru();

			this._system = new System();
			this._system.db = this._db;

			this._dialog = new Dialog();

			console.log('app cont:');
			console.log(this._mainCont);

			this.init();
		}
	}

	private init(): void {
		console.log('akun init');

		this._edit.init();
		this._baru.init();

		this._system.load();
		this._homePage.init();
		this._homePage.attach(this._pageCont)

		this._menu.cont = this._mainCont;
		this._menu.init();

		this._lapBulanan.db = this._db;
		this._lapMinggu.db = this._db;
		this._lapTahunan.db = this._db;

		this._dialog.attach(document.body as HTMLDivElement);
		this._dialog.hide();

		this.debug();
	}

	contBersih(): void {
		while (this._mainCont.firstElementChild) {
			this._mainCont.removeChild(this._mainCont.firstElementChild);
		}
	}

	pageContBersih(): void {
		while (this._pageCont.firstChild) {
			this._pageCont.removeChild(this._pageCont.firstChild);
		}
	}

	belumSelesai(): void {
		this._dialog.setAsOK();
		this._dialog.teks = 'Fitur ini masih dalam pengembangan';
		this._dialog.show();
	}

	import() {
		console.log('import');
		this.belumSelesai();
	}

	test(): void {
		let date: Date = new Date();
		let time: number = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
		let date2: Date = new Date(time);

		console.log(date);
		console.log(time);
		console.log(date2);
	}

	debug(): void {
	}

	public get db(): Db {
		return this._db;
	}

	public get lapBulanan(): LapBulanan {
		return this._lapBulanan;
	}

	public static get inst(): Akun {
		return Akun._inst;
	}

	public get template(): Template {
		return this._template;
	}

	public get edit(): Edit {
		return this._edit;
	}

	public get menu(): Menu {
		return this._menu;
	}

	public get dialog(): Dialog {
		return this._dialog;
	}

	public get lapMinggu(): LapMingguan {
		return this._lapMinggu;
	}

	public get homePage(): HomePage {
		return this._homePage;
	}
	public get mainCont(): HTMLDivElement {
		return this._mainCont;
	}
	public get system(): System {
		return this._system;
	}
	public get lapTahunan(): LapTahunan {
		return this._lapTahunan;
	}

	public get pageCont(): HTMLDivElement {
		return this._pageCont;
	}
	public set pageCont(value: HTMLDivElement) {
		this._pageCont = value;
	}



}

new Akun();