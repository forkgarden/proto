import { MingguUtil } from "./MingguUtil.js";

export class JournalRow {
	private _date: number = 0;
	private _desc: string = '';
	private _jumlah: number = 0;
	private mingguUtil: MingguUtil;

	private _tahun: number = 0;
	private _bulan: number = 0;
	private _minggu: number = 0;
	private _tanggal: number = 0;

	constructor() {
		let date: Date = new Date();
		this._date = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
		this.mingguUtil = new MingguUtil();
		this.pecahTanggal();
	}

	padding(str: string): string {
		str = "00" + str;
		return str.slice(str.length - 2, str.length);
	}

	pecahTanggal(): void {
		let date: Date = new Date(this._date);
		this._tahun = date.getFullYear();
		this._bulan = date.getMonth();
		this._minggu = this.mingguUtil.getWeekN(date, date.getDate());
		this._tanggal = date.getDate();
	}

	clone(): JournalRow {
		let res: JournalRow = new JournalRow();

		res.date = this._date;
		res.desc = this._desc;
		res.jumlah = this._jumlah;
		res._tahun = this._tahun;
		res._bulan = this._bulan;
		res._minggu = this._minggu;
		res._tanggal = this._tanggal;

		return res;
	}

	date2Input(): string {
		let date: Date = (new Date(this._date));
		let res: string;
		let month: string = (date.getMonth() + 1) + '';
		let day: string = (date.getDate()) + '';

		res = date.getFullYear() + '-' + this.padding(month) + '-' + this.padding(day);

		return res;
	}

	getDateStr(): string {
		let date: Date = (new Date(this._date));
		let res: string = date.toLocaleDateString();
		return res;
	}

	backup(): void {

	}

	restore(): void {

	}

	public get tahun(): number {
		// let date: Date = new Date(this._date);
		// return date.getFullYear();
		return this._tahun;
	}
	public get bulan(): number {
		// return (new Date(this._date)).getMonth();
		return this._bulan;
	}
	public get minggu(): number {
		return this._minggu;
	}
	public get tanggal(): number {
		return this._tanggal;
	}

	public get date(): number {
		return this._date;
	}
	public set date(value: number) {
		this._date = value;
	}

	public get desc(): string {
		return this._desc;
	}
	public set desc(value: string) {
		this._desc = value;
	}

	public get jumlah(): number {
		return this._jumlah;
	}
	public set jumlah(value: number) {
		this._jumlah = value;
	}


}