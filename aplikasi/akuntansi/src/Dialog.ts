import { BaseComponent } from "./BaseComponent.js";

export class Dialog extends BaseComponent {
	private _contentEl: HTMLDivElement = null;
	private _okBtn: HTMLButtonElement = null;
	private _batalBtn: HTMLButtonElement = null;
	private _footerEl: HTMLDivElement = null;

	private _hasil: number = 0;
	private _onClick: Function = null;

	constructor() {
		super();
		this._template = `
			<div class='ha-dialog'>
				<div class='box'>
					<div class='content'>
					</div>
					<div class='footer'>
						<button class='ok'>Ya</button>
						<button class='batal'>Tidak</button>
					</div>
				</div>
			</div>
		`;

		this.build();
		this._contentEl = this.getEl('div.content') as HTMLDivElement;
		this._okBtn = this.getEl('button.ok') as HTMLButtonElement;
		this._batalBtn = this.getEl('button.batal') as HTMLButtonElement;
		this._footerEl = this.getEl('div.footer') as HTMLDivElement;


		this._el.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			this.hide();
			console.log('dialog click');
			this._hasil = 1;
			if (this._onClick) {
				this._onClick();
				this._onClick = () => { };
			}
		}

		this._batalBtn.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			this.hide();
			this._hasil = 0;
			if (this._onClick) {
				this._onClick();
				this._onClick = () => { };
			}
		}
	}

	private footerClear(): void {
		while (this._footerEl.childElementCount > 0) {
			this._footerEl.removeChild(this._footerEl.firstChild);
		}
	}

	setAsOK(): void {
		this.footerClear();
		this._footerEl.appendChild(this._okBtn);
		this._okBtn.innerText = 'OK';
	}

	setAsYN(): void {
		this.footerClear();
		this._footerEl.appendChild(this._okBtn);
		this._footerEl.appendChild(this._batalBtn);
		this._okBtn.innerText = 'Ya';
		this._batalBtn.innerText = 'Tidak';
	}

	public get contentEl(): HTMLDivElement {
		return this._contentEl;
	}

	public get batalBtn(): HTMLButtonElement {
		return this._batalBtn;
	}

	public get okBtn(): HTMLButtonElement {
		return this._okBtn;
	}

	public get hasil(): number {
		return this._hasil;
	}

	public get onClick(): Function {
		return this._onClick;
	}
	public set onClick(value: Function) {
		this._onClick = value;
	}
	public get teks(): string {
		return this._contentEl.innerHTML;
	}

	public set teks(value: string) {
		this._contentEl.innerHTML = value;
	}

}