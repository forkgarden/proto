export class Template {
	private template: DocumentFragment;

	constructor() {
		this.template = document.body.querySelector('template').content;
	}

	private getEl(query: string): HTMLElement {
		return this.template.querySelector(query).cloneNode(true) as HTMLElement;
	}

	get daftar(): HTMLElement {
		return this.template.querySelector('div.item-transaksi').cloneNode(true) as HTMLElement;
	}

	get form(): HTMLElement {
		return this.template.querySelector('div.form').cloneNode(true) as HTMLElement;
	}

	get formEdit(): HTMLElement {
		return this.getEl('div.form-edit');
	}

	get lapSemua():HTMLElement {
		return this.getEl('div.lap-semua');
	}

	get menuItem(): HTMLElement {
		return this.getEl('div.menu-item');
	}
}