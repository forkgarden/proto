export class MingguUtil {
	private _lastDay: Date;

	constructor() {

	}

	getWeekN(date: Date, n: number): number {
		let listMingguan: Array<number> = [];
		let res: number = 0;

		listMingguan = this.getDaftarMinggu(date);
		listMingguan.push(this._lastDay.getDate());

		res = listMingguan.length;
		for (let i: number = listMingguan.length - 1; i >= 0; i--) {
			if (listMingguan[i] >= n) res = i;
		}

		return res;

	}

	getList(date: Date): Array<Array<number>> {
		let listMinggu: Array<number>;
		let res: Array<Array<number>> = [];

		listMinggu = this.getDaftarMinggu(date);
		res = this.getDateGroupByWeek(listMinggu);

		return res;
	}

	/**
	 * Daftar mingguan dalam sebuan
	 * @param date 
	 */
	getDaftarMinggu(date: Date): Array<number> {
		let res: Array<number> = [];

		let y: number = date.getFullYear()
		let m: number = date.getMonth();

		this._lastDay = new Date(y, m + 1, 0);

		for (let i: number = 1; i <= this._lastDay.getDate(); i++) {
			let date2: Date = new Date(date.getFullYear(), date.getMonth(), i);
			if (date2.getDay() == 0) {
				res.push(i);
			}
		}

		return res;
	}

	getDateGroupByWeek(listMinggu: Array<number>): Array<Array<number>> {
		let res: Array<Array<number>> = []
		let i = 0;

		res.push(this.getDateFirstWeek(listMinggu))

		for (i = 1; i < listMinggu.length; i++) {
			res.push(this.getDateWeekN(i, listMinggu));
		}

		res.push(this.getDateLastWeek(listMinggu));

		return res;
	}

	getDateLastWeek(listMinggu: Array<number>) {
		let res = [];
		let date1 = listMinggu[listMinggu.length - 1];

		for (let i: number = date1 + 1; i <= this._lastDay.getDate(); i++) {
			res.push(i);
		}

		return res;
	}

	getDateWeekN(n: number, listMinggu: Array<number>) {
		let res = [];
		let date1 = listMinggu[n - 1];
		let date2 = listMinggu[n];

		for (let i: number = date1 + 1; i <= date2; i++) {
			res.push(i);
		}
		return res;
	}

	getDateFirstWeek(listMinggu: Array<number>) {
		let res = [];
		let minggu1 = listMinggu[0];

		for (let i: number = 1; i <= minggu1; i++) {
			res.push(i);
		}

		return res;
	}

	public get lastDay(): Date {
		return this._lastDay;
	}
	public set lastDay(value: Date) {
		this._lastDay = value;
	}

}
