import { JournalRow } from "./JournalRow.js"

export class Db {
	private _list: Array<JournalRow> = [];

	removeAll(): void {
		while (this._list.length > 0) {
			this._list.pop();
		}
	}

	total(): number {
		let tot: number = 0;

		console.log('get total, list length ' + this._list.length);

		for (let i: number = 0; i < this._list.length; i++) {
			tot += this._list[i].jumlah;
		}

		console.log('total ' + tot);

		return tot;
	}

	public get list(): Array<JournalRow> {
		return this._list;
	}

	insert(journal: JournalRow): void {
		this._list.push(journal);
	}

	toString(): string {
		return JSON.stringify(this._list);
	}

	fromString(str: string): void {
		let data: Array<any> = JSON.parse(str);

		while (this._list.length > 0) {
			this._list.pop();
		}

		for (let i: number = 0; i < data.length; i++) {
			let item: JournalRow = new JournalRow();
			item.desc = data[i]._desc;
			item.date = data[i]._date;
			item.jumlah = data[i]._jumlah;
			item.pecahTanggal();

			this.insert(item);
		}

	}

	delete(item: JournalRow): void {
		for (let i: number = 0; i < this._list.length; i++) {
			if (this._list[i] == item) {
				this._list.splice(i, 1);
				return;
			}
		}

		throw new Error('');
	}
}