import { BaseComponent } from "../../BaseComponent.js";
import { JournalRow } from "../../JournalRow.js";
// import { Akun } from "../../Akun.js";
import { ILaporan } from "../ILaporan.js";
import { Db } from "../../Db.js";

export class LapBulanan extends BaseComponent implements ILaporan {
	private anggotaAr: Array<Baris> = [];
	private _db: Db;

	constructor() {
		super();

		this._template = `
			<div class='lap-bulanan'>
				<p class='judul'>Laporan Bulanan</p>
				<hr/>
				<div class='cont'></div>
			</div>
		`;
		this.build();
	}

	ada(tahun: number, bulan: number): Baris {
		for (let i: number = 0; i < this.anggotaAr.length; i++) {
			let baris: Baris = this.anggotaAr[i];
			if (baris.tahun == tahun && baris.bulan == bulan) {
				return baris;
			}
		}

		return null;
	}

	buatLaporan(): void {
		let list: Array<JournalRow> = this._db.list;

		this.anggotaAr = [];

		for (let i: number = 0; i < list.length; i++) {
			let j: JournalRow = list[i];
			let baris: Baris;

			baris = this.ada(j.tahun, j.bulan);
			if (baris) {
				baris.anggotaAr.push(j);
			}
			else {
				baris = new Baris();
				baris.tahun = j.tahun;
				baris.bulan = j.bulan;
				baris.anggotaAr.push(j);
				this.anggotaAr.push(baris);
			}
		}
	}

	render(): void {
		this.cont.innerHTML = '';

		for (let i: number = 0; i < this.anggotaAr.length; i++) {
			let baris: Baris;
			baris = this.anggotaAr[i];
			baris.render();
			baris.attach(this.cont);
		}
	}

	get cont(): HTMLDivElement {
		return this.getEl('div.cont') as HTMLDivElement;
	}

	public get db(): Db {
		return this._db;
	}
	public set db(value: Db) {
		this._db = value;
	}

}

class Baris extends BaseComponent {
	private _anggotaAr: Array<JournalRow> = [];
	private _bulan: number = 0;
	private _tahun: number = 0;

	constructor() {
		super();
		this._template = `
			<div class='baris-bulanan'>
				<p class='tahun'></p>
				<p class='bulan'></p>
				<div class='cont'></div>
				<p class='total'></p>
			</div>
		`;
		this.build();
	}

	render(): void {
		this.renderAnggota();
		this.renderJumlah();
	}

	renderJumlah(): void {
		let jumlah: number = 0;

		for (let i: number = 0; i < this._anggotaAr.length; i++) {
			jumlah += this._anggotaAr[i].jumlah;
		}

		this.getEl('p.total').innerText = jumlah + '';
	}

	renderAnggota(): void {
		this.cont.innerHTML = '';

		for (let i: number = 0; i < this._anggotaAr.length; i++) {
			let anggota: JournalRow;
			let view: JournalRowView;

			anggota = this._anggotaAr[i];
			view = new JournalRowView();
			view.journal = anggota;
			view.render();
			view.attach(this.cont);
		}
	}

	public get cont(): HTMLDivElement {
		return this.getEl('div.cont') as HTMLDivElement;
	}

	public get anggotaAr(): Array<JournalRow> {
		return this._anggotaAr;
	}

	public get tahun(): number {
		return this._tahun;
	}
	public set tahun(value: number) {
		this._tahun = value;
		this.getEl('p.tahun').innerText = 'Tahun: ' + value + '';
	}
	public get bulan(): number {
		return this._bulan;
	}
	public set bulan(value: number) {
		this._bulan = value;
		this.getEl('p.bulan').innerText = 'Bulan: ' + value + '';
	}
}

class JournalRowView extends BaseComponent {
	private _journal: JournalRow = null;

	constructor() {
		super();
		this._template = `
			<div class='journal-row-view'>
				<p class='tanggal'></p>
				<p class='deskripsi'></p>
				<p class='jumlah'></p>
			</div>
		`;
		this.build();
	}

	render(): void {
		this.getEl('p.tanggal').innerText = this._journal.getDateStr();
		this.getEl('p.deskripsi').innerText = this._journal.desc;
		this.getEl('p.jumlah').innerText = this._journal.jumlah + '';
	}

	public get journal(): JournalRow {
		return this._journal;
	}

	public set journal(value: JournalRow) {
		this._journal = value;
	}
}