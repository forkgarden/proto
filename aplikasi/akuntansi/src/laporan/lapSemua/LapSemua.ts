import { BaseComponent } from "../../BaseComponent.js";
import { ListView } from "./ListView.js";
import { Akun } from "../../Akun.js";
import { Db } from "../../Db.js";
import { Util } from "../../Util.js";

export class LapSemua extends BaseComponent {
	private _listView: ListView = null;
	private _listCont: HTMLDivElement = null;
	private totalEl: HTMLSpanElement = null;
	private _db: Db;

	constructor() {
		super();
		this._listView = new ListView();
	}

	refresh(): void {
		console.log('lap semua refresh');
		this.totalEl.innerText = Util.tambahTitik(this._db.total() + '');
		this._listView.refresh();
	}

	init(): void {
		console.log('lapSemua init');
		this._db = Akun.inst.db;
		this._el = Akun.inst.template.lapSemua;

		this._listCont = this.getEl('div.daftar') as HTMLDivElement;
		this.totalEl = this.getEl('div.total span.total') as HTMLSpanElement;

		this._listView.init();
	}

	public get db(): Db {
		return this._db;
	}

	public get listCont(): HTMLDivElement {
		return this._listCont;
	}

	public get listView(): ListView {
		return this._listView;
	}
	public set listView(value: ListView) {
		this._listView = value;
	}

}