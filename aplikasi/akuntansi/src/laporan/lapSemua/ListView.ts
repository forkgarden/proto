import { Db } from "../../Db.js";
import { Akun } from "../../Akun.js";
import { BaseComponent } from "../../BaseComponent.js";
import { JournalRow } from "../../JournalRow.js";
import { Util } from "../../Util.js";
import { Edit } from "../../Edit.js";
import { LapSemua } from "./LapSemua.js";

export class ListView {
	private _list: Array<Item> = [];
	private db: Db = null;
	private _itemAktif: Item;
	private _cont: HTMLDivElement = null;

	init(): void {
		this._cont = Akun.inst.homePage.lapSemua.listCont;
		this.db = Akun.inst.db;
	}

	refresh(): void {
		this.clear();

		for (let i: number = 0; i < this.db.list.length; i++) {
			let journal: JournalRow = this.db.list[i];
			let item: Item = new Item(journal);
			item.attach(this._cont);
			this._list.push(item);
		}
	}

	update(): void {
		for (let i: number = 0; i < this._list.length; i++) {
			let item: Item;
			item = this._list[i];
			item.update();
		}
	}

	clear(): void {
		while (this._list.length > 0) {
			let item: Item;
			item = this._list.pop();
			item.destroy();
		}
	}

	public get itemAktif(): Item {
		return this._itemAktif;
	}
	public set itemAktif(value: Item) {
		this._itemAktif = value;
	}
	public get list(): Array<Item> {
		return this._list;
	}


}

export class Item extends BaseComponent {
	private _journal: JournalRow;

	private desc: HTMLDivElement = null;
	private jml: HTMLDivElement = null;
	private tombolCont: HTMLDivElement;
	private editTbl: HTMLButtonElement;
	private hapusTbl: HTMLButtonElement;
	private tgl: HTMLParagraphElement = null;

	constructor(journal: JournalRow) {
		super();
		this._el = Akun.inst.template.daftar;

		this.desc = Util.getEl(this._el, "div.desc") as HTMLDivElement;
		this.jml = Util.getEl(this._el, 'div.jml') as HTMLDivElement;
		this.tombolCont = Util.getEl(this._el, 'div.cont') as HTMLDivElement;
		this.editTbl = this.getEl('button.edit') as HTMLButtonElement;
		this.hapusTbl = this.getEl('button.hapus') as HTMLButtonElement;
		this.tgl = this.getEl('p.tgl') as HTMLParagraphElement;

		this.editTbl.onclick = (event: MouseEvent) => {
			event.stopPropagation();
			this.editClick();;
		}

		this.hapusTbl.onclick = (event: MouseEvent) => {
			event.stopPropagation();
			this.hapusClick();
		}

		this._journal = journal;
		this.update();

		this._el.onclick = this.onClick.bind(this);
	}

	editClick(): void {
		let edit: Edit = Akun.inst.edit;
		edit.edit(this._journal);
		this.hideTombol();
		Akun.inst.pageContBersih();
		Akun.inst.edit.attach(Akun.inst.pageCont);
	}

	hapusClick(): void {
		let db: Db = Akun.inst.db;
		let lapSemua: LapSemua = Akun.inst.homePage.lapSemua;

		db.delete(this._journal);
		lapSemua.refresh();
		Akun.inst.system.simpan();
		this.hideTombol();
	}

	hideTombol(): void {
		this.tombolCont.style.display = 'none';
	}

	onClick(): void {
		let display: string = this.tombolCont.style.display;
		let itemAktif: Item;

		itemAktif = Akun.inst.homePage.lapSemua.listView.itemAktif;

		if (itemAktif) {
			itemAktif.hideTombol();
		}

		if (display == 'none' || display == '') {
			this.tombolCont.style.display = 'block';
		}
		else if (this.tombolCont.style.display == 'block') {
			this.hideTombol();
		}
		else {
			// console.log(this.tombolCont.style.display);
			// console.log(this.tombolCont.style);
			throw new Error(this.tombolCont.style.display);
		}
		Akun.inst.homePage.lapSemua.listView.itemAktif = this;
	}

	// private tambahTitik(str: string): string {
	// 	let res: string = '';
	// 	let ctr: number = 0;

	// 	for (let i: number = str.length - 1; i >= 0; i--) {
	// 		let char: string;

	// 		char = str.slice(i, i + 1);
	// 		res = char + res;

	// 		ctr++;

	// 		if (ctr % 3 == 0) {
	// 			res = "." + res;
	// 		}
	// 	}

	// 	if (res.slice(0, 1) == ".") {
	// 		res = res.slice(1, res.length);
	// 	}
	// 	return res;
	// }

	update(): void {
		this.desc.innerText = this._journal.desc;
		this.jml.innerText = Util.tambahTitik((this._journal.jumlah) + '');
		this.tgl.innerText = this._journal.getDateStr();
	}
}