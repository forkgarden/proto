import { BaseComponent } from "../../BaseComponent.js";
import { Db } from "../../Db.js";
// import { Akun } from "../../Akun.js";
import { MingguUtil } from "../../MingguUtil.js";
import { JournalRow } from "../../JournalRow.js";
import { ILaporan } from "../ILaporan.js";

export class LapMingguan extends BaseComponent implements ILaporan {
	private _cont: HTMLDivElement = null;
	private _db: Db = null;
	private itemMingguan: Array<Array<number>> = [];
	private minggu: MingguUtil = new MingguUtil();
	private anggotaAr: Array<Baris> = [];

	constructor() {
		super();
		this._template = `
            <div class='lap-mingguan'>
				<p class='judul'>Laporan Bulanan</p>
				<hr/>
				<div class='cont'></div>
            </div>
        `;
		this.build();
		this._cont = this.getEl('div.cont') as HTMLDivElement;
	}

	debug(): void {
		console.log(this._cont);
		console.log(this.itemMingguan);
		console.log(this.minggu);
	}

	init(): void {
		// this.db = Akun.inst.db;
	}

	clear() {
		this._el.innerHTML = '';
	}

	ada(tahun: number, bulan: number, minggu: number): Baris {
		for (let i: number = 0; i < this.anggotaAr.length; i++) {
			let baris: Baris = this.anggotaAr[i];
			let tahun2: number = parseInt(baris.tahunP.innerText);
			let bulan2: number = parseInt(baris.bulanP.innerText);
			let minggu2: number = parseInt(baris.mingguP.innerText);

			if (tahun == tahun2 && bulan2 == bulan && (minggu2 == minggu)) {
				return baris;
			}
		}

		return null;
	}

	buatLaporan(): void {
		let list: Array<JournalRow> = this._db.list;

		this.anggotaAr = [];

		for (let i: number = 0; i < list.length; i++) {
			let j: JournalRow = list[i];
			let baris: Baris;

			baris = this.ada(j.tahun, j.bulan, j.minggu);
			if (baris) {
				baris.anggotaAr.push(j);
			}
			else {
				baris = new Baris();
				baris.tahunP.innerText = j.tahun + '';
				baris.bulanP.innerText = j.bulan + '';
				baris.mingguP.innerText = j.minggu + '';
				baris.anggotaAr.push(j);
				this.anggotaAr.push(baris);
			}
		}
	}

	render(): void {
		this._cont.innerHTML = '';

		for (let i: number = 0; i < this.anggotaAr.length; i++) {
			let baris: Baris;
			baris = this.anggotaAr[i];
			baris.render();
			baris.attach(this._cont);
		}
	}

	refresh(): void {
		this.clear();

		let res: Array<JournalRow> = [];

		for (let i: number = 0; i < this._db.list.length; i++) {
			let item: JournalRow = this._db.list[i];
			res.push(item);
		}

		for (let i: number = 0; i < res.length; i++) {

		}
	}

	// public get db(): Db {
	// 	return this._db;
	// }
	public set db(value: Db) {
		this._db = value;
	}

}

class Baris extends BaseComponent {
	private _anggotaAr: Array<JournalRow> = [];

	constructor() {
		super();
		this._template = `
			<div class='baris-mingguan'>
				<p class='tahun'></p>
				<p class='bulan'></p>
				<p class='minggu'></p>
				<div class='cont'></div>
				<p class='total'></p>
			</div>
		`;
		this.build();
	}

	render(): void {
		this.renderAnggota();
		this.renderJumlah();
	}

	renderJumlah(): void {
		let jumlah: number = 0;

		for (let i: number = 0; i < this._anggotaAr.length; i++) {
			jumlah += this._anggotaAr[i].jumlah;
		}

		this.getEl('p.total').innerText = jumlah + '';
	}

	renderAnggota(): void {
		this.cont.innerHTML = '';

		for (let i: number = 0; i < this._anggotaAr.length; i++) {
			let anggota: JournalRow;
			let view: JournalRowView;

			anggota = this._anggotaAr[i];
			view = new JournalRowView();
			view.journal = anggota;
			view.render();
			view.attach(this.cont);
		}
	}

	public get cont(): HTMLDivElement {
		return this.getEl('div.cont') as HTMLDivElement;
	}

	public get anggotaAr(): Array<JournalRow> {
		return this._anggotaAr;
	}

	public get tahunP(): HTMLParagraphElement {
		return this.getEl('p.tahun') as HTMLParagraphElement;
	}

	public get bulanP(): HTMLParagraphElement {
		return this.getEl('p.bulan') as HTMLParagraphElement;
	}

	public get mingguP(): HTMLParagraphElement {
		return this.getEl('p.minggu') as HTMLParagraphElement;
	}
}

class JournalRowView extends BaseComponent {
	private _journal: JournalRow = null;

	constructor() {
		super();
		this._template = `
			<div class='journal-row-view'>
				<p class='tanggal'></p>
				<p class='deskripsi'></p>
				<p class='jumlah'></p>
			</div>
		`;
		this.build();
	}

	render(): void {
		this.getEl('p.tanggal').innerText = this._journal.getDateStr();
		this.getEl('p.deskripsi').innerText = this._journal.desc;
		this.getEl('p.jumlah').innerText = this._journal.jumlah + '';
	}

	public get journal(): JournalRow {
		return this._journal;
	}

	public set journal(value: JournalRow) {
		this._journal = value;
	}
}