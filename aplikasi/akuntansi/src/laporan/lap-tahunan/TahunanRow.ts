import { JournalRow } from "../../JournalRow.js";
import { BaseComponent } from "../../BaseComponent.js";

export class TahunanRow extends BaseComponent {
	private _tahun: number = 0;
	private _members: Array<JournalRow> = [];
	private _view: JournalRowView = new JournalRowView();

	constructor() {
		super();
		this._template = `
			<div class='lap-tahunan-item'>
				<p class='tahun'></p>
				<p class='total'></p>
				<div class='cont'></div>
			</div>
		`;
		this.build();
	}

	total(): number {
		let hasil: number = 0;
		for (let i: number = 0; i < this._members.length; i++) {
			let item: JournalRow;
			item = this._members[i];
			hasil += item.jumlah;
		}

		return hasil;
	}

	render(): void {
		for (let i: number = 0; i < this._members.length; i++) {
			let view: JournalRowView = new JournalRowView();
			let item: JournalRow = this._members[i];
			view.tanggalP.innerText = item.getDateStr();
			view.deskP.innerHTML = item.desc;
			view.jumlahP.innerHTML = item.jumlah + '';
			view.attach(this.getEl('div.cont') as HTMLDivElement);
		}
		this.getEl('p.total').innerText = 'Total: ' + this.total();
	}

	public get members(): Array<JournalRow> {
		return this._members;
	}

	public get tahun(): number {
		return this._tahun;
	}
	public set tahun(value: number) {
		this._tahun = value;
		this.getEl('p.tahun').innerText = 'Tahun: ' + value;
	}

	push(j: JournalRow): void {
		this.members.push(j);
	}

	public get view(): JournalRowView {
		return this._view;
	}

}

class JournalRowView extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='journal-item'>
				<p class='tanggal'></tanggal>
				<p class='desk'></p>
				<p class='jumlah'></p>
			</div>
		`;
		this.build();
	}

	get tanggalP(): HTMLParagraphElement {
		return this.getEl('p.tanggal') as HTMLParagraphElement;
	}

	get deskP(): HTMLParagraphElement {
		return this.getEl('p.desk') as HTMLParagraphElement;
	}

	get jumlahP(): HTMLParagraphElement {
		return this.getEl('p.jumlah') as HTMLParagraphElement;
	}
}