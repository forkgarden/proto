import { BaseComponent } from "../../BaseComponent.js";
import { JournalRow } from "../../JournalRow.js";
import { TahunanRow } from "./TahunanRow.js";
import { Db } from "../../Db.js";
import { ILaporan } from "../ILaporan.js";

export class LapTahunan extends BaseComponent implements ILaporan {
	protected list: Array<TahunanRow> = [];
	private _db: Db;

	constructor() {
		super();
		this._template = `
			<div class='lap-tahunan'>
				<p class='judul'>Laporan Tahunan</p>
				<p class='total'></p>
				<hr/>
				<div class='cont'></div>
			</div>
		`;

		this.build();

	}

	total(): number {
		let hasil: number = 0;
		for (let i: number = 0; i < this.list.length; i++) {
			hasil += this.list[i].total();
		}
		return hasil;
	}

	protected ada(tahun: number): TahunanRow {
		for (let i: number = 0; i < this.list.length; i++) {
			let item: TahunanRow = this.list[i];
			if (item.tahun == tahun) {
				return item;
			}
		}

		return null;
	}

	buatLaporan(): void {
		let daftar: Array<JournalRow> = this._db.list;

		console.log('buat ' + daftar.length);

		for (let i: number = 0; i < daftar.length; i++) {
			let item: JournalRow = daftar[i];
			let baris: TahunanRow = this.ada(item.tahun);

			if (baris) {
				baris.push(item);
			}
			else {
				baris = new TahunanRow();
				baris.tahun = item.tahun;
				baris.push(item);
				this.list.push(baris);
			}
		}

	}

	render(): void {
		let cont: HTMLDivElement = this.getEl('div.cont') as HTMLDivElement;

		cont.innerText = '';
		for (let i: number = 0; i < this.list.length; i++) {
			let item: TahunanRow = this.list[i];

			item.render();
			item.attach(cont);
		}
		this.getEl('p.total').innerText = 'Total ' + this.total();
	}

	renderLog(): void {
		console.log('render ' + this.list.length);
		console.log('db');
		console.log(this._db);

		for (let i: number = 0; i < this.list.length; i++) {
			console.group('tahun: ' + this.list[i].tahun);
			for (let j: number = 0; j < this.list[i].members.length; j++) {
				let item2: JournalRow = this.list[i].members[j];
				console.log("item tahun " + item2.tahun + '/item ' + item2);
			}
			console.groupEnd();
		}
	}

	public set db(db: Db) {
		this._db = db;
	}



}