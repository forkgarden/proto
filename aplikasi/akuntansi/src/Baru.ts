import { Util } from "./Util.js";
import { Db } from "./Db.js";
import { Akun } from "./Akun.js";
import { JournalRow } from "./JournalRow.js";
import { Dialog } from "./Dialog.js";

export class Baru {
	private el: HTMLElement = null;

	private okTbl: HTMLButtonElement = null;
	private desc: HTMLTextAreaElement = null;
	private jml: HTMLInputElement = null;

	private db: Db;

	constructor() {
		this.el = document.querySelector('template').content.querySelector('div.form-baru').cloneNode(true) as HTMLElement;
		this.okTbl = Util.getEl(this.el, "button.ok") as HTMLButtonElement;
		this.okTbl.onclick = this.okClick.bind(this);

		this.desc = Util.getEl(this.el, "textarea.deskripsi") as HTMLTextAreaElement;
		this.jml = Util.getEl(this.el, 'input.jml') as HTMLInputElement;
	}

	init(): void {
		console.log('Baru init');
		this.db = Akun.inst.db;

		// this.desc.onfocus = () => {
		// 	this.desc.select();
		// }

		// this.jml.onfocus = () => {
		// 	this.jml.select();
		// }
	}

	okClick(): void {
		let journal: JournalRow;
		let dialog: Dialog = Akun.inst.dialog;

		if (!(parseInt(this.jml.value) > 0)) {
			dialog.setAsOK();
			dialog.show();
			dialog.teks = 'Jumlah harus diisi dengan benar!';
			return;
		}

		if (this.desc.value == '') {
			dialog.setAsOK();
			dialog.show();
			dialog.teks = 'Deskripsi tidak boleh kosong !';
			return;
		}

		journal = new JournalRow();
		journal.desc = this.desc.value;
		journal.jumlah = parseInt(this.jml.value);

		this.db.insert(journal);
		Akun.inst.homePage.lapSemua.refresh();
		Akun.inst.system.simpan();

		this.desc.value = '';
		this.jml.value = '';
	}

	attach(cont: HTMLDivElement): void {
		cont.appendChild(this.el);
	}

	update(): void {

	}
}