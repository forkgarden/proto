import { MingguUtil } from "../MingguUtil.js";

export class Test {
	private testMingguNTbl: HTMLButtonElement = null;

	constructor() {
		window.onload = () => {
			this.init();
		};
	}

	init(): void {
		this.testMingguNTbl = document.querySelector('button.test-minggu-n') as HTMLButtonElement;
		this.testMingguNTbl.onclick = () => {
			this.testMinggu();
		}
	}

	testMinggu(): void {
		// let minggu: Minggu = new Minggu();
		let date: Date = new Date();

		this.testMinggu1(1, date);
		this.testMinggu1(5, date);
		this.testMinggu1(7, date);
		this.testMinggu1(10, date);
		this.testMinggu1(16, date);
		this.testMinggu1(21, date);
		this.testMinggu1(25, date);
		this.testMinggu1(28, date);

	}

	testMinggu1(test: number, date: Date): void {
		let minggu: MingguUtil = new MingguUtil();

		let n: number = minggu.getWeekN(date, test);
		// console.log('test date ' + test);
		// console.log('week res ' + n);
		// console.log('week member ' + minggu.getList(date)[n]);
		console.log(minggu.getList(date)[n].indexOf(test) > -1);
	}
}

new Test();