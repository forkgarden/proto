import { JournalRow } from "../JournalRow.js";
import { Db } from "../Db.js";
import { LapTahunan } from "../laporan/lap-tahunan/LapTahunan.js";

export class TestLapTahunan {
	private db: Db = new Db();

	constructor() {
		console.log(this);
		window.onload = () => {
			this.init();
		}
	}

	init(): void {
		console.log('init');
		this.insert(2001, 1, 1);
		this.insert(2001, 1, 2);
		this.insert(2001, 1, 3);
		this.insert(2002, 1, 1);
		this.insert(2003, 1, 1);
		this.insert(2004, 1, 1);

		let lap: LapTahunan = new LapTahunan();
		lap.db = this.db;
		lap.buatLaporan();
		lap.render();
		lap.attach(window.document.body as HTMLDivElement);
	}

	insert(tahun: number, bulan: number, tanggal: number): void {
		let journal: JournalRow = new JournalRow();
		let date: Date = new Date(tahun, bulan, tanggal);
		journal.date = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
		journal.desc = 'deskripsi';
		journal.jumlah = Math.floor(Math.random() * 100) * 100;
		journal.pecahTanggal();
		this.db.insert(journal);
		console.log(this.db);
	}
}

new TestLapTahunan();