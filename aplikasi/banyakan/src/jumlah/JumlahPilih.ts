import { BaseSoal } from "../BaseSoal.js";
import { Acak } from "../Acak.js";
import { iconBuah } from "../Buah.js";

export class JumlahPilih extends BaseSoal {

	protected tblAr: Array<Tombol> = [];
	protected soalKotak: HTMLDivElement = null;
	protected jawabCont: HTMLDivElement = null;

	protected angkaJawaban: number = 0;
	protected angkaDipilih: number = 0;
	protected acakAngkaSoal: Acak = null;

	constructor() {
		super();

		this._template = `
			<div class='jumlah-pilih'>
				<div class='bar-cont'></div>
				<p class='judul-soal'>Berapa Jumlahnya</p> 
				<div class='soal'>
					<div class='soal1'></div>
					<div class='soal2'/></div>
				</div>
				<div class='jawaban'>
					<button class='jawab satu putih'></button>
					<button class='jawab dua putih'></button>
					<button class='jawab tiga putih'></button>
				</div>
				<div class='kirim-cont'>
					<button class='kirim normal'>Kirim</button>
				</div>
			</div>
		`;
		this.build();

		this.soalKotak = this.getEl('div.soal div.soal1') as HTMLDivElement;
		this.jawabCont = this.getEl('div.jawaban') as HTMLDivElement;

		this.tblAr = [];
		this.setTombol(this.getEl('button.jawab.satu') as HTMLButtonElement);
		this.setTombol(this.getEl('button.jawab.dua') as HTMLButtonElement);
		this.setTombol(this.getEl('button.jawab.tiga') as HTMLButtonElement);

		this.acakAngkaSoal = new Acak(this.angkaMax);
	}


	jawabanBenar(): string {
		return this.angkaJawaban + '';;
	}

	setTombol(tblView: HTMLButtonElement): void {
		let tbl: Tombol = new Tombol();
		tbl.view = tblView;
		this.tblAr.push(tbl);
		tbl.onClick = () => {
			this.tombolClick(tbl);
		}
	}

	check(): boolean {
		if (this.angkaDipilih == this.angkaJawaban) return true;
		return false;

		// this.soalIdx++;
		// this.bar.persen2(this.soalIdx, this.jmlSoal);

		// if (idx.angka == this.angkaSoal) {
		// 	this._nilai++;
		// 	this.feedbackBenarShow(this._cont);
		// }
		// else {
		// 	this.feedbackSalahShow(this._cont);
		// }
	}

	tombolClick(tombol: Tombol): void {
		console.log('tombol click ' + tombol.angka + '/' + this.angkaJawaban);
		this.angkaDipilih = tombol.angka;

		for (let i: number = 0; i < this.tblAr.length; i++) {
			let tombol1: Tombol = this.tblAr[i];
			tombol1.view.classList.remove('dipilih');
		}

		tombol.view.classList.add('dipilih');
	}

	debug(): void { }

	reset() {
		console.log('Jumlah: reset');
		super.reset();

		this.angkaJawaban = this.acakAngkaSoal.angka() + 1;

		this.tblAr[0].angka = this.angkaJawaban;
		this.tblAr[1].angka = this.acakAngkaSoal.angkaKecuali([this.tblAr[0].angka]);
		this.tblAr[2].angka = this.acakAngkaSoal.angkaKecuali([this.tblAr[0].angka, this.tblAr[1].angka]);

		this.tblAr[0].tulis();
		this.tblAr[1].tulis();
		this.tblAr[2].tulis();

		for (let i: number = 0; i < 1000; i++) {
			this.acakSoal();
		}

		for (let i: number = 0; i < this.tblAr.length; i++) {
			this.tblAr[i].view.parentElement.removeChild(this.tblAr[i].view);
		}

		for (let i: number = 0; i < 3; i++) {
			this.jawabCont.appendChild(this.tblAr[i].view);
		}

		for (let i: number = 0; i < this.tblAr.length; i++) {
			this.tblAr[i].view.classList.remove('dipilih');
		}


		let str: string = '';
		let iconIdx: number = Math.floor(Math.random() * iconBuah.length);
		for (let i: number = 0; i < this.angkaJawaban; i++) {
			str += iconBuah[iconIdx];
		}
		this.soalKotak.innerHTML = str;
	}

	acakSoal(): void {
		let a: number = Math.floor(Math.random() * 3);
		let b: number = Math.floor(Math.random() * 3);

		if (a == b) return;

		let angka: Tombol = new Tombol();
		angka = this.tblAr[a];
		this.tblAr[a] = this.tblAr[b];
		this.tblAr[b] = angka;
	}
}

class Tombol {

	protected _angka: number = 0;
	protected _view: HTMLButtonElement;

	public set onClick(value: Function) {
		this._view.onclick = () => {
			value(this);
		}
	}

	public get angka(): number {
		return this._angka;
	}
	public set angka(value: number) {
		this._angka = value;
	}
	public get view(): HTMLButtonElement {
		return this._view;
	}
	public set view(value: HTMLButtonElement) {
		this._view = value;
	}

	tulis(): void {
		this._view.innerText = this._angka + '';
	}

	copy(angka2: Tombol): void {
		angka2.angka = this._angka;
		angka2.view = this._view;
	}

	copyFrom(angka: Tombol): void {
		this._angka = angka.angka;
		this._view = angka.view;
	}

}