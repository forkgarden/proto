import { BaseComponent } from "./BaseComponent.js";

export class Feedback extends BaseComponent {

	private labelP: HTMLParagraphElement = null;
	private button: HTMLButtonElement = null;
	private _onClick: Function;

	constructor() {
		super();
		this._template = `
			<div class='feedback'>
				<div class='cont'>
					<p class='feedback'>Jawaban kamu benar</p>
					<p class='jawaban'><b>Jawaban Benar: </b><span class='jawaban'></span></p>
					<button class='normal'>soal berikutnya</button>
				</div>
			</div>
		`;
		this.build();
		this.button = this.getEl('button') as HTMLButtonElement;
		this.labelP = this.getEl('p.feedback') as HTMLParagraphElement;
	}

	onAttach(): void {
		this.button.focus();
	}

	init(): void {

	}

	get jawabP(): HTMLParagraphElement {
		return this.getEl('p.jawaban') as HTMLParagraphElement;
	}

	public get label(): string {
		return this.labelP.innerHTML;
	}
	public set label(value: string) {
		this.labelP.innerHTML = value;
	}

	public set onClick(value: Function) {
		this._onClick = value;

		this.button.onclick = () => {
			this.button.blur();
			this.detach();
			this._onClick();
		}
	}

	public set type(value: number) {
		if (value == FeedbackEnum.BENAR) {
			this.getEl('div.cont').style.backgroundColor = '#b2fa91';
		}
		else if (value == FeedbackEnum.SALAH) {
			this.getEl('div.cont').style.backgroundColor = '#ff9999';
		}
		else {
			throw new Error();
		}
	}


}

export enum FeedbackEnum {
	BENAR,
	SALAH
}