import { Game } from "../Game.js";
import { Acak } from "../Acak.js";
import { BaseSoal } from "../BaseSoal.js";

export class BandingkanTanda extends BaseSoal {

	private _kdTbl: HTMLButtonElement = null;
	private _sdTbl: HTMLButtonElement = null;
	private _ldTbl: HTMLButtonElement = null;

	private kiriEl: HTMLDivElement = null;
	private tengahEl: HTMLDivElement = null;
	private kananEl: HTMLDivElement = null;

	private angkaAcak: Acak = null;
	private angkaAcak2: Acak = null;
	private angkas: Array<number> = [];
	private jawabanUser: string = '';

	constructor() {
		super();
		this._template = `
			<div class='banyakan-tanda'>
				<div class='bar-cont'></div>
				<p class='judul'>Pilih Tanda Yang Sesuai</judul>
				<br/>
				<br/>
				<div class='soal'>
					<div class='angka kiri'>

					</div>
					<div class='tanda'>

					</div>
					<div class='angka kanan'>

					</div>
				</div>
				<div class='jawaban'>
					<button class='kiri'>&lt</button>
					<button class='tengah'>=</button>
					<button class='kanan'>&gt</button>
				</div>
				<div class='kirim-cont'>
					<button class='normal kirim'>Kirim</button>
				</div>
			</div>		
			`;

		this.build();

		this._kdTbl = this.getEl('div.jawaban button.kiri') as HTMLButtonElement;
		this._sdTbl = this.getEl('div.jawaban button.tengah') as HTMLButtonElement;
		this._ldTbl = this.getEl('div.jawaban button.kanan') as HTMLButtonElement;

		this.kiriEl = this.getEl('div.soal div.angka.kiri') as HTMLDivElement;
		this.tengahEl = this.getEl('div.soal div.tanda') as HTMLDivElement;
		this.kananEl = this.getEl('div.soal div.angka.kanan') as HTMLDivElement;

		this.angkaAcak = new Acak(10);
		this.angkaAcak2 = new Acak(10);
	}

	init(): void {
		super.init();

		this._cont = Game.inst.cont;

		this._kdTbl.onclick = () => {
			this.kdClick();
		}

		this._sdTbl.onclick = () => {
			this.sdClick();
		}

		this._ldTbl.onclick = () => {
			this.ldClick();
		}

	}

	jawabanBenar(): string {
		// console.log('jawaban benar ');
		if (this.angkas[0] > this.angkas[1]) {
			// console.log('>');
			return ">"
		}
		else if (this.angkas[0] < this.angkas[1]) {
			// console.log('<');
			return "<";
		}
		else if (this.angkas[0] == this.angkas[1]) {
			// console.log('=');
			return "=";
		}
		else {
			throw new Error();
		}
	}

	reset(): void {
		super.reset();

		this.angkas[0] = this.angkaAcak.angka();
		this.angkas[1] = this.angkaAcak2.angka();

		if (this.angkas[0] > 10) {
			this.angkas[0] -= 10;
		}

		if (this.angkas[1] > 10) {
			this.angkas[1] -= 10;
		}

		this.kiriEl.innerHTML = this.angkas[0] + '';
		this.kananEl.innerHTML = this.angkas[1] + '';
		this.tengahEl.innerHTML = '';
	}

	kdClick(): void {
		this.jawabanUser = '<';
		this.tengahEl.innerHTML = this.jawabanUser;
	}

	sdClick(): void {
		this.jawabanUser = '=';
		this.tengahEl.innerHTML = this.jawabanUser;
	}

	ldClick(): void {
		this.jawabanUser = '>';
		this.tengahEl.innerHTML = this.jawabanUser;
	}

	check(): boolean {
		if (this.angkas[0] > this.angkas[1]) {
			if (this.jawabanUser != '>') return false;
		}

		if (this.angkas[0] == this.angkas[1]) {
			if (this.jawabanUser != '=') return false;
		}

		if (this.angkas[0] < this.angkas[1]) {
			if (this.jawabanUser != '<') return false;
		}

		return true;
	}

	public get kdTbl(): HTMLButtonElement {
		return this._kdTbl;
	}
	public get sdTbl(): HTMLButtonElement {
		return this._sdTbl;
	}
	public get ldTbl(): HTMLButtonElement {
		return this._ldTbl;
	}


}