export class TableSoal {

	insert(baris: BarisSoal) {
		//TODO:
		baris;
	}

	//TODO:
	getById(id: number): BarisSoal {
		id;
		return null;
	}

	//TODO:
	deleteById(id: number): boolean {
		id;
		return true;
	}

}

interface Baris {
	id: number;
}

export class BarisSoal implements Baris {
	private _id: number = -1;
	public get id(): number {
		return this._id;
	}
	public set id(value: number) {
		this._id = value;
	}
	private _key: string = '';
	public get key(): string {
		return this._key;
	}
	public set key(value: string) {
		this._key = value;
	}
	private _label: string = '';
	public get label(): string {
		return this._label;
	}
	public set label(value: string) {
		this._label = value;
	}
	private _description: string = '';
	public get description(): string {
		return this._description;
	}
	public set description(value: string) {
		this._description = value;
	}
	private _parentId: number;
	public get parentId(): number {
		return this._parentId;
	}
	public set parentId(value: number) {
		this._parentId = value;
	}
}