import { BaseSoal } from "../BaseSoal.js";
import { Angka } from "./Angka.js";
import { Acak } from "../Acak.js";

export class Penjumlahan extends BaseSoal {
	static readonly J_AWAL: number = 0;
	static readonly J_TENGAH: number = 1;
	static readonly J_AKHIR: number = 2;

	private angkas: Array<Angka> = [];
	private acak: Acak = null;
	private acakPos2: Acak = null;
	private _posisiJawaban: number = Penjumlahan.J_AKHIR;
	private _batasAtas: number = 10;
	private _batasBawah: number = 0;
	private _acakPos: boolean = false;
	private judulP: HTMLParagraphElement = null;
	private operatorSpan: HTMLSpanElement = null;
	private _pengurangan: boolean = false;
	// private debug: boolean = true;
	// private debugCtr: number = 0;

	constructor() {
		super();
		this._template = `
			<div class='penjumlahan'>
				<div class='bar-cont'></div>
				<p class='judul-soal'>Berapa Jumlahnya</p>
				<hr/>
				<div class='soal-cont'>
					<input class='angka satu'>
					<span class='operator'>+</span>
					<input class='angka dua'>
					<span>=</span>
					<input class='angka tiga'>
				</div>
				<hr/> 
				<button class='normal kirim'>Kirim</button>
			</div>
		`;
		this.build();

		for (let i: number = 0; i < 3; i++) {
			let angka: Angka;
			let input: HTMLElement;
			input = this.getEl2('input.angka', i);
			angka = new Angka(input);
			angka.input.type = 'text';
			this.angkas.push(angka);
		}

		this.judulP = this.getEl('p.judul-soal') as HTMLParagraphElement;
		this.operatorSpan = this.getEl('span.operator') as HTMLSpanElement;

		this.acak = new Acak(10);
		this.acakPos2 = new Acak(3);
	}

	jawabanBenar(): string {

		for (let i: number = 0; i < 3; i++) {
			let angka: Angka;
			angka = this.angkas[i];
			if (angka.readonly == false) {
				return "" + angka.angka;
			}
		}

		return "";
	}

	check(): boolean {
		for (let i: number = 0; i < 3; i++) {
			let angka: Angka;
			angka = this.angkas[i];
			if (parseInt(angka.value) != angka.angka) {
				return false;
			}
		}
		return true;
	}

	validasiSimpanPenjumlahan(): boolean {
		if (this._batasAtas <= 10) return true;

		// this.debugCtr++;
		// if (this.debugCtr > 200) {
		// 	console.log('gagal');
		// 	return true;
		// }

		// console.log(this.angkas[1].angka);

		if ((this.angkas[0].satuan + this.angkas[1].satuan) >= 10) return false;
		// if ((this.angkas[0].puluhan + this.angkas[1].puluhan) > 10) return false;

		// console.log('sukses');
		return true;
	}

	validasiSimpanPengurangan(): boolean {
		if (this.angkas[0].satuan < this.angkas[1].satuan) return true;
		if (this.angkas[0].puluhan < this.angkas[1].puluhan) return true;
		return false;
	}

	// validasiBatas(): boolean {
	// 	if (this.angkas[2].angka <= this._batasAtas) {
	// 		if (this.angkas[2].angka >= this._batasBawah) {
	// 			return true;
	// 		}
	// 	}
	// 	return false;
	// }

	validasiPenjumlahan(): boolean {
		// if (!this.validasiBatas()) return false;
		if (!this.validasiSimpanPenjumlahan()) return false;
		// console.log('validasi selesai');
		return true;
	}

	validasiPengurangan(): boolean {
		// if (!this.validasiBatas()) return false;
		if (!this.validasiSimpanPengurangan()) return false;

		return true;
	}

	buatAngkaPengurangan(): void {
		for (let i: number = 0; i < 2; i++) {
			let angka: Angka = this.angkas[i];
			angka.angka = angka.acak.get3(this._batasBawah, this._batasAtas);
			angka.input.type = 'text';
			angka.readonly = true;
		}
		while (true) {
			let angka: Angka = this.angkas[1];
			angka.angka = angka.acak.get3(this._batasBawah, this._batasAtas);
			if (this.angkas[1].angka > this.angkas[0].angka) {
				let a: number = this.angkas[1].angka;
				this.angkas[1].angka = this.angkas[0].angka;
				this.angkas[0].angka = a;
			}
			this.angkas[2].angka = this.angkas[0].angka - this.angkas[1].angka;
			if (this.validasiPengurangan()) return;
		}
	}

	buatAngkaPenjumlahan(): void {
		for (let i: number = 0; i < 2; i++) {
			let angka: Angka = this.angkas[i];
			angka.angka = angka.acak.get3(this._batasBawah, this.batasAtas);
			angka.input.type = 'text';
			angka.readonly = true;
		}
		console.log(this.angkas[0].angka);

		while (true) {
			let angka: Angka = this.angkas[1];
			angka.angka = angka.acak.get3(this._batasBawah, this.batasAtas);
			this.angkas[2].angka = this.angkas[0].angka + this.angkas[1].angka;
			if (this.validasiPenjumlahan()) return;
		}
	}

	setPosisiJawaban(): void {
		let angka: Angka;

		if (this._acakPos) {
			this._posisiJawaban = this.acakPos2.angka();
		}

		angka = this.angkas[this._posisiJawaban];
		angka.input.type = 'number';
		angka.readonly = false;
		angka.value = '';
		angka.elHtml.focus();
	}

	reset(): void {
		super.reset();

		this.acak.max = this._batasAtas;
		this.angkas[0].acak.max = this._batasAtas;
		this.angkas[1].acak.max = this._batasAtas;
		this.angkas[2].acak.max = this._batasAtas;

		if (this._pengurangan) {
			this.operatorSpan.innerText = "-";
		}
		else {
			this.buatAngkaPenjumlahan();
			this.operatorSpan.innerText = "+";
		}

		this.setPosisiJawaban();
		if (this._posisiJawaban != 2) {
			this.judulP.innerHTML = 'Isikan kotak yang kosong';
		}
		else {
			this.judulP.innerHTML = 'Berapa Jumlahnya';
		}
	}

	public get posisiJawaban(): number {
		return this._posisiJawaban;
	}
	public set posisiJawaban(value: number) {
		this._posisiJawaban = value;
	}
	public get batasBawah(): number {
		return this._batasBawah;
	}
	public set batasBawah(value: number) {
		this._batasBawah = value;
	}
	public get acakPos(): boolean {
		return this._acakPos;
	}
	public set acakPos(value: boolean) {
		this._acakPos = value;
	}
	public get batasAtas(): number {
		return this._batasAtas;
	}
	public set batasAtas(value: number) {
		this._batasAtas = value;
	}
	public get pengurangan(): boolean {
		return this._pengurangan;
	}
	public set pengurangan(value: boolean) {
		this._pengurangan = value;
	}

}