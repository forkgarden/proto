import { BaseSoal } from "../BaseSoal.js";
import { Angka } from "./Angka.js";
import { Acak } from "../Acak.js";

//TODO: disatukan
export class BilanganBase extends BaseSoal {
	protected angkaAr: Array<Angka> = [];
	protected batasAtas: number = 10;
	protected batasBawah: number = 5;
	protected acak: Acak = null;
	// protected judul: string = '';

	protected jawabCont: HTMLDivElement = null;
	protected angkaCont: HTMLDivElement = null;
	protected batasSpan: HTMLSpanElement = null;

	constructor() {
		super();

		this._template = `
			<div class='bilangan cacah'>
				<div class='bar-cont'></div>
				<p class='judul-soal'>Sebutkan bilangan cacah kurang dari <span class='batas'>10</span></p>
				<div class='jawab-cont'>
				</div>
				<hr/>
				<div class='angka-cont'>
				</div>
				<div class='kirim-cont'>
					<button class='normal kirim'>Kirim</button>
				</div>
			</div>
		`;
		this.build();

		this.angkaCont = this.getEl('div.angka-cont') as HTMLDivElement;
		this.jawabCont = this.getEl('div.jawab-cont') as HTMLDivElement;
		this.batasSpan = this.getEl('p.judul-soal span.batas') as HTMLSpanElement;

		this.acak = new Acak(this.batasAtas - this.batasBawah);
	}

	angkaAda(angkaP: number): boolean {
		for (let i: number = 0; i < this.angkaAr.length; i++) {
			let angka: Angka;
			angka = this.angkaAr[i];
			if (angka.elHtml.parentElement == this.jawabCont) {
				if (angka.angka == angkaP) {
					return true;
				}
			}
		}

		return false;
	}

	check(): boolean {
		let jml: number = 0;
		for (let i: number = 0; i < this.angkaAr.length; i++) {
			let angka: Angka = this.angkaAr[i];
			if (angka.elHtml.parentElement == this.jawabCont) {
				jml++;
			}
		}

		if (jml != (this.batasAtas)) {
			console.log("jml " + jml + '/batas atas ' + this.batasAtas);
			return false;
		}

		console.log("check, batas atas: " + this.batasAtas);
		for (let i: number = 0; i < this.batasAtas; i++) {
			let ada: boolean = this.angkaAda(i);
			if (false == ada) {
				return false;
			}
		}

		return true;
	}

	init(): void {
		super.init();

		for (let i: number = 0; i <= 10; i++) {
			let angka: Angka = new Angka();
			angka.angka = i;
			angka.attach(this.angkaCont);
			angka.angkaClick = () => {
				this.angkaClick(angka);
			}
			this.angkaAr.push(angka);
		}
	}

	angkaClick(angka: Angka): void {
		if (angka.elHtml.parentElement == this.jawabCont) {
			angka.attach(this.angkaCont);
		}
		else if (angka.elHtml.parentElement == this.angkaCont) {
			angka.attach(this.jawabCont);
		}
		else {
			throw new Error('');
		}
	}

	reset(): void {
		super.reset();
		for (let i: number = 0; i < this.angkaAr.length; i++) {
			this.angkaAr[i].attach(this.angkaCont);
		}

		this.batasAtas = this.acak.angka() + this.batasBawah;
		this.batasSpan.innerText = this.batasAtas + '';
	}
}