import { BaseSoal } from "../BaseSoal.js";
import { Angka } from "./Angka.js";
import { Acak } from "../Acak.js";

export class Bilangan extends BaseSoal {
	static readonly BILANGAN_CACAH: number = 1;
	static readonly BILANGAN_ASLI: number = 2;
	static readonly BILANGAN_GANJIL: number = 3;
	static readonly BILANGAN_GENAP: number = 4;

	private angkaAr: Array<Angka> = [];
	private batasAtas: number = 10;
	private batasBawah: number = 0;
	private acak: Acak = null;
	private tipe: number = Bilangan.BILANGAN_CACAH;

	private jawabCont: HTMLDivElement = null;
	private angkaCont: HTMLDivElement = null;
	private batasSpan: HTMLSpanElement = null;

	constructor() {
		super();

		this._template = `
			<div class='bilangan cacah'>
				<div class='bar-cont'></div>
				<p class='judul-soal'>Sebutkan bilangan cacah kurang dari <span class='batas'>10</span></p>
				<div class='jawab-cont'>
				</div>
				<hr/>
				<div class='angka-cont'>
				</div>
				<div class='kirim-cont'>
					<button class='normal kirim'>Kirim</button>
				</div>
			</div>
		`;
		this.build();

		this.angkaCont = this.getEl('div.angka-cont') as HTMLDivElement;
		this.jawabCont = this.getEl('div.jawab-cont') as HTMLDivElement;
		this.batasSpan = this.getEl('p.judul-soal span.batas') as HTMLSpanElement;

		this.acak = new Acak(this.batasAtas - this.batasBawah);
	}

	jawabanBenar(): string {
		let hsl: Array<Angka> = [];
		let hasilStr: string = '';

		hsl = this.angkaAr.slice(0, this.batasAtas);

		for (let i: number = 0; i < hsl.length; i++) {
			hasilStr += hsl[i].angka + ', ';
		}

		hasilStr = hasilStr.slice(0, hasilStr.length - 2);

		return hasilStr;
	}

	angkaAdaDiJawabanCont(angkaP: number): boolean {
		for (let i: number = 0; i < this.angkaAr.length; i++) {

			let angka: Angka = this.angkaAr[i];

			if (angka.elHtml.parentElement == this.jawabCont) {
				if (angka.angka == angkaP) {
					return true;
				}
			}
		}

		return false;
	}

	jumlahJawabanYangBenar(): number {
		let hsl: number = 0;

		//TODO:
		if (Bilangan.BILANGAN_CACAH == this.tipe) {
			return this.batasAtas;
		}
		else if (Bilangan.BILANGAN_ASLI == this.tipe) {
			return this.batasAtas - 1;
		}
		else if (Bilangan.BILANGAN_GENAP == this.tipe) {
			hsl = 0;
			for (let i: number = this.batasBawah; i < this.batasAtas; i++) {
				if ((i % 2) == 0) {
					hsl++;
				}
			}

			return hsl;
		}
		else if (Bilangan.BILANGAN_GANJIL == this.tipe) {
			hsl = 0;
			for (let i: number = this.batasBawah; i < this.batasAtas; i++) {
				if ((i % 2) == 1) {
					hsl++;
				}
			}

			return hsl;
		}
		else {
			console.error('tipe tidak benar ' + this.tipe);
		}

		return 0;
	}

	hitungJmlJawabanUser(): number {
		let jml: number = 0;
		for (let i: number = 0; i < this.angkaAr.length; i++) {
			let angka: Angka = this.angkaAr[i];
			if (angka.elHtml.parentElement == this.jawabCont) {
				jml++;
			}
		}

		return jml;
	}

	daftarJawaban(): number[] {
		//TODO:
		return [];
	}

	check(): boolean {
		let jml: number = this.hitungJmlJawabanUser();

		if (jml != (this.jumlahJawabanYangBenar())) {
			//jumlah jawaban tidak benar
			return false;
		}

		console.log("check, batas atas: " + this.batasAtas);
		for (let i: number = 0; i < this.batasAtas; i++) {
			let ada: boolean = this.angkaAdaDiJawabanCont(i);
			if (false == ada) {
				return false;
			}
		}

		return true;
	}

	init(): void {
		super.init();

		for (let i: number = 0; i <= 10; i++) {
			let angka: Angka = new Angka();
			angka.angka = i;
			angka.attach(this.angkaCont);
			angka.angkaClick = () => {
				this.angkaClick(angka);
			}
			this.angkaAr.push(angka);
		}
	}

	angkaClick(angka: Angka): void {
		if (angka.elHtml.parentElement == this.jawabCont) {
			angka.attach(this.angkaCont);
		}
		else if (angka.elHtml.parentElement == this.angkaCont) {
			angka.attach(this.jawabCont);
		}
		else {
			throw new Error('');
		}
	}

	reset(): void {
		super.reset();
		for (let i: number = 0; i < this.angkaAr.length; i++) {
			this.angkaAr[i].attach(this.angkaCont);
		}

		this.batasAtas = this.acak.angka() + this.batasBawah;
		this.batasSpan.innerText = this.batasAtas + '';
	}
}