import { ITombol } from "./Menu.js";
import { Game } from "../Game.js";
import { Banyakan } from "../banyakan/Banyakan.js";
// import { click } from "./Data.js";

export const membandingkan: ITombol = {
	label: 'Membandingkan',
	members: [
		{
			label: 'Latihan 1',	//dengan gambar dua angka
			description: 'Membandingkan 2 gambar',
			onclick: (e: MouseEvent) => {
				e.stopPropagation();
				console.log('banyakan click');
				Game.inst.menu.detach();

				let banyakan: Banyakan = new Banyakan();
				banyakan.jmlAngka = 2;
				banyakan.angkaSaja = false;
				banyakan.init();
				banyakan.attach(Game.inst.cont);
				banyakan.mulai();
			},
			members: []
		},
		{
			label: 'Latihan 2',	//dengan gambar tiga angka
			description: 'Membandingkan 3 gambar',
			onclick: (e: MouseEvent) => {
				e.stopPropagation();
				console.log('banyakan click gambar 3');
				Game.inst.menu.detach();

				let banyakan: Banyakan = new Banyakan();
				banyakan.jmlAngka = 3;
				banyakan.angkaSaja = false;
				banyakan.init();
				banyakan.attach(Game.inst.cont);
				banyakan.mulai();
			},
			members: []
		},
		{
			label: 'Latihan 3', //'Dua Angka',
			description: 'Membandingkan dua angka',
			onclick: (e: MouseEvent) => {
				console.log('banyakan click angka 2');
				e.stopPropagation();
				Game.inst.menu.detach();

				let banyakan: Banyakan = new Banyakan();
				banyakan.jmlAngka = 2;
				banyakan.angkaSaja = true;
				banyakan.init();
				banyakan.attach(Game.inst.cont);
				banyakan.mulai();
			},
			members: []
		},
		{
			label: 'Latihan 4',//'Tiga Angka',
			description: 'Membandingkan 3 angka',
			onclick: (e: MouseEvent) => {
				console.log('banyakan click gambar 3');
				e.stopPropagation();
				Game.inst.menu.detach();

				let banyakan: Banyakan = new Banyakan();
				banyakan.jmlAngka = 3;
				banyakan.angkaSaja = true;
				banyakan.init();
				banyakan.attach(Game.inst.cont);
				banyakan.mulai();
			},
			members: []
		},
		{
			label: 'Latihan 5', //'Dengan Simbol',
			description: "Menggunakan simbol < > dan =",
			members: [],
			onclick: (e: MouseEvent) => {
				console.log('banyakan click gambar 3');
				e.stopPropagation();
				Game.inst.menu.detach();
				Game.inst.simbol.attach(Game.inst.cont);
				Game.inst.simbol.mulai();
			}
		}

	]
}