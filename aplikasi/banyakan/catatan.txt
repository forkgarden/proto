publish:
========
mengurutkan dari besar ke kecil

baru:
=====
mengurutkan lebih dari tiga angka
refactor array

mana yang paling kecil
jam
hari
penjumlahan bersusun
pengurangan bersusun

upgrade 1:
==========
jawaban benar pada feedback


upgrade 2:
==========
automation diaplikasikan (test)
penggunaan acak dipisah untuk tiap angka
base soal diaplikasikan ke semuanya
puluhan dan satuan dipisah untuk penjumlahan tanpa menyimpan
penjumlahan tanpa meminjam
pengurangan tanpa meminjam
membandingkan dengantidak pernah ada angka yang sama (acak dipisah)
kirim dibuat komponen


jangka panjang:
===============
angka mulai dari 1
mengenal angka 0
urutkan dari yang terbesar
coba tiga kali baru ganti soal
mengurutkan dengan angka yang jaraknya 1

game maze mengurutkan 
click pada angka 
karakter bergerak mengikuti angka di klik

lomba mobil
soal ulangan dengan soal di acak