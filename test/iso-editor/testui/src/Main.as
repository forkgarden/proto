package{
	import fg.ui.TombolToggle;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author test
	 */
	public class Main extends Sprite {
		
		public function Main() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			testTombol();
		}
		
		private function testTombol():void {
			var tombol:TombolToggle;
			
			tombol = new TombolToggle();
			tombol.clickCallBack = tombolOnClick;
			addChild(tombol.view);
			
		}
		
		private function tombolOnClick(e:MouseEvent):void {
			
		}
		
	}
	
}