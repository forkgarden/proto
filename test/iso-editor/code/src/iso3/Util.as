package iso3 {
	import flash.geom.Point;

	public class Util {
		
		public function Util() {
			
		}
		
		public static function assert(b:Boolean, msg:String = ""):void {
			if (false == b) {
				try {
					throw new Error(msg);
				}
				catch (e:Error) {
					trace(e.getStackTrace());
					throw new Error();
				}
			}
		}
		
		public static function pSejajarHor(p1:Point, p2:Point):Boolean {
			return p1.y == p2.y;
		}
		
		public static function pSejajarVer(p1:Point, p2:Point):Boolean {
			return p1.x == p2.x;
		}
		
		public static function pEqual(p1:Point, p2:Point):Boolean {
			return (pSejajarHor(p1, p2) && pSejajarVer(p1, p2));
		}
		
	}

}