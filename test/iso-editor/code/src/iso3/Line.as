package iso3 {
	import flash.display.Graphics;
	import flash.geom.Point;

	public class Line {
		
		private var _p1:Point = new Point();
		private var _p2:Point = new Point();
		
		public function Line() {
			
		}
		
		public function validatePos(pt1:Point, pt2:Point):Boolean {
			if (!Util.pEqual(_p1, pt1)) return false;
			if (!Util.pEqual(_p2, pt2)) return false;
			
			return true;
		}
		
		public function isConnectedNext(l:Line):Boolean {
			if (!Util.pEqual(_p1, l.p2)) return false;
			if (Util.pEqual(_p2, l.p1)) return false;
			if (Util.pEqual(_p1, l.p1)) return false;
			return true;
		}
		
		public function isHor():Boolean {
			return _p2.y == _p1.y;
		}
		
		public function isVer():Boolean {
			return _p2.x == _p1.x;
		}
		
		public function crossOnDotHor(i:int, j:int):Boolean {
			return (minY() == j) || (maxY() == j);
		}
		
		public function debug():void {
			trace("debug line " + _p1 + '/' +_p2);
		}
		
		public function render(g:Graphics):void {
			g.moveTo(_p1.x, _p1.y);
			g.lineTo(_p2.x, _p2.y);
		}
		
		public function get p1():Point {
			return _p1;
		}
		
		public function get p2():Point {
			return _p2;
		}
		
		public function minX():Number {
			return Math.min(_p1.x, _p2.x);
		}
		
		public function maxX():Number {
			return Math.max(_p1.x, _p2.x);
		}
		
		public function minY():Number {
			return Math.min(p1.y, p2.y);
		}
		
		public function maxY():Number {
			return Math.max(p1.y, p2.y);
		}
		
		public function getM(dy:Number, dx:Number):Number {
			if (dx == 0) dx = .001;
			return dy/dx;
		}
		
		public function rightSide():Boolean {
			return _p2.x > _p1.x;
		}
		
		public function leftSide():Boolean {
			return _p1.x > _p2.x;
		}
		
		public function debugCross(x:int, y:int):void {
			trace("line debug cross " + x + "/" + y);
			trace("right " + rightSide() + "/left " + leftSide());
			trace("hor " + isHor());
			trace("ver " + isVer());
			trace("m " + getM(_p2.y - _p1.y, _p2.x - _p1.x));
			trace("dot m " + getM(y - _p1.y, x - _p1.x));
			trace("cross dot " + crossOnDotHor(x, y));
			trace("cross m " + crossM(x, y));
			trace("cross " + crossLineHorRight(x, y));
		}
		
		public function inRangeX(x:Number,y:Number):Boolean {
			return x >= minX() && x <= maxX();
		}
		
		public function crossVer(x:Number, y:Number):Boolean {
			var resY:Boolean;
			var resX:Boolean;
			
			if (isVer() && inRangeY(x, y)) {
				resX = minX() > x;
				return resX;
			}
			
			return false;
		}
		
		public function inRangeY(x:Number, y:Number):Boolean {
			return (minY() < y) && (maxY() > y);
		}
		
		public function crossM(x:Number, y:Number):Boolean {
			var mDot:Number;
			var m:Number;
			
			m = getM(_p2.y - _p1.y, _p2.x - _p1.x);
			mDot = getM(y - _p1.y, x - _p1.x);
			
			if (rightSide()) {
				if (Math.abs(mDot) > Math.abs(m)) {
					return true;
				}
			}
			else if (leftSide()) {
				if (Math.abs(mDot) < Math.abs(m)) {
					return true;
				}
			}
			
			return false;
		}
		
		public function crossLineHorRight(x:Number, y:Number):Boolean {
			if (isHor()) return false;
			
			if (!inRangeY(x, y)) return false;
			
			if (crossOnDotHor(x, y)) return false;
				
			if (!inRangeX(x, y)) {
				if (x < minX()) {
					return true;
				}
				else {
					return false;
				}
			}
			else {	
				
				if (crossVer(x, y)) return true;
				
				if (crossM(x, y)) return true;
				
			}
			
			return false;
			
		}
		
		public function destroy():void {
			
		}
		
	}

}