package iso3 {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObjectContainer;
	import flash.display.GradientType;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import fg.isometrics.Iso;

	public class Block {
		
		public static const P_TOP:int = 0;
		public static const P_RIGHT:int = 1;
		public static const P_LEFT:int = 2;
		
		public static const B_HEIGHT:int = 40;
		
		private const DOT_LEN:int = 8;
		private const P_LEN:int = 3;
		
		private var _view:Sprite;
		private var _x:Number = 0;
		private var _y:Number = 0;
		private var _z:Number = 0;
		
		private var dots:Vector.<Point>;
		private var polygons:Vector.<Polygon4>;
		private var imgHovers:Vector.<Bitmap>;
		private var _polygonHoverIdx:int =-1;
		
		private var polygon1:Polygon4;
		private var polygon2:Polygon4;
		private var polygon3:Polygon4;
		
		//private var topSelectedImg:Bitmap;
		//private var rightSelectedImg:Bitmap;
		//private var leftSelectedImg:Bitmap;
		
		//private var topSprite:Sprite;
		//private var rightSprite:Sprite;
		//private var leftSprite:Sprite;
		
		//private var topDots:Vector.<Point>;
		//private var rightDots:Vector.<Point>;
		//private var leftDots:Vector.<Point>;
		
		private var clr:uint = 0x00ee00;
		private var iso:Iso;
		
		public function Block() {
			iso = new Iso();
			
			_view = new Sprite();
			_view.graphics.lineStyle(1, 0,1,true);
			
			dots = new Vector.<Point>();
			imgHovers = new Vector.<Bitmap>();
			
			polygonInit();
			
			setDots([
				new Point(  0, -40),
				new Point( 32, -24),
				new Point( 32,  16),
				new Point(  0,  32),
				new Point(-32,  16),
				new Point(-32, -24),
				new Point(  0,  -8),
				new Point(  0,   0)
			]);
			
		}
		
		public static function create():Block {
			var block:Block;
			
			block = new Block();
			block.renderClear();
			block.renderWhite();
			block.renderLine();
			
			return block;
		}
		
		public function rotateLeft():void {
			//TODO:
		}
		
		public function rotateDepan():void {
			//TODO:
		}
		
		public function rotateBelakang():void {
			//TODO:
		}
		
		public function flipHor():void {
			//TODO:
		}
		
		public function rotateRight():void {
			var x2:int;
			var z2:int;
			
			x2 = z;
			z2 = -x;
			
			x = x2;
			z = z2;
		}
		
		public function getLocalX(x:Number):Number {
			return x - _view.x;
		}
		
		public function getLocalY(y:Number):Number {
			return y - _view.y;
		}
		
		public function getViewPosY():Number {
			return _view.y;
		}
		
		public function getZIndex():int {
			return _view.parent.getChildIndex(_view);
		}
		
		public function setZIndex(idx:int):void {
			_view.parent.setChildIndex(_view, idx);
		}
		
		public function swapIdx(other:Block):void {
			var idx:int;
			
			//trace("swap idx");
			
			idx = getZIndex();
			setZIndex(other.getZIndex());
			other.setZIndex(idx);
		}
		
		public function setPos(x:int, y:int, z:int):void {
			_x = x;
			_y = y;
			_z = z;
		}
		
		public function zOrder(other:Block):Boolean {
			if (didepan(other)) {
				if (getZIndex() < other.getZIndex()) {
					//swapIdx(other);
					return true;
				}
			}
			else if (other.didepan(this)) {
				if (getZIndex() > other.getZIndex()) {
					//swapIdx(other);
					return true;
				}
			}
			
			return false;
		}
		
		public function debugZorder(other:Block):void {
			trace("");
			trace("debug z order");
			trace("z index " + getZIndex() + "/" + other.getZIndex());
			trace("y " + _y + "/" + other.y);
			trace("");
		}
		
		//public function drawSide(i:int):void {
			//polygons[i].render(_view.graphics, clr);
		//}
		
		//private function drawFromArray();
		
		//private function drawRightSprite():void {
			//
		//}
		
		//private function drawTopSprite():void {
			//var g:Graphics;
			//
			//g = _view.graphics;
			//g.clear();
			//g.lineStyle(1);
			//g.beginFill(clr);
			//g.moveTo(dots[0].x, dots[0].y);
			//g.lineTo(dots[1].x, dots[1].y);
			//g.lineTo(dots[6].x, dots[6].y);
			//g.lineTo(dots[5].x, dots[5].y);
			//g.lineTo(dots[0].x, dots[0].y);
			//g.endFill();
		//}
		
		private function polygonInit():void {
			polygons = new Vector.<Polygon4>();
			
			polygon1 = new Polygon4();
			polygon2 = new Polygon4();
			polygon3 = new Polygon4();
			
			polygons.push(polygon1);
			polygons.push(polygon2);
			polygons.push(polygon3);			
		}
		
		public function renderClear():void {
			_view.graphics.clear();
		}
		
		//public function renderHovered(idx:int):void {
			//renderWhite();
			//renderPolygon(idx);
		//}
		
		public function renderPolygon(i:int, clr:uint = 0x00ff00):void {
			polygons[i].render(_view.graphics, clr);
		}
		
		//private function setSelectedImg():void {
			//topSelectedImg = new Bitmap(Main.inst.bmp.isoTop.bitmapData);
			//topSelectedImg.x = -topSelectedImg.width / 2;
			//topSelectedImg.y = -40;
			//_view.addChild(topSelectedImg);
			//
			//leftSelectedImg = new Bitmap(Main.inst.bmp.isoLeft.bitmapData);
			//leftSelectedImg.x = - 32;
			//leftSelectedImg.y = -24;
			//_view.addChild(leftSelectedImg);
			//
			//rightSelectedImg = new Bitmap(Main.inst.bmp.isoRight.bitmapData);
			//rightSelectedImg.x = 0;
			//rightSelectedImg.y = -24;
			//_view.addChild(rightSelectedImg);		
			//
			//imgHovers.push(topSelectedImg);
			//imgHovers.push(rightSelectedImg);
			//imgHovers.push(leftSelectedImg);
		//}
		
		public function getPolygon(i:int):Polygon4 {
			return polygons[i];
		}
		
		public function destroy():void {
			var p:Polygon4;
			var i:int;
			
			for (i = 0; i < P_LEN; i++ ) {
				polygons[i].destroy();
			}
			
			while (polygons.length > 0) polygons.pop();
			
			polygon1 = null;
			polygon2 = null;
			polygon3 = null;
			
			if (_view.parent) _view.parent.removeChild(_view);
			_view.removeChildren();
			_view = null;
		}
		
		/*
		public function setBmp(bmpData:BitmapData):void {
			var bmp:Bitmap;
			
			_view.removeChildren();
			
			bmp = new Bitmap(bmpData);
			_view.addChild(bmp);
			bmp.x = - bmp.width / 2;
			bmp.y = -bmp.height + 32;
		}
		*/
		
		private function dotClear():void {
			while (dots.length > 0) {
				dots.pop();
			}
		}
		
		public function didepan(other:Block):Boolean {
			var res:Boolean;
			var resY:int;
			var resZ:int;
			var resX:int;
			
			resY = _y - other.y;
			resZ = _z - other.z;
			resX = _x - other.x;
			
			if (resY > 0) {
				return true;
			}
			else if (resY == 0) {
				if (resZ > 0) {
					return true;
				}
				else if (resZ == 0) {
					if (resX > 0) {
						return true;
					}
					else if (resX == 0 ) {
						return false;
					}
					else {
						return false;
					}
				}
				else {
					return false;
				}
			}
			else {
				return false;
			}
		}
		
		public function getPolygonHoverIdxAbs(x:int, y:int):int {
			var idx:int;
			
			idx = getPolygonHoverIdx(x - _view.x, y - _view.y);
			
			return idx;
		}
		
		//TODO: filter mouse position outside block
		public function getPolygonHoverIdx(x:int, y:int):int {
			var i:int;
			
			for (i = 0; i < P_LEN; i++) {
				if (polygons[i].dotInside(x, y)) {
					return i;
				}
			}
			
			return -1;
		}
		
		public function onHover(sideIdx:int):void {
			renderClear();
			renderPolygon(sideIdx);
			renderLine();
		}
		
		public function dotInside(x:int, y:int):Boolean {
			return getPolygonHoverIdx(x, y) > -1;
		}
		
		public function setDots(dots:Array):void {
			var i:int;
			var p:Point;
			
			dotClear();
			
			for (i = 0; i < dots.length; i++) {
				p = dots[i];
				this.dots.push(p);
			}
			
			polygon1.setDot([dots[0], dots[1], dots[6], dots[5]]);
			polygon2.setDot([dots[6], dots[1], dots[2], dots[3]]);
			polygon3.setDot([dots[5], dots[6], dots[3], dots[4]]);
			
			validate();
		}
			
		public function debugSetViewPos(i:int, j:int):void {
			_view.x = i;
			_view.y = j;
		}
		
		public function debug():void {
			var i:int;
			
			trace("debug block");
			for (i = 0; i < dots.length; i++) {
				trace("point " + dots[i]);
			}
			
			for (i = 0; i < polygons.length; i++) {
				polygons[i].debug();
			}
		}
		
		public function renderSelected(idx:int):void {
			renderClear();
			renderWhite();
			renderPolygon(idx);
			renderLine();
		}
		
		public function renderWhite():void {
			renderClear();
			renderPolygon(0, 0xffffff);
			renderPolygon(1, 0xeeeeee);
			renderPolygon(2, 0xdddddd);
			renderLine();
		}
		
		public function renderIso(offsetX:int, offsetY:int = 0):void {
			_view.x = iso.isoProjectX(_x * Main.TILE_SIZE, _z * Main.TILE_SIZE) + offsetX;
			_view.y = iso.isoProjectZ(_x * Main.TILE_SIZE, _z * Main.TILE_SIZE) + offsetY;
			_view.y -= _y * (B_HEIGHT);
		}
		
		public function renderLine():void {
			var i:int;
			
			_view.graphics.lineStyle(1);
			for (i = 0; i < polygons.length; i++) {
				polygons[i].renderLine(_view.graphics);
			}
			
		}
		
		public function renderDefault():void {
			renderClear();
			renderLine();
		}
		
		public function debugOnHover(x:int, y:int):int {
			var px:int;
			var py:int;
			var idx:int;			
			
			px = x;
			py = y;
			
			px -= _view.x;
			py -= _view.y;
			
			idx = getPolygonHoverIdx(px, py);
			
			renderClear();
			
			if ( -1 != idx) {
				renderPolygon(idx);
			}
			
			renderLine();
			
			return idx;
		}
		
		public function addToStage(cont:DisplayObjectContainer):void {
			cont.addChild(_view);
		}
		
		public function validate():void {
			var i:int;
			
			Util.assert(null !== dots);
			Util.assert(DOT_LEN == dots.length);
			
			polygon1.validate();
			polygon2.validate();
			polygon3.validate();
			
			//validate dots
			Util.assert(Util.pSejajarHor(dots[5], dots[1]));
			Util.assert(Util.pSejajarHor(dots[4], dots[2]));
			
			Util.assert(Util.pSejajarVer(dots[0], dots[6]));
			Util.assert(Util.pSejajarVer(dots[6], dots[7]));
			Util.assert(Util.pSejajarVer(dots[7], dots[3]));
			
			Util.assert(Util.pSejajarVer(dots[5], dots[4]));
			Util.assert(Util.pSejajarVer(dots[1], dots[2]));
			
			//
		}
		
		//public function get view():Sprite {
			//return _view;
		//}
		
		public function get x():Number {
			return _x;
		}
		
		public function set x(value:Number):void {
			_x = value;
		}
		
		public function get y():Number {
			return _y;
		}
		
		public function set y(value:Number):void {
			_y = value;
		}
		
		public function get z():Number {
			return _z;
		}
		
		public function set z(value:Number):void {
			_z = value;
		}
		
	}

}