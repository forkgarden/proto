package iso3 {
	import fg.debugs.Debugger;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	/**
	 * ...
	 * @author test
	 */
	public class BlockManager {
		//private var blockList:Vector.<Block>;
		private var hoverBlock:Block;
		private var hoverSide:int = 0;
		private var clickBlock:Block;
		private var _cont:Sprite;
		private var sortBlock:Block;
		private var debugBlock:Block;
		private var _blockData:BlockData;
		private var debugSortStatus:Boolean;
		
		public function BlockManager() {
			//blockList = new Vector.<Block>();
			//blockList = blockData.
		}
		
		public function rotateRight():void {
			var block:Block;
			var i:int;
			
			for (i = 0; i < _blockData.length(); i++) {
				block = _blockData.getByIdx(i);
				block.rotateRight();
				block.renderIso(Main.ISO_OFFSET, Main.ISO_OFFSETY);
			}
		}
		
		public function init():void {
			firstBlock();
		}
		
		private function firstBlock():void {
			var block:Block;
			
			block = Block.create();
			block.x = 0;
			block.y = 0;
			block.z = 0;
			block.renderIso(Main.ISO_OFFSET, Main.ISO_OFFSETY);
			block.addToStage(_cont);
			_blockData.add(block);
			
			block = Block.create();
			block.x = 1;
			block.y = 0;
			block.z = -1;
			block.renderIso(Main.ISO_OFFSET, Main.ISO_OFFSETY);
			block.addToStage(_cont);
			_blockData.add(block);			
		}
		
		/*
		public function sort():void {
			var block2:Block;
			var block:Block;
			var i:int;
			var j:int;
			var len:int;
			var blockTemp:Block;
			
			len = blockList.length;
			
			for (i = 0; i < len; i++) {
				for (j = i + 1; j < len; j++) {
					block = blockList[i];
					
					block2 = blockList[j];
					
					
					if (block.zOrder(block2)) {
						block.swapIdx(block2);
						
					}
				}
			}
			
		}
		*/
		
		public function onClick(x:int, y:int):void {
			var block:Block;
			
			if (hoverBlock) {
				block = new Block();
				block.addToStage(_cont);
				//blockList.push(block);
				_blockData.add(block);
				
				if (hoverSide == 0) {
					block.x = hoverBlock.x;
					block.y = hoverBlock.y + 1;
					block.z = hoverBlock.z;
				}
				else if (hoverSide == 1) {
					block.x = hoverBlock.x + 1;
					block.y = hoverBlock.y;
					block.z = hoverBlock.z;
				}
				else if (hoverSide == 2) {
					block.x = hoverBlock.x;
					block.y = hoverBlock.y;
					block.z = hoverBlock.z + 1;			
				}
				else {
					throw new Error();
				}
				
				block.renderIso(Main.ISO_OFFSET, Main.ISO_OFFSETY);
				block.renderWhite();
				//sort();
			}
		}
		
		public function onHover(block:Block, x:int, y:int):void {
			var polygonIdx:int;
			
			polygonIdx = block.getPolygonHoverIdxAbs(x, y);
			
			if (polygonIdx > -1) {
				if (hoverBlock) {
					if (block.didepan(hoverBlock)) {
						hoverBlock = block;
						hoverSide = polygonIdx;
					}
				}
				else {
					hoverBlock = block;
					hoverSide = polygonIdx;
				}
			}
		}
		
		private function hoverBlockReset():void {
			if (hoverBlock) {
				hoverBlock.renderWhite();
			}
			hoverBlock = null;
			hoverSide = -1;
		}
		
		public function debug():void {
			var debugBlock:Block;
			var localX:Number;
			var localY:Number;
			var d:Debugger;
			
			debugBlock = _blockData.getByIdx(0);
			localX = debugBlock.getLocalX(_cont.mouseX);
			localY = debugBlock.getLocalY(_cont.mouseY);
			d = Debugger.getInst();
			
			Debugger.getInst().addLine("hover block " + hoverBlock, true);
			Debugger.getInst().addLine("hover side " + hoverSide);
			Debugger.getInst().addLine("pos " + _cont.mouseX + "/" + _cont.mouseY);
			Debugger.getInst().addLine("local " + localX + "/" + localY);
			Debugger.getInst().addLine("cross " + debugBlock.getPolygon(0).dotInside(localX, localY));			
			Debugger.getInst().addLine("debug sort " + debugSortStatus);			
			
			//informasi debug block
			d.addLine("block 0 hover " + _blockData.getByIdx(0).getPolygonHoverIdxAbs(_cont.mouseX, _cont.mouseY));
			d.addLine("block 1 hover " + _blockData.getByIdx(1).getPolygonHoverIdxAbs(_cont.mouseX, _cont.mouseY));
			
			d.addLine("block 1 depan " + _blockData.getByIdx(1).didepan(_blockData.getByIdx(0)));
			d.addLine("block 0 depan " + _blockData.getByIdx(0).didepan(_blockData.getByIdx(1)));
		}
		
		public function update():void {
			var i:int;
			var len:int;
			
			hoverBlockReset();
			debugSortStatus = false;
			
			len = _blockData.length();
			for (i = 0; i < len; i++) {
				onHover(_blockData.getByIdx(i), _cont.mouseX, _cont.mouseY);
				debugSortStatus = debugSortStatus || _blockData.sort(i);
			}
			
			if (hoverBlock) {
				hoverBlock.renderSelected(hoverSide);
			}
			
			debug();
		}
		
		public function get cont():Sprite {
			return _cont;
		}
		
		public function set cont(value:Sprite):void {
			_cont = value;
		}
		
		public function get blockData():BlockData {
			return _blockData;
		}
		
		public function set blockData(value:BlockData):void {
			_blockData = value;
		}
		
	}

}