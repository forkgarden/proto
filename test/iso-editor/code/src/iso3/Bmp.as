package iso3 {
	import flash.display.Bitmap;
	/**
	 * ...
	 * @author test
	 */
	public class Bmp {
		
		[Embed(source = "../../imgs/left.png")]
		private var IsoLeft:Class;
		
		[Embed(source = "../../imgs/right.png")]
		private var IsoRight:Class;
		
		[Embed(source="../../imgs/top.png")]
		private var IsoTop:Class;
		
		[Embed(source = "../../imgs/back.png")]
		private var Back:Class;
		
		private var _isoLeft:Bitmap;
		private var _isoRight:Bitmap;
		private var _isoTop:Bitmap;
		private var _back:Bitmap;
		
		public function Bmp() {
			_isoLeft = new IsoLeft();
			_isoRight = new IsoRight();
			_isoTop = new IsoTop();
			_back = new Back();
		}
		
		public function get isoLeft():Bitmap {
			return _isoLeft;
		}
		
		public function get isoRight():Bitmap {
			return _isoRight;
		}
		
		public function get isoTop():Bitmap {
			return _isoTop;
		}
		
		public function get back():Bitmap {
			return _back;
		}
		
	}

}