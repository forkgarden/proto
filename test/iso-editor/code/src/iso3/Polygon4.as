package iso3 {
	import flash.display.Graphics;
	import flash.geom.Point;

	public class Polygon4 {
		
		private var line1:Line;
		private var line2:Line;
		private var line3:Line;
		private var line4:Line;
		
		private const DOT_LEN:int = 4;
		private const LINE_LEN:int = 4;
		
		private var dots:Vector.<Point>;
		private var lines:Vector.<Line>;
		
		public function Polygon4() {
			line1 = new Line();
			line2 = new Line();
			line3 = new Line();
			line4 = new Line();
			
			dots = new Vector.<Point>();
			lines = new Vector.<Line>();
			
			lines.push(line1);
			lines.push(line2);
			lines.push(line3);
			lines.push(line4);
		}
		
		public function renderLine(g:Graphics):void {
			var i:int;
			
			for (i = 0; i < LINE_LEN; i++) {
				lines[i].render(g);
			}
		}
		
		public function destroy():void {
			var l:Line;
			var p:Point
			
			while (lines.length > 0) {
				l = lines.pop();
				l.destroy();
			}
			
			while (dots.length > 0) {
				dots.pop();
			}
			
			lines = null;
			dots = null;
			line1 = null;
			line2 = null;
			line3 = null;
		}
		
		public function setLinePos(line:Line, p1:Point, p2:Point):void {
			line.p1.x = p1.x;
			line.p1.y = p1.y;
			line.p2.x = p2.x;
			line.p2.y = p2.y;
		}
		
		public function setDot2Line():void {
			setLinePos(line1, dots[0], dots[1]);
			setLinePos(line2, dots[1], dots[2]);
			setLinePos(line3, dots[2], dots[3]);
			setLinePos(line4, dots[3], dots[0]);			
		}
		
		public function validate():void {
			Util.assert(lines != null, "lines null");
			
			Util.assert(lines.length == DOT_LEN, "line length invalid " + lines.length);
			
			Util.assert(line2.isConnectedNext(line1), "line 2 is not connected ");			
			Util.assert(line3.isConnectedNext(line2), "line 3 is not connected ");
			Util.assert(line4.isConnectedNext(line3), "line4 is not connected correctly");
			Util.assert(line1.isConnectedNext(line4), "line1 is not connected correctly");
			
			//negative test
			Util.assert(line1.validatePos(dots[0], dots[1]));
			//if (!line1.validatePos(dots[0], dots[1])) return false;
			Util.assert(line2.validatePos(dots[1], dots[2]));
			//if (!line2.validatePos(dots[1], dots[2])) return false;
			Util.assert(line3.validatePos(dots[2], dots[3]));
			//if (!line3.validatePos(dots[2], dots[3])) return false;
			Util.assert(line4.validatePos(dots[3], dots[0]));
			//if (!line4.validatePos(dots[3], dots[0])) return false;
			
			//;
		}
		public function getLine(i:int):Line {
			return lines[i];
		}
		
		public function debugCrossedLine(x:int, y:int):void {
			var i:int;
			var line:Line;
			
			trace("debug crossed line ");
			
			for (i = 0; i < lines.length; i++) {
				line = lines[i];
				if (line.crossLineHorRight(x, y)) {
					trace("line " + i + " success");
					//line.debug();
					//line.debugCross(x, y);
				}
				else {
					trace("line " + i + " failed");
					line.debug();
					line.debugCross(x, y);			
				}
			}
		}
		
		public function render(g:Graphics, clr:uint):void {
			var i:int;
			
			g.beginFill(clr);
			g.moveTo(dots[0].x, dots[0].y);
			g.lineTo(dots[1].x, dots[1].y);
			g.lineTo(dots[2].x, dots[2].y);
			g.lineTo(dots[3].x, dots[3].y);
			g.lineTo(dots[0].x, dots[0].y);
			g.endFill();
		}
		
		public function hasLineCrossedOnDot(i:int, j:int):Boolean {
			var i:int;
			
			for (i = 0; i < LINE_LEN; i++) {
				if (lines[i].crossOnDotHor(i, j)) return true;
			}
			
			return false;
		}
		
		public function getNumLineCrossed(x:int, y:int):int {
			var i:int;
			var line:Line;
			var crossCount:int = 0;
			
			
			for (i = 0; i < LINE_LEN; i++) {
				line = lines[i];
				if (line.crossLineHorRight(x, y)) {
					crossCount++;
				}
			}
			
			return crossCount;
		}
		
		public function maxY():Number {
			var res:Number;
			var i:int;
			
			res = dots[0].y;
			
			for (i = 0; i < DOT_LEN; i++) {
				if (dots[i].y > res) {
					res = dots[i].y;
				}
			}
			
			return res;			
		}
		
		public function minY():Number {
			var res:Number;
			var i:int;
			
			res = dots[0].y;
			
			for (i = 0; i < DOT_LEN; i++) {
				if (dots[i].y < res) {
					res = dots[i].y;
				}
			}
			
			return res;
		}
		
		public function maxX():Number {
			var res:Number;
			var i:int;
			
			res = dots[0].x;
			
			for (i = 0; i < DOT_LEN; i++) {
				if (dots[i].x > res) {
					res = dots[i].x;
				}
			}
			
			return res;
		}
		
		public function minX():Number {
			var res:Number;
			var i:int;
			
			res = dots[0].x;
			
			for (i = 0; i < DOT_LEN; i++) {
				if (dots[i].x < res) {
					res = dots[i].x;
				}
			}
			
			return res;
		}
		
		public function dotOutside(x:int, y:int):Boolean {
			if (x <= minX()) return true;
			if (y <= minY()) return true;
			if (x >= maxX()) return true;
			if (y >= maxY()) return true;
			return false;
		}
		
		public function dotInside(x:int, y:int):Boolean {
			var i:int;
			var crossCount:int = 0;
			var mod:Number = 0;
			var line:Line;
			
			if (hasLineCrossedOnDot(x,y)) {
				y++;
			}
			
			//if (dotOutside(x, y)) return false;
			
			crossCount = getNumLineCrossed(x, y);
			
			mod = crossCount % 2;
			
			if (1 == mod) {
				return true;
			}
			else if (0 == mod) {
				return false;
			}
			else {
				throw new Error();
			}
		}
		
		private function dotClear():void {
			while (dots.length > 0) {
				dots.pop();
			}			
		}
		
		public function setDot(ar:Array):void {
			var p:Point;
			var i:int;
			
			dotClear();
			
			for (i = 0; i < 4; i++) {
				dots.push(ar[i]);
			}
			
			setDot2Line();
			validate();
		}
		
		public function debug():void {
			trace("debug polygon");
			line1.debug();
			line2.debug();
			line3.debug();
			line4.debug();
			trace("");
		}
		
	}

}