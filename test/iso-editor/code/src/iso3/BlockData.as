package iso3 {
	/**
	 * ...
	 * @author test
	 */
	public class BlockData {
		
		private var blocks:Vector.<Block>;
		private static var inst:BlockData;
		
		public function BlockData() {
			if (inst) throw new Error;
			
			inst = this;
			blocks = new Vector.<Block>();
		}
		
		public function getByIdx(idx:int):Block {
			return blocks[idx];
		}
		
		public function length():int {
			return blocks.length;
		}
		
		public function add(block:Block):Boolean {
			var block1:Block;
			
			for each (block1 in blocks) {
				if (block1 == block) return false;
			}
			
			blocks.push(block);
			return true;
		}
		
		public function sort(i:int):Boolean {
			var blockTemp:Block;
			
			if (i >= blocks.length - 1) return false;
			
			if (blocks[i].zOrder(blocks[i + 1])) {
				blocks[i].swapIdx(blocks[i + 1]);
				
				blockTemp = blocks[i];
				blocks[i] = blocks[i + 1];
				blocks[i + 1] = blockTemp;
				
				return true;
			}
			
			return false;
		}
		
	}

}