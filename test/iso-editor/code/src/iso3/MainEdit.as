package iso3 {
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;

	public class MainEdit extends Sprite {
		
		public static const ISO_OFFSET:int = 400;
		
		private var blockList:Vector.<Block>;
		private static var _inst:MainEdit;
		
		public function MainEdit() {
			blockList = new Vector.<Block>();
			addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(e:Event):void {
			removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseOnMove, false, 0, true);
			stage.addEventListener(MouseEvent.CLICK, mouseOnClick, false, 0, true)
		}
		
		private function mouseOnMove(e:MouseEvent):void {
			
		}
		
		private function mouseOnClick(e:MouseEvent):void {
			
		}
		
		static public function get inst():MainEdit {
			return _inst;
		}
		
		
		
	}

}