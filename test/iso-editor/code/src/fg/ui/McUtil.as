package fg.ui {
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	/**
	 * ...
	 * @author 
	 */
	public class McUtil {
		import flash.display.MovieClip;
		
		public function McUtil() {
			
		}
		
		protected function strDup(str:String, len:int):String {
			var res:String = '';
			var i:int;
			
			for (i = 0; i < len; i++) {
				res += str;
			}
			
			return res;
			
		}
		
		public function traceChild(mc:DisplayObjectContainer, level:int = 0):void { 
			var i:int;
			var disp:DisplayObject;
			var mc1:MovieClip;
			
			trace(strDup('   ', level) + disp);
			
			for (i = 0; i < mc.numChildren; i++) {
				disp = mc.getChildAt(i);
				mc1 = disp as DisplayObjectContainer;
				
				if (mc1) {
					traceChild(mc1, level + 1);
				}
			}
		}		
		
	}

}