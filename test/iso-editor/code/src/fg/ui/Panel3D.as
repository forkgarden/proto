package fg.ui {
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Transform;

	public class Panel3D {
		
		public static const PANEL1:int = 0;
		public static const PANEL2:int = 1;
		public static const PANEL3:int = 2;
		
		protected var _reverse:Boolean = false;
		protected var _mode:int = 0;
		protected var id:String = '';
		protected var _view:Sprite;
		protected var _width:Number = 32;
		protected var _height:Number = 32;
		protected var _pos:Point = new Point();
		
		public function Panel3D() {
			_view = new Sprite();
			refresh();
		}
		
		public function toObject():Object {
			var res:Object = {};
			
			res.x = pos.x;
			res.y = pos.y;
			res.width = _width;
			res.height = _height;
			res.name = "Panel3D";
			res.mode = _mode;
			res.id = id;
			
			return res;
		}
		
		public function fromObject(obj:Object):void {
			_pos.x = obj.x;
			_pos.y = obj.y;
			_width = obj.width;
			_height = obj.height;
			id = obj.id;
			refresh();
		}
		
		public static function create():Panel3D {
			var res:Panel3D;
			
			res = new Panel3D();
			
			return res;
		}
		
		public function setMode(n:Number):Panel3D {
			mode = n;
			refresh();
			return this;
		}
		
		public function destroy():void {
			_view.removeChildren();
			if (_view.parent) {
				_view.parent.removeChild(_view);
			}
			_view = null;
		}
		
		public function refresh():void {
			var clr:uint = 0;
			
			_view.graphics.clear();
			_view.graphics.beginFill(0xc3c3c3, 1);
			_view.graphics.drawRect(0, 0, _width, _height);
			_view.graphics.endFill();
			
			//atas kiri
			if (_mode == PANEL1) {
				clr = 0xffffff;
			}
			else if (mode == PANEL2) {
				clr = 0x7f7f7f;
			}
			else {
				clr = 0xc3c3c3;
			}
			
			_view.graphics.lineStyle(1, clr);
			_view.graphics.moveTo(0, 0);
			_view.graphics.lineTo(_width - 1, 0);
			_view.graphics.moveTo(0, 0);
			_view.graphics.lineTo(0, _height - 1);
			
			//kanan bawah
			if (_mode == PANEL1) {
				clr = 0x7f7f7f;
			}
			else if (mode == PANEL2) {
				clr = 0xffffff;
			}
			else {
				clr = 0xc3c3c3;
			}
			
			_view.graphics.lineStyle(1, clr);
			_view.graphics.moveTo(0, _height-1);
			_view.graphics.lineTo(_width, _height - 1);
			_view.graphics.moveTo(_width - 1, 0);
			_view.graphics.lineTo(_width - 1, _height);
			
			//trace('panel refresh');
			//trace("cwidth/cheight " + _width + '/' + _height + "w/h " + width + '/' + height);
		}
		
		public function get reverse():Boolean {
			return _reverse;
		}
		
		public function set reverse(value:Boolean):void {
			_reverse = value;
			if (!_reverse) _mode = PANEL1 else _mode = PANEL2;
			refresh();
		}
		
		public function get mode():int 
		{
			return _mode;
		}
		
		public function set mode(value:int):void 
		{
			_mode = value;
			refresh();
		}
		
		public function get view():Sprite 
		{
			return _view;
		}
		
		public function set view(value:Sprite):void 
		{
			_view = value;
		}
		
		public function get width():Number 
		{
			return _width;
		}
		
		public function set width(value:Number):void 
		{
			if (value < 3) value = 3;
			_width = value;
			refresh();
		}
		
		public function get height():Number 
		{
			return _height;
		}
		
		public function set height(value:Number):void 
		{
			if (value < 3) value = 3;
			_height = value;
			refresh();
		}
		
		public function get pos():Point 
		{
			return _pos;
		}
		
		public function set pos(value:Point):void 
		{
			_pos = value;
		}
		
	}

}