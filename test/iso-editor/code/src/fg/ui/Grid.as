package fg.ui 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	
	public class Grid extends Sprite
	{
		
		protected var items:Vector.<GridItem>;
		protected var _col:Number = 2;
		protected var _itemWidth:Number = 32;
		protected var _itemHeight:Number = 32;
		protected var _spacing:Number = 4;
		
		public function Grid() 
		{
			items = new Vector.<GridItem>();
			refresh();
		}
		
		public function toObject():Object {
			var res:Object = {};
			
			res.col = _col;
			res.itemWidth = _itemWidth;
			res.itemHeight = _itemHeight;
			res.x = x;
			res.y = y;
			res.name = 'Grid';
			
			return res;
		}
		
		public function fromObject(obj:Object):void {
			_col = obj.col;
			_itemWidth = obj.itemWidth;
			_itemHeight = obj.itemHeight;
			x = obj.x;
			y = obj.y;
			refresh();
		}
		
		public function destroy():void {
			var item:GridItem;
			
			while (items.length > 0) {
				item = items.pop();
				item = items.pop();
				item.destroy();
			}
		}
		
		public function refresh():void {
			var item:GridItem;
			var ix:Number;
			var jx:Number;
			var i:Number = 0;
			
			removeChildren();
			
			for each (item in items) {
				ix = i % _col;
				jx = Math.floor(i / _col);
				item.x = ix * (_itemWidth + spacing);
				item.y = jx * (_itemHeight + spacing);
				item.x += spacing;
				item.y += spacing;
				item.cWidth = _itemWidth;
				item.cHeight = _itemHeight;
				item.border = true;
				item.render();
				addChild(item);
				i++;
			}
			
		}
		
		public function addItem(d:DisplayObject):void {
			var item:GridItem;
			
			item = new GridItem();
			item.addContent(d);
			items.push(item);
			
			refresh();
		}
		
		public function clear():void {
			var item:GridItem;
			
			while (items.length > 0) {
				item = items.pop();
				item.destroy();
			}
			refresh();
		}
		
		public function get col():Number 
		{
			return _col;
		}
		
		public function set col(value:Number):void 
		{
			_col = Math.max(value, 1);
			refresh();
		}
		
		public function get itemWidth():Number 
		{
			return _itemWidth;
		}
		
		public function set itemWidth(value:Number):void 
		{
			if (value < 32) value = 32;
			_itemWidth = value;
			refresh();
		}
		
		public function get itemHeight():Number 
		{
			return _itemHeight;
		}
		
		public function set itemHeight(value:Number):void 
		{
			if (value < 32) value = 32;
			_itemHeight = value;
			refresh();
		}
		
		public function get spacing():Number 
		{
			return _spacing;
		}
		
		public function set spacing(value:Number):void 
		{
			_spacing = value;
		}
		
		
	}
}