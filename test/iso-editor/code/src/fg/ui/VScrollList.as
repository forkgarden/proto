package fg.ui 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class VScrollList extends Sprite
	{
		
		protected var back:Panel3D = new Panel3D();
		protected var _list:VLayout = new VLayout();
		protected var _scroll:VScrollBar = new VScrollBar();
		protected var rect:Rectangle = new Rectangle();
		protected var prev:Point = new Point();
		protected var cWidth:Number = 32;
		protected var cHeight:Number = 32;
		
		
		public function VScrollList() 
		{
			addChild(back);
			addChild(_list);
			addChild(_scroll);
			
			_scroll.mouseMoveCallBack = scrollOnMouseMove;
			
			refresh();
			
			setWidth(200);
			setHeight(200);
			refresh();
		}
		
		public function toObject():Object {
			var res:Object = {};
			
			res.width = cWidth;
			res.height = cHeight;
			res.x = x;
			res.y = y;
			res.name = "VScrollList";
			
			return res;
		}
		
		public function fromObject(obj:Object):void {
			setWidth(obj.width);
			setHeight(obj.height);
			x = obj.x;
			y = obj.y;
			refresh();
		}
		
		public function destroy():void {
			removeChildren();
			
			back.destroy();
			_list.destroy();
			_scroll.destroy();
			
			rect = null;
			prev = null;
			back = null;
			_list = null;
			_scroll = null;
		}
		
		public function addItem(disp:DisplayObject):void {
			_list.addItem(disp);
			refresh();
		}

		
		protected function scrollOnMouseMove():void {
			rect.y = _scroll.percent * (_list.getItemTotalHeight() - rect.height);
			_list.scrollRect = rect;
		}
		
		public function setHeight(value:Number):void {
			if (value < 32) value = 32;
			
			cHeight = value;
			back.setHeight(value);
			_scroll.setHeight(value);
			refresh();
		}
		
		//TODO: width dibuat untuk tiap component
		public function setWidth(value:Number):void {
			if (value < 32) value = 32;
			
			cWidth = value;
			back.setWidth(value - _scroll.width);
			rect.width = back.width - 8;
			refresh();
		}
		
		/*
		public override function set width(value:Number):void {
			
			//throw new Error();
			//trace('set width ' + value);
			
			//back.width = value - scroll.width;
			//rect.width = back.width - 8;
			//refresh();
			
			//trace('scroll width ' + scroll.width);
			//trace('back width ' + back.width);
		}
		*/
		
		public function get list():VLayout 
		{
			return _list;
		}
		
		public function get scroll():VScrollBar 
		{
			return _scroll;
		}
		
		public function clear():void {
			_list.clear();
		}
		
		public function refresh():void {
			_scroll.x = back.width;
			
			rect.height = _scroll.height - 8;
			_list.x = 4;
			_list.y = 4;
			_list.scrollRect = rect;
		}
		
	}

}