package fg.ui {
	import flash.display.Sprite;
	import flash.text.TextFormatAlign;
	
	/**
	 * ...
	 * @author test
	 */
	public class CTLabel {
		
		private var cTombol:CTombol;
		private var teks:Teks;
		private var _width:Number;
		private var _height:Number;
		private var _label:String;
		private var _clickCallBack:Function;
		private var _view:Sprite;
		
		public function CTLabel() {
			cTombol = new CTombol();
			teks = new Teks();
			teks.format.font = 'Arial';
			teks.format.align = TextFormatAlign.CENTER;
			teks.formatApply();
			
			_view = new Sprite();
			_view.addChild(cTombol.view);
			_view.addChild(teks);
			
			width = 100;
			height = 32;
		}
		
		public function get width():Number {
			return _width;
		}
		
		public function set width(value:Number):void {
			_width = value;
			
			cTombol.width = value;
			teks.width = value;
		}
		
		public function get height():Number {
			return _height;
			
		}
		
		public function set height(value:Number):void {
			_height = value;
			cTombol.height = value;
			teks.height = value;
		}
		
		public function get label():String {
			return _label;
		}
		
		public function set label(value:String):void {
			_label = value;
			teks.text = value;
		}
		
		public function get clickCallBack():Function {
			return _clickCallBack;
		}
		
		public function set clickCallBack(value:Function):void {
			_clickCallBack = value;
			cTombol.clickCallBack = value;
		}
		
		public function get view():Sprite {
			return _view;
		}
	
	}

}