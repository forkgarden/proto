package fg.ui {
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	public class CTombol {
		
		private var _panel3D:Panel3D;
		private var _panel3DPencet:Panel3D;
		private var _view:Sprite;
		private var _clickCallBack:Function;
		private var _mouseUpCallBack:Function;
		private var _mouseDownCallBack:Function;
		private var _width:Number;
		private var _height:Number;
		private var _toggle:Boolean = false;
		private var _active:Boolean = false;
		
		public function CTombol() {
			_panel3D = new Panel3D();
			_panel3D.view.mouseEnabled = false;
			_panel3DPencet = new Panel3D();
			_panel3DPencet.setMode(1);
			_panel3DPencet.view.mouseEnabled = false;
			
			_view = new Sprite();
			_view.mouseChildren = false;
			
			_view.addChild(_panel3DPencet.view);
			_view.addChild(_panel3D.view);
			
			_view.addEventListener(MouseEvent.CLICK, panelOnClick);
			_view.addEventListener(MouseEvent.MOUSE_DOWN, panelMouseDown);
			_view.addEventListener(MouseEvent.MOUSE_UP, panelMouseUp);
			
			width = 100;
			height = 32;
		}
		
		private function panelMouseUp(e:MouseEvent):void {
			if (_mouseUpCallBack != null) {
				_mouseUpCallBack(e);
			}
		}
		
		private function panelMouseDown(e:MouseEvent):void {
			if (_mouseDownCallBack != null) {
				_mouseDownCallBack(e);
			}
		}
		
		private function panelOnClick(e:MouseEvent):void {
			if (null != _clickCallBack) {
				_clickCallBack(e);
			}
		}
		
		public function get view():Sprite {
			return _view;
		}
		
		public function get width():Number {
			return _width;
		}
		
		public function set width(value:Number):void {
			_width = value;
			_panel3D.width = value;
			_panel3DPencet.width = value;
		}
		
		public function get height():Number {
			return _height;
		}
		
		public function set height(value:Number):void {
			_height = value;
			_panel3D.height = value;
			_panel3DPencet.height = value;
		}
		
		public function get clickCallBack():Function {
			return _clickCallBack;
		}
		
		public function set clickCallBack(value:Function):void {
			_clickCallBack = value;
		}
		
		public function get toggle():Boolean {
			return _toggle;
		}
		
		public function set toggle(value:Boolean):void {
			_toggle = value;
		}
		
		public function get active():Boolean {
			return _active;
		}
		
		public function get mouseUpCallBack():Function {
			return _mouseUpCallBack;
		}
		
		public function set mouseUpCallBack(value:Function):void {
			_mouseUpCallBack = value;
		}
		
		public function get mouseDownCallBack():Function {
			return _mouseDownCallBack;
		}
		
		public function set mouseDownCallBack(value:Function):void {
			_mouseDownCallBack = value;
		}
		
		public function get panel3D():Panel3D {
			return _panel3D;
		}
		
		public function get panel3DPencet():Panel3D {
			return _panel3DPencet;
		}
		
	}

}