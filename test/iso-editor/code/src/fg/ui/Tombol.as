package fg.ui {
	import flash.display.SimpleButton;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;

	public class Tombol extends Sprite {
		
		protected var tbl:SimpleButton;
		protected var teks:TextField;
		protected var _label:String = '';
		
		protected var mWidth:Number = 100;
		protected var mHeight:Number = 32;
		
		protected var _onClick:Function;
		protected var _id:String;
		protected var _enable:Boolean = true;
		
		public function Tombol() {
			var format:TextFormat;
			
			teks = new TextField();
			teks.mouseEnabled = false;
			teks.mouseWheelEnabled = false;
			teks.doubleClickEnabled = false;
			teks.tabEnabled = false;
			teks.autoSize = TextFieldAutoSize.NONE;
			
			format = teks.getTextFormat();
			format.font = 'Arial';
			format.align = TextFormatAlign.CENTER;
			
			teks.setTextFormat(format);
			teks.defaultTextFormat = format;
			
			addChild(teks);
			
			refresh();
		}
		
		protected function viewOnMouseDown(e:MouseEvent):void {}
		
		public static function create():Tombol {
			return new Tombol();
		}
		
		public function destroy():void {
			_onClick = null;
			//downState.destroy();
			//upState.destroy();
			removeChildren();
			
			teks = null;
			tbl = null;
		}
		
		protected function viewOnClick(e:MouseEvent):void {
			if (null != _onClick) _onClick(this);
		}
		
		protected function stateCreate(mode:int):Panel3D {
			var panel:Panel3D;
			
			panel = new Panel3D();
			panel.width = mWidth;
			panel.height = mHeight;
			panel.mode = mode;
			
			return panel;
		}
		
		protected function refresh():void {
			removeChildren();
			
			var upState:Panel3D = stateCreate(Panel3D.PANEL1);
			var downState:Panel3D = stateCreate(Panel3D.PANEL2);
			var overState:Panel3D = stateCreate(Panel3D.PANEL1);
			var hitState:Panel3D = stateCreate(Panel3D.PANEL2);
			
			tbl = new SimpleButton(upState.view, overState.view, downState.view, hitState.view);
			addChild(tbl);
			tbl.addEventListener(MouseEvent.CLICK, viewOnClick);
			tbl.addEventListener(MouseEvent.MOUSE_DOWN, viewOnMouseDown);
			
			teks.x = 4;
			teks.y = 4;
			teks.text = _label;
			teks.width = mWidth - 8;
			teks.height = teks.textHeight + 4;
			teks.y = mHeight / 2 - teks.height / 2;
			addChild(teks);
		}
		
		public function setHeight(n:Number):void {
			mHeight = Math.max(n, 32);
			refresh();
		}
		
		public function setWidth(n:Number):void {
			mWidth = Math.max(n, 32);
			refresh();
		}
		
		public function get label():String {
			return _label;
		}
		
		public function setLabel(str:String):void {
			_label = str;
			refresh();
		}
		
		public function set label(value:String):void {
			_label = value;
			refresh();
		}
		
		public function get onClick():Function 
		{
			return _onClick;
		}
		
		public function set onClick(value:Function):void 
		{
			_onClick = value;
		}
		
		public function get id():String 
		{
			return _id;
		}
		
		public function set id(value:String):void 
		{
			_id = value;
		}
		
		public function get enable():Boolean 
		{
			return _enable;
		}
		
		public function set enable(value:Boolean):void 
		{
			_enable = value;
			
			if (_enable) {
				teks.textColor = 0;
				mouseEnabled = true;
			}
			else {
				teks.textColor = 0xeeeeee;
				mouseEnabled = false;
			}
		}
		
	}

}