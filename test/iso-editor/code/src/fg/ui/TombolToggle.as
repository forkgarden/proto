package fg.ui {
	import flash.display.Sprite;
	import flash.events.MouseEvent;

	public class TombolToggle {
		
		private var tombol:CTombol;
		private var _pencet:Boolean;
		private var _clickCallBack:Function;
		private var _view:Sprite;
		
		public function TombolToggle() {
			tombol = new CTombol();
			tombol.mouseDownCallBack = mouseDown;
			tombol.mouseUpCallBack = mouseUp;
			tombol.clickCallBack = mouseClick;
			_pencet = false;
			_view = tombol.view;
		}
		
		private function mouseClick(e:MouseEvent):void {
			if (_clickCallBack != null) {
				_clickCallBack(e);
			}
		}
		
		private function mouseDown(e:MouseEvent):void {
			tombol.panel3D.view.visible = false;
		}
		
		private function updatePencet():void {
			if (_pencet) {
				tombol.panel3D.view.visible = false;
			}
			else {
				tombol.panel3D.view.visible = true;
			}
			
			trace(_pencet);			
		}
		
		private function mouseUp(e:MouseEvent):void {
			_pencet = !_pencet;
			updatePencet();
		}
		
		public function get clickCallBack():Function {
			return _clickCallBack;
		}
		
		public function set clickCallBack(value:Function):void {
			_clickCallBack = value;
		}
		
		public function get width():Number {
			return tombol.width;
		}
		
		public function set width(value:Number):void {
			tombol.width = value;
		}
		
		public function get height():Number {
			return tombol.height;
		}
		
		public function set height(value:Number):void {
			tombol.height = value;
		}		
		
		public function get pencet():Boolean {
			return _pencet;
		}
		
		public function get view():Sprite {
			return _view;
		}
		
		public function set pencet(value:Boolean):void {
			_pencet = value;
			updatePencet();
		}
		
	}

}