package fg.ui 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class VScrollBarCtrl extends Sprite
	{
		protected var tombol:Panel3D = new Panel3D();
		protected var bar:Panel3D = new Panel3D();
		protected var tombolPos:Point = new Point();
		protected var mousePos:Point = new Point();
		protected var _percent:Number = 0;
		protected var _mouseMoveCallBack:Function;
		
		public function VScrollBarCtrl() 
		{
			bar.reverse = true;
			tombol.addEventListener(MouseEvent.MOUSE_DOWN, tombolOnMouseDown, false, 0, true);
			
			tombol.width = 10;
			tombol.height = 10;
			bar.width = 5;
			bar.height = 100;
			
			addChild(bar);
			addChild(tombol);
			
			width = 10;
		}
		
		protected function tombolOnMouseDown(evt:MouseEvent):void {
			evt.stopPropagation();
			
			tombol.stage.addEventListener(MouseEvent.MOUSE_MOVE, tombolOnMouseMove, false, 0, true);
			tombol.stage.addEventListener(MouseEvent.MOUSE_UP, tombolOnMouseUp, false, 0, true);
			
			tombolPos.y = tombol.y;
			mousePos.y = evt.stageY;
		}
		
		protected function tombolOnMouseMove(evt:MouseEvent):void {
			evt.stopPropagation();
			
			tombol.y = tombolPos.y + (evt.stageY - mousePos.y);
			
			if (tombol.y > (bar.height - tombol.height)) {
				tombol.y = (bar.height - tombol.height);
			}
			
			if (tombol.y < 0) tombol.y = 0;
			
			_percent = (tombol.y / (bar.height - tombol.height));
			if (_mouseMoveCallBack) _mouseMoveCallBack();			
		}
		
		protected function tombolOnMouseUp(evt:MouseEvent):void {
			evt.stopPropagation();
			
			tombol.stage.removeEventListener(MouseEvent.MOUSE_MOVE, tombolOnMouseMove);
			tombol.stage.removeEventListener(MouseEvent.MOUSE_UP, tombolOnMouseUp);
		}
		
		public override function set height(value:Number):void {
			bar.height = value;
		}
		
		public override function set width(value:Number):void {
			tombol.width = value;
			bar.width = value;
		}
		
		public function get percent():Number 
		{
			return _percent;
		}
		
		public function set percent(value:Number):void 
		{
			_percent = value;
		}
		
		public function get mouseMoveCallBack():Function 
		{
			return _mouseMoveCallBack;
		}
		
		public function set mouseMoveCallBack(value:Function):void 
		{
			_mouseMoveCallBack = value;
		}
	}

}