package fg.ui {
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;

	public class VLayout extends BaseLayout {
		
		public function VLayout() {
			super();
		}
		
		public override function refresh():void {
			var d:DisplayObject;
			var posY:int = 0;
			
			removeChildren();
			
			for each (d in items) {
				if (d.visible) {
					d.y = posY;
					addChild(d);
					
					if (d is VLayout) {
						(d as VLayout).refresh();;
					}
					
					posY += d.height;
				}
			}
			
			//flAddChildSafe = false;
		}

	}
}