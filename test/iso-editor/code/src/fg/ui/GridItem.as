package fg.ui 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author test
	 */
	public class GridItem extends Sprite
	{
		private var _cWidth:Number = 32;
		private var _cHeight:Number = 32;
		private var _content:DisplayObject;
		private var _border:Boolean = false;
		private var panel:Panel3D;
		
		public function GridItem() 
		{
			panel = new Panel3D();
			panel.mode = Panel3D.PANEL2;
		}
		
		public function destroy():void {
			panel.destroy();
			removeChildren();
			_content = null;
			panel = null;
		}
		
		public function addContent(d:DisplayObject):void {
			_content = d;
			render();
		}
		
		public function removeContent(d:DisplayObject):void {
			_content = null;
			render();
		}
		
		public function render():void {
			removeChildren();
			if (_content) {
				addChild(_content);
			}
			else {
				panel.setWidth(_cWidth);
				panel.setHeight(_cHeight);
				panel.refresh();
				addChild(panel);
			}
			
			if (_border) {
				graphics.lineStyle(1);
				graphics.drawRect(0, 0, _cWidth, _cHeight);
			}
		}
		
		public function get cWidth():Number 
		{
			return _cWidth;
		}
		
		public function set cWidth(value:Number):void 
		{
			_cWidth = value;
			render();
		}
		
		public function get cHeight():Number 
		{
			return _cHeight;
		}
		
		public function set cHeight(value:Number):void 
		{
			_cHeight = value;
			render();
		}
		
		public function get content():DisplayObject 
		{
			return _content;
		}
		
		public function get border():Boolean 
		{
			return _border;
		}
		
		public function set border(value:Boolean):void 
		{
			_border = value;
		}
		
		
	}

}