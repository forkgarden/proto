package fg.ui 
{
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	public class HScrollList extends Sprite
	{
		protected var back:Panel3D = new Panel3D();
		protected var list:HLayout = new HLayout();
		protected var scroll:HScrollBar = new HScrollBar();
		protected var rect:Rectangle = new Rectangle();
		
		public function HScrollList() 
		{
			addChild(back);
			addChild(list);
			addChild(scroll);
			
			scroll.mouseMoveCallBack = scrollOnMouseMove;
			
			refresh();
			
			width = 200;
			height = 200;
		}
		
		public function addItem(disp:DisplayObject):void {
			list.addItem(disp);
			refresh();
		}

		protected function scrollOnMouseMove():void {
			rect.x = scroll.percent * (list.getItemTotalHeight() - rect.width);
			list.scrollRect = rect;
		}
		
		public override function set height(value:Number):void {
			back.height = value - scroll.height;
			refresh();
		}
		
		public override function set width(value:Number):void {
			back.width = value;
			scroll.width = value;
			refresh();
		}
		
		public function refresh():void {
			scroll.x = 0;
			scroll.y = back.height;
			
			rect.x = 0;
			rect.y = 0;
			rect.width = back.width;
			rect.height = back.height;
			
			list.scrollRect = rect;
		}
		
	}

}