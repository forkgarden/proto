package fg.ui 
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	
	public class VScrollBar extends Sprite
	{
		protected var tombol:Panel3D = new Panel3D();
		protected var bar:Panel3D = new Panel3D();
		protected var tombolPos:Point = new Point();
		protected var mousePos:Point = new Point();
		protected var _percent:Number = 0;
		protected var _mouseMoveCallBack:Function;
		
		public function VScrollBar() 
		{
			bar.reverse = true;
			tombol.addEventListener(MouseEvent.MOUSE_DOWN, tombolOnMouseDown, false, 0, true);
			
			tombol.setWidth(10);
			tombol.setHeight(10);
			bar.setWidth(5);
			bar.setHeight(100);
			
			addChild(bar);
			addChild(tombol);
			
			setWidth(10);
		}
		
		public function destroy():void {
			removeChildren();
			
			tombol.destroy();
			bar.destroy();
			_mouseMoveCallBack = null;
			
			tombol = null;
			bar = null;
			mousePos = null;
			tombolPos = null;
		}
		
		protected function tombolOnMouseDown(evt:MouseEvent):void {
			evt.stopPropagation();
			
			tombol.stage.addEventListener(MouseEvent.MOUSE_MOVE, tombolOnMouseMove, false, 0, true);
			tombol.stage.addEventListener(MouseEvent.MOUSE_UP, tombolOnMouseUp, false, 0, true);
			
			tombolPos.y = tombol.y;
			mousePos.y = evt.stageY;
		}
		
		protected function tombolOnMouseMove(evt:MouseEvent):void {
			evt.stopPropagation();
			
			tombol.y = tombolPos.y + (evt.stageY - mousePos.y);
			
			if (tombol.y > (bar.height - tombol.height)) {
				tombol.y = (bar.height - tombol.height);
			}
			
			if (tombol.y < 0) tombol.y = 0;
			
			_percent = (tombol.y / (bar.height - tombol.height));
			if (_mouseMoveCallBack != null) _mouseMoveCallBack();			
		}
		
		protected function tombolOnMouseUp(evt:MouseEvent):void {
			evt.stopPropagation();
			
			tombol.stage.removeEventListener(MouseEvent.MOUSE_MOVE, tombolOnMouseMove);
			tombol.stage.removeEventListener(MouseEvent.MOUSE_UP, tombolOnMouseUp);
		}
		
		public function setHeight(value:Number):void {
			bar.setHeight(value);
		}
		
		public function setWidth(value:Number):void {
			tombol.setWidth(value);
			bar.setWidth(value);
		}
		
		public function get percent():Number 
		{
			return _percent;
		}
		
		public function set percent(value:Number):void 
		{
			_percent = value;
		}
		
		public function get mouseMoveCallBack():Function 
		{
			return _mouseMoveCallBack;
		}
		
		public function set mouseMoveCallBack(value:Function):void 
		{
			_mouseMoveCallBack = value;
		}
	}

}