package fg.isometrics {
	import flash.geom.Point;
	
	public class Iso {
		
		//public static const OFFSET_X:int = 224;
		
		public function Iso() {
		}
		
		public function screen2IsoX(sx:Number, sy:Number):Number {
			return (sx + 2 * sy) / 2;
		}
		
		public function screen2IsoY(sx:Number, sy:Number):Number {
			return (2 * sy - sx) / 2;
		}
		
		public function isoProjectX(isoX:Number, isoY:Number):Number {
			return (isoX - isoY);			
		}
		
		public function isoProjectZ(isoX:Number, isoY:Number):Number {
			return (isoX + isoY) / 2;
		}
		
		
	}
}