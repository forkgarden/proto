package{
	import fg.debugs.Debugger;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import iso3.Block;
	import iso3.BlockData;
	import iso3.BlockManager;
	import iso3.Bmp;
	import tests.MainTest;

	[Frame(factoryClass="Preloader")]
	public class Main extends Sprite {
		
		public static const ISO_OFFSET:int = 400;
		public static const ISO_OFFSETY:int = 300;
		public static const TILE_SIZE:int = 32;
		
		private var blockList:Vector.<Block>;
		private var blockManager:BlockManager;
		private var back:Bitmap;
		private var blockData:BlockData;
		
		private static var _inst:Main;
		
		private var _bmp:Bmp;
		private var _moveCallBack:Function;
		private var _clickCallBack:Function;

		public function Main() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
			
			_inst = this;
		}
		
		private function setBack():void {
			back = new Bitmap(_bmp.back.bitmapData);
			back.x = 400 - back.width / 2;
			back.y = 300 - back.height / 2;
			addChild(back);
			setChildIndex(back, 0);			
		}
		
		public function destroy():void {
			removeEventListener(Event.ENTER_FRAME, update);
			removeChildren();
		}

		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			stage.quality = StageQuality.LOW;
			
			_bmp = new Bmp();
			//blockList = new Vector.<Block>();
			
			//new MainTest(this);
			//return;
			
			setBack();
			
			blockData = new BlockData();
			blockManager = new BlockManager();
			blockManager.blockData = blockData;
			blockManager.cont = this;
			blockManager.init();
			
			addChild(Debugger.getInst());
			
			stage.addEventListener(MouseEvent.CLICK, mClick);
			stage.addEventListener(MouseEvent.MOUSE_MOVE, mMove, false, 0, true);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, keyboardOnClick);
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		private function keyboardOnClick(e:KeyboardEvent):void {
			trace("key down " + e.keyCode);
			blockManager.rotateRight();
			
		}
		
		private function update(e:Event):void {
			blockManager.update();
		}
		
		private function mMove(e:MouseEvent):void {
			if (_moveCallBack != null) {
				_moveCallBack(e);
				return
			}
		}
		
		private function mClick(e:MouseEvent):void {
			if (_clickCallBack != null) {
				_clickCallBack(e);
				return;
			}
			
			blockManager.onClick(e.stageX, e.stageY);
			
			
		}
		
		static public function get inst():Main {
			return _inst;
		}
		
		public function get bmp():Bmp {
			return _bmp;
		}
		
		public function get moveCallBack():Function {
			return _moveCallBack;
		}
		
		public function set moveCallBack(value:Function):void {
			_moveCallBack = value;
		}
		
		public function get clickCallBack():Function {
			return _clickCallBack;
		}
		
		public function set clickCallBack(value:Function):void {
			_clickCallBack = value;
		}

	}

}