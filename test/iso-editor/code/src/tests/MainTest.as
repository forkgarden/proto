package tests {
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import tests.manual.BoxClickOnSide;
	import tests.manual.BoxHover;
	import tests.manual.RenderBlock;
	import tests.manual.BoxHover2;
	import tests.manual.RenderIso;
	import tests.manual.RenderLine;
	import tests.manual.RenderWhite;
	import tests.manual.RotateRight;
	/**
	 * ...
	 * @author test
	 */
	public class MainTest extends Sprite {
		
		private var test:BaseTest;
		
		public function MainTest(cont:DisplayObjectContainer) {
			new TestLine();
			new TestPolygon();
			new TestBlock(cont);
			
			//render
			//new RotateRight(cont);
			//new BoxClickOnSide();
			//test = new BoxHover(cont);
			//new BoxHover2(cont);
			//new RenderBlock();
			//new RenderWhite();
			//new BlockRenderIso();
			//new RenderLine();
			//test = new RenderIso(cont);
			
			//debug
			//(new TestBlock()).debugDotInside2();
		}
		
		public function update():void {
			if (test) test.update();
		}
		
	}

}