package tests.manual {
	/**
	 * ...
	 * @author test
	 */
	import iso3.Block;
	 
	public class BlockUtil {
		
		public function BlockUtil() {
			
		}
		
		public function blockCreate(x:int, y:int, z:int):Block {
			var block:Block;
			
			block = Block.create();
			block.x = x;
			block.y = y;
			block.z = z;
			block.renderIso(Main.ISO_OFFSET);
			
			return block;
		}
		
	}

}