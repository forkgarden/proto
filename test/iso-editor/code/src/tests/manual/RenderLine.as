package tests.manual {
	import iso3.Block;
	
	/**
	 * ...
	 * @author test
	 */
	
	public class RenderLine {
		
		public function RenderLine() {
			var block:Block;
			
			block = new Block();
			block.x = 100;
			block.y = 100;
			block.debugSetViewPos(400, 0);
			block.renderClear();
			block.renderLine();
			block.addToStage(Test.inst);
		}
		
	}

}