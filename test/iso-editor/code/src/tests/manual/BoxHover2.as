package tests.manual {
	import fg.debugs.Debugger;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import iso3.Block;
	/**
	 * ...
	 * @author test
	 */
	public class BoxHover2 {
		
		private var block1:Block;
		private var block2:Block;
		private var cont:Sprite;
		
		public function BoxHover2(cont:DisplayObjectContainer) {
			
			block1 = blockCreate(100, 100);
			block1.addToStage(cont);
			
			block2 = blockCreate(120, 100);
			block2.addToStage(cont);
			
			cont.stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseOnMove);
		}
		
		private function blockCreate(x:Number, y:Number):Block {
			var block:Block;
			
			block = new Block();
			block.renderClear();
			block.renderLine();
			block.debugSetViewPos(x, y);
			
			return block
		}
		
		public function mouseOnMove(e:MouseEvent):void {	
			block1.renderClear();
			block1.renderLine();
			
			block2.renderClear();
			block2.renderLine();
				
			if (block2.debugOnHover(e.stageX,e.stageY) == -1) {
				block1.debugOnHover(e.stageX, e.stageY);
			}
			
			//Debugger.getInst().addLine("px " + px + "/py " + py + "/p " + idx,  true);
		}
		 
		
	}

}