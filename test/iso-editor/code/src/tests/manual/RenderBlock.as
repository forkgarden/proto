package tests.manual {
	import flash.display.Sprite;
	import iso3.Block;
	/**
	 * ...
	 * @author test
	 */
	public class RenderBlock extends Sprite {
		
		public function RenderBlock() {
			test();
		}
		
		public function test():void {
			var block:Block;
			
			block = new Block();
			block.x = 100;
			block.y = 100;
			block.debugSetViewPos(100, 100);
			block.renderClear();
			block.renderPolygon(0);
			block.renderPolygon(1);
			block.renderPolygon(2);
			block.renderLine();
			block.addToStage(Test.inst);
		}
		
	}

}