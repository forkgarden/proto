package tests.manual {
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import iso3.Block;
	import iso3.BlockData;
	import fg.debugs.Debugger;
	/**
	 * ...
	 * @author test
	 */
	public class BugSort extends Sprite {
		
		private var util:BlockUtil
		private var _blockData:BlockData
		
		public function BugSort(cont:DisplayObjectContainer) {
			var block:Block;
			
			util = new BlockUtil();
			_blockData = new BlockData();
			
			block = util.blockCreate(0, 0, 0);
			block.renderIso(Main.ISO_OFFSET, Main.ISO_OFFSETY);
			_blockData.add(block);
			
			addEventListener(Event.ENTER_FRAME, update);
		}
		
		private function hoverBlockReset():void {
			if (hoverBlock) {
				hoverBlock.renderWhite();
			}
			hoverBlock = null;
			hoverSide = -1;
		}
		
		public function onHover(block:Block, x:int, y:int):void {
			var polygonIdx:int;
			
			polygonIdx = block.getPolygonHoverIdxAbs(x, y);
			
			if (polygonIdx > -1) {
				if (hoverBlock) {
					if (block.didepan(hoverBlock)) {
						hoverBlock = block;
						hoverSide = polygonIdx;
					}
				}
				else {
					hoverBlock = block;
					hoverSide = polygonIdx;
				}
			}
		}		
		
		private function update(e:Event):void {
			var i:int;
			var len:int;
			
			hoverBlockReset();
			
			len = _blockData.length();
			for (i = 0; i < len; i++) {
				onHover(_blockData.getByIdx(i), _cont.mouseX, _cont.mouseY);
				_blockData.sort(i);			
			}
			
			if (hoverBlock) {
				hoverBlock.renderSelected(hoverSide);
			}
			
			debug();			
		}
		
		public function debug():void {
			var debugBlock:Block;
			var localX:Number;
			var localY:Number;
			
			debugBlock = _blockData.getByIdx(0);
			localX = debugBlock.getLocalX(_cont.mouseX);
			localY = debugBlock.getLocalY(_cont.mouseY);
			
			Debugger.getInst().addLine("hover block " + hoverBlock, true);
			Debugger.getInst().addLine("hover side " + hoverSide);
			Debugger.getInst().addLine("pos " + _cont.mouseX + "/" + _cont.mouseY);
			Debugger.getInst().addLine("local " + localX + "/" + localY);
			Debugger.getInst().addLine("cross " + debugBlock.getPolygon(0).dotInside(localX, localY));			
		}		
		
	}

}