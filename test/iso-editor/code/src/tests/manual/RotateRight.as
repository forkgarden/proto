package tests.manual {
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	import iso3.Block;
	/**
	 * ...
	 * @author test
	 */
	public class RotateRight extends Sprite {
		
		private var block:Block;
		
		public function RotateRight(cont:DisplayObjectContainer) {
			//addEventListener(Event.ADDED_TO_STAGE, addedToStage);
			addedToStage(cont);
		}
		
		private function addedToStage(cont:DisplayObjectContainer):void {
			//cont.y = 0;
			block = Block.create();
			block.setPos(2, 0, 0);
			block.renderIso(400, 300);
			block.addToStage(cont);
			
			cont.stage.addEventListener(KeyboardEvent.KEY_UP, keyOnUp);
		}
		
		private function keyOnUp(e:KeyboardEvent):void {
			block.rotateRight();
			block.renderIso(400, 300);
		}
		
	}

}