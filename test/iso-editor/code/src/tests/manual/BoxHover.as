package tests.manual {
	import fg.debugs.Debugger;
	import flash.display.DisplayObjectContainer;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import iso3.Block;
	import tests.BaseTest;
	/**
	 * ...
	 * @author test
	 */
	public class BoxHover extends BaseTest {
		
		private var block:Block;
		
		public function BoxHover(cont:DisplayObjectContainer) {
			this.cont = cont;
			
			block = new Block();
			block.renderClear();
			block.renderLine();
			block.debugSetViewPos(100, 100);
			block.addToStage(cont);
		}
		
		public override function update():void {
			var idx:int;
			
			idx = block.getPolygonHoverIdxAbs(cont.mouseX, cont.mouseY);
			if (idx > -1) {
				block.onHover(idx);
			}
		}
		
		//public function mouseOnMove(e:MouseEvent):void {			
			//block.debugOnHover(e);
			//
			////Debugger.getInst().addLine("px " + px + "/py " + py + "/p " + idx,  true);
		//}
		 //
		
	}

}