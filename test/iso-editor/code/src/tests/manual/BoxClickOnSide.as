package tests.manual {
	import fg.debugs.Debugger;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import iso3.Block;
	
	/**
	 * ...
	 * block click on side, show clicked side
	 */
	public class BoxClickOnSide {
		
		private var block:Block;
		private var cont:Sprite;
		
		public function BoxClickOnSide() {
			cont = Test.inst;
			
			block = new Block();
			block.renderLine();
			block.debugSetViewPos(100, 100);
			block.addToStage(Test.inst);
			Test.inst.clickCallBack = mouseOnClick;
		}
		
		public function mouseOnClick(e:MouseEvent):void {
			var idx:int;
			
			idx = block.getPolygonHoverIdxAbs(e.stageX, e.stageY);
			
			block.renderClear();
			if (idx > -1) {
				block.renderPolygon(idx);
			}
			block.renderLine();
		}
		
	}

}