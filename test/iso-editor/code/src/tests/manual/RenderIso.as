package tests.manual {
	import flash.display.DisplayObjectContainer;
	import iso3.Block;
	/**
	 * ...
	 * @author test
	 */
	public class RenderIso {
		
		private var cont:DisplayObjectContainer;
		
		public function RenderIso(cont:DisplayObjectContainer) {
			var block:Block;
			
			this.cont = cont;
			
			blockCreate(4, 0, 3);
			blockCreate(4, 0, 4);
			blockCreate(4, 1, 4);
			blockCreate(5, 0, 4);
			blockCreate(4, 0, 5);
		}
		
		private function blockCreate(x:int, y:int, z:int):Block {
			var block:Block;
			
			block = Block.create();
			block.x = x;
			block.y = y;
			block.z = z;
			block.renderIso(Main.ISO_OFFSET);
			block.addToStage(cont);	
			
			return block;
		}
		
	}

}