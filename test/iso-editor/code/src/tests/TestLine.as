package tests {
	import flash.geom.Point;
	import iso3.Line;
	import iso3.Util;
	/**
	 * ...
	 * @author test
	 */
	public class TestLine extends BaseTest {
		
		private var line:Line;
		
		public function TestLine() {
			line = new Line();
			line.p1.y = 10;
			line.p1.x = 10;
			line.p2.x = 100;
			line.p2.y = 100;
			
			test();
		}
		
		public function lineCreateXY(x1:int, y1:int, x2:int, y2:int):Line {
			return lineCreate(new Point(x1, y1), new Point(x2, y2));
		}
		
		public function lineCreate2(x:int,y:int):Line {
			return lineCreateXY(x, x, y, y);
		}
		
		public function lineCreate(p1:Point, p2:Point):Line {
			var line:Line;
			
			line = new Line();
			line.p1.x = p1.x;
			line.p1.y = p1.y;
			
			line.p2.x = p2.x;
			line.p2.y = p2.y;
			
			return line;
		}
		
		public override function test():void {
			//testCross3();
			hor();
			ver();
			testInRange();
			testRight();
			testLeft();
			testCrossLurus();
			testCross();
			testCross2();
			testLineConnected();
			testMinMax();
			testCrossOnDot();
			
			destroy();
		}
		
		public function testInRange():void {
			var line:Line;
			
			line = lineCreateXY(10, 10, 100, 100);
			assert(line.inRangeX(11, 11));
			assertFalse(line.inRangeX(9, 9));
			assert(line.inRangeX(10, 10));
			assert(line.inRangeX(100, 100));
			assertFalse(line.inRangeX(110, 110));
			
			assert(line.inRangeY(11, 11));
			assertFalse(line.inRangeY(10, 10));
			assertFalse(line.inRangeY(100, 100));
			assertFalse(line.inRangeY(110, 110));
			
			line.destroy();
		}
		
		public function hor():void {
			var line:Line;
			
			line = lineCreateXY(10, 10, 50, 10);
			assert(line.isHor());
			line.destroy();
			
			line = lineCreateXY(10, 10, 50, 50);
			assert(line.isHor() == false);
			line.destroy();
			
			line = lineCreateXY(10, 10, 10, 40);
			assertFalse(line.isHor());
			line.destroy();
		}
		
		public function ver():void {
			var line:Line;
			
			
			line = lineCreateXY(10, 10, 10, 40);
			assert(line.isVer());
			line.destroy();
			
			line = lineCreateXY(10, 10, 50, 50);
			assertFalse(line.isVer());
			line.destroy();
			
			line = lineCreateXY(10, 10, 50, 10);
			assertFalse(line.isVer());
			line.destroy();
		}
		
		public function testRight():void {
			var line:Line;
			
			line = lineCreateXY(10, 10, 50, 50);
			assert(line.rightSide());
			assertFalse(line.leftSide());
			line.destroy();
			
			line = lineCreateXY(10, -10, 50, -50);
			assert(line.rightSide());
			assertFalse(line.leftSide());
			line.destroy();
			
			line = lineCreateXY(10, 50, 50, 10);
			assert(line.rightSide());
			assertFalse(line.leftSide());
			line.destroy();
		}
		
		public function testLeft():void {
			var line:Line;
			
			//normal atas
			line = lineCreateXY(0, 0, -50, 50);
			assert(line.leftSide());
			assertFalse(line.rightSide());
			line.destroy();
			
			//normal bawah
			line = lineCreateXY(0, 0, -50, -50);
			assert(line.leftSide());
			assertFalse(line.rightSide());
			line.destroy();
			
			//positif left atas
			line = lineCreateXY(50, 10, 10, 50);
			assert(line.leftSide());
			assertFalse(line.rightSide());
			line.destroy();
			
			//positif left bawah
			line = lineCreateXY(50, 10, 10, -50);
			assert(line.leftSide());
			assertFalse(line.rightSide());
			line.destroy();
		}
		
		public function testMinMax():void {
			var line:Line;
			
			line = lineCreate(new Point(10, 10), new Point(100, 100));
			assert(line.minY() == 10);
			assert(line.maxY() == 100);
			
			line.destroy();
		}
		
		public function testLineConnected():void {
			var line1:Line;
			var line2:Line;
			
			line1 = lineCreate(new Point(10, 10), new Point(20, 20));
			line2 = lineCreate(new Point(20, 20), new Point(30, 30));
			assert(line2.isConnectedNext(line1));
			
			line1.destroy();
			line2.destroy();
		}
		
		public function testCrossOnDot():void {
			var line:Line;
			
			line = lineCreate(new Point(10, 10), new Point(50, 50));
			assert(line.crossOnDotHor(10, 10));
			assert(line.crossOnDotHor(50, 50));
			assert(line.crossOnDotHor(100, 10));
			assert(line.crossOnDotHor(100, 50));
			
			//negative
			assert(line.crossOnDotHor(10, 20) == false);
			
			line.destroy();
		}
		
		public function testCrossLurus():void {
			var line:Line;
			
			//ver normal atas
			line = lineCreateXY(0, 0, 0, 50);
			assert(line.crossLineHorRight( -10, 10));
			assertFalse(line.crossLineHorRight(10, 10));
			assertFalse(line.crossLineHorRight(10, 60));
			assertFalse(line.crossLineHorRight(0, 0));
			assertFalse(line.crossLineHorRight(0, 50));
			line.destroy();
			
			//hor normal kanan
			line = lineCreateXY(0, 0, 50, 0);
			assertFalse(line.crossLineHorRight(20, 0));
			assertFalse(line.crossLineHorRight(0, 0));
			assertFalse(line.crossLineHorRight(50, 0));
			assertFalse(line.crossLineHorRight(20, 10));
			line.destroy();
			
			//hor normal bawah
			line = lineCreateXY(0, 0, 0, -50);
			assert(line.crossLineHorRight( -10, -20));
			assertFalse(line.crossLineHorRight(10, -20));
			assertFalse(line.crossLineHorRight(0, -20));
			assertFalse(line.crossLineHorRight(0, 0));
			line.destroy();
			
			//hor normal kiri
			line = lineCreateXY(0, 0, -50, 0);
			assertFalse(line.crossLineHorRight(0,0));
			assertFalse(line.crossLineHorRight(-20,0));
			assertFalse(line.crossLineHorRight(-20,-20));
			assertFalse(line.crossLineHorRight(20,20));
			assertFalse(line.crossLineHorRight( -20, 20));
			line.destroy();
		}
		
		public function testCross2():void {
			var line:Line;
			
			line = lineCreate2(0, 100);
			assert(line.crossLineHorRight(10, 20));
			assertFalse(line.crossLineHorRight(10, 10));
			assertFalse(line.crossLineHorRight(30, 10));
			line.destroy();
			
			line = lineCreate2(0, -100);
			assert(line.crossLineHorRight(-10, -5));
			line.destroy();
			
			line = lineCreateXY(0, 0, -100, 100);
			assert(line.crossLineHorRight( -20, 10));
			assertFalse(line.crossLineHorRight( -20, 30));
			line.destroy();
			
			line = lineCreateXY(0, 0, 100, -100);
			assert(line.crossLineHorRight(20, -30));
			assertFalse(line.crossLineHorRight(20, -10));
			line.destroy();
		}
		
		public function testCross3():void {
			var line:Line;
			
			line = lineCreate2(0, -100);
			(line.debugCross( -10, -20));
			
			line.destroy();
			
		}
		
		public function testCross():void {
			var line:Line;
			
			line = lineCreate(new Point(10, 10), new Point(50, 50));
			
			assert(line.crossLineHorRight(10, 20));
			assertFalse(line.crossLineHorRight(10, 10));
			assertFalse(line.crossLineHorRight(10, 50));
			
			//negative
			assertFalse(line.crossLineHorRight(10, 5));
			assertFalse(line.crossLineHorRight(10, 60));
			
			line.destroy();
		}
		
		public function destroy():void {
			if (line) line.destroy();
		}
		
	}

}