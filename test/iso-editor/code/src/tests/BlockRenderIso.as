package tests {
	import iso3.Block;
	/**
	 * ...
	 * @author test
	 */
	public class BlockRenderIso {
		
		public function BlockRenderIso() {
			var i:int;
			var j:int;
			var block:Block;
			
			for (i = 0; i < 10; i++) {
				for (j = 0; j < 1; j++) {
					block = Block.create();
					block.x = i * 32;
					block.y = j * 32;
					block.renderIso(Test.ISO_OFFSET);
					block.addToStage(Test.inst);
				}
			}
			
		}
		
	}

}