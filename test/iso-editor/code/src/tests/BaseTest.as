package tests {
	import flash.display.DisplayObjectContainer;
	import flash.external.ExternalInterface;
	/**
	 * ...
	 * @author test
	 */
	public class BaseTest {
		
		protected var cont:DisplayObjectContainer
		
		public function BaseTest() {
			
		}
		
		public function test():void {
			
		}
		
		public function debug(msg:String = ""):void {
			try {
				throw new Error(msg);
			}
			catch (e:Error) {
				trace(e.message);
				trace(e.getStackTrace());
				trace("");
				throw new Error();
			}
		}
		
		public function assertFalse(b:Boolean, msg:String = ""):void {
			if (b) {
				debug(msg);
				throw new Error();
			}
		}
		
		public function assert(b:Boolean, msg:String = ""):void {
			if (!b) {
				debug(msg);
				throw new Error();
			}
		}
		
		public function update():void {
			
		}
		
	}

}