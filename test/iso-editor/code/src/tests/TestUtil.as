package tests {
	import flash.geom.Point;
	import iso3.Util;
	/**
	 * ...
	 * @author test
	 */
	public class TestUtil extends BaseTest{
		
		public function TestUtil() {
			//assert(Util.pEqual(new Point(12, 12), new Point(12, 12)), "point tidak sama");
			//assert(Util.pSejajarHor(new Point(10, 10), new Point(20, 10)), "point tidak sejajar");
			//assert(Util.pSejajarVer(new Point(10, 10), new Point(10, 20)), "point tidak sejajar ver");
		}
		
		public override function test():void {
			var p1:Point;
			var p2:Point;
			
			p1 = new Point(12, 12);
			p2 = new Point(12, 12);
			assert(Util.pEqual(p1, p2), "point tidak sama " + p1 + "/" + p2);
			
			p1 = new Point(10, 10);
			p2 = new Point(20, 10);
			assert(Util.pSejajarHor(p1, p2), "point tidak sejajar hor " + p1.x + "/" + p2.x);
		}
		
	}

}