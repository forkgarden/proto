package tests {
	import flash.geom.Point;
	import iso3.Polygon4;
	/**
	 * ...
	 * @author test
	 */
	public class TestPolygon extends BaseTest {

		
		public function TestPolygon() {
			test();
		}
		
		private function polygonCreate():Polygon4 {
			var p:Polygon4;
			
			p = new Polygon4();
			p.setDot(
				[
					new Point(10, 10),
					new Point(50, 10),
					new Point(50, 50),
					new Point(10,50)
				]
			);
			
			return p;
		}
		
		public override function test():void {
			var p:Polygon4;
			
			p = polygonCreate();
			
			assert(p.dotInside(20, 20));
			
			assert(p.dotInside(5, 5) == false);
			assert(p.dotInside(5, 15) == false);
			assert(p.dotInside(55, 15) == false);
			assert(p.dotInside(15, 55) == false);
			
			p.destroy();
		}
		
	}

}