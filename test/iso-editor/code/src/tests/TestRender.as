package tests {
	import flash.display.Sprite;
	import flash.geom.Point;
	import iso3.Polygon4;
	/**
	 * ...
	 * @author test
	 */
	public class TestRender {
		
		public function TestRender(c:Sprite) {
			c.graphics.lineStyle(1);
			renderPolygonSelected(c);
		}
		
		public function renderPolygonSelected(c:Sprite):void {
			var p:Polygon4;
			
			p = new Polygon4();
			p.setDot([
				new Point(0, 0),
				new Point(100, 0),
				new Point(100, 100),
				new Point(0, 100)
			]);
			p.renderLine(c.graphics);
		}
		
	}

}