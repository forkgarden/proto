package tests {
	import flash.display.DisplayObjectContainer;
	import flash.geom.Point;
	import iso3.Block;
	import tests.manual.BlockUtil;
	/**
	 * ...
	 * @author test
	 */
	public class TestBlock extends BaseTest{
		
		private var util:BlockUtil;
		
		
		public function TestBlock(cont:DisplayObjectContainer) {
			this.cont = cont;
			util = new BlockUtil();
			
			test();
			
		}
		
		public override function test():void {
			testDotInside();
			//testOrder();
			testDepan();
			//testOrder();
			testDotInside();
		}
		
		/*
		public function testOrder():void {
			var block:Block;
			var block2:Block;
			
			cont.removeChildren();
			
			block = Block.create();
			block.addToStage(cont);
			assert(block.getZIndex() == 0, "cont children count " + cont.numChildren);
			
			block2 = Block.create();
			block2.debugSetViewPos(0, 100);
			block2.addToStage(cont);
			assert(block2.getZIndex() == 1);
			
			block.swapIdx(block2);
			assert(block.getZIndex() == 1);
			assert(block2.getZIndex() == 0);
			
			//block.debugZorder(block2);
			//trace("zorder " + block.zOrder(block2));
			//trace("");
			
			block.x = 0;
			block2.x = 1;
			assert(block2.didepan(block));
			block.zOrder(block2);
			assert(block.getZIndex() == 0);
			assert(block2.getZIndex() == 1);
			
			block2.zOrder(block);
			assert(block.getZIndex() == 0);
			assert(block2.getZIndex() == 1);
			
			block.x = 1;
			block2.x = 0;
			assert(block.didepan(block2));
			block.zOrder(block2);
			assert(block2.getZIndex() == 0);
			assert(block.getZIndex() == 1);
			
			block2.zOrder(block);
			assert(block2.getZIndex() == 0);
			assert(block.getZIndex() == 1);
			
			block.setPos(0, 1, 0);
			block2.setPos(0, 0, 0);
			assert(block.didepan(block2));
			block.zOrder(block2);
			assert(block2.getZIndex() == 0);
			assert(block.getZIndex() == 1);
			
			block2.zOrder(block);
			assert(block2.getZIndex() == 0);
			assert(block.getZIndex() == 1);
			
			block.destroy();
			block2.destroy();
		}
		*/
		
		public function debugDotInside2():void {
			var block:Block = new Block();
			var mx:Number;
			var my:Number;
			var px:int = -1;
			
			mx = -28;
			my = -14;
			
			trace("test dot inside ");
			
			px =  block.getPolygonHoverIdx(mx, my);
			
			trace("test polygon hovered " + px);
			trace("get cross dot " + block.getPolygon(px).hasLineCrossedOnDot(mx, my));
			
			if (px > -1) {
				block.getPolygon(px).debugCrossedLine(mx, my);
				//trace("test polygon 1 get inside " + block.getPolygon(1).dotInside(mx, my));
				//trace("line 1 cross " + block.getPolygon(1).getLine().crossLineHorRight(8, -32));
				//trace("dot cross " + block.getPolygon(0).getLine(0).crossOnDotHor(8, -32));
				//trace("n cross line " + block.getPolygon(0).getNumLineCrossed(8, -32));				
			}
		}
		
		public function testDotInside():void {
			var block:Block = new Block();
			
			assert(block.getPolygonHoverIdx(0, -24) == Block.P_TOP);
			assert(block.getPolygonHoverIdx(1, 8) == Block.P_RIGHT);
			assert(block.getPolygonHoverIdx(-1, -8) == Block.P_LEFT);
			assert(block.dotInside(0, 0));
			
			//negative test
			assert(block.dotInside(0, 64) == false);
			assert(false == block.dotInside(64, 0));
			assert(false == block.dotInside(-64, 0));
			assert(false == block.dotInside(0, -64));
			
			block.destroy();
		}
		
		public function testDepan():void {
			var block:Block;
			var block2:Block;
			
			block = util.blockCreate(0, 0, 0);
			block2 = util.blockCreate(1, 0, 0);
			
			//depan x
			assert(block2.didepan(block));
			assertFalse(block.didepan(block2));
			
			//depan z
			block2.setPos(0, 0, 1);
			assert(block2.didepan(block));
			assertFalse(block.didepan(block2));
			
			//belakang z
			block2.setPos(0, 0, -1);
			assert(block.didepan(block2));
			assertFalse(block2.didepan(block));
			
			//belakang x
			block2.setPos(-1, 0, 0);
			assert(block.didepan(block2));
			assertFalse(block2.didepan(block));
			
			//diagonal kanan depan
			block2.setPos(1, 0, 1);
			assert(block2.didepan(block));
			assertFalse(block.didepan(block2));
			
			//diagonal kiri depan
			block2.setPos(-1, 0, 1);
			assert(block2.didepan(block));
			assertFalse(block.didepan(block2));
			
			//diagonal kiri belakang
			block2.setPos(-1, 0, -1);
			assert(block.didepan(block2));
			assertFalse(block2.didepan(block));
			
			//diagonal kanan belakang
			block2.setPos(1, 0, -1);
			assert(block.didepan(block2));
			assertFalse(block2.didepan(block));
			
			//y atas
			block2.setPos(0, 1, 0);
			assert(block2.didepan(block));
			assertFalse(block.didepan(block2));
			
			//y atas depan
			block2.setPos(0, 1, 1);
			assert(block2.didepan(block));
			assertFalse(block.didepan(block2));
			
			//y bawah
			block2.setPos(0, -1, 0);
			assert(block.didepan(block2));
			assertFalse(block2.didepan(block));
			
			block.destroy();
			block2.destroy();
		}
		
		
	}

}