package{
	import com.adobe.tvsdk.mediacore.ABRControlParameters;
	import flash.display.Sprite;
	import flash.display.StageQuality;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import tests.MainTest;
	
	/**
	 * ...
	 * @author test
	 */
	public class Test extends Sprite {
		
		//private var main:Main;
		private var updateCallBack:Function;
		private var mainTest:MainTest;
		
		public function Test() {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			this.stage.quality = StageQuality.LOW;
			this.stage.addEventListener(MouseEvent.CLICK, mouseOnClick);
			this.addEventListener(Event.ENTER_FRAME, update);
			
			mainTest = new MainTest(this);
		}
		
		private function update(e:Event):void {
			mainTest.update();
		}
		
		private function mouseOnClick(e:MouseEvent):void {
			
		}
		
	}
	
}