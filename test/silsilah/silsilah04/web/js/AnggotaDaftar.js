import { db } from "./AnggotaDb.js";
import { form } from "./AnggotaForm.js";
import { BaseComponent } from "./BaseComponent.js";
import { index } from "./Index.js";
class Daftar extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='daftar-anggota'>
				<button class='tutup'>X</button>
				<h1>Daftar Anggota</h1>
				<button class='tambah'>Tambah Data</button>
				<div class='cont'>
				</div>
			</div>
		`;
        this.build();
        this.tutupTbl.onclick = () => {
            this._selesai();
        };
        this.tambahTbl.onclick = () => {
            this.detach();
            form.attach(index.cont);
            form.selesai = () => {
                form.detach();
                this.attach(index.cont);
            };
        };
    }
    set selesai(value) {
        this._selesai = value;
    }
    memuat() {
        db.get().then((data) => {
            data.forEach((anggota) => {
                let item = new Item();
                item.nama.innerHTML = anggota.nama;
                item.attach(this.cont);
            });
        }).catch((e) => {
            console.log(e);
        });
    }
    get cont() {
        return this.getEl('div.cont');
    }
    get tutupTbl() {
        return this.getEl('button.tutup');
    }
    get tambahTbl() {
        return this.getEl('button.tambah');
    }
}
class Item extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='anggota-item'>
				<p class='nama'></p>
			</div>
		`;
        this.build();
    }
    get nama() {
        return this.getEl('p.nama');
    }
}
export var daftar = new Daftar();
