export class OrtuObj {
    get anggota1() {
        return this._anggota1;
    }
    set anggota1(value) {
        this._anggota1 = value;
    }
    get anggota2() {
        return this._anggota2;
    }
    set anggota2(value) {
        this._anggota2 = value;
    }
}
