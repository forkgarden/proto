import { daftar } from "./AnggotaDaftar.js";
// import { form } from "./AnggotaForm.js";
import { BaseComponent } from "./BaseComponent.js";
import { index } from "./Index.js";
class Beranda extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='beranda'>
				<h1>Silsilah</h1>
				<button class='admin'>admin</button>
				<div class='cont'>
				</div>
			</div>
		`;
        this.build();
        this.adminTbl.onclick = () => {
            this.detach();
            daftar.attach(index.cont);
            daftar.memuat();
            daftar.selesai = () => {
                daftar.detach();
                this.attach(index.cont);
            };
        };
    }
    get adminTbl() {
        return this.getEl('button.admin');
    }
}
export var beranda = new Beranda();
