import { View, Foto } from "./AnggotaView.js";
class Anggota {
    render(cont, anggota) {
        let view = new View();
        view.attach(cont);
        let foto = new Foto();
        foto.gbr.src = anggota.url;
        foto.namaP.innerHTML = anggota.nama;
        foto.attach(view.atasCont);
    }
    renderKeluarga(cont, anggota, anakAr, pasangan) {
        this.render(cont, anggota);
        //render anak2
        anakAr.forEach((anak) => {
            this.render(anggota.view.bawahCont, anak);
        });
        this.render(anggota.view.atasCont, pasangan);
    }
    get foto() {
        return this._foto;
    }
    set foto(value) {
        this._foto = value;
    }
    get view() {
        return this._view;
    }
    set view(value) {
        this._view = value;
    }
}
export var anggota = new Anggota();
