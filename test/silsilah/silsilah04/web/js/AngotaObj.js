export class AnggotaObj {
    get id() {
        return this._id;
    }
    set id(value) {
        this._id = value;
    }
    get ortuId() {
        return this._ortuId;
    }
    set ortuId(value) {
        this._ortuId = value;
    }
    get foto() {
        return this._foto;
    }
    set foto(value) {
        this._foto = value;
    }
    get view() {
        return this._view;
    }
    set view(value) {
        this._view = value;
    }
    get nama() {
        return this._nama;
    }
    set nama(value) {
        this._nama = value;
    }
    get url() {
        return this._url;
    }
    set url(value) {
        this._url = value;
    }
}
