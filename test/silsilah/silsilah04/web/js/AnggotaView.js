import { BaseComponent } from "./BaseComponent.js";
export class Foto extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='foto'>
				<img src=''>
				<p class='nama'></p>
			</div>
		`;
        this.build();
    }
    get gbr() {
        return this.getEl('img');
    }
    get namaP() {
        return this.getEl('p.nama');
    }
}
export class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='anggota-cont>
				<div class='atas'></div>
				<div class='bawah'></div>
			</div>
		`;
        this.build();
    }
    get atasCont() {
        return this.getEl('div.atas');
    }
    get bawahCont() {
        return this.getEl('div.bawah');
    }
}
