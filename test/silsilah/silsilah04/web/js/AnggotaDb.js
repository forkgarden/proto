var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
// import { BaseComponent } from "./BaseComponent.js";
class Db {
    constructor() {
        this.nama = 'silsilah-anggota';
    }
    buatId() {
        let hasil;
        let data = new Date();
        hasil = data.getFullYear() + "_" + data.getMonth() + "_" + data.getDate() + "_" + data.getHours() + "_" + data.getMinutes() + "_" + data.getSeconds() + "_" + data.getMilliseconds() + '';
        return hasil;
    }
    get() {
        return __awaiter(this, void 0, void 0, function* () {
            let data = window.localStorage.getItem(this.nama);
            let obj = JSON.parse(data);
            if (!obj)
                obj = [];
            return obj;
        });
    }
    getId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            id;
            return null;
        });
    }
    baru(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            let data = yield this.get();
            obj.id = this.buatId();
            data.push(obj);
            window.localStorage.setItem(this.nama, JSON.stringify(data));
        });
    }
    update(id, anggota) {
        return __awaiter(this, void 0, void 0, function* () {
            id;
            anggota;
        });
    }
    delete() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
}
export var db = new Db();
