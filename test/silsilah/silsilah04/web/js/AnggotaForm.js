import { db } from "./AnggotaDb.js";
import { BaseComponent } from "./BaseComponent.js";
class Form extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='anggota-form'>
				<form>
					<label>Nama:</label>
					<input type='text' name='nama' class='nama' required/>

					<button type='submit'>Ok</button>
				</form>
			</div>
		`;
        this.build();
        this.form.onsubmit = () => {
            try {
                let data = {
                    id: '',
                    nama: this.nama.value,
                    url: '',
                    ortuId: ''
                };
                db.baru(data).then(() => {
                    console.log('isi data selesai');
                    this._selesai();
                }).catch((e) => {
                    console.log(e);
                });
            }
            catch (e) {
                console.log(e);
            }
            return false;
        };
    }
    set selesai(value) {
        this._selesai = value;
    }
    get form() {
        return this.getEl('form');
    }
    get nama() {
        return this.getEl('input.nama');
    }
}
export var form = new Form();
