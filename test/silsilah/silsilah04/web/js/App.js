import { beranda } from "./Beranda.js";
import { index } from "./Index.js";
class App {
    constructor() {
        window.onload = () => {
            this.init();
        };
    }
    init() {
        beranda.attach(index.cont);
    }
}
export var app = new App();
