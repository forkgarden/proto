import { db } from "./AnggotaDb.js";
import { form } from "./AnggotaForm.js";
import { IAnggotaObj } from "./AngotaObj.js";
import { BaseComponent } from "./BaseComponent.js";
import { index } from "./Index.js";

class Daftar extends BaseComponent {
	private _selesai: Function;
	public set selesai(value: Function) {
		this._selesai = value;
	}

	constructor() {
		super();
		this._template = `
			<div class='daftar-anggota'>
				<button class='tutup'>X</button>
				<h1>Daftar Anggota</h1>
				<button class='tambah'>Tambah Data</button>
				<div class='cont'>
				</div>
			</div>
		`;
		this.build();

		this.tutupTbl.onclick = () => {
			this._selesai();
		}

		this.tambahTbl.onclick = () => {
			this.detach();
			form.attach(index.cont);
			form.selesai = () => {
				form.detach();
				this.attach(index.cont);
			}
		}
	}

	memuat(): void {
		db.get().then((data: IAnggotaObj[]) => {
			data.forEach((anggota: IAnggotaObj) => {
				let item: Item = new Item();
				item.nama.innerHTML = anggota.nama;
				item.attach(this.cont);
			});
		}).catch((e) => {
			console.log(e);
		});
	}

	get cont(): HTMLDivElement {
		return this.getEl('div.cont') as HTMLDivElement;
	}

	get tutupTbl(): HTMLButtonElement {
		return this.getEl('button.tutup') as HTMLButtonElement;
	}

	get tambahTbl(): HTMLButtonElement {
		return this.getEl('button.tambah') as HTMLButtonElement;
	}
}

class Item extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='anggota-item'>
				<p class='nama'></p>
			</div>
		`;
		this.build();
	}

	get nama(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}
}

export var daftar: Daftar = new Daftar();