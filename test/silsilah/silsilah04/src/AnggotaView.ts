import { BaseComponent } from "./BaseComponent.js";

export class Foto extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='foto'>
				<img src=''>
				<p class='nama'></p>
			</div>
		`;
		this.build();
	}

	get gbr(): HTMLImageElement {
		return this.getEl('img') as HTMLImageElement;
	}

	get namaP(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}
}

export class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='anggota-cont>
				<div class='atas'></div>
				<div class='bawah'></div>
			</div>
		`;
		this.build();
	}

	get atasCont(): HTMLDivElement {
		return this.getEl('div.atas') as HTMLDivElement;
	}

	get bawahCont(): HTMLDivElement {
		return this.getEl('div.bawah') as HTMLDivElement;
	}
}