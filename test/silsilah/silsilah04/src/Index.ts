class Index {
	get cont(): HTMLDivElement {
		return document.body.querySelector('div.cont');
	}
}

export var index: Index = new Index();