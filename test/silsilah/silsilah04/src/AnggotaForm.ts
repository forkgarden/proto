import { db } from "./AnggotaDb.js";
import { IAnggotaObj } from "./AngotaObj.js";
import { BaseComponent } from "./BaseComponent.js";

class Form extends BaseComponent {
	private _selesai: Function;
	public set selesai(value: Function) {
		this._selesai = value;
	}

	constructor() {
		super();
		this._template = `
			<div class='anggota-form'>
				<form>
					<label>Nama:</label>
					<input type='text' name='nama' class='nama' required/>

					<button type='submit'>Ok</button>
				</form>
			</div>
		`;
		this.build();

		this.form.onsubmit = () => {
			try {
				let data: IAnggotaObj = {
					id: '',
					nama: this.nama.value,
					url: '',
					ortuId: ''
				};

				db.baru(data).then(() => {
					console.log('isi data selesai');
					this._selesai();
				}).catch((e) => {
					console.log(e);
				});
			}
			catch (e) {
				console.log(e);
			}

			return false;
		}
	}

	get form(): HTMLFormElement {
		return this.getEl('form') as HTMLFormElement;
	}

	get nama(): HTMLInputElement {
		return this.getEl('input.nama') as HTMLInputElement;
	}
}

export var form: Form = new Form();