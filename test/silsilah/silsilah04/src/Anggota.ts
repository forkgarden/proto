import { View, Foto } from "./AnggotaView.js";
import { AnggotaObj } from "./AngotaObj.js";

class Anggota {
	private _view: View;
	private _foto: Foto;

	render(cont: HTMLDivElement, anggota: AnggotaObj): void {
		let view: View = new View();
		view.attach(cont);

		let foto: Foto = new Foto();
		foto.gbr.src = anggota.url;
		foto.namaP.innerHTML = anggota.nama;
		foto.attach(view.atasCont);
	}

	renderKeluarga(cont: HTMLDivElement, anggota: AnggotaObj, anakAr: AnggotaObj[], pasangan: AnggotaObj): void {
		this.render(cont, anggota);

		//render anak2
		anakAr.forEach((anak: AnggotaObj) => {
			this.render(anggota.view.bawahCont as HTMLDivElement, anak);
		});

		this.render(anggota.view.atasCont, pasangan);
	}

	public get foto(): Foto {
		return this._foto;
	}
	public set foto(value: Foto) {
		this._foto = value;
	}
	public get view(): View {
		return this._view;
	}
	public set view(value: View) {
		this._view = value;
	}
}



export var anggota: Anggota = new Anggota();