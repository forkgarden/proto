import { Foto, View } from "./AnggotaView.js";

export class AnggotaObj implements IAnggotaObj {
	private _url: string;
	private _nama: string;
	private _ortuId: string;
	private _view: View;
	private _foto: Foto;
	private _id: string;

	public get id(): string {
		return this._id;
	}
	public set id(value: string) {
		this._id = value;
	}

	public get ortuId(): string {
		return this._ortuId;
	}
	public set ortuId(value: string) {
		this._ortuId = value;
	}

	public get foto(): Foto {
		return this._foto;
	}
	public set foto(value: Foto) {
		this._foto = value;
	}

	public get view(): View {
		return this._view;
	}
	public set view(value: View) {
		this._view = value;
	}

	public get nama(): string {
		return this._nama;
	}
	public set nama(value: string) {
		this._nama = value;
	}
	public get url(): string {
		return this._url;
	}
	public set url(value: string) {
		this._url = value;
	}
}

export interface IAnggotaObj {
	id: string;
	nama: string;
	url: string;
	ortuId: string;
}