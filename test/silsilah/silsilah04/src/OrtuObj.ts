export class OrtuObj {
	private _anggota1: string;
	public get anggota1(): string {

		return this._anggota1;
	}
	public set anggota1(value: string) {
		this._anggota1 = value;
	}
	private _anggota2: string;
	public get anggota2(): string {
		return this._anggota2;
	}
	public set anggota2(value: string) {
		this._anggota2 = value;
	}
}