import { AnggotaObj, IAnggotaObj } from "./AngotaObj.js";
// import { BaseComponent } from "./BaseComponent.js";

class Db {
	nama: string = 'silsilah-anggota';

	buatId(): string {
		let hasil: string;
		let data: Date = new Date();
		hasil = data.getFullYear() + "_" + data.getMonth() + "_" + data.getDate() + "_" + data.getHours() + "_" + data.getMinutes() + "_" + data.getSeconds() + "_" + data.getMilliseconds() + '';
		return hasil;
	}

	async get(): Promise<IAnggotaObj[]> {
		let data: string = window.localStorage.getItem(this.nama);
		let obj: IAnggotaObj[] = JSON.parse(data);

		if (!obj) obj = [];

		return obj;
	}

	async getId(id: string): Promise<AnggotaObj> {
		id;
		return null;
	}

	async baru(obj: IAnggotaObj): Promise<void> {
		let data: IAnggotaObj[] = await this.get();
		obj.id = this.buatId();
		data.push(obj);
		window.localStorage.setItem(this.nama, JSON.stringify(data));
	}

	async update(id: string, anggota: AnggotaObj): Promise<void> {
		id;
		anggota;
	}

	async delete(): Promise<void> {

	}
}

export var db: Db = new Db();
