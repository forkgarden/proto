import { AnggotaDao } from "./AnggotaDao.js"
import { BaseComponent } from "./BaseComponent.js";
// import { viewProfile } from "./HalViewProfile.js";
import { Util } from "./Util.js";

class App {
	readonly anggotaDao: AnggotaDao = new AnggotaDao();

	async loadAnggota(id: number): Promise<ISlAnggota> {
		let anggota: ISlAnggota = (await app.anggotaDao.bacaId(id))[0];
		anggota.populated = false;
		anggota.anak = [];
		return anggota;
	}

	async loadAnak(anggota: ISlAnggota): Promise<void> {
		let anak: ISlAnggota[] = await this.anggotaDao.bacaAnak(anggota);
		anak.forEach((item: ISlAnggota) => {
			item.populated = false;
		})
		anggota.anak = anak;
	}

	renderAnggota(anggota: ISlAnggota, cont: HTMLDivElement): void {
		console.log('render anggota');
		console.log(anggota);
		let view: AnggotaView = new AnggotaView();
		view.nama.innerHTML = anggota.nama;
		view.anggota = anggota;
		view.attach(cont);
		view.utama.onclick = (evt: MouseEvent) => {
			console.debug('view utama click');
			evt.stopPropagation();
			this.anggotaKlik(view);
		}

	}

	async anggotaKlik(view: AnggotaView): Promise<void> {
		console.group('anggota klik');

		if (view.anggota.populated == false) {
			console.debug('anggota belum di populate');
			view.anggota.populated = true;

			if (view.anggota.rel_id) {
				console.debug('load pasangan');
				let pas: ISlAnggota = (await app.anggotaDao.bacaPasangan(view.anggota))[0];
				if (pas) {
					console.debug('pasangan loaded');
					view.anggota.pasangan_id = pas.id;
					view.anggota.pas = pas;
				}
				else {
					console.debug('pasangan tidak ketemu');
				}
			}
			else {
				console.debug('pasangan relasi tidak ada')
			}

			//load anak
			console.group('load anak');
			await this.loadAnak(view.anggota);
			console.groupEnd();
		}

		if (view.anggota.pas) {
			console.debug('render pasangan');
			view.namaPasangan.innerHTML = view.anggota.pas.nama;
			view.pasangan.classList.toggle('display-none');
		}

		view.bawah.innerHTML = '';
		view.anggota.anak.forEach((anak: ISlAnggota) => {
			this.renderAnggota(anak, view.bawah);
		})

		view.bawah.classList.toggle('display-none');
		view.bawah.classList.toggle('display-flex');

		console.groupEnd();
	}

}

class AnggotaView extends BaseComponent {
	private _anggota: ISlAnggota;
	public get anggota(): ISlAnggota {
		return this._anggota;
	}
	public set anggota(value: ISlAnggota) {
		this._anggota = value;
	}

	constructor() {
		super();
		this._elHtml = document.body.querySelector('template').content.querySelector('div.cont').cloneNode(true) as HTMLDivElement;

	}

	get bawah(): HTMLDivElement {
		return this.getEl('div.bawah') as HTMLDivElement;
	}

	get nama(): HTMLDivElement {
		return this.getEl('div.utama div.nama') as HTMLDivElement;
	}

	get img(): HTMLDivElement {
		return this.getEl('div.utama img.foto') as HTMLImageElement;
	}

	get utama(): HTMLDivElement {
		return this.getEl('div.utama') as HTMLImageElement;
	}

	get pasangan(): HTMLDivElement {
		return this.getEl('div.pasangan') as HTMLDivElement;
	}

	get imgPasangan(): HTMLImageElement {
		return this.getEl('div.pasangan img.foto') as HTMLImageElement;
	}

	get namaPasangan(): HTMLDivElement {
		return this.getEl('div.pasangan div.nama') as HTMLDivElement;
	}


}

var app: App = new App();

window.onload = () => {
	let w: any = window;
	w.app = app;

	app.loadAnggota(1).then((hasil: ISlAnggota) => {
		console.log(hasil);
		app.renderAnggota(hasil, document.body as HTMLDivElement);
	}).catch((e) => {
		Util.error(e);
	});

	window.document.body.onclick = () => {
		console.log('window on click')
	}
}


