import { IAnggota } from "./Data.js";
import { db } from "./Db.js";


class AnggotaController {

	constructor() {
	}

	/**
	 * Isi dengan default value
	 * @param anggota 
	 */
	default(anggota: IAnggota): void {
		anggota.stBuka = false;
		anggota.stPopulate = false;
		anggota.stPopulateAnak = false;
		anggota.anak2Obj = [];
		anggota.stIstri = false;
		anggota.pasanganObj = null;
		anggota.stRender = false;
		anggota.induk = null;
		anggota.indukId = 0;

		anggota.view = null;
		anggota.viewFoto = null;
		anggota.viewTambahPasangan = null;

		// //TODO: dep
		// if (anggota.pasanganObj) {
		// 	anggota.stPasangan = false;
		// 	anggota.pasanganObj.pasanganObj = anggota;
		// 	anggota.pasanganObj.stPasangan = true;
		// }

		// if (!anggota.anak2) anggota.anak2 = [];

		// //TODO: dep
		// anggota.anak2.forEach((item: IAnggota) => {
		// 	item.induk = anggota;
		// 	this.default(item);
		// });
	}

	populateAnak(anggota: IAnggota): void {
		if (anggota.stPopulateAnak) {
			return;
		}

		anggota.anak2Obj = [];
		anggota.anak2Obj = db.getByInduk(anggota.id).slice();
		anggota.anak2Obj.forEach((item: IAnggota) => {
			this.default(item);
			item.induk = anggota;
			item.indukId = anggota.id;
		});
		anggota.stPopulateAnak = true;
	}

	populatePasangan(anggota: IAnggota): void {
		if (anggota.pasanganId && !anggota.pasanganObj) {
			anggota.pasanganObj = db.getById(anggota.pasanganId);
			this.default(anggota.pasanganObj);
		}
	}

	populate(anggota: IAnggota): void {
		if (anggota.stPopulate) return;

		this.populateAnak(anggota);
		this.populatePasangan(anggota);

		// //pasangan belum diload
		// if (anggota.pasanganId && !anggota.pasanganObj) {
		// 	anggota.pasanganObj = db.getById(anggota.pasanganId);
		// 	this.default(anggota.pasanganObj);
		// }

		// anggota.anak2 = [];
		// anggota.anak2 = db.getByInduk(anggota.id).slice();
		// anggota.anak2.forEach((item: IAnggota) => {
		// 	item.induk = anggota;
		// 	item.indukId = anggota.id;
		// 	this.default(item);
		// });

		anggota.stPopulate = true;

		console.log('populate anggota selesai');
	}

	hapusAnak(anggota: IAnggota, anak: IAnggota): void {
		for (let i: number = 0; i < anggota.anak2Obj.length; i++) {
			if (anggota.anak2Obj[i] == anak) {
				let anakHapus: IAnggota = anggota.anak2Obj.splice(i, 1)[0];
				db.hapus(anakHapus.id);
			}
		}
	}

	// updateViewToggle(anggota: IAnggota): void {
	// 	console.log('update view toggle');

	// 	//status terbuka
	// 	if (anggota.buka) {
	// 		console.log('anggota buka');

	// 		//ada anak
	// 		if (anggota.view && anggota.view.anakCont) {
	// 			console.log('render anak');
	// 			anggota.view.anakCont.style.display = 'table';
	// 		}

	// 		//punya pasangan
	// 		if (anggota.pasanganObj) {

	// 			//suami
	// 			if (!anggota.stPasangan) {
	// 				anggota.view.istriCont.style.display = 'table-cell';
	// 				anggota.view.suamiCont.style.textAlign = 'right';
	// 			}
	// 			else {
	// 				//istri
	// 			}
	// 		}

	// 		//jomblo
	// 		else {

	// 			//suami
	// 			if (!anggota.stPasangan) {
	// 				anggota.view.istriCont.style.display = 'none';
	// 				anggota.view.suamiCont.style.textAlign = 'center';
	// 			}

	// 			//istri error
	// 			else {

	// 			}
	// 		}
	// 	}

	// 	//anggota tutup
	// 	else {
	// 		console.log('status tutup');

	// 		if (anggota.view && anggota.view.anakCont) {
	// 			console.log('tutup anak');
	// 			anggota.view.anakCont.style.display = 'none';
	// 		}

	// 		//punya pasangan
	// 		if (anggota.pasanganObj) {

	// 			//suami
	// 			if (!anggota.stPasangan) {
	// 				anggota.view.istriCont.style.display = 'none';
	// 				anggota.view.suamiCont.style.textAlign = 'center';
	// 				// anggota.view.istriCont.classList.remove('normal');
	// 				// anggota.view.istriCont.classList.add('sembunyi');
	// 			}

	// 			//istri
	// 			else {

	// 			}
	// 		}

	// 		//jomblo
	// 		else {

	// 			if (anggota.view) {
	// 				anggota.view.istriCont.style.display = 'none';
	// 				anggota.view.suamiCont.style.textAlign = 'center';
	// 			}
	// 		}
	// 	}
	// }

	// renderHubung(view: Hubung, hubung: number): void {
	// 	view.kanan.classList.remove('border-kanan', 'border-kiri', 'border-atas', 'border-bawah');
	// 	view.kiri.classList.remove('border-kanan', 'border-kiri', 'border-atas', 'border-bawah');

	// 	if (hubung == -1) {

	// 	} else if (hubung == 0) {
	// 		view.kanan.classList.add('border-kiri');
	// 		view.kiri.classList.add('border-kanan');
	// 	} else if (hubung == 1) {
	// 		view.kiri.classList.add('border-kanan');
	// 		view.kanan.classList.add('border-atas');
	// 		view.kanan.classList.add('border-kiri');
	// 	} else if (hubung == 2) {
	// 		view.kiri.classList.add('border-kanan');
	// 		view.kiri.classList.add('border-atas');
	// 		view.kanan.classList.add('border-atas');
	// 		view.kanan.classList.add('border-kiri');

	// 	} else if (hubung == 3) {
	// 		view.kiri.classList.add('border-atas');
	// 		view.kiri.classList.add('border-kanan');
	// 		view.kanan.classList.add('border-kiri');
	// 	}
	// }

	// renderContainer(hubung: number): KotakView {
	// 	let view: KotakView = new KotakView();
	// 	let hubungView: Hubung = new Hubung();
	// 	hubungView.attach(view.hubungCont);
	// 	render.renderHubung(hubungView, hubung);
	// 	return view;
	// }


}

export var anggotaController: AnggotaController = new AnggotaController();

