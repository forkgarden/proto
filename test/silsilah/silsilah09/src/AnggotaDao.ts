import { Util } from "./Util.js";

export class AnggotaDao {
	async baca(): Promise<ISlAnggota[]> {
		return (await Util.sql(`SELECT * FROM sl_anggota`));
	}

	async bacaAnak(anggota: ISlAnggota): Promise<ISlAnggota[]> {

		if (anggota.rel_id == 0) {
			console.debug('tidak ada data pasangan.');
			return [];
		}

		return (await Util.sql(`
            SELECT *
            FROM sl_anggota
            WHERE ortu_id = ${anggota.rel_id}`));
	}

	//TODO: pakai object anggota untuk dapatkan id dan rel_id
	async bacaPasangan(anggota: ISlAnggota): Promise<ISlAnggota[]> {
		console.group('baca pasangan api:');
		let pas: ISlAnggota[] = [];

		let relAr: ISlRelasi[] = await Util.sql(`
			SELECT * FROM sl_relasi WHERE id = ${anggota.rel_id}`) as ISlRelasi[];
		let rel: ISlRelasi = relAr[0];

		if (rel) {
			pas = await Util.sql(`
				SELECT * FROM sl_anggota 
				WHERE rel_id = ${rel.id}
				AND id != ${anggota.id}
			`) as ISlAnggota[];
		}
		else {

		}


		console.log('pasangan');
		console.log(pas);


		console.groupEnd();

		return pas;
	}

	async bacaId(id: number): Promise<ISlAnggota[]> {
		return (await Util.sql(`
            SELECT *
            FROM sl_anggota
            WHERE id = ${id}`));
	}
}
