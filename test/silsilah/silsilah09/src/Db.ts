import { id } from "./Counter.js";
import { IAnggota, file2, IDb } from "./Data.js";

class Db {
	private counter: CounterDb = new CounterDb();
	private anggotaAr: IAnggota[] = [];

	constructor() {
		this.counter;
	}

	load(): IDb {
		let dataStr: string = window.localStorage.getItem(file2);
		let dbObj: IDb

		console.log(dataStr);

		if (!dataStr || dataStr == '') {
			dbObj = {
				anggota: [this.buatAnggotaObj()],
				ctr: 1,
				awal: null
			}
			dbObj.awal = 1;
			id.id = 0;
			dbObj.anggota[0].id = id.id;
		}
		else {
			dbObj = JSON.parse(dataStr);
			id.id = dbObj.ctr;
		}

		this.anggotaAr = [];
		dbObj.anggota.forEach((item: IAnggota) => {
			this.anggotaAr.push(item);
		});

		console.log(dbObj);
		return dbObj;
	}

	buatAnggotaObj(): IAnggota {
		console.log('buat anggota');
		return {
			indukId: 0,
			nama: 'nama',
			anak2Obj: [],
			stIstri: false,
			stBuka: true
		}
	}

	cariAwal(): IAnggota {
		let hasil: IAnggota;

		this.anggotaAr.forEach((item: IAnggota) => {
			if (item.indukId == 0) hasil = item;
		});

		return hasil;
	}

	toObj(anggota: IAnggota): IAnggota {
		console.group('anggota to obj, id ' + anggota.id);
		let hasil: IAnggota = {
			id: anggota.id,
			nama: anggota.nama,
			anak2Obj: [],
			stIstri: anggota.stIstri ? true : false,
		}

		if (anggota.induk) {
			hasil.indukId = anggota.induk.id;
		}

		if (anggota.pasanganObj) {
			console.log('ada pasangan');
			hasil.pasanganId = anggota.pasanganObj.id;
		}

		console.log('anggota');
		console.log(anggota);
		console.log('hasil:');
		console.log(hasil);
		console.groupEnd();

		return hasil;
	}

	getByInduk(id: number): IAnggota[] {
		let hasil: IAnggota[] = [];

		this.anggotaAr.forEach((item: IAnggota) => {
			if (item.indukId == id) hasil.push(item);
		});

		return hasil;
	}

	baru(anggota: IAnggota): void {
		anggota.id = id.id;
		this.anggotaAr.push(anggota);
		console.log('anggota insert');
		console.log(anggota);
		this.simpan();
	}

	hapus(id: number): void {
		for (let i: number = 0; i < this.anggotaAr.length; i++) {
			if (this.anggotaAr[i].id == id) {
				this.anggotaAr.splice(i, 1);
				this.simpan();
				return;
			}
		}

		throw Error('gak ketemu ' + id);
	}

	update(id: number, data: IAnggota): void {
		let anggota: IAnggota = this.getById(id);
		anggota.nama = data.nama;

		this.simpan();
	}

	getById(id: number): IAnggota {
		let hasil: IAnggota;

		this.anggotaAr.forEach((item: IAnggota) => {
			if (item.id == id) hasil = item;
		});

		return hasil;
	}

	simpan(): void {
		console.log('simpan');
		let idb: IDb = {
			anggota: [],
			ctr: id.id,
			awal: null
		}

		this.anggotaAr.forEach((item: IAnggota) => {
			idb.anggota.push(this.toObj(item));
		});

		window.localStorage.setItem(file2, JSON.stringify(idb));
	}
}

class CounterDb {

	load(): void {

	}

	simpan() {

	}
}

export var db: Db = new Db();