class Counter {
	private _id: number = 0;

	public get id(): number {
		console.log('get id' + this._id);
		this._id++;
		return this._id;
	}

	public set id(value: number) {
		this._id = value;
	}
}

export var id: Counter = new Counter();
