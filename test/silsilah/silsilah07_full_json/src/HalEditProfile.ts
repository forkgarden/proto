import { BaseComponent } from "./BaseComponent.js";
import { IAnggota } from "./Data.js";

class EditProfile {
	private _view: View = new View();
	private _selesai: Function;
	private anggota: IAnggota;
	private anggotaLama: IAnggota;

	public set selesai(value: Function) {
		this._selesai = value;
	}
	public get view(): View {
		return this._view;
	}
	public set view(value: View) {
		this._view = value;
	}

	constructor() {

		this._view.tblTutup.onclick = () => {
			this.anggota.nama = this.anggotaLama.nama;
			this._view.detach();
			this._selesai();
		}

		this._view.tblSimpan.onclick = () => {
			this.anggota.nama = this._view.inputNama.value;
			this._view.detach();
			this._selesai();
		}

	}

	tampil(anggota: IAnggota): void {
		console.log(this.anggotaLama);
		this._view.inputNama.value = anggota.nama;
		this.anggotaLama.nama = anggota.nama;
		this.anggotaLama.id = anggota.id;
		this.anggota = anggota;
		this._view.attach(document.body);
	}

}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='edit-profile'>
				<div>
					<div class='kotak'>
						<div class='tutup'>
							<button class='tutup'>&times;</button>
						</div>
						<div class='judul'>
							Edit Profile
						</div>
						<br/>
						<div>
							<label>Nama:</nama><br/>
							<input type='text' class='nama' placeholder='nama'/><br/>
							<br/>
							<button class='simpan'>OK</button>
						</div>
					</div>
				</div>
			</div>
		`;
		this.build();
	}

	get tblTutup(): HTMLButtonElement {
		return this.getEl('button.tutup') as HTMLButtonElement;
	}

	get inputNama(): HTMLInputElement {
		return this.getEl('input.nama') as HTMLInputElement;
	}

	get tblSimpan(): HTMLButtonElement {
		return this.getEl('button.simpan') as HTMLButtonElement;
	}

}

export var editProfile: EditProfile = new EditProfile();