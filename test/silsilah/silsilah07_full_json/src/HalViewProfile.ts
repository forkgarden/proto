import { BaseComponent } from "./BaseComponent.js";
import { IAnggota } from "./Data.js";
import { editProfile } from "./HalEditProfile.js";

class ViewProfile {
	private _view: View = new View();
	private _selesai: Function;
	private anggota: IAnggota;

	constructor() {

		this._view.tblTutup.onclick = (e: MouseEvent) => {
			console.log('view profile tutup');
			e.stopPropagation();
			this._view.detach();
			this._selesai();
		}

		this._view.tblSimpan.onclick = (e: MouseEvent) => {
			// console.log('ok click');
			e.stopPropagation();
			this._view.detach();
			this._selesai();
		}

		this._view.tblEdit.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			this._view.detach();
			editProfile.tampil(this.anggota);
			editProfile.selesai = () => {
				this._selesai();
			}
		}

	}

	tampil(anggota: IAnggota): void {
		this._view.inputNama.value = anggota.nama;
		this.anggota = anggota;
		this._view.attach(document.body);
	}

	public set selesai(value: Function) {
		this._selesai = value;
	}

	public get view(): View {
		return this._view;
	}


}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='edit-profile'>
				<div>
					<div class='kotak'>
						<div class='tutup'>
							<button class='tutup'>&times;</button>
						</div>
						<div class='judul'>
							Lihat Profile
						</div>
						<br/>
						<div>
							<label>Nama:</label><br/>
							<input type='text' class='nama' placeholder='nama' readonly enabled=false/><br/>
							<br/>
							<button class='simpan'>OK</button>
							<button class='edit'>Edit</button>
						</div>
					</div>
				</div>
			</div>
		`;
		this.build();
	}

	get tblTutup(): HTMLButtonElement {
		return this.getEl('button.tutup') as HTMLButtonElement;
	}

	get inputNama(): HTMLInputElement {
		return this.getEl('input.nama') as HTMLInputElement;
	}

	get tblSimpan(): HTMLButtonElement {
		return this.getEl('button.simpan') as HTMLButtonElement;
	}

	get tblEdit(): HTMLButtonElement {
		return this.getEl('button.edit') as HTMLButtonElement;
	}

}

export var viewProfile: ViewProfile = new ViewProfile();