import { anggotaController } from "./AnggotaController.js";
import { Foto, IAnggota, data } from "./Data.js";
import { db } from "./Db.js";
import { viewProfile } from "./HalViewProfile.js";
import { render } from "./Render.js";

class AnggotaEvent {
	setEvent(foto: Foto, anggota: IAnggota) {

		foto.elHtml.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			render.resetMenuPopUp();
			console.log('foto on click');
			console.log('anggota populate status ' + anggota.stPopulate);

			anggota.stBuka = !anggota.stBuka;

			if (!anggota.stPopulate) {
				console.warn('anggota belum di populate');
			}

			render.updateViewToggle(anggota);
		}

		foto.tombolMenuBtn.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			console.log('foto button click');
			foto.menuPopupDiv.style.display = 'block';

			if (data.popupAktif) {
				data.popupAktif.style.display = 'none';
			}
			data.popupAktif = foto.menuPopupDiv;

			foto.tambahPasanganTbl.style.display = 'block';
			foto.tambahAnakTbl.style.display = 'block';

			if (anggota.pasanganObj) {
				foto.tambahPasanganTbl.style.display = 'none';
			}

			if (anggota.stIstri) {
				foto.tambahPasanganTbl.style.display = 'none';
				foto.tambahAnakTbl.style.display = 'none';
			}
		}

		foto.hapusTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();

			console.log('hapus tombol');
			// console.log('status pasangan ' + anggota.stPasangan);

			if (!anggota.stIstri) {
				//hapus suami
				if (anggota.induk) {
					anggotaController.hapusAnak(anggota.induk, anggota);
					render.renderAnak(anggota.induk);
					render.updateViewToggle(anggota.induk);
					db.hapus(anggota.id);
				}
				else {
					//TODO: info tidak bisa hapus anggota awal
				}
			}
			else {
				//hapus istri
				let suami: IAnggota = anggota.pasanganObj;
				anggota.viewFoto.detach();
				suami.pasanganObj = null;
				anggota.pasanganObj = null;
				db.hapus(anggota.id);
				render.updateViewToggle(suami);
			}

			render.resetMenuPopUp();
		}

		foto.profileTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			render.resetMenuPopUp();

			viewProfile.tampil(anggota);
			viewProfile.selesai = () => {
				foto.namaDiv.innerHTML = anggota.nama;
				db.update(anggota.id, anggota);
			}
		}

		foto.tambahPasanganTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			render.resetMenuPopUp();

			let pasangan: IAnggota = db.buatAnggotaObj();
			pasangan.stIstri = true;
			pasangan.stPopulate = true;
			pasangan.pasanganObj = anggota;

			anggota.pasanganObj = pasangan;
			anggota.stBuka = true;

			db.baru(anggota.pasanganObj);

			render.renderPasangan(anggota);
			render.updateViewToggle(anggota);
		}

		foto.tambahAnakTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			render.resetMenuPopUp();

			let anak: IAnggota = db.buatAnggotaObj();
			anak.induk = anggota;
			anggota.stBuka = true;
			anggota.stPopulate = true;
			db.baru(anak);

			anggota.anak2Obj.push(anak);
			render.renderAnak(anggota);
			render.updateViewToggle(anggota);
			render.updateViewToggle(anak);
		}

		foto.debugTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			render.resetMenuPopUp();
			console.log(anggota);
		}
	}
}

export var aevent: AnggotaEvent = new AnggotaEvent();