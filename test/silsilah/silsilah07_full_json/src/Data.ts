import { BaseComponent } from "./BaseComponent.js";

class Data {
	private _anggotaAwal: IAnggota;
	private _menuAktif: HTMLDivElement;
	private _popupAktif: HTMLDivElement;

	public get popupAktif(): HTMLDivElement {
		return this._popupAktif;
	}
	public set popupAktif(value: HTMLDivElement) {
		this._popupAktif = value;
	}
	public get menuAktif(): HTMLDivElement {
		return this._menuAktif;
	}
	public set menuAktif(value: HTMLDivElement) {
		this._menuAktif = value;
	}

	public get anggotaAwal(): IAnggota {
		return this._anggotaAwal;
	}
	public set anggotaAwal(value: IAnggota) {
		this._anggotaAwal = value;
	}
}
export var data: Data = new Data();

// export const file: string = 'silsilah07';
export const file2: string = 'silsilah08';

export class KotakView extends BaseComponent {
	// private _anggota: IAnggota;

	// public get anggota(): IAnggota {
	// 	return this._anggota;
	// }

	constructor() {
		super();
		this._template = `
			<div class='kotak'>
				<div class='hubung-cont'>

				</div>
				<div class='cont-pasangan padding'>
					<div class='suami normal'></div>
					<div class='istri normal'></div>
				</div>
				<div class='anak'>
		
				</div>
			</div>
		`;
		this.build();
	}

	get hubungCont(): HTMLDivElement {
		return this.getEl('div.hubung-cont') as HTMLDivElement;
	}

	get pasanganCont(): HTMLDivElement {
		return this.getEl('div.cont-pasangan') as HTMLDivElement;
	}

	get suamiCont(): HTMLDivElement {
		return this.getEl('div.cont-pasangan div.suami') as HTMLDivElement;
	}

	get istriCont(): HTMLDivElement {
		return this.getEl('div.cont-pasangan div.istri') as HTMLDivElement;
	}

	get anakCont(): HTMLDivElement {
		return this.getEl('div.anak') as HTMLDivElement
	}
}

export class Foto extends BaseComponent {
	constructor() {
		super();
		this._template = `
		<div class='foto inline'>
			<div class='padding'>

				<div class='info'>
					<div class='foto'> 
						<img src='./gbr/kosong.png'/>
					</div>
					<div class='nama '></div>
				</div>

				<div class='menu relative'>
					<button class='menu'>|||</button>
					<div class='menu-popup padding absolute border'>
						<button class='hapus block'>hapus</button>
						<button class='pasangan block'>tambah pasangan</button>
						<button class='anak block'>tambah anak</button>
						<button class='profile block'>edit profile</button>
						<button class='debug block'>debug</button>
					</div>
				</div>
			<div>
		</div>`;
		this.build();
	}

	get hapusTbl(): HTMLButtonElement {
		return this.getEl('div.padding div.menu div.menu-popup button.hapus') as HTMLButtonElement;
	}

	get tambahPasanganTbl(): HTMLButtonElement {
		return this.getEl('div.padding div.menu div.menu-popup button.pasangan') as HTMLButtonElement;
	}

	get tambahAnakTbl(): HTMLButtonElement {
		return this.getEl('div.padding div.menu div.menu-popup button.anak') as HTMLButtonElement;
	}


	get debugTbl(): HTMLButtonElement {
		return this.getEl('div.padding div.menu div.menu-popup button.debug') as HTMLButtonElement;
	}

	get profileTbl(): HTMLButtonElement {
		return this.getEl('div.padding div.menu div.menu-popup button.profile') as HTMLButtonElement;
	}

	get namaDiv(): HTMLDivElement {
		return this.getEl('div.nama') as HTMLDivElement;
	}

	get infoDiv(): HTMLDivElement {
		return this.getEl('div.info') as HTMLDivElement
	}

	get menuDiv(): HTMLDivElement {
		return this.getEl('div.menu') as HTMLDivElement;
	}

	get menuPopupDiv(): HTMLDivElement {
		return this.getEl('div.menu-popup') as HTMLDivElement;
	}

	get tombolMenuBtn(): HTMLButtonElement {
		return this.menuDiv.querySelector('button.menu') as HTMLButtonElement;
	}


}

export class Tambah extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='tambah-anak'>
				<div class='hubung-cont'>

				</div>
				<div class='padding'>
					<button class='tambah'>tambah<br/>anak</button>
				</div>
			</div>
		`;
		this.build();
	}

	get hubungCont(): HTMLDivElement {
		return this.getEl('div.hubung-cont') as HTMLDivElement;
	}

	get tambahTbl(): HTMLButtonElement {
		return this.getEl('button.tambah') as HTMLButtonElement;
	}
}

export class TambahPasangan extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='tambah'>
				<div class='padding'>
					<button class='tambah'>tambah<br/>pasangan</button>
				</div>
			</div>
		`;
		this.build();
	}

	get tambahTbl(): HTMLButtonElement {
		return this.getEl('button.tambah') as HTMLButtonElement;
	}
}

export class Hubung extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='hubung'>
				<div class='kiri'></div>
				<div class='kanan'></div>
			</div>
		`;
		this.build();
	}

	get hubungDiv(): HTMLDivElement {
		return this.getEl('div.hubung') as HTMLDivElement;
	}

	get kanan(): HTMLDivElement {
		return this.getEl('div.hubung div.kanan') as HTMLDivElement;
	}
	get kiri(): HTMLDivElement {
		return this.getEl('div.hubung div.kiri') as HTMLDivElement;
	}


}


export interface IAnggota {
	id?: number;
	nama?: string;
	tglLahir?: Date;
	indukId?: number;
	pasanganId?: number;

	//tambahan
	anak2Obj: IAnggota[];
	pasanganObj?: IAnggota;
	induk?: IAnggota;
	stIstri: boolean;
	stBuka?: boolean;
	stPopulate?: boolean;
	stPopulateAnak?: boolean;
	stRender?: boolean;

	view?: KotakView;
	viewFoto?: Foto;
	viewTambahPasangan?: TambahPasangan;
}

export interface Tanggal {
	tahun: number;
	bulan: number;
	hari: number;
}

export interface IDb {
	anggota: IAnggota[];
	awal: number;
	ctr: number;
}