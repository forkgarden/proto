import { db } from "./Db.js";
class AnggotaController {
    constructor() {
    }
    /**
     * Isi dengan default value
     * @param anggota
     */
    default(anggota) {
        anggota.stBuka = false;
        anggota.stPopulate = false;
        anggota.stPopulateAnak = false;
        anggota.anak2Obj = [];
        anggota.stPasangan = false;
        anggota.pasanganObj = null;
        anggota.stRender = false;
        anggota.induk = null;
        anggota.indukId = 0;
        anggota.view = null;
        anggota.viewFoto = null;
        anggota.viewTambahPasangan = null;
        // //TODO: dep
        // if (anggota.pasanganObj) {
        // 	anggota.stPasangan = false;
        // 	anggota.pasanganObj.pasanganObj = anggota;
        // 	anggota.pasanganObj.stPasangan = true;
        // }
        // if (!anggota.anak2) anggota.anak2 = [];
        // //TODO: dep
        // anggota.anak2.forEach((item: IAnggota) => {
        // 	item.induk = anggota;
        // 	this.default(item);
        // });
    }
    populateAnak(anggota) {
        if (anggota.stPopulateAnak) {
            return;
        }
        anggota.anak2Obj = [];
        anggota.anak2Obj = db.getByInduk(anggota.id).slice();
        anggota.anak2Obj.forEach((item) => {
            this.default(item);
            item.induk = anggota;
            item.indukId = anggota.id;
        });
        anggota.stPopulateAnak = true;
    }
    populatePasangan(anggota) {
        if (anggota.pasanganId && !anggota.pasanganObj) {
            anggota.pasanganObj = db.getById(anggota.pasanganId);
            this.default(anggota.pasanganObj);
        }
    }
    populate(anggota) {
        if (anggota.stPopulate)
            return;
        this.populateAnak(anggota);
        this.populatePasangan(anggota);
        // //pasangan belum diload
        // if (anggota.pasanganId && !anggota.pasanganObj) {
        // 	anggota.pasanganObj = db.getById(anggota.pasanganId);
        // 	this.default(anggota.pasanganObj);
        // }
        // anggota.anak2 = [];
        // anggota.anak2 = db.getByInduk(anggota.id).slice();
        // anggota.anak2.forEach((item: IAnggota) => {
        // 	item.induk = anggota;
        // 	item.indukId = anggota.id;
        // 	this.default(item);
        // });
        anggota.stPopulate = true;
        console.log('populate anggota selesai');
    }
    hapusAnak(anggota, anak) {
        for (let i = 0; i < anggota.anak2Obj.length; i++) {
            if (anggota.anak2Obj[i] == anak) {
                let anakHapus = anggota.anak2Obj.splice(i, 1)[0];
                db.hapus(anakHapus.id);
            }
        }
    }
}
export var anggotaController = new AnggotaController();
