import express from "express";
import path from "path";
import bodyparser from "body-parser";
import { Anggota } from "./Anggota";

const app: express.Express = express();
const port: number = 3009;

app.use(express.static(__dirname + "\\..\\..\\..\\webclient\\web\\"));
app.use(bodyparser.json());

app.get("/", (req: express.Request, res: express.Response) => {
	res.send("hello world");
});

app.get("/web", (req, res) => {
	res.sendFile(path.join(__dirname, "\\..\\..\\..\\webclient\\web\\", "index.html"));
});

app.post("/anggota/insert", (req:express.Request, res:express.Response) => {
	Anggota;
});

app.post("/anggota/get", (req:express.Request, res:express.Response) => {

});

app.post("/anggota/getById", (req:express.Request, res:express.Response) => {

});

app.post("/anggota/update", (req:express.Request, res:express.Response) => {

});

app.post("/anggota/delete", (req:express.Request, res:express.Response) => {

});

app.listen(port, () => {
	console.log("app started at port " + port);
})

// app.get("/json", (req, res) => {
// 	res.send('test');
// 	let google: GoogleTest2 = new GoogleTest2();
// 	google.run();
// 	// let json
// });

// app.post("/json", (req, res) => {
// 	console.log("req. body");
// 	console.log(req.body);
// 	res.send(req.body);

// 	let google:GoogleTest2 = new GoogleTest2();
// 	google.runFromData(req.body.steps);
	
// 	// google.run();
// 	// let json
// });

// app.get("/test", (req: express.Request, res: express.Response) => {
// 	res.send('test');
// 	let google: GoogleTest = new GoogleTest();
// 	google.run();
// });