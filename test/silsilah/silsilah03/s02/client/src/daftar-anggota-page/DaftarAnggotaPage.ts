import { BaseComponent } from "../ha/BaseComponent.js";
// import { FireBaseClient } from "../server/firebase-client/FirebaseClient.js";
import { AnggotaObj } from "../ent/AnggotaObj.js";
import { Item } from "./Item.js";
import { Util } from "../Util.js";
import { MySqlAdapter } from "../server/mysql/MysqlAdapter.js";

export class DaftarAnggotaPage extends BaseComponent {
	private client: MySqlAdapter = new MySqlAdapter();
	private anggota: AnggotaObj[];
	private anggotaRenderAr: AnggotaObj[];

	constructor() {
		super();
		Util.loadingStart();

		window.onload = () => {
			console.log('window onload');
			this._elHtml = Util.getEl('div.cont');
			this.init().then(() => {
				Util.loadingEnd();
			}).catch((e: Error) => {
				console.log(e);
				Util.loadingEnd();
				Util.alertMsg(e.message);
			});
		}
	}

	async init(): Promise<void> {

		await this.client.init();
		this.anggota = await this.client.anggota.get();
		this.anggotaRenderAr = this.anggota.slice();
		this.renderList(this.anggota);

		this.cariInput.oninput = () => {
			this.anggotaRenderAr = this.anggota.filter((item: AnggotaObj) => {
				if (item.namaLengkap.toLocaleLowerCase().indexOf(this.cariInput.value.toLowerCase()) > -1) {
					return true;
				};

				if (item.keterangan.toLocaleLowerCase().indexOf(this.cariInput.value.toLowerCase()) > -1) {
					return true;
				};

				return false;
			});
			this.renderList(this.anggotaRenderAr);
		}

		this.formSearch.onsubmit = () => {
			return false;
		}

		this.formTambahAnggota.onsubmit = () => {
			try {
				let anggota: AnggotaObj = new AnggotaObj();
				anggota.namaLengkap = this.inputNama.value;

				Util.loadingStart();
				this.client.anggota.insert(anggota).then((id: string) => {
					console.log('success ' + id);
					Util.bukaEditProfile(id, window.top.location.href);
					return false;
				}).catch((e: Error) => {
					console.log(e);
					console.log(e.message);
					Util.alertMsg(e.message);
					return false;
				});

				return false;
			}
			catch (e) {
				console.log(e);
				return false;
			}
		}

	}

	reload(): void {
		window.top.location.href = Util.urlDaftarAnggota + "?r=" + Math.floor(Math.random() * 1000);
	}

	renderList(items: AnggotaObj[]): void {
		this.listCont.innerText = '';

		items.sort((item1: AnggotaObj, item2: AnggotaObj) => {
			if (item1.namaLengkap.charCodeAt(0) > item2.namaLengkap.charCodeAt(0)) return 1;
			if (item1.namaLengkap.charCodeAt(0) < item2.namaLengkap.charCodeAt(0)) return -1;
			return 0;
		});

		items.forEach((item: AnggotaObj) => {
			let view: Item = new Item();
			console.log(item);
			view.id = item.id;
			view.namaP.innerHTML = item.namaLengkap + " (" + item.nama + ")" + " - " + item.keterangan;
			view.attach(this.listCont);

			view.elHtml.onclick = () => {
				console.log(view.elHtml.querySelectorAll('div.group-tbl'));
				this.listCont.querySelectorAll('div.group-tbl').forEach((item: Element) => {
					item.classList.add('sembunyi');
				})
				view.groupTbl.classList.remove('sembunyi');
			}

			view.silsilahTbl.onclick = () => {
				window.top.location.href = Util.urlHome + "?id=" + item.id;
			}

			view.editTbl.onclick = (e: MouseEvent) => {
				e.stopPropagation();
				console.log(view);
				Util.bukaEditProfile(view.id, window.top.location.href);
			}

			view.hapusTbl.onclick = (e: MouseEvent) => {
				e.stopPropagation();
				console.log(view);
				Util.loadingStart();
				this.client.anggota.hapus(view.id).then(() => {
					this.reload();
				}).catch((e: Error) => {
					Util.loadingEnd();
					console.log(e);
					Util.alertMsg(e.message);
				});
			}
		});
	}

	get listCont(): HTMLDivElement {
		return this.getEl('div.list-item-cont') as HTMLDivElement;
	}

	get inputNama(): HTMLInputElement {
		return this.getEl('form.form-anggota-baru input[name="nama"]') as HTMLInputElement;
	}

	get simpanTbl(): HTMLButtonElement {
		return this.getEl('form.form-anggota-baru button.simpan') as HTMLButtonElement;
	}

	get tambahTbl(): HTMLButtonElement {
		return this.getEl('button.tambah') as HTMLButtonElement;
	}

	get formTambahAnggota(): HTMLFormElement {
		return this.getEl('form.form-anggota-baru') as HTMLFormElement;
	}

	get formSearch(): HTMLFormElement {
		return this.getEl('form.search') as HTMLFormElement;
	}

	get cariInput(): HTMLInputElement {
		return this.getEl('input.search') as HTMLInputElement;
	}

}

new DaftarAnggotaPage();