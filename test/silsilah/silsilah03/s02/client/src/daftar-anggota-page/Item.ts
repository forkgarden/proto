import { BaseComponent } from "../ha/BaseComponent.js";

export class Item extends BaseComponent {
	private _id: string;

	constructor() {
		super();
		this._template = `
			<div class='list-item card'>
				<div class='card-body'>
					<p class='nama'></p>
					<div class='group-tbl sembunyi'>
						<button type="button" class='btn btn-primary edit'>edit</button>
						<button type="button" class='btn btn-secondary silsilah'>silsilah</button>
						<button type="button" class='btn btn-danger hapus'>hapus</button>
					</div>
				</div>
			</div>
		`;
		this.build();
	}

	get groupTbl(): HTMLDivElement {
		return this.getEl('div.group-tbl') as HTMLDivElement;
	}

	get namaP(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}

	get editTbl(): HTMLButtonElement {
		return this.getEl('button.edit') as HTMLButtonElement;
	}

	get hapusTbl(): HTMLButtonElement {
		return this.getEl('button.hapus') as HTMLButtonElement;
	}

	get silsilahTbl(): HTMLButtonElement {
		return this.getEl('button.silsilah') as HTMLButtonElement;
	}

	get id(): string {
		return this._id;
	}

	set id(value: string) {
		this._id = value;
	}


}