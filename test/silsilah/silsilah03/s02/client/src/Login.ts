import { Util } from "./Util.js"
// import { FireBaseClient } from "./server/firebase-client/FirebaseClient.js";
// import { IFBUserCredential } from "./IFirebase.js";
import { MySqlAdapter } from "./server/mysql/MysqlAdapter.js";

declare namespace firebase {
	namespace auth {
		namespace Auth {
			namespace Persistence {
				let SESSION: any;
				let NONE: any;
				let LOCAL: any;
			}
		}
	}
}

export class Login {
	private client: MySqlAdapter = new MySqlAdapter();
	private uid: string;

	constructor() {
		window.onload = () => {
			Util.loadingStart();
			this.init().then(() => {
				Util.loadingEnd();
				console.log('test');
				// console.log(firebase);
				// firebase.auth.Auth;
			}).catch((e: Error) => {
				Util.alertMsg(e.message);
			});
		}
	}

	async init(): Promise<void> {
		let id: string = '';
		let nama: string = '';

		//TODO: set default later

		id = Util.getQuery('id') || 'KxpUGabpITtGAEyHDJcL';
		nama = Util.getQuery('nama') || 'Baihaqi Sofwan';
		if (nama) nama = window.decodeURIComponent(nama);

		this.spanNama.innerHTML = nama;

		console.log('login click');
		await this.client.init();

		this.loginBtn.onclick = () => {
			Util.loadingStart();
			this.loginClick().then(() => {
				if (this.uid == "7S9TWZXggIPLWX6R8RGH0E7EOZ52") {
					window.top.location.href = './admin.html';
				}
				else {
					console.log('buka home');
					// console.log(this.uid);
					Util.bukaHome(id);
				}
			}).catch((e: Error) => {
				Util.alertMsg(e.message);
			});
		}

		// this.tblTamu.onclick = () => {
		// 	Util.bukaHome(id);
		// }
	}

	async loginClick(): Promise<void> {
		console.log('client login');
		await this.client.login(this.inputUsername.value, this.inputPassword.value, firebase.auth.Auth.Persistence.SESSION).then((data: any) => {
			// console.log(data);
			// console.log(data.credential ? data.credential.providerId : "");
			// console.log(data.credential ? data.credential.signInMethod : "");
			// console.log(data.user.displayName);
			// console.log(data.user.email);
			// console.log(data.user.uid);
			this.uid = data.user.uid;
		});
	}

	get inputUsername(): HTMLInputElement {
		return Util.getEl('input.username') as HTMLInputElement;
	}

	get inputPassword(): HTMLInputElement {
		return Util.getEl('input.password') as HTMLInputElement;
	}

	get loginBtn(): HTMLButtonElement {
		return Util.getEl('button.login') as HTMLButtonElement;
	}

	// get tblTamu(): HTMLButtonElement {
	// 	return Util.getEl('button.tamu') as HTMLButtonElement;
	// }

	get spanNama(): HTMLSpanElement {
		return Util.getEl('span.nama') as HTMLSpanElement;
	}
}

new Login();