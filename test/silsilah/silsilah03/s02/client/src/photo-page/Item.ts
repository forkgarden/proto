import { BaseComponent } from "../ha/BaseComponent.js";
import { Util } from "../Util.js";
import { FotoObj } from "../ent/FotoObj.js";

export class Item extends BaseComponent {
	private _foto: FotoObj;
	public get foto(): FotoObj {
		return this._foto;
	}
	public set foto(value: FotoObj) {
		this._foto = value;
	}

	constructor() {
		super();
		this._elHtml = Util.getTemplate('div.item');
	}

	get img(): HTMLImageElement {
		return this.getEl('img') as HTMLImageElement;
	}

	get hapusTbl(): HTMLButtonElement {
		return this.getEl('button.hapus') as HTMLButtonElement;
	}
}