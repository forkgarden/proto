import { Util } from "../Util.js";
import { AnggotaObj } from "../ent/AnggotaObj.js";
// import { MySqlAdapter } from "../server/firebase-client/MySqlAdapter.js";
import { RelPasanganObj } from "../ent/RelPasanganObj.js";
import { FotoObj } from "../ent/FotoObj.js";
import { MySqlAdapter } from "../server/mysql/MysqlAdapter.js";

export class ViewProfilePage {
	private server: MySqlAdapter = new MySqlAdapter();
	private id: string = '';
	private anggota: AnggotaObj;
	private urlBalik: string = '';
	private rel: RelPasanganObj;

	constructor() {
		Util.loadingStart();
		this.init().then(() => {
			Util.loadingEnd();
		}).catch((e: Error) => {
			console.log(e.message);
			console.log(e);
			Util.loadingEnd();
			Util.alertMsg(e.message, false);
		});
	}

	async ambilDataRelasi(): Promise<RelPasanganObj> {
		let rel: RelPasanganObj;

		console.group('ambil data relasi');
		console.log('data anggota');
		console.log(this.anggota);

		try {
			rel = await this.server.relasi.getByAnggotaId(this.anggota.id);
		}
		catch (e) {
			console.log(e);
		}

		console.log('hasil rel');
		console.log(rel);
		console.groupEnd();

		return rel;
	}

	async populateAnggota(): Promise<void> {
		if (this.id && this.id.length > 0) {
			let res: AnggotaObj[] = await this.server.anggota.getByKey("id", this.id);
			this.anggota = res[0];
			let foto: FotoObj = await this.server.foto.getByIdOrDefault(this.anggota.idFoto);
			this.anggota.fotoUrl = foto.photoUrl;
			let img: HTMLImageElement = (Util.getEl('div.cont div.edit-foto img') as HTMLImageElement);
			img.onerror = () => {
				img.src = Util.imgSalah;
			}
			img.src = foto.photoUrl;
		}
		else {
			throw new Error('user tidak ditemukan');
		}
	}

	populateIdDariPasangan(idAnggota: string, rel: RelPasanganObj): string {
		if (rel.anak1 == idAnggota) return rel.anak2;
		if (rel.anak2 == idAnggota) return rel.anak1;
		return null;
	}

	async renderPasangan(anggota: AnggotaObj, rel: RelPasanganObj): Promise<void> {
		let hasil: AnggotaObj = null;
		let id: string;
		let nama: string = '';
		let btnProfile: HTMLButtonElement = Util.getEl('button.pasangan.profile') as HTMLButtonElement;
		let btnSilsilah: HTMLButtonElement = Util.getEl('button.pasangan.silsilah') as HTMLButtonElement;

		if (!rel) {
			return;
		}

		id = this.populateIdDariPasangan(anggota.id, rel);
		if (!id) {
			return;
		}

		(btnProfile).onclick = () => {
			Util.bukaViewProfile(id, Util.urlHome + "?id=" + id);
		}

		(btnSilsilah).onclick = () => {
			Util.bukaHome(id);
		}

		try {
			hasil = await this.server.anggota.getByDoc(id);
			if (hasil) {
				nama = hasil.namaLengkap;
				Util.getEl('p.pasangan').innerHTML = nama;
				btnProfile.style.display = 'inline';
				btnSilsilah.style.display = 'inline';
			}
			else {

			}
		}
		catch (e) {
			console.log(e);
		}
	}

	populateUrl(): void {
		this.id = Util.getQuery('id');
		this.urlBalik = decodeURIComponent(Util.getQuery('url_balik'));

		console.log('url balik:');
		console.log(this.urlBalik);
	}

	normalize2(data: string): string {
		if ('' == data) return 'Tidak ada data';
		return data;
	}

	normalize(): void {
		// if (0 == this.anggota.tglLahir)
		// if ('' == this.anggota.nama) this.anggota.nama = 'Tidak ada data';
		// if ('' == this.anggota.alamat) this.anggota.alamat = 'Tidak ada data';
		this.anggota.nama = this.normalize2(this.anggota.nama);
		this.anggota.alamat = this.normalize2(this.anggota.alamat);
		this.anggota.namaLengkap = this.normalize2(this.anggota.namaLengkap);
		this.anggota.wa = this.normalize2(this.anggota.wa);
		this.anggota.facebook = this.normalize2(this.anggota.facebook);
		this.anggota.instagram = this.normalize2(this.anggota.instagram);
		this.anggota.linkedin = this.normalize2(this.anggota.linkedin);

	}

	renderSelf(): void {
		Util.getEl('p.nama-lengkap').innerHTML = this.anggota.namaLengkap;

		Util.getEl('p.nama').innerHTML = this.anggota.nama;

		Util.getEl('p.jkl').innerHTML = this.anggota.jkl;

		Util.getEl('p.alamat').innerHTML = this.anggota.alamat;

		Util.getEl('p.tgl-lahir').innerHTML = Util.date2Input(this.anggota.tglLahir);

		Util.getEl('p.tgl-meninggal').innerHTML = Util.date2Input(this.anggota.tglMeninggal);

		//akun sosial
		Util.getEl('p.wa').innerHTML = this.anggota.wa;

		Util.getEl('p.instagram').innerHTML = this.anggota.instagram;

		Util.getEl('p.fb').innerHTML = this.anggota.facebook;

		Util.getEl('p.linkedin').innerHTML = this.anggota.linkedin;
	}

	async renderAnak(): Promise<void> {
		console.group('render anak');

		if (!this.rel) {
			console.log('rel tidak ada');
			console.groupEnd();
			return;
		}

		let anaks: AnggotaObj[] = [];

		for (let i: number = 0; i < this.rel.anaks.length; i++) {
			try {
				let anak: AnggotaObj = await this.server.anggota.getByDoc(this.rel.anaks[i]);
				if (!anak) {
					anaks.push(Util.anakError());
				}
				else {
					anaks.push(anak);
				}
			}
			catch (e) {
				anaks.push(Util.anakError());
			}
		}

		console.log('anaks:');
		console.log(anaks);

		anaks.forEach((item: AnggotaObj) => {
			let view: HTMLElement;
			let anakCont: HTMLDivElement = Util.getEl('div.anak-cont') as HTMLDivElement;

			view = Util.getTemplate('div.item-anak');
			view.querySelector('p.nama').innerHTML = item.namaLengkap;
			(view.querySelector('button.profile') as HTMLButtonElement).onclick = () => {
				Util.bukaViewProfile(item.id, Util.urlHome + "?id=" + item.id);
			}

			(view.querySelector('button.silsilah') as HTMLButtonElement).onclick = () => {
				Util.bukaHome(item.id);
			}

			anakCont.appendChild(view);
		});

		if (anaks.length > 0) {
			Util.getEl('p.anak.kosong').style.display = 'none';
		}

		console.groupEnd();
	}

	populateLinkSilsilah(): void {
		let linkStr = Util.urlLink + "?id=" + this.id + "&nama=" + window.encodeURIComponent(this.anggota.namaLengkap); 
		this.link.href = linkStr;
		this.link.innerHTML = linkStr;
	}

	async init(): Promise<void> {
		await this.server.init();

		this.populateUrl();
		await this.populateAnggota();
		this.populateLinkSilsilah();

		this.rel = await this.ambilDataRelasi();
		this.normalize();
		this.renderSelf();
		await this.renderPasangan(this.anggota, this.rel);
		await this.renderAnak();

		this.tutupBtn.onclick = () => {
			if (this.urlBalik) {
				window.top.location.href = this.urlBalik;
			}
		}
	}

	get pasanganInput(): HTMLInputElement {
		return Util.getEl('div.pasangan input.nama') as HTMLInputElement;
	}

	get pilihAnakFragment(): HTMLDivElement {
		return Util.getEl('div.pilih-anak') as HTMLDivElement;
	}

	get tutupBtn(): HTMLButtonElement {
		return Util.getEl('button.tutup') as HTMLButtonElement;
	}

	get link(): HTMLLinkElement {
		return Util.getEl('a.link-silsilah') as HTMLLinkElement;
	}

}

new ViewProfilePage();