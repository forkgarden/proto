import { BaseComponent } from "../ha/BaseComponent.js";
import { Util } from "../Util.js";

export class ItemAnak extends BaseComponent {
	constructor() {
		super();
		this._elHtml = Util.getTemplate('div.item-anak');
	}

	get namaP(): HTMLParagraphElement {
		return this.getEl("p.nama") as HTMLParagraphElement;
	}

	get atasTbl(): HTMLButtonElement {
		return this.getEl('button.atas') as HTMLButtonElement;
	}

	get bawahTbl(): HTMLButtonElement {
		return this.getEl('button.bawah') as HTMLButtonElement;
	}

	get hapusTbl(): HTMLButtonElement {
		return this.getEl('button.hapus') as HTMLButtonElement;
	}

}