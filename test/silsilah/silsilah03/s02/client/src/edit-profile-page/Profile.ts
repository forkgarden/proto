import { BaseComponent } from "../ha/BaseComponent.js";
import { Util } from "../Util.js";
// import { MySqlAdapter } from "../server/firebase-client/MySqlAdapter.js";
import { AnggotaObj } from "../ent/AnggotaObj.js";
import { MySqlAdapter } from "../server/mysql/MysqlAdapter.js";

export class Profile extends BaseComponent {
	private server: MySqlAdapter;
	private anggota: AnggotaObj;

	constructor() {
		super();
	}

	async init(anggota: AnggotaObj, server: MySqlAdapter): Promise<void> {
		this.server = server;
		this.anggota = anggota;

		this.inputNama.value = anggota.nama;
		this.inputAlamat.value = anggota.alamat;
		this.inputNamaLengkap.value = anggota.namaLengkap;
		this.setInputJKL(anggota.jkl);
		this.inputTglLahir.value = Util.date2Input(anggota.tglLahir);
		this.inputTglMeninggal.value = Util.date2Input(anggota.tglMeninggal);
		this.inputFacebook.value = anggota.facebook;
		this.inputWA.value = anggota.wa;
		this.inputLinkedIn.value = anggota.linkedin;
		this.inputInstagram.value = anggota.instagram;
		this.inputKeterangan.value = anggota.keterangan;

		this._elHtml = Util.getEl('div.cont form.profile');

		this.form.onsubmit = () => {
			try {
				Util.loadingStart();
				this.simpanClick().then(() => {
					// window.top.location.reload();
					console.log('success');
					Util.loadingEnd();
				}).catch((e: Error) => {
					console.log(e);
					Util.loadingEnd();
					Util.alertMsg(e.message);
				});
			}
			catch (e) {
				Util.alertMsg(e.message);
			}
			return false;
		}

	}

	async simpanClick(): Promise<void> {

		console.log('jkl');

		this.anggota.nama = this.inputNama.value;
		this.anggota.alamat = this.inputAlamat.value;
		this.anggota.namaLengkap = this.inputNamaLengkap.value;
		this.anggota.jkl = this.getInputJKL();

		this.anggota.facebook = this.inputFacebook.value;
		this.anggota.instagram = this.inputInstagram.value;
		this.anggota.linkedin = this.inputLinkedIn.value;
		this.anggota.wa = this.inputWA.value;
		this.anggota.keterangan = this.inputKeterangan.value;

		this.anggota.tglLahir = Util.Input2Date(this.inputTglLahir.value);
		this.anggota.tglMeninggal = Util.Input2Date(this.inputTglMeninggal.value);

		console.log('simpan profile');
		console.log(this.anggota);

		await this.server.anggota.update(this.anggota);
	}

	get simpanTbl(): HTMLButtonElement {
		return Util.getEl('form.profile button.ok') as HTMLButtonElement;
	}

	get inputNama(): HTMLInputElement {
		return Util.getEl('form.profile input.nama') as HTMLInputElement;
	}

	get inputNamaLengkap(): HTMLInputElement {
		return Util.getEl('form.profile input.nama-lengkap') as HTMLInputElement;
	}

	get inputAlamat(): HTMLTextAreaElement {
		return Util.getEl('form.profile textarea.alamat') as HTMLTextAreaElement;
	}

	get inputKeterangan(): HTMLTextAreaElement {
		return Util.getEl('form.profile textarea.keterangan') as HTMLTextAreaElement;
	}

	get inputTglLahir(): HTMLInputElement {
		return Util.getEl('form.profile input.tgl-lahir') as HTMLInputElement;
	}

	get inputTglMeninggal(): HTMLInputElement {
		return Util.getEl('form.profile input.tgl-meninggal') as HTMLInputElement;
	}

	get inputWA(): HTMLInputElement {
		return Util.getEl('form.profile input.wa') as HTMLInputElement;
	}

	get inputFacebook(): HTMLInputElement {
		return Util.getEl('form.profile input.facebook') as HTMLInputElement;
	}

	get inputLinkedIn(): HTMLInputElement {
		return Util.getEl('form.profile input.linkedin') as HTMLInputElement;
	}

	get inputInstagram(): HTMLInputElement {
		return Util.getEl('form.profile input.instagram') as HTMLInputElement;
	}

	get form(): HTMLFormElement {
		return Util.getEl('form.profile') as HTMLFormElement;
	}

	setInputJKL(value: string) {
		document.querySelectorAll('input[name="jkl"]').forEach((item: Element) => {
			if (item.getAttribute("value") == value) {
				item.setAttribute("checked", 'true');
			}
		});
	}

	getInputJKL() {
		let res;
		res = document.querySelector('input[name="jkl"]:checked').getAttribute('value');
		// document.querySelectorAll("input[name='jkl']").forEach((item: Element) => {
		// 	if (item.getAttribute("checked")) {
		// 		res = item.getAttribute("value");
		// 	}
		// 	else {
		// 		console.log(item.getAttribute("checked"));
		// 	}
		// });
		return res;
	}


}