import { BaseComponent } from "../ha/BaseComponent.js";
import { Util } from "../Util.js";
// import { MySqlAdapter } from "../server/firebase-client/MySqlAdapter.js";
import { FotoObj } from "../ent/FotoObj.js";
// import { PilihFoto } from "./PilihFoto.js";
import { AnggotaObj } from "../ent/AnggotaObj.js";
import { MySqlAdapter } from "../server/mysql/MysqlAdapter.js";

export class EditFoto extends BaseComponent {
	private client: MySqlAdapter;
	private anggota: AnggotaObj;
	private fotoObj: FotoObj;

	constructor() {
		super();
	}

	async init(client: MySqlAdapter, anggota: AnggotaObj): Promise<void> {
		this.anggota = anggota;
		this.client = client;

		this._elHtml = Util.getEl('div.edit-foto');

		this.editTbl.onclick = () => {
			Util.loadingStart();
			this.editTblClick2().then(() => {
				Util.loadingEnd();
			}).catch((e: Error) => {
				console.log(e);
				Util.loadingEnd();
				Util.alertMsg(e.message);
			});
		}

		this.hapusTbl.onclick = () => {
			Util.loadingStart();
			this.hapusPhoto().then(() => {
				Util.loadingEnd();
				this.img.src = Util.defImage;
				// window.top.location.reload();
			}).catch((e: Error) => {
				Util.alertMsg(e.message);
			});
		}

		await this.loadImage();

	}

	async hapusPhoto(): Promise<void> {
		this.client.foto.hapus(this.fotoObj);
	}

	async editTblClick2(): Promise<void> {
		// window.top.lo
		Util.bukaUploadPhoto2(this.anggota.id, window.top.location.href);
	}

	// async editTblClick(): Promise<void> {
	// 	console.log('edit foto click');
	// 	await this.pilihFoto.render();
	// 	this.pilihFoto.elHtml.style.display = 'flex';

	// 	this.pilihFoto.fotoDipilih = (foto: FotoObj) => {
	// 		console.log('foto dipilih');
	// 		Util.loadingStart();

	// 		if (await this.fotoDipilih(foto)) {

	// 		}
	// 		else {

	// 		}
	// 		this.fotoDipilih(foto).then(() => {
	// 			Util.loadingEnd();
	// 			window.top.location.reload();
	// 		}).catch((e: Error) => {
	// 			console.log(e);
	// 			Util.loadingEnd();
	// 			Util.alertMsg(e.message);
	// 		});
	// 	}

	// 	this.pilihFoto.batalBtn.onclick = () => {
	// 		this.pilihFoto.detach();
	// 	}
	// }

	// async fotoDipilih(foto: FotoObj): Promise<void> {
	// 	this.img.src = foto.photoUrl;
	// 	this.anggota.idFoto = foto.id;
	// 	await this.client.anggota.update(this.anggota);
	// }

	async loadImage(): Promise<void> {
		console.group('load photo');
		console.log('id foto ' + this.anggota.idFoto);
		console.log('foto obj:');
		console.log(this.fotoObj);

		if ("" == this.anggota.idFoto) {
			console.log('id foto tidak ada');
			console.groupEnd();
			return;
		}

		await this.client.foto.getById(this.anggota.idFoto).then((item: FotoObj) => {
			if (item) {
				this.fotoObj = item;
				this.img.src = item.photoUrl;
				this.editTbl.style.display = 'none';
				this.hapusTbl.style.display = 'block';
			}
			else {
				this.editTbl.style.display = 'block';
				this.hapusTbl.style.display = 'none';
			}
		});

		console.log('hasil:');
		console.log(this.fotoObj);
		console.groupEnd();
	}

	get img(): HTMLImageElement {
		return this.getEl('img') as HTMLImageElement;
	}

	get editTbl(): HTMLButtonElement {
		return this.getEl('button.edit') as HTMLButtonElement;
	}

	get hapusTbl(): HTMLButtonElement {
		return this.getEl('button.hapus') as HTMLButtonElement;
	}
}

