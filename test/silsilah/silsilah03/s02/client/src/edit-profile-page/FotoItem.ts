import { BaseComponent } from "../ha/BaseComponent.js";
import { Util } from "../Util.js";

export class FotoItem extends BaseComponent {
	constructor() {
		super();
		this._elHtml = Util.getTemplate('div.item-foto');
	}

	init(): void {

	}

	get img(): HTMLImageElement {
		return this.getEl('img') as HTMLImageElement;
	}
}