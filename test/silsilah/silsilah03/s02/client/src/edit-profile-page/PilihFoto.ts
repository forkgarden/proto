import { BaseComponent } from "../ha/BaseComponent.js";
import { Util } from "../Util.js";
// import { MySqlAdapter } from "../server/firebase-client/MySqlAdapter.js";
import { FotoObj } from "../ent/FotoObj.js";
import { FotoItem } from "./FotoItem.js";
import { MySqlAdapter } from "../server/mysql/MysqlAdapter.js";

export class PilihFoto extends BaseComponent {
	private _fotoDipilih: Function;
	private client: MySqlAdapter;

	public set fotoDipilih(value: Function) {
		this._fotoDipilih = value;
	}

	constructor() {
		super();
	}

	init(client: MySqlAdapter): void {
		this.client = client;
		this._elHtml = Util.getEl('div.pilih-foto') as HTMLDivElement;
	}

	async render(): Promise<void> {
		console.log('render pilih foto');
		let foto: FotoObj[] = await this.client.foto.get();

		this.listCont.innerHTML = '';

		// console.log(foto);

		foto.forEach((item: FotoObj) => {
			let view: FotoItem = new FotoItem();

			view.img.src = item.thumbUrl;
			view.attach(this.listCont);

			view.elHtml.onclick = () => {
				this._fotoDipilih(item);
			}
		});
	}

	get listCont(): HTMLDivElement {
		return this.getEl('div.list-cont') as HTMLDivElement;
	}

	get batalBtn(): HTMLButtonElement {
		return this.getEl('button.batal') as HTMLButtonElement;
	}
}