import { BaseComponent } from "../ha/BaseComponent.js";
import { Util } from "../Util.js";
import { AnggotaObj } from "../ent/AnggotaObj.js";
import { MySqlAdapter } from "../server/mysql/MysqlAdapter.js";
// import { MySqlAdapter } from "../server/firebase-client/MySqlAdapter.js";

export interface HandlerPilihAnggota {
	anggotaDipilih(anggota: AnggotaObj, type: number): Promise<void>;
}

export class PilihAnggota extends BaseComponent {
	static readonly PILIH_PASANGAN: number = 1;
	static readonly PILIH_ANAK: number = 2;

	private server: MySqlAdapter;
	private anggotaAr: AnggotaObj[] = [];
	private handler: HandlerPilihAnggota;
	private mode: number;

	constructor() {
		super();
		this._elHtml = Util.getEl('div.cont div.pilih-anak');
	}

	init(server: MySqlAdapter): void {
		this.server = server;

		this.tutupTbl.onclick = () => {
			this._elHtml.style.display = 'none';
		}

		this.formSearch.onsubmit = () => {
			return false;
		}

		this.cariInput.oninput = () => {
			let anggotaRenderAr: AnggotaObj[] = [];

			anggotaRenderAr = this.anggotaAr.filter((item: AnggotaObj) => {
				if (item.namaLengkap.toLocaleLowerCase().indexOf(this.cariInput.value.toLowerCase()) > -1) {
					return true;
				};

				if (item.keterangan.toLocaleLowerCase().indexOf(this.cariInput.value.toLowerCase()) > -1) {
					return true;
				};

				return false;
			});
			this.render(anggotaRenderAr);
		}
	}

	async ambilDataSemuaAnggota(): Promise<AnggotaObj[]> {
		return this.server.anggota.get();
	}

	render(anggotaAr: AnggotaObj[]): void {
		this.listCont.innerHTML = '';

		for (let i: number = 0; i < anggotaAr.length; i++) {
			let anggota: AnggotaObj = anggotaAr[i];
			let view: ItemPilihAnggota = new ItemPilihAnggota();
			view.namaP.innerHTML = anggota.namaLengkap + " (" + anggota.nama + ") - " + anggota.keterangan;
			view.anggota = anggota;
			view.attach(this.listCont);
			view.elHtml.onclick = () => {
				view.elHtml.onclick = null;
				this.itemOnClick(anggota);
			}
		}
	}

	itemOnClick(anggota: AnggotaObj): void {
		Util.loadingStart();
		this.handler.anggotaDipilih(anggota, this.mode).then(() => {
			// window.top.location.reload();
		}).catch((e: Error) => {
			console.log(e);
			Util.loadingEnd();
			Util.alertMsg(e.message);
		});

	}

	async tampil(judul: string, handler: HandlerPilihAnggota, mode: number): Promise<void> {
		// let anggotaAr: AnggotaObj[] = [];

		this._elHtml.style.display = 'flex';
		this.judulP.innerHTML = judul;
		this.handler = handler;
		this.mode = mode;

		Util.loadingStart();
		this.anggotaAr = await this.ambilDataSemuaAnggota();
		this.anggotaAr.sort((item1: AnggotaObj, item2: AnggotaObj) => {
			if (item1.namaLengkap < item2.namaLengkap) return -1;
			if (item1.namaLengkap > item2.namaLengkap) return 1;

			return 0;
		})

		this.render(this.anggotaAr);
		Util.loadingEnd();
	}

	private get listCont(): HTMLDivElement {
		return this.getEl('div.list-cont') as HTMLDivElement;
	}

	private get judulP(): HTMLParagraphElement {
		return this.getEl('p.judul') as HTMLParagraphElement;
	}

	private get tutupTbl(): HTMLButtonElement {
		return this.getEl('button.tutup') as HTMLButtonElement;
	}

	get formSearch(): HTMLFormElement {
		return this.getEl('form.search') as HTMLFormElement;
	}

	get cariInput(): HTMLInputElement {
		return this.getEl('input.search') as HTMLInputElement;
	}
}


export class ItemPilihAnggota extends BaseComponent {
	private _anggota: AnggotaObj;

	constructor() {
		super();
		this._elHtml = Util.getTemplate('div.item-anak-dipilih');
	}

	get namaP(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}

	public get anggota(): AnggotaObj {
		return this._anggota;
	}
	public set anggota(value: AnggotaObj) {
		this._anggota = value;
	}
}