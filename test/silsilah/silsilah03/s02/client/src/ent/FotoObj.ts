export class FotoObj implements IPhotoObj {
	private _thumbUrl: string = '';
	private _photoUrl: string = '';
	private _id: string = '';
	private _idPhoto: string = '';
	private _idThumb: string = '';

	public get idPhoto(): string {
		return this._idPhoto;
	}

	public set idPhoto(value: string) {
		this._idPhoto = value;
	}

	public get idThumb(): string {
		return this._idThumb;
	}

	public set idThumb(value: string) {
		this._idThumb = value;
	}

	public get id(): string {
		return this._id;
	}
	public set id(value: string) {
		this._id = value;
	}
	public get photoUrl(): string {
		return this._photoUrl;
	}
	public set photoUrl(value: string) {
		this._photoUrl = value;
	}
	public get thumbUrl(): string {
		return this._thumbUrl;
	}
	public set thumbUrl(value: string) {
		this._thumbUrl = value;
	}
}

export interface IPhotoObj {
	id: string;
	photoUrl: string;
	thumbUrl: string;
	idPhoto: string;
	idThumb: string;
}