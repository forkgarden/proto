export class RelAnak2 implements IRelAnak2 {
	private _relPasanganId: string = '';
	private _anakId: string = '';
	private _urutan: number = 0;
	private _id: string = '';

	public get id(): string {
		return this._id;
	}
	public set id(value: string) {
		this._id = value;
	}

	public get relPasanganId(): string {
		return this._relPasanganId;
	}
	public set relPasanganId(value: string) {
		this._relPasanganId = value;
	}

	public get urutan(): number {
		return this._urutan;
	}
	public set urutan(value: number) {
		this._urutan = value;
	}

	public get anakId(): string {
		return this._anakId;
	}
	public set anakId(value: string) {
		this._anakId = value;
	}
}

export interface IRelAnak2 {
	id: string;
	relPasanganId: string;
	anakId: string;
	urutan: number;
}