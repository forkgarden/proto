export class RelPasanganObj implements IRelPasangan {
	private _anak1: string = '';
	private _anak2: string = '';
	private _id: string = '';
	private _anaks: string[] = [];
	private _anakStr: string;

	public get anakStr(): string {
		return this._anakStr;
	}
	public set anakStr(value: string) {
		this._anakStr = value;
	}

	public get anaks(): string[] {
		return this._anaks;
	}
	public set anaks(value: string[]) {
		this._anaks = value;
	}

	public get id(): string {
		return this._id;
	}
	public set id(value: string) {
		this._id = value;
	}

	public get anak2(): string {
		return this._anak2;
	}
	public set anak2(value: string) {
		this._anak2 = value;
	}
	public get anak1(): string {
		return this._anak1;
	}
	public set anak1(value: string) {
		this._anak1 = value;
	}
}

export interface IRelPasangan {
	id: string;
	anak1: string;
	anak2: string;
	anaks: string[];
	anakStr: string;
}

// export interface IRelPasanganDbo {
// 	id: string;
// 	anak1: string;
// 	anak2: string;
// 	anaks: string;
// }
