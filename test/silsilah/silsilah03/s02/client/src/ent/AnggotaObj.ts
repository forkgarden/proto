export class AnggotaObj implements IAnggota {
	private _nama: string = '';
	private _id: string = '';
	private _idFoto: string = '';
	private _namaLengkap: string = '';
	private _alamat: string = '';
	private _tglLahir: number = NaN;
	private _tglMeninggal: number = NaN;
	private _wa: string = '';
	private _facebook: string = '';
	private _instagram: string = '';
	private _linkedin: string = '';
	private _jkl: string = '';
	private _keterangan: string = '';

	//populated object
	private _thumbUrl: string = '';
	private _fotoUrl: string = '';
	private _isDefault: boolean = false;
	private _anakNama: string[] = [];
	private _pasanganNama: string = '';
	private _ortuNama: string = '';
	private _ortuId: string = '';
	private _anakIds: string[] = [];

	public get anakIds(): string[] {
		return this._anakIds;
	}
	public set anakIds(value: string[]) {
		this._anakIds = value;
	}

	public get ortuNama(): string {
		return this._ortuNama;
	}
	public set ortuNama(value: string) {
		this._ortuNama = value;
	}
	public get ortuId(): string {
		return this._ortuId;
	}
	public set ortuId(value: string) {
		this._ortuId = value;
	}

	public get anaks(): string[] {
		return this._anakNama;
	}
	public set anaks(value: string[]) {
		this._anakNama = value;
	}
	public get pasanganNama(): string {
		return this._pasanganNama;
	}
	public set pasanganNama(value: string) {
		this._pasanganNama = value;
	}

	public get keterangan(): string {
		return this._keterangan;
	}
	public set keterangan(value: string) {
		this._keterangan = value;
	}

	public get isDefault(): boolean {
		return this._isDefault;
	}
	public set isDefault(value: boolean) {
		this._isDefault = value;
	}

	public get fotoUrl(): string {
		return this._fotoUrl;
	}
	public set fotoUrl(value: string) {
		this._fotoUrl = value;
	}

	public get thumbUrl(): string {
		return this._thumbUrl;
	}
	public set thumbUrl(value: string) {
		this._thumbUrl = value;
	}

	public get jkl(): string {
		return this._jkl;
	}
	public set jkl(value: string) {
		this._jkl = value;
	}

	public get namaLengkap(): string {
		return this._namaLengkap;
	}
	public set namaLengkap(value: string) {
		this._namaLengkap = value;
	}
	public get alamat(): string {
		return this._alamat;
	}
	public set alamat(value: string) {
		this._alamat = value;
	}
	public get tglLahir(): number {
		return this._tglLahir;
	}
	public set tglLahir(value: number) {
		this._tglLahir = value;
	}
	public get tglMeninggal(): number {
		return this._tglMeninggal;
	}
	public set tglMeninggal(value: number) {
		this._tglMeninggal = value;
	}
	public get wa(): string {
		return this._wa;
	}
	public set wa(value: string) {
		this._wa = value;
	}
	public get facebook(): string {
		return this._facebook;
	}
	public set facebook(value: string) {
		this._facebook = value;
	}
	public get linkedin(): string {
		return this._linkedin;
	}
	public set linkedin(value: string) {
		this._linkedin = value;
	}
	public get instagram(): string {
		return this._instagram;
	}
	public set instagram(value: string) {
		this._instagram = value;
	}

	public get idFoto(): string {
		return this._idFoto;
	}
	public set idFoto(value: string) {
		this._idFoto = value;
	}

	public get nama(): string {
		return this._nama;
	}
	public set nama(value: string) {
		this._nama = value;
	}

	public get id(): string {
		return this._id;
	}
	public set id(value: string) {
		this._id = value;
	}
}

export interface IAnggota {
	nama: string;
	id: string;
	idFoto: string;
	// orangTuaId: string;
	namaLengkap: string;
	alamat: string;
	tglLahir: number;
	tglMeninggal: number;
	wa: string;
	facebook: string;
	linkedin: string;
	instagram: string;
	jkl: string;
	keterangan: string;
}