import { RelPasanganObj, IRelPasangan } from "../../ent/RelPasanganObj.js";
import { Util } from "../../Util.js";

export class Relasi {
	private path: string = '/relasi';

	async hapus(rel: RelPasanganObj): Promise<void> {
		await Util.Ajax('post', this.path + '/hapus', rel.id);
	}

	async update(_rel: RelPasanganObj): Promise<void> {
		await Util.Ajax('post', this.path + '/update', JSON.stringify(_rel));
	}

	async getByAnakId(id: string): Promise<RelPasanganObj> {
		let xml: XMLHttpRequest;
		let hasil: IRelPasangan[] = [];
		let hasil2: RelPasanganObj[] = [];

		xml = await Util.Ajax('post', this.path + '/getbyanakid', id);
		hasil = JSON.parse(xml.responseText);

		hasil.forEach((item: IRelPasangan) => {
			hasil2.push(this.fromObj(item, item.id));
		});

		return hasil2[0];

	}

	async getByAnggotaId(id: string): Promise<RelPasanganObj> {
		let xml: XMLHttpRequest;
		let hasil: IRelPasangan[] = [];
		let hasil2: RelPasanganObj[] = [];

		xml = await Util.Ajax('post', this.path + '/getbyanggotaid', id);
		hasil = JSON.parse(xml.responseText);

		hasil.forEach((item: IRelPasangan) => {
			hasil2.push(this.fromObj(item, item.id));
		});

		return hasil2[0];
	}

	async insert(rel: RelPasanganObj): Promise<string> {
		let xml: XMLHttpRequest = await Util.Ajax('post', this.path + '/insert', JSON.stringify(rel));
		return xml.responseText;
	}

	toObj(rel: RelPasanganObj): IRelPasangan {
		return {
			id: rel.id,
			anak1: rel.anak1,
			anak2: rel.anak2,
			anakStr: JSON.stringify(rel.anaks),
			anaks: rel.anaks
		}
	}

	fromObj(rel: IRelPasangan, id: string): RelPasanganObj {
		let hasil: RelPasanganObj = new RelPasanganObj();
		let anaks: string[] = JSON.parse(rel.anakStr);

		if (!id) throw new Error();
		if ("" == id) throw new Error();

		hasil.id = id;
		hasil.anak1 = rel.anak1 ? rel.anak1 : "";
		hasil.anak2 = rel.anak2 ? rel.anak2 : "";
		hasil.anaks = anaks ? anaks : []
		hasil.anakStr = rel.anakStr;
		return hasil;
	}

}