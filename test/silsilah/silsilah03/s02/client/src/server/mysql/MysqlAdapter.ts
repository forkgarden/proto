import { Anggota } from "./Anggota.js";
import { Foto } from "./Foto.js";
import { Relasi } from "./Relasi.js";
import { Auth } from "./Auth.js";

export class MySqlAdapter {
	private _anggota: Anggota = new Anggota();
	private _foto: Foto = new Foto();
	private _relasi: Relasi = new Relasi();
	// private firebaseConnector: FireBaseConnector = new FireBaseConnector();
	private auth: Auth = new Auth();

	constructor() {

	}

	async init(): Promise<any> {
		// await this.firebaseConnector.init();
		await this.auth.init();
		await this.foto.init();
	}

	//TODO: login dengan firebase untuk foto dan kemungkinan buat user juga dengan memperhatikan use session
	async login(username: string, password: string, persistence: string): Promise<void> {
		await this.auth.login(username, password, persistence);

		//TODO: login mysql
		username;
		password;
		persistence;
		return null;
	}

	public get foto(): Foto {
		return this._foto;
	}

	public get anggota(): Anggota {
		return this._anggota;
	}

	public get relasi(): Relasi {
		return this._relasi;
	}
}