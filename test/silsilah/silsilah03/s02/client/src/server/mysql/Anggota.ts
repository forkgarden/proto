import { AnggotaObj, IAnggota } from "../../ent/AnggotaObj.js";
import { Util } from "../../Util.js";

export class Anggota {
	private path: string = '/anggota';

	async update(anggota: AnggotaObj): Promise<void> {
		await Util.Ajax('post', this.path + '/update', JSON.stringify(anggota));
	}

	async hapus(id: string): Promise<void> {
		await Util.Ajax('post', this.path + '/hapus', id);
	}

	async get(): Promise<AnggotaObj[]> {
		let xml: XMLHttpRequest = await Util.Ajax('post', this.path + '/getall', null);
		let str: string = xml.responseText;
		return JSON.parse(str);
	}

	async getByDoc(key: string): Promise<AnggotaObj> {
		return (await this.getByKey('id', key))[0];
	}

	async getByKey(key: string, value: string): Promise<AnggotaObj[]> {
		let xml: XMLHttpRequest;
		let data: any = [key, value];
		let hasil: IAnggota[] = [];
		let hasil2: AnggotaObj[] = [];

		xml = await Util.Ajax('post', this.path + '/get', JSON.stringify(data));
		hasil = JSON.parse(xml.responseText);

		hasil.forEach((item: IAnggota) => {
			hasil2.push(this.fromObj(item, item.id));
		});

		return hasil2;
	}

	fromObj(obj: IAnggota, id: string): AnggotaObj {
		let hasil: AnggotaObj = new AnggotaObj();

		hasil.id = id;
		hasil.nama = obj.nama ? obj.nama : '';
		hasil.namaLengkap = obj.namaLengkap || '';
		hasil.idFoto = obj.idFoto ? obj.idFoto : '';
		hasil.alamat = obj.alamat || '';
		hasil.facebook = obj.facebook || '';
		hasil.instagram = obj.instagram || '';
		hasil.wa = obj.wa || '';
		hasil.linkedin = obj.linkedin || '';
		hasil.tglLahir = obj.tglLahir || 0;
		hasil.tglMeninggal = obj.tglMeninggal || 0;
		hasil.jkl = obj.jkl || 'L';
		hasil.keterangan = obj.keterangan || '';

		return hasil;
	}

	toObj(data: IAnggota): IAnggota {
		let obj: IAnggota = {
			id: data.id,
			nama: data.nama,
			idFoto: data.idFoto ? data.idFoto : "",
			// orangTuaId: data.orangTuaId ? data.orangTuaId : "",
			alamat: data.alamat,
			facebook: data.facebook,
			instagram: data.instagram,
			linkedin: data.linkedin,
			namaLengkap: data.namaLengkap,
			tglLahir: data.tglLahir,
			tglMeninggal: data.tglMeninggal,
			wa: data.wa,
			jkl: data.jkl,
			keterangan: data.keterangan
		}

		return obj;
	}

	async insert(anggota: AnggotaObj): Promise<string> {
		let xml: XMLHttpRequest = await Util.Ajax('post', this.path + '/insert', JSON.stringify(anggota));
		return xml.responseText;
	}
}