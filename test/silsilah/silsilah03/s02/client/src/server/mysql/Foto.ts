import { FotoObj, IPhotoObj } from "../../ent/FotoObj.js";
import { Util } from "../../Util.js";

export class Foto {
	private path: string = '/anggota';


	async init(): Promise<void> {
	}

	async hapus(foto: FotoObj): Promise<void> {
		await Util.Ajax('post', this.path + '/hapus', JSON.stringify({ id: foto.id }));
	}

	async fotoInsert(foto: FotoObj): Promise<string> {
		let xml: XMLHttpRequest = await Util.Ajax('post', this.path + '/baru', JSON.stringify({ id: foto.id }));
		return xml.responseText;
	}

	async getByIdOrDefault(id: string): Promise<FotoObj> {
		return this.getById(id);
	}

	async getById(id: string): Promise<FotoObj> {
		let xml: XMLHttpRequest;

		xml = await Util.Ajax('post', this.path + '/ambil', JSON.stringify({ id: id }));
		let hasil: IPhotoObj[] = JSON.parse(xml.responseText);
		let hasil2: FotoObj = this.fromObj(hasil[0], id);

		return hasil2;
	}

	async get(): Promise<FotoObj[]> {
		let xml: XMLHttpRequest;

		xml = await Util.Ajax('post', this.path + '/ambil-semua', null);
		let hasil: IPhotoObj[] = JSON.parse(xml.responseText);
		let hasil2: FotoObj[] = [];

		hasil.forEach((item: IPhotoObj) => {
			hasil2.push(this.fromObj(item, item.id));
		});

		return hasil2;
	}

	fromObj(obj: IPhotoObj, id: string): FotoObj {
		let rel: FotoObj = new FotoObj();

		console.log('form obj');
		console.log(obj);

		rel.id = id;
		rel.photoUrl = obj.photoUrl;
		rel.thumbUrl = obj.thumbUrl;
		rel.idPhoto = obj.idPhoto;
		rel.idThumb = obj.idThumb;

		return rel;
	}

	toObj(rel: FotoObj): IPhotoObj {
		return {
			id: rel.id,
			photoUrl: rel.photoUrl,
			thumbUrl: rel.thumbUrl,
			idPhoto: rel.idPhoto,
			idThumb: rel.idThumb
		}
	}

	createName(): string {
		let hasil: string = 'foto';

		for (let i: number = 0; i < 10; i++) {
			hasil += Math.floor(Math.random() * 10);
		}
		let date: Date = new Date();
		hasil += '_' + Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());

		return hasil;
	}

	async upload(canvas: HTMLCanvasElement): Promise<IUResult> {
		let resp: XMLHttpRequest = await Util.Ajax('post', this.path + '/hapus', canvas.toDataURL());
		let result: IUResult = JSON.parse(resp.responseText);
		return result;
	}

}

export interface IUResult {
	name: string;
	url: string;
}