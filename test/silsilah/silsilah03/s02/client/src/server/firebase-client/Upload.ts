import { IUploadTask, IFBStorage, IFBReference, IFireBase } from "../../IFirebase";

declare var firebase: IFireBase;


export class Upload {

	async init(): Promise<any> {

		return Promise.resolve();
	}

	createName(): string {
		let hasil: string = 'foto';

		for (let i: number = 0; i < 10; i++) {
			hasil += Math.floor(Math.random() * 10);
		}
		let date: Date = new Date();
		hasil += '_' + Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());

		return hasil;
	}

	upload(dataUrl: string): IUploadTask {
		let storage: IFBStorage = firebase.storage();
		let storageRef: IFBReference = storage.ref();
		let fileRef: IFBReference;
		let name: string = this.createName();

		console.log(name);

		try {
			fileRef = storageRef.child('silsilah/' + name);
		}
		catch (error) {
			console.log(error);
		}

		console.log('file ref' + fileRef);

		return fileRef.putString(dataUrl, 'data_url');
	}

}