import { IUploadTask, IFBStorage, IFBReference, IFireBase, IFireStore, IDocReference, IQuerySnapshot, IQueryDocumentSnapshot, IDocumentSnapshot } from "../../IFirebase.js";
import { FotoObj, IPhotoObj } from "../../ent/FotoObj.js";
import { Util } from "../../Util.js";

declare var firebase: IFireBase;

export class Foto {
	private storage: IFBStorage = null;
	private nama: string = 'silsilah_foto';
	private db: IFireStore;
	private readonly dir: string = 'silsilah/';

	async hapus(foto: FotoObj): Promise<void> {
		await this.db.collection(this.nama).doc(foto.id).delete().catch((e: Error) => {
			console.log(e)
		});
		await this.hapusGbr(foto.idPhoto).catch((e: Error) => {
			console.log(e);
		});
		await this.hapusGbr(foto.idThumb).catch((e: Error) => {
			console.log(e);
		});
	}

	async fotoInsert(foto: FotoObj): Promise<string> {
		let id: IDocReference = await this.db.collection(this.nama).add(this.toObj(foto));
		return id.id;
	}

	async getByIdOrDefault(id: string): Promise<FotoObj> {
		let hasil: FotoObj;

		try {
			hasil = await this.getById(id);
			if (!hasil) {
				hasil = Util.fotoError();
			}
		}
		catch (e) {
			console.log(e);
			hasil = Util.fotoError();
		}

		return hasil;
	}

	async getById(id: string): Promise<FotoObj> {
		let docSnapshot: IDocumentSnapshot;
		let hasil: FotoObj = null;

		try {
			console.group('foto get by id, id ' + id);

			docSnapshot = await this.db.collection(this.nama).doc(id).get();
			if (!docSnapshot.data()) {
				console.log('no data');
				console.groupEnd();
				return null;
			}

			hasil = this.fromObj(docSnapshot.data(), id);

			console.log('hasil:');
			console.log(hasil);
			console.groupEnd();
			return hasil;
		}
		catch (e) {
			// console.log(e);
			console.groupEnd();
			throw new Error(e.message);
		}

	}

	async get(): Promise<FotoObj[]> {
		let res: FotoObj[] = [];

		try {
			let qSnapshot: IQuerySnapshot = await this.db.collection(this.nama).get();

			console.log('get');

			qSnapshot.forEach((item: IQueryDocumentSnapshot) => {
				res.push(this.fromObj(item.data(), item.id));
			})
		}
		catch (e) {
			console.log(e.message);
			res = [];
		}

		return res;
	}

	async hapusGbr(id: string): Promise<void> {
		let ref: IFBReference = this.storage.ref();
		ref = ref.child(this.dir + id);

		await ref.delete().then(() => {
			console.log('hapus gambar ' + id + ' berhasil');
		});
	}

	fromObj(obj: IPhotoObj, id: string): FotoObj {
		let rel: FotoObj = new FotoObj();

		console.log('form obj');
		console.log(obj);

		rel.id = id;
		rel.photoUrl = obj.photoUrl;
		rel.thumbUrl = obj.thumbUrl;
		rel.idPhoto = obj.idPhoto;
		rel.idThumb = obj.idThumb;

		return rel;
	}

	toObj(rel: FotoObj): IPhotoObj {
		return {
			id: rel.id,
			photoUrl: rel.photoUrl,
			thumbUrl: rel.thumbUrl,
			idPhoto: rel.idPhoto,
			idThumb: rel.idThumb
		}
	}

	async init(db: IFireStore): Promise<void> {
		this.db = db;
		this.storage = firebase.storage();
		return Promise.resolve();
	}

	private createName(): string {
		let hasil: string = 'foto';

		for (let i: number = 0; i < 10; i++) {
			hasil += Math.floor(Math.random() * 10);
		}
		let date: Date = new Date();
		hasil += '_' + Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());

		return hasil;
	}

	async upload(canvas: HTMLCanvasElement): Promise<IUResult> {
		console.log('upload');
		return new Promise<IUResult>((resolve, reject) => {
			let name: string = this.createName();
			let uploadTask: IUploadTask = this.getUploadTask(canvas.toDataURL(), name);

			uploadTask.on(
				'state_changed',
				null,
				(error: any) => {
					console.log('rejected');
					reject(error);
				},
				() => {
					uploadTask.snapshot.ref.getDownloadURL().then((downloadURL: string) => {
						resolve({ name: name, url: downloadURL });
					});
				});
		});
	}

	private getUploadTask(dataUrl: string, name: string): IUploadTask {
		let storageRef: IFBReference = this.storage.ref();
		let fileRef: IFBReference;

		console.log(name);

		try {
			fileRef = storageRef.child('silsilah/' + name);
		}
		catch (error) {
			console.log('error');
			console.log(error);
		}

		console.log('file ref' + fileRef);

		return fileRef.putString(dataUrl, 'data_url');
	}

}

export interface IUResult {
	name: string;
	url: string;
}