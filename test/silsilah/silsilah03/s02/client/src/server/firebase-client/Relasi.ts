import { RelPasanganObj, IRelPasangan } from "../../ent/RelPasanganObj.js";
import { IFireStore, IQuerySnapshot, IQueryDocumentSnapshot, IDocReference } from "../../IFirebase.js";

export class Relasi {
	private db: IFireStore = null;
	private nama: string = 'silsilah_relasi';

	async hapus(rel: RelPasanganObj): Promise<void> {
		console.group('relasi hapus:');
		console.log(rel);
		console.groupEnd();
		await this.db.collection(this.nama).doc(rel.id).delete();
	}

	async init(db: IFireStore): Promise<void> {
		this.db = db;
		return Promise.resolve();
	}

	async update(_rel: RelPasanganObj): Promise<void> {
		await this.db.collection(this.nama).doc(_rel.id).set(this.toObj(_rel));
	}

	async getByAnakId(id: string): Promise<RelPasanganObj> {
		let querySnapshot: IQuerySnapshot;
		let hasil: RelPasanganObj = null;

		console.group('get by anak');

		querySnapshot = await this.db.collection(this.nama).where('anaks', 'array-contains', id).get();
		querySnapshot.forEach((item: IQueryDocumentSnapshot) => {
			hasil = this.fromObj(item.data(), item.id);
		})

		console.log('hasil:');
		console.log(hasil);
		console.groupEnd();
		return hasil;
	}

	async getByAnggotaId(id: string): Promise<RelPasanganObj> {
		let querySnapshot: IQuerySnapshot = await this.db.collection(this.nama).where('anak1', '==', id).get();
		let hasil: RelPasanganObj = null;

		console.group('get by anak');

		querySnapshot.forEach((item: IQueryDocumentSnapshot) => {
			hasil = this.fromObj(item.data(), item.id);
		})

		if (hasil) {
			console.log('hasil:');
			console.log(hasil);
			console.groupEnd();
			return hasil;
		}

		querySnapshot = await this.db.collection(this.nama).where('anak2', '==', id).get();
		querySnapshot.forEach((item: IQueryDocumentSnapshot) => {
			hasil = this.fromObj(item.data(), item.id);
		})

		console.log('hasil:');
		console.log(hasil);
		console.groupEnd();

		return hasil;
	}

	async insert(rel: RelPasanganObj): Promise<string> {
		let docRef: IDocReference = await this.db.collection(this.nama).add(this.toObj(rel));
		return docRef.id;
	}

	toObj(rel: RelPasanganObj): IRelPasangan {
		return {
			id: rel.id,
			anak1: rel.anak1,
			anak2: rel.anak2,
			anakStr: JSON.stringify(rel.anaks),
			anaks: rel.anaks
		}
	}

	fromObj(rel: IRelPasangan, id: string): RelPasanganObj {
		let hasil: RelPasanganObj = new RelPasanganObj();
		let anaks: string[] = JSON.parse(rel.anakStr);

		if (!id) throw new Error();
		if ("" == id) throw new Error();

		hasil.id = id;
		hasil.anak1 = rel.anak1 ? rel.anak1 : "";
		hasil.anak2 = rel.anak2 ? rel.anak2 : "";
		hasil.anaks = anaks ? anaks : []
		hasil.anakStr = rel.anakStr;
		return hasil;
	}


}