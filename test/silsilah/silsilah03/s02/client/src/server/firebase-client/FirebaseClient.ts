import { IFireStore, IFireBase, IFBUserCredential } from "../../IFirebase.js";
import { Anggota } from "./Anggota.js";
import { Foto } from "./Foto.js";
import { Relasi } from "./Relasi.js";
import { Auth } from "./Auth.js";
import { FireBaseConnector } from "./FireBaseConnector.js";

declare var firebase: IFireBase;

export class FireBaseClient {
	private _anggota: Anggota = new Anggota();
	private _foto: Foto = new Foto();
	private _relasi: Relasi = new Relasi();
	private auth2:Auth = new Auth();
	private firebaseConnector:FireBaseConnector = new FireBaseConnector();

	private db: IFireStore = null;

	constructor() {

	}

	async init(): Promise<any> {
		await this.firebaseConnector.init();
		this.db = firebase.firestore();
		await this.auth2.init();
		await this._anggota.init(this.db);
		await this._foto.init(this.db);
		await this._relasi.init(this.db);

		console.log('firebase complete');
	}

	async login(username: string, password: string, persistence: string): Promise<IFBUserCredential> {
		return await this.auth2.login(username,password, persistence);
	}

	public get foto(): Foto {
		return this._foto;
	}

	public get anggota(): Anggota {
		return this._anggota;
	}

	public get relasi(): Relasi {
		return this._relasi;
	}
}




