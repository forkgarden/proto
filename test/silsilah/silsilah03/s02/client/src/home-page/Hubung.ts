import { BaseComponent } from "../ha/BaseComponent.js";

export class Hubung extends BaseComponent {

	static readonly ATAS: string = 'atas';
	static readonly KANAN: string = 'kanan';
	static readonly KIRI: string = 'kiri';
	static readonly TENGAH: string = 'tengah';
	static readonly KOSONG: string = 'kosong';

	constructor() {
		super();
	}

	setClass(className: string): void {
		this._elHtml.classList.remove(className);
		this._elHtml.classList.add(className);
	}


}