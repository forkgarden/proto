import { BaseComponent } from "../ha/BaseComponent.js";
import { IAnggota } from "../ent/AnggotaObj.js";
import { Util } from "../Util.js";
// import { Kons } from "../Kons.js";

export class Foto extends BaseComponent {
	private _anggota: IAnggota;

	constructor() {
		super();

		this._elHtml = Util.getTemplate('div#foto');

		this.lihatTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			console.log('edit tbl click');
			Util.bukaViewProfile(this._anggota.id, window.top.location.href);
		}

		this.utamaTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			e.stopImmediatePropagation();
			window.top.location.href = Util.urlHome + "?id=" + this._anggota.id;
		}
	}

	init(): void {
		this.img.onerror = () => {
			this.img.src = Util.imgSalah;
		}
	}

	// updateTampil(): void {
	// 	if (this._elHtml.classList.contains('tampil')) {
	// 		this.attach(this._parent);
	// 	}
	// 	else {
	// 		this.detach();
	// 	}
	// }

	sembunyiTombol(): void {
		this.lihatTbl.classList.remove('tampil');
		this.utamaTbl.classList.remove('tampil');
	}

	// updateView(): void {
	// 	this.pNama.innerText = this._anggota.nama;
	// }

	get lihatTbl(): HTMLButtonElement {
		return this.getEl('button.edit') as HTMLButtonElement;
	}

	get img(): HTMLImageElement {
		return this.getEl('img.foto2') as HTMLImageElement;
	}

	get pNama(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}

	get utamaTbl(): HTMLButtonElement {
		return this.getEl('button.utama') as HTMLButtonElement;
	}

	get fotoCont(): HTMLDivElement {
		return this.getEl('div.foto-cont') as HTMLDivElement;
	}

	public set anggota(value: IAnggota) {
		this._anggota = value;
	}
}