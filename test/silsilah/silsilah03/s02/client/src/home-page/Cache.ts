import { AnggotaObj } from "../ent/AnggotaObj";

export class Cache {

	tambah(obj: AnggotaObj): void {
		if (this.load2(obj.id)) {
			this.simpan(obj.id, JSON.stringify(obj));
		}
		else {
			this.simpan(obj.id, JSON.stringify(obj));
		}
	}

	muat(key: string): AnggotaObj {
		let str: string = this.load2(key);
		let obj: AnggotaObj = JSON.parse(str);

		return obj;
	}

	private load2(key: string): string {
		let storage: Storage = window.sessionStorage;

		return storage.getItem(key);
	}

	simpan(key: string, value: string): void {
		let storage: Storage = window.sessionStorage;

		try {
			storage.setItem(key, value);
		}
		catch (e) {
			console.log(e);
		}
	}
}