import { IRelPasangan } from "./ent/RelPasanganObj";

export interface IDb {
	init(...params: any[]): Promise<any>;
	get(): Promise<any>;
	insert(data: IRelPasangan): Promise<any>;
	getById(id: string): Promise<any>;
	update(id: string, data: any): Promise<any>;
	toObj(data: IRelPasangan): IRelPasangan;
	delete(id: string): Promise<any>;
}