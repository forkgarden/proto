import { Loading } from "./Loading.js";
import { Alert } from "./Alert.js";
import { AnggotaObj } from "./ent/AnggotaObj.js";
import { FotoObj } from "./ent/FotoObj.js";
import { MySqlAdapter } from "./server/mysql/MysqlAdapter.js";

export class Util {
	static readonly urlHome: string = "./home2.html";
	static readonly urlEditProfile: string = './edit-profile.html';
	static readonly urlViewProfile: string = './view-profile-page.html';
	static readonly urlDaftarAnggota: string = './daftar-anggota.html';
	static readonly urlMenu: string = './admin.html';
	static readonly urlFoto: string = './foto-upload.html';
	static readonly urlUploadPhoto: string = './foto-upload2.html';
	static readonly urlLink: string = 'https://hagarden.netlify.app/ssl2/index.html';
	static readonly urlServer: string = 'localhost:3000';

	static readonly defImage: string = "./imgs/kucing.png";
	static readonly imgSalah: string = "./imgs/profile_salah.png";
	static readonly redirect_edit_relasi: string = 'rer';
	static readonly paramUrlBalik: string = 'url_balik';

	private static loading: Loading = new Loading();
	private static alert: Alert = new Alert();

	static bukaEditProfile(id: string, url: string): void {
		Util.loadingStart();
		window.top.location.href = Util.urlEditProfile + "?id=" + id + "&url_balik=" + window.encodeURIComponent(url);
	}

	static bukaViewProfile(id: string, urlBalik: string): void {
		Util.loadingStart();
		window.top.location.href = Util.urlViewProfile + "?id=" + id + "&url_balik=" + window.encodeURIComponent(urlBalik);
	}

	getServer(): MySqlAdapter {
		return new MySqlAdapter();
	}

	static anakError(): AnggotaObj {
		let anak: AnggotaObj;

		anak = new AnggotaObj();
		anak.nama = '[-----]';
		anak.namaLengkap = '[-----]';
		anak.jkl = 'L';
		anak.id = '';
		anak.isDefault = true;

		return anak;
	}

	static async Ajax(type: string, url: string, data: string): Promise<XMLHttpRequest> {

		return new Promise((resolve: any, reject: any) => {
			console.log('send data');

			let xhr: XMLHttpRequest = new XMLHttpRequest();

			xhr.onload = () => {
				console.log('on load');
				if (200 == xhr.status) {
					resolve(xhr);
				}
				else {
					// console.log(xhr);
					console.log('reject ' + xhr.statusText);
					reject(new Error(xhr.status + ": " + xhr.statusText));
				}
			};

			xhr.onerror = () => {
				reject(new Error('Error'));
			}

			xhr.open(type, url, true);
			xhr.setRequestHeader('Content-type', 'application/json');
			xhr.send(data);
			data;
		});
	}

	static fotoError(): FotoObj {
		let foto: FotoObj = new FotoObj();

		foto.thumbUrl = Util.defImage;
		foto.photoUrl = Util.defImage;

		return foto;
	}

	reloadPage(): void {

	}

	static bukaHome(id: string): void {
		Util.loadingStart();
		window.top.location.href = Util.urlHome + '?id=' + id;
	}

	static bukaUploadPhoto2(idAnggota: string, urlBalik: string): void {
		window.top.location.href = Util.urlUploadPhoto + "?id=" + idAnggota + "&url_balik=" + window.encodeURIComponent(urlBalik);
	}

	static getEl(query: string): HTMLElement {
		let el: HTMLElement;

		el = document.body.querySelector(query);

		if (el) {
			return el
		} else {
			console.log(document.body);
			console.log(query);
			throw new Error('query not found ');
		}
	}

	static alertMsg(msg: string, tbl: boolean = true, fn: Function = null): void {
		Util.loadingEnd();
		this.alert.text.innerHTML = msg;
		this.alert.attach(document.body);
		if (tbl) {
			this.alert.okTbl.style.display = 'block';
		}
		else {
			this.alert.okTbl.style.display = 'none';
		}
		this.alert.okTbl.onclick = () => {
			this.alert.detach();
			if (fn) {
				fn();
			}
		}
	}

	static padding(str: string): string {
		str = "00" + str;
		return str.slice(str.length - 2, str.length);
	}

	static date2Input(dateP: number): string {
		let date: Date = (new Date(dateP));
		let res: string;
		let month: string = (date.getMonth() + 1) + '';
		let day: string = (date.getDate()) + '';

		if (dateP == 0) return '';

		res = date.getFullYear() + '-' + Util.padding(month) + '-' + Util.padding(day);

		return res;
	}

	static now(): number {
		let date: Date = new Date();
		return Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
	}

	static Input2Date(str: string): number {
		let date: Date = new Date(str);
		return Date.UTC(date.getFullYear(), date.getMonth(), date.getDate());
	}

	static loadingStart(): void {
		Util.loading.attach(window.document.body);
	}

	static loadingEnd(): void {
		Util.loading.detach();
	}

	static getAllEl(target: HTMLElement, query: string): NodeListOf<Element> {
		let hasil: NodeListOf<Element> = null;

		hasil = target.querySelectorAll(query);

		if (hasil && hasil.length > 0) {
			return hasil;
		}
		else {
			console.log(target);
			console.log(query);
			throw new Error('query not found ');
			return null;
		}

	}

	static getTemplate(query: string): HTMLElement {
		let template: DocumentFragment = document.body.querySelector('template').content;
		return template.querySelector(query).cloneNode(true) as HTMLElement;
	}

	static getQuery(key: string): string {
		let urlParam: URLSearchParams = new URLSearchParams(window.location.search);
		return urlParam.get(key);
	}

	static getLocalSession(): void {

	}

}