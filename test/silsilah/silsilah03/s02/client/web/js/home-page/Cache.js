export class Cache {
    tambah(obj) {
        if (this.load2(obj.id)) {
            this.simpan(obj.id, JSON.stringify(obj));
        }
        else {
            this.simpan(obj.id, JSON.stringify(obj));
        }
    }
    muat(key) {
        let str = this.load2(key);
        let obj = JSON.parse(str);
        return obj;
    }
    load2(key) {
        let storage = window.sessionStorage;
        return storage.getItem(key);
    }
    simpan(key, value) {
        let storage = window.sessionStorage;
        try {
            storage.setItem(key, value);
        }
        catch (e) {
            console.log(e);
        }
    }
}
