import { Anggota } from "./Anggota.js";
import { Foto } from "./Foto.js";
import { Relasi } from "./Relasi.js";
// import { IFBAuth, IFBUserCredential } from "../../IFirebase";
export class MySqlAdapter {
    // private _auth: IFBAuth;
    // private db: IFireStore = null;
    constructor() {
        this._anggota = new Anggota();
        this._foto = new Foto();
        this._relasi = new Relasi();
    }
    async init() {
    }
    async login(username, password, persistence) {
        username;
        password;
        persistence;
        return null;
    }
    get foto() {
        return this._foto;
    }
    get anggota() {
        return this._anggota;
    }
    get relasi() {
        return this._relasi;
    }
}
