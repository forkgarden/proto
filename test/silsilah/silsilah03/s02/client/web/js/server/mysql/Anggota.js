import { AnggotaObj } from "../../ent/AnggotaObj.js";
// import { IDocReference, IQuerySnapshot, IQueryDocumentSnapshot, IDocumentSnapshot } from "../../IFirebase.js";
import { Util } from "../../Util.js";
export class Anggota {
    constructor() {
        this.path = '/anggota';
    }
    // async init(): Promise<any> {
    // 	return Promise.resolve();
    // }
    async update(anggota) {
        // let xml:XMLHttpRequest;		
        await Util.Ajax('post', this.path + '/update', JSON.stringify(anggota));
    }
    async hapus(id) {
        await Util.Ajax('post', this.path + '/hapus', id);
    }
    async get() {
        let xml = await Util.Ajax('post', this.path + '/get', null);
        let str = xml.responseText;
        return JSON.parse(str);
    }
    async getByDoc(key) {
        key;
        return null;
    }
    async getByKey(key, value) {
        key;
        value;
        return null;
    }
    fromObj(obj, id) {
        let hasil = new AnggotaObj();
        hasil.id = id;
        hasil.nama = obj.nama ? obj.nama : '';
        hasil.namaLengkap = obj.namaLengkap || '';
        hasil.idFoto = obj.idFoto ? obj.idFoto : '';
        hasil.alamat = obj.alamat || '';
        hasil.facebook = obj.facebook || '';
        hasil.instagram = obj.instagram || '';
        hasil.wa = obj.wa || '';
        hasil.linkedin = obj.linkedin || '';
        hasil.tglLahir = obj.tglLahir || 0;
        hasil.tglMeninggal = obj.tglMeninggal || 0;
        hasil.jkl = obj.jkl || 'L';
        hasil.keterangan = obj.keterangan || '';
        return hasil;
    }
    toObj(data) {
        let obj = {
            id: data.id,
            nama: data.nama,
            idFoto: data.idFoto ? data.idFoto : "",
            // orangTuaId: data.orangTuaId ? data.orangTuaId : "",
            alamat: data.alamat,
            facebook: data.facebook,
            instagram: data.instagram,
            linkedin: data.linkedin,
            namaLengkap: data.namaLengkap,
            tglLahir: data.tglLahir,
            tglMeninggal: data.tglMeninggal,
            wa: data.wa,
            jkl: data.jkl,
            keterangan: data.keterangan
        };
        return obj;
    }
    async insert(anggota) {
        anggota;
        return null;
    }
}
