import { RelPasanganObj } from "../../ent/RelPasanganObj.js";
// import { IFireStore, IQuerySnapshot, IQueryDocumentSnapshot, IDocReference } from "../../IFirebase.js";
export class Relasi {
    // private db: IFireStore = null;
    // private nama: string = 'silsilah_relasi';
    async hapus(rel) {
        rel;
        // console.group('relasi hapus:');
        // console.log(rel);
        // console.groupEnd();
        // await this.db.collection(this.nama).doc(rel.id).delete();
    }
    // async init(db: IFireStore): Promise<void> {
    // 	this.db = db;
    // 	return Promise.resolve();
    // }
    async update(_rel) {
        _rel;
    }
    async getByAnakId(id) {
        id;
        return null;
    }
    async getByAnggotaId(id) {
        id;
        return null;
    }
    async insert(rel) {
        rel;
        return '';
    }
    toObj(rel) {
        return {
            id: rel.id,
            anak1: rel.anak1,
            anak2: rel.anak2,
            anakStr: JSON.stringify(rel.anaks),
            anaks: rel.anaks
        };
    }
    fromObj(rel, id) {
        let hasil = new RelPasanganObj();
        let anaks = JSON.parse(rel.anakStr);
        if (!id)
            throw new Error();
        if ("" == id)
            throw new Error();
        hasil.id = id;
        hasil.anak1 = rel.anak1 ? rel.anak1 : "";
        hasil.anak2 = rel.anak2 ? rel.anak2 : "";
        hasil.anaks = anaks ? anaks : [];
        hasil.anakStr = rel.anakStr;
        return hasil;
    }
}
