import express from "express";
import helmet from "helmet";
import { Connection } from "./module/Connection";
// import { router } from "./module/router/Barang";
import { router as fileRouter } from "./module/router/File";
import { router as authRouter } from "./module/router/Auth";
// import { router as routerInstall } from "./module/router/Install";
// import { router as routerTest } from "./module/router/TokoTest";
import { router as routerPengguna } from "./module/router/Anggota";
// import { lapakRouter } from "./module/router/Lapak";
import { logT } from "./module/Log";
import cookieSession from "cookie-session";
import { Server } from "http";
// import { berandaRouter } from "./module/router/Beranda";
// import { routerApiBarang } from "./module/admin/router/Barang";
import { util } from "./module/Util";
import { configRouter } from "./module/router/Config";

util.buatRandom();

const app: express.Express = express();
const port: number = 3000;

app.use(express.static(__dirname + "/public"));
app.use(express.json({ limit: '5mb' }));
app.use(cookieSession({
	name: 'toko_session',
	keys: ['Auni_202002_cookie_session']
}));

app.use(helmet.contentSecurityPolicy({
	directives: {
		defaultSrc: ["'self'", "'unsafe-eval'", "'unsafe-inline'", "data:", "blob:"]
	}
}))


app.use("/file", fileRouter);
app.use("/auth", authRouter);
app.use("/anggota", routerPengguna);
app.use("/konfig", configRouter);

app.use((_req: express.Request, _resp: express.Response, _next: Function) => {
	logT.log(_req.path);
	logT.log('404');
	_resp.status(404).send('Halaman Tidak Ditemukan');
})

process.on('SIGTERM', () => {
	try {
		logT.log('shutdown');

		Connection.pool.end((err) => {
			if (err) {
				logT.log('sql shutdown error');
				logT.log(err.sqlMessage);
			}
			else {
				logT.log('connection tutup');
			}
		});

	} catch (e) {
		logT.log(e);
	}

});

Connection.connect();

export const server: Server = app.listen(port, () => {
	logT.log("app started");
});

// beranda.init(app);

/*
app.get("/lapak/:lapak", (_req: express.Request, resp: express.Response) => {
	try {
		logT.log("rendar beranda lapak " + _req.params.lapak);
		barangSql.bacalapakPublish(_req.params.lapak)
			.then((data: any[]) => {
				logT.log(data);
				return render.renderBeranda(data, _req.params.lapak, false);
			})
			.then((data: string) => {
				resp.status(200).header("Cache-Control", "max-age=7200").send(data);
			})
			.catch((err) => {
				logT.log(err);
				resp.status(500).send('Error');
			});
	}
	catch (err) {
		logT.log(err);
		resp.status(500).send('Error');
	}
})

app.get("/", (_req: express.Request, resp: express.Response) => {
	try {
		barangSql.bacaPublish()
			.then((data: any[]) => {
				return render.renderBeranda(data, "", false);
			})
			.then((data: string) => {
				resp.status(200).send(data);
			})
			.then(() => {
				barangSql.updateLastViewDate();
			})
			.catch((err) => {
				logT.log(err);
				resp.status(500).send('Error');
			});
	}
	catch (err) {
		logT.log(err);
		resp.status(500).send('Error');
	}
})
*/


// app.get("/item/:id", (_req: express.Request, resp: express.Response) => {
// 	try {
// 		render.renderHalBarang(_req.params.id, "").then((hasil: string) => {
// 			resp.status(200).send(hasil);
// 		}).catch((err) => {
// 			logT.log(err);
// 			resp.status(500).send('Error');
// 		});
// 	}
// 	catch (err) {
// 		logT.log(err);
// 		resp.status(500).send('Error');
// 	}
// })

/*
app.get("/admin", (_req: express.Request, resp: express.Response) => {
	try {
		resp.sendFile(path.join('index.html'), {
			root: __dirname + '/public/admin'
		});
	}
	catch (e) {
		resp.status(500).send(e);
	}
})

app.get("/shutdown", (req: express.Request, resp: express.Response) => {
	try {
		logT.log('shutdown');

		resp.status(200).end();

		server.close((e) => {
			if (e) {
				logT.log('server close error');
				logT.log(e.message);
			} else {
				logT.log('server tutup');
			}
		})

		Connection.pool.end((err) => {
			if (err) {
				logT.log('sql shutdown error');
				logT.log(err.sqlMessage);
			}
			else {
				logT.log('connection tutup');
			}
		});

		//process.kill(process.pid, 'SIGTERM');
	} catch (e) {
		logT.log(e);
		resp.status(500).send(e);
	}
});

app.use((_req: express.Request, _resp: express.Response, _next: Function) => {
	logT.log(_req.path);
	logT.log('404');
	_resp.status(404).send('Halaman Tidak Ditemukan');
})

process.on('SIGTERM', () => {
	try {
		Connection.pool.end((err) => {
		});
	}
	catch (e) {

	}
})
*/