import { Connection } from "../Connection";
import { IAnggota } from "../Type";
// import { util } from "../Util";

class Anggota {


	async bacaDaftarLapakAktif(): Promise<IAnggota> {
		let query: string = ` SELECT * FROM pengguna`;
		console.log(query);

		return new Promise((resolve, reject) => {
			Connection.pool.query(query, [],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						// console.log(_rows);
						resolve(_rows);
					}
				});
		});
	}

	async baca(opt: IAnggota): Promise<IAnggota[]> {
		console.log(opt);

		let whereQuery: string = 'WHERE 1 ';
		let data: any[] = [];

		if (opt.id) {
			whereQuery += 'AND pengguna.id = ? ';
			data.push(opt.id);
		}

		//TODO: where query



		let query: string = ` SELECT * FROM pengguna ${whereQuery}`;
		console.log(query);

		return new Promise((resolve, reject) => {
			Connection.pool.query(query, data,
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						// console.log(_rows);
						resolve(_rows);
					}
				});
		});
	}

	async baru(data: IAnggota): Promise<any> {
		return new Promise((resolve, reject) => {
			// console.log('anggota baru');
			// console.log(data);

			Connection.pool.query(
				`INSERT INTO pengguna SET ?`,
				data,
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						console.log(_rows);
						resolve(_rows);
					}
				});
		});
	}

	async update(data: IAnggota, id: string): Promise<any> {
		return new Promise((resolve, reject) => {
			Connection.pool.query(
				"UPDATE pengguna SET ? WHERE ID = ?",
				[
					data,
					id
				],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});

	}

	async hapus(id: string): Promise<any> {
		return new Promise((resolve, reject) => {
			Connection.pool.query(
				"DELETE FROM pengguna WHERE ID = ?", [id],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}

}

export var anggota: Anggota = new Anggota();
