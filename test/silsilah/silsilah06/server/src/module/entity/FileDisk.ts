import fs from "fs";

class FileDisk {
	async bacaFile(dir: string): Promise<string[]> {
		return new Promise((_resolve, _reject) => {
			fs.readdir(dir, (err: NodeJS.ErrnoException, files: string[]) => {
				if (err) {
					_reject(err.message);
				}
				else {
					_resolve(files);
				}
			})

		});
	}

	async hapusFile(url: string): Promise<void> {
		console.log('hapus file ' + url);
		return new Promise((resolve, reject) => {
			fs.unlink(url, (err) => {
				if (err) {
					console.log(err);
					resolve();
				}
				resolve();
			})
		})
	}
}

export var fileDisk: FileDisk = new FileDisk();