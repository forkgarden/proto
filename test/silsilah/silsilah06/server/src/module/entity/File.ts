import { Connection } from "../Connection";
import fs from 'fs';
import { logT } from "../Log";
import { IFile, IFileKosong } from "../Type";
import { fileDisk } from "./FileDisk";
// import { barangSql } from "./BarangSql";

class File {

	private qsemua: string = ``;
	private qfileTanpaReferensi: string = ``;

	constructor() {
		this.qsemua;	//TODO:
		this.qfileTanpaReferensi; //TODO:
	}

	async bacaId(id: string): Promise<any> {
		return new Promise((resolve, reject) => {
			console.log('baca file by id ' + id);
			Connection.pool.query(
				`SELECT * FROM file WHERE id = ?`, [id],
				(_err, _rows) => {
					if (_err) {
						logT.log(_err.code + '/' + _err.message);
						reject(_err);
					}
					else {
						logT.log('ok');
						resolve(_rows[0]);
					}
				});

		});
	}

	async baru(gbrBesarData: string, gbrBesarNama: string, gbrKecilData: string, gbrKecilNama: string): Promise<void> {
		return new Promise((resolve, reject) => {
			let buf: Buffer;
			let folderUnggah: string = './public/upload/';
			let folderUrlUnggah: string = '/upload/';

			//simpan gambar besar;
			buf = Buffer.from(gbrBesarData, 'base64');
			fs.writeFileSync(folderUnggah + gbrBesarNama, buf);
			logT.log('file written ' + folderUnggah + gbrBesarNama);

			//simpan gambar kecil
			buf = Buffer.from(gbrKecilData, 'base64');
			fs.writeFileSync(folderUnggah + gbrKecilNama, buf);
			logT.log('file written ' + folderUnggah + gbrKecilNama);

			//simpan ke database
			Connection.pool.query(
				`INSERT INTO FILE SET ?
				`,
				{
					thumb: folderUrlUnggah + gbrKecilNama,
					gbr: folderUrlUnggah + gbrBesarNama
				},
				(_err, _rows) => {
					if (_err) {
						logT.log(_err.code + '/' + _err.message);
						reject(_err);
					}
					else {
						logT.log('ok');
						resolve(_rows);
					}
				});
		});
	}

	//TODO:
	async tanpaReferensi(): Promise<any> {
		return new Promise((resolve, reject) => {
			Connection.pool.query(
				this.qsemua,
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});

	}

	async bacaBerdasarThumb(thumb: string): Promise<string> {
		return new Promise((resolve, reject) => {
			console.log('baca file berdasar thumb ' + thumb);
			Connection.pool.query(
				`SELECT id FROM file WHERE thumb = ?`, thumb,
				(_err, _rows) => {
					if (_err) {
						logT.log(_err.code + '/' + _err.message);
						reject(_err);
					}
					else {
						logT.log('ok');
						resolve(_rows[0]);
					}
				});
		});

	}

	async bacaBerdasarGbr(gbr: string): Promise<string> {
		return new Promise((resolve, reject) => {
			console.log('baca file berdasar gbr ' + gbr);
			Connection.pool.query(
				`SELECT id FROM file WHERE gbr = ?`, gbr,
				(_err, _rows) => {
					if (_err) {
						logT.log(_err.code + '/' + _err.message);
						reject(_err);
					}
					else {
						logT.log('ok');
						resolve(_rows[0]);
					}
				});
		});
	}

	async bacaBerdasarFile(item: string, type: string = 'kecil'): Promise<string> {
		if ('kecil' == type) {
			return await this.bacaBerdasarThumb(item);
		}
		else if ('besar' == type) {
			return await this.bacaBerdasarGbr(item);
		}
		else {
			return '';
		}
	}

	async baca(): Promise<any> {
		return new Promise((resolve, reject) => {
			Connection.pool.query(
				this.qsemua,
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});

	}

	async hapus(id: string): Promise<void> {
		let file: IFile = await this.bacaId(id);

		console.log('hapus');

		console.log('file')
		console.log(file);

		console.log('hapus db id ' + id);
		await this.hapusDb(id);

		console.log('hapus file, thumb ' + file.thumb);
		await fileDisk.hapusFile('./public' + file.thumb);

		console.log('hapus file, gbr ' + file.gbr);
		await fileDisk.hapusFile('./public' + file.gbr);
	}

	async bacaDiskKosong(): Promise<any[]> {
		let files: string[] = await fileDisk.bacaFile('./public/upload');
		let hasil: IFileKosong[] = [];

		for (let i: number = 0; i < files.length; i++) {
			let item = files[i];
			let type: string;
			let id: string;

			if (item.indexOf('gbr_besar') > -1) {
				type = 'besar';
			}
			else if (item.indexOf('gbr_kecil') > -1) {
				type = 'kecil';
			}
			else {
				throw Error('');
			}

			id = await this.bacaBerdasarFile(item, type);

			hasil.push({
				file: item,
				type: type,
				fileId: id
			});

		}

		return hasil;
	}

	//TODO:
	async bacaFileKosong(): Promise<any[]> {
		return [];
	}

	async hapusDb(id: string): Promise<void> {
		return new Promise((resolve, reject) => {
			logT.log('hapus file db, id ' + id);
			Connection.pool.query(
				`DELETE FROM file WHERE ID = ?`, [id],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});


	}

}

export var file: File = new File();