//NOTE: FINAL
export interface IFile {
	id: string,
	thumb: string,
	gbr: string
}

export interface IAdmin {
	id?: string;
	user_name?: string;
	password?: string;
}

export interface IAnggota {
	id?: string;
	info_id?: string;
	foto_id?: string;
}

export interface IFileKosong {
	file: string,
	type: string,
	fileId: string
}

export interface IConfigDb {
	id?: string,
	kunci?: string,
	nilai?: string,
	deskripsi?: string
}