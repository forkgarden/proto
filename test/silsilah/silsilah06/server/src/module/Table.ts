var pengguna = {
	id: "id",
	user_id: "user_id",
	lapak: "lapak",
	deskripsi: "deskripsi"
}

var file = {
	tabel: "FILE",
	id: "id",
	thumb: "thumb",
	gbr: "gbr"

}

var barang = {
	tabel: "BARANG",
	id: "id",
	nama: "nama",
	deskripsi: "deskripsi",
	deskripsiPanjang: "deskripsi_panjang",
	fileId: "file_id",
	harga: "harga",
	wa: "wa",
	publish: "publish",
	lapakId: "lapak_id",
	lastView: "last_view"
}

export var table = {
	pengguna: pengguna,
	barang: barang,
	file: file
}
// table.pengguna = pengguna;