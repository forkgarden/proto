import { logT } from "./Log";
import express from "express";
import { session } from "./SessionData";
import { IAdmin } from "./Type";

class Auth {

	//TODO:
	async login(userId: string, password: string): Promise<IAdmin> {
		logT.log('Auth: login ');

		return {

		}
	}

	async checkLevel(level: string): Promise<boolean> {
		//TODO:
		return false;
	}
}

export var auth: Auth = new Auth();

//check auth middle ware
export function checkAuth(req: express.Request, resp: express.Response, next: express.NextFunction) {
	if (!session(req).statusLogin) {
		resp.status(401).send('belum login');
	}
	else {
		next();
	}
}

export function setCache(_req: express.Request, resp: express.Response, next: express.NextFunction) {
	resp.header("Cache-Control", "max-age=7201");
	next();
}