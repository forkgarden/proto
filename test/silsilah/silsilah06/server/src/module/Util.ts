import fs from "fs";

class Util {
	private caches: ICache[] = [];
	private _randId: string = '';

	buatDate(): string {
		let date: Date = new Date();

		return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
	}

	//TODO:
	query(query: string): string {
		let ar: string[] = query.match(/\:[a-zA-Z]+/);
		ar.forEach((item: string) => {
			query = query.replace(item, "");
		});

		return query;
	}

	arr2String(ar: string[]): string {
		let hasil: string = ' ';

		ar.forEach((item: string, idx: number) => {
			if (0 === idx) {
				hasil += item;
			}
			else {
				hasil += " ," + item;
			}
		})

		hasil += ' ';

		return hasil;
	}

	buatRandom(): void {
		this._randId = '';
		for (let i: number = 0; i < 10; i++) {
			this._randId += (Math.floor(Math.random() * 10) + '');
		}
	}

	private ambilDariCache(url: string): string {
		let hasil: string = '';

		this.caches.forEach((item: ICache) => {
			if (item.url === url) {
				hasil = item.string;
			}
		})

		return hasil;
	}

	hapusCache(): void {
		this.caches = [];
	}

	async getFile(file: string): Promise<string> {
		return new Promise((resolve, reject) => {
			let cache: string;
			cache = this.ambilDariCache(file);

			if (cache != '') {
				console.log('ambil dari cache ' + file);
				resolve(cache);
			}

			fs.readFile(file, (err: NodeJS.ErrnoException, content) => {
				if (err) {
					reject(err.message);
				}
				else {
					this.caches.push({
						url: file,
						string: content.toString()
					})
					resolve(content.toString());
				}
			})

		});
	}

	async tulisKeFile(path: string, data: string): Promise<void> {
		return new Promise((resolve, reject) => {
			fs.writeFile(path, data, (err) => {
				if (err) {
					reject(err.message);
				}
				else {
					resolve();
				}
			})
		});
	}

	buatWa(wa: string, namaBarang: string): string {
		return 'https://wa.me/' + wa + "?text==========%0D%0A" + namaBarang + "%0D%0A=========%0D%0AAssalamu'alaikum:";
	}

	public get randId(): string {
		return this._randId;
	}

}

export var util: Util = new Util();

interface ICache {
	url: string,
	string: string
}

