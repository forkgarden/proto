import express from "express";
// import { stat } from "fs";
import { auth } from "../Auth";
import { session } from "../SessionData";
import { logT } from "../Log";
import { IAnggota } from "../Type";

export var router = express.Router();

router.post("/login", (req: express.Request, resp: express.Response) => {
	try {
		logT.log('login');

		auth.login(req.body.user_id, req.body.password).then((h: IAnggota) => {
			if (h) {
				//TODO: session
				session(req).statusLogin = true;
				session(req).id = h.id;
				resp.status(200).send(h);
			}
			else {
				req.session = null;
				resp.status(401).send('username/password salah');
			}
		}).catch((e) => {
			req.session = null;
			logT.log(e);
			resp.status(501).send(e.message);
		});
	}
	catch (e) {
		req.session = null;
		logT.log(e);
		resp.status(502).send(e.message);
	}
});

router.get("/logout", (req: express.Request, resp: express.Response) => {
	try {
		logT.log('logout')
		req.session = null;
		resp.redirect('/');
	}
	catch (e) {
		logT.log(e);
		resp.status(500).send(e.message);
	}
});



