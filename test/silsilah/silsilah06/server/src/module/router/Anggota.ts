import express from "express";
import { checkAuth } from "../Auth";
import { anggota } from "../entity/AnggotaSql";
import { IAnggota } from "../Type";

export var router = express.Router();

router.post("/hapus/:id", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		anggota.hapus(req.params.id).then(() => {
			resp.status(200).end();
		}).catch((e) => {
			resp.status(500).send(e.message);
		});
	}
	catch (err) {
		resp.status(500).send(err.message);
	}
});

router.post("/baru", (req: express.Request, resp: express.Response) => {
	try {
		let data: IAnggota =
		{
			info_id: req.body.info_id,
			foto_id: req.body.foto_id
		};

		anggota.baru(data)
			.then(() => {
				resp.status(200).end();
			}).catch((e: any) => {
				console.log(e.errno);
				resp.status(500).send(e.message);
				//TODO: err 1062 duplicate entry
			});
	}
	catch (e) {
		resp.status(500).send(e.message);
	}
});

router.post("/update/:id", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		let opt: IAnggota = {
			info_id: req.body.info_id,
			foto_id: req.body.foto_id
		}

		anggota.update(opt, req.params.id).then(() => {
			resp.status(200).end();
		}).catch((e: any) => {
			resp.status(500).send(e.message)
		});

	}
	catch (error) {
		resp.status(500).send(error.message);
	}
});

router.post("/baca", (req: express.Request, resp: express.Response) => {
	try {
		console.log(req.body);

		anggota.baca({
			id: req.body.id,
			info_id: req.body.info_id,
			foto_id: req.body.foto_id
		}).then((h) => {
			resp.status(200).send(h);
		}).catch((e) => {
			resp.status(500).send(e.message);
		});
	}
	catch (err) {
		resp.status(500).send(err.message);
	}
});