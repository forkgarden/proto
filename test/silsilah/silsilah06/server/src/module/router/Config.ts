import express from "express";
import { checkAuth } from "../Auth";
import { config } from "../Config";
import { configController } from "../ConfigController";
import { configSql } from "../entity/ConfigSql";
import { logT } from "../Log";

export var configRouter = express.Router();
var router = configRouter;

router.get("/simpan", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		configController.update2DbSemua().then(() => {
			resp.status(200).send('ok');
		}).catch((e) => {
			resp.status(500).send(e.message);
		});
	}
	catch (err) {
		logT.log(err);
		resp.status(500).send(err.message);
	}
});


router.post("/baca", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		resp.status(200).send(config.bacaSemua());
	}
	catch (err) {
		logT.log(err);
		resp.status(500).send(err.message);
	}
});


router.post("/update/:id", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		configSql.update(req.body, req.params.id)
			.then(() => {
				resp.status(200).send('');
			}).catch((err) => {
				logT.log(err);
				resp.status(500).send(err.message);
			});
	}
	catch (err) {
		logT.log(err);
		resp.status(500).send(err.message);
	}
})

router.post("/reload", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		configController.ambilDariDbSemua().then(() => {
			resp.status(200).send();
		}).catch((e) => {
			logT.log(e);
			resp.status(500).send(e.message);
		});
	}
	catch (e) {
		logT.log(e);
		resp.status(500).send(e.message);
	}
})