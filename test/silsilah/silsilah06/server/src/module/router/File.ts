import express from "express";
import { Connection } from "../Connection";
import fs from "fs";
import { checkAuth } from "../Auth";
import { logT } from "../Log";
import { file } from "../entity/File";

export var router = express.Router();

router.post("/baca", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		//TODO:
		Connection.pool;
		resp.status(200).send('');
	}
	catch (err) {
		logT.log('error');
		resp.status(500).send(err);
	}
});

router.get("/baca/:id", checkAuth, (req: express.Request, resp: express.Response) => {
	//TODO: file admin
	resp.status(200).send();
})

router.post("/baru", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		let data: string;
		let buf: Buffer;
		let gbrBesarNama: string;
		let gbrKecilNama: string;
		let folderUnggah: string = './public/upload/';
		let folderUrlUnggah: string = '/upload/';

		//simpan gambar besar;
		gbrBesarNama = req.body.gbr_besar_nama;
		data = req.body.gbr_besar.split(',')[1];
		buf = Buffer.from(data, 'base64');
		fs.writeFileSync(folderUnggah + gbrBesarNama, buf);
		logT.log('file written ' + folderUnggah + gbrBesarNama);

		//simpan gambar kecil
		gbrKecilNama = req.body.gbr_kecil_nama;
		data = req.body.gbr_kecil.split(',')[1];
		buf = Buffer.from(data, 'base64');
		fs.writeFileSync(folderUnggah + gbrKecilNama, buf);
		logT.log('file written ' + folderUnggah + gbrKecilNama);

		// log.info();

		//simpan ke database
		Connection.pool.query(
			`INSERT INTO FILE SET ?
			`,
			{
				thumb: folderUrlUnggah + gbrKecilNama,
				gbr: folderUrlUnggah + gbrBesarNama
			},
			(_err, _rows) => {
				if (_err) {
					logT.log(_err.sqlMessage);
					resp.status(500).send(_err.message)
				}
				else {
					logT.log('ok');
					resp.status(200).send({
						gbr_url: folderUrlUnggah + gbrKecilNama,
						baris_info: _rows
					});
				}
			});
	}
	catch (e) {
		logT.log(e);
		resp.status(500).send(JSON.stringify(e));
	}
});

router.post('/baca/file/kosong', checkAuth, (req: express.Request, resp: express.Response) => {
	try {

	}
	catch (e) {

	}
})

router.post("/baca/disk/kosong", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		file.bacaDiskKosong().then((item: any[]) => {
			resp.status(200).send('');
		}).catch((e) => {
			console.log(e);
			resp.status(500).send(e);
		});
	}
	catch (e) {
		console.log(e);
		resp.status(500).send(e);
	}
});

router.post("/hapus/", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		file.hapus(req.body.id).then(() => {
			resp.status(200).send('');
		}).catch((e) => {
			console.log(e);
			resp.status(500).send(e);
		});

	}
	catch (e) {
		console.log(e);
		resp.status(500).send(e);
	}
});