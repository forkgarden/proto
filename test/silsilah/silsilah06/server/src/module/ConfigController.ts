import { config, ISetting } from "./Config";
import { configSql } from "./entity/ConfigSql";
import { IConfigDb } from "./Type";

class ConfigController {
	constructor() {

	}

	async update2DbSemua(): Promise<void> {
		let items: ISetting[] = config.bacaSemua();
		for (let i: number = 0; i < items.length; i++) {
			await this.update2Db(items[i]);
		}
	}

	async ambilDariDbSemua(): Promise<void> {
		let items: ISetting[] = config.bacaSemua();

		for (let i: number = 0; i < items.length; i++) {
			await this.ambilDariDb(items[i].kunci);
		}
	}

	async ambilDariDb(key: string): Promise<void> {
		let item: IConfigDb[] = await configSql.bacaKey(key);

		if (item.length > 0) {
			let konfig: ISetting = config.getSetting(key);
			konfig.deskripsi = item[0].deskripsi;
			konfig.nilai = item[0].nilai;
		}
	}

	async update2Db(item: ISetting): Promise<void> {
		let ada: IConfigDb[] = await configSql.bacaKey(item.kunci);

		// console.log('update2db');
		// console.log(item);
		// console.log(ada);

		if (ada.length > 0) {
			await configSql.update({
				deskripsi: item.deskripsi,
				kunci: item.kunci,
				nilai: item.nilai
			}, ada[0].id);
		}
		else {
			await configSql.insert({
				deskripsi: item.deskripsi,
				kunci: item.kunci,
				nilai: item.nilai
			});
		}
	}
}

export var configController: ConfigController = new ConfigController();