//NOTE: FINAL

export class Config {
	static readonly NAV_LAPAK: string = 'nav_lapak';
	static readonly TERKAIT: string = 'terkait';
	static readonly NAMA_TOKO: string = 'nama_toko';
	static readonly JML_PER_HAL: string = 'jml_per_hal';
	static readonly FOOTER: string = 'footer';

	private settingAr: ISetting[] = [];

	constructor() {
		this.settingAr = [{
			kunci: Config.NAV_LAPAK,
			nilai: '0',
			deskripsi: 'Tampilkan navigasi lapak di halaman depan'
		},
		{
			kunci: Config.TERKAIT,
			nilai: '0',
			deskripsi: 'Tampilkan barang terkait'
		},
		{
			kunci: Config.NAMA_TOKO,
			nilai: 'Auni Store',
			deskripsi: 'Nama toko'
		},
		{
			kunci: Config.JML_PER_HAL,
			nilai: '25',
			deskripsi: 'Jumlah item per halaman'
		},
		{
			kunci: Config.FOOTER,
			nilai: `<H3>Auni Store</H3>Perum Taman Melati Blok FE 07 Bojong Sari - Sawangan - Depok<br/><br/>`,
			deskripsi: 'Footer'
		}

		]
	}

	bacaSemua(): ISetting[] {
		return this.settingAr;
	}

	updateNilai(key: string, nilai: string): void {
		this.getSetting(key).nilai = nilai;
	}

	getSetting(key: string): ISetting {
		for (let i: number = 0; i < this.settingAr.length; i++) {
			if (this.settingAr[i].kunci == key) {
				return this.settingAr[i];
			}
		}

		return null;
	}

	getNilai(key: string): string {
		for (let i: number = 0; i < this.settingAr.length; i++) {
			if (this.settingAr[i].kunci == key) {
				return this.settingAr[i].nilai;
			}
		}

		throw new Error('key tidak ketemu: ' + key);
	}

}

export interface ISetting {
	kunci: string,
	deskripsi: string,
	nilai: string
}

export var config: Config = new Config();