import { BaseComponent } from "../BaseComponent.js";
import { data } from "../Data.js";
import { relasiAnak } from "../model/RelasiAnak.js";
// import { relasi } from "../model/Relasi.js";
import { baru } from "./Baru.js";
class Daftar extends BaseComponent {
    constructor() {
        super();
        this.items = [];
        console.log('daftar');
        this._template = `
			<div class='daftar-relasi'>
				<h1>Daftar Relasi</h1>
				<div class='cont'>
				</div>
				<button class='edit'>edit</button>
				<button class='tambah'>tambah</button>
				<button class='hapus'>hapus</button>
				<button class='kembali'>kembali</button>
			</div>
		`;
        this.build();
        this.kembaliTbl.onclick = () => {
            this._selesai();
        };
        this.hapusTbl.onclick = () => {
            relasiAnak.hapus(this.relDipilih);
            this.load();
        };
        this.tambahTbl.onclick = () => {
            this.detach();
            baru.view.attach(data.cont);
            baru.init();
            baru.selesai = () => {
                baru.view.detach();
                this.attach(data.cont);
                this.load();
            };
        };
        this.editTbl.onclick = () => {
            //TODO:
        };
    }
    load() {
        this.items = [];
        this.cont.innerHTML = '';
        relasiAnak.list.forEach((item) => {
            let view = new Item();
            view.nama.innerHTML = "(" + item.relasi.anggota1.info.nama + '-' + item.relasi.anggota2.info.nama + ") - " + item.anggota.info.nama;
            view.attach(this.cont);
            view.rel = item;
            view.elHtml.onclick = () => {
                let el = this.cont.querySelector('div.item.pilih');
                if (el) {
                    el.classList.remove('pilih');
                }
                view.elHtml.classList.add('pilih');
            };
            this.items.push(view);
        });
        this.items[0].elHtml.classList.add('pilih');
    }
    get kembaliTbl() {
        return this.getEl('button.kembali');
    }
    get viewDipilih() {
        let hasil;
        this.items.forEach((item) => {
            if (item.elHtml.classList.contains('pilih')) {
                hasil = item;
            }
        });
        return hasil;
    }
    get relDipilih() {
        let view;
        view = this.viewDipilih;
        return view.rel;
    }
    get cont() {
        return this.getEl('div.cont');
    }
    get editTbl() {
        return this.getEl('button.edit');
    }
    get selesai() {
        return this._selesai;
    }
    set selesai(value) {
        this._selesai = value;
    }
    get tambahTbl() {
        return this.getEl('button.tambah');
    }
    get hapusTbl() {
        return this.getEl('button.hapus');
    }
}
class Item extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
        this.build();
    }
    get rel() {
        return this._rel;
    }
    set rel(value) {
        this._rel = value;
    }
    get nama() {
        return this.getEl('p.nama');
    }
}
export var daftar = new Daftar();
