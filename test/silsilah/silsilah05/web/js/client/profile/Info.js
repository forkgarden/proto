import { BaseComponent } from "../../BaseComponent.js";
class Info {
    constructor() {
        this._view = new View();
    }
    get view() {
        return this._view;
    }
}
class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='info'>
				<p class='label'>Nama:</p>
				<p class='info nama'></p>
				<p class='label'>Pasangan:</p>
				<p class='info pasangan'></p>
			</div>
		`;
        this.build();
    }
    get namaP() {
        return this.getEl('p.nama');
    }
    get pasanganP() {
        return this.getEl('p.pasangan');
    }
}
export var info = new Info();
