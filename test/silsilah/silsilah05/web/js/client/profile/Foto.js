import { BaseComponent } from "../../BaseComponent.js";
class Foto extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='foto'>
				<img src=''><br/>
			</div>
		`;
        this.build();
    }
    get gbr() {
        return this.getEl('img');
    }
    get hapusTbl() {
        return this.getEl('button.hapus');
    }
    get unggahTbl() {
        return this.getEl('button.unggah');
    }
}
export var foto = new Foto();
