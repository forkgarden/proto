import { BaseComponent } from "../../BaseComponent.js";
import { data } from "../../Data.js";
import { util } from "../../model/Util.js";
import { foto } from "./Foto.js";
import { info } from "./Info.js";
class Profile {
    constructor() {
        this._view = new View();
        info.view.attach(this._view.infoCont);
        foto.attach(this._view.fotoCont);
        this.view.kembaliTbl.onclick = () => {
            this._selesai();
        };
    }
    load() {
        info.view.namaP.innerHTML = data.anggAktif.info.nama;
        let pasangan = util.getPasangan(data.anggAktif);
        if (pasangan) {
            info.view.pasanganP.innerHTML = pasangan.info.nama;
        }
        foto.gbr.src = data.anggAktif.foto.url;
    }
    get view() {
        return this._view;
    }
    get selesai() {
        return this._selesai;
    }
    set selesai(value) {
        this._selesai = value;
    }
}
class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='profile'>
				<div class='foto-cont'></div>
				<div class='info-cont'></div>
				<div class='anak-cont'></div>
				<div>
					<button class='tambah-pasangan'>tambah pasangan</button>
					<button class='hapus-pasangan'>hapus pasangan</button>
					<button class='hapus-foto'>hapus foto</button>
					<button class='unggah-foto'>unggah foto</button>
					<button class='edit-anak'>edit anak</button>
				</div>
				<button class='kembali'>kembali</button>
			</div>
		`;
        this.build();
    }
    get kembaliTbl() {
        return this.getEl('button.kembali');
    }
    get fotoCont() {
        return this.getEl('div.foto-cont');
    }
    get infoCont() {
        return this.getEl('div.info-cont');
    }
    get anakCont() {
        return this.getEl('div.anak-cont');
    }
}
export var profile = new Profile();
