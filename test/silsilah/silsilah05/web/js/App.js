import { beranda } from "./client/Beranda.js";
import { data } from "./Data.js";
class App {
    // private anggotaObj: AnggotaObj = new AnggotaObj();
    // private istriObj: AnggotaObj = new AnggotaObj();
    // private anak1Obj: AnggotaObj = new AnggotaObj();
    // private anak2Obj: AnggotaObj = new AnggotaObj();
    // private anak3Obj: AnggotaObj = new AnggotaObj();
    // private relObj: RelasiObj = new RelasiObj();
    constructor() {
        window.onload = () => {
            console.log('App');
            data.cont = this.cont;
            beranda.attach(data.cont);
            beranda.load();
            // let view: View = new View();
            this.init();
            // anggota.buatKeluarga('01');
            // render.render2(view, this.anggotaObj);
            // view.attach(this.cont);
            // this.adminTbl.onclick = () => {
            // 	this.cont.innerHTML = '';
            // 	daftar.attach(this.cont);
            // 	daftar.load();
            // 	console.log('admin click');
            // }
        };
    }
    get adminTbl() {
        return document.body.querySelector('button.admin');
    }
    init() {
        // this.anggotaObj = anggota.buatAnggota();
        // this.istriObj = anggota.buatAnggota();
        // this.anak1Obj = anggota.buatAnggota();
        // this.anak2Obj = anggota.buatAnggota();
        // this.anak3Obj = anggota.buatAnggota();
        // this.anggotaObj.info.nama = 'anak';
        // this.istriObj.info.nama = 'istri';
        // this.anak1Obj.info.nama = 'anak1';
        // this.anak2Obj.info.nama = 'anak2';
        // this.anak3Obj.info.nama = 'anak3';
        // anggota.list.push(this.anggotaObj);
        // anggota.list.push(this.istriObj);
        // anggota.list.push(this.anak1Obj);
        // anggota.list.push(this.anak2Obj);
        // anggota.list.push(this.anak3Obj);
        // this.relObj.anggota1 = this.anggotaObj;
        // this.relObj.anggota2 = this.istriObj;
        // anggota.listRelasi.push(this.relObj);
        // this.buatRelAnak(this.relObj, this.anak1Obj);
        // this.buatRelAnak(this.relObj, this.anak2Obj);
        // this.buatRelAnak(this.relObj, this.anak3Obj);
    }
    // buatRelAnak(rel: RelasiObj, anak: AnggotaObj): void {
    // 	let relAnak: RelasiAnakObj = new RelasiAnakObj();
    // 	relAnak.relasi = rel;
    // 	relAnak.anggota = anak;
    // 	anggota.listRelasiAnak.push(relAnak);
    // }
    get cont() {
        return document.body.querySelector('div.cont');
    }
}
new App();
