class RelasiAnak {
    constructor() {
        this.list = [];
    }
    baru(rel) {
        this.list.push(rel);
    }
    getByRel(rel) {
        let hasil = [];
        this.list.forEach((item) => {
            if (item.relasi == rel) {
                hasil.push(item);
            }
        });
        return hasil;
    }
}
export var relasiAnak = new RelasiAnak();
