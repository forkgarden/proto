import { BaseComponent } from "../BaseComponent.js";
class Edit extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='anggota-edit'>
				<form>
					<label>Nama:</label><br/>
					<input type="text" class='nama'>
					<input type='submit' class='simpan' value='simpan'>
				</form>
			</div>
		`;
        this.build();
        this.form.onsubmit = () => {
            try {
                this._anggota.info.nama = this.namaInput.value;
                this._finish();
            }
            catch (e) {
                console.error(e);
            }
            return false;
        };
    }
    isiForm() {
        this.namaInput.value = this._anggota.info.nama;
    }
    get namaInput() {
        return this.getEl('input.nama');
    }
    get form() {
        return this.getEl('form');
    }
    get anggota() {
        return this._anggota;
    }
    set anggota(value) {
        this._anggota = value;
    }
    set finish(value) {
        this._finish = value;
    }
}
export var edit = new Edit();
