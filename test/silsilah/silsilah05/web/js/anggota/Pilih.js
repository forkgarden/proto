import { anggota } from "../model/AnggotaController.js";
import { BaseComponent } from "../BaseComponent.js";
class Pilih extends BaseComponent {
    constructor() {
        super();
        this.items = [];
        this.ok = true;
        this._template = `
			<div class='daftar-anggota'>
				<h1>Pilih Anggota</h1>
				<div class='cont'>
				</div>
				<button class='pilih'>pilih</button>
				<button class='kembali'>kembali</button>
			</div>
		`;
        this.build();
        this.pilihTbl.onclick = () => {
            this.ok = true;
            this._selesai();
        };
        this.kembaliTbl.onclick = () => {
            this.ok = false;
            this._selesai();
        };
    }
    load() {
        this.items = [];
        this.cont.innerHTML = '';
        anggota.list.forEach((item) => {
            let view = new Item();
            view.nama.innerHTML = item.info.nama;
            view.attach(this.cont);
            view.anggota = item;
            view.elHtml.onclick = () => {
                let el = this.cont.querySelector('div.item.pilih');
                if (el) {
                    el.classList.remove('pilih');
                }
                view.elHtml.classList.add('pilih');
            };
            this.items.push(view);
        });
        this.items[0].elHtml.classList.add('pilih');
    }
    get pilihTbl() {
        return this.getEl('button.pilih');
    }
    get kembaliTbl() {
        return this.getEl('button.kembali');
    }
    get itemAktif() {
        let hasil;
        this.items.forEach((item) => {
            if (item.elHtml.classList.contains('pilih')) {
                hasil = item;
            }
        });
        return hasil;
    }
    get anggotaDipilih() {
        let view;
        if (!this.ok) {
            return null;
        }
        view = this.itemAktif;
        return view.anggota;
    }
    get cont() {
        return this.getEl('div.cont');
    }
    set selesai(value) {
        this._selesai = value;
    }
}
class Item extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
        this.build();
    }
    get anggota() {
        return this._anggota;
    }
    set anggota(value) {
        this._anggota = value;
    }
    get nama() {
        return this.getEl('p.nama');
    }
}
export var pilih = new Pilih();
