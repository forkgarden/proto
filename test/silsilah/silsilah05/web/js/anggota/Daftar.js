import { anggota } from "../model/AnggotaController.js";
import { BaseComponent } from "../BaseComponent.js";
import { data } from "../Data.js";
import { baru } from "./Baru.js";
// import { edit } from "./Edit.js";
class Daftar extends BaseComponent {
    constructor() {
        super();
        this.items = [];
        console.log('Daftar anggota');
        this._template = `
			<div class='daftar-anggota'>
				<h1>Daftar Anggota</h1>
				<div class='cont'>
				</div>
				<button class='edit'>edit</button>
				<button class='tambah'>tambah</button>
				<button class='hapus'>hapus</button>
				<button class='kembali'>kembali</button>
			</div>
		`;
        this.build();
        this.kembaliTbl.onclick = () => {
            this._selesai();
        };
        this.hapusTbl.onclick = () => {
            anggota.hapus(this.anggotaDipilih);
            this.load();
        };
        this.tambahTbl.onclick = () => {
            baru.view.attach(data.cont);
            baru.modeBaru = true;
            this.detach();
            baru.selesai = () => {
                baru.view.detach();
                this.attach(data.cont);
                this.load();
            };
        };
        this.editTbl.onclick = () => {
            baru.view.attach(data.cont);
            baru.modeBaru = false;
            baru.dataAwal = this.anggotaDipilih;
            this.detach();
            baru.selesai = () => {
                baru.view.detach();
                this.attach(data.cont);
                this.load();
            };
        };
    }
    //TODO: loading
    load() {
        this.items = [];
        this.cont.innerHTML = '';
        anggota.getAktif().forEach((item) => {
            let view = new Item();
            view.nama.innerHTML = item.info.nama + "/" + item.foto.url;
            view.attach(this.cont);
            view.anggota = item;
            view.elHtml.onclick = () => {
                let el = this.cont.querySelector('div.item.pilih');
                if (el) {
                    el.classList.remove('pilih');
                }
                view.elHtml.classList.add('pilih');
            };
            this.items.push(view);
        });
        this.items[0].elHtml.classList.add('pilih');
    }
    get kembaliTbl() {
        return this.getEl('button.kembali');
    }
    get viewDipilih() {
        let hasil;
        this.items.forEach((item) => {
            if (item.elHtml.classList.contains('pilih')) {
                hasil = item;
            }
        });
        return hasil;
    }
    get anggotaDipilih() {
        // let hasil: AnggotaObj;
        let view;
        view = this.viewDipilih;
        return view.anggota;
    }
    get cont() {
        return this.getEl('div.cont');
    }
    get editTbl() {
        return this.getEl('button.edit');
    }
    get selesai() {
        return this._selesai;
    }
    set selesai(value) {
        this._selesai = value;
    }
    get tambahTbl() {
        return this.getEl('button.tambah');
    }
    get hapusTbl() {
        return this.getEl('button.hapus');
    }
}
class Item extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
        this.build();
    }
    get anggota() {
        return this._anggota;
    }
    set anggota(value) {
        this._anggota = value;
    }
    get nama() {
        return this.getEl('p.nama');
    }
}
export var daftar = new Daftar();
