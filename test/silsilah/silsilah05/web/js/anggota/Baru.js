import { anggota } from "../model/AnggotaController.js";
import { AnggotaObj } from "../AnggotaObj.js";
import { BaseComponent } from "../BaseComponent.js";
import { pilih } from "../info/Pilih.js";
import { pilih as pilihFoto } from "../foto/Pilih.js";
import { data } from "../Data.js";
class Baru {
    constructor() {
        this._view = new View();
        this.view.form.onsubmit = () => {
            try {
                if (this._modeBaru) {
                    let anggotaObj = new AnggotaObj();
                    anggotaObj.info = this.infoObj;
                    anggotaObj.foto = this.fotoObj;
                    anggota.baru(anggotaObj);
                    this._selesai();
                }
                else {
                    this.dataAwal.info = this.infoObj;
                    this.dataAwal.foto = this.fotoObj;
                    anggota.edit(this.dataAwal);
                    this._selesai();
                }
            }
            catch (e) {
                console.error(e);
            }
            return false;
        };
        this.view.infoBtn.onclick = () => {
            this.view.detach();
            pilih.attach(data.cont);
            pilih.load();
            pilih.selesai = () => {
                pilih.detach();
                this.view.attach(data.cont);
                this.infoObj = pilih.infoDipilih;
                this.view.infoInput.value = this.infoObj.nama;
            };
        };
        this.view.fotoBtn.onclick = () => {
            this.view.detach();
            pilihFoto.attach(data.cont);
            pilihFoto.load();
            pilihFoto.selesai = () => {
                this.fotoObj = pilihFoto.fotoDipilih;
                pilihFoto.detach();
                this.view.attach(data.cont);
                this.view.fotoInput.value = this.fotoObj.url;
            };
        };
        this.view.kembaliBtn.onclick = () => {
            this._selesai();
        };
    }
    get dataAwal() {
        return this._dataAwal;
    }
    set dataAwal(value) {
        this._dataAwal = value;
    }
    set selesai(value) {
        this._selesai = value;
    }
    get view() {
        return this._view;
    }
    get modeBaru() {
        return this._modeBaru;
    }
    set modeBaru(value) {
        this._modeBaru = value;
    }
}
class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='info-edit'>
				<h1>edit Anggota</h1>
				<form>
					<label>Info:</label><br/>
					<input type="text" class='info'><button class='info' type='button'>...</button><br/>
					<label>Foto:</label><br/>
					<input type="text" class='foto'><button class='foto' type='button'>...</button><br/>
					<input type='submit' class='simpan' value='simpan'>
					<button type='button' class='kembali' value='kembali'>kembali</button>
				</form>
			</div>
		`;
        this.build();
    }
    get kembaliBtn() {
        return this.getEl('button.kembali');
    }
    get infoBtn() {
        return this.getEl('button.info');
    }
    get fotoBtn() {
        return this.getEl('button.foto');
    }
    get infoInput() {
        return this.getEl('input.info');
    }
    get fotoInput() {
        return this.getEl('input.foto');
    }
    get form() {
        return this.getEl('form');
    }
}
export var baru = new Baru();
