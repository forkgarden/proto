import { BaseComponent } from "./BaseComponent.js";
import { data } from "./Data.js";
import { util } from "./model/Util.js";
class Renderer {
    set infoTampil(value) {
        this._infoTampil = value;
    }
    render(view, anggotaObj, renderOrtu = false) {
        let foto = this.renderFoto(view.atas, anggotaObj.foto.url, anggotaObj.info.nama);
        foto.renderOrtu = renderOrtu;
        foto.utama = true;
        foto.viewCont = view;
        foto.anggota = anggotaObj;
        this.handleFoto(foto);
    }
    renderFoto(cont, imgUrl, nama, sembunyi = false) {
        let foto = new FotoView();
        foto.img.src = imgUrl;
        foto.namaP.innerHTML = nama;
        foto.attach(cont);
        if (sembunyi) {
            foto.elHtml.classList.add('sembunyi');
        }
        return foto;
    }
    handleFoto(foto) {
        if (foto.renderOrtu) {
            //TODO: render ortu
        }
        foto.elHtml.onclick = () => {
            if (foto.diload && foto.utama) {
                foto.toggle();
            }
            if (foto.utama && !foto.pasanganDiLoad) {
                foto.pasanganDiLoad = true;
                let pasangan = util.getPasangan(foto.anggota);
                if (pasangan) {
                    let fotoPasangan = this.renderFoto(foto.viewCont.atas, pasangan.foto.url, pasangan.info.nama);
                    fotoPasangan.pasangan = fotoPasangan;
                    fotoPasangan.utama = false;
                    fotoPasangan.pasanganDiLoad = true;
                    fotoPasangan.anakDiload = true;
                    fotoPasangan.anggota = pasangan;
                    this.handleFoto(fotoPasangan);
                }
            }
            if (foto.utama && !foto.anakDiload) {
                foto.anakDiload = true;
                let anak = util.getAnak2(foto.anggota);
                anak.forEach((item) => {
                    let div = document.createElement('div');
                    div.classList.add('cont-anak');
                    let view2 = new View();
                    this.render(view2, item);
                    view2.attach(div);
                    foto.viewCont.bawah.appendChild(div);
                });
                foto.anakCont = foto.viewCont.bawah;
            }
        };
        foto.infoTbl.onclick = (e) => {
            e.stopPropagation();
            console.log('foto info click');
            console.log(foto);
            data.anggAktif = foto.anggota;
            this._infoTampil();
        };
    }
}
export class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='anggota-cont'>
				<div class='atas'>
				</div>
				<div class='bawah'>
				</div>
			</div>
		`;
        this.build();
    }
    get atas() {
        return this.getEl('div.atas');
    }
    get bawah() {
        return this.getEl('div.bawah');
    }
}
class FotoView extends BaseComponent {
    constructor() {
        super();
        this._utama = true;
        this._renderOrtu = true;
        this._template = `
			<div class='foto'>
				<img src=''>
				<p class='nama'></p>
				<button class='info'>Info</button>
			</div>
		`;
        this.build();
    }
    get renderOrtu() {
        return this._renderOrtu;
    }
    set renderOrtu(value) {
        this._renderOrtu = value;
    }
    get viewCont() {
        return this._viewCont;
    }
    set viewCont(value) {
        this._viewCont = value;
    }
    get utama() {
        return this._utama;
    }
    set utama(value) {
        this._utama = value;
    }
    toggle() {
        if (this.pasangan) {
            this._pasangan._elHtml.classList.toggle('sembunyi');
        }
        if (this._anakCont) {
            this._anakCont.classList.toggle('sembunyi');
        }
    }
    get diload() {
        return this._pasanganDiLoad && this._anakDiload;
    }
    get anakDiload() {
        return this._anakDiload;
    }
    set anakDiload(value) {
        this._anakDiload = value;
    }
    get pasanganDiLoad() {
        return this._pasanganDiLoad;
    }
    set pasanganDiLoad(value) {
        this._pasanganDiLoad = value;
    }
    get img() {
        return this.getEl('img');
    }
    get namaP() {
        return this.getEl('p.nama');
    }
    get infoTbl() {
        return this.getEl('button.info');
    }
    get pasangan() {
        return this._pasangan;
    }
    set pasangan(value) {
        this._pasangan = value;
    }
    get anakCont() {
        return this._anakCont;
    }
    set anakCont(value) {
        this._anakCont = value;
    }
    get anggota() {
        return this._anggota;
    }
    set anggota(value) {
        this._anggota = value;
    }
}
export var render = new Renderer();
