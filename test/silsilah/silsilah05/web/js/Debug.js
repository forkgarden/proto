import { anggota } from "./model/AnggotaController.js";
import { RelasiObj, RelasiAnakObj } from "./AnggotaObj.js";
import { relasi } from "./Relasi.js";
import { relasiAnak } from "./RelasiAnak.js";
class Debug {
    buatKeluarga(tag, jmlAnak = 3) {
        let suami = anggota.buatAnggota();
        suami.info.nama = 'suami' + tag;
        anggota.baru(suami);
        let istri = anggota.buatAnggota();
        istri.info.nama = 'istri' + tag;
        anggota.baru(istri);
        let rel = new RelasiObj();
        rel.anggota1 = suami;
        rel.anggota2 = istri;
        relasi.baru(rel);
        for (let i = 0; i < jmlAnak; i++) {
            let anak = anggota.buatAnggota();
            anak.info.nama = 'anak' + i + tag;
            anggota.baru(anak);
            let relasiAnakObj = new RelasiAnakObj();
            relasiAnakObj.relasi = rel;
            relasiAnakObj.anggota = anak;
            relasiAnak.baru(relasiAnakObj);
        }
        return suami;
    }
}
export var debug = new Debug();
