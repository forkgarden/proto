import { daftar as daftarAnggota } from "./anggota/Daftar.js";
import { BaseComponent } from "./BaseComponent.js";
import { data } from "./Data.js";
import { daftar as daftarRelasi } from "./relasi/Daftar.js";
import { daftar as daftarInfo } from "./info/Daftar.js";
import { daftar as daftarFoto } from "./foto/Daftar.js";
import { daftar as daftarRelAnak } from "./relAnak/Daftar.js";
class Admin {
    constructor() {
        this._view = new View();
        console.log('Admin');
        this.view.anggotaTbl.onclick = () => {
            this.view.detach();
            daftarAnggota.attach(data.cont);
            daftarAnggota.load();
            daftarAnggota.selesai = () => {
                daftarAnggota.detach();
                this.view.attach(data.cont);
            };
        };
        this.view.relasiTbl.onclick = () => {
            this._view.detach();
            daftarRelasi.attach(data.cont);
            daftarRelasi.load();
            daftarRelasi.selesai = () => {
                daftarRelasi.detach();
                this.view.attach(data.cont);
            };
        };
        this.view.relasiAnakTbl.onclick = () => {
            this.view.detach();
            daftarRelAnak.attach(data.cont);
            daftarRelAnak.load();
            daftarRelAnak.selesai = () => {
                daftarRelAnak.detach();
                this.view.attach(data.cont);
            };
        };
        this.view.kembaliTbl.onclick = () => {
            this._selesai();
        };
        this.view.infoTbl.onclick = () => {
            this.view.detach();
            daftarInfo.attach(data.cont);
            daftarInfo.load();
            daftarInfo.selesai = () => {
                daftarInfo.detach();
                this.view.attach(data.cont);
            };
        };
        this.view.fotoTbl.onclick = () => {
            daftarFoto.attach(data.cont);
            daftarFoto.load();
            this.view.detach();
            daftarFoto.selesai = () => {
                daftarFoto.detach();
                this.view.attach(data.cont);
            };
        };
    }
    get selesai() {
        return this._selesai;
    }
    set selesai(value) {
        this._selesai = value;
    }
    get view() {
        return this._view;
    }
}
class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='admin'>
				<button class='anggota'>Anggota</button>
				<button class='relasi'>Relasi</button>
				<button class='relasi-anak'>Relasi Anak</button>
				<button class='foto'>Foto</button>
				<button class='info'>Info</button>
				<button class='kembali'>Kembali</button>
			</div>
		`;
        this.build();
    }
    get anggotaTbl() {
        return this.getEl('button.anggota');
    }
    get infoTbl() {
        return this.getEl('button.info');
    }
    get fotoTbl() {
        return this.getEl('button.foto');
    }
    get relasiTbl() {
        return this.getEl('button.relasi');
    }
    get relasiAnakTbl() {
        return this.getEl('button.relasi-anak');
    }
    get kembaliTbl() {
        return this.getEl('button.kembali');
    }
}
export var admin = new Admin();
