class Anggota {
    constructor() {
        this._list = [];
    }
    baru(anggotaObj) {
        this._list.push(anggotaObj);
    }
    getAktif() {
        let hasil = [];
        this._list.forEach((item) => {
            if (!item.hapus) {
                hasil.push(item);
            }
        });
        return hasil;
    }
    hapus(anggotaObj) {
        anggotaObj.hapus = true;
    }
    edit(anggotaObj) {
        anggotaObj;
    }
    // buatAnggota(nama: string = 'nama', jkl: string = 'L'): AnggotaObj {
    // 	let anggotaObj = new AnggotaObj();
    // 	anggotaObj.info = new InfoObj();
    // 	anggotaObj.foto = new FotoObj();
    // 	anggotaObj.info.nama = nama;
    // 	anggotaObj.info.jkl = jkl;
    // 	return anggotaObj
    // }
    get list() {
        return this._list;
    }
    set list(value) {
        this._list = value;
    }
}
export var anggota = new Anggota();
