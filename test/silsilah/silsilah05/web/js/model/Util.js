import { AnggotaObj, RelasiAnakObj, InfoObj, FotoObj } from "../AnggotaObj.js";
// import { anggota } from "./AnggotaController.js";
import { foto } from "./Foto.js";
import { info } from "./Info.js";
import { relasi } from "./Relasi.js";
import { relasiAnak } from "./RelasiAnak.js";
class Util {
    buatInfo(nama) {
        let infoObj = new InfoObj();
        infoObj.nama = nama;
        info.baru(infoObj);
        return infoObj;
    }
    buatFoto(url = 'gbr/kosong.png') {
        let fotoObj = new FotoObj();
        fotoObj.url = url;
        foto.baru(fotoObj);
        return fotoObj;
    }
    buatAnggota(nama = 'nama', jkl = 'L') {
        let anggotaObj = new AnggotaObj();
        let infoObj = this.buatInfo(nama);
        anggotaObj.info = infoObj;
        anggotaObj.foto = this.buatFoto();
        anggotaObj.info.nama = nama;
        anggotaObj.info.jkl = jkl;
        anggotaObj.hapus = false;
        return anggotaObj;
    }
    getAnak2(anggotaObj) {
        let rel = relasi.getRelasiByAnggota(anggotaObj);
        let relAnak = relasiAnak.getByRel(rel);
        let hasil = [];
        relAnak.forEach((item) => {
            hasil.push(item.anggota);
        });
        return hasil;
    }
    tambahPasangan2(angg, nama) {
        let angg2 = util.buatAnggota(nama);
        this.tambahPasangan(angg, angg2);
    }
    tambahPasangan(angg, pasangan) {
        relasi.baru2(angg, pasangan);
    }
    tambahAnak2(angg, nama) {
        let anggObj = util.buatAnggota(nama);
        this.tambahAnak(angg, anggObj);
    }
    tambahAnak(angg, anak) {
        let rel = relasi.getRelasiByAnggota(angg);
        let rel2 = new RelasiAnakObj();
        rel2.relasi = rel;
        rel2.anggota = anak;
        relasiAnak.baru(rel2);
    }
    getPasangan(anggota) {
        let rel = relasi.getRelasiByAnggota(anggota);
        if (!rel)
            return null;
        if (rel.anggota1 != anggota) {
            return rel.anggota1;
        }
        else if (rel.anggota2 != anggota) {
            return rel.anggota2;
        }
        else {
            console.error('error');
            return null;
        }
    }
}
export var util = new Util();
