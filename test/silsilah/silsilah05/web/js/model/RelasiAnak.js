class RelasiAnak {
    constructor() {
        this._list = [];
    }
    baru(rel) {
        this.list.push(rel);
    }
    hapus(rel) {
        rel.hapus = true;
    }
    get list() {
        return this._list;
    }
    getByRel(rel) {
        let hasil = [];
        this.list.forEach((item) => {
            if (item.relasi == rel) {
                hasil.push(item);
            }
        });
        return hasil;
    }
}
export var relasiAnak = new RelasiAnak();
