class Data {
    get anggAktif() {
        return this._angg;
    }
    set anggAktif(value) {
        this._angg = value;
    }
    get cont() {
        return this._cont;
    }
    set cont(value) {
        this._cont = value;
    }
}
export var data = new Data();
