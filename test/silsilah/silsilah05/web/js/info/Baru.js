// import { anggota } from "../model/AnggotaController.js";
import { InfoObj } from "../AnggotaObj.js";
import { BaseComponent } from "../BaseComponent.js";
// import { util } from "../model/Util.js";
import { info } from "../model/Info.js";
class Baru extends BaseComponent {
    constructor() {
        super();
        this._modeBaru = true;
        this._template = `
			<div class='anggota-edit'>
				<form>
					<label>Nama:</label><br/>
					<input type="text" class='nama'>
					<input type='submit' class='simpan' value='simpan'>
				</form>
			</div>
		`;
        this.build();
        this.form.onsubmit = () => {
            try {
                if (this._modeBaru) {
                    let infoObj = new InfoObj();
                    infoObj.nama = this.namaInput.value;
                    info.baru(infoObj);
                }
                else {
                    this._infoAwal.nama = this.namaInput.value;
                    info.edit(this._infoAwal).then(() => {
                        this._finish();
                    }).catch(() => {
                        //TODO:
                    });
                }
            }
            catch (e) {
                console.error(e);
            }
            return false;
        };
    }
    get namaInput() {
        return this.getEl('input.nama');
    }
    get form() {
        return this.getEl('form');
    }
    set finish(value) {
        this._finish = value;
    }
    get modeBaru() {
        return this._modeBaru;
    }
    set modeBaru(value) {
        this._modeBaru = value;
    }
    get infoAwal() {
        return this._infoAwal;
    }
    set infoAwal(value) {
        this._infoAwal = value;
    }
}
export var baru = new Baru();
