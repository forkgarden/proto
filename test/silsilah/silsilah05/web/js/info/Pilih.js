import { BaseComponent } from "../BaseComponent.js";
import { info } from "../model/Info.js";
class Pilih extends BaseComponent {
    constructor() {
        super();
        this.items = [];
        this._template = `
			<div class='daftar-info'>
				<h1>Pilih Info</h1>
				<div class='cont'>
				</div>
				<button class='kembali'>pilih</button>
			</div>
		`;
        this.build();
        this.kembaliTbl.onclick = () => {
            this._selesai();
        };
    }
    load() {
        this.items = [];
        this.cont.innerHTML = '';
        info.list.forEach((item) => {
            let view = new Item();
            view.nama.innerHTML = item.nama;
            view.attach(this.cont);
            view.info = item;
            view.elHtml.onclick = () => {
                let el = this.cont.querySelector('div.item.pilih');
                if (el) {
                    el.classList.remove('pilih');
                }
                view.elHtml.classList.add('pilih');
            };
            this.items.push(view);
        });
        this.items[0].elHtml.classList.add('pilih');
    }
    get kembaliTbl() {
        return this.getEl('button.kembali');
    }
    get itemDipilih() {
        let hasil;
        this.items.forEach((item) => {
            if (item.elHtml.classList.contains('pilih')) {
                hasil = item;
            }
        });
        return hasil;
    }
    get infoDipilih() {
        let view;
        view = this.itemDipilih;
        return view.info;
    }
    get cont() {
        return this.getEl('div.cont');
    }
    set selesai(value) {
        this._selesai = value;
    }
}
class Item extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
        this.build();
    }
    get info() {
        return this._info;
    }
    set info(value) {
        this._info = value;
    }
    get nama() {
        return this.getEl('p.nama');
    }
}
export var pilih = new Pilih();
