import { admin } from "./Admin.js";
import { BaseComponent } from "./BaseComponent.js";
import { data } from "./Data.js";
import { debug } from "./model/Debug.js";
import { profile } from "./client/profile/Profile.js";
import { render, View } from "./Renderer.js";
class Beranda extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='beranda'>
				<button class='admin'>admin</button>
				<div class='cont'>
				</div>
			<div>
		`;
        this.build();
        this.adminTbl.onclick = () => {
            this.detach();
            admin.view.attach(data.cont);
            admin.selesai = () => {
                admin.view.detach();
                this.attach(data.cont);
                this.refresh();
            };
        };
        render.infoTampil = () => {
            profile.view.attach(data.cont);
            profile.load();
            this.detach();
            profile.selesai = () => {
                profile.view.detach();
                this.attach(data.cont);
            };
        };
    }
    refresh() {
        this.cont.innerHTML = '';
        let view = new View();
        render.render(view, this.anggotaObj);
        view.attach(this.cont);
    }
    load() {
        this.anggotaObj = debug.debug();
        this.refresh();
    }
    get adminTbl() {
        return this.getEl('button.admin');
    }
    get cont() {
        return this.getEl('div.cont');
    }
}
export var beranda = new Beranda();
