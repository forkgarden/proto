export class InfoObj {
    constructor() {
        this._id = '';
    }
    get hapus() {
        return this._hapus;
    }
    set hapus(value) {
        this._hapus = value;
    }
    get jkl() {
        return this._jkl;
    }
    set jkl(value) {
        this._jkl = value;
    }
    get id() {
        return this._id;
    }
    set id(value) {
        this._id = value;
    }
    get nama() {
        return this._nama;
    }
    set nama(value) {
        this._nama = value;
    }
}
export class RelasiObj {
    get hapus() {
        return this._hapus;
    }
    set hapus(value) {
        this._hapus = value;
    }
    get anggota1() {
        return this._anggota1;
    }
    set anggota1(value) {
        this._anggota1 = value;
    }
    get anggota2() {
        return this._anggota2;
    }
    set anggota2(value) {
        this._anggota2 = value;
    }
}
export class RelasiAnakObj {
    get hapus() {
        return this._hapus;
    }
    set hapus(value) {
        this._hapus = value;
    }
    get urutan() {
        return this._urutan;
    }
    set urutan(value) {
        this._urutan = value;
    }
    get relasi() {
        return this._relasi;
    }
    set relasi(value) {
        this._relasi = value;
    }
    get anggota() {
        return this._anggota;
    }
    set anggota(value) {
        this._anggota = value;
    }
}
export class AnggotaObj {
    get hapus() {
        return this._hapus;
    }
    set hapus(value) {
        this._hapus = value;
    }
    get info() {
        return this._info;
    }
    set info(value) {
        this._info = value;
    }
    get foto() {
        return this._foto;
    }
    set foto(value) {
        this._foto = value;
    }
}
export class FotoObj {
    get hapus() {
        return this._hapus;
    }
    set hapus(value) {
        this._hapus = value;
    }
    get url() {
        return this._url;
    }
    set url(value) {
        this._url = value;
    }
}
