import { BaseComponent } from "../BaseComponent.js";
import { data } from "../Data.js";
import { baru } from "./Baru.js";
import { edit } from "./Edit.js";
import { info } from "../model/Info.js";
class Daftar extends BaseComponent {
    constructor() {
        super();
        this.items = [];
        this._template = `
			<div class='daftar-info'>
				<h1>Daftar Info</h1>
				<div class='cont'>
				</div>
				<button class='edit'>edit</button>
				<button class='tambah'>tambah</button>
				<button class='hapus'>hapus</button>
				<button class='kembali'>kembali</button>
			</div>
		`;
        this.build();
        this.kembaliTbl.onclick = () => {
            this._selesai();
        };
        this.hapusTbl.onclick = () => {
            info.hapus(this.infoDipilih);
            this.load();
        };
        this.tambahTbl.onclick = () => {
            baru.attach(data.cont);
            this.detach();
            baru.finish = () => {
                baru.detach();
                this.attach(data.cont);
                this.load();
            };
        };
        this.editTbl.onclick = () => {
            this.detach();
            edit.attach(data.cont);
            edit.info = this.infoDipilih;
            edit.isiForm();
            edit.finish = () => {
                edit.detach();
                this.attach(data.cont);
                this.load();
            };
        };
    }
    load() {
        this.items = [];
        this.cont.innerHTML = '';
        info.list.forEach((item) => {
            let view = new Item();
            view.nama.innerHTML = item.nama;
            view.attach(this.cont);
            view.info = item;
            view.elHtml.onclick = () => {
                let el = this.cont.querySelector('div.item.pilih');
                if (el) {
                    el.classList.remove('pilih');
                }
                view.elHtml.classList.add('pilih');
            };
            this.items.push(view);
        });
        this.items[0].elHtml.classList.add('pilih');
    }
    get kembaliTbl() {
        return this.getEl('button.kembali');
    }
    get viewDipilih() {
        let hasil;
        this.items.forEach((item) => {
            if (item.elHtml.classList.contains('pilih')) {
                hasil = item;
            }
        });
        return hasil;
    }
    get infoDipilih() {
        // let hasil: AnggotaObj;
        let view;
        view = this.viewDipilih;
        return view.info;
    }
    get cont() {
        return this.getEl('div.cont');
    }
    get editTbl() {
        return this.getEl('button.edit');
    }
    get selesai() {
        return this._selesai;
    }
    set selesai(value) {
        this._selesai = value;
    }
    get tambahTbl() {
        return this.getEl('button.tambah');
    }
    get hapusTbl() {
        return this.getEl('button.hapus');
    }
}
class Item extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
        this.build();
    }
    get info() {
        return this._info;
    }
    set info(value) {
        this._info = value;
    }
    get nama() {
        return this.getEl('p.nama');
    }
}
export var daftar = new Daftar();
