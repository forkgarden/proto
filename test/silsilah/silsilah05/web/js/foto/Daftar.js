import { BaseComponent } from "../BaseComponent.js";
import { data } from "../Data.js";
import { foto } from "../model/Foto.js";
import { fotoUpload } from "./PhotoUploadPage.js";
// import { baru } from "./Baru.js";
class Daftar extends BaseComponent {
    constructor() {
        super();
        this.items = [];
        this._template = `
			<div class='daftar-foto'>
				<h1>Daftar Foto</h1>
				<div class='cont'>
				</div>
				<button class='tambah'>tambah</button>
				<button class='hapus'>hapus</button>
				<button class='kembali'>kembali</button>
			</div>
		`;
        this.build();
        this.kembaliTbl.onclick = () => {
            this._selesai();
        };
        this.hapusTbl.onclick = () => {
            foto.hapus(this.fotoDipilih);
            this.load();
        };
        this.tambahTbl.onclick = () => {
            fotoUpload.attach(data.cont);
            this.detach();
            fotoUpload.selesai = () => {
                fotoUpload.detach();
                this.attach(data.cont);
                this.load();
            };
        };
    }
    load() {
        this.items = [];
        this.cont.innerHTML = '';
        foto.baca().forEach((item) => {
            let view = new Item();
            view.nama.innerHTML = item.url;
            view.attach(this.cont);
            view.foto = item;
            view.elHtml.onclick = () => {
                let el = this.cont.querySelector('div.item.pilih');
                if (el) {
                    el.classList.remove('pilih');
                }
                view.elHtml.classList.add('pilih');
            };
            this.items.push(view);
        });
        this.items[0].elHtml.classList.add('pilih');
    }
    get kembaliTbl() {
        return this.getEl('button.kembali');
    }
    get viewDipilih() {
        let hasil;
        this.items.forEach((item) => {
            if (item.elHtml.classList.contains('pilih')) {
                hasil = item;
            }
        });
        return hasil;
    }
    get fotoDipilih() {
        let view;
        view = this.viewDipilih;
        return view.foto;
    }
    get cont() {
        return this.getEl('div.cont');
    }
    get selesai() {
        return this._selesai;
    }
    set selesai(value) {
        this._selesai = value;
    }
    get tambahTbl() {
        return this.getEl('button.tambah');
    }
    get hapusTbl() {
        return this.getEl('button.hapus');
    }
}
class Item extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
        this.build();
    }
    get foto() {
        return this._foto;
    }
    set foto(value) {
        this._foto = value;
    }
    get nama() {
        return this.getEl('p.nama');
    }
}
export var daftar = new Daftar();
