import { anggota } from "../model/AnggotaController.js";
import { BaseComponent } from "../BaseComponent.js";
import { util } from "../model/Util.js";
class Baru extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='anggota-edit'>
				<form>
					<label>Nama:</label><br/>
					<input type="text" class='nama'>
					<input type='submit' class='simpan' value='simpan'>
				</form>
			</div>
		`;
        this.build();
        this.form.onsubmit = () => {
            try {
                let anggotaObj = util.buatAnggota(this.namaInput.value);
                anggota.baru(anggotaObj);
                this._finish();
            }
            catch (e) {
                console.error(e);
            }
            return false;
        };
    }
    get namaInput() {
        return this.getEl('input.nama');
    }
    get form() {
        return this.getEl('form');
    }
    set finish(value) {
        this._finish = value;
    }
}
export var baru = new Baru();
