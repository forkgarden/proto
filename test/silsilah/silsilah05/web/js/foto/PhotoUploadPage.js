var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { BaseComponent } from "../BaseComponent.js";
class PhotoUploadPage extends BaseComponent {
    constructor() {
        super();
        this._selesai = null;
        this._insertedId = '';
        this._gbrUrl = '';
        this._template = `
		<div class='foto-page'>
			<h3>Upload Foto:</h3>
			<form>
				<input type="file" accept="">
				<br />
				<p>Foto:</p>
				<div class='foto-cont'>

				</div>
				<p>Thumbnail:</p>
				<div class='thumb-cont'>

				</div>
				<br />

				<input type='submit' class='btn btn-primary upload' value='upload'>
				<button type="button" class='btn btn-secondary tutup'>tutup</button>
			</form>
		</div>
		`;
        this.build();
        this.init();
    }
    createName(prefix, pjg = 12) {
        let hasil = prefix;
        let karakter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        let date = new Date();
        for (let i = 0; i < pjg; i++) {
            hasil += karakter.charAt(Math.floor(Math.random() * karakter.length));
        }
        hasil += date.getFullYear() + '_' + date.getMonth() + '_' + date.getDate() + '_' + date.getHours() + '_' + date.getMinutes() + '_' + date.getSeconds() + '_' + date.getMilliseconds();
        hasil += '.png';
        console.log('nama: ' + hasil);
        return hasil;
    }
    init() {
        this.initInput(this.input);
        this.form.onsubmit = () => {
            this.upload();
            this._selesai();
            return false;
        };
        this.tutupTbl.onclick = () => {
            this._selesai();
        };
        console.groupEnd();
    }
    loadImage3(file) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.loadImage2(file, 800, 800, "gbr_besar", this.fotoCont);
            yield this.loadImage2(file, 128, 128, "thumb", this.thumbCont);
        });
    }
    loadImage2(file, panjang, lebar, id, cont) {
        return __awaiter(this, void 0, void 0, function* () {
            let canvas;
            let img = yield loadImage(file.files[0], {
                maxWidth: panjang,
                maxHeight: lebar,
                canvas: true,
                orientation: true,
                imageSmoothingQuality: 'high'
            });
            canvas = img.image;
            canvas.setAttribute("id", id);
            cont.appendChild(canvas);
        });
    }
    populateData() {
        let formData = new FormData();
        formData.append("gbr_besar", this.canvasBesar.toDataURL());
        formData.append("gbr_kecil", this.canvasThumb.toDataURL());
        return formData;
    }
    populateJson() {
        let obj = {
            gbr_besar: this.canvasBesar.toDataURL(),
            gbr_kecil: this.canvasThumb.toDataURL(),
            gbr_besar_nama: this.createName('gbr_besar_', 8),
            gbr_kecil_nama: this.createName('gbr_kecil_', 8)
        };
        return JSON.stringify(obj);
    }
    initInput(input) {
        input.onchange = () => {
            this.loadImage3(input).then(() => {
            }).catch((e) => {
                console.error(e);
            });
        };
    }
    upload() {
    }
    get listCont() {
        return this.getEl('div.list-cont');
    }
    get form() {
        return this.getEl('form');
    }
    get input() {
        return this.getEl('input');
    }
    get uploadTbl() {
        return this.getEl('input.upload');
    }
    get canvasBesar() {
        return this.getEl('canvas#gbr_besar');
    }
    get canvasThumb() {
        return this.getEl('canvas#thumb');
    }
    get tutupTbl() {
        return this.getEl('button.tutup');
    }
    get selesai() {
        return this._selesai;
    }
    set selesai(value) {
        this._selesai = value;
    }
    get insertedId() {
        return this._insertedId;
    }
    get gbrUrl() {
        return this._gbrUrl;
    }
    get fotoCont() {
        return this.getEl('div.foto-cont');
    }
    get thumbCont() {
        return this.getEl('div.thumb-cont');
    }
}
export var fotoUpload = new PhotoUploadPage();
