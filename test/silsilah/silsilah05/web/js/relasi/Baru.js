import { pilih } from "../anggota/Pilih.js";
import { RelasiObj } from "../AnggotaObj.js";
import { BaseComponent } from "../BaseComponent.js";
import { data } from "../Data.js";
import { relasi } from "../model/Relasi.js";
class BaruEdit {
    constructor() {
        this._modeBaru = true;
        this._view = new View();
        console.log('Baru');
    }
    init() {
        this.anggota1 = null;
        this.anggota2 = null;
        this.view.kembaliTbl.onclick = () => {
            this._selesai();
        };
        this.view.form.onsubmit = () => {
            console.log('on submit');
            try {
                if (!this.anggota1) {
                    console.log('anggota 1 belum dipilih');
                    return false;
                }
                if (!this.anggota2) {
                    console.log('anggota 2 belum dipilih');
                    return false;
                }
                if (this.modeBaru) {
                    let rel = new RelasiObj();
                    rel.anggota1 = this.anggota1;
                    rel.anggota2 = this.anggota2;
                    relasi.baru(rel);
                    this._selesai();
                }
                else {
                    this._rel.anggota1 = this.anggota1;
                    this._rel.anggota2 = this.anggota2;
                    this._selesai();
                }
            }
            catch (e) {
                console.error(e);
            }
            return false;
        };
        this.view.angg1Tbl.onclick = () => {
            this.view.detach();
            pilih.attach(data.cont);
            pilih.load();
            pilih.selesai = () => {
                this.anggota1 = pilih.anggotaDipilih;
                pilih.detach();
                this.view.attach(data.cont);
                this.view.anggota1Input.value = this.anggota1.info.nama;
            };
        };
        this.view.angg2Tbl.onclick = () => {
            this.view.detach();
            pilih.attach(data.cont);
            pilih.load();
            pilih.selesai = () => {
                this.anggota2 = pilih.anggotaDipilih;
                pilih.detach();
                this.view.attach(data.cont);
                if (this.anggota2) {
                    this.view.anggota2Input.value = this.anggota2.info.nama;
                }
                else {
                    this.view.anggota2Input.value = '';
                }
            };
        };
    }
    get view() {
        return this._view;
    }
    get rel() {
        return this._rel;
    }
    set rel(value) {
        this._rel = value;
    }
    set selesai(value) {
        this._selesai = value;
    }
    get modeBaru() {
        return this._modeBaru;
    }
    set modeBaru(value) {
        this._modeBaru = value;
    }
}
class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='relasi-baru'>
				<form>
					<label>Anggota 1</label><br/>
					<input type='text' class='anggota1'>
					<button class='anggota1'>...</button><br/>

					<label>Anggota 2</label><br/>
					<input type='text' class='anggota2'>
					<button type='button' class='anggota2'>...</button><br/>

					<button type='submit'>simpan</button>
					<button type='button' class='kembali'>kembali</button>
				</form>
			</div>
		`;
        this.build();
    }
    get anggota1Input() {
        return this.getEl('input.anggota1');
    }
    get anggota2Input() {
        return this.getEl('input.anggota2');
    }
    get angg1Tbl() {
        return this.getEl('button.anggota1');
    }
    get angg2Tbl() {
        return this.getEl('button.anggota2');
    }
    get form() {
        return this.getEl('form');
    }
    get kembaliTbl() {
        return this.getEl('button.kembali');
    }
}
export var baruEdit = new BaruEdit();
