import { BaseComponent } from "../BaseComponent.js";
import { relasi } from "../model/Relasi.js";
class Pilih extends BaseComponent {
    constructor() {
        super();
        this.items = [];
        this.ok = true;
        this._template = `
			<div class='daftar-relasi'>
				<h1>Pilih Relasi</h1>
				<div class='cont'>
				</div>
				<button class='pilih'>pilih</button>
				<button class='kembali'>kembali</button>
			</div>
		`;
        this.build();
        this.kembaliTbl.onclick = () => {
            this.ok = false;
            this._selesai();
        };
        this.pilihTbl.onclick = () => {
            this.ok = true;
            this._selesai();
        };
    }
    load() {
        this.items = [];
        this.cont.innerHTML = '';
        relasi.list.forEach((item) => {
            let view = new Item();
            view.nama.innerHTML = item.anggota1.info.nama + "-" + item.anggota2.info.nama;
            view.attach(this.cont);
            view.relasi = item;
            view.elHtml.onclick = () => {
                let el = this.cont.querySelector('div.item.pilih');
                if (el) {
                    el.classList.remove('pilih');
                }
                view.elHtml.classList.add('pilih');
            };
            this.items.push(view);
        });
        this.items[0].elHtml.classList.add('pilih');
    }
    get kembaliTbl() {
        return this.getEl('button.kembali');
    }
    get pilihTbl() {
        return this.getEl('button.pilih');
    }
    get itemViewDipilih() {
        let hasil;
        this.items.forEach((item) => {
            if (item.elHtml.classList.contains('pilih')) {
                hasil = item;
            }
        });
        return hasil;
    }
    get relasiDipilih() {
        let view;
        if (!this.ok) {
            return null;
        }
        view = this.itemViewDipilih;
        return view.relasi;
    }
    get cont() {
        return this.getEl('div.cont');
    }
    set selesai(value) {
        this._selesai = value;
    }
}
class Item extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
        this.build();
    }
    get relasi() {
        return this._relasi;
    }
    set relasi(value) {
        this._relasi = value;
    }
    get nama() {
        return this.getEl('p.nama');
    }
}
export var pilih = new Pilih();
