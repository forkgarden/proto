import { AnggotaObj, InfoObj, FotoObj } from "./AnggotaObj.js";
// import { relasi } from "./Relasi.js";
class Anggota {
    constructor() {
        this._list = [];
    }
    baru(anggotaObj) {
        this._list.push(anggotaObj);
    }
    hapus(anggotaObj) {
        for (let i = 0; i < this._list.length; i++) {
            if (this._list[i] == anggotaObj) {
                this._list.splice(i, 1);
            }
        }
    }
    buatAnggota(nama = 'nama', jkl = 'L') {
        let anggotaObj = new AnggotaObj();
        anggotaObj.info = new InfoObj();
        anggotaObj.foto = new FotoObj();
        anggotaObj.info.nama = nama;
        anggotaObj.info.jkl = jkl;
        return anggotaObj;
    }
    get list() {
        return this._list;
    }
    set list(value) {
        this._list = value;
    }
}
export var anggota = new Anggota();
