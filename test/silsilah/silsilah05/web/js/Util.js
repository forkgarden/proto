import { relasi } from "./model/Relasi.js";
import { relasiAnak } from "./RelasiAnak.js";
class Util {
    getAnak2(anggotaObj) {
        let rel = relasi.getRelasi(anggotaObj);
        let relAnak = relasiAnak.getByRel(rel);
        let hasil = [];
        relAnak.forEach((item) => {
            hasil.push(item.anggota);
        });
        return hasil;
    }
    tambahPasangan(angg, pasangan) {
        relasi.baru2(angg, pasangan);
    }
    tambahAnak(angg, anak) {
        let rel = relasi.getRelasi(angg);
    }
    getPasangan(anggota) {
        let rel = relasi.getRelasi(anggota);
        if (!rel)
            return null;
        if (rel.anggota1 != anggota) {
            return rel.anggota1;
        }
        else if (rel.anggota2 != anggota) {
            return rel.anggota2;
        }
        else {
            console.error('error');
            return null;
        }
    }
}
export var util = new Util();
