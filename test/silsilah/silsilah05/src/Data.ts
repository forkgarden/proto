import { AnggotaObj } from "./AnggotaObj";

class Data {
	private _cont: HTMLDivElement;
	private _angg: AnggotaObj;
	public get anggAktif(): AnggotaObj {
		return this._angg;
	}
	public set anggAktif(value: AnggotaObj) {
		this._angg = value;
	}

	public get cont(): HTMLDivElement {
		return this._cont;
	}
	public set cont(value: HTMLDivElement) {
		this._cont = value;
	}
}

export var data: Data = new Data();