import { BaseComponent } from "../../BaseComponent.js";

class Info {
	private _view: View = new View();
	public get view(): View {
		return this._view;
	}
}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='info'>
				<p class='label'>Nama:</p>
				<p class='info nama'></p>
				<p class='label'>Pasangan:</p>
				<p class='info pasangan'></p>
			</div>
		`;
		this.build();
	}

	get namaP(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}
	get pasanganP(): HTMLParagraphElement {
		return this.getEl('p.pasangan') as HTMLParagraphElement;
	}
}

export var info: Info = new Info();