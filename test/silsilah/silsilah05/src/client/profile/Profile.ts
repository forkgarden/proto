import { AnggotaObj } from "../../AnggotaObj.js";
import { BaseComponent } from "../../BaseComponent.js";
import { data } from "../../Data.js";
import { util } from "../../model/Util.js";
import { foto } from "./Foto.js";
import { info } from "./Info.js";

class Profile {
	private _view: View = new View();
	private _selesai: Function;

	constructor() {
		info.view.attach(this._view.infoCont);
		foto.attach(this._view.fotoCont);

		this.view.kembaliTbl.onclick = () => {
			this._selesai();
		}
	}

	load(): void {
		info.view.namaP.innerHTML = data.anggAktif.info.nama;
		let pasangan: AnggotaObj = util.getPasangan(data.anggAktif);
		if (pasangan) {
			info.view.pasanganP.innerHTML = pasangan.info.nama;
		}

		foto.gbr.src = data.anggAktif.foto.url;
	}

	public get view(): View {
		return this._view;
	}

	public get selesai(): Function {
		return this._selesai;
	}
	public set selesai(value: Function) {
		this._selesai = value;
	}

}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='profile'>
				<div class='foto-cont'></div>
				<div class='info-cont'></div>
				<div class='anak-cont'></div>
				<div>
					<button class='tambah-pasangan'>tambah pasangan</button>
					<button class='hapus-pasangan'>hapus pasangan</button>
					<button class='hapus-foto'>hapus foto</button>
					<button class='unggah-foto'>unggah foto</button>
					<button class='edit-anak'>edit anak</button>
				</div>
				<button class='kembali'>kembali</button>
			</div>
		`;
		this.build();

	}

	get kembaliTbl(): HTMLButtonElement {
		return this.getEl('button.kembali') as HTMLButtonElement;
	}

	get fotoCont(): HTMLDivElement {
		return this.getEl('div.foto-cont') as HTMLDivElement;
	}

	get infoCont(): HTMLDivElement {
		return this.getEl('div.info-cont') as HTMLDivElement;
	}

	get anakCont(): HTMLDivElement {
		return this.getEl('div.anak-cont') as HTMLDivElement;
	}

}

export var profile: Profile = new Profile();

