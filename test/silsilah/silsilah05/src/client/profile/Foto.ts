import { BaseComponent } from "../../BaseComponent.js";

class Foto extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='foto'>
				<img src=''><br/>
			</div>
		`;
		this.build();
	}

	get gbr(): HTMLImageElement {
		return this.getEl('img') as HTMLImageElement;
	}

	get hapusTbl(): HTMLButtonElement {
		return this.getEl('button.hapus') as HTMLButtonElement;
	}

	get unggahTbl(): HTMLButtonElement {
		return this.getEl('button.unggah') as HTMLButtonElement;
	}
}

export var foto: Foto = new Foto();

