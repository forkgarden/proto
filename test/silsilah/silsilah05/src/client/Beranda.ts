import { admin } from "../admin/Admin.js";
// import { anggota } from "./AnggotaController.js";
import { AnggotaObj } from "../AnggotaObj.js";
import { BaseComponent } from "../BaseComponent.js";
import { data } from "../Data.js";
import { debug } from "../model/Debug.js";
import { profile } from "./profile/Profile.js";
import { render, View } from "../Renderer.js";

class Beranda extends BaseComponent {
	private anggotaObj: AnggotaObj;

	constructor() {
		super();
		this._template = `
			<div class='beranda'>
				<button class='admin'>admin</button>
				<div class='cont'>
				</div>
			<div>
		`;
		this.build();

		this.adminTbl.onclick = () => {
			this.detach();
			admin.view.attach(data.cont);
			admin.selesai = () => {
				admin.view.detach();
				this.attach(data.cont);
				this.refresh();
			}
		}

		render.infoTampil = () => {
			profile.view.attach(data.cont);
			profile.load();
			this.detach();

			profile.selesai = () => {
				profile.view.detach();
				this.attach(data.cont);
			}
		}
	}

	refresh(): void {
		this.cont.innerHTML = '';
		let view: View = new View();
		render.render(view, this.anggotaObj);
		view.attach(this.cont);
	}

	load(): void {
		this.anggotaObj = debug.debug();
		this.refresh();
	}

	get adminTbl(): HTMLButtonElement {
		return this.getEl('button.admin') as HTMLButtonElement;
	}

	get cont(): HTMLDivElement {
		return this.getEl('div.cont') as HTMLDivElement;
	}
}

export var beranda: Beranda = new Beranda();