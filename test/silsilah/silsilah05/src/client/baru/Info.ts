import { BaseComponent } from "../../BaseComponent.js";

class Info {
	private _view: View = new View();

	public get view(): View {
		return this._view;
	}
}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = ``;
		this.build();
	}
}

export var info: Info = new Info();