import { BaseComponent } from "../../BaseComponent.js";

class Pasangan {
	private _view: View = new View();

	public get view(): View {
		return this._view;
	}
}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = ``;
		this.build();
	}
}

export var pasangan: Pasangan = new Pasangan();