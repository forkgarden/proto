import { BaseComponent } from "../../BaseComponent.js";

class Anak {
	private _view: View = new View();

	public get view(): View {
		return this._view;
	}
}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='anak-baru'>
				<div class='daftar-anak-cont'></div>
				<button class='tambah'>tambah</button>
			</div>
		`;
		this.build();
	}

	load(): void {

	}

	get cont(): HTMLDivElement {
		return this.getEl('div.daftar-anak-cont') as HTMLDivElement;
	}

	get tambahTbl(): HTMLButtonElement {
		return this.getEl('button.tambah') as HTMLButtonElement;
	}

}

class AnakItem {

}

export var anak: Anak = new Anak();