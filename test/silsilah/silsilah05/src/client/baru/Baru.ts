import { BaseComponent } from "../../BaseComponent.js";
import { anak } from "./Anak.js";
import { foto } from "./Foto.js";
import { info } from "./Info.js";
import { pasangan } from "./Pasangan.js";

class Baru {
	private _view: View = new View();
	private _selesai: Function;
	private _modeBaru: boolean = false;

	constructor() {
		info.view.attach(this._view.infoCont);
		foto.view.attach(this._view.fotoCont);
		pasangan.view.attach(this._view.pasanganCont);
		anak.view.attach(this._view.anakCont);
		this._view.simpanTbl.onclick = () => {
			this._selesai();
		}

		this._view.tutupTbl.onclick = () => {
			this._selesai();
		}
	}

	public get modeBaru(): boolean {
		return this._modeBaru;
	}


	public set selesai(value: Function) {
		this._selesai = value;
	}

	public get view(): View {
		return this._view;
	}
}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='Anggota-Baru'>
				<div class='foto-cont'></div>
				<div class='info-cont'></div>
				<div class='pasangan-cont'></div>
				<div class='anak-cont'></div>
				<button class='simpan'>Simpan</button>
				<button class='tutup'>Tutup</button>
			</div>
		`;
		this.build();
	}

	get simpanTbl(): HTMLButtonElement {
		return this.getEl('button.simpan') as HTMLButtonElement;
	}

	get tutupTbl(): HTMLButtonElement {
		return this.getEl('button.tutup') as HTMLButtonElement;
	}

	get fotoCont(): HTMLDivElement {
		return this.getEl('div.foto-cont') as HTMLDivElement;
	}

	get infoCont(): HTMLDivElement {
		return this.getEl('div.info-cont') as HTMLDivElement;
	}
	get pasanganCont(): HTMLDivElement {
		return this.getEl('div.pasangan-cont') as HTMLDivElement;
	}
	get anakCont(): HTMLDivElement {
		return this.getEl('div.anak-cont') as HTMLDivElement;
	}

}

export var baru: Baru = new Baru();