import { BaseComponent } from "../../BaseComponent.js";

class Foto {
	private _view: View = new View();

	public get view(): View {
		return this._view;
	}
}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='foto-baru'>
				<canvas></canvas>
				<button class='upload'>Unggah</button>
			</div>
		`;
		this.build();
	}

	get unggahBtn(): HTMLButtonElement {
		return this.getEl('button.upload') as HTMLButtonElement;
	}

	get canvas(): HTMLCanvasElement {
		return this.getEl('canvas') as HTMLCanvasElement;
	}

}

export var foto: Foto = new Foto();