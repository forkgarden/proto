import { AnggotaObj } from "./AnggotaObj.js";
import { BaseComponent } from "./BaseComponent.js";
import { data } from "./Data.js";
import { util } from "./model/Util.js";

class Renderer {

	private _infoTampil: Function;
	public set infoTampil(value: Function) {
		this._infoTampil = value;
	}

	render(view: View, anggotaObj: AnggotaObj, renderOrtu: boolean = false): void {
		let foto: FotoView = this.renderFoto(view.atas, anggotaObj.foto.url, anggotaObj.info.nama);
		foto.renderOrtu = renderOrtu;
		foto.utama = true;
		foto.viewCont = view;
		foto.anggota = anggotaObj;

		this.handleFoto(foto);
	}

	renderFoto(cont: HTMLDivElement, imgUrl: string, nama: string, sembunyi: boolean = false): FotoView {
		let foto: FotoView = new FotoView();
		foto.img.src = imgUrl;
		foto.namaP.innerHTML = nama;
		foto.attach(cont);

		if (sembunyi) {
			foto.elHtml.classList.add('sembunyi')
		}

		return foto;
	}

	handleFoto(foto: FotoView): void {
		if (foto.renderOrtu) {
			//TODO: render ortu
		}

		foto.elHtml.onclick = () => {

			if (foto.diload && foto.utama) {
				foto.toggle();
			}

			if (foto.utama && !foto.pasanganDiLoad) {
				foto.pasanganDiLoad = true;
				let pasangan: AnggotaObj = util.getPasangan(foto.anggota);

				if (pasangan) {
					let fotoPasangan: FotoView = this.renderFoto(foto.viewCont.atas, pasangan.foto.url, pasangan.info.nama);
					fotoPasangan.pasangan = fotoPasangan;
					fotoPasangan.utama = false;
					fotoPasangan.pasanganDiLoad = true;
					fotoPasangan.anakDiload = true;
					fotoPasangan.anggota = pasangan;
					this.handleFoto(fotoPasangan);
				}
			}

			if (foto.utama && !foto.anakDiload) {
				foto.anakDiload = true;

				let anak: AnggotaObj[] = util.getAnak2(foto.anggota);
				anak.forEach((item: AnggotaObj) => {
					let div: HTMLDivElement = document.createElement('div');
					div.classList.add('cont-anak');

					let view2: View = new View();
					this.render(view2, item);
					view2.attach(div);
					foto.viewCont.bawah.appendChild(div);
				});

				foto.anakCont = foto.viewCont.bawah;
			}
		}

		foto.infoTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			console.log('foto info click');
			console.log(foto);
			data.anggAktif = foto.anggota;
			this._infoTampil();
		}
	}

}

export class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='anggota-cont'>
				<div class='atas'>
				</div>
				<div class='bawah'>
				</div>
			</div>
		`;
		this.build();
	}

	get atas(): HTMLDivElement {
		return this.getEl('div.atas') as HTMLDivElement;
	}

	get bawah(): HTMLDivElement {
		return this.getEl('div.bawah') as HTMLDivElement;
	}

}

class FotoView extends BaseComponent {
	private _pasangan: FotoView;
	private _utama: boolean = true;
	private _anggota: AnggotaObj;
	private _pasanganDiLoad: boolean;
	private _anakDiload: boolean;
	private _anakCont: HTMLDivElement;
	private _viewCont: View;
	private _renderOrtu: boolean = true;

	public get renderOrtu(): boolean {
		return this._renderOrtu;
	}
	public set renderOrtu(value: boolean) {
		this._renderOrtu = value;
	}

	public get viewCont(): View {
		return this._viewCont;
	}
	public set viewCont(value: View) {
		this._viewCont = value;
	}

	constructor() {
		super();
		this._template = `
			<div class='foto'>
				<img src=''>
				<p class='nama'></p>
				<button class='info'>Info</button>
			</div>
		`;
		this.build();
	}

	public get utama(): boolean {
		return this._utama;
	}
	public set utama(value: boolean) {
		this._utama = value;
	}


	toggle(): void {
		if (this.pasangan) {
			this._pasangan._elHtml.classList.toggle('sembunyi');
		}

		if (this._anakCont) {
			this._anakCont.classList.toggle('sembunyi');
		}
	}

	get diload(): boolean {
		return this._pasanganDiLoad && this._anakDiload;
	}

	public get anakDiload(): boolean {
		return this._anakDiload;
	}
	public set anakDiload(value: boolean) {
		this._anakDiload = value;
	}

	public get pasanganDiLoad(): boolean {
		return this._pasanganDiLoad;
	}
	public set pasanganDiLoad(value: boolean) {
		this._pasanganDiLoad = value;
	}


	get img(): HTMLImageElement {
		return this.getEl('img') as HTMLImageElement;
	}

	get namaP(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}

	get infoTbl(): HTMLButtonElement {
		return this.getEl('button.info') as HTMLButtonElement;
	}

	public get pasangan(): FotoView {
		return this._pasangan;
	}
	public set pasangan(value: FotoView) {
		this._pasangan = value;
	}
	public get anakCont(): HTMLDivElement {
		return this._anakCont;
	}
	public set anakCont(value: HTMLDivElement) {
		this._anakCont = value;
	}

	public get anggota(): AnggotaObj {
		return this._anggota;
	}
	public set anggota(value: AnggotaObj) {
		this._anggota = value;
	}


}

export var render: Renderer = new Renderer();