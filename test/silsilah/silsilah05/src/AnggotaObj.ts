export class InfoObj {
	private _id: string = '';
	private _nama: string;
	private _jkl: string;
	private _hapus: boolean;

	public get hapus(): boolean {
		return this._hapus;
	}
	public set hapus(value: boolean) {
		this._hapus = value;
	}

	public get jkl(): string {
		return this._jkl;
	}
	public set jkl(value: string) {
		this._jkl = value;
	}

	public get id(): string {
		return this._id;
	}
	public set id(value: string) {
		this._id = value;
	}

	public get nama(): string {
		return this._nama;
	}
	public set nama(value: string) {
		this._nama = value;
	}
}

export class RelasiObj {
	private _anggota1: AnggotaObj;
	private _anggota2: AnggotaObj;
	private _hapus: boolean;

	public get hapus(): boolean {
		return this._hapus;
	}
	public set hapus(value: boolean) {
		this._hapus = value;
	}

	public get anggota1(): AnggotaObj {
		return this._anggota1;
	}
	public set anggota1(value: AnggotaObj) {
		this._anggota1 = value;
	}
	public get anggota2(): AnggotaObj {
		return this._anggota2;
	}
	public set anggota2(value: AnggotaObj) {
		this._anggota2 = value;
	}
}

export class RelasiAnakObj {
	private _relasi: RelasiObj;
	private _anggota: AnggotaObj;
	private _urutan: number;
	private _hapus: boolean;

	public get hapus(): boolean {
		return this._hapus;
	}
	public set hapus(value: boolean) {
		this._hapus = value;
	}

	public get urutan(): number {
		return this._urutan;
	}
	public set urutan(value: number) {
		this._urutan = value;
	}

	public get relasi(): RelasiObj {
		return this._relasi;
	}
	public set relasi(value: RelasiObj) {
		this._relasi = value;
	}

	public get anggota(): AnggotaObj {
		return this._anggota;
	}
	public set anggota(value: AnggotaObj) {
		this._anggota = value;
	}
}

export class AnggotaObj {
	private _info: InfoObj;
	private _foto: FotoObj;
	private _hapus: boolean;

	public get hapus(): boolean {
		return this._hapus;
	}
	public set hapus(value: boolean) {
		this._hapus = value;
	}

	public get info(): InfoObj {
		return this._info;
	}
	public set info(value: InfoObj) {
		this._info = value;
	}
	public get foto(): FotoObj {
		return this._foto;
	}
	public set foto(value: FotoObj) {
		this._foto = value;
	}
}

export class FotoObj {
	private _url: string;
	private _hapus: boolean;

	public get hapus(): boolean {
		return this._hapus;
	}
	public set hapus(value: boolean) {
		this._hapus = value;
	}

	public get url(): string {
		return this._url;
	}
	public set url(value: string) {
		this._url = value;
	}
}