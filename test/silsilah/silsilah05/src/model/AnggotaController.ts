import { AnggotaObj } from "../AnggotaObj.js";

class Anggota {
	private _list: AnggotaObj[] = [];

	baru(anggotaObj: AnggotaObj): void {
		this._list.push(anggotaObj);
	}

	getAktif(): AnggotaObj[] {
		let hasil: AnggotaObj[] = [];
		this._list.forEach((item: AnggotaObj) => {
			if (!item.hapus) {
				hasil.push(item);
			}
		});
		return hasil;
	}

	hapus(anggotaObj: AnggotaObj): void {
		anggotaObj.hapus = true;
	}

	edit(anggotaObj: AnggotaObj): void {
		anggotaObj;
	}

	// buatAnggota(nama: string = 'nama', jkl: string = 'L'): AnggotaObj {
	// 	let anggotaObj = new AnggotaObj();

	// 	anggotaObj.info = new InfoObj();
	// 	anggotaObj.foto = new FotoObj();

	// 	anggotaObj.info.nama = nama;
	// 	anggotaObj.info.jkl = jkl;

	// 	return anggotaObj
	// }

	public get list(): AnggotaObj[] {
		return this._list;
	}

	public set list(value: AnggotaObj[]) {
		this._list = value;
	}

}

export var anggota: Anggota = new Anggota();