import { FotoObj } from "../AnggotaObj.js";

class Foto {
	private _list: FotoObj[] = [];

	// get list(): FotoObj[] {
	// 	return this._list;
	// }

	baca(): FotoObj[] {
		return this._list;
	}

	edit(fotoObj: FotoObj): void {
		fotoObj;
	}

	baru(fotoObj: FotoObj): void {
		this._list.push(fotoObj);
	}

	hapus(fotoObj: FotoObj): void {
		fotoObj.hapus = true;
	}

}

export var foto: Foto = new Foto();