import { RelasiAnakObj, RelasiObj } from "../AnggotaObj.js";

class RelasiAnak {
	private _list: RelasiAnakObj[] = [];

	baru(rel: RelasiAnakObj): void {
		this.list.push(rel);
	}

	hapus(rel: RelasiAnakObj): void {
		rel.hapus = true;
	}

	get list(): RelasiAnakObj[] {
		return this._list;
	}

	getByRel(rel: RelasiObj): RelasiAnakObj[] {
		let hasil: RelasiAnakObj[] = [];

		this.list.forEach((item: RelasiAnakObj) => {
			if (item.relasi == rel) {
				hasil.push(item);
			}
		})

		return hasil;
	}

}

export var relasiAnak: RelasiAnak = new RelasiAnak();