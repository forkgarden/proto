import { InfoObj } from "../AnggotaObj.js";

class Info {
	private _list: InfoObj[] = [];

	get list(): InfoObj[] {
		return this._list;
	}

	baru(infoObj: InfoObj): void {
		this.list.push(infoObj);
	}

	async edit(infoObj: InfoObj): Promise<void> {
		infoObj;
	}

	hapus(infoObj: InfoObj): void {
		infoObj.hapus = true;
	}
}

export var info: Info = new Info();