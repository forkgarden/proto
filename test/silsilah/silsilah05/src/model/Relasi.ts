import { AnggotaObj, RelasiObj } from "../AnggotaObj.js";

class Relasi {
	private _list: RelasiObj[] = [];

	baru(obj: RelasiObj): void {
		this.list.push(obj);
	}

	edit(obj: RelasiObj): void {
		obj;
	}

	baru2(angg1: AnggotaObj, angg2: AnggotaObj): void {
		let rel: RelasiObj = new RelasiObj();
		rel.anggota1 = angg1;
		rel.anggota2 = angg2;
		this.baru(rel);
	}

	baca(): RelasiObj[] {
		return this._list;
	}

	get list(): RelasiObj[] {
		return this._list;
	}

	hapus(rel: RelasiObj): void {
		rel.hapus = true;
	}

	getRelasiByAnggota(anggota: AnggotaObj): RelasiObj {
		let hasil: RelasiObj;

		this.list.forEach((item: RelasiObj) => {
			if (item.anggota1 == anggota) {
				hasil = item;
			}

			if (item.anggota2 == anggota) {
				hasil = item;
			}
		})

		return hasil;
	}
}

export var relasi: Relasi = new Relasi();