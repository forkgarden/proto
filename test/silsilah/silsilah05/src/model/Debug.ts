import { anggota } from "./AnggotaController.js";
import { AnggotaObj, RelasiObj, RelasiAnakObj } from "../AnggotaObj.js";
import { relasi } from "./Relasi.js";
import { relasiAnak } from "./RelasiAnak.js";
import { util } from "./Util.js";

class Debug {
	buatKeluarga(tag: string, jmlAnak: number = 3): AnggotaObj {
		let suami: AnggotaObj = util.buatAnggota();

		suami.info.nama = 'suami' + tag;
		anggota.baru(suami);

		let istri: AnggotaObj = util.buatAnggota();
		istri.info.nama = 'istri' + tag;
		anggota.baru(istri);

		let rel: RelasiObj = new RelasiObj();
		rel.anggota1 = suami;
		rel.anggota2 = istri;
		relasi.baru(rel);

		for (let i: number = 0; i < jmlAnak; i++) {
			let anak: AnggotaObj = util.buatAnggota();
			anak.info.nama = 'anak' + i + tag;
			anggota.baru(anak);

			let relasiAnakObj: RelasiAnakObj = new RelasiAnakObj();
			relasiAnakObj.relasi = rel;
			relasiAnakObj.anggota = anak;
			relasiAnak.baru(relasiAnakObj);
		}

		return suami;
	}

	debug(): AnggotaObj {
		let anggObj: AnggotaObj = this.buatKeluarga('a', 3);
		let anakObj: AnggotaObj = util.getAnak2(anggObj)[0];
		util.tambahPasangan2(anakObj, "mantu01");
		util.tambahAnak2(anakObj, "cucu1");
		util.tambahAnak2(anakObj, "cucu2");
		util.tambahAnak2(anakObj, "cucu3");
		util.tambahAnak2(anakObj, "cucu4");
		return anggObj;
	}
}

export var debug: Debug = new Debug();