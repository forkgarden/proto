import { AnggotaObj, RelasiObj, RelasiAnakObj, InfoObj, FotoObj } from "../AnggotaObj.js";
// import { anggota } from "./AnggotaController.js";
import { foto } from "./Foto.js";
import { info } from "./Info.js";
import { relasi } from "./Relasi.js";
import { relasiAnak } from "./RelasiAnak.js";

class Util {
	buatInfo(nama: string): InfoObj {
		let infoObj: InfoObj = new InfoObj();
		infoObj.nama = nama;

		info.baru(infoObj);

		return infoObj;
	}

	buatFoto(url: string = 'gbr/kosong.png'): FotoObj {
		let fotoObj: FotoObj = new FotoObj();

		fotoObj.url = url;
		foto.baru(fotoObj);

		return fotoObj;
	}

	buatAnggota(nama: string = 'nama', jkl: string = 'L'): AnggotaObj {
		let anggotaObj = new AnggotaObj();
		let infoObj: InfoObj = this.buatInfo(nama);

		anggotaObj.info = infoObj;
		anggotaObj.foto = this.buatFoto();

		anggotaObj.info.nama = nama;
		anggotaObj.info.jkl = jkl;
		anggotaObj.hapus = false;

		return anggotaObj
	}

	getAnak2(anggotaObj: AnggotaObj): AnggotaObj[] {
		let rel: RelasiObj = relasi.getRelasiByAnggota(anggotaObj);
		let relAnak: RelasiAnakObj[] = relasiAnak.getByRel(rel);
		let hasil: AnggotaObj[] = [];

		relAnak.forEach((item: RelasiAnakObj) => {
			hasil.push(item.anggota);
		});

		return hasil;
	}

	tambahPasangan2(angg: AnggotaObj, nama: string): void {
		let angg2: AnggotaObj = util.buatAnggota(nama);
		this.tambahPasangan(angg, angg2);
	}

	tambahPasangan(angg: AnggotaObj, pasangan: AnggotaObj): void {
		relasi.baru2(angg, pasangan);
	}

	tambahAnak2(angg: AnggotaObj, nama: string): void {
		let anggObj: AnggotaObj = util.buatAnggota(nama);
		this.tambahAnak(angg, anggObj);
	}

	tambahAnak(angg: AnggotaObj, anak: AnggotaObj): void {
		let rel: RelasiObj = relasi.getRelasiByAnggota(angg);
		let rel2: RelasiAnakObj = new RelasiAnakObj();
		rel2.relasi = rel;
		rel2.anggota = anak;
		relasiAnak.baru(rel2);
	}

	getPasangan(anggota: AnggotaObj): AnggotaObj {
		let rel: RelasiObj = relasi.getRelasiByAnggota(anggota);

		if (!rel) return null;

		if (rel.anggota1 != anggota) {
			return rel.anggota1;
		}
		else if (rel.anggota2 != anggota) {
			return rel.anggota2;
		}
		else {
			console.error('error');
			return null;
		}
	}


}

export var util: Util = new Util();