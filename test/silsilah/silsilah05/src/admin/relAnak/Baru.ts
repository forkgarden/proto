import { pilih as pilihAnggota } from "../anggota/Pilih.js";
import { AnggotaObj, RelasiAnakObj, RelasiObj } from "../../AnggotaObj.js";
import { BaseComponent } from "../../BaseComponent.js";
import { data } from "../../Data.js";
import { relasiAnak } from "../../model/RelasiAnak.js";
import { pilih } from "../relasi/Pilih.js";

class Baru {
	private _selesai: Function;
	private anggota: AnggotaObj;
	private relasi: RelasiObj;

	private _view: View = new View();

	constructor() {
		console.log('Baru');
	}

	init(): void {
		this.anggota = null;
		this.relasi = null;

		this.view.kembaliTbl.onclick = () => {
			this._selesai();
		}

		this.view.form.onsubmit = () => {
			console.log('on submit');
			try {
				if (!this.anggota) {
					console.log('anggota 1 belum dipilih');
					return false;
				}

				if (!this.relasi) {
					console.log('relasi belum dipilih');
					return false;
				}

				let rel: RelasiAnakObj = new RelasiAnakObj();
				rel.relasi = this.relasi;
				rel.anggota = this.anggota;
				relasiAnak.baru(rel);
				this._selesai();
			}
			catch (e) {
				console.error(e);
			}
			return false;
		}

		this.view.anggTbl.onclick = () => {
			this.view.detach();
			pilihAnggota.attach(data.cont);
			pilihAnggota.load();
			pilihAnggota.selesai = () => {
				this.anggota = pilihAnggota.anggotaDipilih;
				pilihAnggota.detach();
				this.view.attach(data.cont);
				this.view.anggotaInput.value = this.anggota.info.nama;
			}

		}

		this.view.relTbl.onclick = () => {
			this.view.detach();
			pilih.attach(data.cont);
			pilih.load();
			pilih.selesai = () => {
				this.relasi = pilih.relasiDipilih;
				pilih.detach();
				this.view.attach(data.cont);
				if (this.relasi) {
					this.view.relasiInput.value = this.relasi.anggota1.info.nama + "-" + this.relasi.anggota2.info.nama;
				}
				else {
					this.view.relasiInput.value = "";
				}
			}
		}

	}

	public get view(): View {
		return this._view;
	}


	public set selesai(value: Function) {
		this._selesai = value;
	}
}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='relasi-anak-baru'>
				<form>
					<label>Anggota</label><br/>
					<input type='text' class='anggota'>
					<button class='anggota'>...</button><br/>

					<label>Relasi</label><br/>
					<input type='text' class='relasi'>
					<button type='button' class='relasi'>...</button><br/>

					<button type='submit'>simpan</button>
					<button type='button' class='kembali'>kembali</button>
				</form>
			</div>
		`;
		this.build();
	}

	get anggotaInput(): HTMLInputElement {
		return this.getEl('input.anggota') as HTMLInputElement;
	}

	get relasiInput(): HTMLInputElement {
		return this.getEl('input.relasi') as HTMLInputElement;
	}

	get anggTbl(): HTMLButtonElement {
		return this.getEl('button.anggota') as HTMLButtonElement;
	}

	get relTbl(): HTMLButtonElement {
		return this.getEl('button.relasi') as HTMLButtonElement;
	}

	get form(): HTMLFormElement {
		return this.getEl('form') as HTMLFormElement;
	}

	get kembaliTbl(): HTMLButtonElement {
		return this.getEl('button.kembali') as HTMLButtonElement;
	}


}

export var baru: Baru = new Baru();