import { RelasiAnakObj } from "../../AnggotaObj.js";
import { BaseComponent } from "../../BaseComponent.js";
import { data } from "../../Data.js";
import { relasiAnak } from "../../model/RelasiAnak.js";
// import { relasi } from "../model/Relasi.js";
import { baru } from "./Baru.js";

class Daftar extends BaseComponent {
	private items: Item[] = [];
	private _selesai: Function;

	constructor() {
		super();
		console.log('daftar');
		this._template = `
			<div class='daftar-relasi'>
				<h1>Daftar Relasi</h1>
				<div class='cont'>
				</div>
				<button class='edit'>edit</button>
				<button class='tambah'>tambah</button>
				<button class='hapus'>hapus</button>
				<button class='kembali'>kembali</button>
			</div>
		`;
		this.build();

		this.kembaliTbl.onclick = () => {
			this._selesai();
		}

		this.hapusTbl.onclick = () => {
			relasiAnak.hapus(this.relDipilih);
			this.load();
		}

		this.tambahTbl.onclick = () => {
			this.detach();
			baru.view.attach(data.cont);
			baru.init();
			baru.selesai = () => {
				baru.view.detach();
				this.attach(data.cont);
				this.load();
			}
		}

		this.editTbl.onclick = () => {
			//TODO:
		}

	}

	load(): void {
		this.items = [];
		this.cont.innerHTML = '';
		relasiAnak.list.forEach((item: RelasiAnakObj) => {
			let view: Item = new Item();

			view.nama.innerHTML = "(" + item.relasi.anggota1.info.nama + '-' + item.relasi.anggota2.info.nama + ") - " + item.anggota.info.nama;
			view.attach(this.cont);
			view.rel = item;

			view.elHtml.onclick = () => {
				let el: Element = this.cont.querySelector('div.item.pilih');
				if (el) {
					el.classList.remove('pilih');
				}
				view.elHtml.classList.add('pilih');
			}

			this.items.push(view);
		});

		this.items[0].elHtml.classList.add('pilih');
	}

	get kembaliTbl(): HTMLButtonElement {
		return this.getEl('button.kembali') as HTMLButtonElement;
	}

	get viewDipilih(): Item {
		let hasil: Item;

		this.items.forEach((item: Item) => {
			if (item.elHtml.classList.contains('pilih')) {
				hasil = item;
			}
		});

		return hasil;
	}

	get relDipilih(): RelasiAnakObj {
		let view: Item;

		view = this.viewDipilih;
		return view.rel;
	}

	get cont(): HTMLDivElement {
		return this.getEl('div.cont') as HTMLDivElement;
	}

	get editTbl(): HTMLButtonElement {
		return this.getEl('button.edit') as HTMLButtonElement;
	}

	public get selesai(): Function {
		return this._selesai;
	}
	public set selesai(value: Function) {
		this._selesai = value;
	}

	get tambahTbl(): HTMLButtonElement {
		return this.getEl('button.tambah') as HTMLButtonElement;
	}

	get hapusTbl(): HTMLButtonElement {
		return this.getEl('button.hapus') as HTMLButtonElement;
	}

}

class Item extends BaseComponent {
	private _rel: RelasiAnakObj;

	public get rel(): RelasiAnakObj {
		return this._rel;
	}
	public set rel(value: RelasiAnakObj) {
		this._rel = value;
	}

	constructor() {
		super();
		this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
		this.build();
	}

	get nama(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}
}

export var daftar: Daftar = new Daftar();