import { InfoObj } from "../../AnggotaObj.js";
import { BaseComponent } from "../../BaseComponent.js";
import { data } from "../../Data.js";
import { baru } from "./Baru.js";
// import { edit } from "./Edit.js";
import { info } from "../../model/Info.js";

class Daftar extends BaseComponent {
	private items: Item[] = [];
	private _selesai: Function;

	constructor() {
		super();
		this._template = `
			<div class='daftar-info'>
				<h1>Daftar Info</h1>
				<div class='cont'>
				</div>
				<button class='edit'>edit</button>
				<button class='tambah'>tambah</button>
				<button class='hapus'>hapus</button>
				<button class='kembali'>kembali</button>
			</div>
		`;
		this.build();

		this.kembaliTbl.onclick = () => {
			this._selesai();
		}

		this.hapusTbl.onclick = () => {
			info.hapus(this.infoDipilih);
			this.load();
		}

		this.tambahTbl.onclick = () => {
			this.detach();
			baru.attach(data.cont);
			baru.modeBaru = true;

			baru.finish = () => {
				baru.detach();
				this.attach(data.cont);
				this.load();
			}
		}

		this.editTbl.onclick = () => {
			this.detach();

			baru.modeBaru = false;
			baru.infoAwal = this.infoDipilih;
			baru.attach(data.cont);
			baru.finish = () => {
				baru.detach();
				this.attach(data.cont);
				this.load();
			}

		}

	}

	load(): void {
		this.items = [];
		this.cont.innerHTML = '';
		info.list.forEach((item: InfoObj) => {
			let view: Item = new Item();

			view.nama.innerHTML = item.nama;
			view.attach(this.cont);
			view.info = item;

			view.elHtml.onclick = () => {
				let el: Element = this.cont.querySelector('div.item.pilih');
				if (el) {
					el.classList.remove('pilih');
				}
				view.elHtml.classList.add('pilih');
			}

			this.items.push(view);
		});

		this.items[0].elHtml.classList.add('pilih');
	}

	get kembaliTbl(): HTMLButtonElement {
		return this.getEl('button.kembali') as HTMLButtonElement;
	}

	get viewDipilih(): Item {
		let hasil: Item;

		this.items.forEach((item: Item) => {
			if (item.elHtml.classList.contains('pilih')) {
				hasil = item;
			}
		});

		return hasil;
	}

	get infoDipilih(): InfoObj {
		// let hasil: AnggotaObj;
		let view: Item;

		view = this.viewDipilih;
		return view.info;
	}

	get cont(): HTMLDivElement {
		return this.getEl('div.cont') as HTMLDivElement;
	}

	get editTbl(): HTMLButtonElement {
		return this.getEl('button.edit') as HTMLButtonElement;
	}

	public get selesai(): Function {
		return this._selesai;
	}
	public set selesai(value: Function) {
		this._selesai = value;
	}

	get tambahTbl(): HTMLButtonElement {
		return this.getEl('button.tambah') as HTMLButtonElement;
	}

	get hapusTbl(): HTMLButtonElement {
		return this.getEl('button.hapus') as HTMLButtonElement;
	}

}

class Item extends BaseComponent {
	private _info: InfoObj;
	public get info(): InfoObj {
		return this._info;
	}
	public set info(value: InfoObj) {
		this._info = value;
	}

	constructor() {
		super();
		this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
		this.build();
	}

	get nama(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}
}

export var daftar: Daftar = new Daftar();