// import { anggota } from "../model/AnggotaController.js";
import { InfoObj } from "../../AnggotaObj.js";
import { BaseComponent } from "../../BaseComponent.js";
// import { util } from "../model/Util.js";
import { info } from "../../model/Info.js";

class Baru extends BaseComponent {
	private _finish: Function;
	private _infoAwal: InfoObj;
	private _modeBaru: boolean = true;

	constructor() {
		super();
		this._template = `
			<div class='anggota-edit'>
				<form>
					<label>Nama:</label><br/>
					<input type="text" class='nama'>
					<input type='submit' class='simpan' value='simpan'>
				</form>
			</div>
		`;
		this.build();
		this.form.onsubmit = () => {
			try {
				if (this._modeBaru) {
					let infoObj: InfoObj = new InfoObj();
					infoObj.nama = this.namaInput.value;
					info.baru(infoObj);
				}
				else {
					this._infoAwal.nama = this.namaInput.value;
					info.edit(this._infoAwal).then(() => {
						this._finish();
					}).catch(() => {
						//TODO:
					});
				}
			}
			catch (e) {
				console.error(e);
			}
			return false;
		}
	}

	get namaInput(): HTMLInputElement {
		return this.getEl('input.nama') as HTMLInputElement;
	}

	get form(): HTMLFormElement {
		return this.getEl('form') as HTMLFormElement;
	}

	public set finish(value: Function) {
		this._finish = value;
	}
	public get modeBaru(): boolean {
		return this._modeBaru;
	}
	public set modeBaru(value: boolean) {
		this._modeBaru = value;
	}
	public get infoAwal(): InfoObj {
		return this._infoAwal;
	}
	public set infoAwal(value: InfoObj) {
		this._infoAwal = value;
	}


}

export var baru: Baru = new Baru();