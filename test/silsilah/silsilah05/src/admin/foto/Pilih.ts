import { FotoObj } from "../../AnggotaObj.js";
import { BaseComponent } from "../../BaseComponent.js";
import { foto } from "../../model/Foto.js";

class Pilih extends BaseComponent {
	private items: Item[] = [];
	private _selesai: Function;

	constructor() {
		super();
		this._template = `
			<div class='daftar-foto'>
				<h1>Pilih Foto</h1>
				<div class='cont'>
				</div>
				<button class='kembali'>pilih</button>
			</div>
		`;
		this.build();

		this.kembaliTbl.onclick = () => {
			this._selesai();
		}

	}

	load(): void {
		this.items = [];
		this.cont.innerHTML = '';
		foto.baca().forEach((item: FotoObj) => {
			let view: Item = new Item();

			view.nama.innerHTML = item.url;
			view.attach(this.cont);
			view.foto = item;

			view.elHtml.onclick = () => {
				let el: Element = this.cont.querySelector('div.item.pilih');
				if (el) {
					el.classList.remove('pilih');
				}
				view.elHtml.classList.add('pilih');
			}

			this.items.push(view);
		});

		this.items[0].elHtml.classList.add('pilih');
	}

	get kembaliTbl(): HTMLButtonElement {
		return this.getEl('button.kembali') as HTMLButtonElement;
	}

	get itemDipilih(): Item {
		let hasil: Item;

		this.items.forEach((item: Item) => {
			if (item.elHtml.classList.contains('pilih')) {
				hasil = item;
			}
		});

		return hasil;
	}

	get fotoDipilih(): FotoObj {
		let view: Item;

		view = this.itemDipilih;
		return view.foto;
	}

	get cont(): HTMLDivElement {
		return this.getEl('div.cont') as HTMLDivElement;
	}

	public set selesai(value: Function) {
		this._selesai = value;
	}
}

class Item extends BaseComponent {
	private _foto: FotoObj;

	public get foto(): FotoObj {
		return this._foto;
	}
	public set foto(value: FotoObj) {
		this._foto = value;
	}

	constructor() {
		super();
		this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
		this.build();
	}

	get nama(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}
}

export var pilih: Pilih = new Pilih();