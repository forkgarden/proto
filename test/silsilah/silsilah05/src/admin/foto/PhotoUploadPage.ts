import { BaseComponent } from "../../BaseComponent.js";

declare var loadImage: Function;


/*

TODO: 
thumbnail kanvas dipindah ke halapan anggota baru


*/
class PhotoUploadPage extends BaseComponent {

	private _selesai: Function = null;
	private _insertedId: string = '';
	private _gbrUrl: string = '';

	constructor() {
		super();
		this._template = `
		<div class='foto-page'>
			<h3>Upload Foto:</h3>
			<form>
				<input type="file" accept="">
				<br />
				<p>Foto:</p>
				<div class='foto-cont'>

				</div>
				<p>Thumbnail:</p>
				<div class='thumb-cont'>

				</div>
				<br />

				<input type='submit' class='btn btn-primary upload' value='upload'>
				<button type="button" class='btn btn-secondary tutup'>tutup</button>
			</form>
		</div>
		`;
		this.build();
		this.init();
	}

	createName(prefix: string, pjg: number = 12): string {
		let hasil: string = prefix;
		let karakter: string = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
		let date: Date = new Date();

		for (let i: number = 0; i < pjg; i++) {
			hasil += karakter.charAt(Math.floor(Math.random() * karakter.length));
		}

		hasil += date.getFullYear() + '_' + date.getMonth() + '_' + date.getDate() + '_' + date.getHours() + '_' + date.getMinutes() + '_' + date.getSeconds() + '_' + date.getMilliseconds();
		hasil += '.png';

		console.log('nama: ' + hasil);

		return hasil;
	}

	init(): void {

		this.initInput(this.input);

		this.form.onsubmit = () => {
			this.upload();
			this._selesai();
			return false;
		}

		this.tutupTbl.onclick = () => {
			this._selesai();
		}

		console.groupEnd();
	}

	async loadImage3(file: HTMLInputElement): Promise<void> {
		await this.loadImage2(file, 800, 800, "gbr_besar", this.fotoCont);
		await this.loadImage2(file, 128, 128, "thumb", this.thumbCont);
	}

	async loadImage2(file: HTMLInputElement, panjang: number, lebar: number, id: string, cont: HTMLDivElement): Promise<void> {
		let canvas: HTMLCanvasElement;
		let img: any = await loadImage(
			file.files[0],
			{
				maxWidth: panjang,
				maxHeight: lebar,
				canvas: true,
				orientation: true,
				imageSmoothingQuality: 'high'
			}
		);

		canvas = img.image;
		canvas.setAttribute("id", id);
		cont.appendChild(canvas);
	}

	populateData(): FormData {
		let formData: FormData = new FormData();
		formData.append("gbr_besar", this.canvasBesar.toDataURL());
		formData.append("gbr_kecil", this.canvasThumb.toDataURL());
		return formData;
	}

	populateJson(): string {
		let obj: any = {
			gbr_besar: this.canvasBesar.toDataURL(),
			gbr_kecil: this.canvasThumb.toDataURL(),
			gbr_besar_nama: this.createName('gbr_besar_', 8),
			gbr_kecil_nama: this.createName('gbr_kecil_', 8)
		}

		return JSON.stringify(obj);
	}

	initInput(input: HTMLInputElement): void {

		input.onchange = () => {
			this.loadImage3(input).then(() => {

			}).catch((e: any) => {
				console.error(e);
			});

		}
	}

	//TODO:
	async upload(): Promise<string> {
		return "";
	}

	get listCont(): HTMLDivElement {
		return this.getEl('div.list-cont') as HTMLDivElement;
	}

	get form(): HTMLFormElement {
		return this.getEl('form') as HTMLFormElement;
	}

	get input(): HTMLInputElement {
		return this.getEl('input') as HTMLInputElement;
	}

	get uploadTbl(): HTMLInputElement {
		return this.getEl('input.upload') as HTMLInputElement;
	}

	get canvasBesar(): HTMLCanvasElement {
		return this.getEl('canvas#gbr_besar') as HTMLCanvasElement;
	}

	get canvasThumb(): HTMLCanvasElement {
		return this.getEl('canvas#thumb') as HTMLCanvasElement;
	}

	get tutupTbl(): HTMLButtonElement {
		return this.getEl('button.tutup') as HTMLButtonElement;
	}

	public get selesai(): Function {
		return this._selesai;
	}

	public set selesai(value: Function) {
		this._selesai = value;
	}

	public get insertedId(): string {
		return this._insertedId;
	}

	public get gbrUrl(): string {
		return this._gbrUrl;
	}

	get fotoCont(): HTMLDivElement {
		return this.getEl('div.foto-cont') as HTMLDivElement;
	}

	get thumbCont(): HTMLDivElement {
		return this.getEl('div.thumb-cont') as HTMLDivElement;
	}

}

export var fotoUpload: PhotoUploadPage = new PhotoUploadPage();