import { daftar as daftarAnggota } from "./anggota/Daftar.js";
import { BaseComponent } from "../BaseComponent.js";
import { data } from "../Data.js";
import { daftar as daftarRelasi } from "./relasi/Daftar.js";
import { daftar as daftarInfo } from "./info/Daftar.js";
import { daftar as daftarFoto } from "./foto/Daftar.js";
import { daftar as daftarRelAnak } from "./relAnak/Daftar.js";

class Admin {
	private _view: View = new View();
	private _selesai: Function;

	constructor() {
		console.log('Admin');

		this.view.anggotaTbl.onclick = () => {
			this.view.detach();
			daftarAnggota.attach(data.cont);
			daftarAnggota.load();
			daftarAnggota.selesai = () => {
				daftarAnggota.detach();
				this.view.attach(data.cont);
			}
		}

		this.view.relasiTbl.onclick = () => {
			this._view.detach();
			daftarRelasi.attach(data.cont);
			daftarRelasi.load();
			daftarRelasi.selesai = () => {
				daftarRelasi.detach();
				this.view.attach(data.cont);
			}
		}

		this.view.relasiAnakTbl.onclick = () => {
			this.view.detach();
			daftarRelAnak.attach(data.cont);
			daftarRelAnak.load();
			daftarRelAnak.selesai = () => {
				daftarRelAnak.detach();
				this.view.attach(data.cont);
			}
		}

		this.view.kembaliTbl.onclick = () => {
			this._selesai();
		}

		this.view.infoTbl.onclick = () => {
			this.view.detach();
			daftarInfo.attach(data.cont);
			daftarInfo.load();
			daftarInfo.selesai = () => {
				daftarInfo.detach();
				this.view.attach(data.cont);
			}
		}

		this.view.fotoTbl.onclick = () => {
			daftarFoto.attach(data.cont);
			daftarFoto.load();
			this.view.detach();

			daftarFoto.selesai = () => {
				daftarFoto.detach();
				this.view.attach(data.cont);
			}
		}

	}

	public get selesai(): Function {
		return this._selesai;
	}
	public set selesai(value: Function) {
		this._selesai = value;
	}
	public get view(): View {
		return this._view;
	}

}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='admin'>
				<button class='anggota'>Anggota</button>
				<button class='relasi'>Relasi</button>
				<button class='relasi-anak'>Relasi Anak</button>
				<button class='foto'>Foto</button>
				<button class='info'>Info</button>
				<button class='kembali'>Kembali</button>
			</div>
		`;
		this.build();
	}

	get anggotaTbl(): HTMLButtonElement {
		return this.getEl('button.anggota') as HTMLButtonElement;
	}

	get infoTbl(): HTMLButtonElement {
		return this.getEl('button.info') as HTMLButtonElement;
	}

	get fotoTbl(): HTMLButtonElement {
		return this.getEl('button.foto') as HTMLButtonElement;
	}

	get relasiTbl(): HTMLButtonElement {
		return this.getEl('button.relasi') as HTMLButtonElement;
	}

	get relasiAnakTbl(): HTMLButtonElement {
		return this.getEl('button.relasi-anak') as HTMLButtonElement;
	}

	get kembaliTbl(): HTMLButtonElement {
		return this.getEl('button.kembali') as HTMLButtonElement;
	}

}

export var admin: Admin = new Admin();