import { anggota } from "../../model/AnggotaController.js";
import { AnggotaObj } from "../../AnggotaObj.js";
import { BaseComponent } from "../../BaseComponent.js";

class Pilih extends BaseComponent {
	private items: Item[] = [];
	private _selesai: Function;
	private ok: boolean = true;


	constructor() {
		super();
		this._template = `
			<div class='daftar-anggota'>
				<h1>Pilih Anggota</h1>
				<div class='cont'>
				</div>
				<button class='pilih'>pilih</button>
				<button class='kembali'>kembali</button>
			</div>
		`;
		this.build();

		this.pilihTbl.onclick = () => {
			this.ok = true;
			this._selesai();
		}

		this.kembaliTbl.onclick = () => {
			this.ok = false;
			this._selesai();
		}

	}

	load(): void {
		this.items = [];
		this.cont.innerHTML = '';
		anggota.list.forEach((item: AnggotaObj) => {
			let view: Item = new Item();

			view.nama.innerHTML = item.info.nama;
			view.attach(this.cont);
			view.anggota = item;

			view.elHtml.onclick = () => {
				let el: Element = this.cont.querySelector('div.item.pilih');
				if (el) {
					el.classList.remove('pilih');
				}
				view.elHtml.classList.add('pilih');
			}

			this.items.push(view);
		});

		this.items[0].elHtml.classList.add('pilih');
	}

	get pilihTbl(): HTMLButtonElement {
		return this.getEl('button.pilih') as HTMLButtonElement;
	}

	get kembaliTbl(): HTMLButtonElement {
		return this.getEl('button.kembali') as HTMLButtonElement;
	}

	get itemAktif(): Item {
		let hasil: Item;

		this.items.forEach((item: Item) => {
			if (item.elHtml.classList.contains('pilih')) {
				hasil = item;
			}
		});

		return hasil;
	}

	get anggotaDipilih(): AnggotaObj {
		let view: Item;

		if (!this.ok) {
			return null;
		}

		view = this.itemAktif;
		return view.anggota;
	}

	get cont(): HTMLDivElement {
		return this.getEl('div.cont') as HTMLDivElement;
	}

	public set selesai(value: Function) {
		this._selesai = value;
	}
}

class Item extends BaseComponent {
	private _anggota: AnggotaObj;

	public get anggota(): AnggotaObj {
		return this._anggota;
	}
	public set anggota(value: AnggotaObj) {
		this._anggota = value;
	}

	constructor() {
		super();
		this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
		this.build();
	}

	get nama(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}
}

export var pilih: Pilih = new Pilih();