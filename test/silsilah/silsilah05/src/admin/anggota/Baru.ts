import { anggota } from "../../model/AnggotaController.js";
import { AnggotaObj, FotoObj, InfoObj } from "../../AnggotaObj.js";
import { BaseComponent } from "../../BaseComponent.js";
import { pilih } from "../info/Pilih.js";
// import { pilih as pilihFoto } from "../foto/Pilih.js";
import { data } from "../../Data.js";
import { fotoUpload } from "../foto/PhotoUploadPage.js";

class Baru {
	private _selesai: Function;
	private infoObj: InfoObj;
	private fotoObj: FotoObj;
	private _modeBaru: boolean;
	private _dataAwal: AnggotaObj;
	private _view: View = new View();

	constructor() {
		this.view.form.onsubmit = () => {
			try {
				if (this._modeBaru) {
					let anggotaObj = new AnggotaObj();
					anggotaObj.info = this.infoObj;
					anggotaObj.foto = this.fotoObj;
					anggota.baru(anggotaObj);
					this._selesai();
				}
				else {
					this.dataAwal.info = this.infoObj;
					this.dataAwal.foto = this.fotoObj;
					anggota.edit(this.dataAwal);
					this._selesai();
				}
			}
			catch (e) {
				console.error(e);
			}
			return false;
		}

		this.view.infoBtn.onclick = () => {
			this.view.detach();

			pilih.attach(data.cont);
			pilih.load();
			pilih.selesai = () => {
				pilih.detach();
				this.view.attach(data.cont);
				this.infoObj = pilih.infoDipilih;
				this.view.infoInput.value = this.infoObj.nama;
			}
		}

		// this.view.fotoBtn.onclick = () => {
		// 	this.view.detach();
		// 	pilihFoto.attach(data.cont);
		// 	pilihFoto.load();

		// 	pilihFoto.selesai = () => {
		// 		this.fotoObj = pilihFoto.fotoDipilih;
		// 		pilihFoto.detach();
		// 		this.view.attach(data.cont);
		// 		this.view.fotoInput.value = this.fotoObj.url;
		// 	}
		// }

		this.view.fotoBtn.onclick = () => {
			this.view.detach();
			fotoUpload.attach(data.cont);

			fotoUpload.selesai = () => {
				fotoUpload.detach();
				this.view.attach(data.cont);
				//TODO:
				//this.view.gbrCont.appendChild(fotoUpload.)
				// this.view.fotoInput.value = this.fotoObj.url;
			}
		}

		this.view.kembaliBtn.onclick = () => {
			this._selesai();
		}
	}

	public get dataAwal(): AnggotaObj {
		return this._dataAwal;
	}
	public set dataAwal(value: AnggotaObj) {
		this._dataAwal = value;
	}

	public set selesai(value: Function) {
		this._selesai = value;
	}

	public get view(): View {
		return this._view;
	}

	public get modeBaru(): boolean {
		return this._modeBaru;
	}
	public set modeBaru(value: boolean) {
		this._modeBaru = value;
	}

}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='info-edit'>
				<h1>edit Anggota</h1>
				<form>
					<label>Info:</label><br/>
					<input type="text" class='info'><button class='info' type='button'>...</button><br/>
					<label>Foto:</label><br/>
					<input type="text" class='foto'><button class='foto' type='button'>...</button><br/>
					<div class='img-cont'>
					
					</div>
					<input type='submit' class='simpan' value='simpan'>
					<button type='button' class='kembali' value='kembali'>kembali</button>
				</form>
			</div>
		`;
		this.build();
	}

	get gbrCont(): HTMLDivElement {
		return this.getEl('div.img-cont') as HTMLDivElement;
	}

	get kembaliBtn(): HTMLButtonElement {
		return this.getEl('button.kembali') as HTMLButtonElement;
	}

	get infoBtn(): HTMLButtonElement {
		return this.getEl('button.info') as HTMLButtonElement;
	}

	get fotoBtn(): HTMLButtonElement {
		return this.getEl('button.foto') as HTMLButtonElement;
	}

	get infoInput(): HTMLInputElement {
		return this.getEl('input.info') as HTMLInputElement;
	}

	get fotoInput(): HTMLInputElement {
		return this.getEl('input.foto') as HTMLInputElement;
	}

	get form(): HTMLFormElement {
		return this.getEl('form') as HTMLFormElement;
	}

}

export var baru: Baru = new Baru();