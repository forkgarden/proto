import { anggota } from "../../model/AnggotaController.js";
import { AnggotaObj } from "../../AnggotaObj.js";
import { BaseComponent } from "../../BaseComponent.js";
import { data } from "../../Data.js";
import { baru } from "./Baru.js";
// import { edit } from "./Edit.js";

class Daftar extends BaseComponent {
	private items: Item[] = [];
	private _selesai: Function;

	constructor() {
		super();
		console.log('Daftar anggota');
		this._template = `
			<div class='daftar-anggota'>
				<h1>Daftar Anggota</h1>
				<div class='cont'>
				</div>
				<button class='edit'>edit</button>
				<button class='tambah'>tambah</button>
				<button class='hapus'>hapus</button>
				<button class='kembali'>kembali</button>
			</div>
		`;
		this.build();

		this.kembaliTbl.onclick = () => {
			this._selesai();
		}

		this.hapusTbl.onclick = () => {
			anggota.hapus(this.anggotaDipilih);
			this.load();
		}

		this.tambahTbl.onclick = () => {
			baru.view.attach(data.cont);
			baru.modeBaru = true;
			this.detach();

			baru.selesai = () => {
				baru.view.detach();
				this.attach(data.cont);
				this.load();
			}
		}

		this.editTbl.onclick = () => {
			baru.view.attach(data.cont);
			baru.modeBaru = false;
			baru.dataAwal = this.anggotaDipilih;
			this.detach();

			baru.selesai = () => {
				baru.view.detach();
				this.attach(data.cont);
				this.load();
			}
		}

	}

	//TODO: loading
	load(): void {
		this.items = [];
		this.cont.innerHTML = '';
		anggota.getAktif().forEach((item: AnggotaObj) => {
			let view: Item = new Item();

			view.nama.innerHTML = item.info.nama + "/" + item.foto.url;
			view.attach(this.cont);
			view.anggota = item;

			view.elHtml.onclick = () => {
				let el: Element = this.cont.querySelector('div.item.pilih');
				if (el) {
					el.classList.remove('pilih');
				}
				view.elHtml.classList.add('pilih');
			}

			this.items.push(view);
		});

		this.items[0].elHtml.classList.add('pilih');
	}

	get kembaliTbl(): HTMLButtonElement {
		return this.getEl('button.kembali') as HTMLButtonElement;
	}

	get viewDipilih(): Item {
		let hasil: Item;

		this.items.forEach((item: Item) => {
			if (item.elHtml.classList.contains('pilih')) {
				hasil = item;
			}
		});

		return hasil;
	}

	get anggotaDipilih(): AnggotaObj {
		// let hasil: AnggotaObj;
		let view: Item;

		view = this.viewDipilih;
		return view.anggota;
	}

	get cont(): HTMLDivElement {
		return this.getEl('div.cont') as HTMLDivElement;
	}

	get editTbl(): HTMLButtonElement {
		return this.getEl('button.edit') as HTMLButtonElement;
	}

	public get selesai(): Function {
		return this._selesai;
	}
	public set selesai(value: Function) {
		this._selesai = value;
	}

	get tambahTbl(): HTMLButtonElement {
		return this.getEl('button.tambah') as HTMLButtonElement;
	}

	get hapusTbl(): HTMLButtonElement {
		return this.getEl('button.hapus') as HTMLButtonElement;
	}

}

class Item extends BaseComponent {
	private _anggota: AnggotaObj;

	public get anggota(): AnggotaObj {
		return this._anggota;
	}
	public set anggota(value: AnggotaObj) {
		this._anggota = value;
	}

	constructor() {
		super();
		this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
		this.build();
	}

	get nama(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}
}

export var daftar: Daftar = new Daftar();