import { pilih } from "../anggota/Pilih.js";
import { AnggotaObj, RelasiObj } from "../../AnggotaObj.js";
import { BaseComponent } from "../../BaseComponent.js";
import { data } from "../../Data.js";
import { relasi } from "../../model/Relasi.js";

class BaruEdit {
	private _selesai: Function;
	private anggota1: AnggotaObj;
	private anggota2: AnggotaObj;
	private _modeBaru: boolean = true;
	private _rel: RelasiObj;
	private _view: View = new View();

	constructor() {
		console.log('Baru');
	}

	init(): void {
		this.anggota1 = null;
		this.anggota2 = null;

		this.view.kembaliTbl.onclick = () => {
			this._selesai();
		}

		this.view.form.onsubmit = () => {
			console.log('on submit');
			try {
				if (!this.anggota1) {
					console.log('anggota 1 belum dipilih');
					return false;
				}

				if (!this.anggota2) {
					console.log('anggota 2 belum dipilih');
					return false;
				}

				if (this.modeBaru) {
					let rel: RelasiObj = new RelasiObj();
					rel.anggota1 = this.anggota1;
					rel.anggota2 = this.anggota2;
					relasi.baru(rel);
					this._selesai();
				}
				else {
					this._rel.anggota1 = this.anggota1;
					this._rel.anggota2 = this.anggota2;
					this._selesai();
				}

			}
			catch (e) {
				console.error(e);
			}
			return false;
		}

		this.view.angg1Tbl.onclick = () => {
			this.view.detach();
			pilih.attach(data.cont);
			pilih.load();
			pilih.selesai = () => {
				this.anggota1 = pilih.anggotaDipilih;
				pilih.detach();
				this.view.attach(data.cont);
				this.view.anggota1Input.value = this.anggota1.info.nama;
			}

		}

		this.view.angg2Tbl.onclick = () => {
			this.view.detach();
			pilih.attach(data.cont);
			pilih.load();
			pilih.selesai = () => {
				this.anggota2 = pilih.anggotaDipilih;
				pilih.detach();
				this.view.attach(data.cont);
				if (this.anggota2) {
					this.view.anggota2Input.value = this.anggota2.info.nama;
				}
				else {
					this.view.anggota2Input.value = '';
				}
			}

		}


	}

	public get view(): View {
		return this._view;
	}

	public get rel(): RelasiObj {
		return this._rel;
	}
	public set rel(value: RelasiObj) {
		this._rel = value;
	}

	public set selesai(value: Function) {
		this._selesai = value;
	}

	public get modeBaru(): boolean {
		return this._modeBaru;
	}
	public set modeBaru(value: boolean) {
		this._modeBaru = value;
	}

}

class View extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='relasi-baru'>
				<form>
					<label>Anggota 1</label><br/>
					<input type='text' class='anggota1'>
					<button class='anggota1'>...</button><br/>

					<label>Anggota 2</label><br/>
					<input type='text' class='anggota2'>
					<button type='button' class='anggota2'>...</button><br/>

					<button type='submit'>simpan</button>
					<button type='button' class='kembali'>kembali</button>
				</form>
			</div>
		`;
		this.build();
	}

	get anggota1Input(): HTMLInputElement {
		return this.getEl('input.anggota1') as HTMLInputElement;
	}

	get anggota2Input(): HTMLInputElement {
		return this.getEl('input.anggota2') as HTMLInputElement;
	}

	get angg1Tbl(): HTMLButtonElement {
		return this.getEl('button.anggota1') as HTMLButtonElement;
	}

	get angg2Tbl(): HTMLButtonElement {
		return this.getEl('button.anggota2') as HTMLButtonElement;
	}

	get form(): HTMLFormElement {
		return this.getEl('form') as HTMLFormElement;
	}

	get kembaliTbl(): HTMLButtonElement {
		return this.getEl('button.kembali') as HTMLButtonElement;
	}


}

export var baruEdit: BaruEdit = new BaruEdit();