// import { anggota } from "../model/AnggotaController.js";
// import { AnggotaObj } from "../AnggotaObj.js";
import { RelasiObj } from "../../AnggotaObj.js";
import { BaseComponent } from "../../BaseComponent.js";
import { relasi } from "../../model/Relasi.js";

class Pilih extends BaseComponent {
	private items: Item[] = [];
	private _selesai: Function;
	private ok: boolean = true;

	constructor() {
		super();
		this._template = `
			<div class='daftar-relasi'>
				<h1>Pilih Relasi</h1>
				<div class='cont'>
				</div>
				<button class='pilih'>pilih</button>
				<button class='kembali'>kembali</button>
			</div>
		`;
		this.build();

		this.kembaliTbl.onclick = () => {
			this.ok = false;
			this._selesai();
		}

		this.pilihTbl.onclick = () => {
			this.ok = true;
			this._selesai();
		}

	}

	load(): void {
		this.items = [];
		this.cont.innerHTML = '';
		relasi.list.forEach((item: RelasiObj) => {
			let view: Item = new Item();

			view.nama.innerHTML = item.anggota1.info.nama + "-" + item.anggota2.info.nama;
			view.attach(this.cont);
			view.relasi = item;

			view.elHtml.onclick = () => {
				let el: Element = this.cont.querySelector('div.item.pilih');
				if (el) {
					el.classList.remove('pilih');
				}
				view.elHtml.classList.add('pilih');
			}

			this.items.push(view);
		});

		this.items[0].elHtml.classList.add('pilih');
	}

	get kembaliTbl(): HTMLButtonElement {
		return this.getEl('button.kembali') as HTMLButtonElement;
	}

	get pilihTbl(): HTMLButtonElement {
		return this.getEl('button.pilih') as HTMLButtonElement;
	}

	get itemViewDipilih(): Item {
		let hasil: Item;

		this.items.forEach((item: Item) => {
			if (item.elHtml.classList.contains('pilih')) {
				hasil = item;
			}
		});

		return hasil;
	}

	get relasiDipilih(): RelasiObj {
		let view: Item;

		if (!this.ok) {
			return null;
		}

		view = this.itemViewDipilih;
		return view.relasi;
	}

	get cont(): HTMLDivElement {
		return this.getEl('div.cont') as HTMLDivElement;
	}

	public set selesai(value: Function) {
		this._selesai = value;
	}
}

class Item extends BaseComponent {
	private _relasi: RelasiObj;

	public get relasi(): RelasiObj {
		return this._relasi;
	}
	public set relasi(value: RelasiObj) {
		this._relasi = value;
	}

	constructor() {
		super();
		this._template = `
			<div class='item'>
				<p class='nama'></p>
			</div>
		`;
		this.build();
	}

	get nama(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}
}

export var pilih: Pilih = new Pilih();