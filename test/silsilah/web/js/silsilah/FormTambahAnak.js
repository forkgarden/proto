import { BaseComponent } from "./ha/BaseComponent.js";
export class FormTambahAnak extends BaseComponent {
    constructor() {
        super();
    }
    get anggota() {
        return this._anggota;
    }
    get onSave() {
        return this._onSave;
    }
    set onSave(value) {
        this._onSave = value;
    }
}
