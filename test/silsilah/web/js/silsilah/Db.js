export class Db {
    constructor() {
        this._list = [];
    }
    idCreate() {
        let id = this._list.length + 10000;
        while (!this.validate(id)) {
            id++;
        }
        return id;
    }
    insert(anggota) {
        anggota.id = this.idCreate();
        this._list.push();
    }
    getById(id) {
        for (let i = 0; i < this._list.length; i++) {
            if (this._list[i].id == id)
                return this._list[i];
        }
        return null;
    }
    validate(idx) {
        for (let i = 0; i < this._list.length; i++) {
            if (this._list[i].id == idx)
                return false;
        }
        return true;
    }
    get list() {
        return this._list;
    }
    set list(value) {
        this._list = value;
    }
}
