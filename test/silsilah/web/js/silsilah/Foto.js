import { BaseComponent } from "./ha/BaseComponent.js";
import { Anggota } from "./Anggota.js";
import { Silsilah } from "./Silsilah.js";
export class Foto extends BaseComponent {
    constructor() {
        super();
        this._anggota = null;
        this._cont = null;
        this._contAnak = null;
        this.db = null;
        this.fotoUtama = null;
        this.fotoPasangan = null;
        this._template = `
			<div class='foto'>
				<div class='cont'>

				</div>
				<div class='anak'>
			
				</div>
			</div>
		`;
        this.build();
        this._cont = this.getEl('cont');
        this.fotoPasangan = new FotoPasangan();
        this.fotoUtama = new FotoUtama();
    }
    init() {
        this.db = Silsilah.inst.db;
        this.fotoUtama.init();
        this.fotoPasangan.init();
        this.fotoUtama.attach(this.cont);
        this.fotoPasangan.attach(this._cont);
        this.fotoUtama.imgEl.src = this._anggota.url;
        this.fotoUtama.tambahPasanganTbl.onclick = this.tambahAnak.bind(this);
    }
    tambahAnak() {
        let anak;
        anak = new Anggota();
        anak.orangTuaId = this._anggota.id;
        anak.view = new Foto();
        anak.view.anggota = anak;
        anak.view.init();
        anak.view.attach(this._contAnak);
        this.db.insert(anak);
    }
    edit() {
    }
    hapus() {
    }
    tambahPasangan() {
    }
    get anggota() {
        return this._anggota;
    }
    set anggota(value) {
        this._anggota = value;
    }
    get cont() {
        return this._cont;
    }
    set cont(value) {
        this._cont = value;
    }
}
export class FotoUtama extends BaseComponent {
    constructor() {
        super();
        this._url = '';
        this._imgEl = null;
        this._tambahPasanganTbl = null;
        this._template = `
			<div class='foto-utama'>
				<img src='' />
				<div class='tombol'>
					<button class='pasangan'>Tambah Pasangan</button>
					<button class='edit'>Edit</button>
					<button class='hapus'>Hapus</button>
					<button class='Tambah Anak'>Tambah Anak</button>
				</div>
			</div>
		`;
        this.build();
        this._imgEl = this.getEl('img');
        this._tambahPasanganTbl = this.getEl('button.pasangan');
    }
    init() {
    }
    get url() {
        return this._url;
    }
    set url(value) {
        this._url = value;
    }
    get imgEl() {
        return this._imgEl;
    }
    get tambahPasanganTbl() {
        return this._tambahPasanganTbl;
    }
}
export class FotoPasangan extends BaseComponent {
    constructor() {
        super();
        this._url = '';
        this._imgEl = null;
        this._template = `
			<div class='foto-pasangan'>
				<img src='' />
				<div class='tombol'>
					<button class='edit'>Edit</button>
					<button class='hapus'>Hapus</button>
				</div>
			</div>
		`;
        this.build();
        this._imgEl = this.getEl('img');
    }
    init() {
    }
    get imgEl() {
        return this._imgEl;
    }
    get url() {
        return this._url;
    }
    set url(value) {
        this._url = value;
    }
}
