export class Anggota {
    constructor() {
        this._nama = '';
        this._id = 0;
        this._orangTuaId = 0;
        this._pasanganId = 0;
        this._view = null;
        this._url = '';
    }
    get url() {
        return this._url;
    }
    set url(value) {
        this._url = value;
    }
    get view() {
        return this._view;
    }
    set view(value) {
        this._view = value;
    }
    get nama() {
        return this._nama;
    }
    set nama(value) {
        this._nama = value;
    }
    get id() {
        return this._id;
    }
    set id(value) {
        this._id = value;
    }
    get orangTuaId() {
        return this._orangTuaId;
    }
    set orangTuaId(value) {
        this._orangTuaId = value;
    }
    get pasanganId() {
        return this._pasanganId;
    }
    set pasanganId(value) {
        this._pasanganId = value;
    }
}
