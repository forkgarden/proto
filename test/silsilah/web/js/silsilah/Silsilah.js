import { Db } from "./Db.js";
export class Silsilah {
    constructor() {
        this._db = new Db();
    }
    get db() {
        return this._db;
    }
    static get inst() {
        return Silsilah._inst;
    }
    static set inst(value) {
        Silsilah._inst = value;
    }
}
