// declare var md5: Function;
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
export class Util {
    static getUrl(url, params) {
        let urlHasil = url;
        console.log('get url');
        params.forEach((item) => {
            urlHasil = urlHasil.replace(/\:[a-z]+/, item);
            console.log('item ' + item);
            console.log('url ' + urlHasil);
        });
        return urlHasil;
    }
    static escape(str) {
        let hasil = str;
        while (hasil.indexOf("<") > -1) {
            hasil = hasil.replace("<", "&lt;");
        }
        while (hasil.indexOf(">") > -1) {
            hasil = hasil.replace(">", "&gt;");
        }
        return hasil;
    }
    static error(code, message) {
        return JSON.stringify({
            code: code,
            message: message
        });
    }
    /**
     * Ajax tanpa loading
     * @param type
     * @param url
     * @param dataStr
     */
    static Ajax(type, url, dataStr) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                try {
                    console.group('send data');
                    console.log(dataStr);
                    console.groupEnd();
                    let xhr = new XMLHttpRequest();
                    xhr.onload = () => {
                        if (200 == xhr.status) {
                            // Util._resp.code = xhr.status;
                            resolve(xhr.responseText);
                        }
                        else {
                            console.log('response error');
                            reject(new Error(JSON.stringify({
                                code: xhr.status,
                                teks: xhr.statusText
                            })));
                        }
                    };
                    xhr.onerror = (e) => {
                        console.log('xhr error');
                        console.log(e);
                        reject(Util.error(500, e.message));
                    };
                    xhr.open(type, url, true);
                    xhr.setRequestHeader('Content-type', 'application/json');
                    xhr.send(dataStr);
                }
                catch (e) {
                    console.log('Util error');
                    console.log(e);
                    reject(Util.error(500, e.message));
                }
            });
        });
    }
}
Util.urlAdmin = '/admin';
Util.urlFileUpload = '/file/baru';
Util.urlAnggotaBaru = "/anggota/baru";
Util.urlAnggotaHapus = "/anggota/hapus/:id";
Util.urlAnggotaBacaBerdasarPersetujuan = '/anggota/baca/setuju/:setuju';
Util.urlAnggotaEdit = '/anggota/edit';
Util.urlAnggotaUpdateSetuju = '/anggota/update/id/:id/setuju/:setuju';
Util.urlAnggotaBacaById = '/anggota/baca/:id'; //TODO: diganti
Util.urlAnggotaUpdate = '/anggota/update';
Util.urlAnggotaUpdatePassword = '/anggota/update/password';
Util.urlLoginStatus = '/auth/status';
Util.urlLogin = '/auth/login';
Util.urlLogout = '/auth/logout';
Util.urlFileHapus = '/file/hapus/:id';
Util.urlBarangBaca = '/barang/baca/'; //TODO: diganti
Util.urlBarangBaru = '/barang/baru/';
Util.urlBarangUpdate = '/barang/update/:id';
Util.urlBarangCariPost = '/barang/cari';
Util.urlBarangTerkait = '/barang/terkait';
Util.urlBarangUpdateTerakhirDilihat = '/barang/update/lastview/:id';
Util.urlConfigUpdate = '/konfig/update/:id';
Util.urlConfigReload = '/konfig/reload';
Util.urlConfigBaca = '/konfig/baca';
Util.sLapak = 'lapak'; //TODO: dihapus
Util.sLapakId = 'lapak_id';
Util.sLevel = 'level';
