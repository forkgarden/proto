var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { BaseComponent } from "./BaseComponent.js";
import { db } from "./db/Db.js";
import { dialog } from "./Dialog.js";
import { loading } from "./Loading.js";
class EditFile {
    constructor() {
        this._view = new View();
        this.copy = {};
        this.editMode = false;
        this._view.simpanTbl.onclick = (e) => {
            e.stopPropagation();
            try {
                loading.tampil();
                this.formSubmit().then(() => {
                    this._view.detach();
                    loading.detach();
                    this._selesai();
                }).catch((e) => {
                    console.log(e);
                    loading.detach();
                    dialog.tampil2(e.message);
                });
            }
            catch (e) {
                loading.detach();
                console.log(e);
                dialog.tampil2(e.message);
            }
        };
        this._view.tutupTbl.onclick = (e) => {
            e.stopPropagation();
            this._view.detach();
            this._selesai();
        };
    }
    set selesai(value) {
        this._selesai = value;
    }
    get view() {
        return this._view;
    }
    formSubmit() {
        return __awaiter(this, void 0, void 0, function* () {
            console.log('form submit');
            this.copy.nama = this._view.namaInput.value;
            this.copy.content = this._view.kontent.value;
            this.copy.tipe = this._view.typeInput.value;
            this.copy.urutan = this._view.urutanInput.value;
            console.log(this.copy);
            if (this.editMode) {
                console.log('simpan edit');
                yield db.fileData.edit(this.copy.id, this.copy);
            }
            else {
                console.log('file data baru');
                yield db.fileData.baru(this.copy);
            }
        });
    }
    tampil(file, dataAsli, editMode) {
        this._view.attach(document.body);
        this.copy.projectId = dataAsli.projectId;
        this.copy.id = dataAsli.id;
        this.copy.nama = dataAsli.nama;
        this.copy.content = dataAsli.content;
        this.copy.tipe = dataAsli.tipe;
        this.copy.urutan = dataAsli.urutan;
        this.editMode = editMode;
        if (file.nama && file.nama != '')
            this.copy.nama = file.nama;
        if (file.content && file.content != '')
            this.copy.content = file.content;
        if (file.tipe && file.tipe != '')
            this.copy.tipe = file.tipe;
        if (file.urutan && file.urutan != '')
            this.copy.urutan = file.urutan;
        this._view.namaInput.value = this.copy.nama;
        this._view.kontent.value = this.copy.content;
        this._view.typeInput.value = this.copy.tipe;
        this._view.urutanInput.value = this.copy.urutan;
    }
}
class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
            <div class='edit-file'>
                <div class='header'>
                    <button class='tutup'>X</button>
                    <div class='judul'>Edit File</div>
                    <button class='simpan'>simpan</button> 
                </div>
                <div class='cont'>
                    <form>
                        <label>Nama File:</label>
                        <input type='text' class='nama'>
                        <label>Type:</label>
                        <input type='text' class='type'>
                        <label>urutan:</label>
                        <input type='text' class='urutan'>
                        <label>Konten:</label>
                        <textarea class='content'></textarea>
                    </form>
                </div>
            </div>          
        `;
        this.build();
    }
    get namaInput() {
        return this.getEl('input.nama');
    }
    get urutanInput() {
        return this.getEl('input.urutan');
    }
    get typeInput() {
        return this.getEl('input.type');
    }
    get kontent() {
        return this.getEl('textarea.content');
    }
    get form() {
        return this.getEl('form');
    }
    get tutupTbl() {
        return this.getEl('button.tutup');
    }
    get simpanTbl() {
        return this.getEl('button.simpan');
    }
}
export var editFile = new EditFile();
