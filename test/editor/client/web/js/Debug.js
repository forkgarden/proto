var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { daftarProject } from "./DaftarProject.js";
import { db } from "./db/Db.js";
import { editFile } from "./EditFile.js";
import { upload } from "./Upload.js";
class Debug {
    debugUpload() {
        upload.tampil();
        upload.selesai = (f) => {
            console.log(f);
        };
    }
    debug() {
        return __awaiter(this, void 0, void 0, function* () {
            yield db.load();
            yield db.simpan();
            yield daftarProject.loadRender();
            daftarProject.view.attach(document.body);
            daftarProject.editItem(daftarProject.projectAr[0]);
        });
    }
    debugEditFile() {
        let file = {
            content: 'file content',
            // folder: 'js',
            id: '1',
            nama: 'nama',
            projectId: '1',
            tipe: 'js'
        };
        let dataAsli = {};
        editFile.tampil(file, dataAsli, false);
    }
}
export var debug = new Debug();
