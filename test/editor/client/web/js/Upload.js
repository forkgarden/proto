var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { BaseComponent } from "./BaseComponent.js";
import { dialog } from "./Dialog.js";
import { loading } from "./Loading.js";
class Upload {
    constructor() {
        this._view = new View();
        this._fileObj = {};
        this._view.input.onchange = (e) => {
            e.stopPropagation();
            console.log('input on change');
            let file = e.target.files[0];
            console.log('file:');
            console.log(file);
            this._fileObj.nama = (file && file.name) ? file.name : '';
            console.log('nama:');
            console.log(this._fileObj.nama);
            loading.tampil();
            this.bacaFile(file).then((c) => {
                loading.detach();
                this._fileObj.content = c;
                this._view.detach();
                this._selesai(this._fileObj);
            }).catch((e) => {
                console.log(e);
                dialog.tampil2(e.message);
                loading.detach();
            });
        };
    }
    get view() {
        return this._view;
    }
    get fileObj() {
        return this._fileObj;
    }
    set selesai(value) {
        this._selesai = value;
    }
    tampil() {
        this._view.attach(document.body);
    }
    bacaFile(f) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                try {
                    const reader = new FileReader();
                    reader.addEventListener('load', (event) => {
                        resolve(event.target.result);
                    });
                    reader.onerror = (e) => {
                        console.log(e);
                        reject(e);
                    };
                    reader.readAsText(f);
                }
                catch (e) {
                    console.log(e);
                    reject(e.message);
                }
            });
        });
    }
}
class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
            <div class='upload'>
                <input type='file'>;
            </div>
        `;
        this.build();
    }
    get input() {
        return this.getEl('input');
    }
}
export var upload = new Upload();
