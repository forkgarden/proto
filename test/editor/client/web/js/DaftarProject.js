var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { BaseComponent } from "./BaseComponent.js";
import { daftarFile } from "./DaftarFile.js";
import { db } from "./db/Db.js";
import { dialog } from "./Dialog.js";
import { loading } from "./Loading.js";
import { menuPopup } from "./MenuPopUp.js";
class DaftarProject {
    constructor() {
        // console.log(this);
        this._view = new View();
        this._projectAr = [];
        this._view.menutbl.onclick = (e) => {
            e.stopPropagation();
            console.log('menu click');
            let nama = window.prompt('Nama Project:');
            this.tambahProject(nama);
        };
    }
    get projectAr() {
        return this._projectAr;
    }
    get view() {
        return this._view;
    }
    tambahProject(nama) {
        let obj = {
            nama: nama
        };
        loading.tampil();
        db.project.baru(obj).then(() => {
            loading.detach();
            this.loadRender();
        }).catch((e) => {
            console.log(e);
            loading.detach();
            dialog.tampil2(e.message);
        });
    }
    tampil(cont) {
        this.view.attach(cont);
        this.loadRender();
    }
    loadRender() {
        return __awaiter(this, void 0, void 0, function* () {
            loading.tampil();
            yield this.load().then(() => {
                this.render(this._projectAr);
                loading.detach();
            }).catch((e) => {
                console.log(e);
                loading.detach();
                dialog.tampil2(e.message);
            });
        });
    }
    load() {
        return __awaiter(this, void 0, void 0, function* () {
            this._projectAr = yield db.project.bacaSemua();
        });
    }
    render(itemAr) {
        this.view.cont.innerHTML = '';
        itemAr.forEach((itemObj) => {
            let item = new Item();
            item.itemObj = itemObj;
            item.attach(this.view.cont);
            item.elHtml.onclick = () => {
                this.itemOnClick(item);
            };
        });
    }
    itemOnClick(item) {
        menuPopup.bersih();
        menuPopup.tampil([
            {
                label: 'hapus',
                f: () => {
                    menuPopup.sembunyi();
                    db.project.hapus(item.itemObj.id);
                    this.loadRender();
                }
            },
            {
                label: 'edit detail',
                f: () => {
                    menuPopup.sembunyi();
                    // this._view.detach();
                    let nama = window.prompt('Nama:', item.itemObj.nama);
                    loading.tampil();
                    db.project.edit(item.itemObj.id, item.itemObj).then(() => {
                        loading.detach();
                    }).catch((e) => {
                        loading.detach();
                        console.log(e);
                        dialog.tampil2(e.message);
                    });
                    //local quick update
                    item.itemObj.nama = nama;
                    item.judulP.innerHTML = nama;
                }
            },
            {
                label: 'edit files',
                f: () => {
                    this.editItem(item.itemObj);
                }
            }
        ]);
    }
    editItem(item) {
        menuPopup.sembunyi();
        this._view.detach();
        daftarFile.selesai = () => {
            this._view.attach(document.body);
        };
        daftarFile.tampil(document.body, item.id);
    }
}
class Item extends BaseComponent {
    constructor() {
        super();
        this._template = `
        <div class='item'>
            <p class='judul'></p>
        </div>`;
        this.build();
        this._elHtml.onclick = (e) => {
            e.stopPropagation();
            //TODO: buat menu
        };
    }
    get judulP() {
        return this.getEl('div.item p.judul');
    }
    get itemObj() {
        return this._itemObj;
    }
    set itemObj(value) {
        this._itemObj = value;
        this.judulP.innerHTML = value.nama;
    }
}
class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
            <div class='daftar-page'>
                <div class='header'>
                    <button class='tutup'>X</button>
                    <div class='judul'>Daftar Project</div>
                    <button class='menu'>+</button> 
                </div>
                <div class='daftar-cont'>
                </div>
            </div>
        `;
        this.build();
    }
    get tutupTbl() {
        return this.getEl('button.tutup');
    }
    get menutbl() {
        return this.getEl('button.menu');
    }
    get cont() {
        return this.getEl('div.daftar-cont');
    }
}
export var daftarProject = new DaftarProject();
