class NomorOto {
    constructor() {
        this._id = 0;
    }
    simpan() {
        window.localStorage.setItem('edit_nomorOto', this._id + '');
    }
    load() {
        this._id = parseInt(window.localStorage.getItem('edit_nomorOto')) || 0;
    }
    get id() {
        this._id++;
        return this._id;
    }
    set id(value) {
        this._id = value;
    }
}
export var nomorOto = new NomorOto();
