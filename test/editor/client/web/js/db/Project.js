var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { db } from "./Db.js";
import { nomorOto } from "./NomorOto.js";
export class Project {
    constructor() {
        this.daftar = [];
        // console.log(this);
    }
    baru(data) {
        return __awaiter(this, void 0, void 0, function* () {
            data.id = nomorOto.id + '';
            this.daftar.push(data);
            yield this.simpan();
            return data.id;
        });
    }
    bacaSemua() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.daftar;
        });
    }
    bacaId(id) {
        return __awaiter(this, void 0, void 0, function* () {
            for (let i = 0; i < this.daftar.length; i++) {
                if (this.daftar[i].id == id) {
                    return this.daftar[i];
                }
            }
            return null;
        });
    }
    hapus(id) {
        return __awaiter(this, void 0, void 0, function* () {
            for (let i = 0; i < this.daftar.length; i++) {
                if (this.daftar[i].id == id) {
                    this.daftar.splice(i, 1);
                }
            }
            yield db.fileData.hapusByProjectId(id);
            this.simpan();
        });
    }
    hapusSemua() {
        return __awaiter(this, void 0, void 0, function* () {
            this.daftar = [];
            this.simpan();
        });
    }
    simpan() {
        return __awaiter(this, void 0, void 0, function* () {
            window.localStorage.setItem('edit_project', JSON.stringify(this.daftar));
        });
    }
    load() {
        return __awaiter(this, void 0, void 0, function* () {
            let str = window.localStorage.getItem('edit_project');
            this.daftar = JSON.parse(str);
            this.daftar = this.daftar || [];
        });
    }
    edit(id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            let item = yield this.bacaId(id);
            item.nama = data.nama;
            this.simpan();
        });
    }
}
