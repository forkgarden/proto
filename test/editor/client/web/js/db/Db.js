var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { FileData } from "./FileData.js";
import { nomorOto } from "./NomorOto.js";
import { Project } from "./Project.js";
class Db {
    constructor() {
        this._project = new Project();
        this._fileData = new FileData();
    }
    get fileData() {
        return this._fileData;
    }
    get project() {
        return this._project;
    }
    simpan() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._project.simpan();
            nomorOto.simpan();
            this._fileData.simpan();
        });
    }
    hapusSemua() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._project.hapusSemua();
            yield this._fileData.hapusSemua();
        });
    }
    load() {
        return __awaiter(this, void 0, void 0, function* () {
            this._project.load();
            nomorOto.load();
            this._fileData.load();
        });
    }
}
export var db = new Db();
