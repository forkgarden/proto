var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { daftarProject } from "./DaftarProject.js";
import { db } from "./db/Db.js";
// import { debug } from "./Debug.js";
// import { debug } from "./Debug.js";
window.onload = () => {
    console.log('window on load');
    mulai();
};
function mulai() {
    return __awaiter(this, void 0, void 0, function* () {
        // await db.hapusSemua();
        yield db.load();
        yield daftarProject.loadRender();
        daftarProject.view.attach(document.body);
        yield db.simpan();
    });
}
window.onbeforeunload = () => {
    db.simpan();
};
