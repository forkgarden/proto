var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { BaseComponent } from "./BaseComponent.js";
import { db } from "./db/Db.js";
import { dialog } from "./Dialog.js";
import { editFile } from "./EditFile.js";
import { loading } from "./Loading.js";
import { menuPopup } from "./MenuPopUp.js";
class DaftarFile {
    constructor() {
        this._view = new View();
        this._projectId = '';
        this.files = [];
        this._view.menutbl.onclick = (e) => {
            e.stopPropagation();
            this.tambahFile();
        };
        this._view.tutupTbl.onclick = (e) => {
            e.stopPropagation();
            console.log('tutup click');
            this._view.detach();
            this._selesai();
        };
    }
    set selesai(value) {
        this._selesai = value;
    }
    get view() {
        return this._view;
    }
    tambahFile() {
        let obj = {};
        obj.projectId = this._projectId;
        this._view.detach();
        editFile.tampil(obj, obj, false);
        editFile.selesai = () => {
            this._view.attach(document.body);
            this.loadRender();
        };
        /*
        upload.tampil();
        upload.selesai = (f: FileObj) => {
            this._view.detach();
            editFile.tampil(f, obj, false);
            editFile.selesai = () => {
                this._view.attach(document.body);
                this.loadRender();
            }
        }
        */
    }
    tampil(cont, projectId) {
        this.view.attach(cont);
        this._projectId = projectId;
        this.loadRender();
    }
    loadRender() {
        return __awaiter(this, void 0, void 0, function* () {
            loading.tampil();
            yield this.load().then((itemAr) => {
                this.files = [];
                itemAr.forEach((item) => {
                    this.files.push(item);
                });
                this.render(itemAr);
                loading.detach();
            }).catch((e) => {
                console.log(e);
                loading.detach();
                dialog.tampil2(e.message);
            });
        });
    }
    load() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield db.fileData.bacaByProjectId(this._projectId);
        });
    }
    render(itemAr) {
        this.view.cont.innerHTML = '';
        itemAr.forEach((itemObj) => {
            let item = new Item();
            item.itemObj = itemObj;
            item.attach(this.view.cont);
            item.elHtml.onclick = () => {
                this.itemOnClick(item);
            };
        });
    }
    pilihItem(idx) {
        return this.files[idx];
    }
    itemOnClick(item) {
        menuPopup.bersih();
        menuPopup.tampil([
            {
                label: 'hapus',
                f: () => {
                    menuPopup.sembunyi();
                    db.project.hapus(item.itemObj.id);
                    this.loadRender();
                }
            },
            {
                label: 'edit',
                f: () => {
                    this.editItem(item.itemObj);
                }
            }
        ]);
    }
    editItem(fileObj) {
        menuPopup.sembunyi();
        this._view.detach();
        editFile.tampil(fileObj, fileObj, true);
        editFile.selesai = () => {
            this._view.attach(document.body);
            this.loadRender();
        };
    }
}
class Item extends BaseComponent {
    constructor() {
        super();
        this._template = `
        <div class='item'>
            <div class='file'></div>
        </div>`;
        this.build();
    }
    get itemObj() {
        return this._itemObj;
    }
    set itemObj(value) {
        this._itemObj = value;
        this.fileDiv().innerHTML = value.nama + ' (' + value.urutan + ')';
    }
    fileDiv() {
        return this.getEl('div.item div.file');
    }
}
class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
            <div class='daftar-page'>
                <div class='header'>
                    <button class='tutup'>X</button>
                    <div class='judul'>Daftar File</div>
                    <button class='menu'>+</button> 
                </div>
                <div class='daftar-cont'>
                </div>
            </div>
        `;
        this.build();
    }
    get tutupTbl() {
        return this.getEl('button.tutup');
    }
    get menutbl() {
        return this.getEl('button.menu');
    }
    get cont() {
        return this.getEl('div.daftar-cont');
    }
}
export var daftarFile = new DaftarFile();
