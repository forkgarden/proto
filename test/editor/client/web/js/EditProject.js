var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { BaseComponent } from "./BaseComponent.js";
import { db } from "./db/Db.js";
import { dialog } from "./Dialog.js";
import { loading } from "./Loading.js";
class EditFile {
    constructor() {
        this._view = new View();
        this.copy = {};
        this.editMode = false;
        this._view.form.onsubmit = () => {
            try {
                this.formSubmit().then(() => {
                    this._view.detach();
                    this._selesai();
                }).catch((e) => {
                    console.log(e);
                    dialog.tampil2(e.message);
                });
            }
            catch (e) {
                console.log(e);
                return false;
            }
            return false;
        };
    }
    set selesai(value) {
        this._selesai = value;
    }
    get view() {
        return this._view;
    }
    formSubmit() {
        return __awaiter(this, void 0, void 0, function* () {
            this.copy.nama = this._view.namaInput.value;
            this.copy.content = this._view.kontent.value;
            this.copy.tipe = this._view.typeInput.value;
            loading.tampil();
            if (this.editMode) {
                yield db.fileData.edit(this.copy.id, this.copy);
            }
            else {
                yield db.fileData.baru(this.copy);
            }
            loading.detach();
        });
    }
    tampil(file, dataAsli, editMode) {
        this._view.attach(document.body);
        this.copy.projectId = dataAsli.projectId;
        this.copy.nama = dataAsli.nama;
        this.copy.content = dataAsli.content;
        this.copy.tipe = dataAsli.tipe;
        this.copy.id = dataAsli.id;
        this.editMode = editMode;
        if (file.nama && file.nama != '')
            this.copy.nama = file.nama;
        if (file.content && file.content != '')
            this.copy.content = file.content;
    }
}
class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
            <div class='edit-project'>
                <div class='header'>
                    <button class='tutup'>X</button>
                    <div class='judul'>Edit Project</div>
                    <button class='simpan'>simpan</button> 
                </div>
                <form>
                    <label>Nama File:</label><br/>
                    <input type='text' class='nama'><br/>
                    <label>Folder:</label><br/>
                    <input type='text' class='folder'><br/>
                    <label>Type:</label><br/>
                    <input type='text' class='type'><br/>
                    <label>Konten:</label><br/>
                    <textarea class='content'></textarea>
                </form>
            </div>          
        `;
        this.build();
    }
    get namaInput() {
        return this.getEl('input.nama');
    }
    get folderInput() {
        return this.getEl('input.folder');
    }
    get typeInput() {
        return this.getEl('input.type');
    }
    get kontent() {
        return this.getEl('textarea.content');
    }
    get form() {
        return this.getEl('form');
    }
    get tutupTbl() {
        return this.getEl('button.tutup');
    }
    get simpanTbl() {
        return this.getEl('button.simpan');
    }
}
export var editFile = new EditFile();
