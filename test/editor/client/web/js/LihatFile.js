var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
import { BaseComponent } from "./BaseComponent.js";
import { db } from "./db/Db.js";
import { dialog } from "./Dialog.js";
import { loading } from "./Loading.js";
class LihatFile {
    constructor() {
        this._view = new View();
        this._fileId = '';
    }
    get view() {
        return this._view;
    }
    tampil(cont, fileId) {
        this.view.attach(cont);
        this._fileId = fileId;
        this.loadRender();
    }
    loadRender() {
        loading.tampil();
        this.load().then((item) => {
            this.render(item);
            loading.detach();
        }).catch((e) => {
            console.log(e);
            loading.detach();
            dialog.tampil2(e.message);
        });
    }
    load() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield db.fileData.bacaId(this._fileId);
        });
    }
    render(item) {
        item;
    }
}
class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
            <div class='daftar-page'>
                <div class='header'>
                    <button class='tutup'>X</button>
                    <div class='judul'>Daftar File</div>
                </div>
                <div class='cont'>
                    <div class='label'>File:</div>
                    <div class='deksripsi file'></div>
            
                    <div class='label'>Folder:</div>
                    <div class='deksripsi folder'></div>
            
                    <div class='label'>Tipe:</div>
                    <div class='deksripsi tipe'></div>
            
                    <div class='label'>Konten:</div>
                    <div class='deksripsi konten'></div>
                </div>
            </div>
        `;
        this.build();
    }
    get tutupTbl() {
        return this.getEl('button.tutup');
    }
    get fileDiv() {
        return this.getEl('div.item div.file');
    }
    get tipeDiv() {
        return this.getEl('div.item div.tipe');
    }
    get kontenDiv() {
        return this.getEl('div.item div.konten');
    }
}
export var daftarFile = new LihatFile();
