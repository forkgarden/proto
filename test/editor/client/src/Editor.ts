import { daftarProject } from "./DaftarProject.js";
import { db } from "./db/Db.js";
// import { debug } from "./Debug.js";
// import { debug } from "./Debug.js";

window.onload = () => {
    console.log('window on load');
    mulai();
}

async function mulai():Promise<void> {
    // await db.hapusSemua();
    await db.load();
    await daftarProject.loadRender();
    daftarProject.view.attach(document.body);
    await db.simpan();
}

window.onbeforeunload = () => {
    db.simpan();
}