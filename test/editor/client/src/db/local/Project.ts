import { db } from "../Db.js";
import { nomorOto } from "./NomorOto.js";

export class Project {
    daftar: IProjectObj[] = [];

    constructor() {
        // console.log(this);
    }

    async baru(data: IProjectObj): Promise<string> {
        data.id = nomorOto.id + '';

        this.daftar.push(data);

        await this.simpan();

        return data.id;
    }

    async bacaSemua(): Promise<IProjectObj[]> {
        return this.daftar
    }

    async bacaId(id: string): Promise<IProjectObj> {
        for (let i: number = 0; i < this.daftar.length; i++) {
            if (this.daftar[i].id == id) {
                return this.daftar[i];
            }
        }

        return null;
    }

    async hapus(id: string): Promise<void> {
        for (let i: number = 0; i < this.daftar.length; i++) {
            if (this.daftar[i].id == id) {
                this.daftar.splice(i, 1);
            }
        }

        await db.fileData.hapusByProjectId(id);
        
        this.simpan();
    }

    async hapusSemua():Promise<void> {
        this.daftar=[];
        this.simpan();
    }

    async simpan(): Promise<void> {
        window.localStorage.setItem('edit_project', JSON.stringify(this.daftar));
    }

    async load(): Promise<void> {
        let str: string = window.localStorage.getItem('edit_project');
        this.daftar = JSON.parse(str);
        this.daftar = this.daftar || [];
    }

    async edit(id: string, data: IProjectObj): Promise<void> {
        let item: IProjectObj = await this.bacaId(id);
        item.nama = data.nama;
        this.simpan();
    }

}

