import { nomorOto } from "./NomorOto.js";

export class FileData {
    daftar: FileObj[] = [];

    constructor() {
        // console.log(this);
    }

    async baru(data: FileObj): Promise<string> {
        data.id = nomorOto.id + '';
        this.daftar.push(data);
        await this.simpan();
        return data.id;
    }

    async bacaSemua(): Promise<FileObj[]> {
        return this.daftar;
    }

    async bacaByProjectId(id: string): Promise<FileObj[]> {

        let hasil: FileObj[] = [];
        for (let i: number = 0; i < this.daftar.length; i++) {
            if (this.daftar[i].projectId == id) {
                hasil.push(this.daftar[i]);
            }
        }

        return hasil;
    }

    async bacaId(id: string): Promise<FileObj> {
        for (let i: number = 0; i < this.daftar.length; i++) {
            if (this.daftar[i].id == id) {
                return this.daftar[i];
            }
        }

        return null;
    }

    async hapus(id: string): Promise<void> {
        for (let i: number = 0; i < this.daftar.length; i++) {
            if (this.daftar[i].id == id) {
                this.daftar.splice(i, 1);
            }
        }

        this.simpan();
    }

    async hapusByProjectId(id: string): Promise<void> {
        for (let i: number = this.daftar.length - 1; i >= 0; i--) {
            if (this.daftar[i].projectId == id) {
                this.daftar.splice(i, 1);
            }
        }

        this.simpan();
    }

    async hapusSemua(): Promise<void> {
        this.daftar=[];
        this.simpan();
    }


    async simpan(): Promise<void> {
        window.localStorage.setItem('edit_file', JSON.stringify(this.daftar));
    }

    async load(): Promise<void> {
        let str: string = window.localStorage.getItem('edit_file');
        this.daftar = JSON.parse(str);
        this.daftar = this.daftar || [];
    }

    async edit(id: string, data: FileObj): Promise<void> {
        let item: FileObj = await this.bacaId(id);
        item.nama = data.nama;
        item.konten = data.konten;
        item.tipe = data.tipe;
        item.urutan = data.urutan;
        this.simpan();
    }
}