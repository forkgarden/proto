class NomorOto {
    private _id: number = 0;

    simpan(): void {
        window.localStorage.setItem('edit_nomorOto', this._id + '');
    }

    load(): void {
        this._id = parseInt(window.localStorage.getItem('edit_nomorOto')) || 0;
    }

    public get id(): number {
        this._id++;
        return this._id;
    }

    public set id(value: number) {
        this._id = value;
    }
}

export var nomorOto: NomorOto = new NomorOto();