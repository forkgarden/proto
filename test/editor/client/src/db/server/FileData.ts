// import { nomorOto } from "./NomorOto.js";

import { data } from "../../Data";
import { Util } from "../../Util";

export class FileData {
    daftar: FileObj[] = [];

    constructor() {

    }

    async baru(file: FileObj): Promise<string> {
        //TODO
        let hasil:string = await Util.Ajax('post', data.urlFileBaru, JSON.stringify(file));
        return (JSON.parse(hasil) as FileObj).id;
    }

    //TODO:
    async bacaSemua(): Promise<FileObj[]> {
        let hasil:string = await Util.Ajax('post', data.urlFileBacaSemua, '');
        return JSON.parse(hasil) as FileObj[];
    }

    async bacaByProjectId(id: string): Promise<FileObj[]> {
        let hasil: string = await Util.Ajax('post', data.urlFileBacaProjectId, JSON.stringify({id:id}));
        return JSON.parse(hasil) as FileObj[];
    }

    async bacaId(id: string): Promise<FileObj> {
        let hasil: string = await Util.Ajax('post', data.urlFileBacaId, JSON.stringify({id:id}));
        return JSON.parse(hasil) as FileObj;
    }

    async edit(id: string, fileObj: FileObj): Promise<void> {
        await Util.Ajax('post', data.urlFileEdit, JSON.stringify({id:id, file:fileObj}));
    }


    async hapus(id: string): Promise<void> {
        await Util.Ajax('post', data.urlFileHapusId, JSON.stringify({id:id}));
    }

    async hapusByProjectId(id: string): Promise<void> {
        id;
        //stub
    }

    async hapusSemua(): Promise<void> {
        //stub
    }


    async simpan(): Promise<void> {
        //stub
    }

    async load(): Promise<void> {
        //stub
    }

}