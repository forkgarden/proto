import { data } from "../../Data";
import { Util } from "../../Util";

export class Project {

    constructor() {
        // console.log(this);
    }

    async baru(projek: IProjectObj): Promise<string> {
        let hasil:string = await Util.Ajax('post', data.urlProjectBaru, JSON.stringify({projek:projek}));
        return (JSON.parse(hasil) as IProjectObj).id;
    }

    async bacaSemua(): Promise<IProjectObj[]> {
        let hasil:string = await Util.Ajax('post', data.urlProjectBacaSemua, '');
        return (JSON.parse(hasil) as IProjectObj[]);
    }

    async bacaId(id: string): Promise<IProjectObj> {
        let hasil:string = await Util.Ajax('post', data.urlProjectBacaSemua, JSON.stringify({id: id}));
        return (JSON.parse(hasil) as IProjectObj);
    }

    async hapus(id: string): Promise<void> {
        await Util.Ajax('post', data.urlProjectHapusId, JSON.stringify({id: id}));
    }

    async edit(id: string, projek: IProjectObj): Promise<void> {
        await Util.Ajax('post', data.urlProjectEdit, JSON.stringify({id: id, projek:projek}));
    }

    async hapusSemua():Promise<void> {
        //stub
    }


}

