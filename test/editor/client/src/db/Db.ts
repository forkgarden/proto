import { Auth } from "./local/Auth.js";
import { FileData } from "./local/FileData.js";
import { nomorOto } from "./local/NomorOto.js";
import { Project } from "./local/Project.js";

class Db {
    private _project: Project = new Project();
    private _fileData: FileData = new FileData();
    private _auth: Auth = new Auth();
    
    public get auth(): Auth {
        return this._auth;
    }

    public get fileData(): FileData {
        return this._fileData;
    }
    public get project(): Project {
        return this._project;
    }

    async simpan(): Promise<void> {
        await this._project.simpan();
        nomorOto.simpan();
        this._fileData.simpan();
    }

    async hapusSemua():Promise<void> {
        await this._project.hapusSemua();
        await this._fileData.hapusSemua();
    }

    async load(): Promise<void> {
        this._project.load();
        nomorOto.load();
        this._fileData.load();
    }
}

export var db: Db = new Db();