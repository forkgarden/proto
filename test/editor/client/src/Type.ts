interface IProjectObj
 {
    id?:string;
    nama?:string;
    meta?:string;
}

interface FileObj {
    id?:string;
    nama?: string;
    // folder?:string;
    konten?:string;
    tipe?:string;
    urutan?:string;
    projectId?:string;
}