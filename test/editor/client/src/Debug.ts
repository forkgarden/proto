import { daftarProject } from "./DaftarProject.js";
import { db } from "./db/Db.js";
import { editFile } from "./EditFile.js";
import { upload } from "./Upload.js";

class Debug {
    debugUpload():void {
        upload.tampil();
        upload.selesai = (f:FileObj) => {
            console.log(f);
        }
    }

    async debug():Promise<void> {
        await db.load();
        await db.simpan();
        await daftarProject.loadRender();
        daftarProject.view.attach(document.body);
        daftarProject.editItem(daftarProject.projectAr[0]);
    }
    
    debugEditFile(): void {
        let file: FileObj = {
            konten: 'file content',
            // folder: 'js',
            id: '1',
            nama: 'nama',
            projectId:'1',
            tipe: 'js'
        };
        let dataAsli: FileObj = {};
    
        editFile.tampil(file, dataAsli, false);
    }
    
}

export var debug:Debug = new Debug();

