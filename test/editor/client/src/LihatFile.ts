import { BaseComponent } from "./BaseComponent.js";
import { db } from "./db/Db.js";
import { dialog } from "./Dialog.js";
import { loading } from "./Loading.js";

class LihatFile {

    private _view: View = new View();
    private _fileId: string = '';
    public get view(): View { 
        return this._view;
    }

    constructor() {
    }

    tampil(cont: HTMLElement, fileId:string): void {
        this.view.attach(cont);
        this._fileId = fileId;
        this.loadRender();
    }

    loadRender(): void {
        loading.tampil();
        this.load().then((item: FileObj) => {
            this.render(item);
            loading.detach();
        }).catch((e) => {
            console.log(e);
            loading.detach();
            dialog.tampil2(e.message);
        });
    }

    async load(): Promise<FileObj> {
        return await db.fileData.bacaId(this._fileId);
    }

    render(item: FileObj): void {
        item;
    }

}

class View extends BaseComponent {

    constructor() {
        super();
        this._template = `
            <div class='daftar-page'>
                <div class='header'>
                    <button class='tutup'>X</button>
                    <div class='judul'>Daftar File</div>
                </div>
                <div class='cont'>
                    <div class='label'>File:</div>
                    <div class='deksripsi file'></div>
            
                    <div class='label'>Folder:</div>
                    <div class='deksripsi folder'></div>
            
                    <div class='label'>Tipe:</div>
                    <div class='deksripsi tipe'></div>
            
                    <div class='label'>Konten:</div>
                    <div class='deksripsi konten'></div>
                </div>
            </div>
        `;
        this.build();
    }

    get tutupTbl(): HTMLButtonElement {
        return this.getEl('button.tutup') as HTMLButtonElement;
    }    

    get fileDiv(): HTMLDivElement {
        return this.getEl('div.item div.file') as HTMLDivElement;
    }

    get tipeDiv(): HTMLDivElement {
        return this.getEl('div.item div.tipe') as HTMLDivElement;
    }

    get kontenDiv(): HTMLDivElement {
        return this.getEl('div.item div.konten') as HTMLDivElement;
    }    
}

export var daftarFile: LihatFile = new LihatFile();