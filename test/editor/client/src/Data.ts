//TODO: FINAL
class Data {
	private _cont: HTMLDivElement;

	readonly urlFileBaru:string = '/src/file/baru';
	readonly urlFileBacaSemua:string = '/src/file/baca/semua';
	readonly urlFileBacaId:string = '/src/file/baca/id';
	readonly urlFileBacaProjectId:string = '/src/file/baca/projectId';
	readonly urlFileHapusId:string = '/src/file/hapus/id';
	readonly urlFileEdit:string = '/src/file/hapus/edit';

	readonly urlProjectBaru:string = '/src/projek/baru';
	readonly urlProjectBacaSemua:string = '/src/projek/baca/semua';
	readonly urlProjectBacaId:string = '/src/projek/baca/id';
	readonly urlProjectHapusId:string = '/src/projek/hapus/id';
	readonly urlProjectEdit:string = '/src/projek/hapus/edit';

	public get cont(): HTMLDivElement {
		return this._cont;
	}
	
	public set cont(value: HTMLDivElement) {
		this._cont = value;
	}
}

export var data: Data = new Data();