import { BaseComponent } from "./BaseComponent.js";
import { db } from "./db/Db.js";
import { dialog } from "./Dialog.js";
import { editFile } from "./EditFile.js";
import { loading } from "./Loading.js";
import { menuPopup } from "./MenuPopUp.js";

class DaftarFile {
    private _view: View = new View();
    private _projectId: string = '';
    private files:FileObj[] = [];
    private _selesai: Function;
    public set selesai(value: Function) {
        this._selesai = value;
    }

    public get view(): View { 
        return this._view;
    }

    constructor() {
        this._view.menutbl.onclick = (e: MouseEvent) => {
            e.stopPropagation();
            this.tambahFile();
        }

        this._view.tutupTbl.onclick = (e:MouseEvent) => {
            e.stopPropagation();
            console.log('tutup click');
            this._view.detach();
            this._selesai();
        }
    }
 
    tambahFile(): void {
        let obj: FileObj = {}
        obj.projectId = this._projectId;

        this._view.detach();
        editFile.tampil(obj, obj, false);
        editFile.selesai = () => {
            this._view.attach(document.body);
            this.loadRender();
        }


        /*
        upload.tampil();
        upload.selesai = (f: FileObj) => {
            this._view.detach();
            editFile.tampil(f, obj, false);
            editFile.selesai = () => {
                this._view.attach(document.body);
                this.loadRender();
            }
        }
        */

    }

    tampil(cont: HTMLElement, projectId:string): void {
        this.view.attach(cont);
        this._projectId = projectId;
        this.loadRender();
    }

    async loadRender(): Promise<void> {
        loading.tampil();
        await this.load().then((itemAr: FileObj[]) => {
            this.files = [];
            itemAr.forEach((item:FileObj) => {
                this.files.push(item);
            });
            this.render(itemAr);
            loading.detach();
        }).catch((e) => {
            console.log(e);
            loading.detach();
            dialog.tampil2(e.message);
        });
    }

    async load(): Promise<FileObj[]> {
        return await db.fileData.bacaByProjectId(this._projectId);
    }

    render(itemAr: FileObj[]): void {
        this.view.cont.innerHTML = '';
        itemAr.forEach((itemObj: FileObj) => {
            let item: Item = new Item();
            item.itemObj = itemObj;
            item.attach(this.view.cont);
            item.elHtml.onclick = () => {
                this.itemOnClick(item);
            }
        });
    }

    pilihItem(idx:number):FileObj {
        return this.files[idx];
    }

    itemOnClick(item: Item): void {
        menuPopup.bersih();
        menuPopup.tampil([
            {
                label: 'hapus',
                f: () => {
                    menuPopup.sembunyi();
                    db.project.hapus(item.itemObj.id);
                    this.loadRender();
                }
            },
            {
                label: 'edit',
                f: () => {
                    this.editItem(item.itemObj);
                }
            }
        ]);
    }

    editItem(fileObj:FileObj):void {
        menuPopup.sembunyi();
        this._view.detach();
        editFile.tampil(fileObj, fileObj, true);
        editFile.selesai = () => {
            this._view.attach(document.body);
            this.loadRender();
        }
    }

}

class Item extends BaseComponent {
    private _itemObj: FileObj;

    constructor() {
        super();
        this._template = `
        <div class='item'>
            <div class='file'></div>
        </div>`;
        this.build();
    }

    public get itemObj(): FileObj {
        return this._itemObj;
    }

    public set itemObj(value: FileObj) {
        this._itemObj = value;
        this.fileDiv().innerHTML = value.nama + ' (' + value.urutan + ')';
    }

    fileDiv(): HTMLDivElement {
        return this.getEl('div.item div.file') as HTMLDivElement;
    }


}

class View extends BaseComponent {

    constructor() {
        super();
        this._template = `
            <div class='daftar-page'>
                <div class='header'>
                    <button class='tutup'>X</button>
                    <div class='judul'>Daftar File</div>
                    <button class='menu'>+</button> 
                </div>
                <div class='daftar-cont'>
                </div>
            </div>
        `;
        this.build();
    }

    get tutupTbl(): HTMLButtonElement {
        return this.getEl('button.tutup') as HTMLButtonElement;
    }

    get menutbl(): HTMLButtonElement {
        return this.getEl('button.menu') as HTMLButtonElement;
    }

    get cont(): HTMLDivElement {
        return this.getEl('div.daftar-cont') as HTMLDivElement;
    }
}

export var daftarFile: DaftarFile = new DaftarFile();