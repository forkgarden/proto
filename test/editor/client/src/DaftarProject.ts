import { BaseComponent } from "./BaseComponent.js";
import { daftarFile } from "./DaftarFile.js";
import { db } from "./db/Db.js";
import { dialog } from "./Dialog.js";
import { loading } from "./Loading.js";
import { menuPopup } from "./MenuPopUp.js";

class DaftarProject {
    private _view: View = new View();
    private _projectAr: IProjectObj[] = [];
    
    public get projectAr(): IProjectObj[] {
        return this._projectAr;
    }

    public get view(): View {
        return this._view;
    }

    constructor() {
        // console.log(this);

        this._view.menutbl.onclick = (e: MouseEvent) => {
            e.stopPropagation();
            console.log('menu click');
            let nama: string = window.prompt('Nama Project:');
            this.tambahProject(nama);
        }
    }

    tambahProject(nama: string): void {
        let obj: IProjectObj = {
            nama: nama
        }


        loading.tampil();


        db.project.baru(obj).then(() => {
            loading.detach();
            this.loadRender();
        }).catch((e) => {
            console.log(e);
            loading.detach();
            dialog.tampil2(e.message);
        });

    }

    tampil(cont: HTMLDivElement): void {
        this.view.attach(cont);
        this.loadRender();
    }

    async loadRender(): Promise<void> {
        loading.tampil();
        await this.load().then(() => {
            this.render(this._projectAr);
            loading.detach();
        }).catch((e) => {
            console.log(e);
            loading.detach();
            dialog.tampil2(e.message);
        });
    }

    async load(): Promise<void> {
        this. _projectAr = await db.project.bacaSemua();
    }

    render(itemAr: IProjectObj[]): void {

        this.view.cont.innerHTML = '';
        itemAr.forEach((itemObj: IProjectObj) => {
            let item: Item = new Item();
            item.itemObj = itemObj;
            item.attach(this.view.cont);
            item.elHtml.onclick = () => {
                this.itemOnClick(item);
            }
        });
    }

    itemOnClick(item: Item): void {
        menuPopup.bersih();
        menuPopup.tampil([
            {
                label: 'hapus',
                f: () => {
                    menuPopup.sembunyi();
                    db.project.hapus(item.itemObj.id);
                    this.loadRender();
                }
            },
            {
                label: 'edit detail',
                f: () => {

                    menuPopup.sembunyi();
                    // this._view.detach();
                    let nama: string = window.prompt('Nama:', item.itemObj.nama);

                    loading.tampil();
                    db.project.edit(item.itemObj.id, item.itemObj).then(() => {
                        loading.detach();
                    }).catch((e) => {
                        loading.detach();
                        console.log(e)
                        dialog.tampil2(e.message);
                    });

                    //local quick update
                    item.itemObj.nama = nama;
                    item.judulP.innerHTML = nama;
                }
            },
            {
                label: 'edit files',
                f: () => {
                    this.editItem(item.itemObj);
                }
            }
        ]);
    }

    editItem(item:IProjectObj):void {
        menuPopup.sembunyi();
        this._view.detach();
        daftarFile.selesai = () => {
            this._view.attach(document.body);
        }
        daftarFile.tampil(document.body, item.id);
    }

}

class Item extends BaseComponent {
    private _itemObj: IProjectObj;

    constructor() {
        super();
        this._template = `
        <div class='item'>
            <p class='judul'></p>
        </div>`;
        this.build();
        this._elHtml.onclick = (e: MouseEvent) => {
            e.stopPropagation();
            //TODO: buat menu
        }
    }

    get judulP(): HTMLParagraphElement {
        return this.getEl('div.item p.judul') as HTMLParagraphElement;
    }

    public get itemObj(): IProjectObj {
        return this._itemObj;
    }
    public set itemObj(value: IProjectObj) {
        this._itemObj = value;
        this.judulP.innerHTML = value.nama;
    }

}

class View extends BaseComponent {

    constructor() {
        super();
        this._template = `
            <div class='daftar-page'>
                <div class='header'>
                    <button class='tutup'>X</button>
                    <div class='judul'>Daftar Project</div>
                    <button class='menu'>+</button> 
                </div>
                <div class='daftar-cont'>
                </div>
            </div>
        `;
        this.build();
    }

    get tutupTbl(): HTMLButtonElement {
        return this.getEl('button.tutup') as HTMLButtonElement;
    }

    get menutbl(): HTMLButtonElement {
        return this.getEl('button.menu') as HTMLButtonElement;
    }

    get cont(): HTMLDivElement {
        return this.getEl('div.daftar-cont') as HTMLDivElement;
    }
}

export var daftarProject: DaftarProject = new DaftarProject();