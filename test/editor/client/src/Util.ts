// declare var md5: Function;

export class Util {


	static getUrl(url: string, params: string[]): string {
		let urlHasil: string = url;

		console.log('get url');

		params.forEach((item: string) => {
			urlHasil = urlHasil.replace(/\:[a-z]+/, item);
			console.log('item ' + item);
			console.log('url ' + urlHasil);
		});

		return urlHasil;
	}

	static escape(str: string): string {
		let hasil: string = str;

		while (hasil.indexOf("<") > -1) {
			hasil = hasil.replace("<", "&lt;");
		}

		while (hasil.indexOf(">") > -1) {
			hasil = hasil.replace(">", "&gt;");
		}

		return hasil;
	}

	// private static error(code:number, message:string): string {
	// 	return JSON.stringify({
	// 		code:code,
	// 		message:message
	// 	});
	// }


	/**
	 * Ajax tanpa loading
	 * @param type 
	 * @param url 
	 * @param dataStr 
	 */
	static async Ajax(type: string, url: string, dataStr: string): Promise<string> {
		return new Promise((resolve: any, reject: any) => {
			try {
				console.group('send data');
				console.log(dataStr);
				console.groupEnd();

				let xhr: XMLHttpRequest = new XMLHttpRequest();

				xhr.onload = () => {
					if (200 == xhr.status) {
						resolve(xhr.responseText);
					}
					else {
						console.log('response error');
						reject(new CError(xhr.status,xhr.statusText));
					}
				};

				xhr.onerror = (e: any) => {
					console.log('xhr error');
					console.log(e);
					reject(new CError(500, e.message));
				}

				xhr.open(type, url, true);
				xhr.setRequestHeader('Content-type', 'application/json');

				xhr.send(dataStr);
			}
			catch (e) {
				console.log('Util error');
				console.log(e);
				reject(new CError(500, e.message));
			}

		});
	}

}

export class CError extends Error {
	private _code: Number;
	public get code(): Number {
		return this._code;
	}

	constructor(code:number, msg:string) {
		super(msg);
		this._code = code;
	}
}