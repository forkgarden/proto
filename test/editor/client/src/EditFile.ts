import { BaseComponent } from "./BaseComponent.js";
import { db } from "./db/Db.js";
import { dialog } from "./Dialog.js";
import { loading } from "./Loading.js";

class EditFile {
    private _view: View = new View();
    private copy:FileObj= {};
    private editMode:boolean = false;
    private _selesai: Function;

    public set selesai(value: Function) {
        this._selesai = value;
    }
    public get view(): View {
        return this._view;
    }

    constructor() {
        this._view.simpanTbl.onclick = (e:MouseEvent) => {
            e.stopPropagation();
            try {
                loading.tampil();
                this.formSubmit().then(() => {
                    this._view.detach();
                    loading.detach();
                    this._selesai();
                }).catch((e) => {
                    console.log(e);
                    loading.detach();
                    dialog.tampil2(e.message);
                });
            }
            catch (e) {
                loading.detach();
                console.log(e);
                dialog.tampil2(e.message);
            }
        }

        this._view.tutupTbl.onclick = (e:MouseEvent) => {
            e.stopPropagation();
            this._view.detach();
            this._selesai();
        }
    }

    async formSubmit():Promise<any> {
        console.log('form submit');
        this.copy.nama = this._view.namaInput.value;
        this.copy.konten = this._view.kontent.value;
        this.copy.tipe = this._view.typeInput.value;
        this.copy.urutan = this._view.urutanInput.value;

        console.log(this.copy);

        if (this.editMode) {
            console.log('simpan edit');
            await db.fileData.edit(this.copy.id, this.copy);
        }
        else {
            console.log('file data baru');
            await db.fileData.baru(this.copy);
        }

    }

    tampil(file:FileObj, dataAsli:FileObj, editMode:boolean):void {
        this._view.attach(document.body);
        
        this.copy.projectId = dataAsli.projectId;
        this.copy.id = dataAsli.id;
        this.copy.nama = dataAsli.nama
        this.copy.konten = dataAsli.konten;
        this.copy.tipe = dataAsli.tipe;
        this.copy.urutan = dataAsli.urutan;

        this.editMode = editMode;

        if (file.nama && file.nama != '') this.copy.nama = file.nama;
        if (file.konten && file.konten != '') this.copy.konten = file.konten; 
        if (file.tipe && file.tipe != '') this.copy.tipe = file.tipe;
        if (file.urutan && file.urutan != '') this.copy.urutan = file.urutan;
 
        this._view.namaInput.value = this.copy.nama;
        this._view.kontent.value = this.copy.konten;
        this._view.typeInput.value = this.copy.tipe;
        this._view.urutanInput.value = this.copy.urutan;
    }
}

class View extends BaseComponent {
    constructor() {
        super();
        this._template = `
            <div class='edit-file'>
                <div class='header'>
                    <button class='tutup'>X</button>
                    <div class='judul'>Edit File</div>
                    <button class='simpan'>simpan</button> 
                </div>
                <div class='cont'>
                    <form>
                        <label>Nama File:</label>
                        <input type='text' class='nama'>
                        <label>Type:</label>
                        <input type='text' class='type'>
                        <label>urutan:</label>
                        <input type='text' class='urutan'>
                        <label>Konten:</label>
                        <textarea class='content'></textarea>
                    </form>
                </div>
            </div>          
        `;
        this.build();
    }

    get namaInput():HTMLInputElement {
        return this.getEl('input.nama') as HTMLInputElement;
    }

    get urutanInput():HTMLInputElement {
        return this.getEl('input.urutan') as HTMLInputElement;
    }

    get typeInput():HTMLInputElement {
        return this.getEl('input.type') as HTMLInputElement;
    }

    get kontent():HTMLTextAreaElement {
        return this.getEl('textarea.content') as HTMLTextAreaElement;
    }

    get form():HTMLFormElement {
        return this.getEl('form') as HTMLFormElement;
    }

    get tutupTbl():HTMLButtonElement {
        return this.getEl('button.tutup') as HTMLButtonElement;
    }

    get simpanTbl():HTMLButtonElement {
        return this.getEl('button.simpan') as HTMLButtonElement;
    }
}

export var editFile:EditFile = new EditFile();