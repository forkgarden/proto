import { BaseComponent } from "./BaseComponent.js";
import { dialog } from "./Dialog.js";
import { loading } from "./Loading.js";

class Upload {
    private _view: View = new View();
    public get view(): View {
        return this._view;
    }

    private _fileObj: FileObj = {};
    public get fileObj(): FileObj {
        return this._fileObj;

    }

    private _selesai: (f: FileObj) => void;
    public set selesai(value: (f: FileObj) => void) {
        this._selesai = value;
    }

    constructor() {
        this._view.input.onchange = (e: Event) => {
            e.stopPropagation();

            console.log('input on change');
            let file: File = (e.target as HTMLInputElement).files[0];

            console.log('file:');
            console.log(file);

            this._fileObj.nama = (file && file.name) ? file.name : '';

            console.log('nama:')
            console.log(this._fileObj.nama);

            loading.tampil();
            this.bacaFile(file).then((c:string) => {
                loading.detach();
                this._fileObj.konten = c;
                this._view.detach();
                this._selesai(this._fileObj);
            }).catch((e) => {
                console.log(e)
                dialog.tampil2(e.message);
                loading.detach();
            });
        }
    }

    tampil():void {
        this._view.attach(document.body);
    }

    async bacaFile(f: File): Promise<string> {
        return new Promise((resolve, reject) => {
            try {
                const reader: FileReader = new FileReader();
                reader.addEventListener('load', (event:any) => {
                    resolve(event.target.result as string);
                });
                reader.onerror = (e) => {
                    console.log(e);
                    reject(e);
                }
                reader.readAsText(f);
            }
            catch(e) {
                console.log(e);
                reject(e.message);
            }
        })
    }
}

class View extends BaseComponent {


    constructor() {
        super();
        this._template = `
            <div class='upload'>
                <input type='file'>;
            </div>
        `;
        this.build();

    }

    get input(): HTMLInputElement {
        return this.getEl('input') as HTMLInputElement;
    }
}


export var upload: Upload = new Upload();