import { BaseComponent } from "./BaseComponent.js";
import { db } from "./db/Db.js";
import { dialog } from "./Dialog.js";

class Login extends BaseComponent {
    private _selesai:Function;

    constructor() {
        super();
        this._template = `
            <div class='form-login'>

                <form class='form-login' action="" method="post">

                    <div class="form-group">
                        <label for="user-name">User:</label>
                        <input type="text" class="form-control user-name" name="user-name" id="user-name" required
                            value="auni" />
                    </div>

                    <div class="form-group">
                        <label for="password">Deskripsi:</label>
                        <input type="password" class="form-control password" name="password" id="password" required
                            value="12345" />
                    </div>
                    <button type="submit" class="btn btn-primary submit">Login</button>
                </form>

            </div>
        `;
        this.build();

        this.form.onsubmit = () => {
            try {
                db.auth.login(this.username.value, this.password.value).then(() => {
                    this.detach();
                    this._selesai();
                }).catch((e) => {
                    console.log(e);
                    dialog.tampil2(e.message);
                });
            }
            catch (e) {
                console.log(e);
                dialog.tampil2(e.message);
            }
        }
    }

    get username():HTMLInputElement {
        return this.getEl('user-name') as HTMLInputElement;
    }

    get password():HTMLInputElement {
        return this.getEl('password') as HTMLInputElement;
    }

    get form():HTMLFormElement {
        return this.getEl('form') as HTMLFormElement;
    }

}

export var login:Login = new Login();