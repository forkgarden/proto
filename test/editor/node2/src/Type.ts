interface IProject {
	nama: string;
	jsExclude: string[];
	file: {
		gbr?: IFile[],
		js: IFile[],
		css: IFile[],
		html: IFile[]
	}
}

interface IFile {
	nama: string;
	url: string;
	konten?: string;
	kontenEdit?: string;
	ui?: {
		el?: HTMLDivElement;
		tbl?: HTMLButtonElement;
	}
}

class CError extends Error {
	private _code: Number;
	public get code(): Number {
		return this._code;
	}

	constructor(code: number, msg: string) {
		super(msg);
		this._code = code;
	}
}