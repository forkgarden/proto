import express from "express";
import { fileRouter } from "./router/FileRouter";

class AppCloudinary {
	login(): void {
		//TODO: login ke cloudinary
	}

	router(app: express.Express): void {
		app.use('cdn/file', fileRouter);
	}
}

export var appCloudinary: AppCloudinary = new AppCloudinary();