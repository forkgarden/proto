import { config, ISetting } from "./Config";
import { configDisk } from "./entity/ConfigDisk";

class ConfigController {
	constructor() {

	}

	async update2DbSemua(): Promise<void> {
		let items: ISetting[] = config.bacaSemua();
		for (let i: number = 0; i < items.length; i++) {
			await this.updateDb(items[i]);
		}
	}

	async ambilDariDbSemua(): Promise<void> {
		let items: ISetting[] = config.bacaSemua();

		for (let i: number = 0; i < items.length; i++) {
			await this.ambilDariDb(items[i].kunci);
		}
	}

	async ambilDariDb(key: string): Promise<void> {
		let item: ISetting = await configDisk.bacaKey(key);

		if (item) {
			let konfig: ISetting = config.getSetting(key);
			konfig.deskripsi = item.deskripsi;
			konfig.nilai = item.nilai;
		}
	}

	async updateDb(item: ISetting): Promise<void> {
		await configDisk.update({
			deskripsi: item.deskripsi,
			kunci: item.kunci,
			nilai: item.nilai
		});
	}
}

export var configController: ConfigController = new ConfigController();