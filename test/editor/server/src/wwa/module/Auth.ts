// import { logT } from "./TokoLog";
import express from "express";
import { session } from "./SessionData";
import { anggotaSql } from "./entity/Anggota";
import { IPengguna } from "./Type";
// import { Config, config } from "./Config";

class Auth {

	async login(userId: string, password: string): Promise<IPengguna> {
		// logT.log('Auth: login ');

		let hasil: IPengguna[] = await anggotaSql.login(userId, password);

		console.log('login');
		console.log('hasil');
		console.log(hasil);

		if (hasil.length == 0) {
			// logT.log('login gagal ' + userId + '/' + password);
			return null;
		}

		if (hasil[0].setuju == 0) {
			return null;
		}

		return {
			id: hasil[0].id,
			lapak: hasil[0].lapak,
			level: hasil[0].level,
			user_id: hasil[0].user_id,
			password: ''
		}
	}

}

export var auth: Auth = new Auth();

//check auth middle ware
export function checkAuth(req: express.Request, resp: express.Response, next: express.NextFunction) {
	if (!session(req).statusLogin) {
		resp.status(401).send('belum login');
	}
	else {
		next();
	}
}

export function checkAdminUser(req: express.Request, resp: express.Response, next: express.NextFunction) {
	let level: string = session(req).level;

	if (level != 'admin' && level != 'user') {
		resp.status(403).send('Perintah tidak diperkenankan');
		return;
	}

	next();
}

export function isAdmin(req: express.Request): boolean {
	if (session(req).level != 'admin') {
		return false;
	}
	return true;
}

//check auth middle ware
export function checkAdmin(req: express.Request, resp: express.Response, next: express.NextFunction) {

	if (session(req).level != 'admin') {
		resp.status(403).send('Perintah tidak diperkenankan');
		return;
	}

	next();

}


export function setCache(_req: express.Request, resp: express.Response, next: express.NextFunction) {
	resp.header("Cache-Control", "max-age=7201");
	next();
}