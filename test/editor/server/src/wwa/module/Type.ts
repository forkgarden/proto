//NOTE: FINAL
export interface IFile {
	id: string,
	thumb: string,
	gbr: string
}

export interface ICariResponse {
	jumlah: number,
	data: IBarangObj[]
}

export interface IBarangBaca {
	publish?: number,
	lapak_id?: string,
	id?: string,
	offset?: number,
	limit?: number,
	orderDateDesc?: number
	orderDateAsc?: number
	orderNamaAsc?: number
	kataKunci?: string
	toko_id?: string

	//generic
	kolom?: string;
	where?: string;
	tambahan?: string;
}

export interface IBarangObj {
	id?: string;
	nama?: string;
	deskripsi?: string;
	deskripsi_panjang?: string;
	harga?: string;
	wa?: string;
	publish?: number;
	// lapak?: string;
	last_view?: string;
	lapak_id?: string;
	toko_id?: string;


	//external
	file_id?: string;
	thumb?: string;
	gbr?: string;

	//request

}

export interface IPengguna {
	id?: string;
	user_id?: string;
	password?: string;
	level?: string;
	lapak?: string;
	deskripsi?: string;
	setuju?: number;
	toko_id?: string;
	wa?: string;
	alamat?: string;
	email?: string;
}

export interface IPenggunaBaca {
	id?: string;
	user_id?: string;
	password?: string;
	level?: string;
	lapak?: string;
	deskripsi?: string;
	setuju?: number;
	toko_id?: string;

	where?: string;
	tambahan?: string;
}

export interface IFileKosong {
	file: string,
	type: string,
	fileId: string
}

export interface IConfigDb {
	id?: string,
	kunci?: string,
	nilai?: string,
	deskripsi?: string
}