//NOTE: FINAL

export class Config {
	static readonly NAV_LAPAK: string = 'NODE_NAV_LAPAK';
	static readonly NAV_LOGIN: string = 'NODE_NAV_LOGIN';
	static readonly NAV_CARI: string = 'NODE_NAV_CARI';
	static readonly TERKAIT: string = 'NODE_TERKAIT';
	static readonly NAMA_TOKO: string = 'NODE_NAMA_TOKO';
	static readonly DESKRIPSI_TOKO: string = 'NODE_DESKRIPSI_TOKO';
	static readonly JML_PER_HAL: string = 'NODE_JML_PER_HAL';
	static readonly FOOTER: string = 'NODE_FOOTER';
	static readonly FIRE_BASE_CONFIG: string = 'NODE_FIRE_BASE_CONFIG';
	static readonly TOKO_ID: string = 'NODE_TOKO_ID';
	static readonly MODE_DEV: string = 'NODE_MODE_DEV';
	static readonly WEBSITE: string = 'NODE_WEB_SITE';

	private settingAr: ISetting[] = [];

	constructor() {
		this.settingAr = [
			{
				kunci: Config.NAV_LAPAK,
				nilai: '0',
				deskripsi: 'Tampilkan navigasi lapak di halaman depan'
			},
			{
				kunci: Config.TERKAIT,
				nilai: '0',
				deskripsi: 'Aktifkan barang terkait'
			},
			{
				kunci: Config.NAMA_TOKO,
				nilai: 'Auni Store',
				deskripsi: 'Nama toko'
			},
			{
				kunci: Config.JML_PER_HAL,
				nilai: '25',
				deskripsi: 'Jumlah item per halaman'
			},
			{
				kunci: Config.FOOTER,
				nilai: `<H3>Auni Store</H3>Perum Taman Melati Blok FE 07 Bojong Sari - Sawangan - Depok<br/><br/>`,
				deskripsi: 'Footer'
			},
			{
				kunci: Config.NAV_LOGIN,
				nilai: `1`,
				deskripsi: 'Tampilkan menu login'
			},
			{
				kunci: Config.NAV_CARI,
				nilai: `1`,
				deskripsi: 'Tampilkan menu Pencarian'
			},
			{
				kunci: Config.FIRE_BASE_CONFIG,
				nilai: ``,
				deskripsi: 'firebase config'
			},
			{
				kunci: Config.TOKO_ID,
				nilai: '1',
				deskripsi: 'id dari toko applikasi'
			},
			{
				kunci: Config.MODE_DEV,
				nilai: '0',
				deskripsi: 'mode development atau produksi'
			},
			{
				kunci: Config.WEBSITE,
				nilai: 'http://warungwa.hagarden.xyz',
				deskripsi: 'web site buat share'
			},
			{
				kunci: Config.DESKRIPSI_TOKO,
				nilai: 'Warungnya kita kita',
				deskripsi: 'deskripsi toko'
			}


		]
	}

	bacaSemua(): ISetting[] {
		return this.settingAr;
	}

	updateNilai(key: string, nilai: string): void {
		this.getSetting(key).nilai = nilai;
	}

	getSetting(key: string): ISetting {
		for (let i: number = 0; i < this.settingAr.length; i++) {
			if (this.settingAr[i].kunci == key) {
				return this.settingAr[i];
			}
		}

		return null;
	}

	getNilai(key: string): string {
		for (let i: number = 0; i < this.settingAr.length; i++) {
			if (this.settingAr[i].kunci == key) {
				return this.settingAr[i].nilai;
			}
		}

		throw new Error('key tidak ketemu: ' + key);
	}

}

export interface ISetting {
	kunci: string,
	deskripsi: string,
	nilai: string
}

export var config: Config = new Config();