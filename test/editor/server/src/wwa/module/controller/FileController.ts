import { logT } from "../TokoLog";
import fs from "fs";
import { fileSql } from "../entity/File";

class FileController {
	async baru(gbrBesarNama: string, gbrKecilNama: string, dataBesar: string, dataKecil: string): Promise<any> {
		let buf: Buffer;
		let folderUnggah: string = './wwa/public/upload/';
		let downloadUrlBesar: string;
		let downloadUrlKecil: string;

		downloadUrlBesar = '/upload/' + gbrBesarNama;
		downloadUrlKecil = '/upload/' + gbrKecilNama;

		//simpan gbr besar
		buf = Buffer.from(dataBesar, 'base64');
		await this.tulisFile(folderUnggah + gbrBesarNama, buf);
		logT.log('file written ' + folderUnggah + gbrBesarNama);

		//simpan gambar kecil
		buf = Buffer.from(dataKecil, 'base64');
		await this.tulisFile(folderUnggah + gbrKecilNama, buf);
		logT.log('file written ' + folderUnggah + gbrKecilNama);

		let _rows: any = await fileSql.baru(downloadUrlBesar, downloadUrlKecil);

		return {
			baris: _rows
		}


	}

	async tulisFile(p: string, data: any): Promise<void> {
		console.log('tulis file');
		console.log(p);
		return new Promise((resolve, reject) => {
			fs.writeFile(p, data, (err) => {
				if (err) {
					reject(err);
				}
				else {
					resolve();
				}
			});
		})
	}


}

export var fileController: FileController = new FileController();