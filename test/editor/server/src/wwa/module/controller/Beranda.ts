// import { config } from "winston";
import { Config, config } from "../Config";
// import { anggotaSql } from "../entity/Anggota";
import { barangSql } from "../entity/BarangSql";
import { render } from "../render/Renderer";
import { IBarangObj } from "../Type";

class BerandaController {
	async beranda(): Promise<string> {

		let data: IBarangObj[] = await barangSql.baca({
			publish: 1,
			limit: parseInt(config.getNilai(Config.JML_PER_HAL)),
			orderDateAsc: 1
		})

		// let lapak: IPengguna = await anggotaSql.bacaId()

		return render.halDepan.render({
			barangData: data,
			lapakId: '',
			hal: 0,
			jml: 0,
			kataKunci: ''
		});
	}

	async cariBarang(kataKunci: string, hal: string, lapakId: string): Promise<string> {
		let daftarBarang: IBarangObj[];
		let jumlahData: IBarangObj[];
		let jumlah: number;
		let hal2: number = parseInt(hal) * parseInt(config.getNilai(Config.JML_PER_HAL));

		jumlahData = await barangSql.baca({
			kataKunci: decodeURI(kataKunci),
			publish: 1,
			lapak_id: lapakId
		});
		jumlah = jumlahData.length;

		daftarBarang = await barangSql
			.baca({
				kataKunci: decodeURI(kataKunci),
				publish: 1,
				offset: hal2,
				orderDateAsc: 1,
				limit: parseInt(config.getNilai(Config.JML_PER_HAL)),
				lapak_id: lapakId
			});

		let html: string = await render.halDepan.render({
			barangData: daftarBarang,
			hal: parseInt(hal),
			jml: jumlah,
			kataKunci: kataKunci,
			lapakId: lapakId
		})

		return html
	}
}

export var berandaController: BerandaController = new BerandaController();