import { Config, config } from "../Config";
import { anggotaSql } from "../entity/Anggota";
import { barangSql } from "../entity/BarangSql";
import { IBarangBaca, IBarangObj, IPengguna } from "../Type";
import { util } from "../Util";
import { RenderUtil } from "./Util";

export class HalBarang {

	private _util: RenderUtil;

	async render(id: string, lapakId: string): Promise<string> {

		let opt: IBarangBaca = {
			id: id
		}
		let barang: IBarangObj[] = await barangSql.baca(opt);

		let index: string = await util.getFile("wwa/view/index.html");
		let header: string = await util.getFile("wwa/view/header_comp.html");
		let barangStr: string = await this.renderBarangDetail(barang[0]);
		let js: string = await util.getFile("wwa/view/item-page/js_hal_item_comp.html");

		//OG
		index = this.renderOg(index, lapakId, barang);

		header = header.replace("{{nama_toko}}", config.getNilai(Config.NAMA_TOKO));
		header = header.replace("{{motto}}", "");

		index = index.replace("{{header}}", header);
		index = index.replace("{{cari}}", "");

		index = this.renderNav(index, lapakId);

		let anggota: IPengguna;
		if (lapakId != '') {
			anggota = await anggotaSql.bacaById(lapakId)
			index = await this._util.renderInfoLapak(index, anggota.lapak, anggota.deskripsi);
		}
		else {
			index = index.replace("{{info-lapak}}", '');
		}


		index = index.replace("{{info}}", "");

		index = index.replace("{{content}}", barangStr);
		index = index.replace("{{js}}", js);
		index = index.replace("{{halaman}}", "")
		index = index.replace("{{daftar-barang-cont-class}}", "daftar-barang-cont");


		// index = index.replace("{{footer}}", config.getNilai(Config.FOOTER));
		index = index.replace("{{footer}}", `<p>${anggota.lapak} : ${anggota.alamat} </p>`);

		index = this._util.cache(index, util.randId);

		return index;
	}

	renderOg(index: string, lapakId: string, barang: IBarangObj[]): string {
		index = index.replace("{{og_deskripsi}}", barang[0].deskripsi);
		index = index.replace("{{og_deskripsi}}", barang[0].deskripsi);
		index = index.replace("{{og_site_name}}", config.getNilai(Config.NAMA_TOKO))

		index = index.replace("{{judul_web}}", config.getNilai(Config.NAMA_TOKO))

		index = index.replace("{{og_url}}", config.getNilai(Config.WEBSITE) + "/lapak/" + lapakId + "/barang/" + barang[0].id + "/ref/" + util.randId);

		index = index.replace("{{og_title}}", barang[0].nama);
		index = index.replace("{{og_gambar}}", config.getNilai(Config.WEBSITE) + barang[0].gbr);

		return index;
	}

	renderNav(index: string, lapak: string): string {
		index = index.replace("{{nav_hal_utama}}", this._util.renderNavTokoUtama);
		index = index.replace("{{nav_beranda}}", this._util.renderNavBeranda(lapak));
		index = index.replace("{{nav_daftar_lapak}}", "");
		index = index.replace("{{nav_login}}", "");
		return index
	}

	private async renderBarangDetail(barang: IBarangObj): Promise<string> {
		let index: string = await util.getFile("wwa/view/item-page/item-page_comp.html");
		let hasil: string = '';

		hasil = index.replace("{{gbrBesar}}", barang.gbr ? barang.gbr : "/");

		hasil = hasil.replace("{{nama}}", barang.nama);
		hasil = hasil.replace("{{harga}}", barang.harga);
		hasil = hasil.replace("{{deskripsiPanjang}}", barang.deskripsi_panjang);
		hasil = hasil.replace("{{wa-link}}", util.buatWa(barang.wa, barang.nama));
		hasil = hasil.replace("{{data-id}}", barang.id);

		return hasil;
	}

	public set util(value: RenderUtil) {
		this._util = value;
	}



}
