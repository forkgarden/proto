import { util } from "../Util";
import { Config, config } from "../Config";
import { IPengguna } from "../Type";
import { RenderUtil } from "./Util";

export class HalDaftarLapak {
	private _renderUtil: RenderUtil;

	async render(opt: IRenderHalDaftarLapak): Promise<string> {

		// console.log('render halaman daftar lapak');
		// console.log(opt);

		let index: string = await util.getFile("wwa/view/index.html");
		let header: string = await util.getFile("wwa/view/header_comp.html");
		let lapakStr: string = await this.renderLapak(opt.lapakData);
		let lapakDeskripsi: string = '';

		opt.lapakData.forEach((item: IPengguna) => {
			if (item.id == opt.lapakId) {
				lapakDeskripsi = item.deskripsi;
			}
		})

		lapakStr = `<h2>Daftar Lapak: </h2>` + lapakStr;

		//OG
		index = index.replace("{{og_site_name}}", config.getNilai(Config.NAMA_TOKO))
		index = index.replace("{{judul_web}}", config.getNilai(Config.NAMA_TOKO))

		if (opt.lapakId && opt.lapakId != '') {
			index = index.replace("{{og_deskripsi}}", lapakDeskripsi);
			index = index.replace("{{og_deskripsi}}", lapakDeskripsi);
			index = index.replace("{{og_gambar}}", "");
			index = index.replace("{{og_url}}", config.getNilai(Config.WEBSITE) + "/lapak/" + opt.lapakId);
		}
		else {
			index = index.replace("{{og_deskripsi}}", config.getNilai(Config.DESKRIPSI_TOKO));
			index = index.replace("{{og_deskripsi}}", config.getNilai(Config.DESKRIPSI_TOKO));
			index = index.replace("{{og_gambar}}", "");
			index = index.replace("{{og_url}}", config.getNilai(Config.WEBSITE));
		}

		header = header.replace("{{nama_toko}}", config.getNilai(Config.NAMA_TOKO));
		header = header.replace("{{motto}}", "");

		index = index.replace("{{nav_hal_utama}}", '');
		index = index.replace("{{nav_beranda}}", this._renderUtil.renderNavBeranda(opt.lapakId));
		index = index.replace("{{nav_daftar_lapak}}", "");
		index = index.replace("{{nav_login}}", ``);

		//info jika data kosong
		index = index.replace("{{info}}", "");
		index = index.replace("{{info-lapak}}", "")

		index = index.replace("{{cari}}", "");
		index = index.replace("{{header}}", header);
		index = index.replace("{{content}}", lapakStr);
		index = index.replace("{{js}}", "");
		index = index.replace("{{halaman}}", "");
		index = index.replace("{{daftar-barang-cont-class}}", "daftar-barang-cont-lapak");
		index = index.replace("{{footer}}", config.getNilai(Config.FOOTER));

		index = this._renderUtil.cache(index, util.randId);

		// console.log('render hal daftar lapak selesai');

		return index;
	}

	async renderLapak(lapakAr: IPengguna[]): Promise<string> {
		let view: string = await util.getFile("wwa/view/daftar_lapak_comp.html");
		let hasil: string = '';

		lapakAr.forEach((item: IPengguna) => {
			let view2: string = view;

			view2 = view2.replace("{{url}}", "/lapak/" + item.id);
			view2 = view2.replace("{{nama}}", item.lapak);
			view2 = view2.replace("{{deskripsi}}", item.deskripsi ? item.deskripsi : ' - deskripsi tidak tersedia - ');

			hasil += view2;
		});


		if (hasil == '') {
			return `<p>Tidak ada lapak yang terdaftar</p>`;
		}



		return hasil;
	}

	public set renderUtil(value: RenderUtil) {
		this._renderUtil = value;
	}


}

export interface IRenderHalDaftarLapak {
	lapakData: IPengguna[],
	lapakId: string,
	hal: number,
	jml: number
}