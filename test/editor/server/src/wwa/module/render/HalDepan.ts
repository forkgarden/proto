import { util } from "../Util";
import { Config, config } from "../Config";
import { IBarangObj, IPengguna } from "../Type";
import { anggotaSql } from "../entity/Anggota";
import { RenderUtil } from "./Util";

export class HalDepan {

	private _util: RenderUtil;
	public set util(value: RenderUtil) {
		this._util = value;
	}

	async render(opt: IRenderHalDepan): Promise<string> {
		let index: string = await util.getFile("wwa/view/index.html");
		let header: string = await util.getFile("wwa/view/header_comp.html");
		let js: string = await util.getFile("wwa/view/js_comp.html");
		let cari: string = await util.getFile("wwa/view/cari_comp.html");
		let barang: string;
		let halaman: string = await this.renderHalaman1(opt.hal, opt.jml, opt.kataKunci);

		let anggota: IPengguna;
		if (opt.lapakId != '') {
			anggota = await anggotaSql.bacaById(opt.lapakId);
			index = await this._util.renderInfoLapak(index, anggota.lapak, anggota.deskripsi);
			barang = await this.renderBerandaBarang(opt.barangData, false, '');
		}
		else {
			barang = await this.renderBerandaBarang(opt.barangData, false, '');
			index = index.replace("{{info-lapak}}", '');
		}

		index = await this.renderOg(index, opt);

		header = header.replace("{{nama_toko}}", config.getNilai(Config.NAMA_TOKO));
		header = header.replace("{{motto}}", "");

		index = this.renderNav(index, opt, cari);

		//info jika data kosong
		index = this.renderInfo(index, opt);

		index = index.replace("{{header}}", header);
		index = index.replace("{{content}}", barang);
		index = index.replace("{{js}}", js);
		index = index.replace("{{halaman}}", halaman);
		index = index.replace("{{daftar-barang-cont-class}}", "daftar-barang-cont");

		if (opt.lapakId) {
			index = index.replace("{{footer}}", `<p>${anggota.lapak} : ${anggota.alamat} </p>`);
		}
		else {
			index = index.replace("{{footer}}", config.getNilai(Config.FOOTER));
		}

		//cache
		index = this._util.cache(index, util.randId);

		return index;
	}

	renderInfo(index: string, opt: IRenderHalDepan): string {
		if (opt.barangData.length == 0) {
			if (!opt.kataKunci || opt.kataKunci == '') {
				index = index.replace("{{info}}", "Belum ada barang yang dijual");
				index = index.replace("{{class_info}}", "isi");
			}
			else {
				index = index.replace("{{info}}", "Pencarian tidak menemukan hasil");
				index = index.replace("{{class_info}}", "isi");
			}
		}
		else {
			if (opt.kataKunci && opt.kataKunci != '') {
				index = index.replace("{{info}}", "Hasil pencarian dengan kata kunci: " + opt.kataKunci);
				index = index.replace("{{class_info}}", "isi");
			}
			else {
				index = index.replace("{{info}}", "");
				index = index.replace("{{class_info}}", "");
			}
		}

		return index;
	}

	renderNav(index: string, opt: IRenderHalDepan, cari: string): string {

		if (config.getNilai(Config.NAV_CARI) == '1' && opt.lapakId == '') {
			cari = cari.replace("{{lapak}}", opt.lapakId);
			index = index.replace("{{cari}}", cari);
		}
		else {
			index = index.replace("{{cari}}", "");
		}

		//navigasi hal utama
		if (opt.kataKunci && opt.kataKunci != '') {
			index = index.replace("{{nav_hal_utama}}", "");
		}
		else {
			index = index.replace("{{nav_hal_utama}}", this._util.renderNavTokoUtama(opt.lapakId) + " ");
		}

		//navigasi beranda
		if (opt.kataKunci && opt.kataKunci != '') {
			index = index.replace("{{nav_beranda}}", this._util.renderNavBeranda(opt.lapakId));
		}
		else {
			index = index.replace("{{nav_beranda}}", "");
		}

		//navigasi lapak
		if (config.getNilai(Config.NAV_LAPAK) == '1') {
			if (opt.kataKunci && opt.kataKunci != '') {
				index = index.replace("{{nav_daftar_lapak}}", "");
			}
			else {
				index = index.replace("{{nav_daftar_lapak}}", this._util.renderNavDaftarLapak(opt.lapakId));
			}
		}
		else {
			index = index.replace("{{nav_daftar_lapak}}", "");
		}

		if (config.getNilai(Config.NAV_LOGIN) == "1") {
			index = index.replace("{{nav_login}}", `<a href='/admin'>LOGIN</a>`);
		}
		else {
			index = index.replace("{{nav_login}}", ``);
		}

		return index;
	}

	async renderOg(index: string, opt: IRenderHalDepan): Promise<string> {
		index = index.replace("{{og_site_name}}", config.getNilai(Config.NAMA_TOKO))
		index = index.replace("{{judul_web}}", config.getNilai(Config.NAMA_TOKO))

		if (opt.lapakId && opt.lapakId != '') {
			let lapak: IPengguna = await anggotaSql.bacaById(opt.lapakId);
			index = index.replace("{{og_deskripsi}}", lapak.deskripsi);
			index = index.replace("{{og_deskripsi}}", lapak.deskripsi);
			index = index.replace("{{og_gambar}}", "");
			index = index.replace("{{og_url}}", config.getNilai(Config.WEBSITE) + "/lapak/" + opt.lapakId);
			index = index.replace("{{og_title}}", lapak.lapak);
		}
		else {
			index = index.replace("{{og_deskripsi}}", config.getNilai(Config.DESKRIPSI_TOKO));
			index = index.replace("{{og_deskripsi}}", config.getNilai(Config.DESKRIPSI_TOKO));
			index = index.replace("{{og_gambar}}", "");
			index = index.replace("{{og_url}}", config.getNilai(Config.WEBSITE));
			index = index.replace("{{og_title}}", config.getNilai(Config.NAMA_TOKO));
		}

		return index;
	}

	//
	private async renderHalaman1(hal: number, jml: number, kataKunci: string): Promise<string> {
		let halaman: string = await util.getFile("wwa/view/halaman1_comp.html");
		let url: string = "/cari/" + kataKunci + "/";
		let url2: string = '';

		if (0 == hal) {
			return '';
		}

		if (0 == jml) {
			return '';
		}

		url2 = url + "1";
		halaman = halaman.replace("{{pertama}}", "<a href='" + url2 + "'>%lt%lt</a > ");

		url2 = url + (((hal - 1) > 0) ? (hal - 1) : "1");
		halaman = halaman.replace("{{sebelumnya}}", "<a href='" + url2 + "'>%lt</a> ");

		halaman = halaman.replace("{{hal}}", hal + "");
		halaman = halaman.replace("{{jumlah}}", jml + " ");

		url2 = url + (((hal + 1) < jml) ? (hal + 1) : jml);
		halaman = halaman.replace("{{selanjutnya}}", "<a href='" + url2 + "'>%gt</a> ");

		url2 = url + jml;
		halaman = halaman.replace("{{terakhir}}", "<a href='" + url2 + "'>%gt%gt</a> ");

		return halaman;
	}

	async renderBerandaBarang(barangData: any[], depan: boolean, namaLapak: string): Promise<string> {
		let view: string = await util.getFile("wwa/view/item_comp.html");
		let hasil: string = '';

		barangData.forEach((item: IBarangObj) => {
			let url: string = '/barang/' + item.id;
			let hasil2: string = '';

			url = '/lapak/' + item.lapak_id + '/barang/' + item.id;

			hasil2 = (view.replace("{{nama}}", item.nama));
			hasil2 = hasil2.replace("{{url}}", url);
			hasil2 = (hasil2.replace("{{deskripsi}}", ""));
			hasil2 = (hasil2.replace("{{deskripsiPanjang}}", item.deskripsi_panjang));
			hasil2 = (hasil2.replace("{{harga}}", (depan ? namaLapak : item.harga + '')));
			hasil2 = hasil2.replace("{{wa}}", item.wa)
			hasil2 = hasil2.replace("{{wa-link}}", util.buatWa(item.wa, item.nama))
			hasil2 = hasil2.replace("{{gbrThumb}}", (item.thumb != null) ? item.thumb : '/gambar/kosong.png');
			hasil2 = hasil2.replace("{{gbrBesar}}", item.gbr);
			hasil2 = hasil2.replace("{{id}}", item.id);
			hasil2 = hasil2.replace("{{lapak}}", item.lapak_id);
			hasil += hasil2;

		});

		if (hasil == '') {
			hasil = "<p>Belum Ada Barang</p>"
		}

		return hasil;
	}

}

export interface IRenderHalDepan {
	barangData: IBarangObj[],
	lapakId: string,
	// lapakNama: string,
	// lapakDeskripsi: string,
	hal: number,
	jml: number,
	kataKunci: string
}