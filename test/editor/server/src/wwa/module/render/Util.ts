import { util } from "../Util";

export class RenderUtil {

	renderNavTokoUtama(lapakId: string): string {
		if (lapakId != '') {
			return `<a href="/">HAL DEPAN </a> | `;
		}
		else {
			return '';
		}
	}

	renderNavBeranda(lapakId: string): string {
		let url: string = "/";
		if (lapakId != '') {
			url = '/lapak/' + lapakId;
		}
		else {
			url = '/';
		}

		return `<a href="${url}">BERANDA</a> `;
	}

	renderNavDaftarLapak(lapakId: string): string {

		let lapakUrl: string = "/lapak/daftar";

		if (lapakId != '') {
			lapakUrl = `/lapak/${lapakId}/daftar`;
		}

		return `<a href="${lapakUrl}">DAFTAR LAPAK</a> `;
	}

	async renderInfoLapak(index: string, lapakNama: string, lapakDeskripsi: string): Promise<string> {
		let infoLapak: string = await util.getFile('wwa/view/info_lapak_comp.html');

		infoLapak = infoLapak.replace('{{nama-lapak}}', lapakNama);
		infoLapak = infoLapak.replace('{{deskripsi-lapak}}', lapakDeskripsi);

		index = index.replace("{{info-lapak}}", infoLapak);

		return index;
	}


	cache(index: string, rand: string): string {
		index = index.replace("{{cache}}", rand);
		index = index.replace("{{cache}}", rand);
		index = index.replace("{{cache}}", rand);
		index = index.replace("{{cache}}", rand);
		index = index.replace("{{cache}}", rand);
		index = index.replace("{{cache}}", rand);
		index = index.replace("{{cache}}", rand);
		index = index.replace("{{cache}}", rand);
		index = index.replace("{{cache}}", rand);
		index = index.replace("{{cache}}", rand);
		index = index.replace("{{cache}}", rand);
		index = index.replace("{{cache}}", rand);

		return index;
	}

}