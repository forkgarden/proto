// import fs from "fs";
// import { barangSql } from "../entity/BarangSql";
// import { config } from "../Config";
// import { BarangObj } from "../Type";
// import { util } from "../Util";
import { HalDepan } from "./HalDepan";
import { HalBarang } from "./Halbarang";
import { HalDaftarLapak } from "./HalLapakDaftar";
import { RenderUtil } from "./Util";


class Renderer {
	private _halDepan: HalDepan = new HalDepan();
	private _halBarang: HalBarang = new HalBarang();
	private _halDaftarLapak: HalDaftarLapak = new HalDaftarLapak();
	private util: RenderUtil = new RenderUtil();

	constructor() {
		this._halBarang.util = this.util;
		this.halDaftarLapak.renderUtil = this.util;
		this._halDepan.util = this.util;
	}

	public get halDaftarLapak(): HalDaftarLapak {
		return this._halDaftarLapak;
	}

	public get halDepan(): HalDepan {
		return this._halDepan;
	}

	public get halBarang(): HalBarang {
		return this._halBarang;
	}

}

export var render: Renderer = new Renderer();