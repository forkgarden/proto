import express, { Router } from "express";
// import { logT } from "../TokoLog";
import { barangSql } from "../entity/BarangSql";
import { render } from "../render/Renderer";
import { Config, config } from "../Config";
import { IBarangObj, IPengguna } from "../Type";
import { session } from "../SessionData";
import { anggotaSql } from "../entity/Anggota";

export var lapakRouter = express.Router();
var router: Router = lapakRouter;

router.get("/daftar", (_req: express.Request, resp: express.Response) => {
	try {
		anggotaSql.query(anggotaSql.daftarLapak, [config.getNilai(Config.TOKO_ID)])
			.then((data: IPengguna[]) => {
				return render.halDaftarLapak.render({
					lapakData: data,
					hal: 0,
					jml: 0,
					lapakId: ""
				});
			})
			.then((data: string) => {
				// session(_req).lapak = '';
				resp.status(200).send(data);
			})
			.catch((err) => {
				// logT.log(err);
				console.error;
				resp.status(500).send(err.message);
			});
	}
	catch (err) {
		// logT.log(err);
		console.error;
		resp.status(500).send(err.message);
	}
})

router.get("/:id/daftar", (req: express.Request, resp: express.Response) => {
	try {
		anggotaSql.query(anggotaSql.daftarLapak, [config.getNilai(Config.TOKO_ID)])
			.then((data: IPengguna[]) => {
				return render.halDaftarLapak.render({
					lapakData: data,
					hal: 0,
					jml: 0,
					lapakId: req.params.id
				});
			})
			.then((data: string) => {
				resp.status(200).send(data);
			})
			.catch((err) => {
				// logT.log(err);
				console.error;
				resp.status(500).send(err.message);
			});
	}
	catch (err) {
		// logT.log(err);
		console.error;
		resp.status(500).send(err.message);
	}
});

router.get("/:id", (_req: express.Request, resp: express.Response) => {
	console.log('render lapak ' + _req.params.id);
	try {
		barangSql
			.baca({
				lapak_id: _req.params.id,
				publish: 1,
				orderDateAsc: 1
			})
			.then((data: IBarangObj[]) => {
				return render.halDepan
					.render({
						barangData: data,
						lapakId: _req.params.id,
						hal: parseInt(_req.params.hal),
						jml: parseInt(config.getNilai(Config.JML_PER_HAL)),
						kataKunci: _req.params.kunci
					})
			})
			.then((data: string) => {
				session(_req).lapak = _req.params.id;
				resp.status(200).send(data);
			})
			.catch((err) => {
				// logT.log(err);
				console.log('error 1');
				console.log(err);
				console.error;
				resp.status(500).send(err.message);
			});

	} catch (err) {
		// logT.log(err);
		console.log('error 2');
		console.log(err);
		console.error;
		resp.status(500).send(err.message);
	}
});

router.get("/:id/cari/:kunci/hal/:hal", (_req: express.Request, resp: express.Response) => {
	try {
		barangSql
			.baca({
				lapak_id: _req.params.id,
				kataKunci: decodeURI(_req.params.kunci),
				publish: 1,
				offset: parseInt(_req.params.hal),
				orderDateAsc: 1,
				limit: 25
			})
			.then((data: IBarangObj[]) => {
				return render.halDepan.render({
					barangData: data,
					lapakId: _req.params.id,
					hal: parseInt(_req.params.hal),
					jml: parseInt(config.getNilai(Config.JML_PER_HAL)),
					kataKunci: _req.params.kunci
				});
			})
			.then((data: string) => {
				resp.status(200).send(data);
			})
			.catch((err) => {
				// logT.log(err);
				console.error;
				resp.status(500).send(err.message);
			});
	}
	catch (err) {
		// logT.log(err);
		console.error;
		resp.status(500).send(err.message);
	}
})

//TODO: digabung di controller
router.get("/:id/barang/:barangId/ref/:ref", (_req: express.Request, resp: express.Response) => {
	try {
		render.halBarang.render(_req.params.barangId, _req.params.id)
			.then((data: string) => {
				resp.status(200).send(data);
			})
			.catch((err) => {
				console.error;
				resp.status(500).send(err.message);
			});

	} catch (err) {
		console.error;
		resp.status(500).send(err.message);
	}
})

router.get("/:id/barang/:barangId", (_req: express.Request, resp: express.Response) => {
	try {
		render.halBarang.render(_req.params.barangId, _req.params.id)
			.then((data: string) => {
				resp.status(200).send(data);
			})
			.catch((err) => {
				console.error;
				resp.status(500).send(err.message);
			});

	} catch (err) {
		console.error;
		resp.status(500).send(err.message);
	}
})