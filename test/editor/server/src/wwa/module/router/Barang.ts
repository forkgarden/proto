import express from "express";
import { barangSql } from "../entity/BarangSql";
import { checkAuth } from "../Auth";
import { util } from "../Util";
import { IBarangBaca, IBarangObj } from "../Type";
import { config, Config } from "../Config";
import { session } from "../SessionData";

export var router = express.Router();

router.get("/test", (req: express.Request, resp: express.Response) => {
	try {
		resp.status(200).end('ok test');
	}
	catch (err) {
		resp.status(500).send(err);
	}
});

router.post("/hapus/:id", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		barangSql.hapus(req.params.id)
			.then(() => {
				resp.status(200).end();
			}).catch((e) => {
				// logT.log(e);
				console.error;
				resp.status(500).send(e.message);
			});
	}
	catch (err) {
		resp.status(500).send(err);
	}
});

router.post("/baru", checkAuth, (req: express.Request, resp: express.Response) => {
	try {

		//TODO:
		//check apakah anggota boleh membuat barang, sudah disetujui

		console.log('barang baru');
		let data: IBarangObj =
		{
			nama: req.body.nama,
			deskripsi_panjang: req.body.deskripsi_panjang,
			deskripsi: req.body.deskripsi,
			harga: req.body.harga,
			wa: req.body.wa,
			file_id: req.body.file_id,
			publish: req.body.publish,
			lapak_id: req.body.lapak_id,
			last_view: util.buatDate()
		};

		barangSql.baru(data)
			.then((data: any) => {
				console.log('barang baru sql response:');
				console.log(data);
				resp.status(200).send(data.insertId);
			}).catch((e) => {
				// logT.log(e);
				console.error;
				resp.status(500).send(e.message);
			});
	}
	catch (e) {
		resp.status(500).send(e);
	}
});

router.post("/clone/:id", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		let data: IBarangObj =
		{
			id: req.params.id
		};

		barangSql.baca(data).then((h: IBarangObj[]) => {
			if (h.length > 0) {
				let item: IBarangObj = h[0];
				return barangSql.baru({
					deskripsi_panjang: item.deskripsi_panjang,
					harga: item.harga,
					lapak_id: item.lapak_id,
					nama: item.nama,
					publish: item.publish,
					wa: item.wa
				});
			}
			else {
				return Promise.resolve();
			}
		}).then(() => {
			resp.status(200).send('');
		}).catch((e) => {
			resp.status(500).send(e.message);
		});
	}
	catch (e) {
		resp.status(500).send(e);
	}
});

router.post("/update/lastview/:id", (req: express.Request, resp: express.Response) => {
	try {
		// logT.log('update terakhir dilihat, id ' + req.params.id);
		barangSql.updateLastViewDate(req.params.id)
			.then(() => {
				resp.status(200).end();
			}).catch((e) => {
				console.error;
				resp.status(500).send(e.message)
			});

	}
	catch (error) {
		console.error;
		resp.status(500).send(error.message);
	}
});

router.post("/update/:id", checkAuth, (req: express.Request, resp: express.Response) => {
	try {

		if (session(req).id != req.body.lapak_id && (session(req).level == 'user')) {
			resp.status(404).send('Invalid');
			return;
		}

		barangSql.update({
			nama: req.body.nama,
			deskripsi_panjang: req.body.deskripsi_panjang,
			deskripsi: req.body.deskripsi,
			harga: req.body.harga,
			wa: req.body.wa,
			file_id: req.body.file_id,
			publish: req.body.publish,
			last_view: util.buatDateLama(),
			lapak_id: req.body.lapak_id
		}, req.params.id)
			.then(() => {
				resp.status(200).end();
			}).catch((e) => {
				resp.status(500).send(e)
			});

	}
	catch (error) {
		resp.status(500).send(error);
	}
});

router.post("/baca/lapak/:lapak/publish/:publish", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		barangSql.baca({
			lapak_id: req.params.lapak,
			publish: parseInt(req.params.publish)
		})
			.then((rows: any) => {
				resp.status(200).send(rows);
			})
			.catch((e) => {
				// logT.log(e);
				console.error;
				resp.status(500).send(e.message);
			});
	}
	catch (e) {
		resp.status(500).send(e);
	}

})

router.post("/baca/", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		let param: IBarangBaca = req.body as any;
		barangSql.baca({
			id: param.id,
			kataKunci: param.kataKunci,
			lapak_id: param.lapak_id,
			limit: param.limit,
			offset: param.offset,
			orderDateDesc: param.orderDateDesc,
			orderNamaAsc: param.orderNamaAsc,
			publish: param.publish
		})
			.then((rows: any) => {
				resp.status(200).send(rows);
			})
			.catch((e) => {
				// logT.log(e);
				console.error;
				resp.status(500).send(e.message);
			});
	}
	catch (e) {
		resp.status(500).send(e);
	}

})

router.post("/terkait", (_req: express.Request, resp: express.Response) => {
	try {
		if (config.getNilai(Config.TERKAIT) == '0') {
			resp.status(200).send([]);
			return;
		}

		barangSql.query(barangSql.bacaBarangTerkait, [config.getNilai(Config.TOKO_ID)])
			.then((hasil: IBarangObj[]) => {
				resp.status(200).send(hasil);
			}).catch((e) => {
				resp.status(500).send(e.message);
			});
	}
	catch (e) {
		resp.status(500).send(e.message);
	}
})
