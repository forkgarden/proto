import express from "express";
import { checkAuth } from "../Auth";
import { config } from "../Config";
// import { configController } from "../ConfigController";
import { configDisk } from "../entity/ConfigDisk";
// import { configSql } from "../entity/ConfigSql";
import { logT } from "../TokoLog";

export var configRouter = express.Router();
var router = configRouter;

//TOOD: dep
router.get("/simpan", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		resp.status(200).send('ok');
	}
	catch (err) {
		logT.log(err);
		resp.status(500).send(err.message);
	}
});


router.post("/baca", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		resp.status(200).send(config.bacaSemua());
	}
	catch (err) {
		logT.log(err);
		resp.status(500).send(err.message);
	}
});


router.post("/update/:id", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		configDisk.update(req.body)
			.then(() => {
				resp.status(200).send('');
			}).catch((err) => {
				logT.log(err);
				resp.status(500).send(err.message);
			});
	}
	catch (err) {
		logT.log(err);
		resp.status(500).send(err.message);
	}
})

//TOOD: dep
router.post("/reload", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		resp.status(200).send('');
	}
	catch (e) {
		logT.log(e);
		resp.status(500).send(e.message);
	}
})