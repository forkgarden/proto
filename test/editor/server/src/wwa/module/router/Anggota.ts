import express from "express";
import { checkAdmin, checkAdminUser, checkAuth, isAdmin } from "../Auth";
import { anggotaSql } from "../entity/Anggota";
import { barangSql } from "../entity/BarangSql";
import { session } from "../SessionData";
import { IPengguna } from "../Type";

export var router = express.Router();


router.post("/hapus/:id", checkAuth, checkAdmin, (req: express.Request, resp: express.Response) => {
	try {

		barangSql.hapusByLapakId(req.params.id).then(() => {
			return anggotaSql.hapus(req.params.id);
		}).then(() => {
			resp.status(200).send('');
		}).catch((e) => {
			resp.status(500).send(e.message);
		});

	}
	catch (err) {
		resp.status(500).send(err.message);
	}
});

router.post("/baru", (req: express.Request, resp: express.Response) => {
	try {
		let data: IPengguna =
		{
			password: req.body.password,
			level: "user",
			user_id: req.body.user_id,
			lapak: req.body.lapak,
			deskripsi: req.body.deskripsi,
			wa: req.body.wa,
			alamat: req.body.alamat,
		};

		anggotaSql.baru(data)
			.then((data: any) => {
				resp.status(200).send(data.insertId + '');
			}).catch((e: any) => {
				console.error;
				console.log('data');
				console.log(data);
				// console.log(e.errno);
				resp.status(500).send(e.message);
			});
	}
	catch (e) {
		resp.status(500).send(e.message);
	}
});

router.post("/update", checkAuth, checkAdminUser, (req: express.Request, resp: express.Response) => {
	try {

		if (!isAdmin(req) && session(req).user_id != req.body.user_id) {
			resp.status(403).send('Perintah tidak diperkenankan');
			return;
		}

		let opt: IPengguna = {}

		if (req.body.deskripsi) opt.deskripsi = req.body.deskripsi;
		if (req.body.lapak) opt.lapak = req.body.lapak;
		if (req.body.password) opt.password = req.body.password;
		if (req.body.setuju) opt.setuju = req.body.setuju;
		if (req.body.user_id) opt.user_id = req.body.user_id;
		if (req.body.wa) opt.wa = req.body.wa;
		if (req.body.alamat) opt.alamat = req.body.alamat;

		anggotaSql.update(opt, req.body.id).then(() => {
			resp.status(200).end();
		}).catch((e: any) => {
			resp.status(500).send(e.message)
		});

	}
	catch (error) {
		resp.status(500).send(error.message);
	}
});

router.post("/update/password", checkAuth, checkAdminUser, (req: express.Request, resp: express.Response) => {
	try {

		if (!isAdmin(req) && session(req).user_id != req.body.user_id) {
			resp.status(403).send('Perintah tidak diperkenankan');
			return;
		}

		let opt: IPengguna = {
			password: req.body.password,
			id: req.body.id
		}

		//validate 
		if (!opt.password || opt.password == '') {
			resp.status(500).send('Password tidak boleh kosong');
			return;
		}

		anggotaSql.update(opt, opt.id).then(() => {
			resp.status(200).end();
		}).catch((e: any) => {
			resp.status(500).send(e.message)
		});

	}
	catch (error) {
		resp.status(500).send(error.message);
	}
});

router.post("/update/id/:id/setuju/:setuju", checkAuth, checkAdmin, (req: express.Request, resp: express.Response) => {
	try {
		let opt: IPengguna = {
			setuju: parseInt(req.params.setuju),
		}

		anggotaSql.update(opt, req.params.id).then(() => {
			resp.status(200).end();
		}).catch((e: any) => {
			console.error;
			resp.status(500).send(e.message)
		});

	}
	catch (error) {
		resp.status(500).send(error.message);
	}
});

router.post("/baca/setuju/:setuju", checkAuth, checkAdmin, (req: express.Request, resp: express.Response) => {
	try {

		if (session(req).level != 'admin') {
			resp.status(403).send('Perintah tidak diperkenankan')
		}

		anggotaSql.bacaBySetuju((req.params.setuju)).then((hasil: any) => {
			resp.status(200).send(hasil);
		}).catch((e) => {
			resp.status(500).send(e.message);
		});
	}
	catch (e) {
		console.log(e);
		resp.status(200).send(e.message);
	}
});

router.post("/baca/:id", (req: express.Request, resp: express.Response) => {
	try {
		anggotaSql.bacaById(req.params.id).then((h) => {
			resp.status(200).send(h);
		}).catch((e) => {
			console.error;
			resp.status(500).send(e.message);
		});
	}
	catch (err) {
		console.error;
		resp.status(500).send(err.message);
	}
});
