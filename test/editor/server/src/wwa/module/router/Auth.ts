import express from "express";
import { auth } from "../Auth";
import { session } from "../SessionData";
import { IPengguna } from "../Type";

export var router = express.Router();

router.post("/login", (req: express.Request, resp: express.Response) => {
	try {

		auth.login(req.body.user_id, req.body.password).then((h: IPengguna) => {
			if (h) {
				session(req).level = h.level;
				session(req).statusLogin = true;
				session(req).lapak = h.lapak;
				session(req).id = h.id;
				session(req).user_id = h.user_id;
				resp.status(200).send(h);
			}
			else {
				req.session = null;
				resp.status(401).send('username/password salah');
			}
		}).catch((e) => {
			req.session = null;
			// logT.log(e);
			console.error;
			resp.status(501).send(e.message);
		});
	}
	catch (e) {
		req.session = null;
		// logT.log(e);
		console.error;
		resp.status(502).send(e.message);
	}
});

router.post("/status", (req: express.Request, resp: express.Response) => {
	try {
		let status: IPengguna = {
			id: session(req).id,
			level: session(req).level,
			lapak: session(req).lapak,
			user_id: session(req).user_id,
			password: ''
		}
		if (session(req).statusLogin) {
			resp.status(200).send(status);
		}
		else {
			req.session = null;
			resp.status(401).send('belum login');
		}
	}
	catch (e) {
		// logT.log(e);
		console.error;
		resp.status(500).send(e.message);
	}
});

router.get("/logout", (req: express.Request, resp: express.Response) => {
	try {
		// logT.log('logout')
		req.session = null;
		resp.redirect('/admin');
	}
	catch (e) {
		// logT.log(e);
		console.error;
		resp.status(500).send(e.message);
	}
});



