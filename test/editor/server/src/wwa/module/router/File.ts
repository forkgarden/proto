import express from "express";
import { Connection } from "../Connection";
// import fs from "fs";
import { checkAuth } from "../Auth";
import { logT } from "../TokoLog";
import { fileSql } from "../entity/File";
import { fileController } from "../controller/FileController";

export var router = express.Router();

router.post("/baca", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		//TODO:
		Connection.pool;
		resp.status(200).send('');
	}
	catch (err) {
		logT.log('error');
		resp.status(500).send(err);
	}
});

router.get("/baca/:id", checkAuth, (req: express.Request, resp: express.Response) => {
	//TODO: file admin
	resp.status(200).send();
})

router.post("/baru", checkAuth, (req: express.Request, resp: express.Response) => {
	try {

		fileController.baru(
			req.body.gbr_besar_nama,
			req.body.gbr_kecil_nama,
			req.body.gbr_besar.split(',')[1],
			req.body.gbr_kecil.split(',')[1])
			.then((h) => {
				console.log(h);
				resp.status(200).send(h);
			})
			.catch((e) => {
				console.error;
				console.log(e);
				resp.status(500).send(e.message);
			});
	}
	catch (e) {
		logT.log(e);
		resp.status(500).send(JSON.stringify(e));
	}
});

router.post('/baca/file/kosong', checkAuth, (req: express.Request, resp: express.Response) => {
	try {

	}
	catch (e) {

	}
})

router.post("/baca/disk/kosong", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		fileSql.bacaDiskKosong().then((item: any[]) => {
			resp.status(200).send('');
		}).catch((e) => {
			console.log(e);
			resp.status(500).send(e);
		});
	}
	catch (e) {
		console.log(e);
		resp.status(500).send(e);
	}
});

router.post("/hapus/:id", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		console.log('file hapus ' + req.params.id);

		fileSql.hapus(req.params.id).then(() => {
			resp.status(200).send('');
		}).catch((e) => {
			console.log(e);
			resp.status(500).send(e);
		});

	}
	catch (e) {
		console.log(e);
		resp.status(500).send(e);
	}
});