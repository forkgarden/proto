import express, { Router } from "express";
import { server } from "../../../App";
import { Connection } from "../Connection";
import { logT } from "../TokoLog";
import { checkAuth } from "../Auth";
import { util } from "../Util";
import { berandaController } from "../controller/Beranda";
import { Config, config } from "../Config";

export var berandaRouter: Router = express.Router();

berandaRouter.get("/cari/:kunci/hal/:hal", (_req: express.Request, resp: express.Response) => {
	try {
		berandaController.cariBarang(decodeURI(_req.params.kunci), _req.params.hal, '')
			.then((data: string) => {
				resp.status(200).send(data);
			}).catch((err) => {
				logT.log(err);
				resp.status(500).send(err.message);
			});


		// barangSql
		// 	.baca({
		// 		kataKunci: decodeURI(_req.params.kunci),
		// 		publish: 1,
		// 		offset: parseInt(_req.params.hal),
		// 		orderDateAsc: 1,
		// 		limit: parseInt(config.getNilai(Config.JML_PER_HAL))
		// 	})
		// 	.then((data: IBarangObj[]) => {
		// 		return render.halDepan.render({
		// 			barangData: data,
		// 			lapakId: '',
		// 			hal: parseInt(_req.params.hal),
		// 			jml: parseInt(config.getNilai(Config.JML_PER_HAL)),
		// 			kataKunci: _req.params.kunci
		// 		});
		// 	})
		// 	.then((data: string) => {
		// 		resp.status(200).send(data);
		// 	})
		// 	.catch((err) => {
		// 		logT.log(err);
		// 		resp.status(500).send(err.message);
		// 	});
	}
	catch (err) {
		logT.log(err);
		resp.status(500).send(err.message);
	}
})

berandaRouter.get("/", (_req: express.Request, resp: express.Response) => {
	try {
		berandaController.beranda()
			.then((data: string) => {
				resp.status(200).send(data);
			})
			.catch((err) => {
				logT.log(err);
				resp.status(500).send(err.message);
			});
	}
	catch (err) {
		logT.log(err);
		resp.status(500).send(err.message);
	}
})

berandaRouter.get("/daftar", (_req: express.Request, resp: express.Response) => {
	try {
		util.getFile('wwa/view/anggota_daftar.html').then((h: string) => {
			h = h.replace("{{cache}}", util.randId);
			h = h.replace("{{toko}}", config.getNilai(Config.NAMA_TOKO));
			resp.status(200).send(h);
		}).catch((e) => {
			resp.status(500).send(e.message);
		})
	}
	catch (e) {
		resp.status(500).send(e.message);
	}
})

berandaRouter.get("/admin", (_req: express.Request, resp: express.Response) => {
	try {
		util.getFile('wwa/view/admin.html').then((h: string) => {
			h = h.replace("{{cache}}", util.randId);
			h = h.replace("{{cache}}", util.randId);
			h = h.replace("{{cache}}", util.randId);
			h = h.replace("{{cache}}", util.randId);
			h = h.replace("{{cache}}", util.randId);
			h = h.replace("{{cache}}", util.randId);
			resp.status(200).send(h);
		}).catch((e) => {
			resp.status(500).send(e.message);
		})
	}
	catch (e) {
		resp.status(500).send(e.message);
	}
})

berandaRouter.get("/test", (_req: express.Request, resp: express.Response) => {
	try {
		util.getFile('wwa/view/test.html').then((h: string) => {
			h = h.replace("{{cache}}", util.randId);
			h = h.replace("{{cache}}", util.randId);
			h = h.replace("{{cache}}", util.randId);
			h = h.replace("{{cache}}", util.randId);
			h = h.replace("{{cache}}", util.randId);
			h = h.replace("{{cache}}", util.randId);
			resp.status(200).send(h);
		}).catch((e) => {
			resp.status(500).send(e.message);
		})
	}
	catch (e) {
		resp.status(500).send(e.message);
	}
})

berandaRouter.get("/hapus-cache", checkAuth, (_req: express.Request, resp: express.Response) => {
	try {
		util.hapusCache();
	}
	catch (e) {
		resp.status(500).send(e);
	}
})

berandaRouter.get("/shutdown", (req: express.Request, resp: express.Response) => {
	try {
		logT.log('shutdown');

		resp.status(200).end();

		server.close((e) => {
			if (e) {
				logT.log('server close error');
				logT.log(e.message);
			} else {
				logT.log('server tutup');
			}
		})

		Connection.pool.end((err) => {
			if (err) {
				logT.log('sql shutdown error');
				logT.log(err.sqlMessage);
			}
			else {
				logT.log('connection tutup');
			}
		});

		//process.kill(process.pid, 'SIGTERM');
	} catch (e) {
		logT.log(e);
		resp.status(500).send(e);
	}
});




// export var beranda: Beranda = new Beranda();