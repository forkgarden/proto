import express from "express";
import { logT } from "../TokoLog";
export var router = express.Router();

router.get("/logm/:msg", (req: express.Request, resp: express.Response) => {
	try {
		logT.log(req.params.msg);
		resp.status(200).send(req.params.msg);
	}
	catch (e) {
		console.log(e);
		resp.status(200).send(e);
	}
});

router.get("/logAmbil", (req: express.Request, resp: express.Response) => {
	try {
		resp.status(200).send(logT.ambil());
	}
	catch (e) {
		console.log(e);
		resp.status(200).send(e);
	}
});

router.use((_req: express.Request, _resp: express.Response, _next: Function) => {
	logT.log(_req.path);
	logT.log('404');
	_resp.status(404).send('Halaman Tidak Ditemukan');
})
