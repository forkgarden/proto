import express from "express";
import { checkAuth } from "../Auth";
import { Connection } from "../Connection";
import { logT } from "../TokoLog";
// import { server } from "../App";

export var router: express.Router = express.Router();

var queryTabelFile: string = `CREATE TABLE IF NOT EXISTS FILE ( 
								id INT NOT NULL AUTO_INCREMENT , 
								thumb VARCHAR(255) NOT NULL , 
								gbr VARCHAR(255) NOT NULL , 
								PRIMARY KEY (id)) ENGINE = InnoDB;
							`;

function jalankanQuery(query: string, data: any[] = []): void {
	logT.log('jalankan query:');
	logT.log(query);

	Connection.pool.query(query, data,
		(_err: any, _rows: any) => {
			if (_err) {
				throw new Error(_err);
			}
			else {
				return;
			}
		});

}

function createTableBarang(): void {
	logT.log('buat table barang');
	Connection.pool.query(
		`CREATE TABLE IF NOT EXISTS BARANG(
			id INT NOT NULL AUTO_INCREMENT,
			nama TINYTEXT,
			deskripsi TEXT,
			deskripsi_panjang TEXT,
			file_id INT,
			gbr_url TEXT,
			harga TINYTEXT,
			WA TINYTEXT,
			PRIMARY KEY (id))`,
		(_err: any, _rows: any) => {
			if (_err) {
				throw new Error(_err);
			}
			else {
				return;
			}
		});
}

function createDb(): void {
	logT.log('buat database');

	Connection.pool.query(`
		CREATE DATABASE IF NOT EXISTS toko;
		USE toko;
	`, (_err: any, _rows: any) => {
		if (_err) {
			throw new Error(_err);
		}
		else {
			return;
		}
	});
}

router.get("/hapusdb", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		resp.status(200).send('ok');
	} catch (e) {
		logT.log(e);
		resp.status(500).send(e);
	}
});

router.get("/setup", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		createDb();
		createTableBarang();
		jalankanQuery(queryTabelFile);
		resp.status(200).send('ok');
	} catch (e) {
		logT.log(e);
		resp.status(500).send(e);
	}
});

router.get("/backup", checkAuth, (req: express.Request, resp: express.Response) => {
	try {
		//TODO:
		//backup barang, file
		logT.log('backup belum selesai');
		resp.status(200).send('');
	} catch (e) {
		logT.log(e);
		resp.status(500).send(e);
	}
});

router.get("/shutdown", (req: express.Request, resp: express.Response) => {
	try {
		logT.log('shutdown');
		Connection.pool.end((err) => {
			logT.log(err.code + '/' + err.message);
		});
		resp.status(200).send('');
		// server.close(() => {
		// 	log.info('server close error');
		// })
		process.kill(process.pid, 'SIGTERM');
	} catch (e) {
		logT.log(e);
		resp.status(500).send(e);
	}
});
