import { ISetting } from "../Config";
import { Connection } from "../Connection";
import { IConfigDb } from "../Type";

class ConfigSql {
	async baca(): Promise<ISetting[]> {
		return new Promise((resolve, reject) => {
			Connection.pool.query(
				`SELECT * FROM KONFIG`, [],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}

	async insert(item: IConfigDb): Promise<void> {
		return new Promise((resolve, reject) => {
			Connection.pool.query(
				`INSERT INTO KONFIG SET ?`, item,
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}

	async bacaKey(id: string): Promise<IConfigDb[]> {
		return new Promise((resolve, reject) => {
			Connection.pool.query(
				`SELECT * FROM KONFIG WHERE kunci = ?`, [id],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}

	async update(data: IConfigDb, id: string): Promise<void> {
		return new Promise((resolve, reject) => {
			Connection.pool.query(
				`UPDATE KONFIG SET ? WHERE kunci = ?`, [data, id],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}
}

export var configSql: ConfigSql = new ConfigSql();