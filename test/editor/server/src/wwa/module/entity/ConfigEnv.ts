import { ISetting } from "../Config";
import { util } from "../Util";

class ConfigEnv {

	async bacaConfig(): Promise<ISetting[]> {
		try {
			let fileString: string = await util.getFileNoCache('config.json');
			let fileObj: ISetting[] = JSON.parse(fileString);

			return fileObj;
		} catch (e) {
			return [];
		}
	}

	async simpanConfig(fileString: string): Promise<void> {
		// console.log('simpan config ' + fileString);
		util.tulisKeFile('config.json', fileString);
	}

	async bacaKey(id: string): Promise<ISetting> {
		let setting: ISetting[] = await this.bacaConfig();

		for (let i: number = 0; i < setting.length; i++) {
			if (setting[i].kunci == id) {
				return setting[i];
			}
		}

		return null;
	}

	async update(data: ISetting): Promise<void> {
		let setting: ISetting[] = await this.bacaConfig();

		for (let i: number = 0; i < setting.length; i++) {
			if (setting[i].kunci == data.kunci) {
				setting[i].nilai = data.nilai;
				await this.simpanConfig(JSON.stringify(setting));
				return;
			}
		}

		setting.push(data);
		await this.simpanConfig(JSON.stringify(setting));
	}
}

export var configEnv: ConfigEnv = new ConfigEnv();