import { Config, config } from "../Config";
import { Connection } from "../Connection";
import { IPengguna, IPengguna as IPenggunaBaca } from "../Type";
import { barangSql } from "./BarangSql";

class Anggota {

	readonly daftarLapak: string = `
		SELECT id, lapak, deskripsi
		FROM pengguna
		WHERE level = 'user'
		AND toko_id = ?
		AND setuju = 1
	`

	readonly table: string = 'pengguna';

	readonly kolom: string = `
	pengguna.id, 
	pengguna.user_id, 
	pengguna.level, 
	pengguna.lapak, 
	pengguna.deskripsi, 
	pengguna.setuju,
	pengguna.toko_id,
	pengguna.wa,
	pengguna.alamat,
	pengguna.email `;

	async nonAktifkanAnggota(id: string): Promise<void> {
		return new Promise((resolve, reject) => {

			Connection.pool.query(
				`update pengguna set setuju = 2 WHERE id = ?`,
				[id],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						console.log(_rows);
						resolve(_rows);
					}
				});
		});
	}

	async login(username: string, pass: string): Promise<IPengguna[]> {
		let query: string = `
			SELECT id, user_id, lapak, level 
			FROM pengguna
			WHERE user_id = ?
			AND password = ?
			AND setuju = 1
			AND toko_id = ${config.getNilai(Config.TOKO_ID)}
		`;

		console.log('login, user name ' + username + '/pass ' + pass);


		return new Promise((resolve, reject) => {
			Connection.pool.query(
				query, [username, pass],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}

	async bacaBySetuju(setuju: string): Promise<IPengguna[]> {
		let hasil: IPengguna[];

		hasil = await this.query(
			`SELECT ${this.kolom}
			FROM ${this.table}
			WHERE pengguna.setuju = ?
			AND pengguna.toko_id = ?
		`, [setuju, config.getNilai(Config.TOKO_ID)]);

		if (hasil) return hasil;

		return null;
	}

	async bacaById(id: string): Promise<IPengguna> {
		let hasil: IPengguna[];

		hasil = await this.query(
			`SELECT 
				pengguna.id, 
				pengguna.user_id, 
				pengguna.level, 
				pengguna.lapak, 
				pengguna.deskripsi, 
				pengguna.setuju,
				pengguna.toko_id,
				pengguna.wa,
				pengguna.alamat,
				pengguna.email
			FROM pengguna
			WHERE pengguna.id = ?
			AND pengguna.toko_id = ?
		`, [id, config.getNilai(Config.TOKO_ID)]);

		if (hasil) return hasil[0];

		return null;
	}

	//TODO: dep
	async baca(opt: IPenggunaBaca): Promise<IPenggunaBaca[]> {

		let whereQuery: string = 'WHERE 1 ';
		let data: any[] = [];
		let kolom: string = `
			pengguna.id, 
			pengguna.user_id, 
			pengguna.level, 
			pengguna.lapak, 
			pengguna.deskripsi, 
			pengguna.setuju,
			pengguna.toko_id,
			pengguna.wa,
			pengguna.alamat,
			pengguna.email `;

		if (opt.id) {
			whereQuery += 'AND pengguna.id = ? ';
			data.push(opt.id);
		}

		if (opt.user_id) {
			whereQuery += 'AND pengguna.user_id = ? ';
			data.push(opt.user_id);
		}

		if (opt.password) {
			whereQuery += 'AND pengguna.password = ? ';
			data.push(opt.password);
		}

		if (!isNaN(opt.setuju)) {
			whereQuery += 'AND pengguna.setuju = ? '
			data.push(opt.setuju);
		}

		if (opt.level) {
			whereQuery += 'AND pengguna.level = ? '
			data.push(opt.level);
		}

		whereQuery += ' AND pengguna.toko_id = ? ';
		data.push(config.getNilai(Config.TOKO_ID));

		let query: string = ` SELECT ${kolom} FROM pengguna ${whereQuery}`;

		return new Promise((resolve, reject) => {
			Connection.pool.query(query, data,
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}

	async query(query: string, data: any[]): Promise<IPenggunaBaca[]> {
		return new Promise((resolve, reject) => {
			Connection.pool.query(
				query, data,
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}

	async baru(data: IPengguna): Promise<any> {
		return new Promise((resolve, reject) => {

			data.toko_id = config.getNilai(Config.TOKO_ID);

			Connection.pool.query(
				`INSERT INTO pengguna SET ?`,
				data,
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						// console.log(_rows);
						resolve(_rows);
					}
				});
		});
	}

	async update(data: IPenggunaBaca, id: string): Promise<any> {
		return new Promise((resolve, reject) => {
			Connection.pool.query(
				"UPDATE pengguna SET ? WHERE ID = ?",
				[
					data,
					id
				],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});

	}

	async hapus(id: string): Promise<any> {

		await barangSql.hapusByLapakId(id);

		return new Promise((resolve, reject) => {
			Connection.pool.query(
				"DELETE FROM pengguna WHERE ID = ?", [id],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}

}

export var anggotaSql: Anggota = new Anggota();
