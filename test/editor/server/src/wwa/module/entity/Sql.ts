import { Connection } from "../Connection";
import { IBarangObj } from "../Type";

class Sql {
	async query(query: string, data: any[]): Promise<IBarangObj[]> {
		return new Promise((resolve, reject) => {
			Connection.pool.query(
				query, data,
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}

}

export var sql: Sql = new Sql();