import { Config, config } from "../Config";
import { Connection } from "../Connection";
import { IBarangBaca, IBarangObj } from "../Type";
import { fileSql } from "./File";

class BarangSql {

	readonly bacaBarangTerkait: string = `
		SELECT BARANG.*, FILE.thumb, FILE.gbr
		FROM BARANG
		LEFT JOIN FILE
		ON BARANG.file_id = File.id
		WHERE 
			BARANG.publish = 1
			AND BARANG.toko_id = ?
		ORDER BY BARANG.last_view
		LIMIT 5 
	`;

	private hapusSql: string = `DELETE FROM BARANG WHERE ID = ?`;
	private updateSql: string = `UPDATE BARANG SET ? WHERE ID = ?`;
	private baruSql: string = `INSERT INTO BARANG SET ?`;

	async query(query: string, data: any[]): Promise<IBarangObj[]> {
		return new Promise((resolve, reject) => {
			Connection.pool.query(
				query, data,
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}

	async bacaById(id: string): Promise<IBarangObj> {
		let hasil: IBarangObj[];

		hasil = await this.baca({
			id: id
		});

		if (hasil && hasil.length > 0) {
			return hasil[0];
		}

		return null;
	}

	async baca(opt: IBarangBaca): Promise<IBarangObj[]> {
		let whereQuery: string = 'WHERE 1 ';
		let offsetQuery: string = '';
		let limitQuery: string = '';
		let orderQuery: string = '';
		let data: any[] = [];
		let kolom: string = '';

		// console.log('Barang baca ');
		// console.log(opt);

		if (opt.kolom) {
			kolom = opt.kolom;
		}
		else {
			kolom = " BARANG.*, FILE.thumb, FILE.gbr ";
		}

		if (opt.id) {
			whereQuery += 'AND BARANG.id = ? ';
			data.push(opt.id);
		}

		if (opt.lapak_id) {
			whereQuery += 'AND BARANG.lapak_id = ? ';
			data.push(opt.lapak_id);
		}

		if (opt.kataKunci) {
			whereQuery += `AND (BARANG.nama like ? OR BARANG.deskripsi_panjang like ?) `
			data.push('%' + opt.kataKunci + '%');
			data.push('%' + opt.kataKunci + '%');
		}

		if (opt.publish) {
			whereQuery += `AND BARANG.publish = ? `
			data.push(opt.publish);
		}

		whereQuery += ' AND BARANG.toko_id = ? ';
		data.push(config.getNilai(Config.TOKO_ID));

		if (!isNaN(opt.limit)) {
			limitQuery = 'LIMIT ? '
			data.push(opt.limit);
		}

		if (!isNaN(opt.offset)) {
			offsetQuery = 'OFFSET ? ';
			data.push(opt.offset);
		}

		if (opt.orderDateDesc) {
			orderQuery = 'ORDER BY last_view DESC ';
		}
		else if (opt.orderNamaAsc) {
			orderQuery = 'ORDER BY BARANG.nama ASC ';
		}
		else if (opt.orderDateAsc) {
			orderQuery = 'ORDER BY last_view ASC ';
		}

		let query: string = `
			SELECT ${kolom}
			FROM BARANG 
			LEFT JOIN FILE ON BARANG.file_id = FILE.id 
			${whereQuery} ${orderQuery} ${limitQuery}  ${offsetQuery}`;

		// console.log(query);

		return new Promise((resolve, reject) => {
			Connection.pool.query(
				query, data,
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
						// console.log(_rows);
					}
				});
		});
	}

	async hapusByLapakId(id: string): Promise<void> {
		console.log('hapus barang, id ' + id);

		return new Promise((resolve, reject) => {
			Connection.pool.query(
				`DELETE FROM BARANG WHERE lapak_id = ?`, [id],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}

	async hapus(id: string): Promise<any> {
		console.log('hapus barang, id ' + id);

		let barang: IBarangObj[] = await this.baca({
			id: id
		});

		await fileSql.hapus(barang[0].file_id).catch((e) => {
			console.log(e.message);
		})

		return new Promise((resolve, reject) => {
			Connection.pool.query(
				this.hapusSql, [id],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});
	}

	async updateLastViewDate(id: string): Promise<any> {
		return new Promise((resolve, reject) => {
			let query: string = `
				update BARANG set LAST_VIEW = NOW() where id = ?
			`;
			Connection.pool.query(
				query, [id],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});

	}

	async update(data: IBarangObj, id: string): Promise<any> {
		// console.log('barang update');
		// console.log(data);

		return new Promise((resolve, reject) => {
			Connection.pool.query(
				this.updateSql,
				[
					data,
					id
				],
				(_err: any, _rows: any) => {
					if (_err) {
						reject(_err);
					}
					else {
						resolve(_rows);
					}
				});
		});

	}

	async baru(data: IBarangObj): Promise<void> {
		data.toko_id = config.getNilai(Config.TOKO_ID);

		return new Promise((resolve, reject) => {
			Connection.pool.query(
				this.baruSql,
				data,
				(_err: any, _rows: any) => {
					if (_err) {
						console.error;
						reject(_err);
					}
					else {
						console.log(_rows);
						resolve(_rows);
					}
				});
		});
	}

}



export var barangSql: BarangSql = new BarangSql();