import mysql from "mysql";
import { logT } from "./TokoLog";
import { configDB } from "./ConfigDB"

export class Connection {
	private static _connection: mysql.Connection;
	private static _pool: mysql.Pool;

	public static get pool(): mysql.Pool {
		return Connection._pool;
	}

	public static get connection(): mysql.Connection {
		return this._connection;
	}

	static getPool(): Promise<mysql.PoolConnection> {
		logT.log('get pool');
		return new Promise((resolve, reject) => {
			Connection._pool.getConnection((err: mysql.MysqlError, connection: mysql.PoolConnection) => {
				if (err) {
					logT.log(err.code + '/' + err.message);
					reject(err.message);
				}
				else {
					resolve(connection);
				}
			})

		})
	}

	static connect() {
		logT.log('create connection 1');
		try {
			logT.log('create connection start');
			Connection._pool = mysql.createPool({
				host: configDB.host,
				user: configDB.user,
				password: configDB.pass,
				database: configDB.db,
				port: configDB.port,
				multipleStatements: true
			})
			logT.log('create connection end');
		} catch (e) {
			logT.log('create connection error');
			logT.log(e);
		}
		logT.log('create connection 2');
	}

	// static connect2(): void {
	// 	if (Connection._connection) {
	// 		logT.log('already connected ');
	// 		return;
	// 	}
	// 	Connection._connection = mysql.createConnection({
	// 		host: process.env.TOKO_DB_HOST,
	// 		user: process.env.TOKO_DB_USER,
	// 		password: process.env.TOKO_DB_PASS,
	// 		database: process.env.TOKO_DB_DB,
	// 		port: 3306,
	// 		multipleStatements: true
	// 	});
	// }
}

// export var connection: Conne