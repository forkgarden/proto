interface ISubQuery {
	nama?: string;
	sql?: ISql;
}

interface ILeftJoin {
	table?: ISubQuery;
	on?: string;
	kolom1?: string;
	opr?: string;
	kolom2: string;
}

interface IWhere {
	opr1?: string;
	kolom1?: string;
	opr2?: string;
	kolom2?: string;
}

interface ISql {
	kolom?: string[];
	table?: ISubQuery;
	leftJoin?: ILeftJoin[];
	where?: IWhere[];
	limit?: string;
	offset?: string;
}

class SqlBuilder {
	build(sql: ISql): string {
		let hasil: string = '';

		hasil += 'SELECT ';

		sql.kolom.forEach((item: string, idx: number) => {
			if (idx == 0) {
				hasil += item;
			}
			else {
				hasil += ' , ' + item;
			}
		})

		if (sql.table && sql.table.nama) {
			hasil += " from " + sql.table.nama + " ";
		}

		if (sql.leftJoin) {
			sql.leftJoin.forEach((item: ILeftJoin) => {
				if (item.table && item.table.nama) {
					hasil += ` left join ${item.table.nama} on  ${item.kolom1} ${item.opr} ${item.kolom2} `;
				}
			});
		}

		if (sql.where) {

		}

		if (sql.limit) {
			hasil += ` limit ${sql.limit} `;
		}

		if (sql.offset) {
			hasil += ` offset ${sql.offset} `;
		}

		return hasil;
	}
}

export var sqlBuilder = new SqlBuilder();