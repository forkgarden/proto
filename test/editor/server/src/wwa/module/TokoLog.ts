// import winston from "winston";

//NOTE: final
//TODO: d

// const logW: winston.Logger = winston.createLogger({
// 	level: 'info',
// 	format: winston.format.simple(),
// 	transports: [
// 		new winston.transports.File({ filename: ('./public/error.log'), level: 'error' }),
// 		new winston.transports.File({ filename: ('./public/combined.log') })
// 	]
// });

class LogM {
	private _logs: string[] = [];
	public get logs(): string[] {
		return this._logs;
	}

	log(msg: string): void {
		this._logs.push(msg);
		if (this._logs.length > 1000) {
			this._logs.shift();
		}
	}

	bersih(): void {
		this._logs = [];
	}

}

export const logM: LogM = new LogM();

class LogT {
	// private logWStatus: boolean = false;

	log(msg: any): void {
		logM.log(msg);
		// if (this.logWStatus) logW.info('test info');
		console.log(msg);
	}

	ambil(): string[] {
		return logM.logs;
	}
}

export const logT: LogT = new LogT();