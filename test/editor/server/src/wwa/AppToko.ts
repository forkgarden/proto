import express from "express";
import { router } from "./module/router/Barang";
import { router as fileRouter } from "./module/router/File";
import { router as authRouter } from "./module/router/Auth";
import { router as routerInstall } from "./module/router/Install";
import { router as routerTest } from "./module/router/TokoTest";
import { router as routerPengguna } from "./module/router/Anggota";
import { configRouter } from "./module/router/Config";
import { lapakRouter } from "./module/router/Lapak";
import { berandaRouter } from "./module/router/Beranda";
import { devRouter } from "./module/router/DevRouter";

class AppToko {

	router(app: express.Express): void {
		app.use("/barang", router);
		app.use("/file", fileRouter);
		app.use("/auth", authRouter);
		app.use("/sys", routerInstall);
		app.use("/toko_test", routerTest);
		app.use("/anggota", routerPengguna);
		app.use("/lapak", lapakRouter);
		app.use("/konfig", configRouter);
		app.use("/dev", devRouter);
		app.use("/", berandaRouter);
	}

}

export var appToko: AppToko = new AppToko();