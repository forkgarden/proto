import express from "express";
import { Connection } from "./wwa/module/Connection";
import { Server } from "http";
import { PoolConnection, MysqlError } from "mysql";
import { logT } from "./wwa/module/TokoLog";

const app: express.Express = express();
const port: number = 3009;
Connection.connect();

export const server: Server = app.listen(port, () => {
	logT.log("app started at port " + port);
});

async function testRead(pool: PoolConnection): Promise<void> {
	return new Promise((resolve, reject) => {
		pool.query("select * from barang limit 1", (err: MysqlError, rows) => {
			if (err) {
				reject(err.sqlMessage + '/' + err.message);
			}
			else {
				resolve(rows);
			}
		})
	});
}

app.get("/test1", (_req: express.Request, resp: express.Response) => {
	try {

		Connection.getPool()
			.then((pool: PoolConnection) => {
				return testRead(pool);
			}).then(() => {
				resp.status(200).send('success');
			}).catch((e) => {
				logT.log(e);
				resp.status(200).send("error: " + e);
			})
	}
	catch (e) {
		logT.log(e);
		resp.status(500).send(e.message);
	}
})

app.use((_req: express.Request, _resp: express.Response, _next: Function) => {
	logT.log(_req.path);
	logT.log('404');
	_resp.status(404).send('Halaman Tidak Ditemukan ' + _req.path);
})

process.on('SIGTERM', () => {
	logT.log('process on close');
	server.close(() => {
		logT.log('Process terminated')
	})
})