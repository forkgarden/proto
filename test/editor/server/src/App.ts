import express from "express";
import { Connection } from "./wwa/module/Connection";
import { logT } from "./wwa/module/TokoLog";
import cookieSession from "cookie-session";
import { Server } from "http";
import { util } from "./wwa/module/Util";
import { configController } from "./wwa/module/ConfigController";
import { appCloudinary } from "./cloudinary_module/AppClodinary";
import { appToko } from "./wwa/AppToko";

util.buatRandom();

const app: express.Express = express();
const port: number = 3000;

app.use(express.static(__dirname + "/wwa/public"));
app.use(express.json({ limit: '5mb' }));
app.use(cookieSession({
	name: 'toko_session',
	keys: ['Auni_202002_cookie_session']
}));

appToko.router(app);
appCloudinary.router(app);

app.use((_req: express.Request, _resp: express.Response, _next: Function) => {
	logT.log(_req.path);
	logT.log('404');
	_resp.status(404).send('Halaman Tidak Ditemukan');
})

process.on('SIGTERM', () => {
	try {
		logT.log('shutdown');

		Connection.pool.end((err) => {
			if (err) {
				logT.log('sql shutdown error');
				logT.log(err.sqlMessage);
			}
			else {
				logT.log('connection tutup');
			}
		});

	} catch (e) {
		logT.log(e);
	}

});

Connection.connect();

configController.ambilDariDbSemua().then(() => {
	return configController.update2DbSemua()
}).catch(() => {
	console.error;
});

export const server: Server = app.listen(port, () => {
	logT.log("app started");
});