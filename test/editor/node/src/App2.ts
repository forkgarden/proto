import fs from "fs";

mulai('').then().catch();
async function mulai(url: string): Promise<void> {
	let kontent: string = comment(baris(await bacaFile(url)));
	await tulisKeFile("_" + url, kontent);
}

function comment(str: string): string {
	let hasil: string = str;
	let fReg: RegExp = /\*\/\r\rfunction/;
	let fReg2: RegExp = /}\r\/\*\*\r/;

	console.log(comment);
	console.log(hasil.match(fReg));

	for (let i: number = 0; i < 1000; i++) {
		hasil = hasil.replace(fReg, '*/\rfunction');
	}

	for (let i: number = 0; i < 1000; i++) {
		hasil = hasil.replace(fReg2, '}\r\r/**\r');
	}

	return hasil;
}

function baris(str: string): string {
	let baris: string[] = [];
	let baris2: string[] = [];
	let hasil: string = '';
	let fReg: RegExp = /= \([a-z]*\) => {/;

	baris = str.split(/\n/);

	for (let i: number = 0; i < baris.length; i++) {
		let baris3 = baris[i];
		if (baris3.indexOf("function") > -1) {
			baris2.push('\r');
		}
		else if (baris3.match(fReg)) {
			baris2.push('\r');
		}
		baris2.push(baris3);
	}

	baris2.forEach((item: string) => {
		hasil += item;
	})

	return hasil;
}

async function bacaFile(nama: string): Promise<string> {
	return new Promise((resolve, reject) => {
		fs.readFile(nama, (err: NodeJS.ErrnoException, content) => {
			if (err) {
				reject(err.message);
			}
			else {
				resolve(content.toString());
			}
		})
	});
}

async function tulisKeFile(nama: string, data: string): Promise<void> {
	return new Promise((resolve, reject) => {
		fs.writeFile(nama, data, (err) => {
			if (err) {
				reject(err.message);
			}
			else {
				resolve();
			}
		})
	});
}