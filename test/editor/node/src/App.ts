import fs from "fs";

var config: IProject =
{
	nama: "test",
	jsExclude: [""],
	file: {
		css: [],
		js: [],
		html: [
			{
				nama: 'index.html',
				url: 'index.html'
			}
		]
	}

}

async function test(): Promise<void> {
	let file: string = await bacaFile('.\\web\\index.html');
	let scriptReg: RegExp = /<script .+<\/script>/g;
	let scrAr: RegExpMatchArray;

	//javascript
	console.log('javascript');
	config.file.js = [];
	scrAr = file.match(scriptReg);
	scrAr.forEach((str: string) => {
		let src: string = str.match(/src="[0-9a-zA-Z\-_\.\/]+"/)[0];
		let src_str: string = src.match(/".+"/)[0]
		let src_url: string = src_str.slice(1, src_str.length - 1);
		let src_nama: string = src_url.match(/[a-zA-Z0-9\-_]+\.js/)[0];

		let exl: boolean = false;
		for (let i: number = 0; i < config.jsExclude.length; i++) {
			if (config.jsExclude[i] == src_nama) {
				exl = true;
			}
		}

		if (!exl) {
			config.file.js.push({
				nama: src_nama,
				url: src_url
			});
		}
	});

	//css
	console.log('css');
	config.file.css = [];
	scrAr = file.match(/<link .+>/g);
	scrAr.forEach((str: string) => {
		let src: string = str.match(/href="[0-9a-zA-Z\-_\.\/]+"/)[0];
		let src_str: string = src.match(/".+"/)[0];
		let src_url: string = src_str.slice(1, src_str.length - 1);
		let src_nama: string = src_url.match(/[a-zA-Z\-_[0-9]+\.css/)[0];
		config.file.css.push({
			nama: src_nama,
			url: src_url
		});

	});

	//baca css file
	for (let i: number = 0; i < config.file.css.length; i++) {
		let item: IFile = config.file.css[i];
		item.konten = baris(await bacaFile('.\\web\\' + item.url));
	}

	//baca file js
	for (let i: number = 0; i < config.file.js.length; i++) {
		let item: IFile = config.file.js[i];
		item.konten = comment(baris(await bacaFile('.\\web\\' + item.url)));
	}

	//html
	config.file.html[0].konten = html(await bacaFile('./web/index.html'));

	let configStr: string = 'let config = ' + JSON.stringify(config);

	await tulisKeFile('.\\web\\edit.json', configStr);
}

function comment(str: string): string {
	let hasil: string = str;
	let fReg: RegExp = /\*\/\r\rfunction/;
	let fReg2: RegExp = /}\r\/\*\*\r/;

	console.log(comment);
	console.log(hasil.match(fReg));

	for (let i: number = 0; i < 1000; i++) {
		hasil = hasil.replace(fReg, '*/\rfunction');
	}

	for (let i: number = 0; i < 1000; i++) {
		hasil = hasil.replace(fReg2, '}\r\r/**\r');
	}

	return hasil;
}

function baris(str: string): string {
	let baris: string[] = [];
	let baris2: string[] = [];
	let hasil: string = '';
	let fReg: RegExp = /= \([a-z]*\) => {/;

	baris = str.split(/\n/);

	for (let i: number = 0; i < baris.length; i++) {
		let baris3 = baris[i];
		if (baris3.indexOf("function") > -1) {
			baris2.push('\r');
		}
		else if (baris3.match(fReg)) {
			baris2.push('\r');
		}
		baris2.push(baris3);
	}

	baris2.forEach((item: string) => {
		hasil += item;
	})

	return hasil;
}

function html(str: string): string {
	let idx: number = 0;
	let idx2: number = 0;
	let scriptReg: RegExp = /<script .+<\/script>/;
	let hasil: string = '';

	idx = str.indexOf('<body>');
	idx2 = str.indexOf('</body>', idx);

	hasil = str.slice(idx + 6, idx2);
	hasil = hasil.replace(scriptReg, '');
	hasil = hasil.replace(scriptReg, '');
	hasil = hasil.replace(scriptReg, '');
	hasil = hasil.replace(scriptReg, '');
	hasil = hasil.replace(scriptReg, '');
	hasil = hasil.replace(scriptReg, '');
	hasil = hasil.replace(scriptReg, '');
	hasil = hasil.replace(scriptReg, '');
	hasil = hasil.replace(scriptReg, '');
	hasil = hasil.replace(scriptReg, '');
	hasil = hasil.replace(scriptReg, '');
	hasil = hasil.replace(scriptReg, '');

	return hasil;
}

async function bacaFile(nama: string): Promise<string> {
	return new Promise((resolve, reject) => {
		fs.readFile(nama, (err: NodeJS.ErrnoException, content) => {
			if (err) {
				reject(err.message);
			}
			else {
				resolve(content.toString());
			}
		})
	});
}

async function tulisKeFile(nama: string, data: string): Promise<void> {
	return new Promise((resolve, reject) => {
		fs.writeFile(nama, data, (err) => {
			if (err) {
				reject(err.message);
			}
			else {
				resolve();
			}
		})
	});
}

tulisKeFile;
test();
