"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.config = {
    nama: "test",
    file: {
        css: [
            {
                nama: 'css.css',
                url: 'css/css.css'
            }
        ],
        js: [
            {
                nama: 'karakter.js',
                url: 'js/karakter.js'
            },
            {
                nama: 'PathFinder.js',
                url: 'js/PathFinder.js'
            },
            {
                nama: 'Peta.js',
                url: 'js/Peta.js'
            },
            {
                nama: 'Window.js',
                url: 'js/Window.js'
            },
            {
                nama: 'Data.js',
                url: 'js/Data.js'
            },
            {
                nama: 'Game.js',
                url: 'js/Game.js'
            }
        ],
        html: [
            {
                nama: 'index.html',
                url: 'index.html'
            }
        ]
    }
};
