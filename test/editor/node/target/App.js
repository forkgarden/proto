"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
var config = {
    nama: "test",
    file: {
        css: [],
        js: [],
        html: [
            {
                nama: 'index.html',
                url: 'index.html'
            }
        ]
    }
};
async function test() {
    let file = await bacaFile('.\\web\\index.html');
    let scriptReg = /<script .+<\/script>/g;
    let scrAr;
    console.log('javascript');
    config.file.js = [];
    scrAr = file.match(scriptReg);
    scrAr.forEach((str) => {
        let src = str.match(/src="[0-9a-zA-Z\-_\.\/]+"/)[0];
        let src_str = src.match(/".+"/)[0];
        let src_isi = src_str.slice(1, src_str.length - 1);
        let src_nama = src_isi.match(/[a-zA-Z\-_]+\.js/)[0];
        config.file.js.push({
            nama: src_nama,
            url: src_isi
        });
    });
    console.log('css');
    config.file.css = [];
    scrAr = file.match(/<link .+>/g);
    scrAr.forEach((str) => {
        let src = str.match(/href="[0-9a-zA-Z\-_\.\/]+"/)[0];
        let src_str = src.match(/".+"/)[0];
        let src_isi = src_str.slice(1, src_str.length - 1);
        let src_nama = src_isi.match(/[a-zA-Z\-_[0-9]+\.css/)[0];
        config.file.css.push({
            nama: src_nama,
            url: src_isi
        });
    });
    for (let i = 0; i < config.file.css.length; i++) {
        let item = config.file.css[i];
        item.konten = baris(await bacaFile('.\\web\\' + item.url));
    }
    for (let i = 0; i < config.file.js.length; i++) {
        let item = config.file.js[i];
        item.konten = comment(baris(await bacaFile('.\\web\\' + item.url)));
    }
    //html
    config.file.html[0].konten = html(await bacaFile('./web/index.html'));
    let configStr = 'let config = ' + JSON.stringify(config);
    await tulisKeFile('.\\web\\edit.json', configStr);
}
function comment(str) {
    let hasil = str;
    let fReg = /\*\/\r\rfunction/;
    let fReg2 = /}\r\/\*\*\r/;
    console.log(comment);
    console.log(hasil.match(fReg));
    for (let i = 0; i < 1000; i++) {
        hasil = hasil.replace(fReg, '*/\rfunction');
    }
    for (let i = 0; i < 1000; i++) {
        hasil = hasil.replace(fReg2, '}\r\r/**\r');
    }
    return hasil;
}
function baris(str) {
    let baris = [];
    let baris2 = [];
    let hasil = '';
    let fReg = /= \([a-z]*\) => {/;
    baris = str.split(/\n/);
    for (let i = 0; i < baris.length; i++) {
        let baris3 = baris[i];
        if (baris3.indexOf("function") > -1) {
            baris2.push('\r');
        }
        else if (baris3.match(fReg)) {
            baris2.push('\r');
        }
        baris2.push(baris3);
    }
    baris2.forEach((item) => {
        hasil += item;
    });
    return hasil;
}
function html(str) {
    let idx = 0;
    let idx2 = 0;
    let scriptReg = /<script .+<\/script>/;
    let hasil = '';
    idx = str.indexOf('<body>');
    idx2 = str.indexOf('</body>', idx);
    hasil = str.slice(idx + 6, idx2);
    hasil = hasil.replace(scriptReg, '');
    hasil = hasil.replace(scriptReg, '');
    hasil = hasil.replace(scriptReg, '');
    hasil = hasil.replace(scriptReg, '');
    hasil = hasil.replace(scriptReg, '');
    hasil = hasil.replace(scriptReg, '');
    return hasil;
}
async function bacaFile(nama) {
    return new Promise((resolve, reject) => {
        fs_1.default.readFile(nama, (err, content) => {
            if (err) {
                reject(err.message);
            }
            else {
                resolve(content.toString());
            }
        });
    });
}
async function tulisKeFile(nama, data) {
    return new Promise((resolve, reject) => {
        fs_1.default.writeFile(nama, data, (err) => {
            if (err) {
                reject(err.message);
            }
            else {
                resolve();
            }
        });
    });
}
tulisKeFile;
test();
