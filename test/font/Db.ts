import { app } from "./App.js";
import { loading } from "./Loading.js";
import { Util } from "./Util.js";

export class Db {
	async login(): Promise<XMLHttpRequest> {
		loading.tampil();
		return new Promise((resolve: any, reject: any) => {
			try {
				let url:string = 'http://localhost:8280/oidc-token-service/default/token?grant_type=password&password=apiclient&username=apiclient&client_id=default&scope=openid%20tags%20content_entitlements%20em_api_access';
				console.group('login');
				let xhr: XMLHttpRequest = new XMLHttpRequest();

				xhr.onload = () => {
					loading.detach();
					resolve(xhr);
				};

				xhr.onerror = (e: any) => {
					console.log('xhr error');
					console.log(e);
					loading.detach();
					reject(new Error(e.message));
				}

				xhr.open('post', url, true);
				xhr.send('');
			}
			catch (e) {
				console.log('Util error');
				console.log(e);
				loading.detach();
				reject(new Error(e.message));
			}

		});
	}

	async getTenantItem(token:string): Promise<XMLHttpRequest> {
		loading.tampil();
		return new Promise((resolve: any, reject: any) => {
			try {
				let url:string = 'http://localhost:8312/tenant-properties-service/default/properties?q=menuitems.icon&fields=name,value,lastModifiedDate,lastModifiedBy&rand='+Math.floor(Math.random() * 1000);
				console.group('login');
				let xhr: XMLHttpRequest = new XMLHttpRequest();

				xhr.onload = () => {
					loading.detach();
					resolve(xhr);
				};

				xhr.onerror = (e: any) => {
					console.log('xhr error');
					console.log(e);
					loading.detach();
					reject(new Error(e.message));
				}

				xhr.open('get', url, true);
				xhr.setRequestHeader("Content-Type","application/ld+json");
				xhr.setRequestHeader("Authorization","OIDC_id_token " + token);
				xhr.send('');

			}
			catch (e) {
				console.log('Util error');
				console.log(e);
				loading.detach();
				reject(new Error(e.message));
			}

		});
	}

	async simpan(): Promise<void> {
		loading.tampil();
		await this.simpanProcess().then((xml:XMLHttpRequest) => {
			if (xml.status == 200) {
				loading.detach();
			}
			else {
				throw Error(xml.responseText);
			}
			loading.detach();
		}).catch((e) => {
			loading.detach();
			Util.error(e);
		})
	}

	async simpanProcess():Promise<XMLHttpRequest> {
		let dataStr:string = JSON.stringify(app.items);
		let body:any = [
			{
				"@type": "vcfg:PropertyUpdateOrCreate",
				"vcfg:name": "menuitems.icon",
				"vcfg:value": dataStr
			}
		];

		console.log(body);

		return new Promise((resolve: any, reject: any) => {
			try {
				let url:string = 'http://localhost:8312/tenant-properties-service/default/properties';
				console.group('login');
				let xhr: XMLHttpRequest = new XMLHttpRequest();

				xhr.onload = () => {
					loading.detach();
					resolve(xhr);
				};

				xhr.onerror = (e: any) => {
					console.log('xhr error');
					console.log(e);
					loading.detach();
					reject(new Error(e.message));
				}

				xhr.open("PATCH", url, true);
				xhr.setRequestHeader("Content-Type","application/ld+json");
				xhr.setRequestHeader("Authorization","OIDC_id_token " + app.token);
				xhr.send(JSON.stringify(body));

			}
			catch (e) {
				console.log('Util error');
				console.log(e);
				loading.detach();
				reject(new Error(e.message));
			}

		});		
	}

}