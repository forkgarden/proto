import { dialog } from "./Dialog.js";

export class Util {

	static renderIcon(value:string):string {
		return "&#x" + value + ";";
	}


	//default error
	static error(e: Error): void {
		console.error(e);
		dialog.tampil(e.message || "error");
	}

}