import { app} from "./App.js";
import { BaseComponent } from "./BaseComponent.js";

export class PilihIcon extends BaseComponent {
	private _selesai: Function;
	public set selesai(value: Function) {
		this._selesai = value;
	}

	readonly code:string[] =[
		"ea4e",	"ea49",	"ea4a",	"ea4b",	"ea4c",	"ea4d",	"ea47",	"ea48",	"ea1f",	
		"ea3d",	"ea46",	"ea16",	"ea17",
		"ea18",	"ea19",	"ea1a",	"ea1b",	"ea1c",	"ea1d",	"ea1e",	"ea0c",	"ea0d",
		"ea0e",	"ea0f",	"ea10",	"ea11",	"ea12",	"ea13",	"ea14",	"ea15",	"ea0b",
		"ea07",	"ea08",	"ea09",	"ea0a",	"ea05",	"ea06",	"ea02",	"ea03",	"ea04",
		"e9fe",	"e9ff",	"ea00",	"ea01",	"e9f7",	"e9f8",	"e9f9",	"e9fa",	"e9fb",
		"e9fc",	"e9fd",	"e9eb",	"e9ec",	"e9ed",	"e9ee",	"e9ef",	"e9f0",	"e9f1",
		"e9f2",	"e9f3",	"e9f4",	"e9f5",	"e9f6",	"e9e5",	"e9e6",	"e9e7",	"e9e8",
		"e9e9",	"e9ea",	"e9e3",	"e9e4",	"e9dc",	"e9dd",	"e9de",	"e9df",	"e9e0",
		"e9e1",	"e9e2",	"e9db",	"e9d9",	"e9da",	"e9d5",	"e9d6",	"e9d7",	"e9d8",
		"e9d4",	"e9d2",	"e9d3",	"e9d1",	"e9d0",	"e9cc",	"e9cd",	"e9ce",	"e9cf",
		"e9ca",	"e9cb",	"e9c9",	"e9c5",	"e9c6",	"e9c7",	"e9c8",	"e9c1",	"e9c2",
		"e9c3",	"e9c4",	"e9bc",	"e9bd",	"e9be",	"e9bf",	"e9c0",	"e9ba",	"e9bb",
		"e908",	"e909",	"e90a",	"e90b",	"e912",	"e932",	"e933",	"e935",
		"e936",	"e937",	"e938",	"e948",	"e949",	"e94a",	"e94b",	"e94f",
		"e951",	"e967",	"e973",	"e97e",	"e982",	"e989",	"e98a",	"e98b",
		"e98c",	"e98d",	"e98e",	"e98f",	"e990",	"e991",	"e992",	"e993",
		"e994",	"e995",	"e996",	"e997",	"e998",	"e999",	"e99a",	"e99b",
		"e99c",	"e99d",	"e99e",	"e99f",	"e9a0",	"e9a1",	"e9a2",	"e9a3",
		"e9a4",	"e9a5",	"e9a6",	"e9a7",	"e9a8",	"e9a9",
		"e9aa",	"e9ab",	"e9ac",	"e9ad",		"e9ae",	"e9af",	"e9b0",	"e9b1",
		"e9b2",	"e9b3",	"e9b4",	"e9b5",		"e9b6",	"e9b7",	"e9b8",	"e904",
		"e905",	"e906",	"e907",	"e903",		"e900",	"e901",	"e800",	"e801",
		"e802",	"e803",	"e804",	"e805",		"e806",	"e807",	"e808",	"e809",
		"e80a",	"e80b",	"e80c",	"e80d",		"e80e",	"e80f",	"e810",	"e811",
		"e812",	"e813",	"e814",	"e815",		"e816",	"e817",	"e818",	"e819",
		"e81a",	"e81b",	"e81c",	"e81d",		"e81e",	"e81f",	"e820",	"e821",
		"e822",	"e823",	"e824",	"e825",		"e826",	"e827",	"e828",	"e829",
		"e82a",	"e82b",		"e82c",	"e82d",
		"e82e",	"e82f",		"e830",	"e831",
		"e832",	"e833",		"e834",	"e835",
		"e836",	"e837",		"e838",	"e839",
		"e83a",	"e83b",		"e83c",	"e83d",
		"e83e",	"e83f",		"e840",	"e841",
		"e842",	"e843",		"e844",	"e845",
		"e846",	"e847",		"e848",	"e849",
		"e84a",	"e84b",		"e84c",	"e84d",
		"e84e",	"e84f",		"e850",
		"e851",		"e852",
		"e853",		"e854",
		"e855",		"e856",
		"e857",		"e858",
		"e859",		"e85a",
		"e85b",		"e85c",
		"e85d",		"e85e",
		"e85f",		"e860",
		"e861",		"e862",
		"e863",		"e864",
		"e865",		"e866",
		"e867",		"e868",
		"e869",		"e86a",
		"e86b",		"e86c",
		"e86d","e86e",
		"e86f",	"e870",
		"e871",	"e872",
		"e873",	"e874",
		"e875",	"e876",
		"e877",	"e878",
		"e879",	"e87a",
		"e87b",	"e87c",
		"e87d",	"e87e",
		"e87f",	"e880",
		"e881",	"e882",
		"e883",	"e884",
		"e885",	"e886",
		"e887",	"e888",
		"e889",	"e88a",
		"e88b",	"e88c",
		"e88d",	"e88e",
		"e88f",	"e890",
		"e891",	"e892",
		"e893",	"e894",
		"e895",	"e896",
		"e897",	"e898",
		"e899",	"e89a",
		"e89b",	"e89c",
		"e89d",	"e89e",
		"e89f",	"e8a0",
		"e8a1",	"e8a2",
		"e8a3",	"e8a4",
		"e8a5",	"e8a6",
		"e8a7",	"e8a8",
		"e8a9",	"e8aa",
		"e8ab",	"e8ac",
		"e8ad",	"e8ae",
		"e8af",	"e8b0",
		"e8b1",	"e8b2",
		"e8b3",	"e8b4",
		"e8b5",	"e8b7",
		"e8b8",	"e8b9",
		"e8ba",	"e8bb",
		"e8bc",	"e8bd",
		"e8be",	"e8bf",
		"e8c0",	"e8c1",
		"e8c2",	"e8c3",
		"e8c4",	"e8c5",
		"e8c6",	"e8c7",
		"e8c8",	"e8c9",
		"e8ca",	"e8cb",
		"e8cc",	"e8cd",
		"e8ce",	"e8cf",
		"e8d0",	"e8d1",
		"e8d2",	"e8d3",
		"e8d4",	"e8d5",
		"e8d6",	"e8d7",
		"e8d8",	"e8da",
		"e8db",	"e8dc",
		"e8dd",	"e8de",
		"e8df",	"e8e0",
		"e8e1",	"e8e2",
		"e8e3",	"e8e4",
		"e8e5",	"e8e6",
		"e8e7",	"e8e8",
		"e8e9",	"e8ea",
		"e8eb",	"e8ef",
		"e8f0",	"e8f2",
		"e8f5",	"e8f6",
		"e8f7",	"e8fe",
		"e8ff", "e902",
		"e90c",
		"e90d",
		"e90e",
		"e90f",
		"e910",
		"e911",
		"e913",
		"e914",
		"e915",
		"e916",
		"e917",
		"e918",
		"e919",
		"e91a",
		"e91b",	"e91c",
		"e91d",	"e91e",
		"e91f",	"e920",
		"e921",	"e922",
		"e923",	"e924",
		"e925",	"e926",
		"e927",	"e928",
		"e929",	"e92a",
		"e92b",	"e92c",
		"e92d",	"e92e",
		"e92f",	"e930",
		"e931",	"e934",
		"e939",	"e93a",
		"e93b",	"e93c",
		"e93d",	"e93e",
		"e93f",	"e940",
		"e941",	"e942",
		"e943",	"e944",
		"e945",	"e946",
		"e947",	"e94c",
		"e94d",	"e94e",
		"e950",	"e952",
		"e953",	"e954",
		"e955",	"e956",
		"e957",	"e958",
		"e959",	"e95a",
		"e95b",	"e95c",
		"e95d",	"e95e",
		"e95f",	"e960",
		"e961",	"e962",
		"e963",	"e964",
		"e965",	"e966",
		"e968",	"e969",
		"e96a",
		"e96b",
		"e96c",
		"e96d",
		"e96e",
		"e96f",
		"e970",
		"e971",
		"e972",
		"e9b9",
		"e974",
		"e975",
		"e976",
		"e977",
		"e978",
		"e979",
		"e97a",
		"e97b",
		"e97c",
		"e97d",
		"e97f",
		"e980",
		"e981",
		"e983",
		"e984",
		"e985",
		"e986",
		"e987",
		"e988",
		"ea20",
		"ea21",
		"ea22",
		"ea23",
		"ea24",
		"ea25",
		"ea26",
		"ea27",
		"ea28",
		"ea29",
		"ea2a",
		"ea2b",
		"ea2c",
		"ea2d",
		"ea2e",
		"ea2f",
		"ea30",
		"ea31",
		"ea32",
		"ea33",
		"ea34",
		"ea35",
		"ea36",
		"ea37",
		"ea38",
		"ea39",
		"ea3a",
		"ea3b",
		"ea3c",
		"ea3e",
		"ea3f",
		"ea40",
		"ea41",
		"ea42",
		"ea43",
		"ea44",
		"ea45",
		"eafb",
		"eb00"];	

	constructor() {
		super();
		this._template = `
			<div class='pilih-icon'>
				<div class='box'>
					<div class='close'>
						<button class="btn btn-primary">X</button>
					</div>

					<h1>Please select your icon</h1>
					
					<div class='button-cont'>

					
					</div>
				</div>
			</div>
		`;
		this.build();

		this.tutupTbl.onclick = () => {
			this.detach();
		}
	}

	render(): void {
		let button: HTMLButtonElement;

		for (let i: number = 0; i < this.code.length; i++) {

			let item: string = this.code[i];

			button = document.createElement('button');
			button.classList.add('symbol');
			button.classList.add('btn');
			button.classList.add('btn-normal');
			button.setAttribute('icon', item);
			button.innerHTML = "&#x" + item + ";";
			this.buttonCont.appendChild(button);

			this.setEvent(button, item);
		}
	}

	onAttach():void {
		//app.iconBaru='';
	}

	setEvent(button:HTMLButtonElement, item:string):void {
		button.onclick = (e:MouseEvent) => {
			e.stopPropagation();

			app.iconBaru = item;

			// app.itemAktif.icon = item
			// app.itemAktifView.icon.innerHTML = `&#x${item};`;

			console.log('button on click, item ' + item);

			this.detach();

			this._selesai();
		}
	}

	get buttonCont():HTMLDivElement {
		return this.getEl('div.box div.button-cont') as HTMLDivElement;
	}

	get tutupTbl():HTMLButtonElement {
		return this.getEl('div.box div.close button') as HTMLButtonElement;
	}

	get box():HTMLDivElement {
		return this.getEl('div.box') as HTMLDivElement;
	}
}