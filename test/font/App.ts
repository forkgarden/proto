// import { BaseComponent } from "./BaseComponent.js";
import {Db} from "./Db.js"
import { Baru } from "./Baru.js";
import { Hal, ItemView } from "./Hal.js";
import { PilihIcon } from "./PilihIcon.js";
import { Edit } from "./Edit.js";

export var app:App;

class App {
	
	private _token: string;
	public set token(value: string) {
		this._token = value;
	}
	public get token(): string {
		return this._token;
	}

	private _hal: Hal;
	public get hal(): Hal {
		return this._hal;
	}

	private _edit:Edit;
	public get edit():Edit {
		return this._edit;
	}
	
	private _baru: Baru;
	public get baru(): Baru {
		return this._baru;
	}

	private _pilihIcon: PilihIcon;
	public get pilihIcon(): PilihIcon {
		return this._pilihIcon;
	}

	private _items: Item[];
	public get items(): Item[] {
		return this._items;
	}
	public set items(value: Item[]) {
		this._items = value;
	}
	private _itemAktif: Item;
	public get itemAktif(): Item {
		return this._itemAktif;
	}
	public set itemAktif(value: Item) {
		this._itemAktif = value;
	}
	private _itemAktifView: ItemView;
	public get itemAktifView(): ItemView {
		return this._itemAktifView;
	}
	public set itemAktifView(value: ItemView) {
		this._itemAktifView = value;
	}

	private _iconBaru: string;
	public get iconBaru(): string {
		return this._iconBaru;
	}
	public set iconBaru(value: string) {
		this._iconBaru = value;
	}

	private _db: Db;
	public get db(): Db {
		return this._db;
	}

	constructor() {
		this._token = '';
		this._hal = new Hal();
		this._baru = new Baru();
		this._pilihIcon = new PilihIcon();
		this._db = new Db();
		this._edit = new Edit();

		this._pilihIcon.render();
	}	
	
}

window.onload = () => {
	// db = new Db();
	// hal = new Hal();
	// pilihIcon = new PilihIcon();
	// edit = new Edit();
	app = new App();
	app.hal.init();
}

export interface Item {
	menu: string,
	icon: string
}