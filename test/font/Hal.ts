import { app, Item } from "./App.js";
import { BaseComponent } from "./BaseComponent.js";
import { loading } from "./Loading.js";
import { Util } from "./Util.js";

export class Hal extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='cont container hal-depan'>
				<h2>TPS menuitems.icon builder</h2>
                <hr/>
				<div class='daftar-icon'>

				</div>
				<button class="tambah">+</button>
			</div>
		`;
        this.build();
    }

    init() {
        this.attach(document.body);
        this.tambahTbl.onclick = () => {
            app.baru.attach(document.body);
        };
        loading.tampil();
        app.db.login().then((xml:XMLHttpRequest) => {
            if (xml.status >= 200 || xml.status < 300) {
                console.log("token " + xml);
                let obj = JSON.parse(xml.responseText);
                app.token = obj.access_token;
                console.log(obj);
                return app.db.getTenantItem(app.token);    
            }
            else {
                throw Error(xml.responseText);
            }

        }).then((xml:XMLHttpRequest) => {
            if (xml.status >= 200 || xml.status < 300) {
                let obj = JSON.parse(xml.responseText);
                app.items = JSON.parse(obj["hydra:member"][0]["vcfg:value"]);
                app.hal.render();
                loading.hide();    
            }
            else {
                throw Error(xml.responseText);
            }
        }).catch((e) => {
            loading.hide();
            Util.error(e);
        });
    }

    render() {
        console.log('render');
        console.log("length " + app.items.length);
        this.daftarIcon.innerHTML = '';
        for (let i = 0; i < app.items.length; i++) {
            this.renderBaru(app.items[i]);
        }
    }

    renderBaru(item:Item):void {
        let itemView = new ItemView();
        itemView.init(item);
        itemView.attach(this.daftarIcon);
    }

    get tambahTbl() {
        return this.getEl('button.tambah');
    }
    get daftarIcon() {
        return this.getEl('div.daftar-icon');
    }
}

export class ItemView extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='item menu'>
                <span class='icon symbol'></span>
                <span class='menu'></span>
				<button class='btn btn-primary edit symbol'>&#xe8a4;</button>&nbsp;
				<button class='btn btn-primary hapus symbol'>&#xe875;</button>
			</div>
		`;
        this.build();
    }

    init(item:Item) {
        this.menu.innerHTML = item.menu;
        
        this.icon.innerHTML = '&#x' + item.icon + ';';

        this.hapusTbl.onclick = () => {
            let ok = window.confirm('Are you sure you want to delete this item');
            if (ok) {
                let idx = app.items.indexOf(item);
                console.debug('idx to delete ' + idx);
                console.debug(app.items[idx]);
                console.debug('pjg sebelum ' + app.items.length);
                app.items.splice(app.items.indexOf(item),1);
                app.hal.render();
                app.db.simpan();
                console.debug('pjg sesudah ' + app.items.length);
            }
        };

        this.editTbl.onclick = () => {
            app.itemAktif = item;
            app.itemAktifView = this;

            app.edit.attach(document.body);
            
            //TODO:
            // app.pilihIcon.attach(document.body);
            // app.pilihIcon.selesai = () => {
            //     app.pilihIcon.detach();

            //     app.itemAktif.icon = app.iconBaru;
            //     app.itemAktifView.icon.innerHTML = `&#x${app.iconBaru};`;

            //     app.db.simpan();
            // };
        };
    }

    get menu() {
        return this.getEl('span.menu');
    }
    get icon() {
        return this.getEl('span.icon');
    }
    get editTbl() {
        return this.getEl('button.edit');
    }
    get hapusTbl() {
        return this.getEl('button.hapus');
    }
}