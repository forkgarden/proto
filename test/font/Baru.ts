import { app, Item } from "./App.js";
import { BaseComponent } from "./BaseComponent.js";
import { Util } from "./Util.js";

export class Baru extends BaseComponent {
    constructor() {
        super();
        this._template = `
                <div class='baru'>
                    <div class="box">
                        <div class='close'>
                            <button class="btn btn-primary">X</button>
                        </div>
                        <form class='baru'>
                            <div class='mb-3'>
                                <label class='form-label'>Menu:</label>
                                <input type="text" class="form-control menu" required>
                            </div>
                            <div class='mb-3'>
                                <label class='form-label'>Icon:</label>
                                <div>
                                    <span class="icon symbol"></span>
                                    <button class="btn btn-outline-secondary browse" type="button" >browse ...</button>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">submit</button>
                        </form>
                    </div>
                </div>
            `;
        this.build();

        this.form.onsubmit = () => {
            console.log('form on submit');
            try {
                let item:Item = {
                    menu: this.menu.value,
                    icon: app.iconBaru
                }
                app.items.push(item);
                app.hal.renderBaru(item);
                app.db.simpan();
                this.detach();
            }
            catch(e) {
                Util.error(e);
            }
            return false;
        }

        this.browserTbl.onclick = () => {
            app.pilihIcon.attach(document.body);
            app.pilihIcon.selesai = () => {
                this.icon.innerHTML="&#x" + app.iconBaru + ";";
            }
        }

        this.tutupBtn.onclick = () => {
            this.detach();
        }
    }

    onAttach():void {
        app.iconBaru = "ea4e";
        this.icon.innerHTML = Util.renderIcon(app.iconBaru);
    }

    get tutupBtn():HTMLButtonElement {
        return this.getEl('div.close button') as HTMLButtonElement;
    }

    get icon():HTMLSpanElement {
        return this.getEl('span.icon') as HTMLSpanElement;
    }

    get menu():HTMLInputElement {
        return this.getEl('form input.menu') as HTMLInputElement;
    }

    get form(): HTMLFormElement {
        return this.getEl('form.baru') as HTMLFormElement;
    }

    get browserTbl():HTMLButtonElement {
        return this.getEl('form button.browse') as HTMLButtonElement;
    }
}
