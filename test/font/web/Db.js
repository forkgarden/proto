import { app } from "./App.js";
import { loading } from "./Loading.js";
import { Util } from "./Util.js";
export class Db {
    async login() {
        loading.tampil();
        return new Promise((resolve, reject) => {
            try {
                let url = 'http://localhost:8280/oidc-token-service/default/token?grant_type=password&password=apiclient&username=apiclient&client_id=default&scope=openid%20tags%20content_entitlements%20em_api_access';
                console.group('login');
                let xhr = new XMLHttpRequest();
                xhr.onload = () => {
                    loading.detach();
                    resolve(xhr);
                };
                xhr.onerror = (e) => {
                    console.log('xhr error');
                    console.log(e);
                    loading.detach();
                    reject(new Error(e.message));
                };
                xhr.open('post', url, true);
                xhr.send('');
            }
            catch (e) {
                console.log('Util error');
                console.log(e);
                loading.detach();
                reject(new Error(e.message));
            }
        });
    }
    async getTenantItem(token) {
        loading.tampil();
        return new Promise((resolve, reject) => {
            try {
                let url = 'http://localhost:8312/tenant-properties-service/default/properties?q=menuitems.icon&fields=name,value,lastModifiedDate,lastModifiedBy&rand=' + Math.floor(Math.random() * 1000);
                console.group('login');
                let xhr = new XMLHttpRequest();
                xhr.onload = () => {
                    loading.detach();
                    resolve(xhr);
                };
                xhr.onerror = (e) => {
                    console.log('xhr error');
                    console.log(e);
                    loading.detach();
                    reject(new Error(e.message));
                };
                xhr.open('get', url, true);
                xhr.setRequestHeader("Content-Type", "application/ld+json");
                xhr.setRequestHeader("Authorization", "OIDC_id_token " + token);
                xhr.send('');
            }
            catch (e) {
                console.log('Util error');
                console.log(e);
                loading.detach();
                reject(new Error(e.message));
            }
        });
    }
    async simpan() {
        loading.tampil();
        await this.simpanProcess().then((xml) => {
            if (xml.status == 200) {
                loading.detach();
            }
            else {
                throw Error(xml.responseText);
            }
            loading.detach();
        }).catch((e) => {
            loading.detach();
            Util.error(e);
        });
    }
    async simpanProcess() {
        let dataStr = JSON.stringify(app.items);
        let body = [
            {
                "@type": "vcfg:PropertyUpdateOrCreate",
                "vcfg:name": "menuitems.icon",
                "vcfg:value": dataStr
            }
        ];
        console.log(body);
        return new Promise((resolve, reject) => {
            try {
                let url = 'http://localhost:8312/tenant-properties-service/default/properties';
                console.group('login');
                let xhr = new XMLHttpRequest();
                xhr.onload = () => {
                    loading.detach();
                    resolve(xhr);
                };
                xhr.onerror = (e) => {
                    console.log('xhr error');
                    console.log(e);
                    loading.detach();
                    reject(new Error(e.message));
                };
                xhr.open("PATCH", url, true);
                xhr.setRequestHeader("Content-Type", "application/ld+json");
                xhr.setRequestHeader("Authorization", "OIDC_id_token " + app.token);
                xhr.send(JSON.stringify(body));
            }
            catch (e) {
                console.log('Util error');
                console.log(e);
                loading.detach();
                reject(new Error(e.message));
            }
        });
    }
}
