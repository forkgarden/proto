import { dialog } from "./Dialog.js";
export class Util {
    static renderIcon(value) {
        return "&#x" + value + ";";
    }
    //default error
    static error(e) {
        console.error(e);
        dialog.tampil(e.message || "error");
    }
}
