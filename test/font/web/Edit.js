import { app } from "./App.js";
import { BaseComponent } from "./BaseComponent.js";
import { Util } from "./Util.js";
export class Edit extends BaseComponent {
    constructor() {
        super();
        this._template = `
                <div class='edit-icon'>
                    <div class="box">
                        <div class='close'>
                            <button class="btn btn-primary">X</button>
                        </div>
                        <form class='baru'>
                            <div class='mb-3'>
                                <label class='form-label'>Menu:</label>
                                <input type="text" class="form-control menu" required>
                            </div>
                            <div class='mb-3'>
                                <label class='form-label'>Icon:</label>
                                <div>
                                    <span class="icon symbol"></span>
                                    <button class="btn btn-outline-secondary browse" type="button" >browse ...</button>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">submit</button>
                        </form>
                    </div>
                </div>
            `;
        this.build();
        this.form.onsubmit = () => {
            console.log('form on submit, edit');
            console.log(Util);
            try {
                app.itemAktif.menu = this.menu.value;
                app.itemAktif.icon = app.iconBaru;
                app.itemAktifView.menu.innerHTML = this.menu.value;
                app.itemAktifView.icon.innerHTML = Util.renderIcon(app.iconBaru);
                app.db.simpan();
                this.detach();
            }
            catch (e) {
                Util.error(e);
            }
            return false;
        };
        this.browserTbl.onclick = () => {
            app.pilihIcon.attach(document.body);
            app.pilihIcon.selesai = () => {
                this.icon.innerHTML = "&#x" + app.iconBaru + ";";
            };
        };
        this.tutupBtn.onclick = () => {
            this.detach();
        };
    }
    onAttach() {
        app.iconBaru = app.itemAktif.icon;
        this.menu.value = app.itemAktif.menu;
        this.icon.innerHTML = `&#x${app.itemAktif.icon};`;
    }
    get tutupBtn() {
        return this.getEl('div.close button');
    }
    get icon() {
        return this.getEl('span.icon');
    }
    get menu() {
        return this.getEl('form input.menu');
    }
    get form() {
        return this.getEl('form.baru');
    }
    get browserTbl() {
        return this.getEl('form button.browse');
    }
}
