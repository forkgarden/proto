// import { BaseComponent } from "./BaseComponent.js";
import { Db } from "./Db.js";
import { Baru } from "./Baru.js";
import { Hal } from "./Hal.js";
import { PilihIcon } from "./PilihIcon.js";
import { Edit } from "./Edit.js";
export var app;
class App {
    _token;
    set token(value) {
        this._token = value;
    }
    get token() {
        return this._token;
    }
    _hal;
    get hal() {
        return this._hal;
    }
    _edit;
    get edit() {
        return this._edit;
    }
    _baru;
    get baru() {
        return this._baru;
    }
    _pilihIcon;
    get pilihIcon() {
        return this._pilihIcon;
    }
    _items;
    get items() {
        return this._items;
    }
    set items(value) {
        this._items = value;
    }
    _itemAktif;
    get itemAktif() {
        return this._itemAktif;
    }
    set itemAktif(value) {
        this._itemAktif = value;
    }
    _itemAktifView;
    get itemAktifView() {
        return this._itemAktifView;
    }
    set itemAktifView(value) {
        this._itemAktifView = value;
    }
    _iconBaru;
    get iconBaru() {
        return this._iconBaru;
    }
    set iconBaru(value) {
        this._iconBaru = value;
    }
    _db;
    get db() {
        return this._db;
    }
    constructor() {
        this._token = '';
        this._hal = new Hal();
        this._baru = new Baru();
        this._pilihIcon = new PilihIcon();
        this._db = new Db();
        this._edit = new Edit();
        this._pilihIcon.render();
    }
}
window.onload = () => {
    // db = new Db();
    // hal = new Hal();
    // pilihIcon = new PilihIcon();
    // edit = new Edit();
    app = new App();
    app.hal.init();
};
