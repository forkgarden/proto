// package fg.mazes {
// import flash.display.Bitmap;
// import flash.display.BitmapData;
// import flash.display.DisplayObject;
// import flash.display.DisplayObjectContainer;
// import flash.display.Graphics;
// import flash.display.Sprite;

///<reference path="./easel.d.ts"/>

export class Maze {

	//JALAN = 1, TEMBOK = 0
	public static readonly NORTH: number = 1;	//;0001
	public static readonly EAST: number = 2;	//;0010
	public static readonly SOUTH: number = 4;	//;0100
	public static readonly WEST: number = 8;	//;1000

	protected maps: Array<Array<number>> = [];
	protected _mapScales: Array<Array<number>> = new Array<Array<number>>();

	protected dirs: Array<number>;
	protected poss: Array<number>;

	protected mapWidth: number = 8;
	protected mapHeight: number = 8;

	protected map2Width: number = 8;
	protected map2Height: number = 8;

	protected mapNGrid: number = 64;
	protected _gridSize: number = 32;
	protected pacmanMode: Boolean = false;

	public Maze() {
		this.dirs = [];
		this.dirs[0] = Maze.NORTH;
		this.dirs[1] = Maze.EAST;
		this.dirs[2] = Maze.SOUTH;
		this.dirs[3] = Maze.WEST;

	}

	public debug(): void {

	}

	protected checkWall(i: number, j: number, dir: number): Boolean {
		return ((this.maps[i][j] & dir) != dir);
	}

	protected scale(n: number = 2): void {
		var i: number;
		var j: number;
		var ix: number;
		var jx: number;
		var k: number;
		var panjang: number = n - 1;
		var jarak: number = n - 1;

		while (this._mapScales.length > 0) this._mapScales.pop();

		this.map2Width = this.mapWidth * n + 1;
		this.map2Height = this.mapHeight * n + 1;

		this._mapScales = [];
		for (i = 0; i < this.map2Width; i++) {
			this._mapScales[i] = [];
		}

		for (i = 0; i < this.map2Width; i++) {
			for (j = 0; j < this.map2Height; j++) {
				//horizontal
				if (i % n == 0) {
					this._mapScales[i][j] = 1;
				}

				//vertical
				if (j % n == 0) {
					this._mapScales[i][j] = 1;
				}
			}
		}

		for (i = 0; i < this.mapWidth; i++) {
			for (j = 0; j < this.mapHeight; j++) {
				ix = i * n + 1;
				jx = j * n + 1;

				if (this.checkWall(i, j, Maze.EAST) == false) {
					for (k = 0; k < panjang; k++) {
						this._mapScales[ix + jarak][jx + k] = 0;
					}
				}

				if (this.checkWall(i, j, Maze.NORTH) == false) {
					for (k = 0; k < panjang; k++) {
						this._mapScales[ix + k][jx - jarak] = 0;
					}
				}

				if (this.checkWall(i, j, Maze.SOUTH) == false) {
					for (k = 0; k < panjang; k++) {
						this._mapScales[ix + k][jx + jarak] = 0;
					}
				}

				if (this.checkWall(i, j, Maze.WEST) == false) {
					for (k = 0; k < panjang; k++) {
						this._mapScales[ix - jarak][jx + k] = 0;
					}
				}
			}
		}

	}

	public drawLine(g: createjs.Graphics, gridSize: number): void {
		var i: number;
		var j: number;
		var map: number;
		var ix: number;
		var jx: number;

		// g.lineStyle(3);

		//check tiap grid dari maze
		for (i = 0; i < this.mapWidth; i++) {
			for (j = 0; j < this.mapHeight; j++) {

				ix = i * gridSize;
				jx = j * gridSize;

				//check tembok utara
				if (this.checkWall(i, j, Maze.NORTH)) {
					g.moveTo(ix, jx);
					g.lineTo(ix + gridSize, jx);
				}

				//check tembok timur
				if (this.checkWall(i, j, Maze.EAST)) {
					g.moveTo(ix + gridSize, jx);
					g.lineTo(ix + gridSize, jx + gridSize);
				}

				//check tembok selatan
				if (this.checkWall(i, j, Maze.SOUTH)) {
					g.moveTo(ix, jx + gridSize);
					g.lineTo(ix + gridSize, jx + gridSize);
				}

				//check tembok barat
				if (this.checkWall(i, j, Maze.WEST)) {
					g.moveTo(ix, jx);
					g.lineTo(ix, jx + gridSize);
				}

			}
		}
	}

	protected drawBlock(i: number, j: number, g: createjs.Graphics, size: number): void {
		g.beginFill("#000");
		g.drawRect(i, j, size, size);
		g.endFill();
		g.setStrokeStyle(1, 0x00ff00);
		g.drawRect(i, j, size, size);
	}

	//TODO: offset
	/*
	public drawWImg(cont: createjs.Container, bmpData: BitmapData, floorBmpData: BitmapData, tileSize: number = 32): void {
		var i: number;
		var j: number;
		var bmp: Bitmap;
		var rumput: Bitmap;

		while (cont.numChildren > 0) cont.removeChildAt(0);

		for (i = 0; i < map2Width; i++) {
			for (j = 0; j < map2Height; j++) {
				rumput = new Bitmap(floorBmpData);
				rumput.x = i * tileSize;
				rumput.y = j * tileSize;

				cont.addChild(rumput);
			}
		}

		for (i = 0; i < map2Width; i++) {
			for (j = 0; j < map2Height; j++) {

				if (_mapScales[i][j] == 1) {
					bmp = new Bitmap(bmpData);
					bmp.x = i * tileSize;
					bmp.y = j * tileSize;
					cont.addChild(bmp);
				}
			}
		}

	}
	*/

	/*
	public draw(cont: createjs.Sprite, tileSize: number): void {
		var i: number;
		var j: number;
		var img: DisplayObject;

		cont.graphics.clear();

		for (i = 0; i < this.map2Width; i++) {
			for (j = 0; j < this.map2Height; j++) {
				if (this._mapScales[i][j] == 1) {
					this.drawBlock(i * tileSize, j * tileSize, cont.graphics, tileSize);
				}
			}
		}
	}
	*/

	protected mapsInit(w: number, h: number): void {
		var i: number;

		while (this.maps.length > 0) this.maps.pop();

		this.maps = new Array<Array<number>>(w);
		for (i = 0; i < w; i++) {
			this.maps[i] = new Array<number>(h);
		}

		this.mapWidth = w;
		this.mapHeight = h;
		this.mapNGrid = w * h;
		this.poss = [];

		//trace('create new map ' + w + '/' + h);
	}

	public create(columns: number, rows: number, roomSize: number = 2, pacmanMode: Boolean = false): void {
		this.pacmanMode = pacmanMode;
		this.mapsInit(columns, rows);
		this.mazeCreate();
		if (pacmanMode) this.pacman(columns, rows);
		this.scale(roomSize + 1);
	}

	public pacman(w: number, h: number): void {
		var i: number;
		var j: number;

		//trace('pacman, w ' + w + '/h ' + h);

		for (i = 0; i < w; i++) {
			for (j = 0; j < h; j++) {
				if (this.maps[i][j] == Maze.NORTH) {
					if (j < h - 1) {
						this.maps[i][j] |= Maze.SOUTH;
						this.maps[i][j + 1] |= Maze.NORTH;
						//trace('destroy south wall, res ' + maps[i][j] + '/res bawah ' + maps[i][j + 1] + '/pos ' + i + '-' + j);
					}
				}
				else if (this.maps[i][j] == Maze.EAST) {
					if (i > 0) {
						this.maps[i][j] |= Maze.WEST;
						this.maps[i - 1][j] |= Maze.EAST;
					}
				}
				else if (this.maps[i][j] == Maze.SOUTH) {
					if (j > 0) {
						this.maps[i][j] |= Maze.NORTH;
						this.maps[i][j - 1] |= Maze.SOUTH;
					}
				}
				else if (this.maps[i][j] == Maze.WEST) {
					if (i < (w - 1)) {
						this.maps[i][j] |= Maze.EAST;
						this.maps[i + 1][j] |= Maze.WEST;
					}
				}
			}
		}
	}

	/*
	protected  create2(w:number, h:number, mScale:number):void {
		trace('pacman mode ' + pacmanMode);
		
		create4(w, h);
		if (pacmanMode) pacman(w,h);
		scale(mScale);
	}
	*/

	/*
	protected  create4(w:number, h:number):void {
		mapsInit(w, h);
		mazeCreate();
	}
	*/

	protected mazeCreate(): void {
		var i: number = 0;
		var j: number = 0;
		var s: String;
		var result: Boolean = true;

		//init
		for (i = 0; i < this.mapWidth; i++) {
			for (j = 0; j < this.mapHeight; j++) {
				this.maps[i][j] = 0;
			}
		}

		this.randomPos();

		//awal sambungan
		j = Math.floor(Math.random() * (this.mapHeight / 2));
		i = Math.floor(Math.random() * (this.mapWidth / 2));

		this.maps[i][j] = this.maps[i][j] | Maze.EAST;
		this.maps[i + 1][j] = this.maps[i + 1][j] | Maze.WEST;

		while (true) {
			if (!this.cellConnect()) break;
		}
	}

	protected randomPos(): void {
		var a: number = 0;
		var b: number = 0;
		var c: number = 0;
		var i: number = 0;

		for (i = 0; i < this.mapNGrid; i++) this.poss[i] = i;

		for (i = 0; i < this.mapNGrid; i++) {
			a = Math.floor(Math.random() * (this.mapNGrid));
			b = Math.floor(Math.random() * (this.mapNGrid));
			c = this.poss[a];
			this.poss[a] = this.poss[b];
			this.poss[b] = c;
		}
	}

	protected cellConnect(): Boolean {
		var loop: Boolean = true;
		var ctr: number = 0;
		var iz: number = 0;
		var jz: number = 0;
		var xx: number = 0;
		var yy: number = 0;
		var a: number = 0;
		var b: number = 0;
		var c: number = 0;
		var i: number = 0;
		var j: number = 0;

		for (j = 0; j < this.mapNGrid; j++) {
			xx = this.poss[j] % this.mapWidth;
			yy = Math.floor(this.poss[j] / this.mapWidth);

			if (this.maps[xx][yy] == 0) {
				if (this.connectIsPossible(xx, yy)) {

					for (i = 0; i < 4; i++) {
						a = Math.floor((Math.random() * 4));
						b = Math.floor((Math.random() * 4));
						c = this.dirs[a];
						this.dirs[a] = this.dirs[b];
						this.dirs[b] = c;
					}

					for (i = 0; i < 4; i++) {
						if (this.connectNorth(i, xx, yy)) return true;
						if (this.connectSouth(i, xx, yy)) return true;
						if (this.connectEast(i, xx, yy)) return true;
						if (this.connectWest(i, xx, yy)) return true;
					}
					return true;
				}
			}
		}
		return false;
	}

	protected connectIsPossible(i: number, j: number): Boolean {

		if (j > 0) {
			if (this.maps[i][j - 1] > 0) return true;
		}

		if (j < (this.mapHeight - 1)) {
			if (this.maps[i][j + 1] > 0) return true;
		}

		if (i > 0) {
			if (this.maps[i - 1][j] > 0) return true;
		}

		if (i < (this.mapWidth - 1)) {
			if (this.maps[i + 1][j] > 0) return true;
		}

		return false;
	}

	protected connectNorth(i: number, iz: number, jz: number): Boolean {
		if (this.dirs[i] == Maze.NORTH) {
			if (jz > 0) {
				if (this.maps[iz][jz] == 0) {
					if (this.maps[iz][jz - 1] > 0) {
						this.maps[iz][jz] = this.maps[iz][jz] | Maze.NORTH;
						this.maps[iz][jz - 1] = this.maps[iz][jz - 1] | Maze.SOUTH;
						return true;
					}
				}
			}
		}
		return false;
	}

	protected connectSouth(i: number, iz: number, jz: number): Boolean {
		if (this.dirs[i] == Maze.SOUTH) {
			if (jz < this.mapHeight - 1) {
				if (this.maps[iz][jz] == 0) {
					if (this.maps[iz][jz + 1] > 0) {
						this.maps[iz][jz] = this.maps[iz][jz] | Maze.SOUTH;
						this.maps[iz][jz + 1] = this.maps[iz][jz + 1] | Maze.NORTH;
						return true;
					}
				}
			}
		}
		return false;
	}

	protected connectEast(i: number, iz: number, jz: number): Boolean {
		if (this.dirs[i] == Maze.EAST) {
			if (iz < this.mapWidth - 1) {
				if (this.maps[iz][jz] == 0) {
					if (this.maps[iz + 1][jz] > 0) {
						this.maps[iz][jz] = this.maps[iz][jz] | Maze.EAST;
						this.maps[iz + 1][jz] = this.maps[iz + 1][jz] | Maze.WEST;
						return true;
					}
				}
			}
		}
		return false;
	}

	protected connectWest(i: number, iz: number, jz: number): Boolean {
		if (this.dirs[i] == Maze.WEST) {
			if (iz > 0) {
				if (this.maps[iz][jz] == 0) {
					if (this.maps[iz - 1][jz] > 0) {
						this.maps[iz][jz] = this.maps[iz][jz] | Maze.WEST;
						this.maps[iz - 1][jz] = this.maps[iz - 1][jz] | Maze.EAST;
						return true;
					}
				}
			}
		}
		return false;
	}

	protected fromString(str: String): void {
		var ar: Array<string> = [];
		var ar2: Array<string> = [];
		var i: number;
		var j: number;

		//trace("from string: " + str);

		ar = str.split("|");

		for (i = 0; i < this.mapWidth; i++) {
			ar2 = String(ar[i]).split(",");
			for (j = 0; j < this.mapHeight; j++) {
				this.maps[i][j] = parseInt(ar2[j]);
			}
		}
	}

	protected toString(): String {
		var i: number;
		var j: number;
		var str: String;

		//*&|

		str = "";
		for (i = 0; i < this.mapWidth; i++) {
			for (j = 0; j < this.mapHeight; j++) {
				str += this.maps[i][j] + ",";
			}
			str = str.slice(0, str.length - 1);
			str += "|";
		}
		str = str.slice(0, str.length - 1);

		//trace("toString " + str);
		return str;
	}

	public get map(): Array<Array<number>> {
		return this._mapScales;
	}

	public get width(): number {
		return this.map2Width;
	}

	public get height(): number {
		return this.map2Height;
	}

}