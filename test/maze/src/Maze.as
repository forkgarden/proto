package fg.mazes {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.DisplayObject;
	import flash.display.DisplayObjectContainer;
	import flash.display.Graphics;
	import flash.display.Sprite;
	
	public final class Maze {
		
		//JALAN = 1, TEMBOK = 0
		public static const NORTH:int 	= 1;	//;0001
		public static const EAST:int 	= 2;	//;0010
		public static const SOUTH:int 	= 4;	//;0100
		public static const WEST:int 	= 8;	//;1000
		
		protected var maps:Vector.<Vector.<int>> = new Vector.<Vector.<int>>();
		protected var _mapScales:Vector.<Vector.<int>> = new Vector.<Vector.<int>>();
		
		protected var dirs:Vector.<int>;
		protected var poss:Vector.<int>;
		
		protected var mapWidth:Number = 8;
		protected var mapHeight:Number = 8;
		
		protected var map2Width:Number = 8;
		protected var map2Height:Number = 8;
		
		protected var mapNGrid:Number = 64;
		protected var _gridSize:int = 32;
		protected var pacmanMode:Boolean = false;
		
		public function Maze() {
			dirs = new Vector.<int>(4);
			dirs[0] = NORTH;
			dirs[1] = EAST;
			dirs[2] = SOUTH;
			dirs[3] = WEST;
		}
		
		public function debug():void {
			
		}
		
		protected function checkWall(i:int, j:int, dir:int):Boolean {
			return ((maps[i][j] & dir) != dir);
		}
		
		protected function scale(n:int = 2):void {
			var i:int;
			var j:int;
			var ix:int;
			var jx:int;
			var k:int;
			var panjang:int = n - 1;
			var jarak:int = n - 1;
			
			while (_mapScales.length > 0) _mapScales.pop();
			
			map2Width = mapWidth * n + 1;
			map2Height = mapHeight * n + 1;	
			
			_mapScales = new Vector.<Vector.<int>>(map2Width);
			for (i = 0; i < map2Width; i++) {
				_mapScales[i] = new Vector.<int>(map2Height);
			}
			
			for (i = 0; i < map2Width; i++ ) {
				for (j = 0; j < map2Height; j++) {
					//horizontal
					if (i % n == 0) {
						_mapScales[i][j] = 1;
					}
					
					//vertical
					if (j % n == 0) {
						_mapScales[i][j] = 1;
					}		
				}
			}
			
			for (i = 0; i < mapWidth; i++) {
				for (j = 0; j < mapHeight; j++) {
					ix = i * n + 1;
					jx = j * n + 1;
					
					if (checkWall(i, j, EAST) == false) {
						for (k = 0; k < panjang; k++) {
							_mapScales[ix + jarak][jx + k] = 0;
						}
					}
					
					if (checkWall(i, j, NORTH) == false) {
						for (k = 0; k < panjang; k++) {
							_mapScales[ix + k][jx - jarak] = 0;
						}
					}
					
					if (checkWall(i, j, SOUTH) == false) {
						for (k = 0; k < panjang; k++) {
							_mapScales[ix + k][jx + jarak] = 0;
						}
					}
					
					if (checkWall(i, j, WEST) == false) {
						for (k = 0; k < panjang; k++) {
							_mapScales[ix - jarak][jx + k] = 0;
						}
					}
				}
			}
			
		}
		
		public function drawLine(g:Graphics, gridSize:int):void {
			var i:int;
			var j:int;
			var map:int;
			var ix:int;
			var jx:int;
			
			g.lineStyle(3);
			
			//check tiap grid dari maze
			for (i = 0; i < mapWidth; i++) {
				for (j = 0; j < mapHeight; j++) {
					
					ix = i * gridSize;
					jx = j * gridSize;
					
					//check tembok utara
					if (checkWall(i, j, Maze.NORTH)) {
						g.moveTo(ix, jx);
						g.lineTo(ix + gridSize, jx);
					}
					
					//check tembok timur
					if (checkWall(i, j, Maze.EAST)) {
						g.moveTo(ix + gridSize, jx);
						g.lineTo(ix + gridSize, jx + gridSize);
					}
					
					//check tembok selatan
					if (checkWall(i, j, Maze.SOUTH)) {
						g.moveTo(ix, jx + gridSize);
						g.lineTo(ix + gridSize, jx + gridSize);
					}
					
					//check tembok barat
					if (checkWall(i, j, Maze.WEST)) {
						g.moveTo(ix, jx);
						g.lineTo(ix, jx + gridSize);
					}
					
				}
			}
		}
		
		protected function drawBlock(i:int, j:int, g:Graphics, size:int):void {
			g.beginFill(0);
			g.drawRect(i, j, size, size);
			g.endFill();
			g.lineStyle(1, 0x00ff00);
			g.drawRect(i, j, size, size);
		}
		
		//TODO: offset
		public function drawWImg(cont:DisplayObjectContainer, bmpData:BitmapData, floorBmpData:BitmapData, tileSize:int=32):void {
			var i:int;
			var j:int;
			var bmp:Bitmap;
			var rumput:Bitmap;
			
			while (cont.numChildren > 0) cont.removeChildAt(0);
			
			for (i = 0; i < map2Width; i++) {
				for (j = 0; j < map2Height; j++) {
					rumput = new Bitmap(floorBmpData);
					rumput.x = i * tileSize;
					rumput.y = j * tileSize;
					
					cont.addChild(rumput);
				}
			}			
			
			for (i = 0; i < map2Width; i++) {
				for (j = 0; j < map2Height; j++) {
					
					if (_mapScales[i][j] == 1) {
						bmp = new Bitmap(bmpData);
						bmp.x = i * tileSize;
						bmp.y = j * tileSize;
						cont.addChild(bmp);
					}
				}
			}
			
		}
		
		public function draw(cont:Sprite, tileSize:int):void {
			var i:int;
			var j:int;
			var img:DisplayObject;
			
			cont.graphics.clear();
			
			for (i = 0; i < map2Width; i++) {
				for (j = 0; j < map2Height; j++) {
					if (_mapScales[i][j] == 1) {
						drawBlock(i * tileSize, j * tileSize, cont.graphics, tileSize);
					}
				}
			}
		}
		
		protected function mapsInit(w:int, h:int):void {
			var i:int;
			
			while (maps.length > 0) maps.pop();
			
			maps = new Vector.<Vector.<int>>(w);
			for (i = 0; i < w; i++) {
				maps[i] = new Vector.<int>(h);
			}
			
			mapWidth = w;
			mapHeight = h;
			mapNGrid = w * h;
			poss = new Vector.<int>(mapNGrid);			
			
			//trace('create new map ' + w + '/' + h);
		}
		
		public function create(columns:Number, rows:Number, roomSize:Number = 2, pacmanMode:Boolean = false):void {
			this.pacmanMode = pacmanMode;
			mapsInit(columns, rows);
			mazeCreate();
			if (pacmanMode) pacman(columns,rows);
			scale(roomSize+1);
		}
		
		public function pacman(w:int, h:int):void {
			var i:int;
			var j:int;
			
			//trace('pacman, w ' + w + '/h ' + h);
			
			for (i = 0; i < w; i++) {
				for (j = 0; j < h; j++) {
					if (maps[i][j] == NORTH) {		
						if (j < h - 1) { 
							maps[i][j] |= SOUTH;
							maps[i][j + 1] |= NORTH;
							//trace('destroy south wall, res ' + maps[i][j] + '/res bawah ' + maps[i][j + 1] + '/pos ' + i + '-' + j);
						}
					}
					else if (maps[i][j] == EAST) {
						if (i > 0) {
							maps[i][j] |= WEST;
							maps[i - 1][j] |= EAST;
						}
					}
					else if (maps[i][j] == SOUTH) {
						if (j > 0) {
							maps[i][j] |= NORTH;
							maps[i][j - 1] |= SOUTH;
						}
					}
					else if (maps[i][j] == WEST) {
						if (i < (w - 1)) {
							maps[i][j] |= EAST;
							maps[i + 1][j] |= WEST;
						}
					}
				}
			}
		}
		
		/*
		protected function create2(w:int, h:int, mScale:int):void {
			trace('pacman mode ' + pacmanMode);
			
			create4(w, h);
			if (pacmanMode) pacman(w,h);
			scale(mScale);
		}
		*/
		
		/*
		protected function create4(w:int, h:int):void {
			mapsInit(w, h);
			mazeCreate();
		}
		*/
		
		protected function mazeCreate():void {
			var i:Number = 0;
			var j:Number = 0;
			var s:String;
			var result:Boolean = true;
			
			//init
			for (i = 0; i < mapWidth; i++) {
				for (j = 0; j < mapHeight; j++) {
					maps[i][j] = 0;
				}
			}
			
			randomPos();
			
			//awal sambungan
			j = Math.floor(Math.random() * (mapHeight / 2));
			i = Math.floor(Math.random() * (mapWidth / 2));
			
			maps[i][j] = maps[i][j] | EAST;
			maps[i + 1][j] = maps[i + 1][j] | WEST;
			
			while (true) {
				if (!cellConnect()) break;
			}
		}
		
		protected function randomPos():void {
			var a:Number = 0; 
			var b:Number = 0;
			var c:Number = 0; 
			var i:Number = 0;
			
			for (i = 0; i < mapNGrid; i++) poss[i] = i;
			
			for (i = 0; i < mapNGrid; i++) {
				a = Math.floor(Math.random() * (mapNGrid));
				b = Math.floor(Math.random() * (mapNGrid));
				c = poss[a];
				poss[a] = poss[b];
				poss[b] = c;
			}
		}
		
		protected function cellConnect():Boolean {
			var loop:Boolean = true;
			var ctr:Number = 0;
			var iz:Number = 0;
			var jz:Number = 0;
			var xx:Number = 0;
			var yy:Number = 0;
			var a:Number = 0;
			var b:Number = 0;
			var c:Number = 0;
			var i:Number = 0;
			var j:Number = 0;
			
			for (j = 0; j < mapNGrid; j++) {
				xx = poss[j] % mapWidth;
				yy = Math.floor(poss[j] / mapWidth);
				
				if (maps[xx][yy] == 0) {
					if (connectIsPossible(xx, yy)) {
						
						for (i = 0; i < 4; i++) {
							a = Math.floor((Math.random() * 4));
							b = Math.floor((Math.random() * 4));
							c = dirs[a];
							dirs[a] = dirs[b];
							dirs[b] = c;
						}
						
						for (i = 0; i < 4; i++) {
							if (connectNorth(i, xx, yy)) return true;
							if (connectSouth(i, xx, yy)) return true;
							if (connectEast(i, xx, yy)) return true;
							if (connectWest(i, xx, yy)) return true;
						}
						return true;
					}
				}
			}
			return false;
		}

		protected function connectIsPossible(i:Number, j:Number):Boolean {
			
			if (j > 0) { 
				if (maps[i][j - 1] > 0) return true;
			}
				
			if (j < (mapHeight - 1)) { 
				if (maps[i][j + 1] > 0) return true;
			}
				
			if (i > 0) { 
				if (maps[i - 1][j] > 0) return true;
			}
				
			if (i < (mapWidth - 1)) { 
				if (maps[i + 1][j] > 0) return true;
			}
			
			return false;
		}
		
		protected function connectNorth(i:Number, iz:Number, jz:Number):Boolean {
			if (dirs[i] == NORTH) { 
			if (jz > 0) { 
			if (maps[iz][jz] == 0) { 
			if (maps[iz][jz - 1] > 0) { 
				maps[iz][jz] = maps[iz][jz] | NORTH;
				maps[iz][jz - 1] = maps[iz][jz - 1] | SOUTH;
				return true;
			}
			}
			}
			}
			return false;
		}

		protected function connectSouth(i:Number, iz:Number, jz:Number):Boolean { 
			if (dirs[i] == SOUTH) { 
				if (jz < mapHeight - 1) {
					if (maps[iz][jz] == 0) { 
						if (maps[iz][jz + 1] > 0) { 
							maps[iz][jz] = maps[iz][jz] | SOUTH;
							maps[iz][jz + 1] = maps[iz][jz + 1] | NORTH;
							return true;
						}
					}
				}
			}
			return false;
		}

		protected function connectEast(i:Number, iz:Number, jz:Number):Boolean { 
			if (dirs[i] == EAST) {
				if (iz < mapWidth - 1) {
					if (maps[iz][jz] == 0) { 
						if (maps[iz + 1][jz] > 0) {
							maps[iz][jz] = maps[iz][jz] | EAST;
							maps[iz + 1][jz] = maps[iz + 1][jz] | WEST;
							return true;
						}
					}
				}
			}
			return false;
		}
		
		protected function connectWest(i:Number, iz:Number, jz:Number):Boolean {
			if (dirs[i] == WEST) {
				if (iz > 0) {
					if (maps[iz][jz] == 0) {
						if (maps[iz - 1][jz] > 0) {
							maps[iz][jz] = maps[iz][jz] | WEST;
							maps[iz - 1][jz] = maps[iz - 1][jz] | EAST;
							return true;
						}
					}
				}
			}
			return false;
		}
		
		protected function fromString(str:String):void {
			var ar:Array = [];
			var ar2:Array = [];
			var i:int;
			var j:int;
			
			//trace("from string: " + str);
			
			ar = str.split("|");
			
			for (i = 0; i < mapWidth; i++) {
				ar2 = String(ar[i]).split(",");
				for (j = 0; j < mapHeight; j++) {
					maps[i][j] = Number(ar2[j]);
				}
			}
		}
		
		protected function toString():String {
			var i:int;
			var j:int;
			var str:String;
			
			//*&|
			
			str = "";
			for (i = 0; i < mapWidth; i++) {				
				for (j = 0; j < mapHeight; j++) {
					str += maps[i][j] + ",";
				}
				str = str.slice(0, str.length - 1);
				str += "|";
			}
			str = str.slice(0, str.length - 1);
			
			//trace("toString " + str);
			return str;
		}
		
		public function get map():Vector.<Vector.<int>> {
			return _mapScales;
		}
		
		public function get width():Number {
			return map2Width;
		}
		
		public function get height():Number {
			return map2Height;
		}
		
	}

}