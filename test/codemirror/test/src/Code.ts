declare var CodeMirror: any;

window.onload = () => {
    let mTextArea: HTMLTextAreaElement = document.body.querySelector('textarea#code');
    let codeMirror: any = CodeMirror((elt: any) => {
        mTextArea.parentNode.replaceChild(elt, mTextArea);
    }, {
        value: mTextArea.value,
        mode: "javascript"
    });
    codeMirror;
}