interface V2D {
	x: number,
	y: number
}

interface V4D {
	p1: V2D,
	p2: V2D
}

interface KanvasObj {
	kanvas: HTMLCanvasElement,
	pemilik: Blok,
	kontek: CanvasRenderingContext2D
}

interface Blok {
	indexX: number;
	indexY: number;
	x: number;
	y: number;
	sx: number;
	sy: number;
	aktif: boolean;
	kanvas: KanvasObj;
	data: number[];
}

interface Karakter {
	x: number;
	y: number;
	gridX: number;
	gridY: number;
	posLayar: V2D;
}

