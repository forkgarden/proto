let karakter: Karakter = {
	x: 0,
	y: 0,
	gridX: 0,
	gridY: 0,
	posLayar: {
		x: 0,
		y: 0
	}
}


let gambarKotak: HTMLImageElement;

let kanvasAr: KanvasObj[] = [];
let bloks: Blok[] = [];
let kanvas: HTMLCanvasElement;
let kontek: CanvasRenderingContext2D;
let area: V4D = {
	p1: {
		x: 0,
		y: 0
	},
	p2: {
		x: 160,
		y: 120
	}
}

let debug: HTMLDivElement;

const blokP: number = 8;
const blokL: number = 10;
const kotakDim: number = 32;
const gp: number = 240;
const gl: number = 320;
const kecJalan: number = 8;
const duniaP: number = 2;
const duniaL: number = 2;
const blokP2: number = blokP * kotakDim;
const blokL2: number = blokL * kotakDim;
