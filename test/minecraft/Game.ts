
window.onload = () => {
	kanvas = document.body.querySelector("canvas#canvas");
	kontek = kanvas.getContext('2d');
	gambarKotak = document.body.querySelector("img#kotak");

	for (let i: number = 0; i < duniaP; i++) {
		for (let j: number = 0; j < duniaL; j++) {
			bloks.push(buatBlok(i, j));
		}
	}

	for (let i: number = 0; i < 4; i++) {
		let kanvas: KanvasObj = {
			kanvas: buatKanvas(),
			pemilik: null,
			kontek: null
		}
		kanvas.kontek = kanvas.kanvas.getContext('2d');

		kanvasAr.push(kanvas)
	}

	debug = document.body.querySelector("div#debug");

	requestAnimationFrame(update);

	window.onkeydown = (e: KeyboardEvent) => {
		// console.log(e.key);

		//kanan: 39
		if ('ArrowRight' == e.key) {
			karakter.x += kecJalan;
			gambar();
		}

		//kiri: 37
		if ('ArrowLeft' == e.key) {
			karakter.x -= kecJalan;
			if (karakter.x < 0) karakter.x = 0;
			gambar();
		}

		//atas: 38
		if ('ArrowUp' == e.key) {
			karakter.y -= kecJalan;
			if (karakter.y < 0) karakter.y = 0;
			gambar();
		}

		//bawah: 40
		if ('ArrowDown' == e.key) {
			karakter.y += kecJalan;
			gambar();
		}

	}
}

function da

function log(str: string): void {
	debug.innerHTML = str;
}

function buatKanvas(): HTMLCanvasElement {
	let canvas: HTMLCanvasElement = document.createElement('canvas');
	canvas.width = 320;
	canvas.height = 240;
	canvas.style.width = "240px";
	canvas.style.height = "320px";

	return canvas;
}

function buatBlok(i: number, j: number): Blok {
	let blok: Blok = {
		x: i * blokP2,
		y: j * blokL2,
		indexX: i,
		indexY: j,
		sx: 0,
		sy: 0,
		aktif: true,
		kanvas: null,
		data: []
	}

	return blok;
}

// function diDalamArea2(x: number, y: number): boolean {
// 	if (Math.abs(x - gp / 2) > gp / 2) return false;
// 	if (Math.abs(y - gl / 2) > gl / 2) return false;

// 	return true;
// }

// function diDalamArea3(x: number, y: number): boolean {
// 	if (x >= 0) {
// 		if (x <= gp) {
// 			if (y >= 0) {
// 				if (y <= gl) {
// 					return true;
// 				}
// 			}
// 		}
// 	}

// 	return false;
// }

function diDalamArea(x: number, y: number): boolean {
	let panjangMin: number = gp / 2 + blokP2 / 2;
	let lebarMin: number = gl / 2 + blokL2 / 2;

	if (Math.abs(x - gp / 2) > panjangMin) {
		return false;
	}

	if (Math.abs(y - gl / 2) > lebarMin) {
		return false;
	}

	return true;
}

function ambilKanvas(): KanvasObj {
	let hasil: KanvasObj;

	kanvasAr.forEach((item: KanvasObj) => {
		if (!item.pemilik) {
			hasil = item;
		}
	});

	return hasil;
}

function updateArea() {
	//update view port
	area.p1.x = karakter.x - gp / 2;
	area.p1.y = karakter.y - gl / 2;

	if (area.p1.x < 0) area.p1.x = 0;
	if (area.p1.y < 0) area.p1.y = 0;

	area.p2.x = area.p1.x + gp;
	area.p2.y = area.p1.y + gl;
}

function blokGambarKanvas(blok: Blok): void {
	blok.data.forEach((item: number, idx) => {
		let x: number = idx % 10;
		let y: number = Math.floor(idx / 10);

		if (item > 0) {
			blok.kanvas.kontek.drawImage(gambarKotak, x * kotakDim, y * kotakDim);
		}
	})
}

function updateBlok(): void {
	//update block
	bloks.forEach((item: Blok) => {
		item.sx = item.x - area.p1.x;
		item.sy = item.y - area.p1.y;

		if (diDalamArea(item.sx + blokP2 / 2, item.sy + blokL2 / 2)) {
			item.aktif = true;
			if (!item.kanvas) {
				//TODO: ambilkanvas
				item.kanvas = ambilKanvas();
				item.kanvas.pemilik = item;
			}
		}
		else {
			item.aktif = false;
			item.kanvas.pemilik = null;
			item.kanvas = null;
		}
	})

}

function update(): void {

	updateArea();

	karakter.posLayar.x = karakter.x - area.p1.x;
	karakter.posLayar.y = karakter.y - area.p1.y;

	updateBlok();
	gambar();

	setTimeout(() => {
		requestAnimationFrame(update);
	}, 50);
}

function gambar(): void {
	kontek.clearRect(0, 0, gp, gl);

	//gambar karakter
	kontek.beginPath();
	kontek.rect(karakter.x - area.p1.x - 2, karakter.y - area.p1.y - 2, 4, 4);
	kontek.stroke();

	//gambar viewport 
	kontek.beginPath();
	kontek.rect(0, 0, area.p2.x - area.p1.x + 1, area.p2.y - area.p1.y + 1);
	kontek.stroke();

	let str: string = '';
	bloks.forEach((item: Blok) => {

		if (item.aktif == false) {
			return;
		}


		str += 'item aktif x ' + item.x + '/' + item.y + "/sx " + item.sx + '/sy ' + item.sy + "<br/>";
		kontek.beginPath();
		kontek.rect(item.sx, item.sy, blokP2, blokL2);
		kontek.stroke();
	});

	let blok: Blok = bloks[0];
	str += "blok 1 " + blok.aktif + " xy " + blok.x + '/' + blok.y + "/sx-sy " + blok.sx + "/" + blok.sy + "<br/>";
	str += "tengah " + (blok.sx + gp / 2) + "/" + (blok.sy + gl / 2) + "<br/>";
	str += "didalam kotak " + diDalamArea((blok.sx + gp / 2), (blok.sy + gl / 2)) + "<br/>";
	str += "jumlah bloks " + bloks.length + "<br/>";
	str += "blok 0 aktif " + bloks[0].aktif;
	log(str);
}


