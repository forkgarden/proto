class Test {

    constructor() {

        console.log('this is a test class');

        let string1 = 'string 1';
        console.log(this.initTemplate(string1));

        string1 = 'string 2';
        console.log(this.initTemplate(string1));
    }

    initTemplate(str:string):string {
        return `template - ${str} - string`;
    }
}