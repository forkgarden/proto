var mousePencet: boolean = false;
var mouseGerak: boolean = false;
var mouseStartX: number = 0;
var mouseStartY: number = 0;
var mouseDeltaX: number = 0;
var mouseDeltaY: number = 0;
var mousePosX: number = 0;
var mousePosY: number = 0;
var p: HTMLParagraphElement = document.body.querySelector('p.p');

document.body.ontouchmove = (e: TouchEvent) => {
    mousePosX = e.touches[0].clientX;
    mousePosY = e.touches[0].clientY;
    mouseGerak=true;
}

document.body.onmousemove = (e: MouseEvent) => {
    mousePosX = e.clientX;
    mousePosY = e.clientY;
    mouseGerak=true;
}

document.body.ontouchstart = (e: TouchEvent) => {
    e.stopPropagation();
    e.preventDefault();
    console.log('touch down');
    mouseDiPencet(e.touches[0].clientX, e.touches[0].clientY);
}

document.body.onmousedown = (e: MouseEvent) => {
    e.stopPropagation();
    e.preventDefault();
    console.log('mouse down');
    mouseDiPencet(e.clientX, e.clientY);
}

document.body.ontouchend = (e: TouchEvent) => {
    e.stopPropagation();
    e.preventDefault();
    mouseDilepas();
}

document.body.onmouseup = (e: MouseEvent) => {
    e.stopPropagation();
    e.preventDefault();
    mouseDilepas();
}

function mouseDiPencet(x: number, y: number): void {
    if (mousePencet) return;
    mousePencet = true;
    document.body.style.overflowY = 'hidden';
    mouseStartX = x;
    mouseStartY = y;

    let d:DOMRect = (p.getBoundingClientRect() as DOMRect);

    p.style.left = d.x + 'px';
    p.style.top = (d.y + document.body.scrollTop) + 'px';

    // console.log(x,y);
    // console.log(document.body.scrollTop);
    // console.log(p.style.left + '/' + p.style.top);
}

function mouseDilepas(): void {
    if (!mousePencet) return;
    mousePencet = false;
    mouseGerak=false;
    console.log('lepas');
    document.body.style.overflowY = 'auto';
}

function ulang(): void {
    // return;
    setTimeout(() => {
        if (mousePencet && mouseGerak) {

            mouseDeltaX = mousePosX - mouseStartX;
            mouseDeltaY = mousePosY - mouseStartY;
            p.style.left = (mouseStartX + mouseDeltaX) + 'px';
            p.style.top = (mouseStartY + mouseDeltaY + document.body.scrollTop) + 'px';
            window.document.title = mouseDeltaX + '';
            // console.log(p);
        }

        ulang();
    }, 100);
}
ulang();