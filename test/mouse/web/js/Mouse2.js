"use strict";
var mousePencet = false;
var mouseGerak = false;
var mouseStartX = 0;
var mouseStartY = 0;
var mouseDeltaX = 0;
var mouseDeltaY = 0;
var mousePosX = 0;
var mousePosY = 0;
var p = document.body.querySelector('p.p');
document.body.ontouchmove = (e) => {
    mousePosX = e.touches[0].clientX;
    mousePosY = e.touches[0].clientY;
    mouseGerak = true;
};
document.body.onmousemove = (e) => {
    mousePosX = e.clientX;
    mousePosY = e.clientY;
    mouseGerak = true;
};
document.body.ontouchstart = (e) => {
    e.stopPropagation();
    e.preventDefault();
    console.log('touch down');
    mouseDiPencet(e.touches[0].clientX, e.touches[0].clientY);
};
document.body.onmousedown = (e) => {
    e.stopPropagation();
    e.preventDefault();
    console.log('mouse down');
    mouseDiPencet(e.clientX, e.clientY);
};
document.body.ontouchend = (e) => {
    e.stopPropagation();
    e.preventDefault();
    mouseDilepas();
};
document.body.onmouseup = (e) => {
    e.stopPropagation();
    e.preventDefault();
    mouseDilepas();
};
function mouseDiPencet(x, y) {
    if (mousePencet)
        return;
    mousePencet = true;
    document.body.style.overflowY = 'hidden';
    mouseStartX = x;
    mouseStartY = y;
    let d = p.getBoundingClientRect();
    p.style.left = d.x + 'px';
    p.style.top = (d.y + document.body.scrollTop) + 'px';
    // console.log(x,y);
    // console.log(document.body.scrollTop);
    // console.log(p.style.left + '/' + p.style.top);
}
function mouseDilepas() {
    if (!mousePencet)
        return;
    mousePencet = false;
    mouseGerak = false;
    console.log('lepas');
    document.body.style.overflowY = 'auto';
}
function ulang() {
    // return;
    setTimeout(() => {
        if (mousePencet && mouseGerak) {
            mouseDeltaX = mousePosX - mouseStartX;
            mouseDeltaY = mousePosY - mouseStartY;
            p.style.left = (mouseStartX + mouseDeltaX) + 'px';
            p.style.top = (mouseStartY + mouseDeltaY + document.body.scrollTop) + 'px';
            window.document.title = mouseDeltaX + '';
            // console.log(p);
        }
        ulang();
    }, 100);
}
ulang();
