"use strict";
let Pohon = {
    aktif: null,
    buat: () => {
        console.log('pohon buat');
        let desk = window.prompt('Deskripsi:');
        let pObj = {
            desk: desk,
            class: [],
            style: [],
            attr: [],
            anggota: []
        };
        pObj.view = Pohon.buatView();
        pObj.view.desk.innerHTML = pObj.desk;
        Pohon.aktif.anggota.push(pObj);
        Pohon.render(pObj, Pohon.aktif.view.cont);
        Pohon.db.simpan();
    },
    hapus: () => {
        if (Pohon.aktif.root)
            return;
        let induk = Pohon.aktif.induk;
        for (let i = induk.anggota.length - 1; i >= 0; i--) {
            if (induk.anggota[i] == Pohon.aktif) {
                induk.anggota.splice(i, 1);
            }
        }
        Pohon.aktif.view.el.parentElement.removeChild(Pohon.aktif.view.el);
        Pohon.aktif = induk;
        Pohon.db.simpan();
    },
    setup: (pohon) => {
        pohon.anggota.forEach((item) => {
            item.induk = pohon;
            Pohon.setup(item);
        });
    },
    toObj: (p) => {
        let hasil = {
            attr: p.attr.slice(),
            class: p.attr.slice(),
            desk: p.desk,
            style: p.style.slice(),
            id: p.id,
            root: p.root,
            tag: p.tag,
            anggota: []
        };
        p.anggota.forEach((item) => {
            hasil.anggota.push(Pohon.toObj(item));
        });
        return hasil;
    },
    init: () => { },
    buatView: () => {
        let template = document.body.querySelector('template').content;
        let el = template.querySelector('div.pohon').cloneNode(true);
        let view = {};
        view.el = el;
        view.cont = el.querySelector('div.anggota');
        view.desk = el.querySelector('div.desk span');
        view.tombol = el.querySelector('div.desk button.menu');
        return view;
    },
    render: (pohon, cont) => {
        pohon.view = Pohon.buatView();
        pohon.view.desk.innerHTML = pohon.desk;
        cont.appendChild(pohon.view.el);
        if (pohon.root) {
            pohon.view.el.classList.add('pertama');
            pohon.view.el.classList.remove('sembunyi');
        }
        pohon.view.desk.onclick = (e) => {
            e.stopPropagation();
            pohon.view.el.classList.toggle('sembunyi');
            Pohon.aktif = pohon;
        };
        pohon.view.tombol.onclick = (e) => {
            e.stopPropagation();
            Pohon.aktif = pohon;
            Pohon.menu2.reset();
            Pohon.menu2.el.style.display = 'block';
            Pohon.menu2.buatTombol('tambah', () => {
                Pohon.buat();
            });
            Pohon.menu2.buatTombol('hapus', () => {
                Pohon.hapus();
            });
        };
        pohon.anggota.forEach((item) => {
            Pohon.render(item, pohon.view.cont);
        });
    },
    renderTag: (p, cont) => {
        if (!p.tag) {
            p.tag = 'div';
        }
        let el = document.createElement(p.tag);
        cont.appendChild(el);
        p.anggota.forEach((p) => {
            Pohon.renderTag(p, el);
        });
        return el;
    },
    buatHTmlString: () => {
        let cont = document.createElement('div');
        Pohon.renderTag(body, cont);
        let str = cont.innerHTML;
        return str;
    }
};
