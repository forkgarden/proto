"use strict";
Pohon.menu = {
    init: () => {
        let m = Pohon.menu;
        m.el = document.body.querySelector('div.menu.back');
        m.hapusTbl = m.el.querySelector('button.hapus');
        m.editTbl = m.el.querySelector('button.edit');
        m.hapusTbl = m.el.querySelector('button.hapus');
        m.tambahTbl = m.el.querySelector('button.tambah');
        m.hapusTbl.onclick = (e) => {
            e.stopPropagation();
            m.el.style.display = 'none';
            console.log('hapus click');
        };
        m.editTbl.onclick = (e) => {
            e.stopPropagation();
            m.el.style.display = 'none';
            console.log('edit click');
        };
        m.tambahTbl.onclick = (e) => {
            e.stopPropagation();
            Pohon.buat();
            m.el.style.display = 'none';
            console.log('tambah click');
        };
        m.el.onclick = (e) => {
            e.stopPropagation();
            m.el.style.display = 'none';
            m.el.style.display = 'none';
        };
    }
};
