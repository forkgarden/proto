"use strict";
Pohon.menu2 = {
    el: null,
    box: null,
    init: () => {
        let m = Pohon.menu2;
        m.el = document.querySelector('div.menu2.back');
        m.box = m.el.querySelector('div.menu-box');
        m.el.onclick = (e) => {
            e.stopPropagation();
            m.el.style.display = 'none';
        };
        m.box.onclick = (e) => {
            e.stopPropagation();
            m.el.style.display = 'none';
        };
    },
    reset: () => {
        let m = Pohon.menu2;
        m.box.innerHTML = '';
    },
    buatTombol: (nama, f) => {
        let m = Pohon.menu2;
        let tbl = document.createElement('button');
        tbl.innerText = nama;
        m.box.appendChild(tbl);
        tbl.onclick = (e) => {
            e.stopPropagation();
            m.el.style.display = 'none';
            f();
        };
    }
};
