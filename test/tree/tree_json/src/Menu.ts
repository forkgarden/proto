Pohon.menu = {
	init: () => {
		let m: IMenu = Pohon.menu;

		m.el = document.body.querySelector('div.menu.back') as HTMLDivElement;
		m.hapusTbl = m.el.querySelector('button.hapus') as HTMLButtonElement;
		m.editTbl = m.el.querySelector('button.edit') as HTMLButtonElement;
		m.hapusTbl = m.el.querySelector('button.hapus') as HTMLButtonElement;
		m.tambahTbl = m.el.querySelector('button.tambah') as HTMLButtonElement;

		m.hapusTbl.onclick = (e: MouseEvent): void => {
			e.stopPropagation();

			m.el.style.display = 'none';
			console.log('hapus click');
		}

		m.editTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			m.el.style.display = 'none';
			console.log('edit click');
		}

		m.tambahTbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			Pohon.buat();
			m.el.style.display = 'none';
			console.log('tambah click');
		}

		m.el.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			m.el.style.display = 'none';
			m.el.style.display = 'none';
		}

	}
}

interface IMenu {
	init?: () => void;
	el?: HTMLElement;
	hapusTbl?: HTMLButtonElement;
	editTbl?: HTMLButtonElement;
	tambahTbl?: HTMLButtonElement;
}

