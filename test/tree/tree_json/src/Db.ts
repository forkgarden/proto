Pohon.db = {
	simpan: (): void => {
		let str: string = JSON.stringify(Pohon.toObj(body));
		window.localStorage.setItem(tableDb, str);
	},

	muat: (): void => {
		let str: string = window.localStorage.getItem(tableDb);

		if (str) {
			body = JSON.parse(str);
		}
		else {
			body = {
				desk: 'body',
				root: true,
				class: [],
				style: [],
				attr: [],
				anggota: []
			}
		}
	}
}

interface IDb {
	simpan?: () => void;
	muat?: () => void;
}