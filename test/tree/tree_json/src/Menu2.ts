Pohon.menu2 = {
	el: null,
	box: null,
	init: () => {
		let m: IMenu2 = Pohon.menu2;
		m.el = document.querySelector('div.menu2.back');
		m.box = m.el.querySelector('div.menu-box');

		m.el.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			m.el.style.display = 'none';
		}

		m.box.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			m.el.style.display = 'none';
		}
	},
	reset: () => {
		let m: IMenu2 = Pohon.menu2;
		m.box.innerHTML = '';
	},
	buatTombol: (nama: string, f: Function) => {
		let m: IMenu2 = Pohon.menu2;

		let tbl: HTMLButtonElement = document.createElement('button') as HTMLButtonElement;
		tbl.innerText = nama;
		m.box.appendChild(tbl);

		tbl.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			m.el.style.display = 'none';
			f();
		}
	}
}

interface IMenu2 {
	el: HTMLDivElement;
	box: HTMLDivElement;
	init: () => void;
	buatTombol: (nama: string, f: Function) => void;
	reset: () => void;
}