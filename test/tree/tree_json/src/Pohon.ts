let Pohon: IPohonController = {
	aktif: null,

	buat: (): void => {
		console.log('pohon buat');
		let desk: string = window.prompt('Deskripsi:');
		let pObj: IPohonObj = {
			desk: desk,
			tag: desk,
			class: [],
			style: [],
			attr: [],
			anggota: []
		}

		pObj.view = Pohon.buatView();
		pObj.view.desk.innerHTML = pObj.desk;

		Pohon.aktif.anggota.push(pObj);

		Pohon.render(pObj, Pohon.aktif.view.cont);

		Pohon.db.simpan();
	},

	hapus: () => {
		if (Pohon.aktif.root) return;
		let induk: IPohonObj = Pohon.aktif.induk;

		for (let i: number = induk.anggota.length - 1; i >= 0; i--) {
			if (induk.anggota[i] == Pohon.aktif) {
				induk.anggota.splice(i, 1);
			}
		}

		Pohon.aktif.view.el.parentElement.removeChild(Pohon.aktif.view.el);
		Pohon.aktif = induk;
		Pohon.db.simpan();
	},

	setup: (pohon: IPohonObj) => {
		pohon.anggota.forEach((item: IPohonObj) => {
			item.induk = pohon;
			Pohon.setup(item);
		});
	},

	index: (): number => {
		let induk: IPohonObj = Pohon.aktif.induk;
		let index: number = 0;

		for (let i: number = 0; i < induk.anggota.length; i++) {
			if (induk.anggota[i] == Pohon.aktif) {
				index = i;
			}
		}

		return index;
	},

	tukar: (idx1: number, idx2: number): void => {
		let induk: IPohonObj = Pohon.aktif.induk;
		let p: IPohonObj = induk.anggota[idx1];

		induk.anggota[idx1] = induk.anggota[idx2];
		induk.anggota[idx2] = p;
	},

	geserAtas: (): void => {
		if (Pohon.aktif.root) return;
		let index: number = 0;

		index = Pohon.index();

		if (index > 0) {
			Pohon.tukar(index, index - 1);
		}
	},

	geserBawah: (): void => {
		let index: number = 0;
		index;
	},
	editClass: (): void => { },
	editCss: (): void => { },

	toObj: (p: IPohonObj): IPohonObj => {
		let hasil: IPohonObj = {
			attr: p.attr.slice(),
			class: p.attr.slice(),
			desk: p.desk,
			style: p.style.slice(),
			id: p.id,
			root: p.root,
			tag: p.tag,
			anggota: []
		}

		p.anggota.forEach((item: IPohonObj) => {
			hasil.anggota.push(Pohon.toObj(item));
		});

		return hasil;
	},

	init: () => { },

	buatView: (): IPohonView => {
		let template: DocumentFragment = document.body.querySelector('template').content;
		let el: HTMLDivElement = template.querySelector('div.pohon').cloneNode(true) as HTMLDivElement;
		let view: IPohonView = {}

		view.el = el;
		view.cont = el.querySelector('div.anggota');
		view.desk = el.querySelector('div.desk span');
		view.tombol = el.querySelector('div.desk button.menu');

		return view;
	},

	render: (pohon: IPohonObj, cont: HTMLElement) => {
		pohon.view = Pohon.buatView();
		pohon.view.desk.innerHTML = pohon.tag;
		cont.appendChild(pohon.view.el);

		if (pohon.root) {
			pohon.view.el.classList.add('pertama');
			pohon.view.el.classList.remove('sembunyi');
		}

		pohon.view.desk.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			pohon.view.el.classList.toggle('sembunyi');
			Pohon.aktif = pohon;
		}

		pohon.view.tombol.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			Pohon.aktif = pohon;
			Pohon.renderMenu();
		}

		pohon.anggota.forEach((item: IPohonObj) => {
			Pohon.render(item, pohon.view.cont);
		})
	},

	renderMenu: () => {
		let m: IMenu2 = Pohon.menu2;

		m.reset();
		m.el.style.display = 'block';

		m.buatTombol('tambah', () => {
			Pohon.buat();
		});

		m.buatTombol('hapus', () => {
			Pohon.hapus();
		});

		m.buatTombol('geser atas', () => {

		});

		m.buatTombol('geser bawah', () => {

		});

		m.buatTombol('edit class', () => { });
		m.buatTombol('edit style', () => { });
		m.buatTombol('edit tag', () => { });
	},

	renderTag: (p: IPohonObj, cont: HTMLElement): HTMLElement => {
		if (!p.tag) {
			p.tag = 'div';
		}

		let el: HTMLElement = document.createElement(p.tag);
		cont.appendChild(el);

		p.anggota.forEach((p: IPohonObj) => {
			Pohon.renderTag(p, el);
		});

		return el;
	},

	buatHTmlString: (): string => {
		let cont: HTMLDivElement = document.createElement('div');
		Pohon.renderTag(body, cont);
		let str: string = cont.innerHTML;

		return str;
	}

};

interface IPohonObj {
	desk: string;
	anggota: IPohonObj[];
	view?: IPohonView;
	induk?: IPohonObj;
	root?: boolean;

	//v02
	tag?: string;
	id?: string;
	class: IKV[]
	style: IKV[]
	attr: IKV[]
}

interface IKV {
	kunci: string,
	nilai: string
}

interface IPohonView {
	el?: HTMLDivElement
	desk?: HTMLDivElement,
	cont?: HTMLDivElement,
	tombol?: HTMLButtonElement
}

interface IPohonController {
	init?: () => void;
	render?: (pohon: IPohonObj, cont: HTMLElement) => void;
	renderMenu?: () => void;
	buatView?: () => IPohonView;
	setup: (pohon: IPohonObj) => void;
	toObj: (p: IPohonObj) => IPohonObj;
	buat?: () => void;
	hapus?: () => void;
	geserAtas: () => void;
	geserBawah: () => void;
	editCss: () => void;
	editClass: () => void;
	index: () => number;
	tukar: (idx1: number, idx2: number) => void;
	menu2?: IMenu2;
	// editor?: IEditorHTML;
	db?: IDb;
	aktif: IPohonObj;
	renderTag: (pohon: IPohonObj, cont: HTMLElement) => HTMLElement;
	buatHTmlString: () => string;
}






