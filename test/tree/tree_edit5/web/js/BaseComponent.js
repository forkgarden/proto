export class BaseComponent {
    constructor() {
        this._template = '';
        this._elHtml = document.createElement('div');
        // public set onReturn(value: Function) {
        // console.log(this);
        // console.log('set on return');
        // console.log(value);
        // this._onReturn = value;
        // }
        // public set onCancel(value: Function) {
        // 	this._onCancel = value;
        // }
    }
    // protected _parent: HTMLElement = null;
    // protected _onReturn: Function = null;
    // protected _onCancel: Function = null;
    // onRender(): void {
    // }
    destroy() {
        if (this.elHtml.parentElement)
            this.elHtml.parentElement.removeChild(this.elHtml);
        this.destroyRec(this.elHtml);
    }
    destroyRec(el) {
        while (el.firstElementChild) {
            let el2 = el.removeChild(el.firstElementChild);
            if (el2) {
                this.destroyRec(el2);
            }
        }
    }
    // mulai():void {
    // }
    onBuild() {
    }
    onAttach() {
    }
    onDetach() {
    }
    onShow() {
    }
    onHide() {
    }
    attach(parent) {
        parent.appendChild(this._elHtml);
        // this._parent = parent;
        this.onAttach();
    }
    detach() {
        if (this._elHtml.parentElement) {
            this._elHtml.parentElement.removeChild(this._elHtml);
        }
        this.onDetach();
    }
    show(el) {
        if (!el) {
            el = this._elHtml;
        }
        el.style.display = 'block';
        this.onShow();
    }
    hide(el) {
        if (!el) {
            el = this._elHtml;
        }
        el.style.display = 'none';
        this.onHide();
    }
    build() {
        let div = document.createElement('div');
        let el;
        div.innerHTML = this._template;
        el = div.firstElementChild;
        if (el == null) {
            console.log(this._template);
            throw new Error('failed to build el');
        }
        if (div.childElementCount == 0) {
            console.error('should be wrapped as singgle element');
        }
        this._elHtml = el;
        this.onBuild();
    }
    getEl(query) {
        let el;
        el = this._elHtml.querySelector(query);
        if (el) {
            return el;
        }
        else {
            console.log(this._elHtml);
            console.log(query);
            throw new Error('query not found ');
        }
    }
    // render(parent: HTMLElement): void {
    // this.build();
    // this.attach(parent);
    // this._parent = parent;
    // this.onRender();
    // }
    get template() {
        return this._template;
    }
    set template(str) {
        this._template = str;
    }
    get elHtml() {
        return this._elHtml;
    }
    set elHtml(value) {
        this._elHtml = value;
    }
}
