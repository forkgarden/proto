export class TreeDb {
    constructor() {
        this.list = [];
    }
    init(tree) {
        return Promise.resolve().then(() => {
            this.tree = tree;
            let storage = window.localStorage;
            let data = storage.getItem('tree');
            console.log('load');
            console.log(data);
            this.string2List(data);
        });
    }
    idCreate() {
        let hasil = this.list.length;
        let hasilStr = hasil + '';
        while (true) {
            if (this.checkDuplicate(hasilStr)) {
                hasil++;
                hasilStr = hasil + '';
            }
            else {
                return hasilStr;
            }
        }
    }
    checkDuplicate(id) {
        let hasil = false;
        this.list.forEach((item) => {
            if (item.id == id) {
                hasil = true;
            }
        });
        return hasil;
    }
    get() {
        return Promise.resolve().then(() => {
            return this.list;
        });
    }
    insert(data) {
        return Promise.resolve().then(() => {
            data.id = this.idCreate();
            this.list.push(data);
            this.simpan();
        });
    }
    getById(ids) {
        return Promise.resolve().then(() => {
            let hasil;
            this.list.forEach((item) => {
                if (item.id == ids) {
                    hasil = item;
                }
            });
            return hasil;
        });
    }
    update(data) {
        return Promise.resolve().then(() => {
            data;
            this.simpan();
        });
    }
    //additional
    // fromObj(obj: any): ITreeItemObj {
    //     let tree: ITreeItemObj = new TreeItemObj();
    //     tree.id = obj.id;
    //     tree.label = obj.label;
    //     tree.parentId = obj.parentId;
    //     tree.type = obj.type;
    //     return tree;
    // }
    string2List(str) {
        let dataAr;
        while (this.list.length > 0) {
            this.list.pop();
        }
        dataAr = JSON.parse(str);
        dataAr.forEach((item) => {
            this.list.push(this.tree.fromObj(item));
        });
    }
    // toObj(tree: ITreeItemObj): ITreeItemObj {
    //     let obj: any = {};
    //     obj.id = tree.id;
    //     obj.label = tree.label;
    //     obj.parentId = tree.parentId;
    //     obj.type = tree.type;
    //     return obj;
    // }
    list2String() {
        let data = [];
        this.list.forEach((item) => {
            data.push(this.tree.toObj(item));
        });
        return JSON.stringify(data);
    }
    simpan() {
        let storage = window.localStorage;
        let dataStr = "";
        let dataStr2 = '';
        //validate
        dataStr = this.list2String();
        this.string2List(dataStr);
        dataStr2 = this.list2String();
        if (dataStr != dataStr2) {
            console.log(dataStr);
            console.log(dataStr2);
            throw new Error();
        }
        storage.setItem('tree', dataStr);
    }
}
