import { BaseComponent } from "../BaseComponent.js";
export class EditDialog extends BaseComponent {
    constructor() {
        super();
        this.insertMode = false;
        this._template = `
            <div class='edit-page'>
                <div class='box'>
                    <div class='header'><button class='close'>X</button></div>
                    <label>Nama Folder:</label><br/>
                    <input type='text' name='nama' value='test'/><br/>
                    <br/>
                    <button class='ok'>OK</button>
                </div>
            </div>
        `;
        this.build();
        this.closeTbl.onclick = () => {
            this.detach();
        };
    }
    okClick() {
        this.item.label = this.namaInput.value;
        this.detach();
        //TODO:
        this.insertMode;
        this.db;
        this.tree;
        // if (this.insertMode) {
        //     this.tree.insert2(this.item).then(() => {
        //         this.detach();
        //         return Promise.resolve();
        //     }).catch((e: Error) => {
        //         console.log(e);
        //     });
        // }
    }
    mulai(insertMode, item, tree, db) {
        this.insertMode = insertMode;
        this.item = item;
        this.tree = tree;
        this.db = db;
    }
    get namaInput() {
        return this.getEl('input[type=text]');
    }
    get closeTbl() {
        return this.getEl('button.close');
    }
    get OkTbl() {
        return this.getEl('button.ok');
    }
}
