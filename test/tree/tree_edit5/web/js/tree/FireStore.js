///<reference path="firestore.d.ts"/>
// import { TreeItemObj } from "./TreeItemObj";
// import { Tree } from "./Tree";
export class FireStoreDB {
    // private tree:Tree;
    constructor() {
    }
    // init(tree:Tree):void {
    // 	this.tree = tree;
    // }
    initFireStore() {
        // Your web app's Firebase configuration
        console.log('firestore init');
        var firebaseConfig = {
            apiKey: "AIzaSyD8BI905B0DldnX6pgYJGK7X5J5jqy2NdU",
            authDomain: "blog-1513057469147.firebaseapp.com",
            databaseURL: "https://blog-1513057469147.firebaseio.com",
            projectId: "blog-1513057469147",
            storageBucket: "blog-1513057469147.appspot.com",
            messagingSenderId: "592396008462",
            appId: "1:592396008462:web:b9432343573f25d3cb505f",
            measurementId: "G-FDL2EWZ65Q"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
        this.db = firebase.firestore();
        return this.db.enablePersistence();
    }
    get() {
        return this.db.collection('tree').get();
    }
    update(data) {
        return this.db.collection("tree").doc(data.id + '').update(data);
    }
    insert(data) {
        console.log('add data');
        console.log(data);
        return this.db.collection("tree").add(data);
    }
}
