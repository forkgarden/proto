import { BaseComponent } from "../BaseComponent.js";
import { TreeItemObj } from "./TreeItemObj.js";
import { EditDialog } from "./EditDialog.js";
import { TreeItemView } from "./View.js";
import { Const } from "./Const.js";
export class TreePage extends BaseComponent {
    constructor() {
        super();
        this.viewCr = null;
        this.tree = null;
        this.cont = null;
        this.viewList = [];
        this._template = `
            <div class='tree-page'>
                <div class='kiri'></div>
                <div class='kanan'>
                    <button class='baru'>folder baru</button>
                    <button class='file'>file baru</button>
                    <button class='edit'>edit</button>
                    <button class='hapus'>hapus</button>
                </div>
            </div>
        `;
        this.build();
        this.baruTbl.onclick = () => {
            let item = new TreeItemObj();
            console.log('baru click');
            if (this.viewCr) {
                item.parentId = this.viewCr.tree.id;
            }
            item.type = Const.ITEM_FOLDER;
            this.dialog(true, item, this.tree, this.db);
        };
        this.editTbl.onclick = () => {
            if (!this.viewCr)
                return;
            console.log('edit click');
            this.dialog(false, this.viewCr.tree, this.tree, this.db);
        };
        this.fileBaruTbl.onclick = () => {
            let item = new TreeItemObj();
            console.log('file baru click');
            if (this.viewCr) {
                item.parentId = this.viewCr.tree.id;
                if (this.viewCr.tree.type != Const.ITEM_FOLDER) {
                    console.log('not folder');
                    return;
                }
            }
            item.type = Const.ITEM_FILE;
            this.dialog(true, item, this.tree, this.db);
        };
        this.deleteTbl.onclick = () => {
            if (!this.viewCr) {
                console.log('no view selected');
                return;
            }
            if (this.tree.getChilds(this.viewCr.tree.id).length > 0) {
                console.log('has child');
                return;
            }
            if (this.viewCr) {
                this.tree.delete(this.viewCr.tree.id);
                this.reload();
            }
        };
    }
    dialog(insert, item, tree, db) {
        let dialog = new EditDialog();
        console.log('baru click');
        dialog.mulai(insert, item, tree, db);
        dialog.attach(this.cont);
        dialog.OkTbl.onclick = () => {
            dialog.okClick();
            if (insert) {
                this.db.insert(tree.toObj(item)).then((res) => {
                    item.id = res.id;
                    this.reload();
                }).catch((e) => {
                    console.log(e);
                });
            }
            else {
                this.db.update(tree.toObj(item)).then(() => {
                    this.reload();
                }).catch((e) => {
                    console.log(e);
                });
            }
        };
    }
    reload() {
        this.detach();
        this.destroy();
        let page = new TreePage();
        page.mulai(this.tree, this.cont, this.db);
        page.attach(this.cont);
    }
    destroy() {
        this.baruTbl.onclick = null;
        this.editTbl.onclick = null;
        this.deleteTbl.onclick = null;
        this._elHtml.innerText = '';
        this._template = '';
    }
    mulai(tree, mCont, db) {
        this.tree = tree;
        this.cont = mCont;
        this.db = db;
        this.db.get().then((res) => {
            this.render("", this.contTree);
        });
        // this.tree.clear();
        // this.db.get().then((res:any[]) => {
        //     console.log(res);
        //     res.forEach((item:any)=>{
        //         console.log(item.id, '=>', item.data());
        //         let item2:TreeItemObj = new TreeItemObj();
        //         item2.id = item.id;
        //         item2.label = item.data().label;
        //         item2.parentId = item.data().parentId?item.data().parentId:-1;
        //         item2.type = item.data().type;
        //         this.tree.list.push(item2);
        //     });
        //     this.render("",this.contTree);
        // })
        // .catch((e:Error) => {
        //     console.log(e);
        // });
    }
    render(parentId, cont) {
        let list = this.tree.getChilds(parentId);
        if (parentId == "") {
            cont.innerHTML = '';
            while (this.viewList.length > 0) {
                this.viewList.pop();
            }
        }
        list.forEach((item) => {
            let view = new TreeItemView();
            view.tree = item;
            view.judulTbl.innerText = item.label;
            if (item.type == Const.ITEM_FILE) {
                view.spanIcon.innerText = '📄';
            }
            else {
                view.spanIcon.innerText = '📁';
            }
            view.attach(cont);
            this.viewList.push(view);
            view.judulTbl.onclick = (e) => {
                e.stopPropagation();
                if (this.viewCr) {
                    this.viewCr.judulTbl.classList.remove('selected');
                    if (this.viewCr.tree.id == view.tree.id) {
                        this.viewCr = null;
                        return;
                    }
                }
                view.judulTbl.classList.add('selected');
                this.viewCr = view;
            };
            this.render(view.tree.id, view.cont);
        });
        if (this.viewCr) {
            this.viewList.forEach((item) => {
                if (item.tree.id == this.viewCr.tree.id) {
                    item.judulTbl.classList.add('selected');
                    this.viewCr = item;
                }
            });
        }
    }
    get baruTbl() {
        return this.getEl('div.kanan button.baru');
    }
    get deleteTbl() {
        return this.getEl('div.kanan button.hapus');
    }
    get fileBaruTbl() {
        return this.getEl('button.file');
    }
    get editTbl() {
        return this.getEl('button.edit');
    }
    get contTree() {
        return this.getEl('div.kiri');
    }
}
new TreePage();
