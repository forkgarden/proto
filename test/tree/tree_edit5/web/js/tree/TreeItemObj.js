export class TreeItemObj {
    constructor() {
        this._id = "";
        this._parentId = "";
        this._label = '';
        this._type = '';
        this._idDoc = '';
        this._parentIdDoc = '';
    }
    get parentIdDoc() {
        return this._parentIdDoc;
    }
    set parentIdDoc(value) {
        this._parentIdDoc = value;
    }
    get idDoc() {
        return this._idDoc;
    }
    set idDoc(value) {
        this._idDoc = value;
    }
    get type() {
        return this._type;
    }
    set type(value) {
        this._type = value;
    }
    get parentId() {
        return this._parentId;
    }
    set parentId(value) {
        this._parentId = value;
    }
    get id() {
        return this._id;
    }
    set id(value) {
        this._id = value;
    }
    get label() {
        return this._label;
    }
    set label(value) {
        this._label = value;
    }
}
