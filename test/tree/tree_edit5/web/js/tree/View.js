import { BaseComponent } from "../BaseComponent.js";
export class TreeItemView extends BaseComponent {
    constructor() {
        super();
        this._tree = null;
        this._template = `
            <div class='item'>
                <span class='icon'></span>
                <button class='judul'></button>
                <div class='cont'>
                </div>
            </div>
        `;
        this.build();
    }
    get tree() {
        return this._tree;
    }
    set tree(value) {
        this._tree = value;
    }
    get spanIcon() {
        return this.getEl('span.icon');
    }
    get judulTbl() {
        return this.getEl('button.judul');
    }
    get cont() {
        return this.getEl('div.cont');
    }
}
