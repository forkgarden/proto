import { TreeItemObj } from "./TreeItemObj.js";
export class Tree {
    constructor() {
        this.list = [];
    }
    init() {
    }
    getChilds(idx) {
        let hasil = [];
        this.list.forEach((item) => {
            if (item.parentId == idx) {
                hasil.push(item);
            }
        });
        return hasil;
    }
    delete(id) {
        for (let i = this.list.length - 1; i >= 0; i--) {
            if (this.list[i].id == id) {
                this.list.splice(i, 1);
            }
        }
        this.simpan();
        // throw new Error("Method not implemented.");
    }
    getById(id) {
        let hasil = null;
        this.list.forEach((item) => {
            if (item.id == id) {
                hasil = item;
            }
        });
        return hasil;
    }
    idCreate() {
        let hasil = this.list.length;
        let hasilStr = hasil + '';
        while (true) {
            if (this.getById(hasilStr)) {
                hasil++;
                hasilStr = hasil + '';
            }
            else {
                return hasilStr;
            }
        }
    }
    // insert2(tree: TreeItemObj): Promise<any> {
    //     return this.db.insert(this.toObj(tree));
    // }
    insert(tree) {
        tree.id = this.idCreate();
        this.list.push(tree);
        this.simpan();
        // this.db.insert(this.toObj(tree)).then((res: any) => {
        //     console.log('data added');
        //     console.log(res);
        //     console.log(res.id);
        // }).catch((e: Error) => {
        //     console.log('data error');
        //     console.log(e);
        // });
    }
    toObj(tree) {
        let obj = {};
        obj.id = tree.id;
        obj.label = tree.label;
        obj.parentId = tree.parentId;
        obj.type = tree.type;
        return obj;
    }
    fromObj(obj) {
        let tree = new TreeItemObj();
        tree.id = obj.id;
        tree.label = obj.label;
        tree.parentId = obj.parentId;
        tree.type = obj.type;
        return tree;
    }
    // load2(): Promise<any> {
    //     return this.db.get();
    // }
    string2List(str) {
        let dataAr;
        while (this.list.length > 0) {
            this.list.pop();
        }
        dataAr = JSON.parse(str);
        dataAr.forEach((item) => {
            this.list.push(this.fromObj(item));
        });
    }
    list2String() {
        let data = [];
        this.list.forEach((item) => {
            data.push(this.toObj(item));
        });
        return JSON.stringify(data);
    }
    hapus(id) {
        for (let i = this.list.length - 1; i >= 0; i--) {
            if (this.list[i].id == id) {
                this.list.splice(i, 1);
            }
        }
    }
    simpan() {
        let storage = window.localStorage;
        let dataStr = "";
        let dataStr2 = '';
        //validate
        dataStr = this.list2String();
        this.string2List(dataStr);
        dataStr2 = this.list2String();
        if (dataStr != dataStr2) {
            console.log(dataStr);
            console.log(dataStr2);
            throw new Error();
        }
        storage.setItem('tree', dataStr);
    }
}
