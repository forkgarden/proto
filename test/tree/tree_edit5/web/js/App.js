import { TreePage } from "./tree/TreePage.js";
import { Tree } from "./tree/Tree.js";
import { TreeDb } from "./tree/TreeDB.js";
// import { FireStoreDB as IDb } from "./tree/FireStoreDB.js";
export class App {
    constructor() {
        this.tree = null;
        this.db = null;
        window.onload = () => {
            this.tree = new Tree();
            console.log('window.load');
            this.db = new TreeDb();
            this.db.init().then(() => {
                console.log('firebase complete');
                let treePage = new TreePage();
                treePage.mulai(this.tree, this.cont, this.db);
                treePage.attach(this.cont);
            }).catch((e) => {
                console.log(e);
            });
        };
    }
    get cont() {
        return document.body.querySelector('div.main-cont');
    }
}
new App();
