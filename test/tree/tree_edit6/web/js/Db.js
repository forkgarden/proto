"use strict";
class Db {
    constructor() {
        this.list = [];
    }
    async insert(tree) {
        this.list.push(tree);
    }
    async get() {
        return this.list;
    }
    async getById(id) {
        let hasil;
        this.list.forEach((item) => {
            if (id == item.id) {
                hasil = item;
            }
        });
        if (!hasil) {
            console.error('get by id not found');
            console.log('id ' + id);
            console.log(this.list);
        }
        return hasil;
    }
    async delete(tree) {
        for (let i = 0; i < this.list.length; i++) {
            if (this.list[i].id == tree.id) {
                this.list.splice(i, 1);
                return;
            }
        }
        console.error('');
        console.log(tree);
        console.log(this.list);
    }
    async edit(tree) {
        tree;
    }
    async getChild(id) {
        let hasil = [];
        this.list.forEach((item) => {
            if (item.parentId == id) {
                hasil.push(item);
            }
        });
        return hasil;
    }
}
