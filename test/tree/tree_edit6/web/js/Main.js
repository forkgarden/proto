"use strict";
class Main {
    static get cont() {
        return Main._cont;
    }
    static get db() {
        return Main._db;
    }
    static get tree() {
        return this._tree;
    }
    static get treeCr() {
        return this._treeCr;
    }
    static set treeCr(value) {
        this._treeCr = value;
    }
    static get halDepan() {
        return Main._halDepan;
    }
    init() {
        Main._cont = document.body.querySelector('div.cont');
        Main.halDepan.view.attach(Main.cont);
        Main.halDepan.init();
        Main.treeCr = Main.tree.buat("data", '');
        Main.db.insert(Main.treeCr);
        Main.treeCr.view.attach(Main.halDepan.view.cont);
        Main.tree.select(Main.treeCr);
    }
}
Main._halDepan = new HalDepan();
Main._tree = new Tree();
Main._db = new Db();
window.onload = () => {
    (new Main()).init();
    console.log('window onload');
};
