"use strict";
class Tree {
    buat(nama, parentId) {
        let date = new Date();
        let id = Date.UTC(date.getFullYear(), date.getMonth(), date.getDay(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds()) + '';
        let hasil = {
            id: id,
            nama: nama,
            parentId: parentId,
            view: new TreeView(),
            members: [],
        };
        hasil.view.nama.innerHTML = hasil.nama;
        hasil.view.elHtml.onclick = (e) => {
            e.stopPropagation();
            console.log('dipilih');
            this.select(hasil);
            Main.treeCr = hasil;
            // Main.treeCr.view.elHtml.classList.remove('dipilih');
            // Main.treeCr = hasil;
            // hasil.view.elHtml.classList.add('dipilih');
        };
        return hasil;
    }
    renderChild(tree) {
        tree.view.cont.innerHTML = '';
        tree.members.forEach((item) => {
            item.view.attach(tree.view.cont);
        });
    }
    select(tree) {
        Main.treeCr.view.nama.classList.remove('dipilih');
        tree.view.nama.classList.add('dipilih');
    }
    async reload() {
        Main.treeCr.members = await Main.db.getChild(Main.treeCr.id);
        this.renderChild(Main.treeCr);
    }
}
class TreeView extends BaseComponent {
    constructor() {
        super();
        this._template = `
		<div class='tree'>
			<p class='nama'></p>
			<div class='cont'></div>
		</div>
		`;
        this.build();
    }
    get nama() {
        return this.getEl('p.nama');
    }
    get cont() {
        return this.getEl('div.cont');
    }
}
class TreeObj {
}
