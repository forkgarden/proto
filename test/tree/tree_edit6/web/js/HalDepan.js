"use strict";
class HalDepan {
    constructor() {
        this._view = new HalDepanView();
    }
    get view() {
        return this._view;
    }
    init() {
        this.view.tblBaru.onclick = () => {
            this.baru();
        };
        this.view.tblEdit.onclick = () => {
            let nama = window.prompt('nama: ');
            Main.treeCr.nama = nama;
            Main.treeCr.view.nama.innerHTML = nama;
        };
        this.view.tblHapus.onclick = () => {
            this.hapus();
        };
        this.view.tblReload.onclick = () => {
            Main.db.getChild(Main.treeCr.id);
        };
    }
    async hapus() {
        if (Main.treeCr.parentId == '')
            return;
        if ((await Main.db.getChild(Main.treeCr.id)).length != 0) {
            console.log('punya child');
            return;
        }
        console.log('hapus, id ' + Main.treeCr.id);
        Main.db.delete(Main.treeCr);
        Main.treeCr = await Main.db.getById(Main.treeCr.parentId);
        Main.tree.reload();
        Main.tree.select(Main.treeCr);
        // Main.treeCr.members = (await Main.db.getChild(Main.treeCr.id));
        // Main.tree.renderChild(Main.treeCr);
    }
    async reload() {
        Main.tree.reload();
        // Main.treeCr.members = (await Main.db.getChild(Main.treeCr.id));
        // Main.tree.renderChild(Main.treeCr);
    }
    async baru() {
        console.log('baru');
        let tree = Main.tree.buat('nama', Main.treeCr.id);
        Main.db.insert(tree);
        Main.tree.reload();
        // Main.treeCr.members = [];
        // Main.treeCr.members = await Main.db.getChild(Main.treeCr.id);
        // Main.tree.renderChild(Main.treeCr);
    }
}
class HalDepanView extends BaseComponent {
    constructor() {
        super();
        this._template = `
			<div class='hal-depan'>
				<div class='daftar-cont'></div>
				<div class='daftar-tbl'>
					<button class='baru btn btn-primary'>Baru</button>
					<button class='edit btn btn-primary'>Edit</button>
					<button class='hapus btn btn-primary'>Hapus</button>
					<button class='reload btn btn-primary'>Reload</button>
				</div>
			</div>
		`;
        this.build();
    }
    get tblBaru() {
        return this.getEl('button.baru');
    }
    get tblEdit() {
        return this.getEl('button.edit');
    }
    get tblHapus() {
        return this.getEl('button.hapus');
    }
    get tblReload() {
        return this.getEl('button.reload');
    }
    get cont() {
        return this.getEl('div.daftar-cont');
    }
}
