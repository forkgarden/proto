class Tree {
	buat(nama: string, parentId: string): TreeObj {
		let date: Date = new Date();
		let id: string = Date.UTC(date.getFullYear(), date.getMonth(), date.getDay(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds()) + '';
		let hasil: TreeObj = {
			id: id,
			nama: nama,
			parentId: parentId,
			view: new TreeView(),
			members: [],
		}

		hasil.view.nama.innerHTML = hasil.nama;

		hasil.view.elHtml.onclick = (e: MouseEvent) => {
			e.stopPropagation();
			console.log('dipilih');
			this.select(hasil);
			Main.treeCr = hasil;
			// Main.treeCr.view.elHtml.classList.remove('dipilih');
			// Main.treeCr = hasil;
			// hasil.view.elHtml.classList.add('dipilih');
		}

		return hasil;
	}

	renderChild(tree: TreeObj): void {
		tree.view.cont.innerHTML = '';
		tree.members.forEach((item: TreeObj) => {
			item.view.attach(tree.view.cont);
		});
	}

	select(tree: TreeObj): void {
		Main.treeCr.view.nama.classList.remove('dipilih');
		tree.view.nama.classList.add('dipilih');
	}

	async reload(): Promise<void> {
		Main.treeCr.members = await Main.db.getChild(Main.treeCr.id);
		this.renderChild(Main.treeCr);
	}
}

class TreeView extends BaseComponent {
	constructor() {
		super();
		this._template = `
		<div class='tree'>
			<p class='nama'></p>
			<div class='cont'></div>
		</div>
		`;
		this.build();
	}

	get nama(): HTMLParagraphElement {
		return this.getEl('p.nama') as HTMLParagraphElement;
	}

	get cont(): HTMLDivElement {
		return this.getEl('div.cont') as HTMLDivElement;
	}
}

class TreeObj implements ITreeObj {
	id: string;
	parentId: string;
	nama: string;
	members: ITreeObj[];
	view: TreeView;
}

interface ITreeObj {
	id: string;
	parentId: string;
	nama: string;

	//gak disimpan
	members: ITreeObj[];
	view: TreeView;
}