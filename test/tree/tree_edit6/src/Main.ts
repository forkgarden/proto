class Main {
	private static _halDepan: HalDepan = new HalDepan();
	private static _treeCr: TreeObj;
	private static _tree: Tree = new Tree();
	private static _db: Db = new Db();
	private static _cont: HTMLDivElement;

	public static get cont(): HTMLDivElement {
		return Main._cont;
	}
	public static get db(): Db {
		return Main._db;
	}
	public static get tree(): Tree {
		return this._tree;
	}
	public static get treeCr(): TreeObj {
		return this._treeCr;
	}
	public static set treeCr(value: TreeObj) {
		this._treeCr = value;
	}
	public static get halDepan(): HalDepan {
		return Main._halDepan;
	}

	init(): void {
		Main._cont = document.body.querySelector('div.cont');
		Main.halDepan.view.attach(Main.cont);
		Main.halDepan.init();

		Main.treeCr = Main.tree.buat("data", '');
		Main.db.insert(Main.treeCr);
		Main.treeCr.view.attach(Main.halDepan.view.cont);
		Main.tree.select(Main.treeCr);
	}
}

window.onload = () => {
	(new Main()).init();
	console.log('window onload');
}

