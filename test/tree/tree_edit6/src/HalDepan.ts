class HalDepan {
	private _view: HalDepanView = new HalDepanView();
	public get view(): HalDepanView {
		return this._view;
	}

	init(): void {
		this.view.tblBaru.onclick = () => {
			this.baru();
		}

		this.view.tblEdit.onclick = () => {
			let nama: string = window.prompt('nama: ');
			Main.treeCr.nama = nama;
			Main.treeCr.view.nama.innerHTML = nama;
		}

		this.view.tblHapus.onclick = () => {
			this.hapus();
		}

		this.view.tblReload.onclick = () => {
			Main.db.getChild(Main.treeCr.id)
		}
	}

	async hapus(): Promise<void> {
		if (Main.treeCr.parentId == '') return;
		if ((await Main.db.getChild(Main.treeCr.id)).length != 0) {
			console.log('punya child');
			return;
		}

		console.log('hapus, id ' + Main.treeCr.id);
		Main.db.delete(Main.treeCr);
		Main.treeCr = await Main.db.getById(Main.treeCr.parentId);

		Main.tree.reload();
		Main.tree.select(Main.treeCr);

		// Main.treeCr.members = (await Main.db.getChild(Main.treeCr.id));
		// Main.tree.renderChild(Main.treeCr);
	}

	async reload(): Promise<void> {
		Main.tree.reload();
		// Main.treeCr.members = (await Main.db.getChild(Main.treeCr.id));
		// Main.tree.renderChild(Main.treeCr);
	}

	async baru(): Promise<void> {
		console.log('baru');
		let tree: TreeObj = Main.tree.buat('nama', Main.treeCr.id);
		Main.db.insert(tree);
		Main.tree.reload();

		// Main.treeCr.members = [];
		// Main.treeCr.members = await Main.db.getChild(Main.treeCr.id);
		// Main.tree.renderChild(Main.treeCr);
	}
}

class HalDepanView extends BaseComponent {
	constructor() {
		super();
		this._template = `
			<div class='hal-depan'>
				<div class='daftar-cont'></div>
				<div class='daftar-tbl'>
					<button class='baru btn btn-primary'>Baru</button>
					<button class='edit btn btn-primary'>Edit</button>
					<button class='hapus btn btn-primary'>Hapus</button>
					<button class='reload btn btn-primary'>Reload</button>
				</div>
			</div>
		`;
		this.build();
	}

	get tblBaru(): HTMLButtonElement {
		return this.getEl('button.baru') as HTMLButtonElement;
	}

	get tblEdit(): HTMLButtonElement {
		return this.getEl('button.edit') as HTMLButtonElement;
	}

	get tblHapus(): HTMLButtonElement {
		return this.getEl('button.hapus') as HTMLButtonElement;
	}

	get tblReload(): HTMLButtonElement {
		return this.getEl('button.reload') as HTMLButtonElement;
	}

	get cont(): HTMLDivElement {
		return this.getEl('div.daftar-cont') as HTMLDivElement;
	}

}