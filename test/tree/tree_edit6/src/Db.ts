class Db {
	private list: TreeObj[] = [];

	async insert(tree: TreeObj): Promise<void> {
		this.list.push(tree);
	}

	async get(): Promise<TreeObj[]> {
		return this.list;
	}

	async getById(id: String): Promise<TreeObj> {
		let hasil: TreeObj;

		this.list.forEach((item: TreeObj) => {
			if (id == item.id) {
				hasil = item;
			}
		});

		if (!hasil) {
			console.error('get by id not found');
			console.log('id ' + id);
			console.log(this.list);
		}

		return hasil;
	}

	async delete(tree: TreeObj): Promise<void> {
		for (let i: number = 0; i < this.list.length; i++) {
			if (this.list[i].id == tree.id) {
				this.list.splice(i, 1);
				return;
			}
		}

		console.error('');
		console.log(tree);
		console.log(this.list);
	}

	async edit(tree: TreeObj): Promise<void> {
		tree;
	}

	async getChild(id: string): Promise<TreeObj[]> {
		let hasil: TreeObj[] = [];

		this.list.forEach((item: TreeObj) => {
			if (item.parentId == id) {
				hasil.push(item);
			}
		});
		return hasil;
	}
}