"use strict";
/**
 * karakter berjalan tidak melewati batas
 * ada background
 * ada sawah dan rumah (pemetaan)
 * karakter berjalan tidak menabarak
 * interaksi awal dari atas, dan di log
 * sawah di siram air
 * rumah ganti hari
 * pohon pertama, langsung panen
 * interaksi dari arah beda
 * 4 arah karakter
 *
 */
const krk = {
    pos: {
        x: 0,
        y: 0
    }
};
const config = {
    grid: {
        x: 32,
        y: 32
    }
};
var canvas;
var ctx;
var imgBola;
window.onload = () => {
    canvas = document.body.querySelector('canvas');
    ctx = canvas.getContext('2d');
    imgBola = document.body.querySelector('div.gbr img.bola');
    window.onkeydown = (e) => {
        if (e.key == "ArrowRight") {
            krk.pos.x += config.grid.x;
        }
        else if (e.key == "ArrowLeft") {
            krk.pos.x -= config.grid.x;
        }
        else if (e.key == "ArrowUp") {
            krk.pos.y -= config.grid.y;
        }
        else if (e.key == "ArrowDown") {
            krk.pos.y += config.grid.y;
        }
        else if (e.key.toLowerCase() == "a") {
            //aksi
        }
        else {
            console.error("invalid key, " + e.key);
        }
        // console.debug(e.key);
        // console.debug(krk.pos);
    };
    update();
};
function update() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(imgBola, krk.pos.x, krk.pos.y);
    setTimeout(() => {
        update();
    }, 50);
}
