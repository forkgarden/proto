"use strict";
var fg;
(function (fg) {
    var battle;
    (function (battle) {
        class Char {
            constructor() {
                this._state = Char.HITUNG;
                this.ctr = 0;
                this._ctrMax = 4;
                this._hp = 10;
                this._kekuatan = 4;
                this._nama = '';
                this._state = Char.HITUNG;
            }
            update() {
                if (this._state == Char.HITUNG) {
                    this.ctr++;
                    if (this.ctr >= this.ctrMax) {
                        this._state = Char.TUNGGU_ACTION;
                    }
                }
                else if (this._state == Char.TUNGGU_ACTION) {
                }
                else if (this.state == Char.ACTION) {
                    this.serang(this._lawan);
                    this._state = Char.TUNGGU_HITUNG;
                    this.ctr = 0;
                }
                // console.log("unit debug " + this._state);
            }
            debug() {
                console.log(this._nama + " state " + this._state);
            }
            diserang(lawan) {
                this._hp -= lawan.kekuatan;
                if (this._hp < 0) {
                    this._state = Char.MATI;
                }
            }
            serang(lawan) {
                console.log(this._nama + ' serang, target hp ' + lawan.hp);
                lawan.diserang(this);
            }
            get kekuatan() {
                return this._kekuatan;
            }
            set kekuatan(value) {
                this._kekuatan = value;
            }
            get lawan() {
                return this._lawan;
            }
            set lawan(value) {
                this._lawan = value;
            }
            get ctrMax() {
                return this._ctrMax;
            }
            set ctrMax(value) {
                this._ctrMax = value;
            }
            get hp() {
                return this._hp;
            }
            set hp(value) {
                this._hp = value;
            }
            get nama() {
                return this._nama;
            }
            set nama(value) {
                this._nama = value;
            }
            get state() {
                return this._state;
            }
            set state(value) {
                //console.log(_nama + " state: " + value);
                this._state = value;
            }
        }
        Char.HITUNG = " hitung";
        Char.ACTION = " action";
        Char.TUNGGU_ACTION = " tunggu_action";
        Char.MATI = " mati";
        Char.PAUSE = " pause";
        Char.TUNGGU_HITUNG = " tunggu_hitung";
        battle.Char = Char;
    })(battle = fg.battle || (fg.battle = {}));
})(fg || (fg = {}));
///<reference path="./Char.ts"/>
var fg;
(function (fg) {
    var battle;
    (function (battle) {
        // import flash.display.Sprite;
        // import flash.events.Event;
        /**
         * ...
         * @author test
         */
        class Main {
            constructor() {
                this.units = [];
                this.init();
            }
            init() {
                // removeEventListener(Event.ADDED_TO_STAGE, init);
                // entry point
                this.mainChar = new battle.Char();
                this.mainChar.ctrMax = 5;
                this.mainChar.hp = 30;
                this.mainChar.nama = 'char';
                this.units.push(this.mainChar);
                this.crit = new battle.Char();
                this.crit.ctrMax = 8;
                this.crit.hp = 20;
                this.crit.nama = 'crit';
                this.units.push(this.crit);
                this.mainChar.lawan = this.crit;
                this.crit.lawan = this.mainChar;
                for (var i = 0; i < 100; i++) {
                    this.update();
                }
            }
            shouldDebug(char) {
                var res = false;
                if (char.state == battle.Char.TUNGGU_ACTION)
                    res = true;
                if (char.state == battle.Char.TUNGGU_HITUNG)
                    res = true;
                // console.log("should debug " + char.state);
                return res;
            }
            debug() {
                if (this.shouldDebug(this.mainChar) || this.shouldDebug(this.crit)) {
                    console.log("debug: ");
                    this.mainChar.debug();
                    this.crit.debug();
                }
            }
            update() {
                // console.log("update")
                this.units.forEach(unit => {
                    unit.update();
                });
                this.debug();
                if (this.mainChar.state == battle.Char.MATI) {
                }
                else if (this.crit.state == battle.Char.MATI) {
                }
                else if (this.mainChar.state == battle.Char.TUNGGU_HITUNG) {
                    this.crit.state = battle.Char.HITUNG;
                    this.mainChar.state = battle.Char.HITUNG;
                    // this.debug();
                }
                else if (this.crit.state == battle.Char.TUNGGU_HITUNG) {
                    this.crit.state = battle.Char.HITUNG;
                    this.mainChar.state = battle.Char.HITUNG;
                    // this.debug();
                }
                else if (this.mainChar.state == battle.Char.TUNGGU_ACTION) {
                    this.mainChar.state = battle.Char.ACTION;
                    this.crit.state = battle.Char.PAUSE;
                    // this.debug();
                }
                else if (this.crit.state == battle.Char.TUNGGU_ACTION) {
                    this.crit.state = battle.Char.ACTION;
                    this.mainChar.state = battle.Char.PAUSE;
                    // this.debug();
                }
                else if (this.mainChar.state == battle.Char.ACTION) {
                }
                else if (this.crit.state == battle.Char.ACTION) {
                }
                // this.debug();
            }
        }
        battle.Main = Main;
    })(battle = fg.battle || (fg.battle = {}));
})(fg || (fg = {}));
