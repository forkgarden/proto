///<reference path="./Char.ts"/>

namespace fg.battle {
    // import flash.display.Sprite;
    // import flash.events.Event;

	/**
	 * ...
	 * @author test
	 */
    export class Main {

        private units: Array<fg.battle.Char> = [];
        private mainChar: Char;
        private crit: Char;

        constructor() {
            this.init();
        }

        private init(): void {
            // removeEventListener(Event.ADDED_TO_STAGE, init);
            // entry point

            this.mainChar = new Char();
            this.mainChar.ctrMax = 5;
            this.mainChar.hp = 30;
            this.mainChar.nama = 'char';
            this.units.push(this.mainChar);

            this.crit = new Char();
            this.crit.ctrMax = 8;
            this.crit.hp = 20;
            this.crit.nama = 'crit';
            this.units.push(this.crit);

            this.mainChar.lawan = this.crit;
            this.crit.lawan = this.mainChar;

            for (var i: number = 0; i < 100; i++) {
                this.update();
            }
        }

        private shouldDebug(char: Char): boolean {
            var res: boolean = false;

            if (char.state == Char.TUNGGU_ACTION) res = true;
            if (char.state == Char.TUNGGU_HITUNG) res = true;

            // console.log("should debug " + char.state);



            return res;
        }

        private debug(): void {
            if (this.shouldDebug(this.mainChar) || this.shouldDebug(this.crit)) {
                console.log("debug: ");
                this.mainChar.debug();
                this.crit.debug();
            }
        }

        private update(): void {

            // console.log("update")

            this.units.forEach(unit => {
                unit.update();
            });

            this.debug();

            if (this.mainChar.state == Char.MATI) {

            }
            else if (this.crit.state == Char.MATI) {

            }
            else if (this.mainChar.state == Char.TUNGGU_HITUNG) {
                this.crit.state = Char.HITUNG;
                this.mainChar.state = Char.HITUNG;
                // this.debug();
            }
            else if (this.crit.state == Char.TUNGGU_HITUNG) {
                this.crit.state = Char.HITUNG;
                this.mainChar.state = Char.HITUNG;
                // this.debug();
            }
            else if (this.mainChar.state == Char.TUNGGU_ACTION) {
                this.mainChar.state = Char.ACTION;
                this.crit.state = Char.PAUSE;
                // this.debug();
            }
            else if (this.crit.state == Char.TUNGGU_ACTION) {
                this.crit.state = Char.ACTION;
                this.mainChar.state = Char.PAUSE;
                // this.debug();
            }
            else if (this.mainChar.state == Char.ACTION) {

            }
            else if (this.crit.state == Char.ACTION) {

            }

            // this.debug();


        }

    }

}