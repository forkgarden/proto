namespace fg.battle {
    export class Char {

        static readonly HITUNG: string = " hitung";
        static readonly ACTION: string = " action";
        static readonly TUNGGU_ACTION: string = " tunggu_action";
        static readonly MATI: string = " mati";
        static readonly PAUSE: string = " pause";
        static readonly TUNGGU_HITUNG: string = " tunggu_hitung";

        private _state: string = Char.HITUNG;
        private ctr: number = 0;
        private _ctrMax: number = 4;
        private _lawan!: Char;
        private _hp: number = 10;
        private _kekuatan: number = 4;
        private _nama: String = '';

        public constructor() {
            this._state = Char.HITUNG;
        }

        public update(): void {
            if (this._state == Char.HITUNG) {
                this.ctr++;
                if (this.ctr >= this.ctrMax) {
                    this._state = Char.TUNGGU_ACTION;
                }
            }
            else if (this._state == Char.TUNGGU_ACTION) {

            }
            else if (this.state == Char.ACTION) {
                this.serang(this._lawan);
                this._state = Char.TUNGGU_HITUNG;
                this.ctr = 0;
            }
            // console.log("unit debug " + this._state);
        }

        debug(): void {
            console.log(this._nama + " state " + this._state);
        }

        public diserang(lawan: Char): void {
            this._hp -= lawan.kekuatan;

            if (this._hp < 0) {
                this._state = Char.MATI;
            }
        }

        public serang(lawan: Char): void {
            console.log(this._nama + ' serang, target hp ' + lawan.hp);
            lawan.diserang(this);
        }

        public get kekuatan(): number {
            return this._kekuatan;
        }

        public set kekuatan(value: number) {
            this._kekuatan = value;
        }

        public get lawan(): Char {
            return this._lawan;
        }

        public set lawan(value: Char) {
            this._lawan = value;
        }

        public get ctrMax(): number {
            return this._ctrMax;
        }

        public set ctrMax(value: number) {
            this._ctrMax = value;
        }

        public get hp(): number {
            return this._hp;
        }

        public set hp(value: number) {
            this._hp = value;
        }

        public get nama(): String {
            return this._nama;
        }

        public set nama(value: String) {
            this._nama = value;
        }

        public get state(): string {
            return this._state;
        }

        public set state(value: string) {
            //console.log(_nama + " state: " + value);
            this._state = value;
        }

    }

}