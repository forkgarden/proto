import { BaseComponent } from "../../ha/BaseComponent.js";
import { TreeEdit } from "../TreeEdit.js";
// import { Folder } from "../tree/TreeData.js";
import { DbFolder } from "../DbFolder.js";
import { Folder } from "../item/TreeData.js";

export class Panel {
    private folderTbl:Tombol;
    private db:DbFolder = null;

    constructor() {
        this.folderTbl = new Tombol();
    }

    init():void {
        this.db = TreeEdit.inst.db;
        this.folderTbl.label = 'Folder Baru';
        this.folderTbl.attach(TreeEdit.inst.kanan);
        this.folderTbl.tbl.onclick = this.folderClick.bind(this);
        console.log(this.folderTbl);
    }

    folderClick():void {
        let parent:Folder = TreeEdit.inst.tree.selected;
        let folder:Folder = Folder.create();

        console.log('folder click');

        folder.parentId = parent.id;
        folder.view.attach(parent.view.folderEl);

        this.db.insert(folder);
    }

}

export class Tombol extends BaseComponent {
    protected _tbl: HTMLButtonElement = null;

    constructor() {
        super();
        this._template = `<button></button>`;
        this.build();
        this._tbl = this._elHtml as HTMLButtonElement;
    }

    public get tbl(): HTMLButtonElement {
        return this._tbl;
    }

    public get label(): string {
        return this._tbl.innerHTML;
    }
    public set label(value: string) {
        this.tbl.innerHTML = value;
    }

}