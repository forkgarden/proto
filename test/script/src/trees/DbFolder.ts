import { Folder } from "./item/TreeData";

// import { Folder } from "./TreeData";

export class DbFolder implements IDb {
    private list:Array<Folder> = [];
    private lastId:number = -1;
    private idGen:IdGen = new IdGen();

    constructor() {

    }

    getLastId():number {
        return Math.max(this.list.length, this.lastId);
    }

    userExistst(id:number):boolean {
        for (let i:number = 0; i< this.list.length; i++) {
            if (this.list[i].id == id) return true;
        }

        return false;
    }

    insert(folder:Folder):void {
        folder.id = this.idGen.gen(this);
        this.list.push(folder);
    }


}

export interface IDb {
    getLastId():number;
    userExistst(id:number):boolean;
}

export class IdGen {

    gen(db:IDb):number {
        let res:number = 0;
        let rep:number=0;

        res = db.getLastId()+1;
        while (db.userExistst(res)) {
            res++
            rep++;
            if (rep > 1000) throw new Error();
        }

        return res;
    }

}