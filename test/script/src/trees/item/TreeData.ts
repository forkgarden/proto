import { TreeEdit } from "../TreeEdit";
import { FolderView } from "./TreeView";

// import { FolderView } from "../trees/item/TreeView.js";
// import { TreeEdit } from "../trees/TreeEdit.js";

export class TreeData {
    private _head: Folder = null;
    private _selected: Folder = null;

    constructor() {
        this._head = new Folder();
    }

    init():void {
        let treeEdit:TreeEdit = TreeEdit.inst;

        this._head.createView();
        this._head.view.attach(treeEdit.kiri);
        this._selected = this._head;
        console.log('this selected ' + this._selected);
    }

    public get head(): Folder {
        return this._head;
    }
    public set head(value: Folder) {
        this._head = value;
    }

    public get selected(): Folder {
        return this._selected;
    }
    public set selected(value: Folder) {
        this._selected = value;
    }

}

export class Folder {
    private _view: FolderView = null;
    private _id: number = -1;
    private _parentId: number = -1;

    static create():Folder {
        let folder:Folder;

        folder = new Folder();
        folder.createView();

        return folder;
    }

    //TODO: dep
    addFile(file:File):void {
        file.parentId = this._id;
    }

    //TODO:
    addfolder(folder:Folder):void {
        folder.parentId = this._id;
        folder.view.attach(this.view.folderEl);
    }

    createView():void {
        this._view = new FolderView();
        this._view.data = this;
    }

    public get view(): FolderView {
        return this._view;
    }

    // public get parent(): Folder {
    //     return this._parent;
    // }
    // public set parent(value: Folder) {
    //     this._parent = value;
    // }

    public get id(): number {
        return this._id;
    }
    public set id(value:number) {
        this._id = value;
    }

    public get parentId(): number {
        return this._parentId;
    }
    public set parentId(value: number) {
        this._parentId = value;
    }


}

export class File {
    // private _parent:Folder = null;
    // private _view:FileView;
    private _parentId: number = -1;

    createView():void {
        // this._view = new fil
    }

    public get parentId(): number {
        return this._parentId;
    }
    public set parentId(value: number) {
        this._parentId = value;
    }
}