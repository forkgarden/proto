import { BaseComponent } from "../../ha/BaseComponent.js"
import { TreeEdit } from "../TreeEdit.js";
import { Folder } from "./TreeData.js";
// import { Folder } from "../../tree/TreeData.js";

export class FolderView extends BaseComponent {
	private _titleEl: HTMLSpanElement = null;
	private _content: HTMLDivElement = null;
	private _toggleEl: HTMLButtonElement = null;
	private expanded: boolean = true;
	private _folderEl: HTMLDivElement = null;
	private _fileEl: HTMLDivElement = null;
	private _data: Folder = null;

	constructor() {
		super();
		this._template = `
                <div class='ha-comm-tree'>
                    <div class='header'>
                        <button class='toggle'>+</button>
                        <span class='title'>title</span>
                    </div>
					<div class='content'>
						<div class='folder'></div>
						<div class='file'></div>
					</div>
                </div>
			`;

		this.build();

	}

	onBuild(): void {
		this._titleEl = this.getEl('span.title') as HTMLSpanElement;
		this._content = this.getEl('div.content') as HTMLDivElement;
		this._toggleEl = this.getEl('button.toggle') as HTMLButtonElement;
		this._folderEl = this.getEl('div.content div.folder') as HTMLDivElement;

		this._toggleEl.onclick = (e:MouseEvent) => {
			e.stopPropagation();
			console.log(e);

			this.expanded = !this.expanded;
			if (this.expanded) {
				this.show(this._content);
			}
			else {
				this.hide(this._content);
			}
		};

		this._titleEl.onclick = () => {
			console.log('folder on click');
			
			if (TreeEdit.inst.tree.selected) {
				TreeEdit.inst.tree.selected.view.elHtml.classList.remove('selected');
			}

			TreeEdit.inst.tree.selected = this._data;
			this._elHtml.classList.add('selected');
		}
	}

	public get title(): string {
		return this._titleEl.innerHTML;
	}

	public set title(value:string) {
		this._titleEl.innerHTML = value;
	}

	public get content(): HTMLDivElement {
		return this._content;
	}

	public get folderEl(): HTMLDivElement {
		return this._folderEl;
	}
	public get fileEl(): HTMLDivElement {
		return this._fileEl;
	}
	public get data(): Folder {
		return this._data;
	}
	public set data(value: Folder) {
		this._data = value;
	}



}

export class FileView extends BaseComponent {
	// static readonly TREE: string = 'TREE';
	// static readonly DISP: string = 'DISPLAY';

	// private _type: string = '';
	// private _view: BaseComponent = null;
	// private _folder: FolderView = null;

	private labelEl:HTMLSpanElement = null;

	constructor() {
		super();
		this._template = `
			<div class='file-view'>
				<span class='label'></label>
			</div>
		`;
		this.build();

		this.labelEl = this.getEl('span.label') as HTMLSpanElement;
	}


	// removeChild():void {

	// }

	// public get folder(): FolderView {
	// 	return this._folder;
	// }
	// public set folder(value: FolderView) {
	// 	this._folder = value;
	// }

	// public get view(): BaseComponent {
	// 	return this._view;
	// }
	// public set view(value: BaseComponent) {
	// 	this._view = value;
	// }

	// public get type(): string {
	// 	return this._type;
	// }
	// public set type(value: string) {
	// 	this._type = value;
	// }

	public get label(): string {
		return this.labelEl.innerHTML;
	}
	public set label(value: string) {
		this.labelEl.innerHTML = value;
	}


}