import { Panel } from "./panel/Panel.js";
import { DbFolder } from "./DbFolder.js";
import { TreeData } from "./item/TreeData.js";

export class TreeEdit {
    private _tree: TreeData =  null;
    private _kanan: HTMLDivElement = null;
    private _kiri: HTMLDivElement = null;
    private panel:Panel;
    private _db: DbFolder;

    private static _inst: TreeEdit = null;

    constructor() {
        TreeEdit._inst = this;
        this._db = new DbFolder();
        window.onload = this.init.bind(this);
    }

    init(){
        console.log(this);
        this._kanan = document.querySelector('div.cont div.kanan') as HTMLDivElement;
        this._kiri = document.querySelector('div.cont div.kiri') as HTMLDivElement;

        this._tree = new TreeData();
        this._tree.init();
        this._tree.head.view.title = 'root';

        this.panel = new Panel();
        this.panel.init(); 
    }

    public get tree(): TreeData{
        return this._tree;
    }
    public static get inst(): TreeEdit {
        return TreeEdit._inst;
    }

    public get kiri(): HTMLDivElement {
        return this._kiri;
    }
    public set kiri(value: HTMLDivElement) {
        this._kiri = value;
    }
    

    public get kanan(): HTMLDivElement {
        return this._kanan;
    }
    public set kanan(value: HTMLDivElement) {
        this._kanan = value;
    }
    public get db(): DbFolder {
        return this._db;
    }


}

new TreeEdit();