// import { Counter } from "./Counter";

export class FloatInput {
    private label:HTMLLabelElement=null;
    private input:HTMLInputElement=null;
    private placeholder:string = 'username';
    // private ctr:Counter;

    constructor() {
        this.label = document.querySelector('div.float-input label');
        this.input = document.querySelector('div.float-input input');
    }

    init():void {
        this.label.style.display = 'none';

        this.updateStatus();

        this.input.oninput = () => {
            this.updateStatus();
        }
    }

    updateStatus():void {
        if (this.input.value != '') {
            this.label.style.display='block';
            this.input.placeholder = '';
            this.label.innerText = this.placeholder + ':';
        }
        else {
            this.label.style.display='none';
            this.input.placeholder = this.placeholder;
        }
        
    }
}