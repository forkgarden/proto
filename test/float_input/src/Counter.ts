export class Counter {
    private pencetBtn: HTMLButtonElement = null;
    private settingBtn: HTMLButtonElement = null;
    private aktifBtn: HTMLButtonElement = null;
    private resetBtn: HTMLButtonElement = null;
    private hitungP: HTMLParagraphElement = null;
    private menuDiv: HTMLDivElement = null;

    private counter: number = 0;
    private batasctr: number = 0;

    constructor() {
        window.onload = () => {
            this.init();
        }
    }

    init(): void {
        console.log("init");

        this.hitungP = document.body.querySelector('div.cont p.counter') as HTMLParagraphElement;
        this.menuDiv = document.body.querySelector("div.cont div.menu");

        this.pencetBtn = document.body.querySelector('div.cont button.pencet') as HTMLButtonElement;
        this.pencetBtn.onclick = () => {

            this.counter++;
            this.batasctr++;

            this.hitungP.innerText = this.counter + '';
            if (this.batasctr >= 100) {
                this.batasctr = 0;
                this.pencetBtn.disabled = true;
            }
            // console.log(this.counter);
            // console.log(this.hitungP.innerText);
        }

        this.settingBtn = document.body.querySelector('div.cont div.setting button.setting') as HTMLButtonElement;
        this.settingBtn.onclick = () => {
            if (this.menuDiv.style.display == "block") {
                this.menuDiv.style.display = "none";
            }
            else {
                this.menuDiv.style.display = "block";
            }
        }

        this.aktifBtn = document.body.querySelector('div.cont div.setting div.menu button.aktif') as HTMLButtonElement;
        this.aktifBtn.onclick = () => {
            this.menuDiv.style.display = "none";
            this.pencetBtn.disabled = false;
        }

        this.resetBtn = document.body.querySelector('div.cont div.menu button.reset') as HTMLButtonElement;
        this.resetBtn.onclick = () => {
            this.menuDiv.style.display = "none";
            this.counter = 0;
            this.batasctr = 0;
            this.pencetBtn.disabled = false;
            this.hitungP.innerText = 0 + '';
        }
    }

    save():void {

    }

    load():void {
        
    }
}

new Counter();