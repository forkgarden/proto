// import { Counter } from "./Counter";
export class FloatInput {
    // private ctr:Counter;
    constructor() {
        this.label = null;
        this.input = null;
        this.placeholder = 'username';
        this.label = document.querySelector('div.float-input label');
        this.input = document.querySelector('div.float-input input');
    }
    init() {
        this.label.style.display = 'none';
        this.updateStatus();
        this.input.oninput = () => {
            this.updateStatus();
        };
    }
    updateStatus() {
        if (this.input.value != '') {
            this.label.style.display = 'block';
            this.input.placeholder = '';
            this.label.innerText = this.placeholder + ':';
        }
        else {
            this.label.style.display = 'none';
            this.input.placeholder = this.placeholder;
        }
    }
}
