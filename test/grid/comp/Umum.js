import { dialog } from "./Dialog.js";
import { Util } from "./Util.js";
class Umum {
    buttonSubmitter() {
        let buttonAr = document.body.querySelectorAll('button[type="form-submit"]');
        buttonAr.forEach((button) => {
            button.onclick = (e) => {
                e.stopPropagation();
                e.preventDefault();
                //TODO:
            };
        });
    }
    buttonAction() {
        let buttonAr = document.body.querySelectorAll('button[type="button"][action]');
        console.log("button action: ");
        console.log(buttonAr);
        buttonAr.forEach((button) => {
            button.onclick = (e) => {
                e.preventDefault();
                e.stopPropagation();
                window.location.href = button.getAttribute('action');
            };
        });
    }
    jsonSubmit() {
        console.log('json submit');
        let formAr = document.body.querySelectorAll('form');
        formAr.forEach((form) => {
            console.log(form);
            form.onsubmit = () => {
                try {
                    console.log('form on submit');
                    let obj = {};
                    let formData = new FormData(form);
                    for (let item of formData.entries()) {
                        if (item[0] == "password") {
                            item[1] = md5(item[1]);
                        }
                        obj[item[0]] = item[1];
                    }
                    console.log(obj);
                    console.log(form.action);
                    Util.Ajax("post", form.action, JSON.stringify(obj)).then((xml) => {
                        console.log('xml finish, status ' + xml.status);
                        console.log('dialog atribute ' + form.hasAttribute('redirect-dialog'));
                        console.log('dialog pesan ' + form.getAttribute('redirect-pesan'));
                        if (xml.status >= 200 && (xml.status <= 300)) {
                            if (form.hasAttribute("redirect-dialog")) {
                                dialog.tampil(form.getAttribute('redirect-pesan'));
                                dialog.okTbl.onclick = () => {
                                    window.top.location.href = form.getAttribute('redirect');
                                };
                            }
                            else {
                                window.top.location.href = form.getAttribute('redirect');
                            }
                        }
                        else {
                            throw Error(xml.responseText);
                        }
                    }).catch((e) => {
                        Util.error(e);
                    });
                }
                catch (e) {
                    Util.error(e);
                }
                return false;
            };
        });
    }
    buttonToggle() {
        let buttonAr = document.body.querySelectorAll('button[type="button"][toggle-src][toggle-idx]');
        console.log("button toggle: ");
        console.log(buttonAr);
        buttonAr.forEach((button) => {
            button.onclick = (e) => {
                e.preventDefault();
                e.stopPropagation();
                let index = button.getAttribute('toggle-idx');
                let el = document.body.querySelector(`[toggle-target][toggle-idx="${index}"`);
                console.log('index ' + index);
                console.log('el: ');
                console.log(el);
                if (el) {
                    if (el.style.display = 'block') {
                        el.style.display = 'none';
                    }
                    else {
                        el.style.display = 'block';
                    }
                }
            };
        });
    }
}
var umum = new Umum();
umum.jsonSubmit();
umum.buttonAction();
umum.buttonToggle();
