import express, { RequestParamHandler } from "express";
// import path from "path";
// import bodyparser from "body-parser";
import multer from "multer";
import fs from "fs";

const app: express.Express = express();
const port: number = 3009;

const storage = multer.diskStorage({
	destination: (req: Express.Request, file: Express.Multer.File, cb) => {
		cb(null, "upload");
	},
	filename: (req, file: Express.Multer.File, cb) => {
		cb(null, "file_" + Date.now() + "-" + file.originalname);
	}
}

);
const upload = multer({ storage: storage });

app.use(express.static(__dirname + "\\web\\"));
// app.use(bodyparser.json());

app.post('/profile', upload.single('avatar'), async (_req, _resp) => {
	const file: Express.Multer.File = _req.file;

	if (!file) {
		_resp.sendStatus(400);
	}

	// resp.send(file.fieldname);
	_resp.write(JSON.stringify(file));
	_resp.end("success");
})

app.post('/uploads', upload.array('fotos', 4), (_req: Express.Request, _resp) => {
	let res: string;

	if (_req.files) {
		res = JSON.stringify(_req.files);
		_resp.send(res);
	}
	else {
		_resp.sendStatus(400).send('error uploading files');
	}
});

app.post('/dataurl', (_req: Express.Request, _resp: Express.Response) => {
	const { name, address, phone } = _req.body;
	let url: string;
	let buff: Buffer;

	_req.body;

	url = _req.body.data;
	buff = {};
	fs.writeFileSync('./upload/test.png', buff);
});

app.listen(port, () => {
	console.log("app started at port " + port);
})
