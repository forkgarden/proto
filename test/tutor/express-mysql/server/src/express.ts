import express, { RequestParamHandler } from "express";
// import path from "path";
import bodyparser from "body-parser";
import multer from "multer";
import mysql from "mysql";

const app: express.Express = express();
const port: number = 3009;

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, "upload");
	},
	filename: (req, file, cb) => {
		cb(null, "file_" + Date.now() + "-" + file.originalname);
	}
}

);
const upload = multer({storage: storage});
app.use(express.static(__dirname + "\\web\\"));
app.use(bodyparser.json());

app.post('/profile', upload.single('avatar'), async (req, resp) => {
	
	const file:Express.Multer.File = req.file;

	if (!file) {
		await resp.sendStatus(400);
	}

	// resp.send(file.fieldname);
	await resp.write(JSON.stringify(file));
	await resp.end("success");

});

app.get('/list', (req:express.Request, res:express.Response) => {
	let connect:mysql.Connection = mysql.createConnection({
		host:"localhost",
		user: "root",
		password: ""
	});

	
});

app.listen(port, () => {
	console.log("app started at port " + port);
})
