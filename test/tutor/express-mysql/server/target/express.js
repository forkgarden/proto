"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const multer_1 = __importDefault(require("multer"));
const app = express_1.default();
const port = 3009;
const storage = multer_1.default.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "upload");
    },
    filename: (req, file, cb) => {
        cb(null, "file_" + Date.now() + "-" + file.originalname);
    }
});
const upload = multer_1.default({ storage: storage });
app.use(express_1.default.static(__dirname + "\\web\\"));
app.use(body_parser_1.default.json());
app.post('/profile', upload.single('avatar'), async (req, resp) => {
    const file = req.file;
    if (!file) {
        await resp.sendStatus(400);
    }
    // resp.send(file.fieldname);
    await resp.write(JSON.stringify(file));
    await resp.end("success");
});
app.listen(port, () => {
    console.log("app started at port " + port);
});
