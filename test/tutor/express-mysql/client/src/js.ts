class App {
    constructor() {
        window.onload = () => {
            this.init();
        }        
    }

    init():void {
        this.loadData();
    }

    dataLoaded(data:string):void {
        let obj:Array<DataImg> = JSON.parse(data);

        console.log('data loaded ');
        console.log(obj); 

        if (!obj) return;

        obj.forEach((item:DataImg) => {
            item;
            let img:HTMLImageElement = document.createElement('img');
            let cont:HTMLDivElement = document.body.querySelector('div.cont-img');

            img.src = item.url;
            cont.appendChild(img);
        })

    }

	loadData(): void {
		console.log('save');

		var xhr: XMLHttpRequest = new XMLHttpRequest();
		xhr.open('GET', 'http://localhost:3009/list', true);

		xhr.onreadystatechange = (ev: Event) => {
			ev;
		}

		xhr.onload = () => {

			if (xhr.status !== 200) {
				console.log('xhr ' + xhr.status);
				console.log('xhr ' + xhr.statusText);
				return; // return is important because the code below is NOT executed if the response is other than HTTP 200 (OK)
			}

			console.log('response');
            console.log(xhr.responseText);
            this.dataLoaded(xhr.responseText);
		};

		xhr.send();
	}    
}

interface DataImg {
    id: number,
    url:string
}
