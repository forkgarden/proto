window.onload = () => {
    imgPicker(document.querySelector('img') as HTMLImageElement, document.querySelector('input[type=file]') as HTMLInputElement);
}

function imgPicker(preview:HTMLImageElement, input:HTMLInputElement):void {
    input.onchange = () => {
        const file:File = (document.querySelector('input[type=file]') as HTMLInputElement).files[0];
        const reader:FileReader = new FileReader();
    
        reader.onload = () =>  {
            preview.src = (reader.result) as string;
        };
    
        if (file) {
            reader.readAsDataURL(file);
        }
    }
}
