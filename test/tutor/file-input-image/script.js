"use strict";
window.onload = () => {
    // const input:HTMLInputElement = document.querySelector('input[type=file]') as HTMLInputElement;
    imgPicker(document.querySelector('img'), document.querySelector('input[type=file]'));
    // input.onchange = () => {
    //     const preview:HTMLImageElement = document.querySelector('img');
    //     const file:File = (document.querySelector('input[type=file]') as HTMLInputElement).files[0];
    //     const reader:FileReader = new FileReader();
    //     reader.onload = (e:ProgressEvent<FileReader>) =>  {
    //         e.stopPropagation();
    //         preview.src = (reader.result) as string;
    //     };
    //     if (file) {
    //         reader.readAsDataURL(file);
    //     }    
    // }
};
function imgPicker(preview, input) {
    input.onchange = () => {
        const file = document.querySelector('input[type=file]').files[0];
        const reader = new FileReader();
        reader.onload = () => {
            preview.src = (reader.result);
        };
        if (file) {
            reader.readAsDataURL(file);
        }
    };
}
