import express, { RequestParamHandler } from "express";
import multer from "multer";
import fs from "fs";
import path from "path";

const app: express.Express = express();
const port: number = 3009;

const storage = multer.diskStorage({
	destination: (req: express.Request, file: Express.Multer.File, cb) => {
		cb(null, "upload");
	},
	filename: (req, file: Express.Multer.File, cb) => {
		cb(null, "file_" + Date.now() + "-" + file.originalname);
	}
}

);
const upload = multer({ storage: storage });

app.use(express.static(__dirname + "\\web\\"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.post('/profile', upload.single('avatar'), async (_req, _resp) => {
	const file: Express.Multer.File = _req.file;

	if (!file) {
		_resp.sendStatus(400);
	}

	_resp.write(JSON.stringify(file));
	_resp.end("success");
})

app.post('/uploads', upload.array('fotos', 4), (_req: express.Request, _resp) => {
	let res: string;

	if (_req.files) {
		res = JSON.stringify(_req.files);
		_resp.send(res);
	}
	else {
		_resp.sendStatus(400).send('error uploading files');
	}
});

app.post('/dataurl', (_req: express.Request, _resp: express.Response) => {
	let url: string;
	let buff: Buffer;
	let data: string;
	let buf: Buffer;

	data = _req.body.gbr_kecil.split(',')[1];
	buf = Buffer.from(data, 'base64');
	fs.writeFileSync('./upload/file.png', buf);
	console.log('file written');
	_resp.status(200).send('ok');
});

app.get('/', (_req: express.Request, _resp: express.Response) => {
	console.log('get');
	_resp.sendFile(path.join(__dirname, '/web/index_canvas.html'));
});

app.get('/test', (_req: express.Request, _resp: express.Response) => {
	console.log('get');
	console.log(_req);
	_resp.status(200).send('ok');
});


app.use((_req: express.Request, _resp: express.Response, _next: Function) => {
	_resp.status(404).send('Halaman Tidak Ditemukan');
})

app.listen(port, () => {
	console.log("app started at port " + port);
})
