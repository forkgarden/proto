"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const multer_1 = __importDefault(require("multer"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const app = express_1.default();
const port = 3009;
const storage = multer_1.default.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "upload");
    },
    filename: (req, file, cb) => {
        cb(null, "file_" + Date.now() + "-" + file.originalname);
    }
});
const upload = multer_1.default({ storage: storage });
app.use(express_1.default.static(__dirname + "\\web\\"));
app.use(express_1.default.json());
app.use(express_1.default.urlencoded({ extended: true }));
app.post('/profile', upload.single('avatar'), async (_req, _resp) => {
    const file = _req.file;
    if (!file) {
        _resp.sendStatus(400);
    }
    _resp.write(JSON.stringify(file));
    _resp.end("success");
});
app.post('/uploads', upload.array('fotos', 4), (_req, _resp) => {
    let res;
    if (_req.files) {
        res = JSON.stringify(_req.files);
        _resp.send(res);
    }
    else {
        _resp.sendStatus(400).send('error uploading files');
    }
});
app.post('/dataurl', (_req, _resp) => {
    let url;
    let buff;
    let data;
    let buf;
    data = _req.body.gbr_kecil.split(',')[1];
    buf = Buffer.from(data, 'base64');
    fs_1.default.writeFileSync('./upload/file.png', buf);
    console.log('file written');
    _resp.status(200).send('ok');
});
app.get('/', (_req, _resp) => {
    console.log('get');
    _resp.sendFile(path_1.default.join(__dirname, '/web/index_canvas.html'));
});
app.get('/test', (_req, _resp) => {
    console.log('get');
    console.log(_req);
    _resp.status(200).send('ok');
});
app.use((_req, _resp, _next) => {
    _resp.status(404).send('Halaman Tidak Ditemukan');
});
app.listen(port, () => {
    console.log("app started at port " + port);
});
