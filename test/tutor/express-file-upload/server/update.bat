echo on
md target\web
md target\web\js

del target\web\js\*.* /s /q
del target\web\*.* /s /q

xcopy ..\client\web\*.* target\web\*.* /s /y
pause
