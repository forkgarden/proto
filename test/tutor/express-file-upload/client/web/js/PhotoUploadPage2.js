"use strict";
class PhotoUploadPage2 extends BaseComponent {
    constructor() {
        super();
        this.canvasImg2 = document.createElement('canvas');
        // private ctxImg2: CanvasRenderingContext2D = this.canvasImg2.getContext('2d');
        this.canvasThumb2 = document.createElement('canvas');
        // private ctxThumb2: CanvasRenderingContext2D = this.canvasThumb2.getContext('2d');
        // private ctx: CanvasRenderingContext2D;
        // private ctx2: CanvasRenderingContext2D;
        this.rotasi = 0;
        window.onload = () => {
            this.canvasImg2.width = 128;
            this.canvasImg2.height = 128;
            this.canvasThumb2.width = 32;
            this.canvasThumb2.height = 32;
            this._elHtml = document.body;
            this.uploadTbl.style.display = 'none';
            this.rotasiTbl.style.display = 'none';
            this.initInput(this.canvasBesarHtml, this.canvasThumbHtml, this.input);
            this.form.onsubmit = () => {
                this.upload('post', './dataurl', this.populateJson());
                return false;
            };
            this.rotasiTbl.onclick = () => {
                this.rotasi += 90;
                if (this.rotasi > 360) {
                    this.rotasi -= 360;
                }
                this.renderImg(this.canvasBesarHtml, this.rotasi, this.canvasImg2, 128 / 2, 128 / 2);
                this.renderImg(this.canvasThumbHtml, this.rotasi, this.canvasThumb2, 32 / 2, 32 / 2);
            };
        };
    }
    populateData() {
        let formData = new FormData();
        formData.append("gbr_besar", this.canvasBesarHtml.toDataURL());
        formData.append("gbr_kecil", this.canvasThumbHtml.toDataURL());
        return formData;
    }
    populateJson() {
        let obj = {
            gbr_besar: this.canvasBesarHtml.toDataURL(),
            gbr_kecil: this.canvasThumbHtml.toDataURL()
        };
        return JSON.stringify(obj);
    }
    renderImg(canvasDest, sudut, canvasSrc, x, y) {
        let ctxDest = canvasDest.getContext('2d');
        sudut = (Math.PI / 180.0) * sudut;
        ctxDest.clearRect(0, 0, canvasDest.width, canvasDest.height);
        // ctxDest.fillStyle = "rgba(0, 0, 0, 0)";
        // ctxDest.fillRect(0, 0, canvasDest.width, canvasDest.height);
        ctxDest.save();
        ctxDest.translate(x, y);
        ctxDest.rotate(sudut);
        ctxDest.drawImage(canvasSrc, -x, -y);
        ctxDest.restore();
        // ctxDest.clearRect(0, 0, canvasDest.width, canvasDest.height);
        console.log(canvasDest.width + '/' + canvasDest.height);
    }
    initInput(canvasHtml, canvasThumbHtml, input) {
        input.onchange = () => {
            let file = input.files[0];
            let reader = new FileReader();
            let image = new Image();
            // this.ctx = canvasHtml.getContext('2d');
            // this.ctx2 = canvasThumbHtml.getContext('2d');
            this.uploadTbl.style.display = 'none';
            this.rotasiTbl.style.display = 'none';
            reader.onload = () => {
                image.onload = () => {
                    let ratio = Math.min(canvasHtml.width / image.naturalWidth, canvasHtml.height / image.naturalHeight);
                    let w2 = image.naturalWidth * ratio;
                    let h2 = image.naturalHeight * ratio;
                    let x = 0 + (canvasHtml.width - w2) / 2;
                    let y = 0 + (canvasHtml.height - h2) / 2;
                    this.canvasImg2.getContext('2d').clearRect(0, 0, this.canvasImg2.width, this.canvasImg2.height);
                    this.canvasImg2.getContext('2d').drawImage(image, x, y, w2, h2);
                    this.renderImg(this.canvasBesarHtml, this.rotasi, this.canvasImg2, 128 / 2, 128 / 2);
                    //gambar thumbnail
                    ratio = Math.min(canvasThumbHtml.width / image.naturalWidth, canvasThumbHtml.height / image.naturalHeight);
                    w2 = image.naturalWidth * ratio;
                    h2 = image.naturalHeight * ratio;
                    x = 0 + (canvasThumbHtml.width - w2) / 2;
                    y = 0 + (canvasThumbHtml.height - h2) / 2;
                    this.canvasThumb2.getContext('2d').clearRect(0, 0, this.canvasThumb2.width, this.canvasThumb2.height);
                    this.canvasThumb2.getContext('2d').drawImage(image, x, y, w2, h2);
                    this.renderImg(this.canvasThumbHtml, this.rotasi, this.canvasThumb2, 32 / 2, 32 / 2);
                    this.uploadTbl.style.display = 'inline';
                    this.rotasiTbl.style.display = 'inline';
                };
                image.src = (reader.result);
            };
            if (file) {
                reader.readAsDataURL(file);
            }
        };
    }
    async upload(type, url, data) {
        return new Promise((resolve, reject) => {
            console.log('send data');
            let xhr = new XMLHttpRequest();
            xhr.onload = () => {
                if (200 == xhr.status) {
                    resolve(xhr.responseText);
                }
                else {
                    reject(new Error('(' + xhr.status + ') ' + xhr.statusText));
                }
            };
            xhr.onerror = () => {
                reject(new Error('Error'));
            };
            xhr.open(type, url, true);
            // xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.setRequestHeader('Content-type', 'application/json');
            xhr.send(data);
        });
    }
    get listCont() {
        return this.getEl('div.list-cont');
    }
    get form() {
        return this.getEl('form');
    }
    get input() {
        return this.getEl('input');
    }
    get canvasBesarHtml() {
        return this.getEl('canvas.foto');
    }
    get canvasThumbHtml() {
        return this.getEl('canvas.thumb');
    }
    get uploadTbl() {
        return this.getEl('input.upload');
    }
    get rotasiTbl() {
        return this.getEl('button.rotasi');
    }
}
new PhotoUploadPage2();
