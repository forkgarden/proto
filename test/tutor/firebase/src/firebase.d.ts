declare var firebase: IFireBase;

declare interface IFireBase {
	initializeApp(config: IFireBaseConfig): void;
	firestore(): void;
}

declare interface IFireBaseConfig {
	apiKey?: string,
	authDomain?: string,
	databaseURL?: string,
	projectId?: string,
	storageBucket?: string,
	messagingSenderId?: string,
	appId?: string,
	measurementId?: string
}

declare class FireDb {

}