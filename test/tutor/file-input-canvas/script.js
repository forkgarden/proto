"use strict";
window.onload = () => {
    canvasPicker(document.querySelector('canvas'), document.querySelector('input[type=file]'));
};
function canvasPicker(preview, input) {
    input.onchange = () => {
        const file = input.files[0];
        const reader = new FileReader();
        const ctx = preview.getContext('2d');
        const image = new Image();
        reader.onload = () => {
            image.onload = () => {
                let ratio = Math.min(preview.width / image.naturalWidth, preview.height / image.naturalHeight);
                ctx.drawImage(image, 0, 0, image.naturalWidth * ratio, image.naturalHeight * ratio);
                console.log(image);
                console.log(preview.toDataURL);
            };
            image.src = (reader.result);
        };
        if (file) {
            reader.readAsDataURL(file);
        }
    };
}
