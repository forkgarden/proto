window.onload = () => {
	canvasPicker(document.querySelector('canvas') as HTMLCanvasElement, document.querySelector('input[type=file]') as HTMLInputElement);
}

function canvasPicker(preview: HTMLCanvasElement, input: HTMLInputElement): void {
	input.onchange = () => {
		const file: File = input.files[0];
		const reader: FileReader = new FileReader();
		const ctx: CanvasRenderingContext2D = preview.getContext('2d');
		const image: HTMLImageElement = new Image();

		reader.onload = () => {
			image.onload = () => {
				let ratio: number = Math.min(preview.width / image.naturalWidth, preview.height / image.naturalHeight);
				ctx.drawImage(image, 0, 0, image.naturalWidth * ratio, image.naturalHeight * ratio);
				console.log(image);
				console.log(preview.toDataURL);
			}
			image.src = (reader.result) as string;
		};

		if (file) {
			reader.readAsDataURL(file);
		}
	}
}
