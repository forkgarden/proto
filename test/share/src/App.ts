///<reference path='Input.ts'/>

class App {

	static readonly input: Input = new Input();
	static readonly data: Data = new Data();
	static readonly db: Db = new Db();

	static inst: App;

	static readonly MODE_EDIT: number = 1;
	static readonly MODE_BARU: number = 2;

	constructor() {
	}

	init(): void {
		App.input.init();
		App.input.render();
	}

}

window.onload = () => {
	(new App()).init();
}