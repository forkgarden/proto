class IPost {
	teks: string;
	noHp: string;
	id: string;
}

class PostObj implements IPost {
	private _teks: string = '';
	private _noHp: string = '';
	private _id: string;

	public get id(): string {
		return this._id;
	}
	public set id(value: string) {
		this._id = value;
	}

	public get teks(): string {
		return this._teks;
	}
	public set teks(value: string) {
		this._teks = value;
	}
	public get noHp(): string {
		return this._noHp;
	}
	public set noHp(value: string) {
		this._noHp = value;
	}
}


class Data {
}