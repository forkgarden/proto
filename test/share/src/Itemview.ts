class ItemView extends BaseComponent {
	private _post: PostObj;
	public get post(): PostObj {
		return this._post;
	}
	public set post(value: PostObj) {
		this._post = value;
	}

	constructor() {
		super();
		this._elHtml = this.getTemplate("div.item");
	}

	get share(): HTMLLinkElement {
		return this.getEl('a') as HTMLLinkElement;
	}

	get text(): HTMLDivElement {
		return this.getEl('div.text') as HTMLDivElement;
	}

	get editTbl(): HTMLButtonElement {
		return this.getEl('button.edit') as HTMLButtonElement;
	}

	get delTbl(): HTMLButtonElement {
		return this.getEl('button.hapus') as HTMLButtonElement;
	}



}