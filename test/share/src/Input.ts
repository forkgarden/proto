class Input extends BaseComponent {
	private mode: number;
	private id: string;

	init(): void {
		this._elHtml = document.body.querySelector('div.cont');
		this.mode = App.MODE_BARU;

		this.simpanTbl.onclick = () => {
			if (App.MODE_EDIT == this.mode) {
				let post: PostObj = App.db.getById(this.id)
				post.teks = this.textArea.value;
				post.noHp = this.nohp.value;
				App.db.update(post);
			}
			else if (App.MODE_BARU == this.mode) {
				let post: PostObj = new PostObj();
				post.teks = this.textArea.value;
				post.noHp = this.nohp.value;
				App.db.insert(post);
			}
			else {
				console.log('this mode ' + this.mode);
				throw new Error();
			}

			this.id = null;
			this.mode = App.MODE_BARU;
			this.textArea.value = '';
			this.nohp.value = '';
			this.render();
		}

	}

	render(): void {
		let posts: PostObj[] = App.db.get();

		this.list.innerHTML = '';
		this.list.style.display = 'block';

		posts.forEach((item: PostObj) => {
			let view: ItemView = new ItemView();

			view.text.innerHTML = (this.renderHtml(item.teks));
			view.post = item;
			view.attach(this.list);
			view.share.href = 'whatsapp://send?text=' + window.encodeURIComponent(this.ubahWAME(item.teks, item.noHp));

			view.editTbl.onclick = () => {
				this.edit(view.post.id);
			}

			view.delTbl.onclick = () => {
				let jawaban: boolean = window.confirm("hapus data?");
				if (jawaban) {
					App.db.hapus(item.id);
					this.render();
				}
			}
		});
	}

	edit(id: string): void {
		let post: PostObj = App.db.getById(id);
		this.id = id;
		this.textArea.value = post.teks;
		this.nohp.value = post.noHp;
		this.mode = App.MODE_EDIT;
		this.list.style.display = 'none';
	}

	ubahWAME(str: string, nohp: string): string {
		return str.replace(/#wame#/gi, 'https://wa.me/' + nohp + "?text=Assalamualaikum");
	}

	renderHtml(str: string): string {
		let hasil: string = '';

		for (let i: number = 0; i < str.length; i++) {
			let code: number = str.charCodeAt(i);
			if (10 == code) {
				hasil += '<br\>';
			}
			else {
				hasil += str.charAt(i);
			}
		}
		return hasil;
	}

	get nohp(): HTMLInputElement {
		return this.getEl('input.nohp') as HTMLInputElement;
	}

	get textArea(): HTMLTextAreaElement {
		return this.getEl('textarea') as HTMLTextAreaElement;
	}

	get simpanTbl(): HTMLButtonElement {
		return this.getEl('div.input button.simpan') as HTMLButtonElement;
	}

	get list(): HTMLDivElement {
		return this.getEl('div.list-cont') as HTMLDivElement;
	}
}
