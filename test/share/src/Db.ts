class Db {
	private nama: string = "share_data";

	hapus(id: string) {
		let postAr: PostObj[] = this.get();
		let post2: PostObj[] = [];

		postAr.forEach((item: PostObj) => {
			if (item.id != id) {
				post2.push(item);
			}
		});

		this.simpan(post2);
	}

	update(post: PostObj) {
		let postAr: PostObj[] = this.get();

		postAr.forEach((item: PostObj) => {
			if (item.id == post.id) {
				item.noHp = post.noHp;
				item.teks = post.teks;
			}
		});

		this.simpan(postAr);
	}

	get(): PostObj[] {
		let str: string = window.localStorage.getItem(this.nama);
		let strAr: string[] = JSON.parse(str);
		let hasil: PostObj[] = [];

		// console.group('get');
		// console.log(str);
		// console.log(strAr)
		// console.groupEnd();

		if (!strAr) {
			// console.log('array kosong')
			// console.groupEnd();
			return []
		}

		strAr.forEach((item: string) => {
			hasil.push(this.fromObj(JSON.parse(item)));
		});

		return hasil;
	}

	getById(id: string): PostObj {
		let postObjAr: PostObj[];
		postObjAr = this.get();

		for (let i: number = 0; i < postObjAr.length; i++) {
			if (postObjAr[i].id == id) return postObjAr[i];
		}

		return null;
	}

	insert(post: PostObj): void {
		let posts: PostObj[] = this.get();
		let date: Date = new Date();

		post.id = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds()) + '';
		posts.push(post);
		this.simpan(posts);
	}

	toStr(posts: PostObj[]): string {
		let strAr: string[] = [];

		posts.forEach((item: PostObj) => {
			let iPost: IPost = this.toObj(item);
			let str: string = JSON.stringify(iPost);

			strAr.push(str);
		});

		// console.group('to string');
		// console.log('strAr:');
		// console.log(strAr);
		// console.log("_");
		// console.groupEnd();

		return JSON.stringify(strAr);
	}

	simpan(posts: PostObj[]): void {
		window.localStorage.setItem(this.nama, this.toStr(posts));
		// this.test();
	}

	test(): void {
		let data1: string = JSON.stringify(this.simpan(this.get()));
		let data2: string = JSON.stringify(this.simpan(this.get()));

		if (data1 != data2) {
			console.log(data1);
			console.log(data2);
			new Error('');
		}
	}

	toObj(post: PostObj): IPost {
		return {
			noHp: post.noHp,
			teks: post.teks,
			id: post.id
		}
	}

	fromObj(obj: IPost): PostObj {
		let post: PostObj = new PostObj();
		post.noHp = obj.noHp;
		post.teks = obj.teks;
		post.id = obj.id;
		return post;
	}
}