function petaKosong(x: number, y: number): boolean {
	return (peta[y].charAt(x) == " ");
}

function petaPosValid(x: number, y: number): boolean {

	if (x < 0) return false;
	if (y >= peta.length) return false;
	if (x >= peta[y].length) return false;
	return petaKosong(x, y);
}

function petaAmbilNilai(x: number, y: number): string {
	// console.log('peta ambil nilai ' + x + '/' + y);
	let hasil: string = (peta[y].charAt(x));
	return hasil;
}

function petaSetNilai(x: number, y: number, nilai: string): void {
	let str: string = peta[y];
	// console.log('str ' + str);
	// console.log('x ' + x + '/y ' + y + '/' + nilai);
	str = str.slice(0, x) + nilai + str.slice(x + 1);
	// console.log('str ' + str);
	peta[y] = str;

	// console.log('set nilai ' + x + '-' + y + '-' + nilai);
}
