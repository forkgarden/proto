const PF_CEPAT: number = 1;
const PF_A_STAR: number = 2;

const ST_AWAL: number = 0;
const ST_MULAI: number = 1;	//belum ada dipilih, inter-state
const ST_ADA_DIPILIH: number = 2;
const ST_TENGAH: number = 3;
const ST_GAME_OVER: number = 4;

let state: number = ST_AWAL;

const warna: string[] = [
	"#ff0000",
	"#00ff00",
	"#0000ff",
	"#ffff00",
	"#ff00ff",
	"#00ffff",
	"#ffffff"
]

let waktu: TWaktu = {
	total: 100,
	sekarang: 100,
}
let waktu2: number = 100;


const gp: number = 32 * 8;
const gl: number = 32 * 7;

const isiPanjang: number = 4;
const isiLebar: number = 3;

let posAwalX: number;
let posAwalY: number;
let posAkhirX: number;
let posAkhirY: number;
let hasil: number[][];
let nilaiAwal: string;
let nilaiSekarang: string;

let cellAr: Cell[] = [];
let cellMax: number = 1000;			//maksimum cell boleh dibuat

let pfConfig: pfConfig = {
	mode: PF_A_STAR,
	jarakx: 1,
	jaraky: 1
}

let peta: string[] = [
	"XXXXXXXX",
	"X      X",
	"X 4212 X",
	"X 1221 X",
	"X 3341 X",
	"X      X",
	"XXXXXXXX"
]

let kanvas: HTMLCanvasElement;
let kanvasCtx: CanvasRenderingContext2D;
let kanvasScaleX: number = 1;
let kanvasScaleY: number = 1;

let gbrBox: HTMLImageElement;

let viewDialogDepan: menuDepan = {
	view: null,
	tombol: null
}

let viewDialogSelesai: menuDepan = {
	view: null,
	tombol: null
}

let viewDialogGameOver: menuDepan = {
	view: null,
	tombol: null
}