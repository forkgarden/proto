let krkTest: Karakter = {
	jalurn: 0,
	jalur: [[1, 1], [2, 1]],
	pindahn: 0,
	pindahJml: 10,
	pos: {
		x: 32,
		y: 32
	},
	status: st_idle
}

function test(): void {
	testPosisiDiGrid();
	testJalan();
}

function krkTestReset(): void {
	krkTest = {
		jalurn: 0,
		jalur: [[1, 1], [2, 1]],
		pindahn: 0,
		pindahJml: 10,
		pos: {
			x: 32,
			y: 32
		},
		status: st_idle
	}
}

function testPosisiDiGrid(): void {
	if (!krkCheckPosisiDiGrid(krkTest)) throw new Error('');
}

function testJalan(): void {
	krkTestReset();

	krkPindahGrid(krkTest);
	console.log(krkTest);
	if (krkTest.pos.x != 32 + 3.2) throw new Error();
	if (krkTest.pos.y != 32) throw new Error();

	krkTestReset();
}

function testPosisi(): void {
	krkTestReset();

	if (krkPosisiGrid(krkTest).x != 1) throw new Error();
	if (krkPosisiGrid(krkTest).y != 1) throw new Error();
}