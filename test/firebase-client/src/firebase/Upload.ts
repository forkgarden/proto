import { IFireBase, IUploadTask, IFBStorage, IFBReference } from "./IFirebase.js";
import { FireBaseConnector } from "./FireBaseConnector.js";

declare var firebase: IFireBase;

export class Upload {

	async init(): Promise<any> {
		let fb: FireBaseConnector = new FireBaseConnector();

		await fb.init();
		return Promise.resolve();
	}

	buatNama(): string {
		let hasil: string = 'foto';

		for (let i: number = 0; i < 10; i++) {
			hasil += Math.floor(Math.random() * 10);
		}
		let date: Date = new Date();
		hasil += '_' + Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds());

		return hasil;
	}

	private getUploadTask(path: string, dataUrl: string): IUploadTask {
		let storage: IFBStorage = firebase.storage();
		let storageRef: IFBReference = storage.ref();
		let fileRef: IFBReference;
		let name: string = this.buatNama();

		// console.log(name);

		try {
			fileRef = storageRef.child(path + name);
			console.log('file ref' + fileRef);
			return fileRef.putString(dataUrl, 'data_url');
		}
		catch (error) {
			console.log(error);
		}

		return null;
	}

	private async getUrl(ref: IFBReference): Promise<string> {
		return new Promise((resolve, reject) => {
			ref.getDownloadURL().then((url: string) => {
				resolve(url);
			}).catch((e) => {
				reject(e);
			})
		});
	}

	private async loading(uploadTask: IUploadTask): Promise<IFBReference> {
		return new Promise((resolve, reject) => {
			uploadTask.on('state_changed',
				(snapshot: any) => {
					snapshot;
					// Observe state change events such as progress, pause, and resume
					// Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
					// var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
					// console.log('Upload is ' + progress + '% done');
					// switch (snapshot.state) {
					// 	case firebase.storage.TaskState.PAUSED: // or 'paused'
					// 		console.log('Upload is paused');
					// 		break;
					// 	case firebase.storage.TaskState.RUNNING: // or 'running'
					// 		console.log('Upload is running');
					// 		break;
					// }
				},
				(_error: any) => {
					console.log(_error);
					reject(_error);
				},
				() => {
					resolve(uploadTask.snapshot.ref);
					// Handle successful uploads on complete
					// For instance, get the download URL: https://firebasestorage.googleapis.com/...
					// uploadTask.snapshot.ref.getDownloadURL().then((url) => {
					// 	console.log('File available at', url);
					// 	downloadUrl = url;
					// });
				}
			);
		});
	}

	async upload(path: string, kanvas: HTMLCanvasElement): Promise<string> {
		let upload: Upload = new Upload();
		let downloadUrl: string = '';

		let uploadTask: IUploadTask = upload.getUploadTask(path, kanvas.toDataURL());

		downloadUrl = await this.getUrl((await this.loading(uploadTask)));

		return downloadUrl;
	}

}