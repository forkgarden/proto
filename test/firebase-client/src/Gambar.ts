import { Upload } from "./firebase/Upload.js";

class Gambar {
	private kontek: CanvasRenderingContext2D;
	private upload: Upload = new Upload();

	constructor() {
		this.kontek = this.kanvas.getContext('2d');
		this.kontek.drawImage(this.gbr, 0, 0);
		this.upload.init();
	}

	async uploadGbr(): Promise<string> {
		let downloadUrl: string = '';

		downloadUrl = await this.upload.upload('silsilah/', this.kanvas);

		return downloadUrl;
	}

	get gbr(): HTMLImageElement {
		return document.body.querySelector('img') as HTMLImageElement;
	}

	get kanvas(): HTMLCanvasElement {
		return document.body.querySelector('canvas') as HTMLCanvasElement;
	}
}

window.onload = () => {
	let gbr: Gambar = new Gambar();
	gbr.uploadGbr()
		.then((url: string) => {
			console.log('url: ' + url);
		})
		.catch((e) => {
			console.log(e);
		})
}