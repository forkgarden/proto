export class AppCamera {
	private video: HTMLVideoElement = null;

	constructor() {
		window.onload = () => {
			this.video = document.querySelector('video') as HTMLVideoElement;
			//this.cameraStream();

			let tombol: HTMLButtonElement;
			tombol = document.querySelector('button.mulai') as HTMLButtonElement;
			tombol.onclick = () => {
				this.cameraStream();
			}
		}
	}

	cameraStream() {
		navigator.getUserMedia(
			// Options
			{
				video: true
			},
			// Success Callback
			(stream: MediaStream) => {

				// Create an object URL for the video stream and
				// set it as src of our HTLM video element.
				console.log(stream);
				this.video.srcObject = stream;

				// Play the video element to show the stream to the user.
				this.video.play();

			},
			// Error Callback
			(err: MediaStreamError) => {

				// Most common errors are PermissionDenied and DevicesNotFound.
				console.error(err);

			}
		);
	}


}

new AppCamera();