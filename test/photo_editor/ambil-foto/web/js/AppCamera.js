export class AppCamera {
    constructor() {
        this.video = null;
        window.onload = () => {
            this.video = document.querySelector('video');
            //this.cameraStream();
            let tombol;
            tombol = document.querySelector('button.mulai');
            tombol.onclick = () => {
                this.cameraStream();
            };
        };
    }
    cameraStream() {
        navigator.getUserMedia(
        // Options
        {
            video: true
        }, 
        // Success Callback
        (stream) => {
            // Create an object URL for the video stream and
            // set it as src of our HTLM video element.
            console.log(stream);
            this.video.srcObject = stream;
            // Play the video element to show the stream to the user.
            this.video.play();
        }, 
        // Error Callback
        (err) => {
            // Most common errors are PermissionDenied and DevicesNotFound.
            console.error(err);
        });
    }
}
new AppCamera();
