import { BaseComponent } from "./BaseComponent.js";

export class Tree {
	private foto: Foto = null;

	constructor() {
		window.onload = () => {
			this.init();
		}
	}

	init(): void {
		this.foto = new Foto();
		this.foto.attach(document.body);
	}
}

export class Foto extends BaseComponent {
	private btn: HTMLButtonElement = null;
	private cont: HTMLDivElement = null;
	private childCont: HTMLDivElement = null;
	private childs: Array<Foto> = [];
	private buka: boolean = false;

	constructor() {
		super();
		this._template = `
			<div class='tree'>
				<button class='foto'>[ foto ]</button>
				<div class='child-cont'>
					<div class='child'></div>
				</div>
			</div>
		`;

		this.build();
		this.btn = this.getEl('button.foto') as HTMLButtonElement;
		this.cont = this.getEl('div.child-cont') as HTMLDivElement;
		this.childCont = this.getEl('div.child-cont div.child') as HTMLDivElement;

		this.btn.onclick = () => {
			this.onClick();
		}
	}

	onClick(): void {
		console.log('click');

		if (this.childs.length == 0) {
			this.childs.push(new Foto());
			this.childs.push(new Foto());
		}

		this.childs[0].attach(this.childCont);
		this.childs[1].attach(this.childCont);

		this.buka = !this.buka;

		if (this.buka) {
			this.cont.style.display = 'block';
		}
		else {
			this.cont.style.display = 'none';
		}
	}
}

let tree: Tree = new Tree();
console.log(tree);