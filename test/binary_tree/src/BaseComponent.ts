export class BaseComponent {
	protected _template: string = '';
	protected _elHtml: HTMLElement = document.createElement('div');
	protected _parent: HTMLElement;

	onRender(): void {

	}

	onAttach(): void {

	}

	onBuild(): void {

	}

	onDetach(): void {

	}

	attach(parent: HTMLElement): void {
		parent.appendChild(this._elHtml);
		this._parent = parent;
		this.onAttach();
	}

	detach(): boolean {
		if (this._elHtml.parentElement) {
			this._elHtml.parentElement.removeChild(this._elHtml);
			this.onDetach();
			return true;
		}

		this.onDetach();
		return false;
	}

	show(el?: HTMLElement): void {
		if (!el) {
			el = this._elHtml;
		}

		el.style.display = 'block';
	}

	hide(el?: HTMLElement): void {
		if (!el) {
			el = this._elHtml;
		}

		el.style.display = 'none';
	}

	getEl(query: string): HTMLElement {
		let el: HTMLElement;

		el = this._elHtml.querySelector(query);

		if (el) {
			return el
		} else {
			console.log(this._elHtml);
			console.log(query);
			throw new Error('query not found ');
		}
	}

	build(): void {
		let div: HTMLElement = document.createElement('div');
		let el: HTMLElement;

		div.innerHTML = this._template;

		el = div.firstElementChild as HTMLElement;

		this._elHtml = el;

		this.onBuild();
	}

	render(parent: HTMLElement): void {
		let div: HTMLElement = document.createElement('div');
		let el: HTMLElement;

		div.innerHTML = this._template;

		el = div.firstElementChild as HTMLElement;

		this._elHtml = el;

		if (parent) {
			parent.appendChild(el);
			this._parent = parent;
		}

		this.onRender();
	}

	public get template() {
		return this._template;
	}

	public set template(str: string) {
		this._template = str;
	}

	public get elHtml(): HTMLElement {
		return this._elHtml;
	}
	public set elHtml(value: HTMLElement) {
		this._elHtml = value;
	}

}
