import { BaseComponent } from "./BaseComponent.js";
export class Tree {
    constructor() {
        this.foto = null;
        window.onload = () => {
            this.init();
        };
    }
    init() {
        this.foto = new Foto();
        this.foto.attach(document.body);
    }
}
export class Foto extends BaseComponent {
    constructor() {
        super();
        this.btn = null;
        this.cont = null;
        this.childCont = null;
        this.childs = [];
        this.buka = false;
        this._template = `
			<div class='tree'>
				<button class='foto'>[ foto ]</button>
				<div class='child-cont'>
					<div class='child'></div>
				</div>
			</div>
		`;
        this.build();
        this.btn = this.getEl('button.foto');
        this.cont = this.getEl('div.child-cont');
        this.childCont = this.getEl('div.child-cont div.child');
        this.btn.onclick = () => {
            this.onClick();
        };
    }
    onClick() {
        console.log('click');
        if (this.childs.length == 0) {
            this.childs.push(new Foto());
            this.childs.push(new Foto());
        }
        this.childs[0].attach(this.childCont);
        this.childs[1].attach(this.childCont);
        this.buka = !this.buka;
        if (this.buka) {
            this.cont.style.display = 'block';
        }
        else {
            this.cont.style.display = 'none';
        }
    }
}
let tree = new Tree();
console.log(tree);
