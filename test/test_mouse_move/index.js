class Move {
    constructor() {
        let box = document.querySelector('div.box');
        let info = document.querySelector('div.info');
        let boxStartX;
        let boxStartY;
        let windowStartX;
        let windowStartY;
        let boxDragged;
        box.style.left = '100px';
        box.style.top = '100px';
        box.onmousedown = (evt) => {
            evt.stopPropagation();
            boxStartX = parseInt(box.style.left);
            boxStartY = parseInt(box.style.top);
            windowStartX = evt.pageX;
            windowStartY = evt.pageY;
            boxDragged = true;
            console.log('start drag');
        };
        window.onmouseup = () => {
            boxDragged = false;
        };
        window.onmousemove = (evt) => {
            let gapX;
            let gapY;
            if (boxDragged) {
                gapX = evt.pageX - windowStartX;
                gapY = evt.pageY - windowStartY;
                info.innerText = gapX + '/' + gapY;
                box.style.left = (boxStartX + gapX) + 'px';
                box.style.top = (boxStartY + gapY) + 'px';
                // console.log('mouse move ' + gapX + '/' + gapY);
            }
        };
    }
}
