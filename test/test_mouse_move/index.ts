class Move {
    constructor() {
        let box:HTMLDivElement = document.querySelector('div.box') as HTMLDivElement;
        let info:HTMLDivElement = document.querySelector('div.info') as HTMLDivElement;

        let boxStartX:number;
        let boxStartY:number;

        let windowStartX:number;
        let windowStartY:number;

        let boxDragged:boolean;

        box.style.left = '100px';
        box.style.top = '100px';


        box.onmousedown = (evt) => {
            evt.stopPropagation();

            boxStartX = parseInt(box.style.left);
            boxStartY = parseInt(box.style.top);

            windowStartX = evt.pageX;
            windowStartY = evt.pageY;

            boxDragged = true;

            console.log('start drag');
        }

        window.onmouseup = () => {
            boxDragged = false;
        }

        window.onmousemove = (evt) => {
            let gapX:number;
            let gapY:number;

            if (boxDragged) {
                gapX = evt.pageX - windowStartX;
                gapY = evt.pageY - windowStartY;

                info.innerText = gapX + '/' + gapY;
                box.style.left = (boxStartX + gapX) + 'px';
                box.style.top = (boxStartY + gapY) + 'px';
                // console.log('mouse move ' + gapX + '/' + gapY);
            }


        }
    }
}